/**
 * This source file is used to print out a stack-trace when your program
 * segfaults. It is relatively reliable and spot-on accurate.
 *
 * This code is in the public domain. Use it as you see fit, some credit
 * would be appreciated, but is not a prerequisite for usage. Feedback
 * on it's use would encourage further development and maintenance.
 *
 * Due to a bug in gcc-4.x.x you currently have to compile as C++ if you want
 * demangling to work.
 *
 * Please note that it's been ported into my ULS library, thus the check for
 * HAS_ULSLIB and the use of the sigsegv_outp macro based on that define.
 *
 * Author: Jaco Kroon <jaco@kroon.co.za>
 *
 * Copyright (C) 2005 - 2009 Jaco Kroon
 *
 * Modified 1/11/2010 Bob McCarthy <bob.mccarthy@experient.com>
 * added output to file for use with Experient Controller.
 * added forced reboot
 */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

/* Bug in gcc prevents from using CPP_DEMANGLE in pure "C" */
#if !defined(__cplusplus) && !defined(NO_CPP_DEMANGLE)
#define NO_CPP_DEMANGLE
#endif

#include <memory.h>
//#include <stdlib.h>
//#include <stdio.h>
//#include <unistd.h>
//#include <signal.h>
#include <fstream>
#include <ucontext.h>
#include <dlfcn.h>
#include "defines.h"
#include "header.h"
#include <sys/reboot.h>
#ifndef NO_CPP_DEMANGLE
#include <cxxabi.h>
#ifdef __cplusplus
using __cxxabiv1::__cxa_demangle;
using namespace std;
#endif
#endif

#ifdef HAS_ULSLIB
#include "uls/logger.h"
#define sigsegv_outp(x)         sigsegv_outp(,gx)
#else
#define sigsegv_outp(x, ...)    fprintf(stderr, x "\n", ##__VA_ARGS__)
#endif

#if defined(REG_RIP)
# define SIGSEGV_STACK_IA64
# define REGFORMAT "%016lx"
# define REGFORMATLEN 16
#elif defined(REG_EIP)
# define SIGSEGV_STACK_X86
# define REGFORMAT "%08x"
# define REGFORMATLEN 8
#else
# define SIGSEGV_STACK_GENERIC
# define REGFORMAT "%x"
# define REGFORMATLEN 0
#endif

static void signal_segv(int signum, siginfo_t* info, void*ptr) {
	static const char *si_codes[3] = {"", "SEGV_MAPERR", "SEGV_ACCERR"};

	int                      i, f = 0;
	ucontext_t               *ucontext = (ucontext_t*)ptr;
	Dl_info                  dlinfo;
	void                     **bp = 0;
	void                     *ip = 0;
        ofstream                 OutputFile;
        int                      RegWidth = 1;
        struct timespec          timespecTimeNow;
        string                   stringTimeStamp;
        extern string            get_time_stamp(timespec timespecArg); 

        OutputFile.open(charPREVIOUS_SHUT_DOWN_STATUS_FILE, ios::app);
        OutputFile << "Segmentation_Fault = <<<SEGFAULT" << endl;
        OutputFile << "Segmentation Fault!" << endl;        
	sigsegv_outp("Segmentation Fault!");
	sigsegv_outp("info.si_signo = %d", signum);
        OutputFile <<"info.si_signo = " << signum << endl;
	sigsegv_outp("info.si_errno = %d", info->si_errno);
        OutputFile <<"info.si_errno = " << info->si_errno << endl;
	sigsegv_outp("info.si_code  = %d (%s)", info->si_code, si_codes[info->si_code]);
        OutputFile <<"info.si_code  = " << info->si_code << " (" << si_codes[info->si_code] << ")" << endl;
	sigsegv_outp("info.si_addr  = %p", info->si_addr);
        OutputFile <<"info.si_addr  = " <<  info->si_addr << endl;        

        if (NGREG) RegWidth = (int) ceil(log10(NGREG));
	for(i = 0; i < NGREG; i++){
		sigsegv_outp("reg[%02d]       = 0x" REGFORMAT, i, ucontext->uc_mcontext.gregs[i]);
                OutputFile << setfill('0') << left << setbase(10) << "reg["  << right << setw(RegWidth) << i << "]" << " = " << hex  << "0x" <<  setw(REGFORMATLEN) << setfill('0') << ucontext->uc_mcontext.gregs[i] << endl;}

#ifndef SIGSEGV_NOSTACK
#if defined(SIGSEGV_STACK_IA64) || defined(SIGSEGV_STACK_X86)
#if defined(SIGSEGV_STACK_IA64)
	ip = (void*)ucontext->uc_mcontext.gregs[REG_RIP];
	bp = (void**)ucontext->uc_mcontext.gregs[REG_RBP];
#elif defined(SIGSEGV_STACK_X86)
	ip = (void*)ucontext->uc_mcontext.gregs[REG_EIP];
	bp = (void**)ucontext->uc_mcontext.gregs[REG_EBP];
#endif

	sigsegv_outp("Stack trace:");
        OutputFile <<"Stack trace:" << endl;        
	while(bp && ip) {
		if(!dladdr(ip, &dlinfo))
			break;

		const char *symname = dlinfo.dli_sname;

#ifndef NO_CPP_DEMANGLE
		int status;
		char * tmp = __cxa_demangle(symname, NULL, 0, &status);

		if (status == 0 && tmp)
			symname = tmp;
#endif

		sigsegv_outp("% 2d: %p <%s+%lu> (%s)",
				++f,
				ip,
				symname,
				(unsigned long)ip - (unsigned long)dlinfo.dli_saddr,
				dlinfo.dli_fname);
                if(symname == NULL) symname = "(NULL)";
                OutputFile << setbase(10) << setw(2) << right << f << ": " << ip << " <" << symname << "+" <<  (unsigned long)ip - (unsigned long)dlinfo.dli_saddr << "> (" << dlinfo.dli_fname << ")" << endl;

#ifndef NO_CPP_DEMANGLE
		if (tmp)
			free(tmp);
#endif

		if(dlinfo.dli_sname && !strcmp(dlinfo.dli_sname, "main"))
			break;

		ip = bp[1];
		bp = (void**)bp[0];
	}
#else
	sigsegv_outp("Stack trace (non-dedicated):");
        OutputFile <<"Stack trace (non-dedicated):" << endl;        
	sz = backtrace(bt, 20);
	strings = backtrace_symbols(bt, sz);
	for(i = 0; i < sz; ++i){
		sigsegv_outp("%s", strings[i]);
                OutputFile << strings[i] << endl;}        
#endif
	sigsegv_outp("End of stack trace.");
        OutputFile << "End of stack trace." << endl << endl;;       
#else
	sigsegv_outp("Not printing stack strace.");
        OutputFile <<"Not printing stack strace." << endl;       
#endif
        OutputFile <<"SEGFAULT" << endl << endl;;
        OutputFile << ERR_INI_KEY_6 << " = true" << endl;
        clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
        stringTimeStamp = get_time_stamp(timespecTimeNow);
        OutputFile << setbase(10) << left << "TimeStamp = " << stringTimeStamp << endl;
                       
        OutputFile.close();
        raise(signum);

//        sleep(SEG_FAULT_REBOOT_DELAY_SEC);
//        if (signum == SIGSEGV) {//cout << " ... system rebooting" << endl;reboot(RB_AUTOBOOT);}
//	_exit (-1);





}

static void __attribute__((constructor)) setup_sigsegv() {
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_sigaction = signal_segv;
	action.sa_flags = SA_SIGINFO | SA_RESETHAND;
	if(sigaction(SIGSEGV, &action, NULL) < 0)
		perror("sigaction");
}

