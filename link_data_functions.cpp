/*****************************************************************************
* FILE: link_data_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the Call_Data Class 
*
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"



void Link_Data::fClear()
{
 strChannelone.clear();
 strChanneloneID.clear();
 strChanneltwo.clear();
 strChanneltwoID.clear();
 strCallerIDone.clear();
 strCallerIDtwo.clear();
 strCustNameOne.clear();
 strCustNameTwo.clear();
 IPaddressOne.fClear();
 IPaddressTwo.fClear();
 intTransferFromPosition = 0;
 bBothChannelsPositions = false;
 
}

void Link_Data::fDisplay()
{
 //cout <<  "Channel 1     : " <<   strChannelone   << endl;
 //cout <<  "Channel 1 id  : " <<   strChanneloneID << endl;
 //cout <<  "Channel 2     : " <<   strChanneltwo   << endl;
 //cout <<  "Channel 2 id  : " <<   strChanneltwoID << endl;
 //cout <<  "Caller id   1 : " <<   strCallerIDone  << endl;
 //cout <<  "Caller Name 1 : " <<   strCustNameOne  << endl;
 //cout <<  "Caller id 2   : " <<   strCallerIDtwo  << endl;
 //cout <<  "Caller Name 2 : " <<   strCustNameTwo  << endl;
 //cout <<  "IP Addr     1 : " <<   IPaddressOne.fDisplay();
 //cout <<  "IP Addr     2 : " <<   IPaddressTwo.fDisplay();
 //cout <<  "Tranfer Pos   : " <<   intTransferFromPosition << endl;
 //cout <<  "Both Pos CH   : " <<   bBothChannelsPositions << endl;
}

bool  Link_Data::fAreBothPositionChannels()
{
 int indexOne, indexTwo;
 extern Telephone_Devices       TelephoneEquipment;
 extern bool                    bool_BCF_ENCODED_IP;
 
 if (bool_BCF_ENCODED_IP)
  {
   indexOne = TelephoneEquipment.fIndexofTelephonewithRegistrationName(strChannelone); 
   indexTwo = TelephoneEquipment.fIndexofTelephonewithRegistrationName(strChanneltwo);
  }
 else
  {
   indexOne = TelephoneEquipment.fIndexWithIPaddress(IPaddressOne);
   indexTwo = TelephoneEquipment.fIndexWithIPaddress(IPaddressTwo);
  }

  if ((indexOne >=0)&&(indexTwo>=0)) 
  {bBothChannelsPositions = ((TelephoneEquipment.Devices[indexOne].intPositionNumber>0)&&(TelephoneEquipment.Devices[indexTwo].intPositionNumber>0));}

  if(indexOne >= 0)
   {
    if (TelephoneEquipment.Devices[indexOne].intPositionNumber>0) 
     {
      strCustNameOne = TelephoneEquipment.Devices[indexOne].strCallerIDname; 
      strCallerIDone = TelephoneEquipment.Devices[indexOne].strCallerIDnumber;
     }
   }
  if(indexTwo >= 0)
   {
    if (TelephoneEquipment.Devices[indexTwo].intPositionNumber>0) 
     {
      strCustNameTwo = TelephoneEquipment.Devices[indexTwo].strCallerIDname; 
      strCallerIDtwo = TelephoneEquipment.Devices[indexTwo].strCallerIDnumber;
     } 
   }
 
 return bBothChannelsPositions;
}

