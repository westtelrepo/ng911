
/*****************************************************************************
* FILE: cti.cpp
*  
*
* DESCRIPTION:
* Contains the thread(s) which communicate to the Polycom CTI Web Server 
*
*
* Polycom CTI is Web based
*
*
* AUTHOR: 3/22/2012 Bob McCarthy
* LAST REVISED:  
*
* COPYRIGHT: 2006-2012 Experient Corporation
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//globals
extern queue <ExperientDataClass> 	        queue_objCTIData;



itimerspec                                      itimerspecCTIMessageMonitorDelay;
itimerspec                                      itimerspecCTIPingInterval;
itimerspec                                      itimerspecCTISendDelay;
timer_t                                         timer_tCTIMessageMonitorTimerId;
timer_t                                         timer_tCTI_Ping_Event_TimerId;
timer_t                                         timer_tCTI_Send_Event_TimerId;
ExperientHTTPPort                               CTI_HTTP_Push_Port[NUM_WRK_STATIONS_MAX+1];
queue <string>                                  CTIsendQueue[NUM_WRK_STATIONS_MAX+1];
sem_t                                           sem_tMutexCTIPostQ[NUM_WRK_STATIONS_MAX+1];


ExperientCommPort                                 CTI_TELEPHONE_EVENT_Port;

//ExperientHTTPPort                               CTI_HTTP_NetworkState_Port;

//fwd function
void   *CTI_HTTP_Event_Thread(void *voidArg);
//void   CTI_Messaging_Monitor_Event(union sigval union_sigvalArg);
void   Ping_CTI_Event(union sigval union_sigvalArg);
void   CheckCTIportsStatus();
string PressLineKeyXMLstring(unsigned int i);
string PressSoftKeyXMLstring(unsigned int i);
string DialTelephoneNumberXMLstring(string strTelephoneNumber);
string DialTelephoneNumberXMLstring(string strTelephoneNumber, int iLine);
string LongDialTelephoneNumberXMLstring(string strNumber);
string PressHoldButtonXMLstring();
string PressHeadsetButton();
string PressDialPad(string strButton);
string PostHttpAddressFromWorkstation(int i);
string PressAnswerButtonXMLstring();
string PressBargeButtonXMLstring(int i, bool boolHold);
string PressHandsFreeButtonXMLstring();
string PressSoftKeyOneXMLstring();
string PressSoftKeyTwoXMLstring();
string PressSoftKeyThreeXMLstring();
string PressSoftKeyFourXMLstring();
string TransferCallXMLstring(string strNumber);
string QuickTransferXMLstring(string strNumber);
string PressTransferButtonXMLstring();
string PressVoulumeUpXMLstring();
string PressVoulumeDownXMLstring();
string PressMuteButtonXMLstring();

extern void *CTI_Messaging_Monitor_Event(void *voidArg);



void *CTI_Thread(void *voidArg)
{
 param_t                        params[NUM_WRK_STATIONS_MAX+1];
 int                            intRC;
 int                            index, sharedlineindex;
 MessageClass                   objMessage;
 ExperientDataClass             objData;
 pthread_t                      CTIListenThread[NUM_WRK_STATIONS_MAX+1];
 pthread_t			pthread_tCTImessagingThread;
 pthread_attr_t                 CTIpthread_attr_tAttr;
 struct sigevent                sigeventCTIMessagingMonitorEvent;
// struct sigevent	        sigeventPingCTIEvent;
 struct timespec                timespecRemainingTime;
 string                         strPostData;
 string                         strLineNumber;
 string                         strRegistrationName;
 bool                           boolCallOnHold;
 int                            iLineNumber;
 int                            iLineAlias;
 Thread_Data                	ThreadDataObj;
 string                         strKey = IPWORKS_RUNTIME_KEY;     
 extern vector <Thread_Data> 	ThreadData;
 extern Port_Data               objBLANK_CTI_PORT;
 extern Text_Data		objBLANK_TEXT_DATA;
 extern Call_Data               objBLANK_CALL_RECORD;
 extern Freeswitch_Data         objBLANK_FREESWITCH_DATA;
 extern ALI_Data                objBLANK_ALI_DATA;
 extern Thread_Trap             OrderlyShutDown;
 extern Thread_Trap             UpdateVars;
 extern Telephone_Devices       TelephoneEquipment; 

/*
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING, 901, objBLANK_CALL_RECORD, objBLANK_CTI_PORT, KRN_MESSAGE_142); enQueue_Message(CTI,objMessage);}
*/


 ThreadDataObj.strThreadName ="CTI";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cti.cpp::CTI_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,600, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_CTI_PORT, objBLANK_FREESWITCH_DATA,
                            objBLANK_ALI_DATA,CTI_MESSAGE_600, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(CTI,objMessage);






//Initalize CTI Ports

 for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
  {

#ifdef IPWORKS_V16
   CTI_HTTP_Push_Port[i].SetRuntimeLicense((char*) strIPWORKS_RUN_TIME_KEY.c_str());
#endif
   CTI_HTTP_Push_Port[i].intPortNumber = i;
   CTI_HTTP_Push_Port[i].SetURLServer("localhost");
   CTI_HTTP_Push_Port[i].SetURLPort(intPOLYCOM_CTI_HTTP_PORT);
   CTI_HTTP_Push_Port[i].SetLocalHost(NULL);
   CTI_HTTP_Push_Port[i].SetAuthScheme(1);
   CTI_HTTP_Push_Port[i].SetUser(strCTI_USERNAME.c_str());
   CTI_HTTP_Push_Port[i].SetPassword(strCTI_PASSWORD.c_str());
   CTI_HTTP_Push_Port[i].SetTimeout(2);
   CTI_HTTP_Push_Port[i].Config("KeepAlive=true");
   CTI_HTTP_Push_Port[i].Config("AbsoluteTimeout=true");
   CTI_HTTP_Push_Port[i].Config("TcpNoDelay=true");
//   CTI_HTTP_Push_Port[i].Config("HTTPVersion=1.0");
   CTI_HTTP_Push_Port[i].SetOtherHeaders("Cache-Control: no-cache");
   CTI_HTTP_Push_Port[i].SetPragma("no-cache");
   CTI_HTTP_Push_Port[i].SetContentType("application/x-com-polycom-spipx");
   sem_init(&sem_tMutexCTIPostQ[i],0,1);
  }

 CTI_TELEPHONE_EVENT_Port.fInitializeCTI_TCP_Port();

// CTI_HTTP_Push_Port.SetContentType("application/text/html");

/*
 CTI_HTTP_NetworkState_Port.SetURLServer("192.168.57.11");
 CTI_HTTP_NetworkState_Port.SetURL("http://192.168.57.11/cti");
 CTI_HTTP_NetworkState_Port.SetURLPort(80);
 CTI_HTTP_NetworkState_Port.SetLocalHost(NULL);
 CTI_HTTP_NetworkState_Port.SetAuthScheme(1);
 CTI_HTTP_NetworkState_Port.SetUser(strCTI_USERNAME.c_str());
 CTI_HTTP_NetworkState_Port.SetPassword(strCTI_PASSWORD.c_str());
 CTI_HTTP_NetworkState_Port.Config("Connection=Keep-Alive");
 CTI_HTTP_NetworkState_Port.SetOtherHeaders("Cache-Control: no-cache");
 CTI_HTTP_NetworkState_Port.SetPragma("no-cache");
 CTI_HTTP_NetworkState_Port.SetFollowRedirects(1); 
// CTI_HTTP_NetworkState_Port.SetContentType("application/x-com-polycom-spipx");

*/



/*
 // set up CTI message monitor timer event
 sigeventCTIMessagingMonitorEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventCTIMessagingMonitorEvent.sigev_notify_function 		= CTI_Messaging_Monitor_Event;
 sigeventCTIMessagingMonitorEvent.sigev_notify_attributes 		= NULL;


 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventCTIMessagingMonitorEvent, &timer_tCTIMessageMonitorTimerId);
 if (intRC){Abnormal_Exit(CTI, EX_OSERR, KRN_MESSAGE_180, "timer_tCTIMessageMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecCTIMessageMonitorDelay, 0, intCTI_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intCTI_MESSAGE_MONITOR_INTERVAL_NSEC );
 Set_Timer_Delay(&itimerspecCTIMessageMonitorDelay, 0, intCTI_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0 );
 timer_settime( timer_tCTIMessageMonitorTimerId, 0, &itimerspecCTIMessageMonitorDelay, NULL);		                 // arm timer
*/
  pthread_attr_init(&CTIpthread_attr_tAttr);
 intRC = pthread_create(&pthread_tCTImessagingThread, &CTIpthread_attr_tAttr, *CTI_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(CTI, EX_OSERR, CTI_MESSAGE_690);}



/*
 // set up Message ping Timed Event
 sigeventPingCTIEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventPingCTIEvent.sigev_notify_function 		        = Ping_CTI_Event;
 sigeventPingCTIEvent.sigev_notify_attributes 			= NULL;
 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventPingCTIEvent, &timer_tCTI_Ping_Event_TimerId);
 if (intRC){Abnormal_Exit(CTI, EX_OSERR, KRN_MESSAGE_180, "timer_tCTI_Ping_Event_TimerId", int2strLZ(errno));}
 // load settings into stucture and then stucture into the timer
 itimerspecCTIPingInterval.it_value.tv_sec                       = CTI_PING_INTERVAL_SEC;	                        // Delay for n sec
 itimerspecCTIPingInterval.it_value.tv_nsec                      = 0;	                				//           n nsec
 itimerspecCTIPingInterval.it_interval.tv_sec                    = 0;			                                // if both 0 run once
 itimerspecCTIPingInterval.it_interval.tv_nsec                   = 0;                 					//           n nsec
 timer_settime(timer_tCTI_Ping_Event_TimerId, 0, &itimerspecCTIPingInterval, NULL);	                                // arm timer
*/

 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,607, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_CTI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,CTI_MESSAGE_607); 
 enQueue_Message(CTI,objMessage);

  //start listen threads
 // pthread_attr_init(&CTIpthread_attr_tAttr);
 

  for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
   {
      params[i].intPassedIn = i;
      pthread_attr_setdetachstate(&CTIpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
      intRC = pthread_create(&CTIListenThread[i], &CTIpthread_attr_tAttr, *CTI_HTTP_Event_Thread, (void*) &params[i]);
      if (intRC) {Abnormal_Exit(CTI, EX_OSERR, CTI_MESSAGE_604);}
   }


 //*********************************************************main loop ******************************************************************
 
  while(!boolSTOP_EXECUTION)
   {
    intRC = sem_wait(&sem_tCTIflag);							// wait for data signal
    if (intRC)
     {
      objMessage.fMessage_Create(LOG_WARNING, 615, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                 objBLANK_CTI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CTI_MESSAGE_615); 
      enQueue_Message(CTI,objMessage);
     } 

   // Shut Down Trap Area
   if (boolSTOP_EXECUTION)
    {
     CTI_TELEPHONE_EVENT_Port.TCP_Server_Port.Shutdown();
     break;
    }
 
  if (boolUPDATE_VARS) 
   {
    UpdateVars.boolCTIThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    UpdateVars.boolCTIThreadReady = false;       
   }
  
   intRC = sem_wait(&sem_tMutexCTIInputQ);						// Lock CTI INPUT Q
   if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexCTIInputQ, "CTI    - sem_wait@sem_tMutexCTIInputQ in CTI_Thread", 1);}
   
   while (!queue_objCTIData.empty())  
    {
     objData = queue_objCTIData.front();
     queue_objCTIData.pop();

     if(!objData.intWorkStation)                       {SendCodingError( "cti.cpp - no workstation passed to CTI !"); continue;}
     if(objData.intWorkStation > intNUM_WRK_STATIONS ) {SendCodingError( "cti.cpp - workstation " + int2str(objData.intWorkStation) +" passed to CTI !"); continue;}


     // lock Send Message Q
     intRC = sem_wait(&sem_tMutexCTIPostQ[objData.intWorkStation]);						
     if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexCTIPostQ[objData.intWorkStation], "CTI    - sem_wait@sem_tMutexCTIPostQ in CTI_Thread", 1);}
      ////cout << "Sending from cti" << endl;
     switch(objData.enumWrkFunction)
      {
       case WRK_TOGGLE_HOLD:
            strPostData = PressHoldButtonXMLstring();
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;
      
       case WRK_OFF_HOLD:
     
            if (objData.CallData.CTIsharedLine > 0) {
              // We press LineIndexes not Keys !!!!
              index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
              if (index < 0 ) {break;} 
              iLineNumber = TelephoneEquipment.Devices[index].fLineIndexofLineNumber(objData.CallData.CTIsharedLine);
              strPostData = PressLineKeyXMLstring(iLineNumber);
             // //cout << strPostData << endl;
              CTIsendQueue[objData.intWorkStation].push(strPostData);
            }
            else {
              iLineNumber = objData.FreeswitchData.fLineNumberofPositiononCall(objData.intWorkStation);
              if (iLineNumber > 0) { strPostData = PressLineKeyXMLstring(iLineNumber); CTIsendQueue[objData.intWorkStation].push(strPostData);}
              else                 { break;}
            }
            break;

       case WRK_SEND_DTMF:
            strPostData = PressDialPad(objData.strDTMFChar);
           // //cout << strPostData << endl;
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_DIAL_OUT:
            // //cout << "WRK DIAL OUT " << endl;
             if (bool_POLYCOM_404_FIRMWARE) {
               //iLineNumber = objData.IndexOpenForDialing;
               iLineNumber = TelephoneEquipment.Devices[TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation) ].fLineNumberWithIndex(objData.IndexOpenForDialing);
              // //cout << "Line : " << iLineNumber << endl;
               // this is coming in as a line number not an index ...... Convert from Index
               switch (TelephoneEquipment.Devices[TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation) ].eDeviceType) {
                
                 case POLYCOM_670:
                       strPostData = DialTelephoneNumberXMLstring(objData.CallData.TransferData.strTransfertoNumber);
                       break;
                 case NO_PHONE_DEFINED: 
                       
                       break;
                 default:
                       // NO_PHONE_DEFINED, GUI_PHONE, NON_GUI_PHONE, BOTH_PHONES, AUDIOCODES, IPGATEWAY, NG911SIPTRUNK, POLYCOM_VVX, YEALINK
                       ////cout << "Phone # " << objData.CallData.TransferData.strTransfertoNumber << endl;
                       if (iLineNumber > 0) {strPostData = DialTelephoneNumberXMLstring(objData.CallData.TransferData.strTransfertoNumber, iLineNumber);}
                       else                 {SendCodingError("cti.cpp - WRK_DIAL_OUT Line number < 1"); break;}
                 break;
               }
             }
             else {
               strPostData = LongDialTelephoneNumberXMLstring(objData.CallData.TransferData.strTransfertoNumber);
             }
            ////cout << "Post Data -> " << strPostData << endl;

            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_ANSWER:
            index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
            if (index < 0) {SendCodingError("cti.cpp - case WRK_BARGE index is less than zero"); break;}
            iLineNumber = TelephoneEquipment.Devices[index].fLineIndexofLineNumber(objData.FreeswitchData.objRing_Dial_Data.iRingingLineNumber[objData.intWorkStation]);

            if (objData.CallData.CTIsharedLine)      { strPostData  = PressLineKeyXMLstring(objData.CallData.CTIsharedLine); }
            else                                     { strPostData  = PressLineKeyXMLstring(iLineNumber);}
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;


           
       case WRK_BARGE:
            //ALWAYS A SHARED LINE BARGE !
            //Wes use line pushing which goes by LineIndex .......
          //  //cout << "WRK BARGE ->" << objData.CallData.CTIsharedLine << endl;
          //  //cout << "Position Barging -> " << objData.intWorkStation << endl;

            if (!objData.CallData.CTIsharedLine) {SendCodingError("cti.cpp - case WRK_BARGE Line number is zero"); break;}

            // Must Press page 1 button first !

            index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
            if (index < 0) {SendCodingError("cti.cpp - case WRK_BARGE index is less than zero"); break;}

            iLineNumber = TelephoneEquipment.Devices[index].fLineIndexofLineNumber(objData.CallData.CTIsharedLine);
            if (iLineNumber <= 0) {
              SendCodingError("cti.cpp - case WRK_BARGE: iLineNumber is zero with sharedlinenumber-> "+ int2str(objData.CallData.CTIsharedLine));
              break;
            }
            boolCallOnHold = ((objData.CallData.fNumPositionsInConference() <= objData.CallData.fNumPositionsOnHold())&&(objData.CallData.fNumPositionsInConference() > 0));
            
           // //cout << "line number is -> " << iLineNumber << " All Positions on hold -> " << boolCallOnHold << endl;            
            strPostData = PressBargeButtonXMLstring(iLineNumber, boolCallOnHold);
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;
       
       case WRK_CONF_BARGE:
           // //cout << "WRK_CONF_BARGE ->" << objData.CallData.intConferenceNumber << endl;
            if (!objData.CallData.intConferenceNumber) {break;}                             // if not then we need to calculate the line number ........
             if (bool_POLYCOM_404_FIRMWARE) {
               //iLineNumber = objData.IndexOpenForDialing;  //cout << "line index" << iLineNumber << endl;
               // line number equates to reg.x in the polycom not lineKey not LineIndex
               iLineNumber = TelephoneEquipment.Devices[TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation) ].fLineNumberWithIndex(objData.IndexOpenForDialing);
                switch (TelephoneEquipment.Devices[TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation) ].eDeviceType) {
                
                 case POLYCOM_670:
                       strPostData = DialTelephoneNumberXMLstring(strCONFERENCE_NUMBER_DIAL_PREFIX+int2str(objData.CallData.intConferenceNumber), iLineNumber);
                       break;
                 default:
                       // NO_PHONE_DEFINED, GUI_PHONE, NON_GUI_PHONE, BOTH_PHONES, AUDIOCODES, IPGATEWAY, NG911SIPTRUNK, POLYCOM_VVX, YEALINK
                       if (iLineNumber > 0) {strPostData = DialTelephoneNumberXMLstring(strCONFERENCE_NUMBER_DIAL_PREFIX+int2str(objData.CallData.intConferenceNumber), iLineNumber);}
                       else                 {SendCodingError("cti.cpp - WRK_DIAL_OUT Line number < 1"); break;}
                 break;
               }
             }
             else {
               strPostData = LongDialTelephoneNumberXMLstring(strCONFERENCE_NUMBER_DIAL_PREFIX+int2str(objData.CallData.intConferenceNumber));
             }
           // //cout << "post Data -> " << endl << strPostData << endl;
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_HANGUP:
            strPostData = PressHeadsetButton();
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;          
/*
            if(!objData.boolConnect)
             {
              strPostData = PressSoftKeyThreeXMLstring(); CTIsendQueue[objData.intWorkStation].push(strPostData);break;
             } 
            switch (intPOLYCOM_CTI_END_CALL_SOFTKEY)
             {
              case 1: strPostData = PressSoftKeyOneXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              case 2: strPostData = PressSoftKeyTwoXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              case 3: strPostData = PressSoftKeyThreeXMLstring(); CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              case 4: strPostData = PressSoftKeyFourXMLstring();  CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              case 5: strPostData = PressSoftKeyFourXMLstring();  CTIsendQueue[objData.intWorkStation].push(strPostData);
                      strPostData = PressSoftKeyOneXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              case 6: strPostData = PressSoftKeyFourXMLstring();  CTIsendQueue[objData.intWorkStation].push(strPostData);

                      strPostData = PressSoftKeyTwoXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              
              default: SendCodingError( "cti.cpp - case switch intPOLYCOM_CTI_END_CALL_SOFTKEY ->"+ int2str(intPOLYCOM_CTI_END_CALL_SOFTKEY)); break;
             }   

            break;
*/

       case WRK_COMPLETE_ATT_XFER:
            index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
            if (index < 0) {SendCodingError("cti.cpp.cpp - CODING ERROR in WRK_COMPLETE_ATT_XFER" );break;} 
            switch (TelephoneEquipment.Devices[index].eDeviceType)
             {
              case POLYCOM_670:
                   strPostData = PressSoftKeyThreeXMLstring();
                   CTIsendQueue[objData.intWorkStation].push(strPostData);
                   break;
              case POLYCOM_VVX:
                 //  strPostData = PressSoftKeyFourXMLstring();
                   strPostData = PressTransferButtonXMLstring();
                   CTIsendQueue[objData.intWorkStation].push(strPostData);
                   break;
              default:
                   SendCodingError("cti.cpp.cpp - Telephone not programmed WRK_COMPLETE_ATT_XFER!"); 
                   break;
             }


            break;

       case WRK_TRANSFER:
            index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
            if (index < 0) {SendCodingError("cti.cpp.cpp - CODING ERROR in WRK_TRANSFER" );break;}
            switch (TelephoneEquipment.Devices[index].eDeviceType)
             {
              case POLYCOM_670:
                   strPostData = TransferCallXMLstring(objData.CallData.TransferData.strTransfertoNumber);
                   CTIsendQueue[objData.intWorkStation].push(strPostData);
                   break;
              case POLYCOM_VVX:
                   strPostData = QuickTransferXMLstring(objData.CallData.TransferData.strTransfertoNumber);
                   CTIsendQueue[objData.intWorkStation].push(strPostData);
                   break;
              default:
                   SendCodingError("cti.cpp.cpp - Telephone not programmed WRK_TRANSFER!"); 
                   break;
            }
            strPostData = PressSoftKeyOneXMLstring(); CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_CNX_TRANSFER:
           // //cout << "WRK_CNX_TRANSFER" << endl;
            index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
            if (index < 0) {SendCodingError("cti.cpp.cpp - CODING ERROR in WRK_CNX_TRANSFER" );break;} 
          //  //cout << "size = " << objData.CallData.vTransferData.size() << endl;
            switch  (!objData.CallData.vTransferData.empty())
             {
              case true:

                   switch (TelephoneEquipment.Devices[index].eDeviceType)
                    {
                     case POLYCOM_670:
                          strPostData = PressSoftKeyTwoXMLstring();
                          CTIsendQueue[objData.intWorkStation].push(strPostData);
                          break;
                     case POLYCOM_VVX:
                          strPostData = PressSoftKeyTwoXMLstring();
                          CTIsendQueue[objData.intWorkStation].push(strPostData);
                          break;
                     default:
                          SendCodingError("cti.cpp.cpp - Telephone not programmed WRK_CNX_TRANSFER a!"); 
                          break;
                    }
                 //  strPostData = PressSoftKeyOneXMLstring(); CTIsendQueue[objData.intWorkStation].push(strPostData);
                   break;

             case false:     

                   switch (TelephoneEquipment.Devices[index].eDeviceType)
                    {
                    case POLYCOM_670:
                         switch (intPOLYCOM_CTI_END_CALL_SOFTKEY)
                          {
                           case 1: strPostData = PressSoftKeyOneXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
                           case 2: strPostData = PressSoftKeyTwoXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
                           case 3: strPostData = PressSoftKeyThreeXMLstring(); CTIsendQueue[objData.intWorkStation].push(strPostData);break;
                           case 4: strPostData = PressSoftKeyFourXMLstring();  CTIsendQueue[objData.intWorkStation].push(strPostData);break;
                           case 5: strPostData = PressSoftKeyFourXMLstring();  CTIsendQueue[objData.intWorkStation].push(strPostData);
                                   strPostData = PressSoftKeyOneXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
                           case 6: strPostData = PressSoftKeyFourXMLstring();  CTIsendQueue[objData.intWorkStation].push(strPostData);
                                   strPostData = PressSoftKeyTwoXMLstring();   CTIsendQueue[objData.intWorkStation].push(strPostData);break;
              
                           default: SendCodingError( "cti.cpp - case switch intPOLYCOM_CTI_END_CALL_SOFTKEY ->"+ int2str(intPOLYCOM_CTI_END_CALL_SOFTKEY)); break;
                          }
                          break;
                     case POLYCOM_VVX:
                          strPostData = PressSoftKeyThreeXMLstring();
                          CTIsendQueue[objData.intWorkStation].push(strPostData);
                          break;
                     default:
                          SendCodingError("cti.cpp.cpp - Telephone not programmed WRK_CNX_TRANSFER b!"); 
                          break;
                    } 
 
             }
            
            break;

       case WRK_MUTE:
            strPostData = PressMuteButtonXMLstring();
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_VOLUME_UP:
            strPostData = PressVoulumeUpXMLstring();
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_VOLUME_DOWN:
            strPostData = PressVoulumeDownXMLstring();
            CTIsendQueue[objData.intWorkStation].push(strPostData);
            break;

       case WRK_PRESS_LINE:
            if (objData.CallData.CTIsharedLine > 0) { 
             index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
             if (index < 0) {SendCodingError("cti.cpp - case WRK_BARGE index is less than zero"); break;}
             iLineNumber = TelephoneEquipment.Devices[index].fLineIndexofLineNumber(objData.CallData.CTIsharedLine);
             if (iLineNumber <= 0) {
              SendCodingError("cti.cpp - case WRK_BARGE: iLineNumber is zero with sharedlinenumber-> "+ int2str(objData.CallData.CTIsharedLine));
              break;
             }
             // //cout << "press Line number -> " << iLineNumber << endl;
              strPostData = PressLineKeyXMLstring(iLineNumber);
              CTIsendQueue[objData.intWorkStation].push(strPostData);
            }            

            break;

       case WRK_PICKUP:
            index = TelephoneEquipment.fIndexWithPositionNumber(objData.intWorkStation);
            if (index < 0) {break;}

            //needs to change !!!!!!! aliasing !!!!


            if (!(objData.boolWorkstationIsOnACall)) 
             {
              // Press Headset button
          //    //cout << "Press Headset" << endl;
              strPostData = PressHeadsetButton();
              CTIsendQueue[objData.intWorkStation].push(strPostData);
             }
            else if (objData.CallData.CTIsharedLine > 0)
             { 
          //    //cout << "press SharedLine number -> " << objData.CallData.CTIsharedLine << endl;
              //Press Line Button (alias) 
              strPostData = PressLineKeyXMLstring(TelephoneEquipment.fDetermine_Shared_Line_Phone_LineIndex(objData.CallData.CTIsharedLine, objData.intWorkStation));
              CTIsendQueue[objData.intWorkStation].push(strPostData);

             }
            else
             {
              //Press Line Button (alias) 
              iLineNumber = objData.FreeswitchData.objRing_Dial_Data.iRingingLineNumber[objData.intWorkStation];
              if (iLineNumber > 0) { 
               iLineAlias = TelephoneEquipment.Devices[index].vPhoneLines[iLineNumber].LineIndex;
               strPostData = PressLineKeyXMLstring(iLineAlias); CTIsendQueue[objData.intWorkStation].push(strPostData);
             //  //cout << "press alias .. " << iLineAlias << " Line number ... " << iLineNumber << endl;                              
              }
              else                 { SendCodingError("cti.cpp Invalid Line Number Returned ...WRK_PICKUP");}
             }
            break;

       default:
            SendCodingError( "cti.cpp - Coding Error in cti.cpp switch(objData.enumWrkFunction) ");
            break;
      }// end  switch(objData.enumWrkFunction)
     
     intRC = sem_post(&sem_tMutexCTIPostQ[objData.intWorkStation]);						
               
    }// end  while (!queue_objCTIData.empty())
  
   sem_post(&sem_tMutexCTIInputQ);                                                    // Unlock Lock CTI INPUT Q

   }// while(!boolSTOP_EXECUTION)						


 // shutdowm thread ...
 // timer_delete(timer_tCTIMessageMonitorTimerId);
  experient_nanosleep(THREE_TENTHS_SEC_IN_NSEC);


  for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
   {   
    pthread_join(CTIListenThread[i], NULL);
   }
  pthread_join( pthread_tCTImessagingThread, NULL);
  OrderlyShutDown.boolCTIThreadReady = true;

  while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}    
  objMessage.fMessage_Create(0, 699, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                             objBLANK_CTI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CTI_MESSAGE_699);
  objMessage.fConsole();

 return NULL;
}

void *CTI_HTTP_Event_Thread(void *voidArg)
{
 
 param_t*                                       p              = (param_t*) voidArg;
 int                                            intWorkstation = p->intPassedIn;
 MessageClass                                   objMessage;
 Port_Data                                      objPortData;
 string                                         strPhoneIPAddress;
 string                                         strData;
 int                                            intRC;
 Thread_Data                    		ThreadDataObj; 
     
 extern vector <Thread_Data> 			ThreadData;
 extern Text_Data				objBLANK_TEXT_DATA;
 extern Call_Data                               objBLANK_CALL_RECORD;
 extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
 extern ALI_Data                                objBLANK_ALI_DATA;

 extern ExperientHTTPPort                       CTI_HTTP_Push_Port[NUM_WRK_STATIONS_MAX+1];
 extern queue <string>                          CTIsendQueue[NUM_WRK_STATIONS_MAX+1];
 extern sem_t                                   sem_tMutexCTIPostQ[NUM_WRK_STATIONS_MAX+1];


 ThreadDataObj.strThreadName ="CTI HTTP Port " + int2strLZ(intWorkstation);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cti.cpp::CTI_HTTP_Event_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 objPortData.fLoadPortData(CTI, intWorkstation);
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,608, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            CTI_MESSAGE_608, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(CTI,objMessage);

 strPhoneIPAddress = PostHttpAddressFromWorkstation(intWorkstation);


  while (!boolSTOP_EXECUTION)
  {
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
   intRC = sem_wait(&sem_tMutexCTIPostQ[intWorkstation]);						
   if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexCTIPostQ[intWorkstation], "CTI    - sem_wait@sem_tMutexCTIPostQ in CTI_HTTP_EVENT_Thread", 1);}


    while (!CTIsendQueue[intWorkstation].empty())  
    {
     strData = CTIsendQueue[intWorkstation].front();
     CTIsendQueue[intWorkstation].pop();
 //    strData += "\n\n";
  //   CTI_HTTP_Push_Port[intWorkstation].SetAttachedFile("/datadisk1/ng911/push.txt");
     CTI_HTTP_Push_Port[intWorkstation].SetPostData(strData.c_str(), strData.length());
     CTI_HTTP_Push_Port[intWorkstation].Post(strPhoneIPAddress.c_str());
     CTI_HTTP_Push_Port[intWorkstation].DoEvents();
    }
   intRC = sem_post(&sem_tMutexCTIPostQ[intWorkstation]);						

   CTI_HTTP_Push_Port[intWorkstation].DoEvents();

   experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC);
  }


 CTI_HTTP_Push_Port[intWorkstation].Interrupt();

 return NULL;								
}// end CTI_HTTP_Event_Thread(void *voidArg)






void Ping_CTI_Event(union sigval union_sigvalArg)
{

 //Disable Timer
// if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tCTI_Ping_Event_TimerId, 0, &itimerspecDisableTimer, NULL);}

 

 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tCTI_Ping_Event_TimerId, 0, &itimerspecCTIPingInterval, NULL);} 
}





void *CTI_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 Thread_Data            	ThreadDataObj;
 extern sem_t                   sem_tMutexCTIMessageQ;
 extern sem_t                   sem_tMutexMainMessageQ;
 extern queue <MessageClass>    queue_objMainMessageQ;
 extern queue <MessageClass>    queue_objCTIMessageQ;
 extern Port_Data               objBLANK_CTI_PORT;
 extern Text_Data		objBLANK_TEXT_DATA;
 extern Call_Data               objBLANK_CALL_RECORD;
 extern Freeswitch_Data         objBLANK_FREESWITCH_DATA;
 extern ALI_Data                objBLANK_ALI_DATA;
 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="CTI Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cti.cpp::CTI_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

do
{

 experient_nanosleep(intCTI_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in CTI_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexCTIMessageQ);
 if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexCTIMessageQ, "sem_wait@sem_tMutexCTIMessageQ in CTI_Messaging_Monitor_Event", 1);}


 CTI_TELEPHONE_EVENT_Port.TCP_Server_Port.DoEvents();


 while (!queue_objCTIMessageQ.empty())
  {
    objMessage = queue_objCTIMessageQ.front();
    queue_objCTIMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 600:						
       // "-[DEBUG] Thread Created, PID %%% ThreadSelf %%%"
       break; 
      case 601:						
       // "[WARNING] CTI Thread Failed to Initialize, System Rebooting" ... should not pass thru here handled in krn
       break;
      case 602:						

       break;
      case 603:

       break;
      case 604:
       // "-[WARNING] CTI Port Thread Failed to Initialize, System Rebooting" 						
       break; 
      case 605:
       // "[WARNING] Unable to set CPU affinity for CTI Thread"					
       break; 
      case 606:

       break; 
      case 607:
       //  "-[DEBUG] Message Monitor Timer Event Initialized"					
       break; 
      case 608:
       //"-[DEBUG] Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 609:

       break;
      case 610:

       break;
      case 611:

       break;
      case 612:

       break;
      case 613:

       break;
      case 614: 

       break;
      case 615:
       //  "-[WARNING] Thread Signalling Semaphore failed to Lock"
       break;
      case 699:
       //  "-Thread Complete"
       break;     	
      default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in CTI Monitor\n";
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock CTI Message Q then Main Message Q
 sem_post(&sem_tMutexCTIMessageQ);
 sem_post(&sem_tMutexMainMessageQ);

 
 CheckCTIportsStatus();


/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tCTIMessageMonitorTimerId, 0, &itimerspecCTIMessageMonitorDelay, NULL);}
*/

}while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 691, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_CTI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CTI_MESSAGE_691);
 objMessage.fConsole();

return NULL;
}// end CTI_Messaging_Monitor_Event()

void CheckCTIportsStatus()
{


 return;
}


string PressAnswerButtonXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Answer\r\n</Data></PolycomIPPhone>";
}

string PressBargeButtonXMLstring(int i, bool OnHold)
{
 string strReturn;
 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:Line";
 string strLast   = "\r\n</Data></PolycomIPPhone>";
 string strNumber = int2str(i);

 strReturn = strFirst + strNumber;
// strReturn += "\r\nKey:Line";
// strReturn += strNumber;

 if(!OnHold) {strReturn += "\r\nKey:Softkey1";}
 
 strReturn += strLast;


 return strReturn;

}

string TransferCallXMLstring(string strNumber)
{
 size_t found;
 size_t sz = strNumber.length();

 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:transfer\n";
 string strLast   = "</Data></PolycomIPPhone>";
 string strMiddle = "";

 found = strNumber.find_first_not_of("0123456789");
 if (found != string::npos)           {return "";}

 for( unsigned int i = 0; i < sz; i++)
  {
   strMiddle += "Key:Dialpad";
   strMiddle += strNumber[i];
   strMiddle += "\n"; 
  }
 strMiddle += "Key:Softkey1\n";

// string stMiddle  = "Tel:\\\\" + strNumber;

 return strFirst + strMiddle + strLast;
}

string PressTransferButtonXMLstring()
{

 string strData  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:transfer\n</Data></PolycomIPPhone>\n";

 return strData;
}





string QuickTransferXMLstring(string strNumber)
{
 size_t found;

 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:transfer\n";
 string strLast   = "</Data></PolycomIPPhone>";
 string strMiddle  = "tel:\\\\" + strNumber;
 strMiddle += "\n";
 found = strNumber.find_first_not_of("0123456789");
 if (found != string::npos)           {return "";}




 return strFirst + strMiddle + strLast;
}





string PressVoulumeUpXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:VolUp\r\n</Data></PolycomIPPhone>";
}

string PressVoulumeDownXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:VolDown\r\n</Data></PolycomIPPhone>";
}

string PressMuteButtonXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:MicMute\r\n</Data></PolycomIPPhone>";
}

string PressHandsFreeButtonXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Handsfree\r\n</Data></PolycomIPPhone>";
}

string PressSoftKeyOneXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:SoftKey1\r\n</Data></PolycomIPPhone>";
}

string PressSoftKeyTwoXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\">Key:SoftKey2\r\n</Data></PolycomIPPhone>";
}

string PressSoftKeyThreeXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:SoftKey3\r\n</Data></PolycomIPPhone>";
}

string PressSoftKeyFourXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:SoftKey4\r\n</Data></PolycomIPPhone>";
}

string PressHoldButtonXMLstring()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Hold\r\n</Data></PolycomIPPhone>";
}

string PressHeadsetButton()
{
 return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Headset\r\n</Data></PolycomIPPhone>";
}

string PressDialPad(string strButton) {
string stringError = "cti.cpp - Coding Error in PressDialPad() -> ";

if      (strButton == "*") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:DialpadStar\r\n</Data></PolycomIPPhone>";}
else if (strButton == "0") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad0\r\n</Data></PolycomIPPhone>";}
else if (strButton == "1") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad1\r\n</Data></PolycomIPPhone>";}
else if (strButton == "2") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad2\r\n</Data></PolycomIPPhone>";}
else if (strButton == "3") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad3\r\n</Data></PolycomIPPhone>";}
else if (strButton == "4") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad4\r\n</Data></PolycomIPPhone>";}
else if (strButton == "5") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad5\r\n</Data></PolycomIPPhone>";}
else if (strButton == "6") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad6\r\n</Data></PolycomIPPhone>";}
else if (strButton == "7") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad7\r\n</Data></PolycomIPPhone>";}
else if (strButton == "8") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad8\r\n</Data></PolycomIPPhone>";}
else if (strButton == "9") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:Dialpad9\r\n</Data></PolycomIPPhone>";}
else if (strButton == "#") { return "<PolycomIPPhone><Data priority=\"Critical\"> Key:DialpadPound\r\n</Data></PolycomIPPhone>";}
else {
   stringError += strButton;
   stringError += " Not Coded.\n";
   SendCodingError(stringError);
}
return "";
}



string PressLineKeyXMLstring(unsigned int i)
{
// //cout << "press line --->" << i << endl;
 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:Line";
 string strLast   = "\r\n</Data></PolycomIPPhone>";
 string strNumber = int2str(i);

 return strFirst + strNumber + strLast;
}

string PressSoftKeyXMLstring(unsigned int i)
{
 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:Softkey";
 string strLast   = "\r\n</Data></PolycomIPPhone>";
 string strNumber = int2str(i);

 return strFirst + strNumber + strLast;
}


string LongDialTelephoneNumberXMLstring(string strNumber)
{
 size_t sz = strNumber.length();

 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> Key:Softkey2\n";
 string strLast   = "</Data></PolycomIPPhone>";
 string strMiddle = "";


 for( unsigned int i = 0; i < sz; i++)
  {
   if (!(isdigit(strNumber[i])))     {continue; } 

   strMiddle += "Key:Dialpad";
   strMiddle += strNumber[i];
   strMiddle += "\n"; 

  }
 strMiddle += "Key:Softkey2\n";


 return strFirst + strMiddle + strLast;
}


string DialTelephoneNumberXMLstring(string strTelephoneNumber)
{
 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> tel://";
 string strLast   = "\r\n</Data></PolycomIPPhone>";

 return strFirst + strTelephoneNumber + strLast;
}

string DialTelephoneNumberXMLstring(string strTelephoneNumber, int Line_number)
{
/* Line values 1 to 48 supported"    */
/* LineIndexes must be converted to Line Numbers not line Keys basically reg.x */
/* i.e. SharedLineThree usually is the 3rd entry it's lineIndex is 31. The line
   number sent should be 3 but we recieve data from polycom by index.  It must be 
   converted prior to the call to this function.

   Line_3_Reg_Name        = SharedLineThree
   Line_3_Dial_Out        = false
   Line_3_Shared          = true
   Line_3_Index           = 31
*/
 string strLineNumber = int2str(Line_number);
 string strError      = "cti.cpp DialTelephoneNumberXMLstring() Out of range -> "+strLineNumber;
 if ((Line_number < 1 )||(Line_number > 48)) {SendCodingError( strError );}
 // No longer LineIndex now linenumber !!!
 string strFirst  = "<PolycomIPPhone><Data priority=\"Critical\"> tel://";
 string strLine   = ";Line" + strLineNumber;
 string strLast   = "\r\n</Data></PolycomIPPhone>";

 return strFirst + strTelephoneNumber + strLine + strLast;
}

string PostHttpAddressFromWorkstation(int i)
{
 int                                            index;
 string                                         strOutput = "http://";
 extern Telephone_Devices                       TelephoneEquipment;
 
 index = TelephoneEquipment.fIndexWithPositionNumber(i);

 if (index < 0)               {return "";}
 return strOutput + TelephoneEquipment.Devices[index].IPaddress.stringAddress + "/push";
}



string ChannelNameInConferenceList(int iWorkstation, Conf_Data objConfData)
{

 //cout << objConfData.strActiveConferencePositions << endl;
 //cout << objConfData.strOnHoldPositions << endl;


 return "";
}
int FindSharedLineNumberofCall(int iWorkstation, Call_Data objCallData)
{
 int                                            intRC;
 int                                            index;
 int                                            iTableIndex = -1;
 Telephone_Device                               Telephone;
 vector <ExperientDataClass>::size_type         sz;
 bool                                           boolVectorsEmpty;
 string                                         strSharedLineName;

 extern vector <ExperientDataClass>             vMainWorkTable;
 extern Telephone_Devices                       TelephoneEquipment;

 index = TelephoneEquipment.fIndexWithPositionNumber(iWorkstation);
 if (index < 0) {return 0;} 

 strSharedLineName = ChannelNameInConferenceList(iWorkstation,objCallData.ConfData);



 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in FindSharedLineNumberofCall()", 1);}
 
 // find call position is on ....
 sz = vMainWorkTable.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if ((vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF, iWorkstation)) && (!vMainWorkTable[i].CallData.fIsOnHold(iWorkstation))) { iTableIndex = i; break;}
  }

 if (iTableIndex < 0) {sem_post(&sem_tMutexMainWorkTable); return 0;}

 ////cout << "Table Index of call" << iTableIndex << endl;

 sem_post(&sem_tMutexMainWorkTable);
 return 0;
}




