/*****************************************************************************
* FILE: initailizationclass.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the Initialization Class 
*
*
*
* AUTHOR: 01/29/2012 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"
#include "/datadisk1/src/simpleini/SimpleIni.h"          // ini software
#include "/datadisk1/src/xmlParser/xmlParser.h"
#include <algorithm>

extern Text_Data                                objBLANK_TEXT_DATA;
extern Call_Data                                objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Trunk_Type_Mapping                       TRUNK_TYPE_MAP;
extern Port_Data                                objBLANK_ANI_PORT;
extern Port_Data                                objBLANK_ALI_PORT;
extern Port_Data                                objBLANK_WRK_PORT;
extern Port_Data                                objBLANK_AMI_PORT;
extern Port_Data                                objBLANK_KRN_PORT;
extern Port_Data                                objBLANK_DSB_PORT;
extern Port_Data                                objBLANK_CAD_PORT;

//Constructor
ANI_Variables::ANI_Variables() {
// intANI_PORT_DOWN_THRESHOLD_SEC		= 180;
// intANI_PORT_DOWN_REMINDER_SEC		= 1800;  
 intNUM_TRUNKS_INSTALLED		= 0;
 intDELAY_AFTER_FLASH_HOOK_MS		= 0;
 intDELAY_BETWEEN_DTMF_MS		= 0;
 intDelay_AFTER_STAR_EIGHT_PD_MS	= 0;
 intDelay_BETWEEN_DTMF_INDIGITAL_MS	= 0;
 intNUM_ANI_PORTS			= 0;
/*
 strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY.clear();
 strASTERISK_AMI_POSITION_HEADER_911.clear();
 strASTERISK_AMI_POSITION_HEADER_FIRE.clear();
*/
 strCONFERENCE_NUMBER_DIAL_PREFIX.clear();
// strASTERISK_AMI_IP_ADDRESS.clear();
 strANI_16_DIGIT_REQ_TEST_KEY.clear();
 strANI_14_DIGIT_REQ_TEST_KEY.clear();
 strGATEWAY_TRANSFER_ORDER.clear();

 bool_BCF_ENCODED_IP			= false;
 boolAUDIOCODES_911_ANI_REVERSED	= false;
 boolANI_20_DIGIT_VALID_CALLBACK	= false; 
 boolAMI_RECORD_SCRIPT			= false;
 boolAMI_TEST_SCRIPT			= false;
 boolTDD_AUTO_DETECT			= false;
 
 boolRestartFreeswitchCLIport		= false;
 boolRestartANIports                   	= false;
 intOldNumberOfANIports			= 0;

 vGATEWAY_TRANSFER_ORDER.clear();
 NPD.clear();
 enumANI_SYSTEM				= NO_SYSTEM;
 for (int i=0; i <= NUM_ANI_PORTS_MAX; i++) {
  ANI_PORT_VARS[i].fClear();
 } 
 //Trunk_Type_MappingTRUNK_TYPE_MAP;
 //Trunk_Sequencing       Trunk_Map;
 //ExperientCommPort      ANIPort[NUM_ANI_PORTS_MAX+1];
 ini.Reset();
}
//copy constructor
ANI_Variables::ANI_Variables(const ANI_Variables *a) {
// intANI_PORT_DOWN_THRESHOLD_SEC		= a->intANI_PORT_DOWN_THRESHOLD_SEC;
// intANI_PORT_DOWN_REMINDER_SEC		= a->intANI_PORT_DOWN_REMINDER_SEC;  
 intNUM_TRUNKS_INSTALLED		= a->intNUM_TRUNKS_INSTALLED;
 intDELAY_AFTER_FLASH_HOOK_MS		= a->intDELAY_AFTER_FLASH_HOOK_MS;
 intDELAY_BETWEEN_DTMF_MS		= a->intDELAY_BETWEEN_DTMF_MS;
 intDelay_AFTER_STAR_EIGHT_PD_MS	= a->intDelay_AFTER_STAR_EIGHT_PD_MS;
 intDelay_BETWEEN_DTMF_INDIGITAL_MS	= a->intDelay_BETWEEN_DTMF_INDIGITAL_MS;
 intNUM_ANI_PORTS			= a->intNUM_ANI_PORTS;
 
/*
 strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY	= a->strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY;
 strASTERISK_AMI_POSITION_HEADER_911		= a->strASTERISK_AMI_POSITION_HEADER_911;
 strASTERISK_AMI_POSITION_HEADER_FIRE		= a->strASTERISK_AMI_POSITION_HEADER_FIRE;
*/
 strCONFERENCE_NUMBER_DIAL_PREFIX		= a->strCONFERENCE_NUMBER_DIAL_PREFIX;
 //strASTERISK_AMI_IP_ADDRESS			= a->strASTERISK_AMI_IP_ADDRESS;
 strANI_16_DIGIT_REQ_TEST_KEY			= a->strANI_16_DIGIT_REQ_TEST_KEY;
 strANI_14_DIGIT_REQ_TEST_KEY			= a->strANI_14_DIGIT_REQ_TEST_KEY;
 strGATEWAY_TRANSFER_ORDER			= a->strGATEWAY_TRANSFER_ORDER;

 bool_BCF_ENCODED_IP			= a->bool_BCF_ENCODED_IP;
 boolAUDIOCODES_911_ANI_REVERSED	= a->boolAUDIOCODES_911_ANI_REVERSED;
 boolANI_20_DIGIT_VALID_CALLBACK	= a->boolANI_20_DIGIT_VALID_CALLBACK; 
 boolAMI_RECORD_SCRIPT			= a->boolAMI_RECORD_SCRIPT;
 boolAMI_TEST_SCRIPT			= a->boolAMI_TEST_SCRIPT;
 boolTDD_AUTO_DETECT			= a->boolTDD_AUTO_DETECT;
 
 boolRestartFreeswitchCLIport		= a->boolRestartFreeswitchCLIport;
 boolRestartANIports                   	= a->boolRestartANIports;
 intOldNumberOfANIports			= a->intOldNumberOfANIports;

 vGATEWAY_TRANSFER_ORDER		= a->vGATEWAY_TRANSFER_ORDER;
 NPD					= a->NPD;
 enumANI_SYSTEM				= a->enumANI_SYSTEM;
 for (int i=0; i <= NUM_ANI_PORTS_MAX; i++) {
  ANI_PORT_VARS[i] = a->ANI_PORT_VARS[i];
 }  
 TRUNK_TYPE_MAP				= a->TRUNK_TYPE_MAP;
 Trunk_Map				= a->Trunk_Map;

/* make a class var
 for (unsigned int i = 0; i <= NUM_ANI_PORTS_MAX; i++){
  ANIPort[i]			        = a->ANIPort[i];
 } 
*/

// copy constructor for CSimpleIniA is private will need to do alternate method to move or "reconstruct" 
// for now we only use this variable by reading in the file every time we wish to do an operation
// ini					= a->ini;

}
//assignment operator
ANI_Variables& ANI_Variables::operator=(const ANI_Variables& a)
{
 if (&a == this ) {return *this;}
// intANI_PORT_DOWN_THRESHOLD_SEC		= a.intANI_PORT_DOWN_THRESHOLD_SEC;
// intANI_PORT_DOWN_REMINDER_SEC		= a.intANI_PORT_DOWN_REMINDER_SEC;  
 intNUM_TRUNKS_INSTALLED		= a.intNUM_TRUNKS_INSTALLED;
 intDELAY_AFTER_FLASH_HOOK_MS		= a.intDELAY_AFTER_FLASH_HOOK_MS;
 intDELAY_BETWEEN_DTMF_MS		= a.intDELAY_BETWEEN_DTMF_MS;
 intDelay_AFTER_STAR_EIGHT_PD_MS	= a.intDelay_AFTER_STAR_EIGHT_PD_MS;
 intDelay_BETWEEN_DTMF_INDIGITAL_MS	= a.intDelay_BETWEEN_DTMF_INDIGITAL_MS;
 intNUM_ANI_PORTS			= a.intNUM_ANI_PORTS;
/*
 strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY	= a.strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY;
 strASTERISK_AMI_POSITION_HEADER_911		= a.strASTERISK_AMI_POSITION_HEADER_911;
 strASTERISK_AMI_POSITION_HEADER_FIRE		= a.strASTERISK_AMI_POSITION_HEADER_FIRE;
*/
 strCONFERENCE_NUMBER_DIAL_PREFIX		= a.strCONFERENCE_NUMBER_DIAL_PREFIX;
// strASTERISK_AMI_IP_ADDRESS			= a.strASTERISK_AMI_IP_ADDRESS;
 strANI_16_DIGIT_REQ_TEST_KEY			= a.strANI_16_DIGIT_REQ_TEST_KEY;
 strANI_14_DIGIT_REQ_TEST_KEY			= a.strANI_14_DIGIT_REQ_TEST_KEY;
 strGATEWAY_TRANSFER_ORDER			= a.strGATEWAY_TRANSFER_ORDER;

 bool_BCF_ENCODED_IP			= a.bool_BCF_ENCODED_IP;
 boolAUDIOCODES_911_ANI_REVERSED	= a.boolAUDIOCODES_911_ANI_REVERSED;
 boolANI_20_DIGIT_VALID_CALLBACK	= a.boolANI_20_DIGIT_VALID_CALLBACK; 
 boolAMI_RECORD_SCRIPT			= a.boolAMI_RECORD_SCRIPT;
 boolAMI_TEST_SCRIPT			= a.boolAMI_TEST_SCRIPT;
 boolTDD_AUTO_DETECT			= a.boolTDD_AUTO_DETECT;
 
 boolRestartFreeswitchCLIport		= a.boolRestartFreeswitchCLIport;
 boolRestartANIports                   	= a.boolRestartANIports;
 intOldNumberOfANIports			= a.intOldNumberOfANIports;  


 vGATEWAY_TRANSFER_ORDER		= a.vGATEWAY_TRANSFER_ORDER;
 NPD					= a.NPD;
 enumANI_SYSTEM				= a.enumANI_SYSTEM;
 for (int i=0; i <= NUM_ANI_PORTS_MAX; i++) {
  ANI_PORT_VARS[i] = a.ANI_PORT_VARS[i];
 }  
 TRUNK_TYPE_MAP				= a.TRUNK_TYPE_MAP;
 Trunk_Map				= a.Trunk_Map;

/*
 //need to revisit ! make a class variable .....
 for (unsigned int i = 0; i <= NUM_ANI_PORTS_MAX; i++){
  ANIPort[i]			        = a.ANIPort[i];
 } 
*/
 return *this;
}
//Comparison
bool ANI_Variables::fCompare(const ANI_Variables& a) const {
 
 bool boolPortcheck = true;
 bool boolRunningCheck = true;

 bool boolIPaddressSame     = (this->ANI_PORT_VARS[1].REMOTE_IP_ADDRESS.stringAddress   == a.ANI_PORT_VARS[1].REMOTE_IP_ADDRESS.stringAddress);
 bool boolNumberofPortsSame = (this->intNUM_ANI_PORTS                                   == a.intNUM_ANI_PORTS);
 bool boolCLIPortSame       = (FREESWITCH_CLI_PORT_NUMBER                               == FREESWITCH_CLI_PORT_NUMBER); // will change later

 // boolRunningCheck = boolRunningCheck && (this->intANI_PORT_DOWN_THRESHOLD_SEC 			== a.intANI_PORT_DOWN_THRESHOLD_SEC);
//  if(!boolRunningCheck) {cout << "a" << endl;}  
//  boolRunningCheck = boolRunningCheck && (this->intANI_PORT_DOWN_REMINDER_SEC 			== a.intANI_PORT_DOWN_REMINDER_SEC);;
  

  boolRunningCheck = boolRunningCheck && (this->intNUM_TRUNKS_INSTALLED             == a.intNUM_TRUNKS_INSTALLED);
  boolRunningCheck = boolRunningCheck && (this->intDELAY_AFTER_FLASH_HOOK_MS        == a.intDELAY_AFTER_FLASH_HOOK_MS); 
  boolRunningCheck = boolRunningCheck && (this->intDELAY_BETWEEN_DTMF_MS            == a.intDELAY_BETWEEN_DTMF_MS); 
  boolRunningCheck = boolRunningCheck && (this->intDelay_AFTER_STAR_EIGHT_PD_MS     == a.intDelay_AFTER_STAR_EIGHT_PD_MS);
  boolRunningCheck = boolRunningCheck && (this->intDelay_BETWEEN_DTMF_INDIGITAL_MS  == a.intDelay_BETWEEN_DTMF_INDIGITAL_MS);  

// check restart of ANI ports
// will need to revisit when we add multiple ports ... this would be a loop ...
  boolRunningCheck = boolRunningCheck && (this->ANI_PORT_VARS[1].fCompare(a.ANI_PORT_VARS[1])); 

  this->boolRestartFreeswitchCLIport = ((!boolIPaddressSame)||(!boolCLIPortSame));
  
 // this->boolRestartANIports          = ((!boolNumberofPortsSame)||(!boolIPaddressSame)||(!boolPortcheck)); // we do not restart ANI ports
  this->boolRestartANIports          = false;
  a.boolRestartANIports              = this->boolRestartANIports;
  a.boolRestartFreeswitchCLIport     = this->boolRestartFreeswitchCLIport;
  this->intOldNumberOfANIports       = this->intNUM_ANI_PORTS;
  a.intOldNumberOfANIports           = this->intNUM_ANI_PORTS;
  
  boolRunningCheck = boolRunningCheck && boolNumberofPortsSame;

//.....................................................................
  boolRunningCheck = boolRunningCheck && (this->strCONFERENCE_NUMBER_DIAL_PREFIX    == a.strCONFERENCE_NUMBER_DIAL_PREFIX);
  boolRunningCheck = boolRunningCheck && boolIPaddressSame; 
  boolRunningCheck = boolRunningCheck && (this->strANI_16_DIGIT_REQ_TEST_KEY        == a.strANI_16_DIGIT_REQ_TEST_KEY);
  boolRunningCheck = boolRunningCheck && (this->strANI_14_DIGIT_REQ_TEST_KEY        == a.strANI_14_DIGIT_REQ_TEST_KEY);
  boolRunningCheck = boolRunningCheck && (this->strGATEWAY_TRANSFER_ORDER           == a.strGATEWAY_TRANSFER_ORDER);
  boolRunningCheck = boolRunningCheck && (this->bool_BCF_ENCODED_IP                 == a.bool_BCF_ENCODED_IP);
  boolRunningCheck = boolRunningCheck && (this->boolAUDIOCODES_911_ANI_REVERSED     == a.boolAUDIOCODES_911_ANI_REVERSED);
  boolRunningCheck = boolRunningCheck && (this->boolANI_20_DIGIT_VALID_CALLBACK     == a.boolANI_20_DIGIT_VALID_CALLBACK);
  boolRunningCheck = boolRunningCheck && (this->boolAMI_RECORD_SCRIPT               == a.boolAMI_RECORD_SCRIPT);
  boolRunningCheck = boolRunningCheck && (this->boolAMI_TEST_SCRIPT                 == a.boolAMI_TEST_SCRIPT); 
  boolRunningCheck = boolRunningCheck && (this->boolTDD_AUTO_DETECT                 == a.boolTDD_AUTO_DETECT); 
  boolRunningCheck = boolRunningCheck && (this->vGATEWAY_TRANSFER_ORDER             == a.vGATEWAY_TRANSFER_ORDER);
  boolRunningCheck = boolRunningCheck && (this->NPD                                 == a.NPD);
  boolRunningCheck = boolRunningCheck && (this->enumANI_SYSTEM                      == a.enumANI_SYSTEM);
  boolRunningCheck = boolRunningCheck && (this->TRUNK_TYPE_MAP                      == a.TRUNK_TYPE_MAP);
  boolRunningCheck = boolRunningCheck && (this->Trunk_Map                           == a.Trunk_Map);

 return boolRunningCheck;
}


void ANI_Variables::Set_GLobal_VARS()
{
 extern int         intANI_PORT_DOWN_THRESHOLD_SEC;
 extern int         intANI_PORT_DOWN_REMINDER_SEC;  
 extern int         intNUM_TRUNKS_INSTALLED;
 extern int         intDELAY_AFTER_FLASH_HOOK_MS;
 extern int         intDELAY_BETWEEN_DTMF_MS;
 extern int         intDelay_AFTER_STAR_EIGHT_PD_MS;
 extern int         intDelay_BETWEEN_DTMF_INDIGITAL_MS;
 extern int         intNUM_ANI_PORTS;
// extern int       intANI_HEARTBEAT_INTERVAL_SEC; this is hard coded ...

 extern string      strCONFERENCE_NUMBER_DIAL_PREFIX;
 extern string      strASTERISK_AMI_IP_ADDRESS;
 extern string      strANI_16_DIGIT_REQ_TEST_KEY;
 extern string      strANI_14_DIGIT_REQ_TEST_KEY;
 extern string      strGATEWAY_TRANSFER_ORDER;

 extern bool        bool_BCF_ENCODED_IP;
 extern bool        boolAUDIOCODES_911_ANI_REVERSED;
 extern bool        boolANI_20_DIGIT_VALID_CALLBACK; 
 extern bool        boolAMI_RECORD_SCRIPT;
 extern bool        boolAMI_TEST_SCRIPT;
 extern bool        boolTDD_AUTO_DETECT;
 
 extern vector <string>         vGATEWAY_TRANSFER_ORDER;
 extern vector <string>         NPD;
 extern ani_system              enumANI_SYSTEM;
 extern Trunk_Type_Mapping      TRUNK_TYPE_MAP;
 extern Trunk_Sequencing        Trunk_Map;
 extern ExperientCommPort       ANIPort[NUM_ANI_PORTS_MAX+1];
 //CSimpleIniA            ini;
 
 intANI_PORT_DOWN_THRESHOLD_SEC     = this->ANI_PORT_VARS[1].intPORT_DOWN_THRESHOLD_SEC;
 intANI_PORT_DOWN_REMINDER_SEC      = this->ANI_PORT_VARS[1].intPORT_DOWN_REMINDER_SEC;
// intANI_HEARTBEAT_INTERVAL_SEC    = this->ANI_PORT_VARS[1].intHEARTBEAT_INTERVAL_SEC; hard coded ....
 intNUM_TRUNKS_INSTALLED            = this->intNUM_TRUNKS_INSTALLED;
 intDELAY_AFTER_FLASH_HOOK_MS       = this->intDELAY_AFTER_FLASH_HOOK_MS; 
 intDELAY_BETWEEN_DTMF_MS           = this->intDELAY_BETWEEN_DTMF_MS;
 intDelay_AFTER_STAR_EIGHT_PD_MS    = this->intDelay_AFTER_STAR_EIGHT_PD_MS;
 intDelay_BETWEEN_DTMF_INDIGITAL_MS = this->intDelay_BETWEEN_DTMF_INDIGITAL_MS;
 intNUM_ANI_PORTS                   = this->intNUM_ANI_PORTS;
 strCONFERENCE_NUMBER_DIAL_PREFIX   = this->strCONFERENCE_NUMBER_DIAL_PREFIX;
 strASTERISK_AMI_IP_ADDRESS         = this->ANI_PORT_VARS[1].REMOTE_IP_ADDRESS.stringAddress;
 strANI_16_DIGIT_REQ_TEST_KEY       = this->strANI_16_DIGIT_REQ_TEST_KEY;
 strANI_14_DIGIT_REQ_TEST_KEY       = this->strANI_14_DIGIT_REQ_TEST_KEY;
 strGATEWAY_TRANSFER_ORDER          = this->strGATEWAY_TRANSFER_ORDER;

 bool_BCF_ENCODED_IP                = this->bool_BCF_ENCODED_IP;
 boolAUDIOCODES_911_ANI_REVERSED    = this->boolAUDIOCODES_911_ANI_REVERSED;
 boolANI_20_DIGIT_VALID_CALLBACK    = this->boolANI_20_DIGIT_VALID_CALLBACK;
 boolAMI_RECORD_SCRIPT              = this->boolAMI_RECORD_SCRIPT;
 boolAMI_TEST_SCRIPT                = this->boolAMI_TEST_SCRIPT;
 boolTDD_AUTO_DETECT                = this->boolTDD_AUTO_DETECT;

 vGATEWAY_TRANSFER_ORDER            = this->vGATEWAY_TRANSFER_ORDER;

 NPD                                = this->NPD;

 enumANI_SYSTEM	                    = this->enumANI_SYSTEM;

 TRUNK_TYPE_MAP	                    = this->TRUNK_TYPE_MAP;

 Trunk_Map                          = this->Trunk_Map;

/*
 THIS HAPPENS IN INITIALIZE ANI PORTS FUNCTION CALL ..................
 // need to revisit !!!!!!!
 need to have a class variable to initialize the port ............ done ...
 for (unsigned int i = 1; i <=  NUM_ANI_PORTS_MAX; i++) 
  {
   ANIPort[i].RemoteIPAddress 		= this->ANIPort[i].RemoteIPAddress;
   ANIPort[i].intRemotePortNumber 	= this->ANIPort[i].intRemotePortNumber;
   ANIPort[i].strNotes 			= this->ANIPort[i].strNotes;
   ANIPort[i].boolPhantomPort 		= this->ANIPort[i].boolPhantomPort;

  }
*/
 return;
}

bool ANI_Variables::fLoadINIfile()
{
 string 	strMessage="ANI_Variables::fLoadINIfile() -Unable to load ini file -> ";
 strMessage +=  charINI_FILE_PATH_AND_NAME;
 
 this->ini.SetUnicode(true);
 this->ini.SetMultiKey(true);
 this->ini.SetMultiLine(true);

 SI_Error rc = this->ini.LoadFile(charINI_FILE_PATH_AND_NAME);
 if (rc < 0) { SendCodingError(strMessage); return false;}
 return true;
}


bool ANI_Variables::fLoadANIports() {
  // may need to revisit ....
  // taken care of in function bool InitializeANIports() could be possibly moved here .......
  return true;
}
bool ANI_Variables::fLoadNPDs()
{
 /* note: these values are in the GENERAL Section.  They probably should be in the ALI section.  
          However certain Vars in the ANI section
          are generated from the NPD's : strANI_16_DIGIT_REQ_TEST_KEY
 */

 int 	i;
 string StringData;
 string Ali_Key;

   this->NPD.clear();
   for (int x = 0; x < 8 ; x++){this->NPD.push_back("000");}

    i = 0;
    do
     {
      if (i == 0) {Ali_Key = "NPD0";}
      else        {Ali_Key = Create_INI_Key("NPD", i);}
      StringData = this->ini.GetValue("GENERAL", Ali_Key.c_str(), "" );
      if (StringData  == "") {continue;}
      if (i > 7 ){return false;}
      this->NPD[i] = StringData; 
      i++;
      
     }while ( StringData != "");   

 return true;
}
bool ANI_Variables::fLoadFreeswitchIPaddress()
{
 /* The number of ANI ports will be calculated by the number of FreeSwitch IP addresses.  Currently we are only using 1.
 */

 string 	StringData;
 char        	NumericAddress[32];

   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_16, DEFAULT_VALUE_ANI_INI_KEY_16 );
   if      (StringData == DEFAULT_VALUE_ANI_INI_KEY_16)  {this->ANI_PORT_VARS[1].REMOTE_IP_ADDRESS.stringAddress = DEFAULT_VALUE_ANI_INI_KEY_16;}
   else    if ( inet_pton( AF_INET , (const char *)StringData.c_str() , NumericAddress) > 0) { this->ANI_PORT_VARS[1].REMOTE_IP_ADDRESS.stringAddress = StringData;}
   else    { return false;}

 this->intNUM_ANI_PORTS 				= 1;
 this->ANI_PORT_VARS[1].intPORT_NUMBER 			= 1;
 this->ANI_PORT_VARS[1].intHEARTBEAT_INTERVAL_SEC 	= intANI_HEARTBEAT_INTERVAL_SEC;
 this->ANI_PORT_VARS[1].ePortConnectionType 		= UDP;
 this->ANI_PORT_VARS[1].eALIportProtocol    		= NO_ALI_PROTO;
 return true;
}
 
bool ANI_Variables::fLoadANIsystem()
{
 // Most likely obsolete .......
 this->enumANI_SYSTEM = ASTERISK; return true; 
}
bool ANI_Variables::fLoad20digitCallback()
{
 return char2bool(this->boolANI_20_DIGIT_VALID_CALLBACK, this->ini.GetValue("ANI", ANI_INI_KEY_6, DEFAULT_VALUE_ANI_INI_KEY_6 ));
}
bool ANI_Variables::fLoadRecordAMI()
{
 this->boolAMI_RECORD_SCRIPT  = true;
 return true;
}
bool ANI_Variables::fLoadTestScript()
{
 return char2bool(this->boolAMI_TEST_SCRIPT, this->ini.GetValue("ANI", ANI_INI_KEY_21, DEFAULT_VALUE_ANI_INI_KEY_21));
}
bool ANI_Variables::fLoadTDDautoDetect()
{
 return char2bool(this->boolTDD_AUTO_DETECT, this->ini.GetValue("ANI", ANI_INI_KEY_3, DEFAULT_VALUE_ANI_INI_KEY_3));
}
bool ANI_Variables::fLoadBCFencodedIP()
{
 return char2bool(this->bool_BCF_ENCODED_IP, this->ini.GetValue("ANI", ANI_INI_KEY_22, DEFAULT_VALUE_ANI_INI_KEY_22 ));
}
bool ANI_Variables::fLoadAudiocodes911ANIreversed()
{
 return char2bool(this->boolAUDIOCODES_911_ANI_REVERSED, this->ini.GetValue("ANI", ANI_INI_KEY_23, DEFAULT_VALUE_ANI_INI_KEY_23 ));
}

bool ANI_Variables::fLoad16DigitTestKey()
{
 // fLoadNPDs() must be called first !
 this->fLoadNPDs();
 this->strANI_16_DIGIT_REQ_TEST_KEY = this->NPD[0] + strSEVEN_DIGIT_ALI_BID_TEST_KEY;
 return true;
} 
bool ANI_Variables::fLoad14DigitTestKey()
{
 this->strANI_14_DIGIT_REQ_TEST_KEY = "0";   
 this->strANI_14_DIGIT_REQ_TEST_KEY += strSEVEN_DIGIT_ALI_BID_TEST_KEY;
 return true;
} 
bool ANI_Variables::fLoadDelayAfterFlashHook()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_17, DEFAULT_VALUE_ANI_INI_KEY_17 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intDELAY_AFTER_FLASH_HOOK_MS = char2int(strNumber.c_str());
 return true;
} 

bool ANI_Variables::fLoadDelayBetweenDTMF()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_18, DEFAULT_VALUE_ANI_INI_KEY_18 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intDELAY_BETWEEN_DTMF_MS = char2int(strNumber.c_str());
 return true;
}
 
bool ANI_Variables::fLoadDelayAfterStar8()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_26, DEFAULT_VALUE_ANI_INI_KEY_26 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intDelay_AFTER_STAR_EIGHT_PD_MS = char2int(strNumber.c_str());
 return true;
} 

bool ANI_Variables::fLoadDelayBetweenInDigitalDTMF()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_27, DEFAULT_VALUE_ANI_INI_KEY_27 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intDelay_BETWEEN_DTMF_INDIGITAL_MS = char2int(strNumber.c_str());
 return true;
} 

bool ANI_Variables::fLoadTrunkTypeMap()
{
 bool 	boolLoadSuccess = true;
 string StringData;
 bool   boolRunningCheck = true;

   // Load Landline trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_7 , DEFAULT_VALUE_ANI_INI_KEY_7);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_7) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(LANDLINE, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading Landline Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load Wireless trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_8 , DEFAULT_VALUE_ANI_INI_KEY_8);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_8) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(WIRELESS, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading Wireless Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load CLID trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_5 , DEFAULT_VALUE_ANI_INI_KEY_5);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_5) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(CLID, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading CLID Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load SIP trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_10 , DEFAULT_VALUE_ANI_INI_KEY_10);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_10) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(SIP_TRUNK, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading SIP Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load NG911 SIP trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_12 , DEFAULT_VALUE_ANI_INI_KEY_12);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_12) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(NG911_SIP, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading NG911 SIP Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load CAMA trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_11 , DEFAULT_VALUE_ANI_INI_KEY_11);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_11) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(CAMA, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading CAMA Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load NG911 Refer trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_13 , DEFAULT_VALUE_ANI_INI_KEY_13);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_13) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(REFER, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading REFER Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load InDigital Wireless trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_25 , DEFAULT_VALUE_ANI_INI_KEY_25);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_25) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(INDIGITAL_WIRELESS, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading InDigital Wireless Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load MSRP trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_28 , DEFAULT_VALUE_ANI_INI_KEY_28);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_28) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(MSRP_TRUNK, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading MSRP Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;
   // Load INTRADO trunk table
   StringData = this->ini.GetValue("ANI", ANI_INI_KEY_29 , DEFAULT_VALUE_ANI_INI_KEY_29);
   if (StringData != DEFAULT_VALUE_ANI_INI_KEY_29) {boolLoadSuccess=this->TRUNK_TYPE_MAP.fLoadTrunkMapType(INTRADO, StringData);}
   if (!boolLoadSuccess)                          {SendCodingError("initialization.cpp: ANI_Variables::fLoadTrunkTypeMap() error loading INTRADO Trunk");}
   boolRunningCheck = boolRunningCheck && boolLoadSuccess;

return boolRunningCheck;
}
bool ANI_Variables::fLoadTrunkMap()   
{
 string strMessage = "initialization.cpp: ANI_Variables::fLoadRotateTrunkGroups() error loading ";
 string strSuffix;
   //Load Rotate Trunk Groups
 
   CSimpleIniA::TNamesDepend Trunk_Rotate_Members_Values, Trunk_Rotate_Rule_Values , Trunk_Rotate_Value_Values;
   CSimpleIniA::TNamesDepend::const_iterator l,m,n;

   this->ini.GetAllValues("ANI", "Trunk_Rotate_Group_Members", Trunk_Rotate_Members_Values);
   this->ini.GetAllValues("ANI", "Trunk_Rotate_Group_Rule", Trunk_Rotate_Rule_Values);
   this->ini.GetAllValues("ANI", "Trunk_Rotate_Group_Rule_Value", Trunk_Rotate_Value_Values);

   Trunk_Rotate_Group objRotateGroup;

   Trunk_Rotate_Members_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Trunk_Rotate_Rule_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Trunk_Rotate_Value_Values.sort(CSimpleIniA::Entry::LoadOrder());

   l = Trunk_Rotate_Members_Values.begin();
   m = Trunk_Rotate_Rule_Values.begin();
   n = Trunk_Rotate_Value_Values.begin();
  
   //push group zero as empty
   Trunk_Map.TrunkRotateGroup.push_back(objRotateGroup); 
   while ((l != Trunk_Rotate_Members_Values.end())||(m != Trunk_Rotate_Rule_Values.end())||(n != Trunk_Rotate_Value_Values.end()) )
    { 
     if(!objRotateGroup.fLoadMembers(l->pItem))      {strSuffix = " member ";     strSuffix += l->pItem; SendCodingError(strMessage+strSuffix); return false;}
     if(!objRotateGroup.fLoadRule(m->pItem))         {strSuffix = " rule ";       strSuffix += m->pItem; SendCodingError(strMessage+strSuffix); return false;}
     if(!objRotateGroup.fLoadRuleValue(n->pItem))    {strSuffix = " rule value "; strSuffix += n->pItem; SendCodingError(strMessage+strSuffix); return false;}
     this->Trunk_Map.TrunkRotateGroup.push_back(objRotateGroup); 
     objRotateGroup.fClear();
     ++l; ++m; ++n;
    }

  //intitalize rotate 911 trunk map
   this->Trunk_Map.TrunkMap = this->TRUNK_TYPE_MAP;
   this->Trunk_Map.fSetNumber911trunks(); 
   this->Trunk_Map.fLoadTrunkMapRotateGroups();

 return true;
}
bool ANI_Variables::Load_Gateway_Order()
{
 size_t         position = 0;
 string         stringTemp;
 string         strGateway;

 this->vGATEWAY_TRANSFER_ORDER.clear();

 this->strGATEWAY_TRANSFER_ORDER = this->ini.GetValue("ANI", ANI_INI_KEY_19 , DEFAULT_VALUE_ANI_INI_KEY_19);

 stringTemp = this->strGATEWAY_TRANSFER_ORDER;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   strGateway.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   strGateway = RemoveLeadingSpaces(strGateway);
   strGateway = RemoveTrailingSpaces(strGateway);

   this->vGATEWAY_TRANSFER_ORDER.push_back(strGateway);

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 strGateway.assign(stringTemp,0 ,string::npos);
 strGateway = RemoveLeadingSpaces(strGateway);
 strGateway = RemoveTrailingSpaces(strGateway);

 if (strGateway.empty()) {return false;}

 this->vGATEWAY_TRANSFER_ORDER.push_back(strGateway);

 return true;
}

bool ANI_Variables::fLoad_CC_Database_Email_addresses()
{
 string StringData;

   StringData = this->ini.GetValue("ANI", KRN_INI_KEY_13, DEFAULT_VALUE_KRN_INI_KEY_13);
   // load email address into all the ports or set to false
   for (int x = 1; x <= this->intNUM_ANI_PORTS; x++){this->ANI_PORT_VARS[x].strVendorEmailAddress = StringData;}  

 return true;
}

bool ANI_Variables::fLoad_CC_Port_Email_addresses()
{
  string StringData;
  string ANI_Key;

   for (int x = 1; x <= intNUM_ANI_PORTS; x++)
    {
     ANI_Key = Create_INI_Key("Port", x, "CC_Alarm_Email_Address");
     StringData = this->ini.GetValue("ANI", ANI_Key.c_str(), DEFAULT_VALUE_KRN_INI_KEY_13 );
     if (StringData  == DEFAULT_VALUE_KRN_INI_KEY_13) {continue;}
     this->ANI_PORT_VARS[x].strVendorEmailAddress = StringData;   
    }

 return true;
}

bool ANI_Variables::fLoadPortDownThreshold()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_1, DEFAULT_VALUE_ANI_INI_KEY_1 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->ANI_PORT_VARS[1].intPORT_DOWN_THRESHOLD_SEC = char2int(strNumber.c_str());
 return true;
}

bool ANI_Variables::fLoadPortDownReminder()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_2, DEFAULT_VALUE_ANI_INI_KEY_2 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->ANI_PORT_VARS[1].intPORT_DOWN_REMINDER_SEC = char2int(strNumber.c_str());
 return true;
}

bool ANI_Variables::fLoadTrunksInstalled()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_4, DEFAULT_VALUE_ANI_INI_KEY_4 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intNUM_TRUNKS_INSTALLED = char2int(strNumber.c_str());
 return true;
}
bool ANI_Variables::fLoadConferenceNumberDialPrefix()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("ANI", ANI_INI_KEY_20, DEFAULT_VALUE_ANI_INI_KEY_20 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->strCONFERENCE_NUMBER_DIAL_PREFIX = strNumber;
 return true;
}
 

bool Initialize_Global_Variables::fLoadANIvariables(ANI_Variables &ANIvar)
{
  string strErrorString = "Initialize_Global_Variables::fLoadANIvariables() Unable to load ";
  bool   boolReturnValue = true;

  // Get ALI INI Variables
   if (!ANIvar.fLoadINIfile()) {
    strErrorString = "Initialize_Global_Variables::fLoadANIvariables() Unable to load file: ";
    strErrorString += charINI_FILE_PATH_AND_NAME;
    SendCodingError( strErrorString );
    return false;
   }

   if (!ANIvar.fLoadNPDs())         			{strErrorString += "NPD Vector "; 				boolReturnValue = false;} 
   if (!ANIvar.fLoadFreeswitchIPaddress())         	{strErrorString += "strASTERISK_AMI_IP_ADDRESS "; 		boolReturnValue = false;} 
   if (!ANIvar.fLoadANIsystem()) 	        	{strErrorString += "enumANI_SYSTEM "; 				boolReturnValue = false;} 
   if (!ANIvar.fLoad20digitCallback()) 	        	{strErrorString += "boolANI_20_DIGIT_VALID_CALLBACK "; 		boolReturnValue = false;} 
   if (!ANIvar.fLoadRecordAMI()) 	        	{strErrorString += "boolAMI_RECORD_SCRIPT "; 			boolReturnValue = false;} 
   if (!ANIvar.fLoadTestScript()) 	        	{strErrorString += "boolAMI_TEST_SCRIPT "; 			boolReturnValue = false;} 
   if (!ANIvar.fLoadTDDautoDetect()) 	        	{strErrorString += "boolTDD_AUTO_DETECT "; 			boolReturnValue = false;} 
   if (!ANIvar.fLoad16DigitTestKey()) 	        	{strErrorString += "strANI_16_DIGIT_REQ_TEST_KEY "; 		boolReturnValue = false;} 
   if (!ANIvar.fLoad14DigitTestKey()) 	        	{strErrorString += "strANI_14_DIGIT_REQ_TEST_KEY "; 		boolReturnValue = false;} 
   if (!ANIvar.fLoadDelayAfterFlashHook())         	{strErrorString += "intDELAY_AFTER_FLASH_HOOK_MS "; 		boolReturnValue = false;} 
   if (!ANIvar.fLoadDelayBetweenDTMF())         	{strErrorString += "intDELAY_BETWEEN_DTMF_MS "; 		boolReturnValue = false;}  
   if (!ANIvar.fLoadDelayAfterStar8()) 	        	{strErrorString += "intDelay_AFTER_STAR_EIGHT_PD_MS "; 		boolReturnValue = false;} 
   if (!ANIvar.fLoadDelayBetweenInDigitalDTMF())       	{strErrorString += "intDelay_BETWEEN_DTMF_INDIGITAL_MS "; 	boolReturnValue = false;} 
   if (!ANIvar.fLoadTrunkTypeMap()) 	        	{strErrorString += "TRUNK_TYPE_MAP "; 				boolReturnValue = false;} 
   if (!ANIvar.fLoadTrunkMap())    		    	{strErrorString += "Trunk_Map "; 				boolReturnValue = false;} 
   if (!ANIvar.Load_Gateway_Order()) 	        	{strErrorString += "vGATEWAY_TRANSFER_ORDER "; 			boolReturnValue = false;} 
   if (!ANIvar.fLoad_CC_Port_Email_addresses())        	{strErrorString += "ANIPort.UDP_Port.strVendorEmailAddress "; 	boolReturnValue = false;} 
   if (!ANIvar.fLoad_CC_Database_Email_addresses())    	{strErrorString += "ANIPort.UDP_Port.strVendorEmailAddress "; 	boolReturnValue = false;} 
   if (!ANIvar.fLoadPortDownThreshold())         	{strErrorString += "intANI_PORT_DOWN_THRESHOLD_SEC "; 		boolReturnValue = false;}  
   if (!ANIvar.fLoadPortDownReminder())         	{strErrorString += "intANI_PORT_DOWN_REMINDER_SEC "; 		boolReturnValue = false;}  
   if (!ANIvar.fLoadTrunksInstalled()) 	        	{strErrorString += "intNUM_TRUNKS_INSTALLED "; 			boolReturnValue = false;} 
   if (!ANIvar.fLoadConferenceNumberDialPrefix())      	{strErrorString += "strCONFERENCE_NUMBER_DIAL_PREFIX "; 	boolReturnValue = false;} 
   if (!ANIvar.fLoadBCFencodedIP())    		  	{strErrorString += "bool_BCF_ENCODED_IP "; 			boolReturnValue = false;} 
   if (!ANIvar.fLoadAudiocodes911ANIreversed())    	{strErrorString += "boolAUDIOCODES_911_ANI_REVERSED "; 		boolReturnValue = false;} 

 if (!boolReturnValue) { SendCodingError( strErrorString );}
 return boolReturnValue;
}

/////// ALI_VARIABLES #######################################################################################################################################################
//Constructor
ALI_Variables::ALI_Variables() {

  boolLEGACY_ALI			= false;
  boolLEGACY_ALI_ONLY			= false;  
  boolUSE_ALI_SERVICE_SERVER 		= false;
  boolSEND_LEGACY_ALI_ON_SIP_TRANSFER	= false;
  boolALLOW_MANUAL_ALI_BIDS		= false;
  boolIGNORE_BAD_ALI			= false;
  boolALLOW_CLID_ALI_BIDS		= false;
  boolCLID_BID_TEST_KEY			= false;
  boolSHOW_GOOGLE_MAP			= false;
  boolVERIFY_ALI_RECORDS		= false;
  boolREBID_PHASE_ONE_ALI_RECORDS_ONLY	= false;
  boolREBID_NG911                       = false;

  intDEFAULT_ALI_NOISE_THRESHOLD	= 5;
  intALI_PORT_DOWN_THRESHOLD_SEC	= 180;
  intALI_PORT_DOWN_REMINDER_SEC		= 1800;
  intALI_WIRELESS_AUTO_REBID_SEC	= 60;
  intALI_WIRELESS_AUTO_REBID_COUNT	= 0;
  intMAX_ALI_RECORD_SIZE		= 516;
  intNUM_ALI_SERVICE_PORTS		= 0;
  intNUM_ALI_PORT_PAIRS			= 0;
  intNUM_ALI_DATABASES			= 0;
  intCLID_ALI_BID_RULE_CODE		= 2;

  boolRestartALILegacyPorts		= false;
  intOldNumberOfLegacyALIports		= 0;
  boolRestartALIservicePorts		= false;
  intOldNumberOfALIservicePorts		= 0;
  for (unsigned int i = 0; i <= intMAX_ALI_DATABASES; i++){
   ALI_Steering[i].fClear();
  }   
  for (unsigned int i = 0; i <= intMAX_ALI_PORT_PAIRS; i++){
   ALI_SERVICE_PORT_VARS[i].fClear();
  }
  for (unsigned int i = 0; i <= intMAX_ALI_PORT_PAIRS; i++){
   for (unsigned int j = 0; j < 3; j++){
    ALI_PORT_VARS[i][j].fClear();
   }
  }

  ini.Reset();
}

//copy constructor
ALI_Variables::ALI_Variables(const ALI_Variables *a)
{
  boolLEGACY_ALI			= a->boolLEGACY_ALI;
  boolLEGACY_ALI_ONLY			= a->boolLEGACY_ALI_ONLY; 
  boolUSE_ALI_SERVICE_SERVER 		= a->boolUSE_ALI_SERVICE_SERVER;
  boolSEND_LEGACY_ALI_ON_SIP_TRANSFER	= a->boolSEND_LEGACY_ALI_ON_SIP_TRANSFER;
  boolALLOW_MANUAL_ALI_BIDS		= a->boolALLOW_MANUAL_ALI_BIDS;
  boolIGNORE_BAD_ALI			= a->boolIGNORE_BAD_ALI;
  boolALLOW_CLID_ALI_BIDS		= a->boolALLOW_CLID_ALI_BIDS;
  boolCLID_BID_TEST_KEY			= a->boolCLID_BID_TEST_KEY;
  boolSHOW_GOOGLE_MAP			= a->boolSHOW_GOOGLE_MAP;
  boolVERIFY_ALI_RECORDS		= a->boolVERIFY_ALI_RECORDS;
  boolREBID_PHASE_ONE_ALI_RECORDS_ONLY	= a->boolREBID_PHASE_ONE_ALI_RECORDS_ONLY;
  boolREBID_NG911                       = a->boolREBID_NG911;

  intDEFAULT_ALI_NOISE_THRESHOLD	= a->intDEFAULT_ALI_NOISE_THRESHOLD;
  intALI_PORT_DOWN_THRESHOLD_SEC	= a->intALI_PORT_DOWN_THRESHOLD_SEC;
  intALI_PORT_DOWN_REMINDER_SEC		= a->intALI_PORT_DOWN_REMINDER_SEC;
  intALI_WIRELESS_AUTO_REBID_SEC	= a->intALI_WIRELESS_AUTO_REBID_SEC;
  intALI_WIRELESS_AUTO_REBID_COUNT	= a->intALI_WIRELESS_AUTO_REBID_COUNT;
  intMAX_ALI_RECORD_SIZE		= a->intMAX_ALI_RECORD_SIZE;
  intNUM_ALI_SERVICE_PORTS		= a->intNUM_ALI_SERVICE_PORTS;
  intNUM_ALI_PORT_PAIRS			= a->intNUM_ALI_PORT_PAIRS;
  intNUM_ALI_DATABASES			= a->intNUM_ALI_DATABASES;
  intCLID_ALI_BID_RULE_CODE		= a->intCLID_ALI_BID_RULE_CODE;

  boolRestartALILegacyPorts		= a->boolRestartALILegacyPorts;
  intOldNumberOfLegacyALIports		= a->intOldNumberOfLegacyALIports;
  boolRestartALIservicePorts		= a->boolRestartALIservicePorts;
  intOldNumberOfALIservicePorts		= a->intOldNumberOfALIservicePorts;

  for (unsigned int i = 0; i <= intMAX_ALI_DATABASES; i++){
   ALI_Steering[i]			= a->ALI_Steering[i];
  } 
  for (unsigned int i = 0; i <= intMAX_ALI_SERVICE_PORTS; i++){ 
   ALI_SERVICE_PORT_VARS[i]		= a->ALI_SERVICE_PORT_VARS[i];
  }
  for (unsigned int i = 0; i <= intMAX_ALI_PORT_PAIRS; i++){ 
     for (unsigned int j = 0; j <= 2; j++){ 
      ALI_PORT_VARS[i][j]		= a->ALI_PORT_VARS[i][j];
     }
  }
// copy constructor for CSimpleIniA is private will need to do alternate method to move or "reconstruct" 
// for now we only use this variable by reading in the file every time we wish to do an operation
// ini					= a->ini;

}

ALI_Variables& ALI_Variables::operator=(const ALI_Variables& a)
{
 if (&a == this ) {return *this;}
  boolLEGACY_ALI			= a.boolLEGACY_ALI;
  boolLEGACY_ALI_ONLY			= a.boolLEGACY_ALI_ONLY;
  boolUSE_ALI_SERVICE_SERVER 		= a.boolUSE_ALI_SERVICE_SERVER;
  boolSEND_LEGACY_ALI_ON_SIP_TRANSFER	= a.boolSEND_LEGACY_ALI_ON_SIP_TRANSFER;
  boolALLOW_MANUAL_ALI_BIDS		= a.boolALLOW_MANUAL_ALI_BIDS;
  boolIGNORE_BAD_ALI			= a.boolIGNORE_BAD_ALI;
  boolALLOW_CLID_ALI_BIDS		= a.boolALLOW_CLID_ALI_BIDS;
  boolCLID_BID_TEST_KEY			= a.boolCLID_BID_TEST_KEY;
  boolSHOW_GOOGLE_MAP			= a.boolSHOW_GOOGLE_MAP;
  boolVERIFY_ALI_RECORDS		= a.boolVERIFY_ALI_RECORDS;
  boolREBID_PHASE_ONE_ALI_RECORDS_ONLY	= a.boolREBID_PHASE_ONE_ALI_RECORDS_ONLY;
  boolREBID_NG911                       = a.boolREBID_NG911;
  intDEFAULT_ALI_NOISE_THRESHOLD	= a.intDEFAULT_ALI_NOISE_THRESHOLD;
  intALI_PORT_DOWN_THRESHOLD_SEC	= a.intALI_PORT_DOWN_THRESHOLD_SEC;
  intALI_PORT_DOWN_REMINDER_SEC		= a.intALI_PORT_DOWN_REMINDER_SEC;
  intALI_WIRELESS_AUTO_REBID_SEC	= a.intALI_WIRELESS_AUTO_REBID_SEC;
  intALI_WIRELESS_AUTO_REBID_COUNT	= a.intALI_WIRELESS_AUTO_REBID_COUNT;
  intMAX_ALI_RECORD_SIZE		= a.intMAX_ALI_RECORD_SIZE;
  intNUM_ALI_SERVICE_PORTS		= a.intNUM_ALI_SERVICE_PORTS;
  intNUM_ALI_PORT_PAIRS			= a.intNUM_ALI_PORT_PAIRS;
  intNUM_ALI_DATABASES			= a.intNUM_ALI_DATABASES;
  intCLID_ALI_BID_RULE_CODE		= a.intCLID_ALI_BID_RULE_CODE;

  boolRestartALILegacyPorts		= a.boolRestartALILegacyPorts;
  intOldNumberOfLegacyALIports		= a.intOldNumberOfLegacyALIports;
  boolRestartALIservicePorts		= a.boolRestartALIservicePorts;
  intOldNumberOfALIservicePorts		= a.intOldNumberOfALIservicePorts;


  for (unsigned int i = 0; i <= intMAX_ALI_DATABASES; i++){
   ALI_Steering[i]			= a.ALI_Steering[i];
  } 
  for (unsigned int i = 0; i <= intMAX_ALI_SERVICE_PORTS; i++){ 
   ALI_SERVICE_PORT_VARS[i]		= a.ALI_SERVICE_PORT_VARS[i];
  }
  for (unsigned int i = 0; i <= intMAX_ALI_PORT_PAIRS; i++){ 
     for (unsigned int j = 0; j <= 2; j++){ 
      ALI_PORT_VARS[i][j]		= a.ALI_PORT_VARS[i][j];
     }
  }
// copy constructor for CSimpleIniA is private will need to do alternate method to move or "reconstruct"
// ini					= a.ini;
    
 return *this;
}
//compare operator
bool ALI_Variables::fCompare(const ALI_Variables& a) const
{
 bool boolRunningCheck = true;
 bool boolPortcheck;

 //note does not check all fields just those in the ini file

 boolRunningCheck = boolRunningCheck && (this->boolLEGACY_ALI 				== a.boolLEGACY_ALI);
 boolRunningCheck = boolRunningCheck && (this->boolLEGACY_ALI_ONLY 			== a.boolLEGACY_ALI_ONLY);
 boolRunningCheck = boolRunningCheck && (this->boolUSE_ALI_SERVICE_SERVER 		== a.boolUSE_ALI_SERVICE_SERVER);
 boolRunningCheck = boolRunningCheck && (this->boolSEND_LEGACY_ALI_ON_SIP_TRANSFER 	== a.boolSEND_LEGACY_ALI_ON_SIP_TRANSFER);
 boolRunningCheck = boolRunningCheck && (this->boolALLOW_MANUAL_ALI_BIDS 		== a.boolALLOW_MANUAL_ALI_BIDS);
 boolRunningCheck = boolRunningCheck && (this->boolIGNORE_BAD_ALI 			== a.boolIGNORE_BAD_ALI);
 boolRunningCheck = boolRunningCheck && (this->boolALLOW_CLID_ALI_BIDS 			== a.boolALLOW_CLID_ALI_BIDS);
 boolRunningCheck = boolRunningCheck && (this->boolCLID_BID_TEST_KEY 			== a.boolCLID_BID_TEST_KEY);
 boolRunningCheck = boolRunningCheck && (this->boolSHOW_GOOGLE_MAP 			== a.boolSHOW_GOOGLE_MAP);
 boolRunningCheck = boolRunningCheck && (this->boolVERIFY_ALI_RECORDS 			== a.boolVERIFY_ALI_RECORDS);
 boolRunningCheck = boolRunningCheck && (this->boolREBID_PHASE_ONE_ALI_RECORDS_ONLY 	== a.boolREBID_PHASE_ONE_ALI_RECORDS_ONLY);
 boolRunningCheck = boolRunningCheck && (this->boolREBID_NG911			 	== a.boolREBID_NG911);

 boolRunningCheck = boolRunningCheck && (this->intDEFAULT_ALI_NOISE_THRESHOLD 		== a.intDEFAULT_ALI_NOISE_THRESHOLD);
 boolRunningCheck = boolRunningCheck && (this->intALI_PORT_DOWN_THRESHOLD_SEC 		== a.intALI_PORT_DOWN_THRESHOLD_SEC);
 boolRunningCheck = boolRunningCheck && (this->intALI_PORT_DOWN_REMINDER_SEC 		== a.intALI_PORT_DOWN_REMINDER_SEC);
 boolRunningCheck = boolRunningCheck && (this->intALI_WIRELESS_AUTO_REBID_SEC 		== a.intALI_WIRELESS_AUTO_REBID_SEC);
 boolRunningCheck = boolRunningCheck && (this->intALI_WIRELESS_AUTO_REBID_COUNT 	== a.intALI_WIRELESS_AUTO_REBID_COUNT);
 boolRunningCheck = boolRunningCheck && (this->intMAX_ALI_RECORD_SIZE 			== a.intMAX_ALI_RECORD_SIZE);

 //Require restart of Ports .........
 this->boolRestartALIservicePorts =     (!(this->intNUM_ALI_SERVICE_PORTS 		== a.intNUM_ALI_SERVICE_PORTS));
 a.boolRestartALIservicePorts          = this->boolRestartALIservicePorts;
 this->intOldNumberOfALIservicePorts   = this->intNUM_ALI_SERVICE_PORTS;
 a.intOldNumberOfALIservicePorts       = this->intNUM_ALI_SERVICE_PORTS;
 boolRunningCheck = boolRunningCheck && (this->intNUM_ALI_SERVICE_PORTS 		== a.intNUM_ALI_SERVICE_PORTS);
 this->boolRestartALILegacyPorts = 	(!(this->intNUM_ALI_PORT_PAIRS		 	== a.intNUM_ALI_PORT_PAIRS));
 this->intOldNumberOfLegacyALIports    = this->intNUM_ALI_PORT_PAIRS;
 a.boolRestartALILegacyPorts           = this->boolRestartALILegacyPorts;
 a.intOldNumberOfLegacyALIports        = this-> intOldNumberOfLegacyALIports;
 boolRunningCheck = boolRunningCheck && (this->intNUM_ALI_PORT_PAIRS		 	== a.intNUM_ALI_PORT_PAIRS);
 //..................................
 boolRunningCheck = boolRunningCheck && (this->intNUM_ALI_DATABASES 			== a.intNUM_ALI_DATABASES);
 boolRunningCheck = boolRunningCheck && (this->intCLID_ALI_BID_RULE_CODE 		== a.intCLID_ALI_BID_RULE_CODE);

 for ( int i = 0; i <=  this->intNUM_ALI_DATABASES; i++)
  {
   boolRunningCheck = boolRunningCheck && (this->ALI_Steering[i] == a.ALI_Steering[i]);
  }

  for ( int i = 0; i <=  this->intNUM_ALI_SERVICE_PORTS; i++)   { 
      boolPortcheck = (this->ALI_SERVICE_PORT_VARS[i].fCompare(a.ALI_SERVICE_PORT_VARS[i])); 
      if (!boolPortcheck) {this->boolRestartALIservicePorts = a.boolRestartALIservicePorts = true;}
      boolRunningCheck = (boolRunningCheck && boolPortcheck); 
  }
  for ( int i = 0; i <=  this->intNUM_ALI_PORT_PAIRS; i++)  {
   for (unsigned int j = 0; j <=  2; j++) {
     boolPortcheck = (this->ALI_PORT_VARS[i][j].fCompare(a.ALI_PORT_VARS[i][j])); 
     if (!boolPortcheck) {this->boolRestartALILegacyPorts = a.boolRestartALILegacyPorts = true;}
     boolRunningCheck = (boolRunningCheck && boolPortcheck); 
   }
  }

 return boolRunningCheck;
}

void ALI_Variables::Set_GLobal_VARS()
{
  extern bool			boolLEGACY_ALI;
  extern bool			boolLEGACY_ALI_ONLY;
  extern bool 			boolUSE_ALI_SERVICE_SERVER;
  extern bool			boolSEND_LEGACY_ALI_ON_SIP_TRANSFER;
  extern bool			boolALLOW_MANUAL_ALI_BIDS;
  extern bool			boolIGNORE_BAD_ALI;
  extern bool			boolALLOW_CLID_ALI_BIDS;
  extern bool			boolCLID_BID_TEST_KEY;
  extern bool			boolSHOW_GOOGLE_MAP;
  extern bool			boolVERIFY_ALI_RECORDS;
  extern bool			boolREBID_PHASE_ONE_ALI_RECORDS_ONLY;
  extern bool                   boolREBID_NG911;

  extern unsigned int		intDEFAULT_ALI_NOISE_THRESHOLD;
  extern int			intALI_PORT_DOWN_THRESHOLD_SEC;
  extern int			intALI_PORT_DOWN_REMINDER_SEC;
  extern int			intALI_WIRELESS_AUTO_REBID_SEC;
  extern int			intALI_WIRELESS_AUTO_REBID_COUNT;
  extern unsigned int		intMAX_ALI_RECORD_SIZE;
  extern int 			intNUM_ALI_SERVICE_PORTS;
  extern int			intNUM_ALI_PORT_PAIRS;
  extern int			intNUM_ALI_DATABASES;
  extern int			intCLID_ALI_BID_RULE_CODE;

  extern ALISteering     	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
  extern ExperientCommPort  	ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
  extern ExperientCommPort      ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ]; 

 boolLEGACY_ALI 			= this->boolLEGACY_ALI;
 boolLEGACY_ALI_ONLY 			= this->boolLEGACY_ALI_ONLY;
 boolUSE_ALI_SERVICE_SERVER 		= this->boolUSE_ALI_SERVICE_SERVER;
 boolSEND_LEGACY_ALI_ON_SIP_TRANSFER 	= this->boolSEND_LEGACY_ALI_ON_SIP_TRANSFER;
 boolALLOW_MANUAL_ALI_BIDS 		= this->boolALLOW_MANUAL_ALI_BIDS; 
 boolIGNORE_BAD_ALI 			= this->boolIGNORE_BAD_ALI;
 boolALLOW_CLID_ALI_BIDS 		= this->boolALLOW_CLID_ALI_BIDS;
 boolCLID_BID_TEST_KEY 			= this->boolCLID_BID_TEST_KEY;
 boolSHOW_GOOGLE_MAP 			= this->boolSHOW_GOOGLE_MAP;
 boolVERIFY_ALI_RECORDS 		= this->boolVERIFY_ALI_RECORDS;
 boolREBID_PHASE_ONE_ALI_RECORDS_ONLY 	= this->boolREBID_PHASE_ONE_ALI_RECORDS_ONLY;
 boolREBID_NG911			= this->boolREBID_NG911;
 intDEFAULT_ALI_NOISE_THRESHOLD 	= this->intDEFAULT_ALI_NOISE_THRESHOLD;
 intALI_PORT_DOWN_THRESHOLD_SEC 	= this->intALI_PORT_DOWN_THRESHOLD_SEC;
 intALI_PORT_DOWN_REMINDER_SEC 		= this->intALI_PORT_DOWN_REMINDER_SEC;
 intALI_WIRELESS_AUTO_REBID_SEC 	= this->intALI_WIRELESS_AUTO_REBID_SEC;
 intALI_WIRELESS_AUTO_REBID_COUNT 	= this->intALI_WIRELESS_AUTO_REBID_COUNT;
 intMAX_ALI_RECORD_SIZE 		= this->intMAX_ALI_RECORD_SIZE;
 intNUM_ALI_SERVICE_PORTS 		= this->intNUM_ALI_SERVICE_PORTS;
 intNUM_ALI_PORT_PAIRS 			= this->intNUM_ALI_PORT_PAIRS;
 intNUM_ALI_DATABASES 			= this->intNUM_ALI_DATABASES;
 intCLID_ALI_BID_RULE_CODE 		= this->intCLID_ALI_BID_RULE_CODE;

 for (unsigned int i = 0; i <=  intMAX_ALI_DATABASES; i++)  {
   ALI_Steering[i] = this->ALI_Steering[i];
 }
/*
  for (unsigned int i = 0; i <=  intMAX_ALI_PORT_PAIRS; i++) 
  { 
   ALIServicePort[i].ePortConnectionType 	= this->ALIServicePort[i].ePortConnectionType;
   ALIServicePort[i].eALIportProtocol 		= this->ALIServicePort[i].eALIportProtocol;
   ALIServicePort[i].intLocalPort 		= this->ALIServicePort[i].intLocalPort;
   ALIServicePort[i].RemoteIPAddress 		= this->ALIServicePort[i].RemoteIPAddress;
   ALIServicePort[i].intRemotePortNumber 	= this->ALIServicePort[i].intRemotePortNumber;
   ALIServicePort[i].strNotes 			= this->ALIServicePort[i].strNotes;
   ALIServicePort[i].boolPhantomPort 		= this->ALIServicePort[i].boolPhantomPort;
   ALIServicePort[i].eRequestKeyFormat 		= this->ALIServicePort[i].eRequestKeyFormat;// set by ALI steering fLoadKeyFormats()
   for (unsigned int j = 0; j <=  2; j++) 
    {
     ALIPort[i][j].ePortConnectionType 	= this->ALIPort[i][j].ePortConnectionType;
     ALIPort[i][j].eALIportProtocol	= this->ALIPort[i][j].eALIportProtocol;
     ALIPort[i][j].intLocalPort 	= this->ALIPort[i][j].intLocalPort;
     ALIPort[i][j].RemoteIPAddress 	= this->ALIPort[i][j].RemoteIPAddress;
     ALIPort[i][j].intRemotePortNumber 	= this->ALIPort[i][j].intRemotePortNumber;
     ALIPort[i][j].strNotes 		= this->ALIPort[i][j].strNotes;
     ALIPort[i][j].boolPhantomPort 	= this->ALIPort[i][j].boolPhantomPort;
     ALIPort[i][j].eRequestKeyFormat 	= this->ALIPort[i][j].eRequestKeyFormat;// set by ALI steering aka fLoadKeyFormats()
    }
  }
*/
 return;
}

void ALI_Variables::fLoadKeyFormats()
{

 for (int i = 1; i <= intNUM_ALI_DATABASES; i++)
  {
   for (int j = 1; j <= intNUM_ALI_PORT_PAIRS; j++)
    {  
     if (this->ALI_Steering[i].boolPortPairInDatabase[j])
      {
       for (int k = 1; k <=2; k++) { this->ALI_PORT_VARS[j] [k].eRequestKeyFormat = this->ALI_Steering[i].eRequestKeyFormat;}
      }     
    }
  }
return;
}

bool ALI_Variables::fLoadINIfile()
{
 string 	strMessage="ALI_Variables::fLoadINIfile() -Unable to load ini file -> ";
 strMessage +=  charINI_FILE_PATH_AND_NAME;
 
 this->ini.SetUnicode(true);
 this->ini.SetMultiKey(true);
 this->ini.SetMultiLine(true);

 SI_Error rc = this->ini.LoadFile(charINI_FILE_PATH_AND_NAME);
 if (rc < 0) { SendCodingError(strMessage); return false;}
 return true;
}

bool ALI_Variables::fLoadLegacyALISetting()
{
 return char2bool(this->boolLEGACY_ALI, ini.GetValue("ALI", ALI_INI_KEY_12, DEFAULT_VALUE_ALI_INI_KEY_12 ));
}

bool ALI_Variables::fLoadLegacyALIonlySetting()
{
 bool ReturnValue;

 ReturnValue =  char2bool(this->boolLEGACY_ALI_ONLY, ini.GetValue("ALI", ALI_INI_KEY_21, DEFAULT_VALUE_ALI_INI_KEY_21 ));
 if (ReturnValue) {
  if (this->boolLEGACY_ALI_ONLY) {
   if (!this->boolLEGACY_ALI) {
    this->boolLEGACY_ALI_ONLY = false;
    SendCodingError("initalization.cpp - Coding Error in ALI_Variables::fLoadLegacyALIonlySetting() Legacy_ALI_Only now set to false!");
   }
  }
 }
 return ReturnValue;
}

bool ALI_Variables::fLoadUseALIserviceServer()
{
 this->intNUM_ALI_SERVICE_PORTS = 0; //intialize it
 return char2bool(this->boolUSE_ALI_SERVICE_SERVER, ini.GetValue("ALI", ALI_INI_KEY_13, DEFAULT_VALUE_ALI_INI_KEY_13 ));
}

bool ALI_Variables::fLoadRebidNG911()
{
 this->boolREBID_NG911 = false; //intialize it
 return char2bool(this->boolREBID_NG911, ini.GetValue("ALI", ALI_INI_KEY_22, DEFAULT_VALUE_ALI_INI_KEY_22 ));
}


bool ALI_Variables::fLoadSendLegacyALIonSIPtransfer()
{
 //This appears to be dead code ........
 return char2bool(this->boolSEND_LEGACY_ALI_ON_SIP_TRANSFER, ini.GetValue("ALI", ALI_INI_KEY_17, DEFAULT_VALUE_ALI_INI_KEY_17 ));
}

bool ALI_Variables::fLoadAllowManualBids()
{
 return char2bool(this->boolALLOW_MANUAL_ALI_BIDS, ini.GetValue("ALI", ALI_INI_KEY_4, DEFAULT_VALUE_ALI_INI_KEY_4 ));
}

bool ALI_Variables::fLoadIgnoreBadALI()
{
 return char2bool(this->boolIGNORE_BAD_ALI, ini.GetValue("ALI", ALI_INI_KEY_10, DEFAULT_VALUE_ALI_INI_KEY_10 ));
}

bool ALI_Variables::fLoadShowGoogleMap()
{
 return char2bool(this->boolSHOW_GOOGLE_MAP, ini.GetValue("ALI", ALI_INI_KEY_7, DEFAULT_VALUE_ALI_INI_KEY_7 ));
}

bool ALI_Variables::fLoadVerifyALIrecords()
{
 return char2bool(this->boolVERIFY_ALI_RECORDS, ini.GetValue("ALI", ALI_INI_KEY_8, DEFAULT_VALUE_ALI_INI_KEY_8 ));
}

bool ALI_Variables::fLoadRebidPhaseOneRecordsOnly()
{
 return char2bool(this->boolREBID_PHASE_ONE_ALI_RECORDS_ONLY, ini.GetValue("ALI", ALI_INI_KEY_9, DEFAULT_VALUE_ALI_INI_KEY_9 ));
}

bool ALI_Variables::fLoadDefaultNoiseThreshold()
{
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("ALI", ALI_INI_KEY_18, DEFAULT_VALUE_ALI_INI_KEY_18 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intDEFAULT_ALI_NOISE_THRESHOLD = char2int(strNumber.c_str());
 return true;
}

bool PORT_INIT_VARS::fLoad_Vendor_Email_Address(string stringArg) {
 if (stringArg == DEFAULT_VALUE_KRN_INI_KEY_13) {return false;}

 this->strVendorEmailAddress = stringArg;
return true;
}


bool ALI_Variables::fLoadPortDownThreshold()
{
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("ALI", ALI_INI_KEY_1, DEFAULT_VALUE_ALI_INI_KEY_1 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
//  this->ALI_PORT_VARS[i].intPORT_DOWN_THRESHOLD_SEC = char2int(strNumber.c_str());
  this->intALI_PORT_DOWN_THRESHOLD_SEC = char2int(strNumber.c_str());
 return true;
}

bool ALI_Variables::fLoadPortDownReminder()
{
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("ALI", ALI_INI_KEY_2, DEFAULT_VALUE_ALI_INI_KEY_2 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intALI_PORT_DOWN_REMINDER_SEC = char2int(strNumber.c_str());
//  this->ALI_PORT_VARS[i].intPORT_DOWN_REMINDER_SEC = char2int(strNumber.c_str());
 return true;
}

bool ALI_Variables::fLoadWirelessRebidSec()
{
 string strNumberOne, strNumberTwo;
 size_t found;

  strNumberOne = ini.GetValue("ALI", ALI_INI_KEY_3, DEFAULT_VALUE_ALI_INI_KEY_3 );
  strNumberTwo = ini.GetValue("ALI", ALI_INI_KEY_19, DEFAULT_VALUE_ALI_INI_KEY_19 );

  found =  strNumberOne.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  found =  strNumberTwo.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}

  this->intALI_WIRELESS_AUTO_REBID_SEC = char2int(strNumberOne.c_str());
  // make old and new field work
   if (this->intALI_WIRELESS_AUTO_REBID_SEC == int( char2int(DEFAULT_VALUE_ALI_INI_KEY_3)))
    {  this->intALI_WIRELESS_AUTO_REBID_SEC  = char2int(strNumberTwo.c_str());}

 return true;
}

bool ALI_Variables::fLoadWirelessRebidCount()
{
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("ALI", ALI_INI_KEY_6, DEFAULT_VALUE_ALI_INI_KEY_6 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intALI_WIRELESS_AUTO_REBID_COUNT = char2int(strNumber.c_str());
 return true;
}

bool ALI_Variables::fLoadMaxALIrecordSize()
{
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("ALI", ALI_INI_KEY_11, DEFAULT_VALUE_ALI_INI_KEY_11 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intMAX_ALI_RECORD_SIZE = char2int(strNumber.c_str());
 return true;
}

bool ALI_Variables::fLoadCLIDbidRules()
{

   this->intCLID_ALI_BID_RULE_CODE = char2int (this->ini.GetValue("ALI", ALI_INI_KEY_5, DEFAULT_VALUE_ALI_INI_KEY_5 ));
   switch(this->intCLID_ALI_BID_RULE_CODE)
    {
     case 0:
            this->boolALLOW_CLID_ALI_BIDS = true;
            this->boolCLID_BID_TEST_KEY   = false;
            break;
     case 1:
            this->boolALLOW_CLID_ALI_BIDS = true;
            this->boolCLID_BID_TEST_KEY   = true;
            break;
     default:
            this->boolALLOW_CLID_ALI_BIDS = false;
    }

 return true;
}

bool ALISteering::fLoadClassofServiceData(XMLNode ClassofServiceNode )
{
 int 	iNumberOfCallbackNodes = 0;
 string 		strRow;
 size_t 	found;
 COSCallbackRow objCOScallback;
 XMLNode        CallbackNode;

 //set class of service row
 if (ClassofServiceNode.getAttribute(XML_FIELD_ROW))
      {strRow = ClassofServiceNode.getAttribute(XML_FIELD_ROW);}
 else                                                         {SendCodingError("ALISteering::fLoadClassofServiceData - Missing COS row!"); return false;}
 found =  strRow.find_first_not_of("0123456789");
 if (found != string::npos)                                   {SendCodingError("ALISteering::fLoadClassofServiceData - COS row not a number!"); return false;}
 this->iClassOfServiceRow = char2int(strRow.c_str());

 if (ClassofServiceNode.getAttribute(XML_FIELD_FIRST_CALLBACK_ROW)){
  strRow = ClassofServiceNode.getAttribute(XML_FIELD_FIRST_CALLBACK_ROW);
  found =  strRow.find_first_not_of("0123456789");
  if (found != string::npos)                                   {SendCodingError("ALISteering::fLoadClassofServiceData - 1st Callback Location row not a number!"); return false;}
  this->intCallbackLocationRow[0] = char2int(strRow.c_str());
 // //cout << "1st Callback ROW -> " << this->intCallbackLocationRow[1] << endl;
 }

 if (ClassofServiceNode.getAttribute(XML_FIELD_FIRST_CALLBACK_COLUMN)){
  strRow = ClassofServiceNode.getAttribute(XML_FIELD_FIRST_CALLBACK_COLUMN);
  found =  strRow.find_first_not_of("0123456789");
  if (found != string::npos)                                   {SendCodingError("ALISteering::fLoadClassofServiceData - 1st Callback Location column not a number!"); return false;}
  this->intCallbackLocationColumn[0] = char2int(strRow.c_str());
 }

 if (ClassofServiceNode.getAttribute(XML_FIELD_SECOND_CALLBACK_ROW)){
  strRow = ClassofServiceNode.getAttribute(XML_FIELD_SECOND_CALLBACK_ROW);
  found =  strRow.find_first_not_of("0123456789");
  if (found != string::npos)                                   {SendCodingError("ALISteering::fLoadClassofServiceData - 2nd Callback Location row not a number!"); return false;}
  this->intCallbackLocationRow[1] = char2int(strRow.c_str());
 // //cout << "2nd Callback ROW -> " << this->intCallbackLocationRow[1] << endl;
 }

 if (ClassofServiceNode.getAttribute(XML_FIELD_SECOND_CALLBACK_ROW)){
  strRow = ClassofServiceNode.getAttribute(XML_FIELD_SECOND_CALLBACK_ROW);
  found =  strRow.find_first_not_of("0123456789");
  if (found != string::npos)                                   {SendCodingError("ALISteering::fLoadClassofServiceData - 2nd Callback Location column not a number!"); return false;}
  this->intCallbackLocationColumn[1] = char2int(strRow.c_str());
 }
 // Get callbacks via COS list
 this->vCOSCallbackRow.clear();

 iNumberOfCallbackNodes = ClassofServiceNode.nChildNode(XML_NODE_CALLBACK);

 for (int i=0; i < iNumberOfCallbackNodes; i++) {
  CallbackNode = ClassofServiceNode.getChildNode(XML_NODE_CALLBACK, i);

  if (CallbackNode.getAttribute(XML_FIELD_ROW))
      {strRow = CallbackNode.getAttribute(XML_FIELD_ROW);} 
  else                                                        {SendCodingError("ALISteering::fLoadClassofServiceData - Missing Callback row!"); return false;} 
  found =  strRow.find_first_not_of("0123456789");
  if (found != string::npos)                                  {SendCodingError("ALISteering::fLoadClassofServiceData - Callback row not a number!"); return false;}
  objCOScallback.iRow = char2int(strRow.c_str());

  if (CallbackNode.getText()) {
   objCOScallback.strClassOfService = CallbackNode.getText();
  }
  else {
   SendCodingError("ALISteering::fLoadClassofServiceData - Callback COS Missing!");
   return false;
  }
  if (objCOScallback.strClassOfService.length() != 4) {SendCodingError("ALISteering::fLoadClassofServiceData - Callback COS not 4 characters!"); return false;}
//  //cout << objCOScallback.strClassOfService << " " << objCOScallback.iRow << endl;
  this->vCOSCallbackRow.push_back(objCOScallback);
 }

 return true;
}
bool ALI_Variables::fLoadAliDataBases()
{
 //fLoadAliPorts() must be called prior to this fn !!!!!
 string						StringData;
 string						Ali_Key;
 int                				i, j, intRC;
 CSimpleIniA::TNamesDepend 			ClassOfService_Values;
 CSimpleIniA::TNamesDepend::const_iterator 	l;

 string Create_INI_Key(string stringArg1, int intArg1, string stringArg2="", int intArg2=0, string stringArg3 = "", int intArg3=0, string stringArg4="", int intArg4=0); 



  i = 1;
   do
    {  
     Ali_Key = Create_INI_Key("Database", i, "Pairs");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     intRC = this->ALI_Steering[i].fLoadPortPairs(i, StringData);
     if (intRC){SendCodingError("ALI_Variables::fLoadAliDataBases() -> ALISteering::fLoadPortPairs() failed");           return false;}
     i++;
    
   }while ( StringData != "");

   // Calculate number of databases
   this->intNUM_ALI_DATABASES = i-1;
   if (this->intNUM_ALI_DATABASES > this->intNUM_ALI_PORT_PAIRS) {SendCodingError("ALI_Variables::fLoadAliDataBases() -> More Databases than Port Pairs");           return false;}   

   // check for "all" Databases CC Email addresses
   StringData = this->ini.GetValue("ALI", KRN_INI_KEY_13, DEFAULT_VALUE_KRN_INI_KEY_13);
   // load email address into all the portpairs or set to false
   for (int x = 1; x <= this->intNUM_ALI_PORT_PAIRS; x++){for (int y = 1; y <= 2; y++){this->ALI_PORT_VARS[x][y].fLoad_Vendor_Email_Address(StringData);}}

   // Load Database specific CC email addresses
   for (int x = 1; x<= intNUM_ALI_DATABASES; x++)
    {
     Ali_Key = Create_INI_Key("Database", x, "CC_Alarm_Email_Address");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), DEFAULT_VALUE_KRN_INI_KEY_13 );

     if (StringData  == DEFAULT_VALUE_KRN_INI_KEY_13) {continue;}
     for (int y = 1; y <= intMAX_ALI_PORT_PAIRS; y++)
      {if (this->ALI_Steering[x].boolPortPairInDatabase[y]){for (int z = 1; z <= 2; z++){this->ALI_PORT_VARS[y][z].fLoad_Vendor_Email_Address(StringData);}}}
    }

   // Load PANI number ranges
   i = 1;
   do
    {
     j = 1;
     do
      {
       Ali_Key = Create_INI_Key("Database", i, "Range",j);
       StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "" );
       if (StringData  == "") {continue;}

       intRC = this->ALI_Steering[i].fLoadPANIRanges(i, j, StringData);
       if (intRC){SendCodingError("ALI_Variables::fLoadAliDataBases() ->  ALISteering::fLoadPANIRanges() failed");           return false;}  
       j++;
      }while ( StringData != "");
     i++;

    }while (i <= this->intNUM_ALI_DATABASES);  

   //load ali database request key lengths
   i = 1;
   do
    {
     Ali_Key = Create_INI_Key("Database", i, "Request",0,"Key",0,"Format");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     intRC = this->ALI_Steering[i].fLoadALIRequestFormat(StringData);
     if (intRC){SendCodingError("ALI_Variables::fLoadAliDataBases() ->  ALISteering::fLoadALIRequestFormat() failed");           return false;}
     i++;
    
    }while ( StringData != "");

   if (this->intNUM_ALI_DATABASES != (i-1)){SendCodingError("ALI_Variables::fLoadAliDataBases() ->  Databases do not match number of trunk keys");           return false;}  

   //load ali database Trunk Type
   i = 1;
   do
    {
     Ali_Key = Create_INI_Key("Database", i, "Trunk",0,"Type");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     if (!this->ALI_Steering[i].fLoadTrunkType(StringData)){SendCodingError("ALI_Variables::fLoadAliDataBases() ->  ALISteering::fLoadTrunkType() failed");           return false;} 
     i++;
    
    }while ( StringData != "");

   if (this->intNUM_ALI_DATABASES != (i-1)){SendCodingError("ALI_Variables::fLoadAliDataBases() -> Databases do not match number of trunk types");           return false;} 

   //load ali database Sends_Single_ALI boolean
   for (int x = 1; x<= this->intNUM_ALI_DATABASES; x++)
    {
     Ali_Key = Create_INI_Key("Database", x, "Sends",0,"Single",0,"ALI");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "true" );
     if(! char2bool(this->ALI_Steering[x].boolDatabaseSendsSingleALI, StringData.c_str())) 
      {SendCodingError("ALI_Variables::fLoadAliDataBases() -> ALISteering::boolDatabaseSendsSingleALI() failed");           return false;}    
    }

   //load ali database Callback Location
   for (int x = 1; x<= this->intNUM_ALI_DATABASES; x++)
    {
     Ali_Key = Create_INI_Key("Database", x, "Callback",0,"LocationA");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "Missing Data" );     
     if (!this->ALI_Steering[x].fLoadCallbackLocation(StringData,0)) {SendCodingError("ALI_Variables::fLoadAliDataBases() -> ALISteering::fLoadCallbackLocation(A) failed");           return false;} 
     Ali_Key = Create_INI_Key("Database", x, "Callback",0,"LocationB");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "0" );     
     if (!this->ALI_Steering[x].fLoadCallbackLocation(StringData,1)){SendCodingError("ALI_Variables::fLoadAliDataBases() -> ALISteering::fLoadCallbackLocation(B) failed");           return false;}
 
    //Get Treat LF as CR property
    Ali_Key = Create_INI_Key("Database", x, "LF",0,"AS",0,"CR");
    StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "false" ); 
    if(! char2bool(this->ALI_Steering[x].boolTreatLinefeedasCR, StringData.c_str()))    
     {SendCodingError("ALI_Variables::fLoadAliDataBases() -> ALISteering::boolTreatLinefeedasCR() failed");           return false;}    

    // Get all Class of Service and Row Data
    Ali_Key = Create_INI_Key("Database", x, "COS",0,"Row");
    StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "0" );     
    this->ALI_Steering[x].fLoadClassOfServiceRow(StringData);
    Ali_Key = Create_INI_Key("Database", x, "COS",0,"Callback");
    this->ini.GetAllValues("ALI", Ali_Key.c_str(), ClassOfService_Values);

    ClassOfService_Values.sort(CSimpleIniA::Entry::LoadOrder());

    l = ClassOfService_Values.begin();
    while (l != ClassOfService_Values.end()) 
     { 
      if(!this->ALI_Steering[x].fLoadCOSvector(l->pItem))      {SendCodingError("ALI_Variables::fLoadAliDataBases() -> ALISteering::fLoadCOSvector() failed");           return false;}   
      ++l; 
     } 
//   //cout << "COS ROW: " << ALI_Steering[x].iClassOfServiceRow << endl;
 //   this->ALI_Steering[x].fDisplayCOSVector();

    }// end for (int x = 1; x<= this->intNUM_ALI_DATABASES; x++) 
  
 return true;
}

bool ALI_Variables::fLoadAliPorts()
{
 //this->ini should be loaded prior to call !
 string					StringData;
 string					Ali_Key;
 int                			i,j;
 string            			Create_INI_Key(string stringArg1, int intArg1, string stringArg2="", int intArg2=0, string stringArg3 = "", int intArg3=0, string stringArg4="", int intArg4=0); 


 this->intNUM_ALI_PORT_PAIRS = 0;

  // Load ALI IP & Port A Data (must be loaded first..)
   i = j = intNUM_ALI_PORT_PAIRS + 1;
   do
    {
     Ali_Key = Create_INI_Key("Pair", i, "IP",0,"Address",0,"A");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     if(!this->LoadPortData( i, "A" , StringData))                       {SendCodingError("ALI_Variables::fLoadAliPorts() -> LoadPortData(A) failed");           return false;} 
     Ali_Key = Create_INI_Key("Pair", i, "Connection_Type_A");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "UDP" );
     if(!this->LoadPortConnectionType( i, "A" , StringData))             {SendCodingError("ALI_Variables::fLoadAliPorts() -> LoadPortConnectionType(A) failed"); return false;}
     Ali_Key = Create_INI_Key("Pair", i, "Connection_Protocol");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "Modem" );
     if(!this->LoadALIPortProtocolType(i, StringData))                   {SendCodingError("ALI_Variables::fLoadAliPorts() -> LoadALIPortProtocolType(A) failed"); return false;}
     Ali_Key = Create_INI_Key("Pair", i, "Connection_Note_A");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "None." );
     this->ALI_PORT_VARS[i] [1].strNotes = StringData;
     this->ALI_PORT_VARS[i] [1].boolPhantomPort = PhantomPort(StringData);  
     i++;
    
   }while ( StringData != "");

   this->intNUM_ALI_PORT_PAIRS = i-1;

   // Load ALI IP & Port B Data 
   i = j;
   do
    {
     Ali_Key = Create_INI_Key("Pair", i, "IP",0,"Address",0,"B");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "" );
     if (StringData  == "") {continue;} 
     if(!this->LoadPortData( i, "B" , StringData))                      {SendCodingError("ALI_Variables::fLoadAliPorts() -> LoadPortData(B) failed");           return false;}
     Ali_Key = Create_INI_Key("Pair", i, "Connection_Type_B");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "UDP" );
     if(!this->LoadPortConnectionType( i, "B" , StringData))            {SendCodingError("ALI_Variables::fLoadAliPorts() -> LoadPortConnectionType(B) failed"); return false;}             
     Ali_Key = Create_INI_Key("Pair", i, "Connection_Protocol");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "Modem" );
     if(!this->LoadALIPortProtocolType(i, StringData))                  {SendCodingError("ALI_Variables::fLoadAliPorts() -> LoadALIPortProtocolType(B) failed"); return false;}
     Ali_Key = Create_INI_Key("Pair", i, "Connection_Note_B");
     StringData = this->ini.GetValue("ALI", Ali_Key.c_str(), "None." );
     this->ALI_PORT_VARS[i] [2].strNotes = StringData;
     this->ALI_PORT_VARS[i] [2].boolPhantomPort = PhantomPort(StringData);
     i++;
    
   }while ( StringData != "");

   if (this->intNUM_ALI_PORT_PAIRS != (i-1)) {SendCodingError("ALI_Variables::fLoadAliPorts() -> Port A's do not equal Port B's"); return false;} 

   return true;
}

bool ALI_Variables::LoadALIPortProtocolType( int i, string stringArg )
{
  int                           iswitch;


 if (i > intMAX_ALI_PORT_PAIRS) {SendCodingError("ALI_Variables::LoadALIPortProtocolType() -> MAX ALI Port Pairs Exceeded"); return false;}

 if      (stringArg == "Modem")   {this->ALI_PORT_VARS[i][1].eALIportProtocol = this->ALI_PORT_VARS[i][2].eALIportProtocol = ALI_MODEM;   iswitch = 1;}
 else if (stringArg == "E2")      {this->ALI_PORT_VARS[i][1].eALIportProtocol = this->ALI_PORT_VARS[i][2].eALIportProtocol = ALI_E2;      iswitch = 2;}
 else if (stringArg == "Service") {this->ALI_PORT_VARS[i][1].eALIportProtocol = this->ALI_PORT_VARS[i][2].eALIportProtocol = ALI_SERVICE; iswitch = 3;} 
 else                             {SendCodingError("ALI_Variables::LoadALIPortProtocolType() -> Unknown Protocol"); return false;}

 switch (iswitch)
  {
   case 1:
          break;
   case 2:
          if (this->ALI_PORT_VARS[i][1].ePortConnectionType == UDP) {SendCodingError("ALI_Variables::LoadALIPortProtocolType() -> E2 cannot be UDP"); return false;}
          break;
   case 3:
          if (this->ALI_PORT_VARS[i][1].ePortConnectionType == UDP) {SendCodingError("ALI_Variables::LoadALIPortProtocolType() -> ALI_SERVICE cannot be UDP"); return false;}
          break;
   default:
          SendCodingError("ALI_Variables::LoadALIPortProtocolType() -> Unspecified Error"); return false;
  }      

 return true;
}




bool ALI_Variables::fLoadAliServicePorts()
{
  //Note boolUSE_ALI_SERVICE_SERVER must be set before calling this function!!!!!
  CSimpleIniA::TNamesDepend 			ALI_SERVICE_IP_Values, ALI_SERVICE_Port_Values , ALI_SERVICE_Connection_Values;
  CSimpleIniA::TNamesDepend::const_iterator 	l,m,n;
  int						i;
  string					StringData;
  int						intRC;

   if(!this->boolUSE_ALI_SERVICE_SERVER) {this->intNUM_ALI_SERVICE_PORTS = 0; return true;}

   ini.GetAllValues("ALI", "ALIservice_IP_Address", ALI_SERVICE_IP_Values);
   ini.GetAllValues("ALI", "ALIservice_Port", ALI_SERVICE_Port_Values);
   ini.GetAllValues("ALI", "ALIservice_Connection", ALI_SERVICE_Connection_Values);

   ALI_SERVICE_IP_Values.sort(CSimpleIniA::Entry::LoadOrder());
   ALI_SERVICE_Port_Values.sort(CSimpleIniA::Entry::LoadOrder());
   ALI_SERVICE_Connection_Values.sort(CSimpleIniA::Entry::LoadOrder());

   l = ALI_SERVICE_IP_Values.begin();
   m = ALI_SERVICE_Port_Values.begin();
   n = ALI_SERVICE_Connection_Values.begin();

   i = 1;
   while ((l != ALI_SERVICE_IP_Values.end())||(m != ALI_SERVICE_Port_Values.end())||(n != ALI_SERVICE_Connection_Values.end()) )
    { 
     StringData =  l->pItem; StringData += ","; StringData += m->pItem;
     if(!this->LoadPortData( i, "S" , StringData))                    {SendCodingError("ALI_Variables::fLoadAliServicePorts() -> LoadPortData() failed");          return false;}
     if(!this->LoadPortConnectionType( i, "S" , n->pItem ))           {SendCodingError("ALI_Variables::fLoadAliServicePorts() -> LoadPortConnectionType() failed");return false;}
     this->ALI_SERVICE_PORT_VARS[i].eALIportProtocol = ALI_SERVICE;
     l++;m++;n++; i++;
    }

   this->intNUM_ALI_SERVICE_PORTS = i-1;

 return true;
}

bool ALI_Variables::LoadPortData( int intArg, string charArg , string stringArg )
{
 string                         stringTemp1 = "";
 string                         stringTemp2 = "";
 size_t                         index1,index2,index3;  
 int                            i,j;     
 IP_Address                     IPAddress;
 string                         stringPort;
 string                         stringPortSuffix="";
 
 if      (charArg == "A") {i = 1;}
 else if (charArg == "B") {i = 2;}
 else if (charArg == "S") {i = 3;}
 else                     {i = 0;}

 if (charArg !=""){stringPortSuffix = " "+charArg;}

 stringTemp1 = stringArg;
 stringPort = int2strLZ(intArg)+stringPortSuffix;

 // assign string from first number to delimeter
 index1 = stringTemp1.find_first_of("0123456789.");
 index2 = stringTemp1.find_first_of(",");
 
 if ((index1 == string::npos)||(index2 == string::npos)){SendCodingError("ALI_Variables::LoadPortData() - No number found or no comma found"); return false; }

 IPAddress.stringAddress.assign(stringTemp1, index1 , (index2-index1) );

 // strip trailing blanks ...
 j = IPAddress.stringAddress.length()-1;
 while ((IPAddress.stringAddress[j] == ' ')&& (j > 0))
  {
   IPAddress.stringAddress.erase(j);
   j--;
  }
 
 // check for valid data (numbers or .'s) 

 index3 =  IPAddress.stringAddress.find_first_not_of("0123456789.");
 if (index3 != string::npos)            {SendCodingError("ALI_Variables::LoadPortData() - Invalid IP address"); return false; }
 if (!IPAddress.fIsValid_IP_Address())  {SendCodingError("ALI_Variables::LoadPortData() - Invalid IP address"); return false; }

 // load the port number
 stringTemp1.erase(index1 ,(index2-index1));
 stringTemp2 = "";
 index1 = stringTemp1.find_first_of("0123456789");
 if (index1 == string::npos)            {SendCodingError("ALI_Variables::LoadPortData() - Invalid Port Number"); return false; }
 stringTemp2.assign(stringTemp1, index1, string::npos );

 // load the port
 if (intArg > intMAX_ALI_PORT_PAIRS) {SendCodingError("ALI_Variables::LoadPortData() - MAX Port Pairs Exceeded"); return false; }
 switch (i)
  {
   case 1: case 2:
           this->ALI_PORT_VARS[intArg] [i].REMOTE_IP_ADDRESS.stringAddress = IPAddress.stringAddress;
           this->ALI_PORT_VARS[intArg] [i].intREMOTE_PORT_NUMBER = char2int(stringTemp2.c_str() );
           this->ALI_PORT_VARS[intArg] [i].intLOCAL_PORT_NUMBER = ALI_PORT_VARS[intArg] [i].intREMOTE_PORT_NUMBER;
           break;
   default:
           this->ALI_SERVICE_PORT_VARS[intArg].REMOTE_IP_ADDRESS.stringAddress = IPAddress.stringAddress;
           this->ALI_SERVICE_PORT_VARS[intArg].intREMOTE_PORT_NUMBER = char2int(stringTemp2.c_str() );
           this->ALI_SERVICE_PORT_VARS[intArg].intLOCAL_PORT_NUMBER = ALI_SERVICE_PORT_VARS[intArg].intREMOTE_PORT_NUMBER;
           break;
  }
       
 return true;
}

bool ALI_Variables::LoadPortConnectionType( int intArg, string charArg , string stringArg)
{
 int                   	i;
 Port_Connection_Type  	ePortConnectionType;

 Port_Connection_Type 	ConnectionType(string strInput);

 if      (charArg == "A") {i = 1;}
 else if (charArg == "B") {i = 2;}
 else if (charArg == "S") {i = 3;}
 else                     {i = 0;}

 ePortConnectionType = ConnectionType(stringArg);

 if (intArg > intMAX_ALI_PORT_PAIRS) {return false;}
 switch (i)
  {
   case 1: case 2:
           this->ALI_PORT_VARS[intArg] [i].ePortConnectionType = ePortConnectionType; break;
   default:
           this->ALI_SERVICE_PORT_VARS[intArg].ePortConnectionType = ePortConnectionType; break;
  }

 return true;
}

//################################################################### CAD_Variables ####################################################################################################

//Constructor
CAD_Variables::CAD_Variables() {
 intNUM_CAD_PORTS = 0;
 boolCAD_EXISTS   = false;
 boolCAD_ERASE    = false;
 intCAD_PORT_DOWN_REMINDER_SEC   = 1800;
 intCAD_PORT_DOWN_THRESHOLD_SEC  = 180;
 boolSEND_TO_CAD_ON_CONFERENCE   = false;
 boolSEND_TO_CAD_ON_TAKE_CONTROL = true;
 
 for (unsigned int i = 0; i <= NUM_CAD_PORTS_MAX; i++){
   CAD_PORT_VARS[i].fClear();
 }   
}

//copy constructor
CAD_Variables::CAD_Variables(const CAD_Variables *a) {
 intNUM_CAD_PORTS                = a->intNUM_CAD_PORTS;
 boolCAD_EXISTS                  = a->boolCAD_EXISTS;
 boolCAD_ERASE                   = a->boolCAD_ERASE;
 intCAD_PORT_DOWN_REMINDER_SEC   = a->intCAD_PORT_DOWN_REMINDER_SEC;
 intCAD_PORT_DOWN_THRESHOLD_SEC  = a->intCAD_PORT_DOWN_THRESHOLD_SEC;
 boolSEND_TO_CAD_ON_CONFERENCE   = a->boolSEND_TO_CAD_ON_CONFERENCE;
 boolSEND_TO_CAD_ON_TAKE_CONTROL = a->boolSEND_TO_CAD_ON_TAKE_CONTROL;
 
 for (unsigned int i = 0; i <= NUM_CAD_PORTS_MAX; i++){
   CAD_PORT_VARS[i] = a->CAD_PORT_VARS[i];
 }         
}

CAD_Variables& CAD_Variables::operator=(const CAD_Variables& a) {
 if (&a == this ) {return *this;}
 intNUM_CAD_PORTS                = a.intNUM_CAD_PORTS;
 boolCAD_EXISTS                  = a.boolCAD_EXISTS;
 boolCAD_ERASE                   = a.boolCAD_ERASE;
 intCAD_PORT_DOWN_REMINDER_SEC   = a.intCAD_PORT_DOWN_REMINDER_SEC;
 intCAD_PORT_DOWN_THRESHOLD_SEC  = a.intCAD_PORT_DOWN_THRESHOLD_SEC;
 boolSEND_TO_CAD_ON_CONFERENCE   = a.boolSEND_TO_CAD_ON_CONFERENCE;
 boolSEND_TO_CAD_ON_TAKE_CONTROL = a.boolSEND_TO_CAD_ON_TAKE_CONTROL;
 
 for (unsigned int i = 0; i <= NUM_CAD_PORTS_MAX; i++){
   CAD_PORT_VARS[i] = a.CAD_PORT_VARS[i];
 }         

 boolRestartLegacyCADPorts      = a.boolRestartLegacyCADPorts;
 intOldNumberOfLegacyCADports   = a.intOldNumberOfLegacyCADports;

 return *this;
}

bool CAD_Variables::fCompare(const CAD_Variables& a) const  {
 bool boolRunningCheck = true;
 bool boolPortRestart  = false;
 
 //note does not check all fields just those in the ini file
 boolRunningCheck = boolRunningCheck && (this->boolCAD_EXISTS                   == a.boolCAD_EXISTS);
 boolRunningCheck = boolRunningCheck && (this->boolCAD_ERASE                    == a.boolCAD_ERASE);
 boolRunningCheck = boolRunningCheck && (this->intCAD_PORT_DOWN_REMINDER_SEC    == a.intCAD_PORT_DOWN_REMINDER_SEC);
 boolRunningCheck = boolRunningCheck && (this->intCAD_PORT_DOWN_THRESHOLD_SEC   == a.intCAD_PORT_DOWN_THRESHOLD_SEC);
 boolRunningCheck = boolRunningCheck && (this->boolSEND_TO_CAD_ON_CONFERENCE    == a.boolSEND_TO_CAD_ON_CONFERENCE);
 boolRunningCheck = boolRunningCheck && (this->boolSEND_TO_CAD_ON_TAKE_CONTROL  == a.boolSEND_TO_CAD_ON_TAKE_CONTROL);


  //Require restart of Ports for new # of ports .........
 this->boolRestartLegacyCADPorts      = (!(this->intNUM_CAD_PORTS   == a.intNUM_CAD_PORTS));
 a.boolRestartLegacyCADPorts          = this->boolRestartLegacyCADPorts;
 this->intOldNumberOfLegacyCADports   = this->intNUM_CAD_PORTS;
 a.intOldNumberOfLegacyCADports       = this->intNUM_CAD_PORTS;
 boolRunningCheck = boolRunningCheck && (this->intNUM_CAD_PORTS     == a.intNUM_CAD_PORTS);

 if  (this->boolRestartLegacyCADPorts) {return false;}
 
 // number of ports are equal (int (i) will not blow up !!!!)
 for ( int i = 0; i <=  this->intNUM_CAD_PORTS; i++)   { 
  boolRunningCheck = boolRunningCheck && (this->CAD_PORT_VARS[i].fCompare(a.CAD_PORT_VARS[i])); 
  
  boolPortRestart = boolPortRestart || (this->CAD_PORT_VARS[i].intREMOTE_PORT_NUMBER  != a.CAD_PORT_VARS[i].intREMOTE_PORT_NUMBER);
  boolPortRestart = boolPortRestart || (this->CAD_PORT_VARS[i].intLOCAL_PORT_NUMBER   != a.CAD_PORT_VARS[i].intLOCAL_PORT_NUMBER);
  boolPortRestart = boolPortRestart || (!this->CAD_PORT_VARS[i].REMOTE_IP_ADDRESS.fCompare(a.CAD_PORT_VARS[i].REMOTE_IP_ADDRESS));
  boolPortRestart = boolPortRestart || (this->CAD_PORT_VARS[i].ePortConnectionType    != a.CAD_PORT_VARS[i].ePortConnectionType);

  if (boolPortRestart) {
      this->boolRestartLegacyCADPorts = a.boolRestartLegacyCADPorts = true;  }
 }
 
 return boolRunningCheck;
}
 
void CAD_Variables::Set_GLobal_VARS() {

 extern int                           intNUM_CAD_PORTS;
 extern bool                          boolCAD_EXISTS;
 extern bool                          boolCAD_ERASE;
 extern int                           intCAD_PORT_DOWN_REMINDER_SEC;
 extern int                           intCAD_PORT_DOWN_THRESHOLD_SEC;
 extern bool                          boolSEND_TO_CAD_ON_CONFERENCE;
 extern bool                          boolSEND_TO_CAD_ON_TAKE_CONTROL;
 
 extern ExperientCommPort             CADPort[NUM_CAD_PORTS_MAX+1];
 
 intNUM_CAD_PORTS                   = this->intNUM_CAD_PORTS;
 boolCAD_EXISTS                     = this->boolCAD_EXISTS;
 boolCAD_ERASE                      = this->boolCAD_ERASE;
 intCAD_PORT_DOWN_REMINDER_SEC      = this->intCAD_PORT_DOWN_REMINDER_SEC;
 intCAD_PORT_DOWN_THRESHOLD_SEC     = this->intCAD_PORT_DOWN_THRESHOLD_SEC;
 boolSEND_TO_CAD_ON_CONFERENCE      = this->boolSEND_TO_CAD_ON_CONFERENCE;
 boolSEND_TO_CAD_ON_TAKE_CONTROL    = this->boolSEND_TO_CAD_ON_TAKE_CONTROL;
 
 /*
 for (unsigned int i = 0; i <=  intMAX_ALI_PORT_PAIRS; i++)   { 
   ALIServicePort[i].ePortConnectionType 	= this->ALIServicePort[i].ePortConnectionType;
   ALIServicePort[i].eALIportProtocol 		= this->ALIServicePort[i].eALIportProtocol;
   ALIServicePort[i].intLocalPort 		= this->ALIServicePort[i].intLocalPort;
   ALIServicePort[i].RemoteIPAddress 		= this->ALIServicePort[i].RemoteIPAddress;
   ALIServicePort[i].intRemotePortNumber 	= this->ALIServicePort[i].intRemotePortNumber;
   ALIServicePort[i].strNotes 			= this->ALIServicePort[i].strNotes;
   ALIServicePort[i].boolPhantomPort 		= this->ALIServicePort[i].boolPhantomPort;
   ALIServicePort[i].eRequestKeyFormat 		= this->ALIServicePort[i].eRequestKeyFormat;// set by ALI steering fLoadKeyFormats()
 }
 */
 return;
}

bool CAD_Variables::fLoadINIfile() {
 string 	strMessage="CAD_Variables::fLoadINIfile() -Unable to load ini file -> ";
 strMessage +=  charINI_FILE_PATH_AND_NAME;
 
 this->ini.SetUnicode(true);
 this->ini.SetMultiKey(true);
 this->ini.SetMultiLine(true);

 SI_Error rc = this->ini.LoadFile(charINI_FILE_PATH_AND_NAME);
 if (rc < 0) { SendCodingError(strMessage); return false;}
 return true;
}

bool CAD_Variables::fLoadPortDownThreshold() {
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("CAD", CAD_INI_KEY_5, DEFAULT_VALUE_CAD_INI_KEY_5 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intCAD_PORT_DOWN_THRESHOLD_SEC = char2int(strNumber.c_str());
 return true;
}

bool CAD_Variables::fLoadPortDownReminder() {
 string strNumber;
 size_t found;

  strNumber = ini.GetValue("CAD", CAD_INI_KEY_6, DEFAULT_VALUE_CAD_INI_KEY_6 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->intCAD_PORT_DOWN_REMINDER_SEC = char2int(strNumber.c_str());
 return true;
}

bool CAD_Variables::fLoadCADexists() {
    
 return char2bool(this->boolCAD_EXISTS, ini.GetValue("CAD", CAD_INI_KEY_1, DEFAULT_VALUE_CAD_INI_KEY_1 ));
}

bool CAD_Variables::fLoadCADsendToConference() {
    
 return char2bool(this->boolSEND_TO_CAD_ON_CONFERENCE, ini.GetValue("CAD", CAD_INI_KEY_7, DEFAULT_VALUE_CAD_INI_KEY_7 ));
}


bool CAD_Variables::fLoadCADtakeControl() {
    
 return char2bool(this->boolSEND_TO_CAD_ON_TAKE_CONTROL, ini.GetValue("CAD", CAD_INI_KEY_8, DEFAULT_VALUE_CAD_INI_KEY_8 ));
}

bool CAD_Variables::LoadPortData( int intArg, string charArg , string stringArg ) {

 string                         stringTemp1 = "";
 string                         stringTemp2 = "";
 size_t                         index1,index2,index3;  
 int                            i,j;     
 IP_Address                     IPAddress;
 string                         stringPort;
 string                         stringPortSuffix="";
 
 extern string truefalse(bool boolARG);

 
 stringPort  = int2strLZ(intArg);
 stringTemp1 = stringArg;
 index1      = stringTemp1.find_first_of("0123456789.");
 index2      = stringTemp1.find_first_of(",");

 
  cout << "CAD VAR LOAD PORT DATA BEGIN" << endl;
 //set IP address
 if ((index1 == string::npos)||(index2 == string::npos)){SendCodingError("CAD_Variables::LoadPortData() - No number found or no comma found"); return false; }
 IPAddress.stringAddress.assign(stringTemp1, index1 , (index2-index1) );
 // strip trailing blanks ...
 j = IPAddress.stringAddress.length()-1;
 while ((IPAddress.stringAddress[j] == ' ')&& (j > 0))   {
   IPAddress.stringAddress.erase(j);
   j--;
 }
 cout << "B" << endl;
 index3 =  IPAddress.stringAddress.find_first_not_of("0123456789.");
 if (index3 != string::npos)            {SendCodingError("CAD_Variables::LoadPortData() - Invalid IP address"); return false; }
 if (!IPAddress.fIsValid_IP_Address())  {SendCodingError("CAD_Variables::LoadPortData() - Invalid IP address"); return false; }

 cout << "C" << endl;
  // load the port number
 stringTemp1.erase(index1 ,(index2-index1));
 stringTemp2 = "";
 index1 = stringTemp1.find_first_of("0123456789");
 if (index1 == string::npos)            {SendCodingError("CAD_Variables::LoadPortData() - Invalid Port Number"); return false; }
 stringTemp2.assign(stringTemp1, index1, string::npos );

 cout << "D" << endl;
  
 if (intArg > NUM_CAD_PORTS_MAX) {SendCodingError("CAD_Variables::LoadPortData() - NUM_CAD_PORTS_MAX exceeded!"); return false; }
 cout << "E" << endl;
 this->CAD_PORT_VARS[intArg].REMOTE_IP_ADDRESS.stringAddress = IPAddress.stringAddress;
 this->CAD_PORT_VARS[intArg].intREMOTE_PORT_NUMBER = char2int(stringTemp2.c_str() );
 this->CAD_PORT_VARS[intArg].intLOCAL_PORT_NUMBER = this->CAD_PORT_VARS[intArg].intREMOTE_PORT_NUMBER;
 this->CAD_PORT_VARS[intArg].ePortConnectionType = UDP;
 cout << "CAD VAR LOAD PORT DATA END" << endl;
 return true;   
}

bool CAD_Variables::fLoadCADPorts() {
   //this->ini should be loaded prior to call !
 string CAD_Key;
 string StringData;
 int    i;
 int    intRC;

 extern string truefalse(bool boolARG);

 extern string  Create_INI_Key(string stringArg1, int intArg1, string stringArg2="", int intArg2=0, string stringArg3 = "", int intArg3=0, string stringArg4="", int intArg4=0); 
  
  i = 1;
  do  {
     CAD_Key = Create_INI_Key("Port", i, "IP",0,"Address");
     StringData = this->ini.GetValue("CAD", CAD_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     if (!this->LoadPortData( i, "" , StringData )) {
      Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_016, int2str(i) );
     }
     CAD_Key = Create_INI_Key("Port", i, "Connection_Note");
     this->CAD_PORT_VARS[i].strNotes = ini.GetValue("CAD", CAD_Key.c_str(), "None." );
     i++;
    
   }while ( StringData != "");
   
   // set number of CAD Ports
   this->intNUM_CAD_PORTS = i-1;
   for (int y = 1; y <=  this->intNUM_CAD_PORTS; y++)    {
    CAD_Key = Create_INI_Key("Port", y, "Connection",0,"Type");
    StringData = this->ini.GetValue("CAD", CAD_Key.c_str(), "" );
    if      (StringData  == "TCP") {this->CAD_PORT_VARS[y].ePortConnectionType = TCP;}
    else                           {this->CAD_PORT_VARS[y].ePortConnectionType = UDP;}

    CAD_Key = Create_INI_Key("Port", y, "Positions");     
    StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
    if (StringData  == "") {continue;}
     
    this->CAD_PORT_VARS[y].WorkstationGroup.strGroupName = "CAD"+int2strLZ(y);
    this->CAD_PORT_VARS[y].WorkstationGroup.fLoadWorkstations(StringData);
   }
 
   for (int y = 1; y <=  this->intNUM_CAD_PORTS; y++)    {
    CAD_Key = Create_INI_Key("Port", y, "NENA",0,"CAD_Header"); 
    
    if(!char2bool( this->CAD_PORT_VARS[y].boolNENA_CADheader, this->ini.GetValue("CAD", CAD_Key.c_str(), "true" ))){   
     SendCodingError("CAD_Variables::fLoadCADPorts() - CAD_PORT_VARS[y].boolNENA_CADheader!"); return false;   
    }
   }

   for (int y = 1; y <=  this->intNUM_CAD_PORTS; y++)    {
     CAD_Key = Create_INI_Key("Port", y, "Position",0,"Aliases"); 
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );     
     if (StringData  == "") {continue;}
     this->CAD_PORT_VARS[y].PositionAliases.fLoadPositionAliases(StringData);
   }


 //load CAD Port Heartbeat & ACK Rules
   i = 1;
   do
    {
     CAD_Key = Create_INI_Key("Port", i, "Rule",0,"Code");
     StringData = this->ini.GetValue("CAD", CAD_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     switch (char2int(StringData.c_str()))
       {
        case 0:
          this->CAD_PORT_VARS[i].boolHeartbeatRequired    = false;
          this->CAD_PORT_VARS[i].boolRecordACKRequired    = false;
          this->CAD_PORT_VARS[i].boolHeartBeatACKrequired = false;
          break;
        case 1:
          this->CAD_PORT_VARS[i].boolHeartbeatRequired    = true;
          this->CAD_PORT_VARS[i].boolRecordACKRequired    = false;
          this->CAD_PORT_VARS[i].boolHeartBeatACKrequired = false;     
          break;
        case 2:
          this->CAD_PORT_VARS[i].boolHeartbeatRequired    = true;
          this->CAD_PORT_VARS[i].boolRecordACKRequired    = true;
          this->CAD_PORT_VARS[i].boolHeartBeatACKrequired = true;     
          break;
        case 3:
          this->CAD_PORT_VARS[i].boolHeartbeatRequired    = false;
          this->CAD_PORT_VARS[i].boolRecordACKRequired    = true;
          this->CAD_PORT_VARS[i].boolHeartBeatACKrequired = false;     
          break;
        case 4:
          this->CAD_PORT_VARS[i].boolHeartbeatRequired    = true;
          this->CAD_PORT_VARS[i].boolRecordACKRequired    = true;
          this->CAD_PORT_VARS[i].boolHeartBeatACKrequired = false;     
          break;
        case 5:
          this->CAD_PORT_VARS[i].boolHeartbeatRequired    = true;
          this->CAD_PORT_VARS[i].boolRecordACKRequired    = false;
          this->CAD_PORT_VARS[i].boolHeartBeatACKrequired = true;
          break;

        default:
          SendCodingError("CAD_Variables::fLoadCADPorts() - Invalid Rule Code! "+int2str(i)); 
          return false;   

       }// end switch
     i++;
    
   }while ( StringData != "");

   if (this->intNUM_CAD_PORTS != (i-1)){
    SendCodingError("CAD_Variables::fLoadCADPorts() - Mismatch between CadPorts and rules defined!"); 
    return false; 
   }
   // Load Send CAD Erase Msg boolean 
   for (int y=1; y <= i; y++){this->CAD_PORT_VARS[y].boolSendCADEraseMsg = false;}          // default value = false
   i = 1;
   do   {
    CAD_Key = Create_INI_Key("Port", i, "Erase",0,"Msg");
    StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
    
    if (StringData  == "") {continue;}
    
    if (i > NUM_CAD_PORTS_MAX) {
     SendCodingError("CAD_Variables::fLoadCADPorts() - NUM_CAD_PORTS_MAX exceeded!"); 
     return false; 
    }
  
    if(!char2bool(this->CAD_PORT_VARS[i].boolSendCADEraseMsg, StringData)){
     SendCodingError("CAD_Variables::fLoadCADPorts() - Invalid Boolean boolSendCADEraseMsg! "); 
     return false;   
    }
    // set Global button if any are true ...
    if (this->CAD_PORT_VARS[i].boolSendCADEraseMsg) {this->boolCAD_ERASE = true;}
    i++;
    
   } while ( StringData != "");

   // Load Erase first CR var
   for (int y=1; y <= i; y++){this->CAD_PORT_VARS[y].boolCAD_EraseFirstALI_CR = false;}          // default value = false

   i = 1;
   do   {
    CAD_Key = Create_INI_Key("Port", i, "Remove_First_ALI_CR");
    StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
    if (StringData  == "") {continue;}
    if (i > NUM_CAD_PORTS_MAX) {
     SendCodingError("CAD_Variables::fLoadCADPorts() - NUM_CAD_PORTS_MAX exceeded!"); 
     return false; 
    }
    if(!char2bool(this->CAD_PORT_VARS[i].boolCAD_EraseFirstALI_CR, StringData)){
     SendCodingError("CAD_Variables::fLoadCADPorts() - Invalid Boolean boolCAD_EraseFirstALI_CR! "); 
     return false;   
    }
    i++;
    
   }while ( StringData != "");

   // check for "all" Databases CC Email addresses
   StringData = ini.GetValue("CAD", KRN_INI_KEY_13, DEFAULT_VALUE_KRN_INI_KEY_13);
   // load email address into all the ports or set to false
   for (int x = 1; x <= this->intNUM_CAD_PORTS; x++){this->CAD_PORT_VARS[x].fLoad_Vendor_Email_Address(StringData);}  

   // check for specific port cc vendor email addresses
   for (int x = 1; x <= intNUM_CAD_PORTS; x++)    {
     CAD_Key = Create_INI_Key("Port", x, "CC_Alarm_Email_Address");
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), DEFAULT_VALUE_KRN_INI_KEY_13 );
     if (StringData  == DEFAULT_VALUE_KRN_INI_KEY_13) {continue;}
     this->CAD_PORT_VARS[x].fLoad_Vendor_Email_Address(StringData);   
   }


 return true;   
}



//################################################################### Telephone_Variables ####################################################################################################

bool Telephone_Variables::fLoadINIfile()
{
 this->ini.SetUnicode(true);
 this->ini.SetMultiKey(true);
 this->ini.SetMultiLine(true);

 SI_Error rc = this->ini.LoadFile(charINI_FILE_PATH_AND_NAME);
 if (rc < 0) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_003, charINI_FILE_PATH_AND_NAME);}
 return true;
}

bool Telephone_Variables::fLoadRCCremotePort()
{
 intRCC_REMOTE_TCP_PORT_NUMBER = char2int (ini.GetValue("TELEPHONE", RCC_INI_KEY_1, DEFAULT_VALUE_RCC_INI_KEY_1 ));

 return true;
}

bool Telephone_Variables::fLoadRCCdefaultContactPositions()
{
 return char2bool(boolRCC_DEFAULT_CONTACT_POSITIONS, ini.GetValue("TELEPHONE", RCC_INI_KEY_2, DEFAULT_VALUE_RCC_INI_KEY_2 ));

}

bool Telephone_Variables::fLoadCTIoption()
{
  if(!char2bool(boolUSE_POLYCOM_CTI, ini.GetValue("TELEPHONE", CTI_INI_KEY_1, DEFAULT_VALUE_CTI_INI_KEY_1 ))) {return false;}

  boolSHOW_ATTENDED_XFER_BUTTON   = boolSHOW_VOLUME_BUTTON = boolSHOW_HANGUP_BUTTON = boolSHOW_HOLD_BUTTON = boolSHOW_BARGE_BUTTON = 
  boolSHOW_ANSWER_BUTTON = boolSHOW_DIAL_PAD_BUTTON = boolSHOW_SPEED_DIAL_BUTTON = boolSHOW_CALLBACK_BUTTON = boolUSE_POLYCOM_CTI;
 return true;
}

bool Telephone_Variables::fLoadCTIhttpPort()
{
  intPOLYCOM_CTI_HTTP_PORT        = char2int (ini.GetValue("TELEPHONE", CTI_INI_KEY_2, DEFAULT_VALUE_CTI_INI_KEY_2 ));
 return true;
}

bool Telephone_Variables::fLoadDialNineForOutboundCallsRule()
{
  return char2bool(boolADD_NINE_FOR_OUTBOUND_CALLS, ini.GetValue("TELEPHONE", CTI_INI_KEY_3, DEFAULT_VALUE_CTI_INI_KEY_3 ));
}


bool Telephone_Variables::fLoadDeviceDownThreshold()
{
  intDEVICE_DOWN_THRESHOLD_SEC = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_1, DEFAULT_VALUE_TELEPHONE_INI_KEY_1 ));
 return true;
}

bool Telephone_Variables::fLoadDeviceDownReminderThreshold()
{
  intDEVICE_DOWN_REMINDER_SEC  = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_2, DEFAULT_VALUE_TELEPHONE_INI_KEY_2 ));
 return true;
}

bool Telephone_Variables::fLoadDefaultCallerIDname()
{
  strCALLER_ID_NAME            =  ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_3, DEFAULT_VALUE_TELEPHONE_INI_KEY_3 );
 return true;
}

bool Telephone_Variables::fLoadDefaultCallerIDnumber()
{
  strCALLER_ID_NUMBER          =  ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_4, DEFAULT_VALUE_TELEPHONE_INI_KEY_4 );
 return true;
}


bool Telephone_Variables::fLoadLocalCallRules()
{
 CSimpleIniA::TNamesDepend Area_Code_Values, NXX_Values; 
 CSimpleIniA::TNamesDepend::const_iterator l;
          
 ini.GetAllValues("TELEPHONE", TELEPHONE_INI_KEY_5, Area_Code_Values);
 ini.GetAllValues("TELEPHONE", TELEPHONE_INI_KEY_6, NXX_Values);

  l = Area_Code_Values.begin();

  while (l != Area_Code_Values.end())
   { 
    LocalCallRules.LocalAreaCode.push_back( l->pItem);
    ++l;
   }

  l = NXX_Values.begin();

  while (l != NXX_Values.end())
   { 
    LocalCallRules.LocalNXXprefix.push_back( l->pItem);
    ++l;
   }

 return true;
}

bool Telephone_Variables::fLoadTelephoneDevices()
{
 int i, iCount;
 string StringData;
 string StringURI;
 string Phones_Key;
   //cout << "SHOULD NOT BE CALLED YET !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
   i = 1; iCount = 0;
   do
   {
    Phones_Key = Create_INI_Key("Phone_", i, "Device_Name");
    StringData = ini.GetValue("TELEPHONE", Phones_Key.c_str(), "" );
    if (StringData  == "") {continue;}
    if (i > intNUM_WRK_STATIONS) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_037 );}   
    if(!TelephoneEquipment.fLoadDevice(i, StringData, StringURI)) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_065);}
    Phones_Key = Create_INI_Key("Phone_", i, "Device_MAC_Addr");
    StringData = ini.GetValue("TELEPHONE", Phones_Key.c_str(), "" );
    if (!TelephoneEquipment.fLoadMACaddress(i, StringData)) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_064);} 
    Phones_Key = Create_INI_Key("Phone_", i, "Device_Type");
    StringData = ini.GetValue("TELEPHONE", Phones_Key.c_str(), "" );
    if (!TelephoneEquipment.fLoadDeviceType(i, StringData)) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066);} 
    Phones_Key = Create_INI_Key("Phone_", i, "Device_IPaddress");
    StringData = ini.GetValue("TELEPHONE", Phones_Key.c_str(), "" );
    if (!TelephoneEquipment.fLoadIPaddress(i, StringData)) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066);} 
    i++; iCount++;   
   }while ( StringData != "");
   TelephoneEquipment.NumberofTelephones = iCount; 


 return true;
}

//################################################################# Initialize_Global_Variables ######################################################################################################
// fred

//constructor
Initialize_Global_Variables::Initialize_Global_Variables()
{
/*
 MainNode=XMLNode::createXMLTopNode("xml",TRUE);
 XMLNode NodeA = MainNode.addChild(XML_NODE_PHONEBOOK);
 XMLNode NodeB = NodeA.addChild(XML_NODE_PHONEBOOK_SETUP);
 NodeB.addAttribute(XML_FIELD_TRANSFER_DEFAULT_GROUP, XML_VALUE_ALL);
 NodeB.addChild(XML_NODE_PHONEBOOK_LIST);
 MainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 MainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
*/

 sem_init(&sem_tMutexMasterNode,0,1);
}

bool Initialize_Global_Variables::fReloadInitFileADV() {

 ADV_Variables        	newADVinitVarible;
 MessageClass		objMessage;

// //cout << "RELOAD ADV" << endl;
 if (!this->fLoadADVvariables(newADVinitVarible)) {return false;}
 //Check for ADV changes (if they are not the same)
 if (!(this->ADVinitVariable.fCompare(newADVinitVarible)))  {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 186, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_186); 
   enQueue_Message(MAIN,objMessage);  
 }
 else {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 187, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_187); 
   enQueue_Message(MAIN,objMessage);   
  return false;
 }
 //make changes ....
 this->ADVinitVariable = newADVinitVarible;
 this->ADVinitVariable.Set_GLobal_VARS();


return true;
}

bool Initialize_Global_Variables::fReloadInitFileDSB() {

 DSB_Variables        	newDSBinitVarible;
 MessageClass		objMessage;

// //cout << "RELOAD DSB" << endl;
 if (!this->fLoadDSBvariables(newDSBinitVarible)) {return false;}

 //Check for DSB changes (if they are not the same)
 if (!(this->DSBinitVariable.fCompare(newDSBinitVarible)))  {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 659, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_659); 
   enQueue_Message(DSB,objMessage);  
 }
 else {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 660, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_660); 
   enQueue_Message(DSB,objMessage);   
  return false;
 }
// //cout << "DSB RESTART CHECK" << endl;
// //cout << this->DSBinitVariable.boolRestartDashboardPort << endl;
// //cout << newDSBinitVarible.boolRestartDashboardPort << endl;
// //cout << newDSBinitVarible.DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC << endl;
 //make changes ....
 this->DSBinitVariable = newDSBinitVarible;
 //cout << this->DSBinitVariable.DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC << endl;
 this->DSBinitVariable.Set_GLobal_VARS();
 

 if (this->DSBinitVariable.boolRestartDashboardPort){
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 661, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_661,
  int2str(this->DSBinitVariable.intOldNumberOfDSBports), int2str(this->DSBinitVariable.intNUM_DSB_PORTS));                              
  enQueue_Message(DSB,objMessage);
 }

 return true; 



}

bool Initialize_Global_Variables::fReloadInitFileANI()
{
 ANI_Variables        	newANIinitVarible;
 MessageClass		objMessage;
 
// //cout << "reload ANI" << endl;
 //Load new ANI Variables 
 if (!this->fLoadANIvariables(newANIinitVarible)) {return false;}

// //cout << "compare check ANI -> " << this->ANIinitVariable.fCompare(newANIinitVarible) << endl;
// //cout << "Number ANI ports was " << this->ANIinitVariable.intNUM_ANI_PORTS << " now is " << newANIinitVarible.intNUM_ANI_PORTS << endl;

 //Check for ANI changes (if they are not the same)
 if (!(this->ANIinitVariable.fCompare(newANIinitVarible)))
  {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 317, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_317); 
   enQueue_Message(ANI,objMessage);  
  }
 else {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 319, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_319); 
   enQueue_Message(ANI,objMessage);   
  return false;
 }

 // make change  
 this->ANIinitVariable = newANIinitVarible;
 this->ANIinitVariable.Set_GLobal_VARS();

 if (this->ANIinitVariable.boolRestartANIports||this->ANIinitVariable.boolRestartFreeswitchCLIport){
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 313, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_313,
  int2str(this->ANIinitVariable.intOldNumberOfANIports), int2str(this->ANIinitVariable.intNUM_ANI_PORTS));                              
  enQueue_Message(ANI,objMessage);
 }

 return true;
}

bool Initialize_Global_Variables::fReloadInitFileCAD() {
    
 CAD_Variables  newCADinitVarible;
 MessageClass   objMessage; 

 cout << "RELOAD CAD VARIABLES" << endl;
 if (!this->fLoadCADvariables(newCADinitVarible)) {return false;}
 
// cout << "compare check CAD -> " << this->CADinitVariable.fCompare(newCADinitVarible) << endl;
// cout << "Number CAD ports was " << this->CADinitVariable.intNUM_CAD_PORTS << " now is " << newCADinitVarible.intNUM_CAD_PORTS << endl;

 if (!(this->CADinitVariable.fCompare(newCADinitVarible)))  {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 444, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_444); 
   enQueue_Message(CAD,objMessage);  
 }
 else {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 445, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_445); 
   enQueue_Message(CAD,objMessage);   
  return false;
 }

 // make change  
 this->CADinitVariable = newCADinitVarible;
 this->CADinitVariable.Set_GLobal_VARS();

 if (this->CADinitVariable.boolRestartLegacyCADPorts){
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 446, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_446,
  int2str(this->CADinitVariable.intOldNumberOfLegacyCADports), int2str(this->CADinitVariable.intNUM_CAD_PORTS));                              
  enQueue_Message(CAD,objMessage);
 }
 
 return true; 
}

bool Initialize_Global_Variables::fLoadCADvariables(CAD_Variables &CADvar)  {
    
  string strErrorString = "Initialize_Global_Variables::fLoadCADvariables() Unable to load ";
  bool   boolReturnValue = true;

  // Get CAD INI Variables
   if (!CADvar.fLoadINIfile()) {
    strErrorString = "Initialize_Global_Variables::fLoadCADvariables() Unable to load file: ";
    strErrorString += charINI_FILE_PATH_AND_NAME;
    SendCodingError( strErrorString );
    return false;
   }
  
  if (!CADvar.fLoadCADexists())                 {strErrorString += "boolCADexists ";                      boolReturnValue = false;}
  if (CADvar.boolCAD_EXISTS) {
 
   
   if (!CADvar.fLoadPortDownThreshold())         {strErrorString += "intCAD_PORT_DOWN_THRESHOLD_SEC";      boolReturnValue = false;}
   if (!CADvar.fLoadPortDownReminder())          {strErrorString += "intCAD_PORT_DOWN_REMINDER_SEC";       boolReturnValue = false;}
   if (!CADvar.fLoadCADsendToConference())       {strErrorString += "boolSEND_TO_CAD_ON_CONFERENCE";       boolReturnValue = false;}
   if (!CADvar.fLoadCADtakeControl())            {strErrorString += "boolSEND_TO_CAD_ON_TAKE_CONTROL";     boolReturnValue = false;}
   if (!CADvar.fLoadCADPorts())                  {strErrorString += "fLoadCADPorts()";                     boolReturnValue = false;}

  } 
 return  boolReturnValue; 
}

bool Initialize_Global_Variables::fReloadInitFileALI()
{
 ALI_Variables  newALIinitVarible;
 MessageClass   objMessage;

 //Load new ALI Variables 
 if (!this->fLoadALIvariables(newALIinitVarible)) {return false;}
 newALIinitVarible.fLoadKeyFormats();   // must load for check to pass
 ////cout << "compare check ALI -> " << this->ALIinitVariable.fCompare(newALIinitVarible) << endl;
 ////cout << "Number ALI Legacy ports was " << this->ALIinitVariable.intNUM_ALI_PORT_PAIRS << " now is " << newALIinitVarible.intNUM_ALI_PORT_PAIRS << endl;
// //cout << "Number ALI Service ports was " << this->ALIinitVariable.intNUM_ALI_SERVICE_PORTS << " now is " << newALIinitVarible.intNUM_ALI_SERVICE_PORTS << endl;
 
 //Check for ALI changes (if they are not the same)
 if (!(this->ALIinitVariable.fCompare(newALIinitVarible)))
  {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 292, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_292); 
   enQueue_Message(ALI,objMessage);  
  }
 else {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 291, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_291); 
   enQueue_Message(ALI,objMessage);   
  return false;
 }

 // make change  
 this->ALIinitVariable = newALIinitVarible;
 this->ALIinitVariable.Set_GLobal_VARS();

 if (this->ALIinitVariable.boolRestartALILegacyPorts){
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 293, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_293, "Legacy", 
                             int2str(this->ALIinitVariable.intOldNumberOfLegacyALIports), int2str(this->ALIinitVariable.intNUM_ALI_PORT_PAIRS));                              
  enQueue_Message(ALI,objMessage);
 }

if (this->ALIinitVariable.boolRestartALIservicePorts){
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 293, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_293, "Service", 
                             int2str(this->ALIinitVariable.intOldNumberOfALIservicePorts), int2str(this->ALIinitVariable.intNUM_ALI_SERVICE_PORTS));
                              
  enQueue_Message(ALI,objMessage);
 }

 return true;
}

bool Initialize_Global_Variables::fLoadALIvariables(ALI_Variables &ALIvar)
{
  string strErrorString = "Initialize_Global_Variables::fLoadALIvariables() Unable to load ";
  bool   boolReturnValue = true;

  // Get ALI INI Variables
   if (!ALIvar.fLoadINIfile()) {
    strErrorString = "Initialize_Global_Variables::fLoadALIvariables() Unable to load file: ";
    strErrorString += charINI_FILE_PATH_AND_NAME;
    SendCodingError( strErrorString );
    return false;
   }
   if (!ALIvar.fLoadLegacyALISetting())         	{strErrorString += "boolLEGACY_ALI "; 				boolReturnValue = false;}
   if (!ALIvar.fLoadLegacyALIonlySetting())         	{strErrorString += "boolLEGACY_ALI_ONLY "; 			boolReturnValue = false;}
   if (!ALIvar.fLoadUseALIserviceServer())      	{strErrorString += "boolUSE_ALI_SERVICE_SERVER "; 		boolReturnValue = false;}
   if (!ALIvar.fLoadRebidNG911())     	     		{strErrorString += "boolREBID_NG911 "; 				boolReturnValue = false;}     
   if (!ALIvar.fLoadSendLegacyALIonSIPtransfer()) 	{strErrorString += "boolSEND_LEGACY_ALI_ON_SIP_TRANSFER ";	boolReturnValue = false;}
   if (!ALIvar.fLoadDefaultNoiseThreshold()) 		{strErrorString += "intDEFAULT_ALI_NOISE_THRESHOLD "; 		boolReturnValue = false;}
   if (ALIvar.boolREBID_NG911) {
     if (!ALIvar.fLoadWirelessRebidSec()) 		{strErrorString += "intALI_WIRELESS_AUTO_REBID_SEC "; 		boolReturnValue = false;}
     if (!ALIvar.fLoadWirelessRebidCount()) 		{strErrorString += "intALI_WIRELESS_AUTO_REBID_COUNT ";		boolReturnValue = false;}
     if (!ALIvar.fLoadRebidPhaseOneRecordsOnly()) 	{strErrorString += "boolREBID_PHASE_ONE_ALI_RECORDS_ONLY ";	boolReturnValue = false;} 
   }
   if (ALIvar.boolLEGACY_ALI) 		   
    {
     if (!ALIvar.fLoadPortDownThreshold()) 		{strErrorString += "intALI_PORT_DOWN_THRESHOLD_SEC "; 		boolReturnValue = false;}
     if (!ALIvar.fLoadPortDownReminder()) 		{strErrorString += "intALI_PORT_DOWN_REMINDER_SEC "; 		boolReturnValue = false;}
     if (!ALIvar.fLoadWirelessRebidSec()) 		{strErrorString += "intALI_WIRELESS_AUTO_REBID_SEC "; 		boolReturnValue = false;}
     if (!ALIvar.fLoadWirelessRebidCount()) 		{strErrorString += "intALI_WIRELESS_AUTO_REBID_COUNT ";		boolReturnValue = false;}
     if (!ALIvar.fLoadAllowManualBids()) 		{strErrorString += "boolALLOW_MANUAL_ALI_BIDS ";		boolReturnValue = false;}
     if (!ALIvar.fLoadIgnoreBadALI()) 			{strErrorString += "boolIGNORE_BAD_ALI ";			boolReturnValue = false;}
     if (!ALIvar.fLoadMaxALIrecordSize()) 		{strErrorString += "intMAX_ALI_RECORD_SIZE ";			boolReturnValue = false;}
     if ( ALIvar.boolUSE_ALI_SERVICE_SERVER)
      {
       if (!ALIvar.fLoadAliServicePorts()) 		{strErrorString += "ALI Service Ports Config failed ";		boolReturnValue = false;}
      }
     else
      {
       if (!ALIvar.fLoadAliPorts()) 			{strErrorString += "ALI Ports Config failed ";			boolReturnValue = false;}
       if (!ALIvar.fLoadAliDataBases()) 		{strErrorString += "ALI Databases Config failed ";		boolReturnValue = false;}
      }
     if (!ALIvar.fLoadCLIDbidRules()) 			{strErrorString += "CLID Bid Rules Config failed ";		boolReturnValue = false;}

     if (!ALIvar.fLoadVerifyALIrecords()) 		{strErrorString += "boolVERIFY_ALI_RECORD ";			boolReturnValue = false;} 
     if (!ALIvar.fLoadRebidPhaseOneRecordsOnly()) 	{strErrorString += "boolREBID_PHASE_ONE_ALI_RECORDS_ONLY ";	boolReturnValue = false;} 
     ALIvar.fLoadKeyFormats();    
    }
   if (!ALIvar.fLoadShowGoogleMap()) 		        {strErrorString += "boolSHOW_GOOGLE_MAP ";			boolReturnValue = false;}
 if (!boolReturnValue) { SendCodingError( strErrorString );}
 return boolReturnValue;
}


string Initialize_Global_Variables::fInitXMLString(int i)
{
 char*   cptrResponse = NULL;

  if (i==0) { 
   sem_wait(&this->sem_tMutexMasterNode);
   cptrResponse  = MasterNode.createXMLString();
   sem_post(&this->sem_tMutexMasterNode);
  }
  else      { cptrResponse  = MainNode[i].createXMLString();}


  if (cptrResponse != NULL)
   {
    strInitXML    = cptrResponse;
    free(cptrResponse);
   }
 cptrResponse = NULL;

 strInitXML = FindandReplaceALL(strInitXML, "/r/n" , "&#10;");
 return strInitXML;
}

bool Initialize_Global_Variables::fLoadMessageControl(bool boolFirstLoad)
{
 XMLNode 					rootNode, MessageControlNode, AdvancedNode, NodeA, NodeB, NodeC, NodeD, NodeE, NodeF;
 XMLNode 					CurrentNode, CurrentGroupNode; 
 XMLResults 					pResults;
 MessageClass  					objMessage; 
 int            				iNumberofNodes;
 bool                                           boolSupressEnable = false;
 string                                         strMessageNumberList;
 string                                         strBlockingEnable;
 string                                         strGroupName;

 if (!fexists(MESSAGE_CONTROL_CONFIG_FILE_PATH)) {return false;}

 rootNode       = XMLNode::parseFile( MESSAGE_CONTROL_CONFIG_FILE_PATH, XML_NODE_CONFIGURATION, &pResults);

 if (pResults.error) 
  {
   if (boolFirstLoad) { Console_Message("CFG |", CFG_MESSAGE_073, MESSAGE_CONTROL_CONFIG_FILE_PATH, XMLNode::getError(pResults.error) );}
   else               { objMessage.fMessage_Create(LOG_WARNING, 73, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                   objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,CFG_MESSAGE_073, 
                                                   MESSAGE_CONTROL_CONFIG_FILE_PATH, XMLNode::getError(pResults.error) );  
                        enQueue_Message(MAIN,objMessage);
                      }
   return false;
  }

 AdvancedNode       =  rootNode.getChildNode(XML_NODE_ADVANCED); 
 MessageControlNode =  AdvancedNode.getChildNode(XML_NODE_MESSAGE_CONTROL); 

 iNumberofNodes = MessageControlNode.nChildNode("group");

 for (int i = 0; i < iNumberofNodes; i++)
  {
   CurrentNode = MessageControlNode.getChildNode("group", i);
   if (CurrentNode.getAttribute(XML_FIELD_NAME))         {strGroupName = CurrentNode.getAttribute(XML_FIELD_NAME);}
   if (CurrentNode.getAttribute(XML_FIELD_BLOCKING))     {strBlockingEnable = CurrentNode.getAttribute(XML_FIELD_BLOCKING);}
   boolSupressEnable = (strBlockingEnable == XML_VALUE_ON);
   if (CurrentNode.getAttribute(XML_FIELD_MESSAGE_LIST)) {strMessageNumberList = CurrentNode.getAttribute(XML_FIELD_MESSAGE_LIST);}
   
   strMessageNumberList = RemoveAllSpaces(strMessageNumberList);
   if (!UpdateSupressMessageNumberList(strMessageNumberList, boolSupressEnable)) 
    {
     if (boolFirstLoad) { Console_Message("CFG |", LOG_MESSAGE_514, strGroupName, strMessageNumberList );}
     else               { objMessage.fMessage_Create(LOG_WARNING, 514, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                     objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LOG_MESSAGE_514, strGroupName, strMessageNumberList );  
                         enQueue_Message(MAIN,objMessage);
    }
     continue;
    }

  


  }


 return true;
}

bool Initialize_Global_Variables::fLoadLineViews(int i)
{
// int 					j;
// string                                 Line_Key, Label_Key;
// extern bool 				boolSHOW_LINE_VIEW_TAB;
// extern bool 				boolSHOW_RINGING_TAB;
// extern Telephone_Devices           	TelephoneEquipment; 


 switch (i)
  {
   case 0:


           break;
   default:


           break;




  }



 return true;
}




int ListPriority(XMLNode List, int iPriority)
{
 int 		iNodes =  List.nChildNode("List_Priority");
 string		strPriority;
 string 	strNumber = int2str(iPriority);
 XMLNode        CurrentNode;

 for (int i = 0; i < iNodes; i++)
  {
   CurrentNode = List.getChildNode("List_Priority", i);
   if(CurrentNode.getText()) {strPriority = CurrentNode.getText();}
   else                      {strPriority = "NULL";}
   if (strPriority == strNumber) {return i;}
  }
 return -1;
}

void Workstation_Variables::fShowColorPaletteUsers()
{
  cout << "Color Palette Users: " << endl;
  for (vector<string>::iterator it = vColorPaletteUsers.begin() ; it != vColorPaletteUsers.end(); ++it) 
  cout << *it << endl;
}
bool Workstation_Variables::fLoadColorPaletteUsers(bool boolFirstLoad)
{
   CSimpleIniA ini(true, true, true);
   CSimpleIniA::TNamesDepend::const_iterator l;
   CSimpleIniA::TNamesDepend                 Color_Palette_Name_Values;
   vector <string>                           vNewColorPaletteUserVector;
   string                                    strItem;
   bool                                      bListChanged = false;
   SI_Error rc = ini.LoadFile(charINI_FILE_PATH_AND_NAME);
   if (rc < 0) {return false;}
  
   ini.GetAllValues("WRK", "Show_Color_Palette_User", Color_Palette_Name_Values);
   Color_Palette_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());

   l = Color_Palette_Name_Values.begin();
 
   while (l != Color_Palette_Name_Values.end())
    { 
     strItem = l->pItem;
     vNewColorPaletteUserVector.push_back(strItem);
     ++l;
    }

  if ((boolFirstLoad)||(this->vColorPaletteUsers.size() != vNewColorPaletteUserVector.size())) {this->vColorPaletteUsers = vNewColorPaletteUserVector; return true;}

  // check if changed
  for (vector<string>::iterator it = vColorPaletteUsers.begin() ; it != vColorPaletteUsers.end(); ++it) 
   {
   strItem = *it;
   if (!( find(vNewColorPaletteUserVector.begin(), vNewColorPaletteUserVector.end(), strItem) != vNewColorPaletteUserVector.end() ) )
    {
      bListChanged = true;  
      break;
    }   
   }
  if (bListChanged)      {this->vColorPaletteUsers = vNewColorPaletteUserVector;}

  return bListChanged;
}



bool Initialize_Global_Variables::fLoadPhonebook(bool boolFirstLoad)
{
 /* Note After modifying MainNode[0] we must update the MasterNode throughout the code   */
 /* It is semaphore protected to avoid writing xml while the node is being modified      */

 XMLNode 	TransferlistNode, DisplayOrderNode, TransferTypeNode, NodeA,NodeB,NodeC,NodeD,NodeE,NodeF;
 XMLNode 	CurrentNode, currentTransferlistnode, CurrentGroupNode, GroupListNode; 
 XMLResults 	pResults;
 MessageClass  	objMessage; 
 int            iNumberofNodes, iGroupNodes, iTypeNodes;
 string         strGroupName, strGroupMatch;
 int            index;
 bool           boolDeleteContent = false;

 ////cout << "fLoadPhonebook" << endl;
// if (!fLoadNG911TranferList(boolFirstLoad)) {return false;}
 if (!fexists(PHONEBOOK_CONFIG_FILE_PATH))  {return false;}

 TransferlistNode = XMLNode::parseFile(PHONEBOOK_CONFIG_FILE_PATH, XML_NODE_TRANSFERLIST, &pResults);

 if (pResults.error) 
  {
   if (boolFirstLoad) { Console_Message("CFG |", CFG_MESSAGE_073, PHONEBOOK_CONFIG_FILE_PATH, XMLNode::getError(pResults.error) );}
   else               { objMessage.fMessage_Create(LOG_WARNING, 73, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                   objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,CFG_MESSAGE_073, 
                                                   PHONEBOOK_CONFIG_FILE_PATH, XMLNode::getError(pResults.error) );  
                        enQueue_Message(MAIN,objMessage);
                      }
   return false;
  }

 DisplayOrderNode = TransferlistNode.getChildNode(XML_NODE_DISPLAY_ORDER);

 iNumberofNodes =  TransferlistNode.nChildNode("Transfer_Name");
 iGroupNodes    =  DisplayOrderNode.nChildNode("Transfer_Group");
 iTypeNodes     =  TransferlistNode.nChildNode("ng911_transfer_type");

// //cout << "number of nodes before -> " << iNumberofNodes << endl;
// //cout << "number of Groups before -> " << iGroupNodes << endl;
// //cout << "number of NG Transfers before -> " << iTypeNodes << endl;

 if ((iTypeNodes == 0)&&(fexists(NG911_TRANSFER_CONFIG_FILE_PATH))){
  if (!fLoadNG911TranferList(boolFirstLoad)) {return false;} 
 }
 else                 {PhoneBookNode = XMLNode::emptyNode(); boolDeleteContent = true;}

 NodeA = MainNode[0].getChildNode(XML_NODE_EVENT); 
 NodeB = NodeA.getChildNode(XML_NODE_INITIALIZATION);
 NodeC = NodeB.getChildNode(XML_NODE_PHONEBOOK);
 NodeD = NodeC.getChildNode(XML_NODE_PHONEBOOK_LIST);
 NodeE = NodeC.getChildNode(XML_NODE_GROUP_LIST);

 if (boolDeleteContent) {
  NodeD.deleteNodeContent();
  NodeD = XMLNode::emptyNode();
  NodeE.deleteNodeContent(); 
  NodeE = XMLNode::emptyNode();
  NodeC.addChild(XML_NODE_GROUP_LIST);
  NodeC.addChild(XML_NODE_PHONEBOOK_LIST);
  NodeD = NodeC.getChildNode(XML_NODE_PHONEBOOK_LIST);
  NodeE = NodeC.getChildNode(XML_NODE_GROUP_LIST);
 }

 //at this point the phonebook should be empty ????

 PhoneBookNode = NodeD;
 GroupListNode = NodeE;

 for (int j = 0; j < iGroupNodes; j++)
  {
   index = ListPriority(DisplayOrderNode, j+1);
   if (index < 0)                   {return false;}
    
   //GroupEntry
   CurrentGroupNode = DisplayOrderNode.getChildNode("Transfer_Group", j);
   if(CurrentGroupNode.getText()) {strGroupName     = CurrentGroupNode.getText();}
   else                           {return false;} 
   CurrentNode = GroupListNode.addChild(XML_NODE_GROUP_ENTRY);
   CurrentNode.addAttribute(XML_FIELD_NAME,CurrentGroupNode.getText());

   CurrentGroupNode = DisplayOrderNode.getChildNode("List_Priority", j);
   if(CurrentGroupNode.getText()) {CurrentNode.addAttribute(XML_FIELD_PRIORITY,CurrentGroupNode.getText());}
   else                           {return false;}

   CurrentGroupNode = DisplayOrderNode.getChildNode("icon_file", j);
   if(CurrentGroupNode.getText()) {CurrentNode.addAttribute(XML_FIELD_ICON_FILE,CurrentGroupNode.getText());}
   else                           {CurrentNode.addAttribute(XML_FIELD_ICON_FILE, XML_VALUE_ICON_FILE_DEFAULT);}
 
   //PhonebookEntries
   for (int i = 0; i < iNumberofNodes; i++)
    {
     currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Group", i);        
     if (currentTransferlistnode.getText()) {strGroupMatch = currentTransferlistnode.getText();}
     else                                   {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebook() Group Name Not Found!");return false;}

     if (strGroupMatch == strGroupName)
      {
       CurrentNode = PhoneBookNode.addChild(XML_NODE_PHONEBOOK_ENTRY);
       currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Name", i);
       if (currentTransferlistnode.getText()) {CurrentNode.addAttribute(XML_FIELD_NAME,currentTransferlistnode.getText()); }
       else                                   {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebook() Blank Transfer Name! index->"+int2str(i));return false; } 
       currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Group", i);
       if (currentTransferlistnode.getText()) {CurrentNode.addAttribute(XML_FIELD_TRANSFER_GROUP,currentTransferlistnode.getText()); }
       else                                   {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebook() Blank Transfer Group! index->"+int2str(i));return false; }   
       currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Number", i); 
       if (currentTransferlistnode.getText()) {CurrentNode.addAttribute(XML_FIELD_TRANSFER_NUMBER,currentTransferlistnode.getText()); }
       else                                   {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebook() Blank Transfer Number! index->"+int2str(i));return false; }   
       currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Code", i);
       if (currentTransferlistnode.getText()) {CurrentNode.addAttribute(XML_FIELD_TRANSFER_CODE,currentTransferlistnode.getText()); }
       else                                   {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebook() Blank Transfer Code! index->"+int2str(i));return false; }   
              
       // Mayank added
       currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Label", i);   
       if (!currentTransferlistnode.isEmpty()) {
        if (currentTransferlistnode.getText()) {
         CurrentNode.addAttribute(XML_FIELD_TRANSFER_LABEL, currentTransferlistnode.getText());         
        }
        else {
         CurrentNode.addAttribute(XML_FIELD_TRANSFER_LABEL, XML_FIELD_TRANSFER_LABEL_DEFAULT);         
        }
       }
       else {
        CurrentNode.addAttribute(XML_FIELD_TRANSFER_LABEL, XML_FIELD_TRANSFER_LABEL_DEFAULT);         
       }   

       // these are optional 
       currentTransferlistnode = TransferlistNode.getChildNode("ng911_transfer_type", i);   
       if (!currentTransferlistnode.isEmpty()) {
        if (currentTransferlistnode.getText()) {
         CurrentNode.addAttribute(XML_FIELD_TRANSFER_TYPE, currentTransferlistnode.getText());         
        }
        else {
         CurrentNode.addAttribute(XML_FIELD_TRANSFER_TYPE, XML_VALUE_LEGACY);         
        }
       }
      else {
       CurrentNode.addAttribute(XML_FIELD_TRANSFER_TYPE, XML_VALUE_LEGACY);         
      }
    
       currentTransferlistnode = TransferlistNode.getChildNode("Speed_Dial_Action", i);   
       if (!currentTransferlistnode.isEmpty()) {
        if (currentTransferlistnode.getText()) {
         CurrentNode.addAttribute(XML_FIELD_SPEED_DIAL_ACTION, currentTransferlistnode.getText());         
        }
        else {
         CurrentNode.addAttribute(XML_FIELD_SPEED_DIAL_ACTION, XML_VALUE_SPEED_DIAL_ACTION_DEFAULT);         
        }
       }
      else {
       CurrentNode.addAttribute(XML_FIELD_SPEED_DIAL_ACTION, XML_VALUE_SPEED_DIAL_ACTION_DEFAULT);         
      }

      currentTransferlistnode = TransferlistNode.getChildNode("icon_file", i);   
       if (!currentTransferlistnode.isEmpty()) {
        if (currentTransferlistnode.getText()) {
         CurrentNode.addAttribute(XML_FIELD_ICON_FILE, currentTransferlistnode.getText());         
        }
        else {
         CurrentNode.addAttribute(XML_FIELD_ICON_FILE, XML_VALUE_ICON_FILE_DEFAULT);         
        }
       }
      else {
       CurrentNode.addAttribute(XML_FIELD_ICON_FILE, XML_VALUE_ICON_FILE_DEFAULT);         
      }   


      } // if (strGroupMatch == strGroupName) 
    }//    for (int i = 0; i < iNumberofNodes; i++)
 
  }
 // put pointer back in PhonebookNode ....
 NodeA = MainNode[0].getChildNode(XML_NODE_EVENT); 
 NodeB = NodeA.getChildNode(XML_NODE_INITIALIZATION);
 PhoneBookNode = NodeB.getChildNode(XML_NODE_PHONEBOOK);

 this->fLoadPhonebookList();  // loads list of names and numbers for LDAP and transfers.....

 //update Master Node
 sem_wait(&this->sem_tMutexMasterNode);
 this->MasterNode = this->MainNode[0].deepCopy();
 sem_post(&this->sem_tMutexMasterNode);
 return true;
}

void Initialize_Global_Variables::fLoadPhonebookList()
{
/* creates the call list vector which is used to auto fill callback names from dialed and received numbers. */
/* this function is designed to be only called from Initialize_Global_Variables::fLoadPhonebook()           */

 XMLNode 					PhonebookListNode, CurrentNode, NodeAa, NodeBb, NodeCc;
 string         				strName, strNumber;
 int             				iNumberofNodes;
 PhoneBookEntry  				objPhoneRecord;
 int           					intRC;

 extern vector <PhoneBookEntry>               vCALL_LIST; 

 NodeAa = MainNode[0].getChildNode(XML_NODE_EVENT); 
 NodeBb = NodeAa.getChildNode(XML_NODE_INITIALIZATION);
 NodeCc = NodeBb.getChildNode(XML_NODE_PHONEBOOK);

 // note the format of the phonebook is in GUI XML format
// //cout << "load phonebook list ..." << endl;
 PhonebookListNode = NodeCc.getChildNode(XML_NODE_PHONEBOOK_LIST);
 iNumberofNodes    = PhonebookListNode.nChildNode(XML_NODE_PHONEBOOK_ENTRY);
 intRC = sem_wait(&sem_tMutexCall_LIST);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexCall_LIST, "sem_wait@sem_tMutexCall_LIST in fn Initialize_Global_Variables::LoadPhonebookList()", 1);}

 vCALL_LIST.clear();
// //cout << "number of nodes -> " << iNumberofNodes << endl; 
 for (int i = 0; i < iNumberofNodes; i++){
  objPhoneRecord.fClear();
  CurrentNode = PhonebookListNode.getChildNode(XML_NODE_PHONEBOOK_ENTRY, i);
  if (CurrentNode.getAttribute(XML_FIELD_NAME))              {objPhoneRecord.strCustName = CurrentNode.getAttribute(XML_FIELD_NAME);}                       
  else                                                       {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebookList() Blank Caller Name!"); continue;}
  if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER))   {objPhoneRecord.strCallerID = CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER);}            
  else                                                       {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebookList() Blank Caller Number!"); continue;}
  if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_GROUP))    {objPhoneRecord.strTransferGroup = CurrentNode.getAttribute(XML_FIELD_TRANSFER_GROUP);}        
  else                                                       {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebookList() Blank Transfer Group!"); continue;}
  if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_CODE))     {objPhoneRecord.strTransferCode = CurrentNode.getAttribute(XML_FIELD_TRANSFER_CODE);}          
  else                                                       {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebookList() Blank Transfer Code!"); continue;}
  if (CurrentNode.getAttribute(XML_FIELD_SPEED_DIAL_ACTION)) {objPhoneRecord.strSpeedDialAction = CurrentNode.getAttribute(XML_FIELD_SPEED_DIAL_ACTION);}   
  else                                                       {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebookList() Blank SpeedDial Action!"); continue;}
  if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE)) {
   objPhoneRecord.ng911TransferType = NG911_TRANSFER_TYPE(CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE));
  }   
  else                                                       {SendCodingError("initialization.cpp - Initialize_Global_Variables::fLoadPhonebookList() Blank Transfer Type!"); continue;}
  vCALL_LIST.push_back(objPhoneRecord);
 }
 ////cout << "SIZE LEAVING IS " << vCALL_LIST.size() << endl;
 sem_post(&sem_tMutexCall_LIST);
 return;
}








bool Initialize_Global_Variables::fLoadNG911TranferList(bool boolFirstLoad)
{
 XMLNode 	TransferlistNode, NodeA,NodeB,NodeC,NodeD,NodeE,NodeF;
 XMLNode 	CurrentNode, currentTransferlistnode, GroupEntryNode; 
 XMLResults 	pResults;
 MessageClass  	objMessage; 
 int            iNumberofNodes;
 extern string  strNG911_PHONEBOOK_SIP_TRANSFER_HEADER;
 extern string  strNG911_PHONEBOOK_SIP_TRANSFER_ICON_FILE;

 // This fuction will be depreciated once the new phonebook is loaded at all sites .....
 // it is olnly called from fLoadPhonebook()

 if (!fexists(NG911_TRANSFER_CONFIG_FILE_PATH)) {return false;}

 TransferlistNode = XMLNode::parseFile(NG911_TRANSFER_CONFIG_FILE_PATH, XML_NODE_TRANSFERLIST, &pResults);

 if (pResults.error) 
  {
   if (boolFirstLoad) { Console_Message("CFG |", CFG_MESSAGE_073, NG911_TRANSFER_CONFIG_FILE_PATH, XMLNode::getError(pResults.error) );}
   else               { objMessage.fMessage_Create(LOG_WARNING, 73, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                   objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,CFG_MESSAGE_073, 
                                                   NG911_TRANSFER_CONFIG_FILE_PATH, XMLNode::getError(pResults.error) );  
                        enQueue_Message(MAIN,objMessage);
                      }
   return false;
  }

 iNumberofNodes = TransferlistNode.nChildNode("Transfer_Name");
 PhoneBookNode = XMLNode::emptyNode();

 NodeA = MainNode[0].getChildNode(XML_NODE_EVENT); 
 NodeB = NodeA.getChildNode(XML_NODE_INITIALIZATION);
 NodeC = NodeB.getChildNode(XML_NODE_PHONEBOOK);
 NodeD = NodeC.getChildNode(XML_NODE_PHONEBOOK_LIST);
 NodeE = NodeC.getChildNode(XML_NODE_GROUP_LIST);

 NodeD.deleteNodeContent();
 NodeD = XMLNode::emptyNode();
 NodeE.deleteNodeContent();
 NodeE = XMLNode::emptyNode();

 CurrentNode    = NodeC.addChild(XML_NODE_GROUP_LIST);
 GroupEntryNode = CurrentNode.addChild(XML_NODE_GROUP_ENTRY);
 GroupEntryNode.addAttribute(XML_FIELD_NAME, strNG911_PHONEBOOK_SIP_TRANSFER_HEADER.c_str());
 GroupEntryNode.addAttribute(XML_FIELD_PRIORITY, "0");
 GroupEntryNode.addAttribute(XML_FIELD_ICON_FILE, strNG911_PHONEBOOK_SIP_TRANSFER_ICON_FILE.c_str()); 


 NG911TransferNode = NodeC.addChild(XML_NODE_PHONEBOOK_LIST);

 for (int i = 0; i < iNumberofNodes; i++)
  {
   CurrentNode = NG911TransferNode.addChild(XML_NODE_PHONEBOOK_ENTRY);
   currentTransferlistnode = TransferlistNode.getChildNode("Transfer_Name", i);
   CurrentNode.addAttribute(XML_FIELD_NAME,currentTransferlistnode.getText());
   CurrentNode.addAttribute(XML_FIELD_TRANSFER_GROUP,strNG911_PHONEBOOK_SIP_TRANSFER_HEADER.c_str());
   currentTransferlistnode = TransferlistNode.getChildNode("Transfer_URI", i);   
   CurrentNode.addAttribute(XML_FIELD_TRANSFER_NUMBER,currentTransferlistnode.getText());
   currentTransferlistnode = TransferlistNode.getChildNode("Type", i);   
   CurrentNode.addAttribute(XML_FIELD_TRANSFER_TYPE,currentTransferlistnode.getText());
   CurrentNode.addAttribute(XML_FIELD_TRANSFER_CODE,"16");

   currentTransferlistnode = TransferlistNode.getChildNode("Speed_Dial_Action", i);   
    if (!currentTransferlistnode.isEmpty()) {
      if (currentTransferlistnode.getText()) {
       CurrentNode.addAttribute(XML_FIELD_SPEED_DIAL_ACTION, currentTransferlistnode.getText());         
      }
      else {
       CurrentNode.addAttribute(XML_FIELD_SPEED_DIAL_ACTION, XML_VALUE_SPEED_DIAL_ACTION_DEFAULT);         
      }
     }
    else {
     CurrentNode.addAttribute(XML_FIELD_SPEED_DIAL_ACTION, XML_VALUE_SPEED_DIAL_ACTION_DEFAULT);         
    }

    currentTransferlistnode = TransferlistNode.getChildNode("icon_file", i);   
     if (!currentTransferlistnode.isEmpty()) {
      if (currentTransferlistnode.getText()) {
       CurrentNode.addAttribute(XML_FIELD_ICON_FILE, currentTransferlistnode.getText());         
      }
      else {
       CurrentNode.addAttribute(XML_FIELD_ICON_FILE, XML_VALUE_ICON_FILE_DEFAULT);         
      }
     }
    else {
     CurrentNode.addAttribute(XML_FIELD_ICON_FILE, XML_VALUE_ICON_FILE_DEFAULT);         
    }   

  } //for (int i = 0; i < iNumberofNodes; i++)

 return true;
}




bool Initialize_Global_Variables::fChannelIsInternalNG911Transfer(string strChannel)
{
 int            iNumberofNodes;
 XMLNode 	CurrentNode;
 string         strText;
 bool           boolInternalTransfer;
 string         strSIPuri;
 size_t         found, sz;
 int            intRC;
 extern vector <PhoneBookEntry>               vCALL_LIST; 

 intRC = sem_wait(&sem_tMutexCall_LIST);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexCall_LIST, "sem_wait@sem_tMutexCall_LIST in fn Initialize_Global_Variables::LoadPhonebookList()", 1);}
   sz = vCALL_LIST.size();
   for (unsigned int i = 0; i < sz; i++) {
    if (vCALL_LIST[i].ng911TransferType == SIP_URI_INTERNAL) {
     found = strChannel.find(RemoveSIPcolon(vCALL_LIST[i].strCallerID));
     if (found != string::npos) {
      sem_post(&sem_tMutexCall_LIST);
      return true;
     }
    }
   }
 sem_post(&sem_tMutexCall_LIST);
 return false;

/* use call list versus xml node entries ....
 iNumberofNodes = PhoneBookNode.nChildNode(XML_NODE_PHONEBOOK_ENTRY);
 for (int i = 0; i < iNumberofNodes; i++)
  {
   CurrentNode = PhoneBookNode.getChildNode(XML_NODE_PHONEBOOK_ENTRY, i);
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE)) {strText = CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE);}         else {continue;}
   if (strText.empty())                                                                                                               {continue;}
   boolInternalTransfer = (strText == "internal");
   if (!boolInternalTransfer)                                                                                                         {continue;}
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER)) {strSIPuri = CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER);} else   {continue;}
   if (strSIPuri.empty())                                                                                                             {continue;}  

   found = strChannel.find(RemoveSIPcolon(strSIPuri));
   if (found != string::npos) {return true;}

  }
 // 
 iNumberofNodes = NG911TransferNode.nChildNode(XML_NODE_PHONEBOOK_ENTRY);
 for (int i = 0; i < iNumberofNodes; i++)
  {
   CurrentNode = NG911TransferNode.getChildNode(XML_NODE_PHONEBOOK_ENTRY, i);
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE)) {strText = CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE);}         else {continue;}
   if (strText.empty())                                                                                                               {continue;}
   boolInternalTransfer = (strText == "internal");
   if (!boolInternalTransfer)                                                                                                         {continue;}
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER)) {strSIPuri = CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER);} else   {continue;}
   if (strSIPuri.empty())                                                                                                             {continue;}  

   found = strChannel.find(RemoveSIPcolon(strSIPuri));
   if (found != string::npos) {return true;}

  }

 return false;
*/
}

sip_ng911_transfer_type Initialize_Global_Variables::fNG911_TransferType(string strSIP_URI)
{
 int            iNumberofNodes;
 XMLNode 	CurrentNode;
 string         strText;
 string         strSIPuri;
 size_t         found, sz;
 int            intRC;
 extern vector <PhoneBookEntry>               vCALL_LIST; 

 strSIPuri = RemoveSIPcolon(strSIP_URI);

 intRC = sem_wait(&sem_tMutexCall_LIST);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexCall_LIST, "sem_wait@sem_tMutexCall_LIST in fn Initialize_Global_Variables::LoadPhonebookList()", 1);}
   sz = vCALL_LIST.size();
   for (unsigned int i = 0; i < sz; i++) {
     found = strSIPuri.find(RemoveSIPcolon(vCALL_LIST[i].strCallerID));
     if (found != string::npos) {
      sem_post(&sem_tMutexCall_LIST);
      return vCALL_LIST[i].ng911TransferType;
     }
    
   }
 sem_post(&sem_tMutexCall_LIST);

 return NO_TRANSFER_DEFINED;

// use call list vs XMLNode data ....
/*
 iNumberofNodes = PhoneBookNode.nChildNode(XML_NODE_PHONEBOOK_ENTRY);

 for (int i = 0; i < iNumberofNodes; i++)
  {
   strText.clear(); strSIPuri.clear();

   CurrentNode = PhoneBookNode.getChildNode(XML_NODE_PHONEBOOK_ENTRY, i);
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER)) {strSIPuri = CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER);} else {continue;}
   if (strSIPuri.empty())                                                                                                           {continue;}

   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE)) {strText = CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE);}       else {continue;}
   if (strText.empty())                                                                                                               {continue;}

   if (strSIP_URI != strSIPuri)                                                                                                       {continue;}

   return NG911_TRANSFER_TYPE(strText);

  }

 iNumberofNodes = NG911TransferNode.nChildNode(XML_NODE_PHONEBOOK_ENTRY);

 for (int i = 0; i < iNumberofNodes; i++)
  {
   strText.clear(); strSIPuri.clear();

   CurrentNode = NG911TransferNode.getChildNode(XML_NODE_PHONEBOOK_ENTRY, i);
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER)) {strSIPuri = CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER);} else {continue;}
   if (strSIPuri.empty())                                                                                                           {continue;}

   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE)) {strText = CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE);}       else {continue;}
   if (strText.empty())                                                                                                               {continue;}

   if (strSIP_URI != strSIPuri)                                                                                                       {continue;}

   return NG911_TRANSFER_TYPE(strText);

  }

 return NO_TRANSFER_DEFINED;
*/

}
bool Initialize_Global_Variables::fIsSIPtransferNumberInternal(string strSIP_URI)
{
 int            iNumberofNodes;
 XMLNode 	CurrentNode;
 string         strText;
 string         strSIPuri;
 size_t         found, sz;
 int            intRC;
 extern vector <PhoneBookEntry>               vCALL_LIST; 

 strSIPuri = RemoveSIPcolon(strSIP_URI);

 intRC = sem_wait(&sem_tMutexCall_LIST);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexCall_LIST, "sem_wait@sem_tMutexCall_LIST in fn Initialize_Global_Variables::LoadPhonebookList()", 1);}
   sz = vCALL_LIST.size();
   for (unsigned int i = 0; i < sz; i++) {
     found = strSIPuri.find(RemoveSIPcolon(vCALL_LIST[i].strCallerID));
     if (found != string::npos) {
      sem_post(&sem_tMutexCall_LIST);
      if (vCALL_LIST[i].ng911TransferType == SIP_URI_INTERNAL) {return true;}
      else                                                     {return false;}
     }
    
   }
 sem_post(&sem_tMutexCall_LIST);

 return false;


/*
 iNumberofNodes = NG911TransferNode.nChildNode(XML_NODE_PHONEBOOK_ENTRY);

 for (int i = 0; i < iNumberofNodes; i++)
  {
   strText.clear(); strSIPuri.clear();

   CurrentNode = NG911TransferNode.getChildNode(XML_NODE_PHONEBOOK_ENTRY, i);
   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER)) {strSIPuri = CurrentNode.getAttribute(XML_FIELD_TRANSFER_NUMBER);} else {continue;}
   if (strSIPuri.empty())                                                                                                           {continue;}

   if (CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE)) {strText = CurrentNode.getAttribute(XML_FIELD_TRANSFER_TYPE);}       else {continue;}
   if (strText.empty())                                                                                                               {continue;}

   if (strSIP_URI != strSIPuri)                                                                                                       {continue;}

   return (strText == "internal");

  }

 return false;
*/
}



bool Telephone_Variables::fLoadTelephoneLineRegistration()
{
 string 	StringData;
 string 	Line_Key;
 int    	index;
 int    	i;
 Phone_Line 	objPhoneLine;

  // To Be updated
  // TBC
  

   //Line Registration Table
   for (int y = 1; y <= TelephoneEquipment.HighestWorkstationNumber; y++)
    {
     index = TelephoneEquipment.fIndexWithPositionNumber(y);
     if (index < 0) {continue;} 
     TelephoneEquipment.Devices[index].vPhoneLines.push_back(objPhoneLine);
     i=1;
     do
      {
       Line_Key = Create_INI_Key("Phone_", y, "Line_", i);
       StringData = ini.GetValue("TELEPHONE", Line_Key.c_str(), "" );
       if (StringData  == "") {break;}
       objPhoneLine.LineRegistrationName = StringData;
       objPhoneLine.OutboundDial         = true;
       TelephoneEquipment.Devices[index].vPhoneLines.push_back(objPhoneLine);
       i++;    
      }while ( StringData != ""); 
     TelephoneEquipment.Devices[index].iNumberOfLines = i-1;
    }

 return true;
}

bool Telephone_Variables::fLoadVOIPgateways()
{
 CSimpleIniA::TNamesDepend Gateway_Name_Values, Gateway_IP_Values, Gateway_Register_Values;
 CSimpleIniA::TNamesDepend::const_iterator l,m;
 ini.GetAllValues("TELEPHONE", "Gateway_Name", Gateway_Name_Values);
 ini.GetAllValues("TELEPHONE", "Gateway_IP_Address", Gateway_IP_Values);

 Gateway_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
 Gateway_IP_Values.sort(CSimpleIniA::Entry::LoadOrder());


 // output all of the items
/*
 l = Gateway_Name_Values.begin();
 m = Gateway_IP_Values.begin();

 while ((l != Gateway_Name_Values.end())||(m != Gateway_IP_Values.end()))
  { 
   if (!TelephoneEquipment.fLoadIPGateway(l->pItem, m->pItem, IsAudiocodes(l->pItem))) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_072, Phones_Key, StringData);} 
   ++l; ++m;
  }
*/
 return true;
}

/* ADV Vars  */
ADV_Variables::ADV_Variables() {

 this->stringEMAIL_FROM.clear();
//					charEMAIL_FROM
 this->stringEMAIL_SEND_TO.clear();
//					charEMAIL_SEND_TO
 this->enumDISPLAY_MODE = NO_DISPLAY_DEFINED;
 boolEmailON		=	true;
 this->strEMAIL_RELAY_SERVER.clear();
 this->strEMAIL_RELAY_USER.clear();
 this->strEMAIL_RELAY_PASSWORD.clear();
 this->intDEBUG_MODE_REMINDER_SEC = 3600;
 this->intALARM_POPUP_DURATION_MS = 6000;
 this->intTCP_RECONNECT_RECURSION_LEVEL = 7;
 this->intTCP_RECONNECT_INTERVAL_SEC	= 5;
 this-> ini.Reset();
 this->boolSTRATUS_SERVER_MONITOR = false;
}

//copy constructor
ADV_Variables::ADV_Variables(const ADV_Variables *a) {

 this->stringEMAIL_FROM			= a->stringEMAIL_FROM;
 this->stringEMAIL_SEND_TO		= a->stringEMAIL_SEND_TO;
 this->enumDISPLAY_MODE			= a->enumDISPLAY_MODE;
 this->boolEmailON			= a->boolEmailON;
 this->strEMAIL_RELAY_SERVER		= a->strEMAIL_RELAY_SERVER;
 this->strEMAIL_RELAY_USER		= a->strEMAIL_RELAY_USER;
 this->strEMAIL_RELAY_PASSWORD          = a->strEMAIL_RELAY_PASSWORD;
 this->intDEBUG_MODE_REMINDER_SEC	= a->intDEBUG_MODE_REMINDER_SEC;
 this->intALARM_POPUP_DURATION_MS	= a->intALARM_POPUP_DURATION_MS;
 this->intTCP_RECONNECT_RECURSION_LEVEL	= a->intTCP_RECONNECT_RECURSION_LEVEL;
 this->intTCP_RECONNECT_INTERVAL_SEC	= a->intTCP_RECONNECT_INTERVAL_SEC;
 this->boolSTRATUS_SERVER_MONITOR       = a->boolSTRATUS_SERVER_MONITOR;
// don't copy ini
}

//assignment operator
ADV_Variables& ADV_Variables::operator=(const ADV_Variables& a) {
 if (&a == this ) {return *this;}
 this->stringEMAIL_FROM			= a.stringEMAIL_FROM;
 this->stringEMAIL_SEND_TO		= a.stringEMAIL_SEND_TO;
 this->enumDISPLAY_MODE			= a.enumDISPLAY_MODE;
 this->boolEmailON			= a.boolEmailON;
 this->strEMAIL_RELAY_SERVER		= a.strEMAIL_RELAY_SERVER;
 this->strEMAIL_RELAY_USER		= a.strEMAIL_RELAY_USER;
 this->strEMAIL_RELAY_PASSWORD          = a.strEMAIL_RELAY_PASSWORD;
 this->intDEBUG_MODE_REMINDER_SEC	= a.intDEBUG_MODE_REMINDER_SEC;
 this->intALARM_POPUP_DURATION_MS	= a.intALARM_POPUP_DURATION_MS;
 this->intTCP_RECONNECT_RECURSION_LEVEL	= a.intTCP_RECONNECT_RECURSION_LEVEL;
 this->intTCP_RECONNECT_INTERVAL_SEC	= a.intTCP_RECONNECT_INTERVAL_SEC;
 this->boolSTRATUS_SERVER_MONITOR       = a.boolSTRATUS_SERVER_MONITOR;
 return *this;
}

//Comparison
bool ADV_Variables::fCompare(const ADV_Variables& a) const
{
 bool boolRunningCheck = true;
 bool boolPortcheck;

  boolRunningCheck = boolRunningCheck && (this->stringEMAIL_FROM 			== a.stringEMAIL_FROM);
  boolRunningCheck = boolRunningCheck && (this->stringEMAIL_SEND_TO 			== a.stringEMAIL_SEND_TO);
  boolRunningCheck = boolRunningCheck && (this->enumDISPLAY_MODE 			== a.enumDISPLAY_MODE);
  boolRunningCheck = boolRunningCheck && (this->boolEmailON 				== a.boolEmailON);
  boolRunningCheck = boolRunningCheck && (this->strEMAIL_RELAY_SERVER 			== a.strEMAIL_RELAY_SERVER);
  boolRunningCheck = boolRunningCheck && (this->strEMAIL_RELAY_USER 			== a.strEMAIL_RELAY_USER);
  boolRunningCheck = boolRunningCheck && (this->strEMAIL_RELAY_PASSWORD 		== a.strEMAIL_RELAY_PASSWORD);
  boolRunningCheck = boolRunningCheck && (this->intDEBUG_MODE_REMINDER_SEC 		== a.intDEBUG_MODE_REMINDER_SEC);
  boolRunningCheck = boolRunningCheck && (this->intALARM_POPUP_DURATION_MS 		== a.intALARM_POPUP_DURATION_MS);
  boolRunningCheck = boolRunningCheck && (this->intTCP_RECONNECT_RECURSION_LEVEL 	== a.intTCP_RECONNECT_RECURSION_LEVEL);
  boolRunningCheck = boolRunningCheck && (this->intTCP_RECONNECT_INTERVAL_SEC 		== a.intTCP_RECONNECT_INTERVAL_SEC);
  boolRunningCheck = boolRunningCheck && (this->boolSTRATUS_SERVER_MONITOR 		== a.boolSTRATUS_SERVER_MONITOR);
 return boolRunningCheck;
}

bool ADV_Variables::fLoadINIfile(){
 this->ini.SetUnicode(true);
 this->ini.SetMultiKey(true);
 this->ini.SetMultiLine(true);

 SI_Error rc = this->ini.LoadFile(charINI_FILE_PATH_AND_NAME);
 if (rc < 0) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_003, charINI_FILE_PATH_AND_NAME);}
  return true;
}

bool Initialize_Global_Variables::fLoadADVvariables(ADV_Variables &ADVvar) {

  string strErrorString = "Initialize_Global_Variables::fLoadADVvariables() Unable to load ";
  bool   boolReturnValue = true;

  // Get ADV INI Variables
   if (!ADVvar.fLoadINIfile()) {
    strErrorString = "Initialize_Global_Variables::fLoadADVvariables() Unable to load file: ";
    strErrorString += charINI_FILE_PATH_AND_NAME;
    SendCodingError( strErrorString );
    return false;
   }
   if (!ADVvar.SetEmailTo())         			        {strErrorString += " EmailTo "; 				boolReturnValue = false;} 
   if (!ADVvar.SetEmailFrom())       			        {strErrorString += " EmailFrom ";				boolReturnValue = false;}    
   if (!ADVvar.SetDisplayMode())          			{strErrorString += " DisplayMode ";				boolReturnValue = false;}    
   if (!ADVvar.SetEmailOnOff()) 	      			{strErrorString += " EmailOn ";					boolReturnValue = false;}    
   if (!ADVvar.SetEmailRelayServer())       			{strErrorString += " EmailRelayServer ";			boolReturnValue = false;}    
   if (!ADVvar.SetEmailRelayUser())      			{strErrorString += " EmailRelayUser ";				boolReturnValue = false;}    
   if (!ADVvar.SetEmailRelayPassword())      			{strErrorString += " EmailRelayPassword ";			boolReturnValue = false;}    
   if (!ADVvar.fLoadDebugDownReminder())      			{strErrorString += " DebugReminder ";				boolReturnValue = false;}    
   if (!ADVvar.SetAlarmPopUpDuration())      			{strErrorString += " AlarmPopUpDuration ";			boolReturnValue = false;} 
   if (!ADVvar.SetTCPRecursionLevel())      			{strErrorString += " TCPRecursionLevel ";			boolReturnValue = false;} 
   if (!ADVvar.SetTCPReconnectInterval())      			{strErrorString += " TCPReconnectInterval ";			boolReturnValue = false;}    
   if (!ADVvar.SetStratusServerMonitor())      			{strErrorString += " StratusServerMonitor ";			boolReturnValue = false;} 
   
 if (!boolReturnValue) { SendCodingError( strErrorString );}
return boolReturnValue;
}

void ADV_Variables::Set_GLobal_VARS() {

extern string				stringEMAIL_FROM;
extern char*				charEMAIL_FROM;
extern string				stringEMAIL_SEND_TO;
extern char*				charEMAIL_SEND_TO;
extern display_mode			enumDISPLAY_MODE;				
extern bool				boolEmailON;
extern string				strEMAIL_RELAY_SERVER;
extern string				strEMAIL_RELAY_USER;
extern string				strEMAIL_RELAY_PASSWORD;
extern long unsigned int		intDEBUG_MODE_REMINDER_SEC;
extern int				intALARM_POPUP_DURATION_MS;
extern int				intTCP_RECONNECT_RECURSION_LEVEL;
extern int				intTCP_RECONNECT_INTERVAL_SEC;
extern bool                             boolSTRATUS_SERVER_MONITOR;

extern void    				Set_Display_Mode(display_mode enumMode);

stringEMAIL_FROM    			= this->stringEMAIL_FROM;
charEMAIL_FROM               		= (char*) stringEMAIL_FROM.c_str();
stringEMAIL_SEND_TO 			= this->stringEMAIL_SEND_TO;
charEMAIL_SEND_TO             	 	= (char*) stringEMAIL_SEND_TO.c_str();
enumDISPLAY_MODE	 		= this->enumDISPLAY_MODE;
boolEmailON				= this->boolEmailON;
strEMAIL_RELAY_SERVER			= this->strEMAIL_RELAY_SERVER;
strEMAIL_RELAY_USER			= this->strEMAIL_RELAY_USER;
strEMAIL_RELAY_PASSWORD			= this->strEMAIL_RELAY_PASSWORD;
intDEBUG_MODE_REMINDER_SEC		= this->intDEBUG_MODE_REMINDER_SEC;
intALARM_POPUP_DURATION_MS		= this->intALARM_POPUP_DURATION_MS;
intTCP_RECONNECT_RECURSION_LEVEL	= this->intTCP_RECONNECT_RECURSION_LEVEL;
intTCP_RECONNECT_INTERVAL_SEC		= this->intTCP_RECONNECT_INTERVAL_SEC;
boolSTRATUS_SERVER_MONITOR              = this->boolSTRATUS_SERVER_MONITOR;

 Set_Display_Mode(this->enumDISPLAY_MODE);
 return;
}

bool ADV_Variables::SetStratusServerMonitor() {
 if(!char2bool(this->boolSTRATUS_SERVER_MONITOR, ini.GetValue("ADVANCED", ADV_INI_KEY_13, DEFAULT_VALUE_ADV_INI_KEY_13 )))   {return false;}
 return true;
}

bool ADV_Variables::SetTCPReconnectInterval() { 
 string strData;
 size_t found;

  strData = ini.GetValue("ADVANCED", ADV_INI_KEY_12, DEFAULT_VALUE_ADV_INI_KEY_12 );
  found = strData.find_first_not_of("0123456789");
  if (found != string::npos) {
   return false;
  } 
  this->intTCP_RECONNECT_INTERVAL_SEC = char2int(strData.c_str());
return true;
}

bool ADV_Variables::SetTCPRecursionLevel() { 
 string strData;
 size_t found;

  strData = ini.GetValue("ADVANCED", ADV_INI_KEY_11, DEFAULT_VALUE_ADV_INI_KEY_11 );
  found = strData.find_first_not_of("0123456789");
  if (found != string::npos) {
   return false;
  } 
  this->intTCP_RECONNECT_RECURSION_LEVEL = char2int(strData.c_str());
return true;
}

bool ADV_Variables::SetAlarmPopUpDuration() { 
 string strData;
 size_t found;

  strData = ini.GetValue("ADVANCED", ADV_INI_KEY_10, DEFAULT_VALUE_ADV_INI_KEY_10 );
  found = strData.find_first_not_of("0123456789");
  if (found != string::npos) {
   return false;
  } 
  this->intALARM_POPUP_DURATION_MS = char2int(strData.c_str());
return true;
}

bool ADV_Variables::fLoadDebugDownReminder() {
 string strData;
 size_t found;

  strData = ini.GetValue("ADVANCED", ADV_INI_KEY_6, DEFAULT_VALUE_ADV_INI_KEY_6 );
  found = strData.find_first_not_of("0123456789");
  if (found != string::npos) {
   return false;
  } 
  this->intDEBUG_MODE_REMINDER_SEC = char2int(strData.c_str());
return true;
}

bool ADV_Variables::SetEmailRelayPassword() { 
  this->strEMAIL_RELAY_PASSWORD = ini.GetValue("ADVANCED", ADV_INI_KEY_5, DEFAULT_VALUE_ADV_INI_KEY_5 ); 
return true;
}

bool ADV_Variables::SetEmailRelayUser() { 
  this->strEMAIL_RELAY_USER = ini.GetValue("ADVANCED", ADV_INI_KEY_4, DEFAULT_VALUE_ADV_INI_KEY_4 ); 
return true;
}

bool ADV_Variables::SetEmailRelayServer() { 

this->strEMAIL_RELAY_SERVER = ini.GetValue("ADVANCED", ADV_INI_KEY_3, DEFAULT_VALUE_ADV_INI_KEY_3 );
return true; 
}
bool ADV_Variables::SetEmailOnOff() { 

if(!char2bool(this->boolEmailON, ini.GetValue("ADVANCED", ADV_INI_KEY_2, DEFAULT_VALUE_ADV_INI_KEY_2 )))   {return false;}
return true;
}

bool ADV_Variables::SetDisplayMode() { 

string			strData;

  strData = ini.GetValue("ADVANCED", ADV_INI_KEY_1, DEFAULT_VALUE_ADV_INI_KEY_1 );
  if      (strData == LOG_DISPLAY_MODE_1) {this->enumDISPLAY_MODE = MINIMAL_DISPLAY;}
  else if (strData == LOG_DISPLAY_MODE_2) {this->enumDISPLAY_MODE = NORMAL_DISPLAY;}
  else  {
   return false; 
  } 
 
 return true;
}

bool ADV_Variables::SetEmailTo() { 
   // Get General Email INI Variables 2.7.1 Read from KRN, if default read from LOG
   this->stringEMAIL_SEND_TO = ini.GetValue("GENERAL", KRN_INI_KEY_20, DEFAULT_VALUE_KRN_INI_KEY_20);
   if (this->stringEMAIL_SEND_TO == DEFAULT_VALUE_KRN_INI_KEY_20) {this->stringEMAIL_SEND_TO = ini.GetValue("LOG", LOG_INI_KEY_2, DEFAULT_VALUE_LOG_INI_KEY_2); }    
   if (this->stringEMAIL_SEND_TO == DEFAULT_VALUE_LOG_INI_KEY_2)  {return false; } 

 return true;
}

bool ADV_Variables::SetEmailFrom() {

size_t 		found;
string		StringData;
extern string 	stringSERVER_HOSTNAME;
extern string 	stringPSAP_Name;

 if (stringSERVER_HOSTNAME.empty()) 	{return false;}
 if (stringPSAP_Name.empty())		{return false;}

   this->stringEMAIL_FROM = ini.GetValue("ADVANCED", ADV_INI_KEY_9, DEFAULT_VALUE_ADV_INI_KEY_9);
   if (this->stringEMAIL_FROM == DEFAULT_VALUE_ADV_INI_KEY_9) {
    found = this->stringEMAIL_SEND_TO.find("@");
    if (found == string::npos) 		{return false; }
    StringData = stringSERVER_HOSTNAME + this->stringEMAIL_SEND_TO.substr(found, string::npos);
   }
   else {
    StringData = stringSERVER_HOSTNAME + "@" + this->stringEMAIL_FROM;
   }

   this->stringEMAIL_FROM = "\""+stringPSAP_Name+"\"<"+StringData+">";

 return true;
}


/* DSB Vars   */
DSB_Variables::DSB_Variables() {

this->boolUSE_DASHBOARD 	= false;
this->boolRestartDashboardPort	= false;
this->strDASHBOARD_API_KEY.clear();
this->intNUM_DSB_PORTS          = 0;
this->intOldNumberOfDSBports    = 0;
//this->intDSB_HEARTBEAT_INTERVAL_SEC    = DASHBOARD_PING_INTERVAL;
//this->intDSB_PORT_DOWN_THRESHOLD_SEC   = DSB_PORT_TIMEOUT_INTERVAL_SEC;
//this->intDSB_PORT_DOWN_REMINDER_SEC    = DSB_PORT_DOWN_REMINDER_INTERVAL_SEC;
this-> ini.Reset();
this->DSB_PORT_VARS.fClear();
}
//copy constructor
DSB_Variables::DSB_Variables(const DSB_Variables *a) {

 this->boolUSE_DASHBOARD		= a->boolUSE_DASHBOARD;
 this->boolRestartDashboardPort		= a->boolRestartDashboardPort;
 this->strDASHBOARD_API_KEY		= a->strDASHBOARD_API_KEY;
 this->intNUM_DSB_PORTS                 = a->intNUM_DSB_PORTS;
// this->DashboardPort                    = a->DashboardPort;
// this->intDSB_HEARTBEAT_INTERVAL_SEC    = a->intDSB_HEARTBEAT_INTERVAL_SEC;
// this->intDSB_PORT_DOWN_THRESHOLD_SEC   = a->intDSB_PORT_DOWN_THRESHOLD_SEC;
// this->intDSB_PORT_DOWN_REMINDER_SEC    = a->intDSB_PORT_DOWN_REMINDER_SEC;
 this->DSB_PORT_VARS			= a->DSB_PORT_VARS;
// don't copy ini
}
//assignment operator
DSB_Variables& DSB_Variables::operator=(const DSB_Variables& a) {
 if (&a == this ) {return *this;}
 this->boolUSE_DASHBOARD		= a.boolUSE_DASHBOARD;
 this->intNUM_DSB_PORTS                 = a.intNUM_DSB_PORTS;
 this->boolRestartDashboardPort		= a.boolRestartDashboardPort;
 this->strDASHBOARD_API_KEY		= a.strDASHBOARD_API_KEY;
// this->DashboardPort                    = a.DashboardPort;
// this->intDSB_HEARTBEAT_INTERVAL_SEC    = a.intDSB_HEARTBEAT_INTERVAL_SEC;
// this->intDSB_PORT_DOWN_THRESHOLD_SEC   = a.intDSB_PORT_DOWN_THRESHOLD_SEC;
// this->intDSB_PORT_DOWN_REMINDER_SEC    = a.intDSB_PORT_DOWN_REMINDER_SEC;
 this->DSB_PORT_VARS			= a.DSB_PORT_VARS;
 //this->intOldNumberOfDSBports           = a.intOldNumberOfDSBports;

 return *this;
}
//Comparison
bool DSB_Variables::fCompare(const DSB_Variables& a) const
{
 bool boolRunningCheck = true;
 bool boolPortcheck;

 bool boolNumberofPortsSame = (this->intNUM_DSB_PORTS 					== a.intNUM_DSB_PORTS);

  boolRunningCheck = boolRunningCheck && (this->boolUSE_DASHBOARD 			== a.boolUSE_DASHBOARD);
  if (!boolRunningCheck) { cout << this->boolUSE_DASHBOARD << " = " << a.boolUSE_DASHBOARD << endl;}
  boolRunningCheck = boolRunningCheck && (this->strDASHBOARD_API_KEY 			== a.strDASHBOARD_API_KEY);
  if (!boolRunningCheck) { cout << "b" << endl;}
  boolRunningCheck = boolRunningCheck && (this->intNUM_DSB_PORTS 			== a.intNUM_DSB_PORTS);

  if (!boolRunningCheck) { cout << "c" << endl;}
  //We only check configurable settings in the port
  this->boolRestartDashboardPort = false;
  boolPortcheck = (this->DSB_PORT_VARS.fCompare(a.DSB_PORT_VARS)); 
  if (!boolPortcheck) { cout << "1.a" << endl;}
  if (!boolPortcheck) {this->boolRestartDashboardPort = true;}  
/*
  boolPortcheck = (this->DashboardPort.intPortDownThresholdSec 		== a.DashboardPort.intPortDownThresholdSec);
  if (!boolPortcheck) { cout << "1" << endl;}
  if (!boolPortcheck) {this->boolRestartDashboardPort = true;}  
  boolPortcheck = (this->DashboardPort.intPortDownReminderThreshold 	== a.DashboardPort.intPortDownReminderThreshold);
  if (!boolPortcheck) { cout << "2" << endl;}
  if (!boolPortcheck) {this->boolRestartDashboardPort = true;}  
  boolPortcheck = (this->DashboardPort.intPortNum 			== a.DashboardPort.intPortNum);
  if (!boolPortcheck) { cout << "3" << endl;}
  if (!boolPortcheck) {this->boolRestartDashboardPort = true;}  
  boolPortcheck =  (this->DashboardPort.intRemotePortNumber 		== a.DashboardPort.intRemotePortNumber);
  if (!boolPortcheck) { cout << "4" << endl;}
  if (!boolPortcheck) {this->boolRestartDashboardPort = true;}  
  boolPortcheck = (this->DashboardPort.Remote_IP.stringAddress		== a.DashboardPort.Remote_IP.stringAddress);
  if (!boolPortcheck) { cout << "5" << endl;}
  if (!boolPortcheck) {this->boolRestartDashboardPort = true;} 
*/
  boolRunningCheck = boolRunningCheck && boolNumberofPortsSame;
  if (!boolRunningCheck) {this->boolRestartDashboardPort = true;}  
 
  boolRunningCheck = boolRunningCheck && (!this->boolRestartDashboardPort);
  if (!boolRunningCheck) { cout << "d" << endl;}


  this->intOldNumberOfDSBports       = this->intNUM_DSB_PORTS;
  a.intOldNumberOfDSBports           = this->intNUM_DSB_PORTS;
 
  a.boolRestartDashboardPort = this->boolRestartDashboardPort;
 return boolRunningCheck;
}

bool DSB_Variables::fLoadINIfile()
{
 this->ini.SetUnicode(true);
 this->ini.SetMultiKey(true);
 this->ini.SetMultiLine(true);

 SI_Error rc = this->ini.LoadFile(charINI_FILE_PATH_AND_NAME);
 if (rc < 0) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_003, charINI_FILE_PATH_AND_NAME);}
 return true;
}

bool Initialize_Global_Variables::fLoadDSBvariables(DSB_Variables &DSBvar) {

  string strErrorString = "Initialize_Global_Variables::fLoadDSBvariables() Unable to load ";
  bool   boolReturnValue = true;

  // Get DSB INI Variables
   if (!DSBvar.fLoadINIfile()) {
    strErrorString = "Initialize_Global_Variables::fLoadDSBvariables() Unable to load file: ";
    strErrorString += charINI_FILE_PATH_AND_NAME;
    SendCodingError( strErrorString );
    return false;
   }
   if (!DSBvar.fLoadUseDashboard())         			{strErrorString += " UseDashboard "; 				boolReturnValue = false;} 
   if (!DSBvar.fLoadPortDownThreshold())       			{strErrorString += " PortDownThreshold ";			boolReturnValue = false;}    
   if (!DSBvar.fLoadPortDownReminder())       			{strErrorString += " PortDownReminder ";			boolReturnValue = false;}    
   if (!DSBvar.fLoadDashboardAPIkey())       			{strErrorString += " APIkey ";					boolReturnValue = false;}    
   if (!DSBvar.fLoadDashboardIPaddress())       		{strErrorString += " IPaddress ";				boolReturnValue = false;}    
   if (!DSBvar.fLoadDashboardPortNumber())      		{strErrorString += " PortNumber ";				boolReturnValue = false;}    
   if (!DSBvar.fLoadHearbeatInterval())      			{strErrorString += " HeartbeatInterval ";			boolReturnValue = false;}    

   DSBvar.intNUM_DSB_PORTS = (int) ((DSBvar.boolUSE_DASHBOARD)&&(boolReturnValue));

 if (!boolReturnValue) { SendCodingError( strErrorString );}
return boolReturnValue;
}

void DSB_Variables::Set_GLobal_VARS() {

extern int		intNUM_DSB_PORTS;
extern bool		boolUSE_DASHBOARD;
extern string           strDASHBOARD_API_KEY;
extern int		intDSB_PORT_DOWN_REMINDER_SEC;
extern int		intDSB_HEARTBEAT_INTERVAL_SEC;
extern int		intDSB_PORT_DOWN_THRESHOLD_SEC;

intNUM_DSB_PORTS		= this->intNUM_DSB_PORTS;
boolUSE_DASHBOARD    		= this->boolUSE_DASHBOARD;
strDASHBOARD_API_KEY 		= this->strDASHBOARD_API_KEY;
intDSB_PORT_DOWN_REMINDER_SEC 	= this->DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC;
intDSB_PORT_DOWN_THRESHOLD_SEC	= this->DSB_PORT_VARS.intPORT_DOWN_THRESHOLD_SEC;
intDSB_HEARTBEAT_INTERVAL_SEC	= this->DSB_PORT_VARS.intHEARTBEAT_INTERVAL_SEC;


////cout << "old number of ports -> " << this->intOldNumberOfDSBports << endl;
////cout << "new number of ports -> " << this->intNUM_DSB_PORTS << endl;
//cout << "restart DSB port    -> " << this->boolRestartDashboardPort << endl;
//cout << "new var " << DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC << endl;
 return;
}

bool DSB_Variables::fLoadHearbeatInterval() {
 string strNumber;
 size_t found;

  //Not using this variable at this time (Hard coded into timer defines.h DASHBOARD_PING_INTERVAL )
  strNumber = this->ini.GetValue("DSB", DSB_INI_KEY_7, DEFAULT_VALUE_DSB_INI_KEY_7 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
  this->DSB_PORT_VARS.intHEARTBEAT_INTERVAL_SEC = char2int(strNumber.c_str());
 return true;
}

bool DSB_Variables::fLoadUseDashboard() {
 return char2bool(this->boolUSE_DASHBOARD, this->ini.GetValue("DSB", DSB_INI_KEY_1, DEFAULT_VALUE_DSB_INI_KEY_1 ));
}

bool DSB_Variables::fLoadDashboardAPIkey() {
 this->strDASHBOARD_API_KEY = this->ini.GetValue("DSB", DSB_INI_KEY_2, DEFAULT_VALUE_DSB_INI_KEY_2 );
 return true;
}

bool DSB_Variables::fLoadPortDownThreshold()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("DSB", DSB_INI_KEY_3, DEFAULT_VALUE_DSB_INI_KEY_3 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
 // this->DashboardPort.intPortDownThresholdSec = char2int(strNumber.c_str());
 // this->intDSB_PORT_DOWN_THRESHOLD_SEC = this->DashboardPort.intPortDownThresholdSec;
  this->DSB_PORT_VARS.intPORT_DOWN_THRESHOLD_SEC = char2int(strNumber.c_str());
 return true;
}

bool DSB_Variables::fLoadPortDownReminder()
{
 string strNumber;
 size_t found;

  strNumber = this->ini.GetValue("DSB", DSB_INI_KEY_4, DEFAULT_VALUE_DSB_INI_KEY_4 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos)            {return false;}
//  this->DashboardPort.intPortDownReminderThreshold = char2int(strNumber.c_str());
//  this->intDSB_PORT_DOWN_REMINDER_SEC = this->DashboardPort.intPortDownReminderThreshold;
  this->DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC = char2int(strNumber.c_str());
 return true;
}

bool DSB_Variables::fLoadDashboardPortNumber()
{
 string strNumber;
 size_t found;
 int    iNumber;

  strNumber = this->ini.GetValue("DSB", DSB_INI_KEY_6, DEFAULT_VALUE_DSB_INI_KEY_6 );
  found =  strNumber.find_first_not_of("0123456789");
  if (found != string::npos) {
   SendCodingError( "Error Loading IP: DSB_Variables::fLoadDashboardPortNumber(), Invalid Port "+strNumber);
   return false;
  }
  iNumber = char2int(strNumber.c_str());
  if ((iNumber < 0)||(iNumber > 65535)) {
   SendCodingError( "Error Loading IP: DSB_Variables::fLoadDashboardPortNumber(), Invalid Port "+strNumber);
   return false;
  }                     
 if (iNumber < 1024) {
   SendCodingError( "Warning: DSB_Variables::fLoadDashboardPortNumber(), 0-1023 are reserved ports! Port="+strNumber);
 }
// this->DashboardPort.intRemotePortNumber = iNumber;
 this->DSB_PORT_VARS.intREMOTE_PORT_NUMBER = iNumber;
 return true;
}

bool DSB_Variables::fLoadDashboardIPaddress()
{
 /* The number of DSB ports will be calculated by the number of FreeSwitch IP addresses.  Currently we are only using 1.
 */

 string 	StringData;
 char        	NumericAddress[32];
 bool           boolErrorDetected = false;

   StringData = this->ini.GetValue("DSB", DSB_INI_KEY_5, DEFAULT_VALUE_DSB_INI_KEY_5 );

   if (StringData == DEFAULT_VALUE_DSB_INI_KEY_5)  {
    if (this->boolUSE_DASHBOARD) {
     boolErrorDetected = true;
    }     
   }
   else {
     if ( inet_pton( AF_INET , (const char *)StringData.c_str() , NumericAddress) > 0) { 
      //this->DashboardPort.Remote_IP.stringAddress = StringData;
      //this->DashboardPort.intPortNum = 1;
      this->DSB_PORT_VARS.REMOTE_IP_ADDRESS.stringAddress = StringData;
      this->DSB_PORT_VARS.intPORT_NUMBER = 1;
     }
     else {
     boolErrorDetected = true;
     }
   }
  if (boolErrorDetected) {
   SendCodingError( "Error Loading IP: DSB_Variables::fLoadDashboardIPaddress(), Thread inhibited");
   this->boolUSE_DASHBOARD = false;
  }
 return (!boolErrorDetected);
}







