/*****************************************************************************
* FILE: transfer_data_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the Transfer_Data Class 
*
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"


// Constructor
Transfer_Data::Transfer_Data()
 {
  strTransferType.clear();
  strTransferFromPosition = "00";
  strTransferFromChannel.clear();
  strTransferFromUniqueid.clear();
  strTransferMatchingChannelId.clear();
  strTransferTargetUUID.clear();
  strTransfertoNumber.clear();
  strNG911PSAP_TransferNumber.clear();
  strConfDisplay.clear();
  strRDNIS.clear();
  boolGUIcancel         = false;
  strHangupCause        = "";
  eTransferMethod       = mBLANK;
  strTransferName.clear();
  boolIsRingBack        = false;
  IPaddress.fClear();
  strValetExtension.clear();
  vUUID.clear();
 }

// assignment operator
Transfer_Data& Transfer_Data::operator=(const Transfer_Data& b)
 {
  if (&b == this ) {return *this;}
  strTransferType              = b.strTransferType;
  strTransferFromPosition      = b.strTransferFromPosition;
  strTransferFromChannel       = b.strTransferFromChannel;
  strTransferFromUniqueid      = b.strTransferFromUniqueid;
  strTransferMatchingChannelId = b.strTransferMatchingChannelId;
  strTransferTargetUUID        = b.strTransferTargetUUID;
  strTransfertoNumber          = b.strTransfertoNumber;
  strNG911PSAP_TransferNumber  = b.strNG911PSAP_TransferNumber;
  strConfDisplay               = b.strConfDisplay;
  strRDNIS                     = b.strRDNIS;
  boolGUIcancel                = b.boolGUIcancel;
  strHangupCause               = b.strHangupCause;
  eTransferMethod              = b.eTransferMethod;
  strTransferName              = b.strTransferName;
  boolIsRingBack               = b.boolIsRingBack;
  IPaddress                    = b.IPaddress;
  strValetExtension            = b.strValetExtension;
  vUUID                        = b.vUUID;
  return *this;
 } 





void Transfer_Data::fClear()
{
 strTransferType.clear();
 strTransferFromPosition = "00";
 strTransferFromChannel.clear();
 strTransferFromUniqueid.clear();
 strTransferMatchingChannelId.clear();
 strTransferTargetUUID.clear();
 strTransfertoNumber.clear();
 strNG911PSAP_TransferNumber.clear();
 strConfDisplay.clear();
 strRDNIS.clear();
 boolGUIcancel         = false;
 strHangupCause.clear();
 eTransferMethod       = mBLANK;
 strTransferName.clear();
 boolIsRingBack        = false;
 IPaddress.fClear();
 strValetExtension.clear();
 vUUID.clear();
}

void Transfer_Data::fDisplayLabel() 
{
 //cout << setw(7)  << left << "Index";
 //cout << setw(5)  << left << "Pos";
 //cout << setw(15) << left << "TransferTo:";
 //cout << setw(35) << left << "TransferToSIP:";
 //cout << setw(15) << left << "RDNIS";
 //cout << setw(35) << left << "Name";
 //cout << setw(20) << left << "Method";
 //cout << setw(10) << left << "Cdisp";
 //cout << setw(10) << left << "Cancel";
 //cout << setw(50) << left << "FromChannel";
 //cout << setw(40) << left << "FromChannelId";
 //cout << setw(25) << left << "Hangup Cause";
 //cout << setw(25) << left << "IP Address" << endl;
}

void Transfer_Data::fDisplay(int i)
{
 unsigned int sz;

 //cout << setw(7)  << left << "Index";
 //cout << setw(5)  << left << "Pos";
 //cout << setw(15) << left << "TransferTo:";
 //cout << setw(35) << left << "TransferToSIP:";
 //cout << setw(15) << left << "RDNIS";
 //cout << setw(35) << left << "Name";
 //cout << setw(20) << left << "Method";
 //cout << setw(10) << left << "Cdisp";
 //cout << setw(10) << left << "Cancel" << endl;;
 //cout << setw(7)  << left << i;
 //cout << setw(5)  << left << strTransferFromPosition;
 //cout << setw(15) << left << strTransfertoNumber;
 //cout << setw(35) << left << strNG911PSAP_TransferNumber;
 //cout << setw(15) << left << strRDNIS;
 //cout << setw(35) << left << strTransferName;
 //cout << setw(20) << left << fTransferMethod();
 //cout << setw(10) << left << strConfDisplay;
 //cout << setw(10) << left << boolGUIcancel << endl;
 //cout << setw(7)  << left << " ";
 //cout << setw(50) << left << "FromChannel";
 //cout << setw(40) << left << "FromChannelId";
 //cout << setw(25) << left << "Hangup Cause";
 //cout << setw(25) << left << "IP Address";
 //cout << setw(15) << left << "Park Ext" << endl;
 //cout << setw(7)  << left << " ";
 //cout << setw(50) << left << strTransferFromChannel;
 //cout << setw(40) << left << strTransferFromUniqueid;
 //cout << setw(25) << left << strHangupCause;
 //cout << setw(25) << left << IPaddress.stringAddress;
 //cout << setw(15) << left << strValetExtension << endl;
 //cout << setw(40) << left << "Target UUID";
 //cout << setw(40) << left << "MatchingChannelId" << endl;
 //cout << setw(40) << left << strTransferTargetUUID;
 //cout << setw(40) << left << strTransferMatchingChannelId;
 //cout << endl;
 //cout << "cached UUID's:" << endl;
 sz = vUUID.size();
 for (unsigned int i = 0; i < sz; i++) {
  //cout << vUUID[i] << endl;
 }
//cout << endl << endl;;
}

bool Transfer_Data::fIsPark(bool &boolValet)
{
 extern Telephone_Devices                       TelephoneEquipment;

 size_t sz;
 boolValet = false;

 if (this->strTransfertoNumber.empty())               {return false;}
 sz = TelephoneEquipment.ParkingLots.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if (TelephoneEquipment.ParkingLots[i] == this->strTransfertoNumber){
    if (i==0) {boolValet = true;}
    return true; 
   }
  }

 return false;
}

void Transfer_Data::fLoadHangupCause(string strInput)
{
  strHangupCause = strInput;
}

bool Transfer_Data::fCheckINDIGITAL_Transfer_Number()
{
 size_t found;

 found = strTransfertoNumber.find("*8#");
 if (found == 0) 
  {
   eTransferMethod = mINDGITAL;
   return true;
  }

 return false;
}

bool Transfer_Data::fCheckINTRADO_TRANSFER(int i)
{
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;

 if (TRUNK_TYPE_MAP.fIsINTRADO(i)) {

   switch (this->eTransferMethod) {

    case mTANDEM: case mNG911_TRANSFER: case mBLANK: 
     this-> eTransferMethod = mINTRADO;
     return true;
    default:
    break;
   }
 }
 return false;
}

bool Transfer_Data::fCheckREFER_TRANSFER(int i)
{
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;

 //cout << "i -> " << i << endl;
 //cout << "bool -> " << TRUNK_TYPE_MAP.fIsREFER(i) << endl;
 if (TRUNK_TYPE_MAP.fIsREFER(i)) {
   //cout << "Transfer_Data::fCheckREFER_TRANSFER transfer method before switch " << this->fTransferMethod() << endl;
   //cout << "Transfer_Data::fCheckREFER_TRANSFER transfer type before switch " << this->strTransferType << endl;
/*
  Here we check the string which will have the original type stored.  The system looks at the number
  and if it is a sip number the method will be calculated as NG911_TRANSFER.  If the original was
  Tandem then we convert to Refer.  Otherwise leave as NG911_Transfer
*/
   switch (this->fTransferMethod(this->strTransferType)) {

    case mTANDEM: case mBLANK:
     this-> eTransferMethod = mREFER;
     //cout << "SET TO REFER" << endl;
     return true;
    default:
     break;
   }

 }
 return false;
}

transfer_method Transfer_Data::fTransferMethod(string strInput) {

  if (strInput == "Blank") 			{return mBLANK;}
  if (strInput == "GUI TRANSFER") 		{return mGUI_TRANSFER;}
  if (strInput == "BLIND TRANSFER") 		{return mBLIND_TRANSFER;}   
  if (strInput == "ATTENDED TRANSFER") 		{return mATTENDED_TRANSFER;}
  if (strInput == "POOR MAN BLIND TRANSFER") 	{return mPOORMAN_BLIND_TRANSFER;}
  if (strInput == "RING BACK") 			{return mRING_BACK;}
  if (strInput == "TANDEM") 			{return mTANDEM;}
  if (strInput == "NG911 SIP TRANSFER") 	{return mNG911_TRANSFER;}
  if (strInput == "FLASH HOOK") 		{return mFLASHOOK;}
  if (strInput == "INDIGITAL") 			{return mINDGITAL;}
  if (strInput == "INTRADO") 			{return mINTRADO;}
  if (strInput == "REFER") 			{return mREFER;}

 return mBLANK;
}




string Transfer_Data::fTransferMethod() {

 switch (eTransferMethod)  {
   case mBLANK:                  return "Blank";
   case mGUI_TRANSFER:           return "GUI TRANSFER";
   case mBLIND_TRANSFER:         return "BLIND TRANSFER";
   case mATTENDED_TRANSFER:      return "ATTENDED TRANSFER";
   case mPOORMAN_BLIND_TRANSFER: return "POOR MAN BLIND TRANSFER";
   case mRING_BACK:              return "RING BACK";
   case mTANDEM:                 return "TANDEM";
   case mNG911_TRANSFER:         return "NG911 SIP TRANSFER";
   case mFLASHOOK:               return "FLASH HOOK";
   case mINDGITAL:		 return "INDIGITAL";
   case mINTRADO:                return "INTRADO";
   case mREFER:                  return "REFER";
 }
 return "";
}  

void Transfer_Data::fLoad_RDNIS(string strData)
{
 strRDNIS = strData;
 return;
}
int Transfer_Data::fLoadFromPosition(int i)
{
 strTransferFromPosition = int2str(i);
 return i;
}
int Transfer_Data::fLoadFromPosition(string strPosition)
{
 size_t                 found;
 unsigned long long int iPosition;

 if (strPosition.empty())                {return -1;}

 found = strPosition.find_first_not_of("0123456789");
 if (found != string::npos)              {return -1;}
 
 iPosition = char2int(strPosition.c_str());
 if (iPosition > (unsigned long long int) intNUM_WRK_STATIONS)    {return -1;}

 strTransferFromPosition = strPosition;
 return iPosition; 
}

int Transfer_Data::fLoadFromPosition(string strData, string strKey)
{

 return fLoadFromPosition(ParseFreeswitchData(strData,strKey));
}

int Transfer_Data::fReturnTransferNumberAsPositionNumber()
{
 string strData;
 int    intData;

 intData = PositionNumberfromExtension(strTransfertoNumber);
 if (intData > intNUM_WRK_STATIONS)    {return 0;}
 else                                  {return intData;}
}

void Transfer_Data::fLoadConferenceDisplayX(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_PBX_TRANSFER_PREFIX;
 if (i) {strConfDisplay += int2str(i);}
}

void Transfer_Data::fLoadConferenceDisplayT(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_TANDEM_TRANSFER_PREFIX;
 if (i) {strConfDisplay += int2str(i);}
}





/*

bool Transfer_Data::fLoad_Blind_Transfer_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum)
{
 Port_Data                      objPortData;

 objPortData.fLoadPortData(enumArg2, intPortNum);

 switch (enumArg)
  {
   case XML_DATA_FIELD:
        if (!fLoad_Transfer_Number(XML_DATA_FIELD, enumArg2, stringArg, intPortNum)) {return false;}
        bBlindTransfer = true;
        break;
   case SOFTWARE:
        if (!fLoad_Transfer_Number(SOFTWARE, enumArg2, stringArg, intPortNum)) {return false;}
        bBlindTransfer = true;   
        break;

   default:
        // ERROR
        //cout << "Coding error ExperientDataClass::fLoad_Blind_Transfer_Number() -> Data Format = " << enumArg << endl;
        return false;
  }

 return true;
}
*/
bool Transfer_Data::fLoad_Transfer_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum)
{
 Port_Data                      objPortData;
 string                         strData;
 size_t 			found;
 extern Telephone_Devices       TelephoneEquipment;
                         
 objPortData.fLoadPortData(enumArg2, intPortNum);
 strData = TelephoneEquipment.fRemoveRoutingPrefix(stringArg);

 //cout << "Transfer_Data::fLoad_Transfer_Number -> " << strData << endl;
 switch (enumArg)
  {
   case XML_DATA_FIELD:
        strTransfertoNumber = strData;
        break;
   case SOFTWARE:
        strTransfertoNumber = strData;     
        break;

   default:
        // ERROR
        SendCodingError("transfer_data_functions.cpp - Coding error Transfer_Data::fLoad_Transfer_Number() -> Data Format = " + int2str( enumArg));
        return false;

  } // end switch

 if (strTransfertoNumber.empty()) {return false;}
 

 // Next Gen Transfer has sip:911 change transfer method to ng_911 ..(removing ...)

 found = strTransfertoNumber.find("sip:");
 if ((eTransferMethod == mTANDEM)&& (found != string::npos))  {strNG911PSAP_TransferNumber = strTransfertoNumber; }

 found = strTransfertoNumber.find("sip:911@");
 if (found != string::npos) {strNG911PSAP_TransferNumber = strTransfertoNumber; eTransferMethod = mNG911_TRANSFER;}
/*
 found = strTransfertoNumber.find("sip:sos@");
 if (found != string::npos) {strNG911PSAP_TransferNumber = strTransfertoNumber; eTransferMethod = mNG911_TRANSFER;}
*/

 found = strTransfertoNumber.find("sip:sos@");
 if (found == string::npos) {
  strTransfertoNumber = RemoveSIPcolon(strTransfertoNumber);
  strTransfertoNumber = RemoveIPaddressFromPhoneNumber(strTransfertoNumber);
 }

 return true;
}

