/*****************************************************************************
* FILE: globalfunctions.h
*  
*
* DESCRIPTION:
*  External function declarations
*
*
*
* AUTHOR: 1/28/2009 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2009 Experient Corporation  
******************************************************************************/

#ifndef globalsfunctions_included_ 
#define globalsfunctions_included_

#include "header.h"
extern float             float_one_point_round           (float value);
extern bool              PhantomPort                     (string strData);
extern string            ANIfunction                     (ani_functions eANIfunction);
extern string            ALIfunction                     (ali_functions eALIfunction);
extern string            TransferMethod                  (transfer_method eTransferMethod);
extern int               ConvertLOGcodetoSYSlogCode      (int intLogCode);
extern Thread_Data       ThreadObjectWithTID             ( pid_t TID);
extern string            FreeswitchUsernameFromReferedBy (string strInput);
extern bool              FreeswitchDestNumberIsConfJoin  (string strInput);
extern bool              FreeswitchCurrentApplicationIsConference(string strInput);
extern string            ParseCallID_NumberFromAudiocodes(string strInput, bool boolReversed = false);
extern string            Determine_BCF_Key               (string strData);
extern string            CreateAbandonedList             ();
extern bool              IS_ABANDONED_POPUP              (string strInput);
extern bool              IS_PSAP_STATUS                  (string strInput);
extern void              experient_nanosleep             (long int iNumberofNanoSecods);
extern int               msleep                          (unsigned long milisec);
extern void              Clear_Workstations              ();
extern bool              fexists                         (const char *filename);
extern bool              UpdateSupressMessageNumberList  (string strList, bool boolNewValue);
extern void              CloseOutXMLfiles                ();
extern string            RemoveXMLHeader                 (string strInput);
extern string            RemoveXMLnamespace              (string strInput);
extern string            MonthFromLogFileName            (string strLogfile);
extern string            Month_DayFromLogFileName        (string strLogfile);
extern string            Return_Content                  (string strInput);
extern int               EventIdInteger                  (string strInput);
extern bool              IsAudiocodes                    (string strInput);
extern string            findWindowsUser                 (string strInput);
extern bool              IsCLIregistrationPacket         (string strInput);
extern string            TDD_Decode                      (string strInput);
extern string            AddNineForOutgoingCalls         (string strInput);
extern string            CheckForLongDistance            (string strInput);
extern void              SendCodingError                 (string strInput);
extern string            RemoveIPaddressFromPhoneNumber  (string strInput);
extern string            CallerNameLookup                (string strNumber);
extern void              Queue_CTI_Event                 (ExperientDataClass objData);
extern void              DisplayTrunkType                (int i);
extern transfer_method   DetermineTransferMethod         (string strInput);
extern string            FreeswitchUsernameFromChannel   (string strInput);
extern string            Ip4addressFromSDP               (string strInput);
extern string            IPaddressFromChannelName        (string strInput);
extern string            ALIProtocol                     (Port_ALI_Protocol_Type eALIportProtocol);
extern string            ConnectionType                  (Port_Connection_Type ePortConnectionType); 
extern string            URLdecode                       (string strInput);
extern void              set_mode                        (int key);
extern int               get_key                         ();
extern bool              RestartFreeswitch               ();
extern bool              SIPnumberhasAllNumbers          (string strInput);
extern string            RemoveSIPcolon                  (string strInput);
extern string            RemoveSIPcolonPlusATaddress     (string strInput);                      
extern bool              IsSIP_URI                       (string strInput);
extern int               NonBlockingNoEchoInputChar      ();
extern string            ConvertToXML                    (string strInput);
extern wstring           StringToWString                 (const string& s);
extern string            AMITestScript                   (string strInput);
extern bool              IsZombieChannel                 (string strInput);
extern bool              IsCloneChannel                  (string strInput);
extern bool              IsMASQChannel                   (string strInput);
extern bool              IsPositionChannel               (string strInput);
extern struct timespec   SecondsInTheFuture              (int i);
extern void              SleepforSeconds                 (int i);
extern void              SendRCC_Signal                  (rcc_state enumRCCstate, int i, bool withseamphore = true);
extern void              SendRCC_Signal                  (rcc_state enumRCCstate, int i, int iDelay);
extern void              SendRCC_Signal                  (rcc_state enumRCCstate,  ExperientDataClass objData);
extern int               noEchoInput                     ();
extern string            get_Local_Time_Stamp            ();
extern void              Controller_Status_Check         ();
extern int               get_current_year                ();
extern int               GMT_Offset                      ();
extern long double       Round_ld                        (long double ldArg, int intDigits);
extern string            HEX_Convert                     (unsigned char ch);
extern string            HEX_String                      (unsigned char* chInput, size_t sLength);
extern unsigned char     EncodeTwoDigits                 (string strInput);
extern unsigned long int NextTransactionID               ();
extern void              Display_Controller_Version      ();
extern string            ChannelPrefix                   (string strInput);
extern string            FQDNfromURI                     (string strInput);
extern bool              FileExists                      (string strFilename);
extern void              CheckFreeDiskSpace              (bool TerminalMode=false);
extern void              RemoveOldLogFiles               ();
extern int               PositionNumberfromExtension     (string strInput);
extern string            UUID_Generate_String            ();
extern string            int2str                         (unsigned long long int intArg);
extern string 	         int2strLZ                       (unsigned long long int intArg);
extern string            int2strTwoLZ                    (unsigned long long int intArg);
extern string            InsertString                    (string strData, string strSubstring, size_t Start, size_t Length);
extern int               GetDayofMonth                   ();
extern int               GetHour                         ();
extern string            CallStatusString                (int intStatusCode);
extern int               CallStatusCode                  (string stringArg);
extern int               CallStateCode                   (string stringArg);
extern string            CallStateString                 (int intCallStateCode);
extern int               ConvertCallStateToStatus        (int intStateCode);
extern int               DetermineIfRingingCall          (int intCallStatusCode);
extern string            ParseFreeswitchData             (string stringArg, string strKey);
extern string            ParseAsteriskMeetmeListData     (string stringArg, string strKey);
extern int               DecodeAsteriskActionId          (string strInput);
extern string            AsteriskPositionFromChannel            (string strInput);
extern string            AsteriskPositionFromANI                (string strInput);
extern string            EraseLocalTransferChannelSuffix        (string strArg);
extern string            AsteriskExtensionFromPositionChannel   (string strInput);
extern string            ParseAsteriskSIPpositionChannel        (string stringInput);
extern string            ParseConferencefromAsteriskLocalChannel(string stringInput);
extern int               ParseLineNumberFromChannelName         (string strInput);
extern bool              IsGUITransferLinkChannelOne            (string strInput);
extern string            ExtensionFromConferenceDisplay         (string strInput);
extern string            RemoveLeadingSpaces                    (string strInput);
extern string            RemoveTrailingSpaces                   (string strInput);
extern string            RemoveAllSpaces                        (string strInput); 
extern bool              CheckforManualBidTrunk                 (int intTrunk);
extern bool              spc_email_isvalid                      (const char *address);
extern string            ServerNametoPositionChannelKey         (unsigned int intNum);
extern string            get_ALI_Record_Local_TimeStamp         (timespec timespecArg);
extern string            get_ALI_Record_Local_TimeStamp         (time_t time_tArg);
extern string            DisplayMode                            (display_mode eDisplayMode);
extern string            MonitCommandLine                       (string strProgram, string strCommand);
extern void              BidNextGenURI                          (int iTableIndex, ExperientDataClass objData);
//extern string            fCheck_Delimiter                       ( string stringData, bool &boolDelimeterFound, char Delimiter1 = '\0' , char Delimiter2 = '\0', char Delimiter3 = '\0');
extern bool              ValidTenDigitNumber         (string strNumber);
extern bool              ValidExtension              (string strNumber);
extern string            AddParensToTenDigitNumber   (string strInput);
extern void              Console_Message             (string stringThreadCalling, string stringArg,  string stringArg1 = "",  string stringArg2 = "",
                                                      string stringArg3 = "",string stringArg4 = "",string stringArg5 = "");
extern string            Create_Message	             (string stringArg, string stringArg1 = "", string stringArg2 = "", string stringArg3 = "",
                                                     string stringArg4 = "",string stringArg5 = "",string stringArg6 = "");
extern void              Set_Timer_Delay             (struct itimerspec *itimerspecArg, int intArg2, int intArg3, int intArg4, int intArg5);
//extern string            Set911TransferPrefix        (string strInput);
extern float             RoundToThousanth            (timespec timespecArg);
extern string            DecimalsToString            (float floatArg);
extern string            get_time_stamp              (timespec timespecArg);
extern long double       time_difference             (timespec timespecArg1, timespec timespecArg2);
extern char              CheckSumGenerate            (string stringText, int intLength);
extern char              BlockCheckCharacter         (string stringText, int intLength);
extern string            Thread_Calling              (threadorPorttype enumThreadCalling);
extern void              Semaphore_Error             (int intReturnCode, threadorPorttype enumThreadCalling, sem_t *sem_tSempointer, 
                                                     string stringMessage, int intNumFails);  
extern int               Transmit_Data               (threadorPorttype enumPortType, int intportNum,const char* charTextMessage, int intTextLength,int intALIPair = 0);
extern void              enQueue_Message             (threadorPorttype enumThread, MessageClass objArg1 );
extern string            GetLogFileName              (log_type enumLogtype);
extern string            ASCII_Convert               (unsigned char charARG);
extern string            seconds2time                (long long int intArg);
extern void              Check_Port_Down_Notification(threadorPorttype enumPortType, int intNumofPorts, ExperientCommPort* CommPort);
extern void              email_headers_init          ();
extern string            ASCII_String                (const char* charInput, size_t length);
extern bool              Ports_Ready_To_Shut_Down    (ExperientCommPort Port[], int intNumPorts);
extern bool              IsAnyPortActive             (ExperientCommPort Port[], int intNumPorts);
extern void              ClearPorts                  (ExperientCommPort Port[], int intNumPorts);
extern bool              TimeCheckBefore             (timespec timespecArg1, timespec timespecArg2);
extern int               PortQueueRoundRobin         (queue <DataPacketIn> *PortQueue, int intNumPorts,  int intFirst, sem_t* mutexlock);
extern bool              Validate_Integer            (string stringArg);
extern bool              ValidateFieldAndRange       (threadorPorttype enumArg, string stringArg, string strRawData, int intPortNum, string stringField, 
                                                     unsigned long long int intHigh, unsigned int intLow);
extern string            MSRPCheckForPound           (string strInput);
extern void              enqueue_Main_Input          (threadorPorttype enumThdCallng, ExperientDataClass objData);
extern string            BuildSTXtoETX               (string stringInput);
extern string            StripURNnenaCompanyID       (string stringInput);
extern bool              Validate_Trunk_Number       (int intArg);
extern bool              char2bool                   (bool &boolARG, string stringArg);
extern string            ReformatLongOutputToLog     (string stringInput, string stringCallData, string stringPrefix = "", bool NewLine = false);
extern string            ReformatALIOutputToLog      (string stringInput, string stringCallData, string stringPrefix, bool bool_LF_AS_CR);
extern string            ReformatALIOutputToHTML     (string stringInput,  bool bool_LF_AS_CR);
extern string            ConvertWhiteSpacestoHTML    (string strInput);
extern string            ReformatTDDConversationToLog(string stringInput, string stringCallData, string stringPrefix);
extern string            FindandReplace              (string stringArg, string stringFind, string stringReplace);
extern string            FindandReplaceALL           (string stringArg, string stringFind, string stringReplace);
extern int               NTP_Time_Server_Control     (threadorPorttype enumThread, bool TurnOn);
extern void              Abnormal_Exit               (enum threadorPorttype  enumThread, int RetCode, string sMessage, string sArg1 = "", string sArg2 = "",
                                                     string sArg3 = "", string sArg4 = "", string sArg5 = "");
extern string            reformat_Ali_Log_to_Email   (string stringArg, int intPrefixLength);
extern bool              CheckAllPortsDown           (threadorPorttype enumArg, bool ShowMessage, bool exitonfirstsuccess);
extern string            Create_INI_Key              (string stringArg1, int intArg1, string stringArg2="", int intArg2=0, string stringArg3 = "", int intArg3=0, string stringArg4="", int intArg4=0); 
//extern int               ConvertTransferTypeToTrunkType(transfer_method eXferMethod);
extern unsigned long long int char2int                      (const char* CharArg);
extern unsigned long long int Julian_Date                   (timespec timespecTimeNow);
extern unsigned long long int Find_10_Digit_Telephone_Number(string strInput,size_t Column);
extern string             StripLeadingZeroes	             (string stringArg);
extern void                Queue_WRK_Event                   (ExperientDataClass objData);
extern void                Queue_ANI_Event                   (ExperientDataClass objData);
extern void                Queue_AMI_Input                   (ExperientDataClass objData);
extern void                SendANItoALI                      (ExperientDataClass objData);
//extern int                 DetermineTrunkTypeFromNumberDialed(string strInput);
extern Telephone_Data_type TelephoneDataType                 (string strInput);
extern string              TelephoneDataType                 (Telephone_Data_type eType);
extern string              FindIP_AddressAfterComma          (string strInput);
extern string              StripPlusOne                      (string strInput);
extern string              StripOnePlus                      (string strInput);
extern string              RemoveTelColon                    (string strInput);
extern string              after_last_colon                  (string strInput);
extern string              RemoveSemicolonToEnd              (string strInput);
extern bool                IsCallerInConferenceDisplay       (string strInput);
extern string              fCallerIDGUIformat                (string strInput);
extern string              CustNameGUIformat                 (string strInput, bool boolWithBrackets);
extern void                CloseOutXMLlogfile                (string strCurrentMonthXMLLogName);
extern void                SendDelayedHangup                 (string strChannel, int iDelay);
extern int 		   PositionNumberFromMSRPpresenceID  (string strInput);
extern void                SendSignaltoThreads               (   );
extern bool                IsControllerBlindTransfer         (string strInput);
extern bool                IsPositionPartofAGroupThatIsOnTheCall(int iPosition, Call_Data objCallData);
extern string              StateAbbreviation                 (string strInput);
extern string              CardinalHeading                   (string strInput);
extern string              PostalStreetSuffix                (string strInput);
extern string              NAPTR                             (string strURI, int i=0);

//test
extern void             MemoryUsage();
extern void 		enterSyncTableTimeToBroadcastData(long double ldArg);
extern void 	        SyncTableTimeToBroadcastStatistics();
extern void 		enterSyncTimeStampData(struct timespec timespecArg);
extern void 		SyncTableBroadcastStatistics();
extern void 		Statistics(long double array[], int size);
extern void             FillConferenceObject();
extern void             testaddconnection();
extern transfer_method  TransferMethod                      (string strInput);
extern sip_ng911_transfer_type NG911_TRANSFER_TYPE          (string strInput);
extern string                  NG911_TRANSFER_TYPE          (sip_ng911_transfer_type etype);
extern sip_ng911_transfer_type NG911_TRANSFER_TYPE_FROM_CLI (string strInput);
extern bool             SIP_TRANSFER_FROM_POSTION           (string strInput);
extern string           ConferenceMemberString              (conference_member eConf, int i);
extern bool 		ThreadsafeEmptyQueueCheck		(queue <ExperientDataClass>* Queue, sem_t* MutexLock );
extern bool 		ThreadsafeEmptyQueueCheck		(deque <ExperientDataClass>* Queue, sem_t* MutexLock );
extern bool             ThreadsafeEmptyQueueCheck               (queue <DataPacketIn>* DataQueue, sem_t* MutexLock );
extern bool             ThreadsafeEmptyQueueCheck               (queue <MessageClass>* MessageQueue, sem_t* MutexLock );
#endif


