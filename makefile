CC=g++
CFLAGS= -ggdb -rdynamic -o0 -std=c++0x -mtune=generic -Wall -DUNIX64 -Wno-unused -Wno-deprecated
OS_ARCH=x64
LFLAGS = -L../lib/ 
LDFLAGS= -L -pthread -lncurses -DUNIX64
LIBS= -pthread -lipworks -ldl -lssl -lcrypto -lz -lrt -luuid
R_PATH=-Wl,-rpath,../../../lib/

SOURCES=  Experient_Controller.cpp ExperientDataClass.cpp ali_e2_functions.cpp MessageClass.cpp CommPortClasses.cpp cad.cpp ali.cpp  ani.cpp wrk.cpp log.cpp rcc.cpp \
          error_routines.cpp globalfunctions.cpp telephone_device_functions.cpp call_data_functions.cpp freeswitch_cli.cpp sigsegv.cpp \
          cti.cpp freeswitch_data_functions.cpp link_data_functions.cpp channel_data_functions.cpp transfer_data_functions.cpp \
          trunk_sequence_mapping.cpp ali_I3_functions.cpp LIS.cpp sms_functions.cpp initialization.cpp inotify.cpp dashboard.cpp adr.cpp


OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=ng911.exe

.PHONY: depend clean

all: $(EXECUTABLE)
	@echo  $(EXECUTABLE) Compiled Successfully!

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(EXECUTABLE) $(OBJECTS) $(LFLAGS) $(LIBS)
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# DO NOT DELETE THIS LINE -- make depend needs

install: all
	sudo chown root ng911.exe
#	sudo chmod 4555 ng911.exe
	sudo chmod 755 ng911.exe
	sudo setcap cap_net_raw+ep ng911.exe
	sudo mv ng911.exe /datadisk1/ng911
clean:
	rm -f *.o


