/*****************************************************************************
* FILE: ExperientDataClass.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the ExperientDataClass 
*
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/


#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"                          // XML Software
#include "/datadisk1/src/xmlParser/xmlParser.cpp"                        // XML Software

 
//fwd function
string ConvertXMLError2String(XMLError xe);
string EncodeBase64(string strInput);
bool   CallFromThreeWayContext(string strArg);
bool   CallFromDefaultContext(string strArg);
bool   CallFromMSRPContext(string strArg);
bool   CallFromBlindTransferContext(string strArg);
bool   CallFromFreeswitchTransfer(string strArg);
bool   CallerDirectionOutbound(string strArg);
string Freeswitch_Channel_Progress_Determine_Trunk_Number(bool boolLineRoll, Freeswitch_Data objFreeswitchData, string strInput);
int    Determine_GUI_LINE_VIEW_Number(string strInput);
bool   DestinationNumberisCONF_JOIN(string strInput);
bool   DestinationNumberisVALET_JOIN(string strInput);
string Freeswitch_Current_Application(string strInput);
bool   BLIND_TRANSFER_GUI_FROM_CLI(string strInput);
bool   BLIND_TRANSFER_FAIL_RINGBACK(string strInput);
int    ChannelStateNumber(string strInput);
bool   PARK_PICKUP(string strInput);

// Globals
extern ExperientCommPort                       	CADPort                                 [NUM_CAD_PORTS_MAX+1];
extern vector <string>                         	NPD;
extern vector <string>                         	vstringWRK_XML_KEYS;
extern vector <string>                         	vstrCLASS_OF_SERVICE_ABBV;
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data			       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern Trunk_Type_Mapping		       	TRUNK_TYPE_MAP;
extern Port_Data                               	objBLANK_ANI_PORT;
extern Port_Data                               	objBLANK_ALI_PORT;
extern Port_Data                               	objBLANK_WRK_PORT;
extern Port_Data			       	objBLANK_AMI_PORT;
 
//ExperientDataClass functions implementation

    // constructor
    ExperientDataClass::ExperientDataClass()
     {
         boolForceDisconnect                     = false;
         boolSMSmessage                          = false;
         enumThreadSending                       = NO_PORT_OR_THREAD;
         enumWrkFunction                         = BAD_KEY;
         ALIData.fClear();
         enumANISystem				 = NO_SYSTEM;
         enumANIFunction                         = UNCLASSIFIED;
         enumRCCstate                            = RADIO;
         enumRCCfunction                         = NONE_DEFINED;
         objGeoLocation.fClear();
         strNenaCallId                           = "";
         strNenaIncidentId                       = "";
         strRawCallinfoHeader                    = "";
         strPidfloByValEntity                    = "";
         strCallInfoProviderCID                  = "";
         strCallInfoProviderURL                  = "";
         strCallInfoServiceCID                   = "";
         strCallInfoServiceURL                   = "";
         strCallInfoSubscriberCID                = "";
         strCallInfoSubscriberURL                = "";         
         strDTMFChar                             = "";
         intNumberAutoALIBids                    = 0;
         intANIPortCallReceived			 = 0;
         intCallStatusCode                       = 0;
         intCallStateCode                        = 0;
         stringRingingTimeStamp                  = "";
         stringConnectTimeStamp                  = "";
         stringDisConnectTimeStamp               = "";
         stringANIAlarmCode                      = "";
         stringANIAlarmMessage                   = "";
         timespecTimeRecordReceivedStamp.tv_sec  = 0;
         timespecTimeRecordReceivedStamp.tv_nsec = 0;
         boolIsHeartbeat			 = false;
         boolConnectOutofOrder                   = false;
         boolRinging                             = false;
         boolDisconnect                          = false;
	 boolConnect				 = false;
         boolRingBack                            = false;
         boolAbandoned				 = false;
         boolMSRPnoAnswer                        = false;
         timespecTimeAbandonedStamp.tv_sec       = 0;
         timespecTimeAbandonedStamp.tv_nsec      = 0;
         timespecAbandonedPopUp.tv_sec           = 0;
         timespecAbandonedPopUp.tv_nsec          = 0;
         timespecSingleChannel.tv_sec            = 0;
         timespecSingleChannel.tv_nsec           = 0;
         timespecRinging.tv_sec                  = 0;
         timespecRinging.tv_nsec                 = 0;
         strCallOriginator                       = "";
         stringWRKStationIPAddr                  = "";
         stringWRKStationListenPort              = "";
         stringWRKStationMSGText		 = "";
         intWorkstation_TCPconnectionID          = 0;
         strGUIVersion                           = "";
         strGUIxmlVersion                        = "";
         strWindowsUser                          = "";
         strWindowsOS                            = "";
         intLogType                              = 0;
         intLogMessageNumber                     = 0;
         stringXMLString                         = "";
         stringTimeOfEvent                       = "";
         boolBroadcastXMLtoSinglePosition        = false;
         boolReloadWorkstationGrid               = false;
         boolWorkstationIsOnACall                = false;
         IndexOpenForDialing                     = 0;
         intWorkStation                          = 0;
         boolDoNotSendToCAD                      = false;
         boolCADEraseMsg                         = false;
         stringCADEraseMsg                       = "";
         intCADEraseMsgLength                    = 0;
         boolRecordRebuiltFlag                   = false;
         strEventUniqueID                        = "";
         WindowButtonData.fClear();
         TDDdata.fClear();
         TextData.fClear();
         CallData.fClear();
         PSAPstatus.fClear();
         vPSAPstatus.clear();
         strErrorMessage.clear();
	 LastTest				 = "LAST FIELD";
     }
    			
    // destructor        
    ExperientDataClass::~ExperientDataClass(){}

    //Copy Constructor
    ExperientDataClass::ExperientDataClass(const ExperientDataClass *a) {

      boolForceDisconnect                       = a->boolForceDisconnect;
      boolSMSmessage                            = a->boolSMSmessage;
      enumThreadSending                         = a->enumThreadSending;
      enumWrkFunction                           = a->enumWrkFunction;
      ALIData                                   = a->ALIData;
      enumANISystem				= a->enumANISystem;
      enumANIFunction                           = a->enumANIFunction;
      enumRCCstate                              = a->enumRCCstate;
      enumRCCfunction                           = a->enumRCCfunction;
      objGeoLocation                            = a->objGeoLocation;
      strNenaCallId                             = a->strNenaCallId;
      strNenaIncidentId                         = a->strNenaIncidentId;
      strRawCallinfoHeader                      = a->strRawCallinfoHeader;
      strPidfloByValEntity                      = a->strPidfloByValEntity;
      strCallInfoProviderCID                    = a->strCallInfoProviderCID;
      strCallInfoProviderURL                    = a->strCallInfoProviderURL;
      strCallInfoServiceCID                     = a->strCallInfoServiceCID;
      strCallInfoServiceURL                     = a->strCallInfoServiceURL;
      strCallInfoSubscriberCID                  = a->strCallInfoSubscriberCID;
      strCallInfoSubscriberURL                  = a->strCallInfoSubscriberURL;
      strDTMFChar                               = a->strDTMFChar;
      FreeswitchData                            = a->FreeswitchData;
      intNumberAutoALIBids                      = a->intNumberAutoALIBids;
      intANIPortCallReceived                    = a->intANIPortCallReceived;
      intCallStatusCode                         = a->intCallStatusCode;
      intCallStateCode                          = a->intCallStateCode;
      stringRingingTimeStamp                    = a->stringRingingTimeStamp;
      stringConnectTimeStamp                    = a->stringConnectTimeStamp;
      stringDisConnectTimeStamp                 = a->stringDisConnectTimeStamp;
      stringANIAlarmCode                        = a->stringANIAlarmCode;
      stringANIAlarmMessage                     = a->stringANIAlarmMessage;
      timespecTimeRecordReceivedStamp.tv_sec    = a->timespecTimeRecordReceivedStamp.tv_sec;
      timespecTimeRecordReceivedStamp.tv_nsec   = a->timespecTimeRecordReceivedStamp.tv_nsec;
      boolIsHeartbeat                           = a->boolIsHeartbeat;
      boolConnectOutofOrder                     = a->boolConnectOutofOrder;
      boolRinging                               = a->boolRinging;
      boolDisconnect                            = a->boolDisconnect;
      stringWRKStationIPAddr                    = a->stringWRKStationIPAddr;
      stringWRKStationListenPort                = a->stringWRKStationListenPort;
      stringWRKStationMSGText			= a->stringWRKStationMSGText;
      intWorkstation_TCPconnectionID            = a->intWorkstation_TCPconnectionID;
      strGUIVersion                             = a->strGUIVersion; 
      strGUIxmlVersion                          = a->strGUIxmlVersion;
      strWindowsUser                            = a->strWindowsUser;
      strWindowsOS                              = a->strWindowsOS;
      intLogType                                = a->intLogType;
      intLogMessageNumber                       = a->intLogMessageNumber;
      stringXMLString                           = a->stringXMLString;
      stringTimeOfEvent                         = a->stringTimeOfEvent;
      boolBroadcastXMLtoSinglePosition          = a->boolBroadcastXMLtoSinglePosition;
      boolReloadWorkstationGrid                 = a->boolReloadWorkstationGrid;
      boolWorkstationIsOnACall                  = a->boolWorkstationIsOnACall;
      IndexOpenForDialing                       = a->IndexOpenForDialing;
      intWorkStation                            = a->intWorkStation;
      boolDoNotSendToCAD                        = a->boolDoNotSendToCAD;
      boolCADEraseMsg                           = a->boolCADEraseMsg;
      stringCADEraseMsg                         = a->stringCADEraseMsg;
      intCADEraseMsgLength                      = a->intCADEraseMsgLength;
      boolRecordRebuiltFlag                     = a->boolRecordRebuiltFlag;
      strEventUniqueID                          = a->strEventUniqueID;
      boolConnect                               = a->boolConnect;
      boolRingBack                              = a->boolRingBack;
      boolAbandoned                             = a->boolAbandoned;
      boolMSRPnoAnswer                          = a->boolMSRPnoAnswer;
      timespecTimeAbandonedStamp.tv_sec         = a->timespecTimeAbandonedStamp.tv_sec;
      timespecTimeAbandonedStamp.tv_nsec        = a->timespecTimeAbandonedStamp.tv_nsec;
      timespecAbandonedPopUp.tv_sec             = a->timespecTimeAbandonedStamp.tv_sec;
      timespecAbandonedPopUp.tv_nsec            = a->timespecTimeAbandonedStamp.tv_nsec;
      timespecSingleChannel.tv_sec              = a->timespecSingleChannel.tv_sec;
      timespecSingleChannel.tv_nsec             = a->timespecSingleChannel.tv_nsec;
      timespecRinging.tv_sec                    = a->timespecRinging.tv_sec;
      timespecRinging.tv_nsec                   = a->timespecRinging.tv_nsec;
      strCallOriginator                         = a->strCallOriginator;
      TDDdata                                   = a->TDDdata;
      TextData                                  = a->TextData;
      CallData                                  = a->CallData;
      WindowButtonData                          = a->WindowButtonData;
      PSAPstatus                                = a->PSAPstatus;
      vPSAPstatus                               = a->vPSAPstatus;
      strErrorMessage                           = a->strErrorMessage;
      LastTest					= a->LastTest;
     }

   ExperientDataClass& ExperientDataClass::operator=(const ExperientDataClass& a)    {
     if (&a == this ) {return *this;}
     boolForceDisconnect                       = a.boolForceDisconnect;
     boolSMSmessage                            = a.boolSMSmessage;
     enumThreadSending                         = a.enumThreadSending;
     enumWrkFunction                           = a.enumWrkFunction;
     ALIData                                   = a.ALIData;
     enumANISystem			       = a.enumANISystem;
     enumANIFunction                           = a.enumANIFunction;
     enumRCCstate                              = a.enumRCCstate;
     enumRCCfunction                           = a.enumRCCfunction;
     objGeoLocation                            = a.objGeoLocation;
     strNenaCallId                             = a.strNenaCallId;
     strNenaIncidentId                         = a.strNenaIncidentId;
     strRawCallinfoHeader                      = a.strRawCallinfoHeader;
     strPidfloByValEntity                      = a.strPidfloByValEntity;
     strCallInfoProviderCID                    = a.strCallInfoProviderCID;
     strCallInfoProviderURL                    = a.strCallInfoProviderURL;
     strCallInfoServiceCID                     = a.strCallInfoServiceCID;
     strCallInfoServiceURL                     = a.strCallInfoServiceURL;
     strCallInfoSubscriberCID                  = a.strCallInfoSubscriberCID;
     strCallInfoSubscriberURL                  = a.strCallInfoSubscriberURL;
     strDTMFChar                               = a.strDTMFChar;
     FreeswitchData                            = a.FreeswitchData;
     intNumberAutoALIBids                      = a.intNumberAutoALIBids;
     intANIPortCallReceived                    = a.intANIPortCallReceived;
     intCallStatusCode                         = a.intCallStatusCode;
     intCallStateCode                          = a.intCallStateCode;
     stringRingingTimeStamp                    = a.stringRingingTimeStamp;
     stringConnectTimeStamp                    = a.stringConnectTimeStamp;
     stringDisConnectTimeStamp                 = a.stringDisConnectTimeStamp;
     stringANIAlarmCode                        = a.stringANIAlarmCode;
     stringANIAlarmMessage                     = a.stringANIAlarmMessage;
     timespecTimeRecordReceivedStamp.tv_sec    = a.timespecTimeRecordReceivedStamp.tv_sec;
     timespecTimeRecordReceivedStamp.tv_nsec   = a.timespecTimeRecordReceivedStamp.tv_nsec;
     boolIsHeartbeat                           = a.boolIsHeartbeat;
     boolConnectOutofOrder                     = a.boolConnectOutofOrder;
     boolRinging                               = a.boolRinging;
     boolDisconnect                            = a.boolDisconnect;
     stringWRKStationIPAddr                    = a.stringWRKStationIPAddr;
     stringWRKStationListenPort                = a.stringWRKStationListenPort;
     stringWRKStationMSGText		       = a.stringWRKStationMSGText;
     intWorkstation_TCPconnectionID            = a.intWorkstation_TCPconnectionID;
     strGUIVersion                             = a.strGUIVersion; 
     strGUIxmlVersion                          = a.strGUIxmlVersion;
     strWindowsUser                            = a.strWindowsUser;
     strWindowsOS                              = a.strWindowsOS;
     intLogType                                = a.intLogType;
     intLogMessageNumber                       = a.intLogMessageNumber;
     stringXMLString                           = a.stringXMLString;
     stringTimeOfEvent                         = a.stringTimeOfEvent;
     boolBroadcastXMLtoSinglePosition          = a.boolBroadcastXMLtoSinglePosition;
     boolReloadWorkstationGrid                 = a.boolReloadWorkstationGrid;
     boolWorkstationIsOnACall                  = a.boolWorkstationIsOnACall;
     IndexOpenForDialing                       = a.IndexOpenForDialing;
     intWorkStation                            = a.intWorkStation;
     boolDoNotSendToCAD                        = a.boolDoNotSendToCAD;
     boolCADEraseMsg                           = a.boolCADEraseMsg;
     stringCADEraseMsg                         = a.stringCADEraseMsg;
     intCADEraseMsgLength                      = a.intCADEraseMsgLength;
     boolRecordRebuiltFlag                     = a.boolRecordRebuiltFlag;
     strEventUniqueID                          = a.strEventUniqueID;
     boolConnect                               = a.boolConnect;
     boolRingBack                              = a.boolRingBack;
     boolAbandoned                             = a.boolAbandoned;
     boolMSRPnoAnswer                          = a.boolMSRPnoAnswer;
     timespecTimeAbandonedStamp.tv_sec         = a.timespecTimeAbandonedStamp.tv_sec;
     timespecTimeAbandonedStamp.tv_nsec        = a.timespecTimeAbandonedStamp.tv_nsec;
     timespecAbandonedPopUp.tv_sec             = a.timespecTimeAbandonedStamp.tv_sec;
     timespecAbandonedPopUp.tv_nsec            = a.timespecTimeAbandonedStamp.tv_nsec;
     timespecSingleChannel.tv_sec              = a.timespecSingleChannel.tv_sec;
     timespecSingleChannel.tv_nsec             = a.timespecSingleChannel.tv_nsec;
     timespecRinging.tv_sec                    = a.timespecRinging.tv_sec;
     timespecRinging.tv_nsec                   = a.timespecRinging.tv_nsec;
     strCallOriginator                         = a.strCallOriginator;
     TDDdata                                   = a.TDDdata;
     TextData                                  = a.TextData;
     CallData                                  = a.CallData;
     PSAPstatus                                = a.PSAPstatus;
     vPSAPstatus                               = a.vPSAPstatus;
     WindowButtonData                          = a.WindowButtonData;
     strErrorMessage                           = a.strErrorMessage;
     LastTest				       = a.LastTest;
     return *this;
    }  


    void ExperientDataClass::fClear_Record() 
        {
         boolForceDisconnect                     = false;
         boolSMSmessage                          = false;
         enumThreadSending                       = NO_PORT_OR_THREAD;
         enumWrkFunction                         = BAD_KEY;
         ALIData.fClear();
         intNumberAutoALIBids                    = 0;
         enumANISystem				 = enumANI_SYSTEM;
         enumANIFunction                         = UNCLASSIFIED;
         enumRCCstate                            = RADIO;
         enumRCCfunction                         = NONE_DEFINED;
         objGeoLocation.fClear();
         strNenaCallId                           = "";
         strNenaIncidentId                       = "";
         strRawCallinfoHeader                    = "";
         strPidfloByValEntity                    = "";
         strCallInfoProviderCID                  = "";
         strCallInfoProviderURL                  = "";
         strCallInfoServiceCID                   = "";
         strCallInfoServiceURL                   = "";
         strCallInfoSubscriberCID                = "";
         strCallInfoSubscriberURL                = "";
         strDTMFChar                             = "";
         intANIPortCallReceived			 = 0;
         intCallStatusCode                       = 0;
         intCallStateCode                        = 0;
         stringRingingTimeStamp                  = "";
         stringConnectTimeStamp                  = "";
         stringDisConnectTimeStamp               = "";
         stringANIAlarmCode                      = "";
         stringANIAlarmMessage                   = "";
         timespecTimeRecordReceivedStamp.tv_sec  = 0;
         timespecTimeRecordReceivedStamp.tv_nsec = 0;
         boolIsHeartbeat			 = false;
         boolConnectOutofOrder                   = false;
         boolRinging                             = false;
         boolDisconnect                          = false;
         boolConnect                             = false;
         boolRingBack				 = false;
         boolAbandoned                           = false;
         boolMSRPnoAnswer                        = false;
         timespecTimeAbandonedStamp.tv_sec       = 0;
         timespecTimeAbandonedStamp.tv_nsec      = 0;
         timespecAbandonedPopUp.tv_sec           = 0;
         timespecAbandonedPopUp.tv_nsec          = 0;
         timespecSingleChannel.tv_sec            = 0;
         timespecSingleChannel.tv_nsec           = 0;
         timespecRinging.tv_sec                  = 0;
         timespecRinging.tv_nsec                 = 0;      
         strCallOriginator                       = "";
         stringWRKStationIPAddr                  = "";
         stringWRKStationListenPort              = "";
         stringWRKStationMSGText		 = "";
         intWorkstation_TCPconnectionID          = 0;
         strGUIVersion                           = "";
         strGUIxmlVersion                        = "";
         strWindowsUser                          = "";
         strWindowsOS                            = "";
         intLogType                              = 0;
         intLogMessageNumber                     = 0;
         stringXMLString                         = "";
         stringTimeOfEvent                       = "";
         boolBroadcastXMLtoSinglePosition        = false;
         boolReloadWorkstationGrid               = false;
         boolWorkstationIsOnACall                = false;
         IndexOpenForDialing                     = 0;
         intWorkStation                          = 0;
         boolDoNotSendToCAD                      = false;
         boolCADEraseMsg                         = false;
         stringCADEraseMsg                       = "";
         intCADEraseMsgLength                    = 0;
         boolRecordRebuiltFlag                   = false;
         strEventUniqueID                        = "";
         WindowButtonData.fClear();
         TDDdata.fClear();
         TextData.fClear();
         CallData.fClear();
         PSAPstatus.fClear();
         vPSAPstatus.clear();
         strErrorMessage.clear();
         LastTest				 = "LAST FIELD";      

 	
        }// end fClear_Record


    Call_Data ExperientDataClass::fCallData()
     {
      return CallData;
     }
  
//Constructor
PSAP_STATUS::PSAP_STATUS() {

 strPSAPname.clear();
 iPositionsOnline 		= 0;
 iPositionsOnEmergencyCall 	= 0;
 iPositionsOnAdminCall 		= 0;
 i911Calls			= 0;
 i911CallsOnHold		= 0;
 i911CallsAbandoned		= 0;
 i911CallsParked		= 0;
 i911CallsRinging		= 0;
 iAdminCalls			= 0;
 iOutboundAdminCalls		= 0;
 iInboundAdminCalls		= 0;
 iAdminCallsOnHold		= 0;
 iAdminCallsAbandoned		= 0;
 iAdminCallsParked		= 0;
 iAdminCallsRinging		= 0;
 iMSRPCalls			= 0;
 iMSRPCallsOnHold		= 0;
 iMSRPCallsAbandoned		= 0;
 iMSRPCallsRinging		= 0;
} 
 
bool PSAP_STATUS::fCompare(const PSAP_STATUS& a) const {

 bool boolRunningCheck = true;

 // Only check the name for now .....
 boolRunningCheck = boolRunningCheck && (this->strPSAPname == a.strPSAPname);

 return boolRunningCheck;
}
 
void PSAP_STATUS::fClear() {

 strPSAPname.clear();
 iPositionsOnline 		= 0;
 iPositionsOnEmergencyCall 	= 0;
 iPositionsOnAdminCall 		= 0;
 i911Calls			= 0;
 i911CallsOnHold		= 0;
 i911CallsAbandoned		= 0;
 i911CallsParked		= 0;
 i911CallsRinging		= 0;
 iAdminCalls			= 0;
 iOutboundAdminCalls		= 0;
 iInboundAdminCalls		= 0;
 iAdminCallsOnHold		= 0;
 iAdminCallsAbandoned		= 0;
 iAdminCallsParked		= 0;
 iAdminCallsRinging		= 0;
 iMSRPCalls			= 0;
 iMSRPCallsOnHold		= 0;
 iMSRPCallsAbandoned		= 0;
 iMSRPCallsRinging		= 0;
}

void PSAP_STATUS::fClearCounters() {

 // Keep Name
 iPositionsOnline 		= 0;
 iPositionsOnEmergencyCall 	= 0;
 iPositionsOnAdminCall 		= 0;
 i911Calls			= 0;
 i911CallsOnHold		= 0;
 i911CallsAbandoned		= 0;
 i911CallsParked		= 0;
 i911CallsRinging		= 0;
 iAdminCalls			= 0;
 iOutboundAdminCalls		= 0;
 iInboundAdminCalls		= 0;
 iAdminCallsOnHold		= 0;
 iAdminCallsAbandoned		= 0;
 iAdminCallsParked		= 0;
 iAdminCallsRinging		= 0;
 iMSRPCalls			= 0;
 iMSRPCallsOnHold		= 0;
 iMSRPCallsAbandoned		= 0;
 iMSRPCallsRinging		= 0;
}



    void ExperientDataClass:: fCAD_Strip_Uneeded_Data()
       {
	stringXMLString     = "";
        TDDdata.fClear();
       }
    void ExperientDataClass::fSet_Time_Recieved() {clock_gettime(CLOCK_REALTIME, &timespecTimeRecordReceivedStamp);}

    bool ExperientDataClass::fIs_HeartBeat() { return boolIsHeartbeat; }

    void ExperientDataClass::fSet_Heartbeat_Record()						
	{
	 CallData.stringPosn			= "";
	 CallData.stringTrunk    		= "";
	 ALIData.stringAliText	                = "Heartbeat Record";
	 boolIsHeartbeat			= true;
	}

 
    
    bool ExperientDataClass::fIs_A_Record()
        {
         if (timespecTimeRecordReceivedStamp.tv_sec)	{return true; }
         else                        			{return false;}
        }
    void ExperientDataClass::fSetNenaIds() {
         MessageClass 						objMessage;
         Port_Data    						objPortData;
         int          						intPortNum;
         extern ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1];

         intPortNum = this->intANIPortCallReceived;
         objPortData = ANIPort[intPortNum].UDP_Port.fPortData(); 
        
         this->strNenaCallId            = this->ALIData.I3Data.strNenaCallId;
         this->strNenaIncidentId        = this->ALIData.I3Data.strNenaIncidentId;
         this->strRawCallinfoHeader     = this->ALIData.I3Data.strRawCallinfoHeader;
         this->strPidfloByValEntity     = this->ALIData.I3Data.strEntity;
         this->strCallInfoProviderCID   = this->ALIData.I3Data.strCallInfoProviderCID;
         this->strCallInfoProviderURL   = this->ALIData.I3Data.strCallInfoProviderURL;
         this->strCallInfoServiceCID    = this->ALIData.I3Data.strCallInfoServiceCID;
         this->strCallInfoServiceURL    = this->ALIData.I3Data.strCallInfoServiceURL;
         this->strCallInfoSubscriberCID = this->ALIData.I3Data.strCallInfoSubscriberCID;
         this->strCallInfoSubscriberURL = this->ALIData.I3Data.strCallInfoSubscriberURL;

         if (this->strNenaCallId.length()) {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 341, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , this->CallData, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_341, this->strNenaCallId);
          enQueue_Message(ANI,objMessage);
         }
         if (this->strNenaIncidentId.length()) {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 341, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , this->CallData, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_341a, this->strNenaIncidentId);
          enQueue_Message(ANI,objMessage);
         }
         if (this->strPidfloByValEntity.length()) {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 341, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , this->CallData, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_341b, this->strPidfloByValEntity);
          enQueue_Message(ANI,objMessage);
         }     
    return;
    }

    void ExperientDataClass::fSetCadRecord()
        {
         size_t found;
         string strCR = "\r";
         string   strReplacePositionInALI = ALIData.stringAliText;
         boolIsHeartbeat				= false;
         ALIData.stringAliTextMessage 	= charSTX;               ALIData.stringAliTextMessage += ALIData.stringType; ALIData.stringAliTextMessage += CallData.stringPosn;
         ALIData.stringAliTextMessage  += ALIData.stringAliText; ALIData.stringAliTextMessage += charETX;
         ALIData.stringAliTextMessage  += BlockCheckCharacter( ALIData.stringAliTextMessage, ALIData.stringAliTextMessage.length());
         ALIData.intAliTextMessageLength	= ALIData.stringAliTextMessage.length(); 

         ALIData.stringALiTestMessageWOCR = charSTX; 		ALIData.stringALiTestMessageWOCR += ALIData.stringType; ALIData.stringALiTestMessageWOCR += CallData.stringPosn;
         ALIData.stringALiTestMessageWOCR  += FindandReplace(ALIData.stringAliText, strCR, "");
         ALIData.stringALiTestMessageWOCR += charETX;
         ALIData.stringALiTestMessageWOCR  += BlockCheckCharacter( ALIData.stringALiTestMessageWOCR, ALIData.stringALiTestMessageWOCR.length());
         ALIData.intAliTextMessageWOCRLength	= ALIData.stringALiTestMessageWOCR.length();       

         if (strReplacePositionInALI.length() < 3) {SendCodingError("ExperientDataClass::fSetCadRecord ALIData.stringAliText length too short!"); return;}
         strReplacePositionInALI.replace(1,2,CallData.stringPosn);
         ALIData.strCADmessageWithoutHeader 	= charSTX;
         ALIData.strCADmessageWithoutHeader    += strReplacePositionInALI;
         ALIData.strCADmessageWithoutHeader    += charETX;
         ALIData.strCADmessageWithoutHeader    += BlockCheckCharacter( ALIData.strCADmessageWithoutHeader, ALIData.strCADmessageWithoutHeader.length());
         ALIData.intCADmessageWithoutHeaderLength	= ALIData.strCADmessageWithoutHeader.length();

         ALIData.strCADmessageWithoutHeaderWOCR 	= charSTX;
         ALIData.strCADmessageWithoutHeaderWOCR    += FindandReplace(strReplacePositionInALI, strCR, "");
         ALIData.strCADmessageWithoutHeaderWOCR    += charETX;
         ALIData.strCADmessageWithoutHeaderWOCR    += BlockCheckCharacter( ALIData.strCADmessageWithoutHeader, ALIData.strCADmessageWithoutHeader.length());
         ALIData.intCADmessageWithoutHeaderWOCRLength	= ALIData.strCADmessageWithoutHeaderWOCR.length();
       




	}// end fSetCad

     void ExperientDataClass::fSetCadEraseMsg()
        {
         string                 stringE = "E";
         boolIsHeartbeat	= false;
         boolCADEraseMsg        = true;
         stringCADEraseMsg 	= charSTX + stringE + int2strLZ(CallData.intPosn) + charETX;
         stringCADEraseMsg	+= BlockCheckCharacter( stringCADEraseMsg, stringCADEraseMsg.length());
         intCADEraseMsgLength	= stringCADEraseMsg.length();        

	}// end fSetCad

void ExperientDataClass::fSetPANItoI3() {
 extern Trunk_Type_Mapping  TRUNK_TYPE_MAP;
 if (TRUNK_TYPE_MAP.fIsMSRP(this->CallData.intTrunk)) {
  this->ALIData.I3Data.strPANI = this->CallData.stringTenDigitPhoneNumber;
  this->ALIData.I3Data.I3XMLData.CallInfo.strClassOfService= "TEXT";
  this->ALIData.I3Data.I3XMLData.CallInfo.strClassOfServiceCode= "T";
  this->ALIData.I3Data.fMapMainTelNumbertoI3xml(this->CallData.stringTenDigitPhoneNumber);
 }
 else {
  this->ALIData.I3Data.strPANI = this->CallData.stringPANI;
 }
}

void ExperientDataClass::fCheckWirelessRebid() {
 // must be semaphore protected when called ...
 struct timespec timespecTimeNow;
 bool boolFirst, boolSecond, boolThird, boolFourth, boolCommon;

 extern bool 				boolREBID_NG911;
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;

 boolCommon = ((!this->ALIData.fIsPhaseTwoALI())||(!boolREBID_PHASE_ONE_ALI_RECORDS_ONLY));
 boolFirst  =  ((this->CallData.boolWirelessVoipCall)&&(boolCommon));
 boolSecond = WindowButtonData.boolALIRebidButton;
 boolThird  = ((this->boolRinging)&&((!this->boolAbandoned)||(!this->boolDisconnect)));
 boolFourth = ((boolREBID_NG911)&&(!boolLEGACY_ALI)&&((this->ALIData.I3Data.objLocationURI.boolDataRecieved)||(this->objGeoLocation.boolDataRecieved))&&(boolCommon));


// cout << "IS PHASE II ALI" << this->ALIData.fIsPhaseTwoALI() << endl;
// cout << "bool 0 -> " << boolCommon << endl;
// cout << "bool 1 -> " << boolFirst << endl;
// cout << "bool 2 -> " << boolSecond << endl;
// cout << "bool 3 -> " << boolThird << endl;
// cout << "bool 4 -> " << boolFourth << endl;


 if ( (boolFirst||boolFourth) && boolSecond && boolThird)  {

   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
   if (((time_difference(timespecTimeNow, this->ALIData.timespecTimeAliAutoRebidOK)) > intALI_WIRELESS_AUTO_REBID_SEC )&&( this->intNumberAutoALIBids < intALI_WIRELESS_AUTO_REBID_COUNT))   {

       cout << "IS PHASE II ALI" << this->ALIData.fIsPhaseTwoALI() << endl;



       this->ALIData.enumALIBidType            = AUTO;
       this->enumANIFunction                   = ALI_REQUEST;
       this->WindowButtonData.fSetButtonsForALIbid();
       // send to workstation
       this->ALIData.intALIState = XML_STATUS_CODE_AUTO_ALI_REBID;
       this->intNumberAutoALIBids++; 
       this->fCallInfo_XML_Message(XML_STATUS_CODE_AUTO_ALI_REBID);
 //      //cout << "before WRK" << endl;
       Queue_WRK_Event(this);
 //      //cout << "after wrk" << endl;
       // send to ALI Next Gen 
       if (boolFourth) {
      cout << "send" << endl;
      cout << "limit = " << intALI_WIRELESS_AUTO_REBID_COUNT << endl;
      cout << "bids = " << this->intNumberAutoALIBids << endl;
        this->ALIData.I3Data.enumLISfunction = URL_RECIEVED;

        switch (TRUNK_TYPE_MAP.Trunk[this->CallData.intTrunk].TrunkType) { 
         case NG911_SIP: case REFER: case CAMA:
          this->ALIData.I3Data.fLoadBidID(this->strNenaCallId);
          // Give more time to wireless rebid from ring .....
          break;
         case MSRP_TRUNK:                         
          this->ALIData.I3Data.fLoadBidID(this->strNenaCallId);
          // Give more time to wireless rebid from ring ..... ???
          break;
         default:
          cout << "Experient_Controller.cpp fCheckWirelessRebid() ... Should not get here" << endl;
        }
        ////cout << "fcheckwireless rebid -> load bid id with -> " << this->ALIData.I3Data.strBidId << endl;
        enqueue_Main_Input(LIS, this); 
       }
       else {
        SendANItoALI(this);
       }
           
   }// end if time_difference(...

 }// end if (boolFirst && boolSecond && boolThird)

}



void ExperientDataClass::fSet_Trunk(int intArg)
{
 CallData.intTrunk    = intArg;
 CallData.stringTrunk = int2strLZ(intArg);
}


void ExperientDataClass::fSetAbandoned()
{
 boolAbandoned = true;
 clock_gettime(CLOCK_REALTIME, &timespecTimeAbandonedStamp);
 timespecAbandonedPopUp = timespecTimeAbandonedStamp;
 timespecAbandonedPopUp.tv_sec += ABANDONED_CALL_NOTIFICATION_TIME_SEC ;
}


bool ExperientDataClass::fFindTelephoneNumber(string stringArg)
{
// (303) 555-1212  stringTenDigitPhoneNumberParensSpDash
// (303) 555 1212  stringTenDigitPhoneNumberParensSpSp
// (303)555-1212   stringTenDigitPhoneNumberParensNoSpDash
// (303)555 1212   stringTenDigitPhoneNumberParensNoSpSp
//  303-555-1212   string stringTenDigitPhoneNumberDashes
//  303 555-1212   string stringTenDigitPhoneNumberSpDash
//  303-555-1212   stringTenDigitPhoneNumberSpDash
//  303 555 1212   stringTenDigitPhoneNumberSpSp
//  3035551212     stringTenDigitPhoneNumber
//  555-1212       stringSevenDigitPhoneNumberDash
//  555 1212       stringSevenDigitPhoneNumberSp
//  5551212        stringSevenDigitPhoneNumber 

 size_t found;

 if (CallData.stringTenDigitPhoneNumber.empty())                             {return false;}
 found = CallData.stringTenDigitPhoneNumber.find_first_not_of("0123456789");
 if (found != string::npos)                                                  {return false;}                      

 string stringTenDigitPhoneNumberParensSpDash     = "("+CallData.stringThreeDigitAreaCode +") " + CallData.stringTelnoPrefix + "-" + CallData.stringTelnoSuffix;
 string stringTenDigitPhoneNumberParensSpSp       = "("+CallData.stringThreeDigitAreaCode +") " + CallData.stringTelnoPrefix + " " + CallData.stringTelnoSuffix;
 string stringTenDigitPhoneNumberParensNoSpDash   = "("+CallData.stringThreeDigitAreaCode +")"  + CallData.stringTelnoPrefix + "-" + CallData.stringTelnoSuffix;
 string stringTenDigitPhoneNumberParensNoSpSp     = "("+CallData.stringThreeDigitAreaCode +")"  + CallData.stringTelnoPrefix + " " + CallData.stringTelnoSuffix;
 string stringTenDigitPhoneNumberDashes           =     CallData.stringThreeDigitAreaCode + "-" + CallData.stringTelnoPrefix + "-" + CallData.stringTelnoSuffix;   
 string stringTenDigitPhoneNumberSpDash           =     CallData.stringThreeDigitAreaCode + " " + CallData.stringTelnoPrefix + "-" + CallData.stringTelnoSuffix; 
 string stringTenDigitPhoneNumberSpSp             =     CallData.stringThreeDigitAreaCode + " " + CallData.stringTelnoPrefix + " " + CallData.stringTelnoSuffix;
 string stringSevenDigitPhoneNumberDash           =                                      CallData.stringTelnoPrefix + "-" + CallData.stringTelnoSuffix; 
 string stringSevenDigitPhoneNumberSp             =                                      CallData.stringTelnoPrefix + " " + CallData.stringTelnoSuffix;


 found = stringArg.find(stringTenDigitPhoneNumberParensSpDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringTenDigitPhoneNumberParensSpSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringTenDigitPhoneNumberParensNoSpDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringTenDigitPhoneNumberParensNoSpSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringTenDigitPhoneNumberDashes);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringTenDigitPhoneNumberSpDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringTenDigitPhoneNumberSpSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(CallData.stringTenDigitPhoneNumber);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringSevenDigitPhoneNumberDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringSevenDigitPhoneNumberSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(CallData.stringSevenDigitPhoneNumber);
 if (found!=string::npos) {return true;}

 return false;

}

bool ExperientDataClass::fFindpANI(string stringArg)
{
// (303) 555-1212  stringTenDigitPhoneNumberParensSpDash
// (303) 555 1212  stringTenDigitPhoneNumberParensSpSp
// (303)555-1212   stringTenDigitPhoneNumberParensNoSpDash
// (303)555 1212   stringTenDigitPhoneNumberParensNoSpSp
//  303-555-1212   string stringTenDigitPhoneNumberDashes
//  303 555-1212   string stringTenDigitPhoneNumberSpDash
//  303-555-1212   stringTenDigitPhoneNumberSpDash
//  303 555 1212   stringTenDigitPhoneNumberSpSp
//  3035551212     stringTenDigitPhoneNumber

 size_t found;

 string stringAreaCode;
 string stringPrefix;
 string stringNumber;

 if (CallData.stringPANI.empty())        {return false;}
 if (CallData.stringPANI.length() != 10) {return false;}

 stringAreaCode.assign(CallData.stringPANI,0,3);
 stringPrefix.assign(CallData.stringPANI,3,3);
 stringNumber.assign(CallData.stringPANI,6,4);

 string stringPANIParensSpDash     = "("+stringAreaCode +") " + stringPrefix + "-" + stringNumber;
 string stringPANIParensSpSp       = "("+stringAreaCode +") " + stringPrefix + " " + stringNumber;
 string stringPANIParensNoSpDash   = "("+stringAreaCode +")"  + stringPrefix + "-" + stringNumber;
 string stringPANIParensNoSpSp     = "("+stringAreaCode +")"  + stringPrefix + " " + stringNumber;
 string stringPANIDashes           =     stringAreaCode + "-" + stringPrefix + "-" + stringNumber;   
 string stringPANISpDash           =     stringAreaCode + " " + stringPrefix + "-" + stringNumber; 
 string stringPANISpSp             =     stringAreaCode + " " + stringPrefix + " " + stringNumber;


 found = stringArg.find(stringPANIParensSpDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringPANIParensSpSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringPANIParensNoSpDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringPANIParensNoSpSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringPANIDashes);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringPANISpDash);
 if (found!=string::npos) {return true;}
 found = stringArg.find(stringPANISpSp);
 if (found!=string::npos) {return true;}
 found = stringArg.find(CallData.stringPANI);
 if (found!=string::npos) {return true;}


 return false;

}


string ExperientDataClass::fFindZeroTelno(string stringArg)
{
// (303) 000-0000  stringTenDigitZEROPhoneNumberParensSpDash
// (303) 000 0000  stringTenDigitZEROPhoneNumberParensSpSp
// (303)000-0000   stringTenDigitZEROPhoneNumberParensNoSpDash
// (303)000 0000   stringTenDigitZEROPhoneNumberParensNoSpSp
//  303-000-0000   stringTenDigitZEROPhoneNumberDashes
//  303 000-0000   stringTenDigitZEROPhoneNumberSpDash
//  303 000 0000   stringTenDigitZEROPhoneNumberSpSp
//  3030000000     stringTenDigitZEROPhoneNumber
//  000-0000       stringSevenDigitZEROPhoneNumberDash
//  000 0000       stringSevenDigitZEROPhoneNumberSp
//  0000000        stringSevenDigitZEROPhoneNumber 

 size_t                         found;
 string                         strPrefix;
 string                         strAreaCode;
 string                         strSuffix;

 if (CallData.stringPANI.length() != 10) {return stringArg;}

 strAreaCode = CallData.stringPANI.substr(0,3);
 strPrefix   = CallData.stringPANI.substr(3,3);
 strSuffix   = CallData.stringPANI.substr(6,4);
 
 string stringTenDigitZEROPhoneNumberParensSpDash     = "(" + NPD[0] +") " + "100-0000";
 string stringTenDigitZEROPhoneNumberParensSpSp       = "(" + NPD[0] +") " + "100 0000";
 string stringTenDigitZEROPhoneNumberParensNoSpDash   = "(" + NPD[0] +")"  + "100-0000";
 string stringTenDigitZEROPhoneNumberParensNoSpSp     = "(" + NPD[0] +")"  + "100 0000";
 string stringTenDigitZEROPhoneNumberDashes           =       NPD[0] + "-" + "100-0000";   
 string stringTenDigitZEROPhoneNumberSpDash           =       NPD[0] + " " + "100-0000"; 
 string stringTenDigitZEROPhoneNumberSpSp             =       NPD[0] + " " + "100 0000";
 string stringTenDigitZEROPhoneNumber                 =       NPD[0] +       "1000000";
 string stringSevenDigitZEROPhoneNumberDash           =                      "100-0000"; 
 string stringSevenDigitZEROPhoneNumberSp             =                      "100 0000";
 string stringSevenDigitZEROPhoneNumber               =                      "1000000";

 // search for string and replace with "pANI" .
 found = stringArg.find(stringTenDigitZEROPhoneNumberParensSpDash);
 if (found!=string::npos) {stringArg.replace(found , 14, "(" + strAreaCode + ") " + strPrefix + "-" + strSuffix); return stringArg;}

 found = stringArg.find(stringTenDigitZEROPhoneNumberParensSpSp);
 if (found!=string::npos) {stringArg.replace(found , 14, "(" + strAreaCode + ") " + strPrefix + " " + strSuffix); return stringArg;}

 found = stringArg.find(stringTenDigitZEROPhoneNumberParensNoSpDash);
 if (found!=string::npos) {stringArg.replace(found , 13, "(" + strAreaCode + ")" + strPrefix + "-" + strSuffix); return stringArg;}
 
 found = stringArg.find(stringTenDigitZEROPhoneNumberParensNoSpSp);
 if (found!=string::npos) {stringArg.replace(found , 13, "(" + strAreaCode + ")" + strPrefix + " " + strSuffix); return stringArg;}

 found = stringArg.find(stringTenDigitZEROPhoneNumberDashes);
 if (found!=string::npos) {stringArg.replace(found , 12, strAreaCode + "-" + strPrefix + "-" + strSuffix); return stringArg;}

 found = stringArg.find(stringTenDigitZEROPhoneNumberSpDash);
 if (found!=string::npos) {stringArg.replace(found , 12, strAreaCode + " " + strPrefix + "-" + strSuffix); return stringArg;}

 found = stringArg.find(stringTenDigitZEROPhoneNumberSpSp);
 if (found!=string::npos) {stringArg.replace(found , 12, strAreaCode + " " + strPrefix + " " + strSuffix); return stringArg;}

 found = stringArg.find(stringTenDigitZEROPhoneNumber);
 if (found!=string::npos) {stringArg.replace(found , 10, strAreaCode + strPrefix + strSuffix); return stringArg;}

 found = stringArg.find(stringSevenDigitZEROPhoneNumberDash);
 if (found!=string::npos) {stringArg.replace(found , 8, strPrefix + "-" + strSuffix); return stringArg;}

 found = stringArg.find(stringSevenDigitZEROPhoneNumberSp);
 if (found!=string::npos) {stringArg.replace(found , 8, strPrefix + " " + strSuffix); return stringArg;}

 found = stringArg.find(stringSevenDigitZEROPhoneNumber);
 if (found!=string::npos) {stringArg.replace(found , 7, strPrefix + strSuffix); return stringArg;}

 return stringArg;
}

bool ExperientDataClass::fLoad_ALI_Type(string stringArg)
{
 int    intType;

 if (stringArg.length() < 2) {SendCodingError("ExperientDataClass.cpp - Invalid string passed to fn ExperientDataClass::fLoad_ALI_Type()"); return false;}
 ALIData.stringType.assign(stringArg,1,1);
 if(!Validate_Integer(ALIData.stringType)){return false;}

 intType = char2int(ALIData.stringType.c_str()); 

 switch (intType)
  {
   case 1: case 2: case 3:  case 5: case 9:
        return true;
   default:
        return false;
  }// end switch
  
}// ExperientDataClass::fLoad_ALI_Type(string stringArg)


bool ExperientDataClass::fBidURI(int iTableIndex) {



 return true;
}


bool ExperientDataClass::fLoad_ALI_Trunk_from_Position(string stringArg)
{
 
 if (stringArg.length() == 4){CallData.stringTrunk.assign(stringArg,2,1);return false;}
 if (stringArg.length() < 4) {CallData.stringTrunk = "";                 return false;}
 CallData.stringTrunk.assign(stringArg,2,2);
 if (!Validate_Integer(CallData.stringTrunk)){return false;}
 
 CallData.intTrunk = char2int(CallData.stringTrunk.c_str());

 if(!Validate_Trunk_Number(CallData.intTrunk)){return false;} 
 
 return true;


}// end ExperientDataClass::fLoad_ALI_Position(string stringArg)



int ExperientDataClass::fFindLocalTransferToPosition()
{
 char   chFirst   = '1';
 char   chSecond  = '0';
 string strPosition;

 if ((CallData.TransferData.strTransfertoNumber.length()==4)&&(CallData.TransferData.strTransfertoNumber[0] == chFirst)&&(CallData.TransferData.strTransfertoNumber[1] == chSecond)) 
 {
  strPosition = CallData.TransferData.strTransfertoNumber.substr(2,2);
  if(ValidateFieldAndRange(ANI, strPosition, CallData.TransferData.strTransfertoNumber, 0, "ExperientDataClass::fFindLocalTransferPosition()", intNUM_WRK_STATIONS, 1))
   {return char2int(strPosition.c_str());}
 }
 return 0;
}

void ExperientDataClass::fUpdateDestChannelLineNumbersInTable(Channel_Data objChannelData) {
 int A,B;
 size_t szConfVector, szDestVector;

 if (objChannelData.iPositionNumber <= 0) {return;}

 szConfVector= this->CallData.ConfData.vectConferenceChannelData.size();
 for (unsigned int i = 0; i < szConfVector; i++) { 
  if (!this->CallData.ConfData.vectConferenceChannelData[i].boolConnected)      {continue;}
  if (this->CallData.ConfData.vectConferenceChannelData[i].iPositionNumber > 0) {continue;}
   
  this->CallData.ConfData.vectConferenceChannelData[i].iGUIlineView = objChannelData.iGUIlineView;
  this->CallData.ConfData.vectConferenceChannelData[i].iLineNumber  = objChannelData.iLineNumber;
 }

 szDestVector= this->FreeswitchData.vectDestChannelsOnCall.size();
 for (unsigned int i = 0; i < szDestVector; i++) { 
  this->FreeswitchData.vectDestChannelsOnCall[i].iGUIlineView = objChannelData.iGUIlineView;
  this->FreeswitchData.vectDestChannelsOnCall[i].iLineNumber  = objChannelData.iLineNumber;
 }

 return;
}

void ExperientDataClass::fUpdateLineNumbersInTable(Channel_Data objChannelData) {
// this function  is used coming out of Park where there would only be 2 channels on the call
size_t szConfVector, szPosVector, szDestVector;

 szConfVector= this->CallData.ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < szConfVector; i++) { 
  if (!this->CallData.ConfData.vectConferenceChannelData[i].boolConnected) {continue;}
  this->CallData.ConfData.vectConferenceChannelData[i].iGUIlineView = objChannelData.iGUIlineView;
  this->CallData.ConfData.vectConferenceChannelData[i].iLineNumber       = objChannelData.iLineNumber;
 }

 //This may be problematic just do the position that is new to the call ..... FRED ....
 szPosVector= this->FreeswitchData.vectPostionsOnCall.size();
 for (unsigned int i = 0; i < szPosVector; i++) { 
  this->FreeswitchData.vectPostionsOnCall[i].iGUIlineView = objChannelData.iGUIlineView;
  this->FreeswitchData.vectPostionsOnCall[i].iLineNumber  = objChannelData.iLineNumber;
 }

 szDestVector= this->FreeswitchData.vectDestChannelsOnCall.size();
 for (unsigned int i = 0; i < szDestVector; i++) { 
  this->FreeswitchData.vectDestChannelsOnCall[i].iGUIlineView = objChannelData.iGUIlineView;
  this->FreeswitchData.vectDestChannelsOnCall[i].iLineNumber  = objChannelData.iLineNumber;
 }

 return;
}

void ExperientDataClass::fUpdate_FREESWITCH_ConferenceData(Channel_Data objChannelData, int intTranVectorIndex)
{
 string 				strTransferLegend = "";
 string                                 strPositionLegend = "";
 int    				index = -1;
 int                                    Pindex = -1;

 //This is an update to a Conference Transfer to another Position
 // 
 if (intTranVectorIndex < 0) { SendCodingError( "ExperientDataClass.cpp - Coding Error in fUpdate_FREESWITCH_ConferenceData() intTranVectorIndex < 0"); return;}
// //cout << "in function" << endl;
 strTransferLegend = CallData.vTransferData[intTranVectorIndex].strConfDisplay;
// //cout << "here a " << endl;
 if (!strTransferLegend.empty()) {index = CallData.fLegendInConferenceChannelDataVector(strTransferLegend);}

 if (index < 0) { SendCodingError( "ExperientDataClass.cpp - Coding Error in fUpdate_FREESWITCH_ConferenceData() index < 0"); return;}

 objChannelData.strTranData.clear();


// //cout << "here b" << endl;
 //check if position connected before:
 strPositionLegend = ConferenceMemberString(POSITION_CONF,objChannelData.iPositionNumber);
// //cout << "before erase" << endl;
 if (!strPositionLegend.empty()) {Pindex = CallData.fLegendInConferenceChannelDataVector(strPositionLegend);}
 if (Pindex >= 0) {CallData.ConfData.vectConferenceChannelData.erase(CallData.ConfData.vectConferenceChannelData.begin()+Pindex);} 
////cout << "after erase" << endl;
 FreeswitchData.vectPostionsOnCall.push_back(objChannelData);
 
 strTransferLegend = CallData.vTransferData[intTranVectorIndex].strConfDisplay;
 if (!strTransferLegend.empty()) {index = CallData.fLegendInConferenceChannelDataVector(strTransferLegend);}
 if (index < 0) { SendCodingError( "ExperientDataClass.cpp - Coding Error in fUpdate_FREESWITCH_ConferenceData() index < 0"); return;}


 CallData.ConfData.vectConferenceChannelData[index] = objChannelData;
 CallData.fConferenceStringAdd(POSITION_CONF, objChannelData.iPositionNumber);
 CallData.fConferenceStringRemove(strTransferLegend); 
 CallData.ConfData.fRemoveTransferFromHistory(strTransferLegend);

}


void ExperientDataClass::fUpdate_FREESWITCH_ConferenceData(Freeswitch_Data objFreeswitchData, int ilinkchannelnumber, int intTranVectorIndex, bool boolCallerIdsAreReversed)
{
 string 				strTemp = "";
 string 				strOldPosition;
 string 				strUserName;
 int    				index = -1;
// extern Telephone_Devices		TelephoneEquipment;
 
// //cout << "updating conf vector" << endl;
 // this function is used to update the Conference vector with extra data after a transfer connects. 
 // initially the vector will have data asscociated with the transfering phone, this will update the
 // vector with the data asscociated with the answering phone.

 FreeswitchData.objChannelData.fClear();
 FreeswitchData.objChannelData.boolConnected = true;
 FreeswitchData.objChannelData.boolIsOutbound = true;

 strTemp = CallData.vTransferData[intTranVectorIndex].strConfDisplay;

 if (!strTemp.empty()) {index = CallData.fLegendInConferenceChannelDataVector(strTemp);}

 FreeswitchData.fLoad_Bridge_Data_Into_Object( objFreeswitchData.objLinkData, ilinkchannelnumber,  this->CallData.intTrunk , boolCallerIdsAreReversed, strTemp, false);

 FreeswitchData.objChannelData.iLineNumber       = FreeswitchData.objRing_Dial_Data.iLineNumber;
 FreeswitchData.objChannelData.iGUIlineView      = FreeswitchData.objRing_Dial_Data.iGUIlineView;

 // Determine if data is a position channel or Dest channel

// objFreeswitchData.objLinkData.fDisplay();
//FreeswitchData.objChannelData.fDisplay();


 if (!FreeswitchData.objChannelData.boolIsPosition) 
  {
   if (index < 0) {return;}
   if (FreeswitchData.objChannelData.strCustName.empty()){
    FreeswitchData.objChannelData.strCustName = CallerNameLookup( FreeswitchData.objChannelData.strCallerID);
   }

   CallData.ConfData.vectConferenceChannelData[index] = FreeswitchData.objChannelData;
   FreeswitchData.vectDestChannelsOnCall.push_back(FreeswitchData.objChannelData);
  }
 else                 
  {
//   //cout << "is a position" << endl;
   strOldPosition = CallData.stringPosn;
   CallData.fLoadPosn(FreeswitchData.objChannelData.iPositionNumber);
////cout << "old Position" << strOldPosition << endl;
   // Check if position has been active before on this call
 //  index = CallData.fLegendInConferenceChannelDataVector(FreeswitchData.objChannelData.strConfDisplay);
 //  if (index < 0) 
 //    { 

 //    CallData.ConfData.vectConferenceChannelData.push_back(FreeswitchData.objChannelData);
 //    strUserName = FreeswitchUsernameFromChannel(FreeswitchData.objChannelData.strChannelName);
  //   //cout << "Username = " << strUserName << endl;
 //    //cout << "index = " << TelephoneEquipment.fIndexofTelephonewithRegistrationName(strUserName) << endl;
 //    //cout << "line number -> " << TelephoneEquipment.Devices[0].fLineNumberWithRegistrationName(strUserName) << endl;
 //    if ( TelephoneEquipment.fIndexofTelephonewithRegistrationName(strUserName) >= 0) 
 //     {
 //      //cout << "is a telephone" << endl;
     //  CallData.ConfData.fReplaceVectorData(FreeswitchData.objChannelData, 
  //     //cout << "username = " << FreeswitchUsernameFromChannel(FreeswitchData.objChannelData.strChannelName) << endl;
   //   }

//    } 
//   //cout << "index -> " << index << endl;
   if (index < 0) {return;}
   CallData.ConfData.vectConferenceChannelData[index] = FreeswitchData.objChannelData;
   CallData.ConfData.fRemoveTransferFromHistory(strTemp);
   CallData.fConferenceStringAdd(POSITION_CONF, CallData.intPosn);
   FreeswitchData.vectPostionsOnCall.push_back(FreeswitchData.objChannelData);
   CallData.fLoadPosn(strOldPosition);  
  }

}

void ExperientDataClass::fUpdate_FREESWITCH_ConferenceData(Channel_Data objChannelData)
{
 int    				index = -1;
 int    				j;
 string 				strTemp = "";
 extern Initialize_Global_Variables 	INIT_VARS;

//Used for a position that has been on the call before ....

 objChannelData.boolConnected = true;
 objChannelData.boolIsOutbound = true;
 this->FreeswitchData.objChannelData = objChannelData;

 strTemp = objChannelData.strConfDisplay;
 if (!strTemp.empty()) {index = this->CallData.fLegendInConferenceChannelDataVector(strTemp);}
 if (index < 0) {
  if (objChannelData.iPositionNumber){fUpdate_FREESWITCH_ConferenceDataNewPosition(objChannelData);}
   return;
 }

 // push conference into Position vector
    if(objChannelData.iPositionNumber)
     {
      CallData.ConfData.vectConferenceChannelData[index] = objChannelData;
      CallData.ConfData.fRemoveTransferFromHistory(strTemp);
      FreeswitchData.vectPostionsOnCall.push_back(objChannelData);
      j = objChannelData.iPositionNumber;
      this->CallData.fConferenceStringAdd(POSITION_CONF, j);
      this->fLoad_Position(SOFTWARE, ANI, int2strLZ(j), 1, "fUpdate_FREESWITCH_ConferenceData");
    //  //cout << "here" << endl;
     }
    else
     {
      if (index < 0) {return;}
      if (INIT_VARS.fChannelIsInternalNG911Transfer(FreeswitchData.objChannelData.strChannelName))
       {
      //  //cout << "remove" << endl;
        //Remove the Data for Internal NG911 Transfers .......They are actually Position Transfers ....
        index = CallData.fLegendInConferenceChannelDataVector(objChannelData.strConfDisplay);
        if (index >= 0) {CallData.ConfData.vectConferenceChannelData.erase(CallData.ConfData.vectConferenceChannelData.begin()+index);} 
        CallData.fConferenceStringRemove(objChannelData.strConfDisplay);
        CallData.ConfData.fRemoveTransferFromHistory(objChannelData.strConfDisplay);
        return;
       }
      strTemp = CallerNameLookup( objChannelData.strCallerID);
      if (!strTemp.empty()) {this->FreeswitchData.objChannelData.strCustName = strTemp;}

      this->CallData.ConfData.vectConferenceChannelData[index] = this->FreeswitchData.objChannelData;
      this->FreeswitchData.vectDestChannelsOnCall.push_back(this->FreeswitchData.objChannelData);
      this->fLoad_Position(SOFTWARE, ANI, "00", 1, "fUpdate_FREESWITCH_ConferenceData");
     }
}

void ExperientDataClass::fUpdate_FREESWITCH_ConferenceDataNewPosition(Channel_Data objChannelData)
{
 // Only Called from ExperientDataClass::fUpdate_FREESWITCH_ConferenceData(Channel_Data objChannelData)
 int j;

 // push conference into Position vector
    if(objChannelData.iPositionNumber)
     {
      objChannelData.fLoadCustomerName(objChannelData.iPositionNumber);
      objChannelData.fLoadPositionCallID(objChannelData.iPositionNumber);
      CallData.ConfData.vectConferenceChannelData.push_back(objChannelData);
      FreeswitchData.vectPostionsOnCall.push_back(objChannelData);
      j = objChannelData.iPositionNumber;
      this->CallData.fConferenceStringAdd(POSITION_CONF, j);
      this->fLoad_Position(SOFTWARE, ANI, int2strLZ(j), 1, "fUpdate_FREESWITCH_ConferenceDataNewPosition");

    //  //cout << "here" << endl;
     }

}      

bool ExperientDataClass::fLoad_CallStatusCode(threadorPorttype enumArg, string sStatusCode, string strXMLData )
{
 MessageClass objMessage;
 int          intStatusCode;
 Port_Data    objPortData;

 if (!Validate_Integer(sStatusCode) )
  {
   objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_710c, sStatusCode,
                              ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg,objMessage);
   return false;
  }
 intStatusCode = char2int(sStatusCode.c_str());

 switch (intStatusCode)
  {
   case 1:
        // Connected
        enumANIFunction = CONNECT;
        break;
    case 2:
        // Disconnected
        enumANIFunction = DISCONNECT;
        break;
    case 5:
        // On Hold
        enumANIFunction = HOLD_ON;
        break;
    case 6:
        // Off Hold
        enumANIFunction = HOLD_OFF;
        break;
    case 7:
        // Abandoned
        enumANIFunction = ABANDONED;
        break;
    case 11:
        // Ringing
        enumANIFunction = RINGING;
        break;
    case 84:
        //TDD Mute
        enumANIFunction = UNCLASSIFIED;
        break;
    case 85:
        // TDD Dispatcher
        enumANIFunction = UNCLASSIFIED;
        break;
    case 86:
        //TDD Recieved
        enumANIFunction = UNCLASSIFIED;
        break;
    case 87:
        // Manual ALI Request
        enumANIFunction = UNCLASSIFIED;
        break;
    case 88:
        // Manual ALI Request Failed
        enumANIFunction = UNCLASSIFIED;
        break;
    case 89:
        // Auto ALI Request
        enumANIFunction = UNCLASSIFIED;
        break;
    case 90:
        // Auto ALI Request Failed
        enumANIFunction = UNCLASSIFIED;
        break;
    case 91:
        // ALI Request
        enumANIFunction = UNCLASSIFIED;
        break;
    case 92:
        // ALI Request Failed
        enumANIFunction = UNCLASSIFIED;
        break;
    case 93:
        // ALI Received
        enumANIFunction = UNCLASSIFIED;
        break;
    case 94:
        // Manual ALI Received
        enumANIFunction = UNCLASSIFIED;
        break;
    case 95:
        // Auto ALI Received
        enumANIFunction = UNCLASSIFIED;
        break;
    case 96:
        // ALI No Update
        enumANIFunction = UNCLASSIFIED; 
        break;
    case 97:
        // ALI Received
        enumANIFunction = UNCLASSIFIED;
        break;
    case 98:
        // ConferenceJoin
        enumANIFunction = UNCLASSIFIED;
        break;
    case 99:
        // ConferenceLeave
        enumANIFunction = UNCLASSIFIED;
        break;

    default:
             objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                        objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_710d, sStatusCode,
                                        ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
             enQueue_Message(enumArg,objMessage);
   }
    return true;
}




bool ExperientDataClass::fLoadCallbackfromALI(ALISteering objALISteering)
{
 unsigned long long int intALICallback;


 if ((CallData.intCallbackNumber > 0)&&(CallData.stringPANI.length() == 10)) {return true;}

 intALICallback = ALIData.fFindCallback(0, objALISteering);

 if (intALICallback == 0)                 {return false;}
 
 CallData.boolCallBackVerified = true;
 fLoad_CallbackNumber(int2str(intALICallback));
 return true;

}


bool ExperientDataClass::fLoadCallbackfromALI(bool boolLegacyALITransfer)
{
 unsigned long long int intALICallback;
 extern ALISteering     ALI_Steering[intMAX_ALI_DATABASES + 1 ];
// //cout << "fLoadCallbackfromALI" << endl;
 if (boolLegacyALITransfer)                         {this->ALIData.intLastDatabaseBid = 1;}
 if ((boolNG_ALI)&&(!boolLEGACY_ALI))               {this->ALIData.intLastDatabaseBid = 1;}                    // will need to mod for ng911

 if ((ALIData.intLastDatabaseBid > intMAX_ALI_DATABASES)||(ALIData.intLastDatabaseBid < 0)) {return false;}
// //cout << "A" << endl;
 if ((CallData.intCallbackNumber > 0)&&(CallData.stringPANI.length() == 10)) {return true;}

 intALICallback = ALIData.fFindCallback(0, ALI_Steering[ALIData.intLastDatabaseBid]);
// //cout << "B" << endl;
// //cout << "callback is " << intALICallback << endl;
 if (intALICallback == 0)                 {return false;}
 
 CallData.boolCallBackVerified = true;
 fLoad_CallbackNumber(int2str(intALICallback));
 return true;

}

bool ExperientDataClass::fLoad_CallbackNumber(string strTenDigitNumber)
{
 if (!CallData.strSIPphoneNumber.empty()) {return true;}
 if (strTenDigitNumber.length() != 10)    {return false;}
 if (!Validate_Integer(strTenDigitNumber)){return false;}
 
 if (CallData.fLoadCallbackNumber(strTenDigitNumber)) {return true;}
 else                                                 {return false;}

}

bool ExperientDataClass::fLoad_SIP_PhoneNumbers(data_format enumArg, int intPortNum, string sArg, bool boolLineRoll, bool boolNG_SIP_911_NON_AUDIOCODES)
{
 
 string                     strAsteriskANI;
 string                     strAsteriskSIPaddr;
 string                     strTest;
 MessageClass               objMessage;
 Port_Data	            objPortData;
 size_t                     found;
 bool                       boolCallbackLoaded = false;
 bool                       boolPANIloaded = false;
 trunk_type                 eTrunkType;
 extern bool                boolAUDIOCODES_911_ANI_REVERSED;

  objPortData.fLoadPortData(ANI, intPortNum);
 
 // note trunk must be loaded before this function call
 //if (CallData.intTrunk==0){//cout << "Coding Error in ExperientDataClass::fLoad_SIP_PhoneNumbers Trunk = ZERO" << endl; return false;}
////cout << "in Load sip phone number" << endl;
////cout << strAsteriskANI << "/length=" << strAsteriskANI.length() << endl;
////cout << strAsteriskSIPaddr << "/length=" << strAsteriskSIPaddr.length() << endl;

   //     //cout << "loading sip phone number" << endl; 

	eTrunkType = TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType;

        if(boolNG_SIP_911_NON_AUDIOCODES) {eTrunkType = NG911_SIP;}
   //     //cout << "trunk type -> " << eTrunkType << endl;
   //    DisplayTrunkType(CallData.intTrunk);
         switch (eTrunkType)
          {
           case CAMA: case LANDLINE: case WIRELESS: case INDIGITAL_WIRELESS: case INTRADO:
                switch (boolLineRoll) 
                 {
                  case true:
                  //     //cout << "Bool LineRoll" << endl;  
                       strAsteriskANI     = ParseCallID_NumberFromAudiocodes(sArg, boolAUDIOCODES_911_ANI_REVERSED);
                  //     //cout << "strAsteriskANI -> " << strAsteriskANI << endl;
                       break;
                  case false:
                       strAsteriskANI     = ParseCallID_NumberFromAudiocodes(sArg, boolAUDIOCODES_911_ANI_REVERSED);
                       strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                       strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");
                       break;
                 }
                switch (boolAUDIOCODES_911_ANI_REVERSED)
                 {
                  case true:
                       boolPANIloaded = CallData.fSet_PhoneNumberFromANI(TEN_DIGIT_PRECHECKED, PSUEDO_ANI ,strAsteriskANI);
                  //     //cout << "PANI->" << CallData.stringPANI << endl;
                       if (!boolPANIloaded) 
                        {
                         CallData.fSet_Phone_Numbers_From_Bad_ANI(strAsteriskANI);
                         objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                    objPortData, objBLANK_FREESWITCH_DATA,
                                                    objBLANK_ALI_DATA,ANI_MESSAGE_323f, strAsteriskANI,
                                                    ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                         enQueue_Message(ANI,objMessage);
                        }
                       break;

                  case false:
                       switch (strAsteriskANI.length())
                        {
                         case 26:                                           // format 20 Digit ANI audiocodes *40NPANXXXXXX#*NPANXXXXXX# 
                              CallData.fSetANIformat(20, CallData.intTrunk); 
                              boolPANIloaded = CallData.fSet_PhoneNumberFromANI(TWENTY_DIGIT, PSUEDO_ANI ,strAsteriskANI);
                              if (boolANI_20_DIGIT_VALID_CALLBACK) {boolCallbackLoaded = CallData.fSet_PhoneNumberFromANI(TWENTY_DIGIT, CALLBACK ,strAsteriskANI);} 
                              if ((boolANI_20_DIGIT_VALID_CALLBACK)&&(!boolCallbackLoaded))
                               {
                                objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                           objPortData, objBLANK_FREESWITCH_DATA,
                                                           objBLANK_ALI_DATA,ANI_MESSAGE_323e, strAsteriskANI,
                                                           ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                                enQueue_Message(ANI,objMessage);
                               }                            
                              if (!boolPANIloaded)
                               {
                                if (!boolCallbackLoaded) {CallData.fSet_Phone_Numbers_From_Bad_ANI(strAsteriskANI);}
                                else                     {CallData.stringPANI = CallData.stringTenDigitPhoneNumber;}
                                objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                           objPortData, objBLANK_FREESWITCH_DATA,
                                                           objBLANK_ALI_DATA,ANI_MESSAGE_323f, strAsteriskANI,
                                                           ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                                enQueue_Message(ANI,objMessage);
                               }
                              CallData.boolCallBackVerified = (true&&boolANI_20_DIGIT_VALID_CALLBACK);
                              break;
                         case 10:                                          // format 8 digit ANI audiocodes *NNPANXXXX#
                              CallData.fSetANIformat(8, CallData.intTrunk); 
                              boolPANIloaded = CallData.fSet_PhoneNumberFromANI(EIGHT_DIGIT, PSUEDO_ANI ,strAsteriskANI);    
                              if (!boolPANIloaded) 
                               {
                                CallData.fSet_Phone_Numbers_From_Bad_ANI(strAsteriskANI);
                                objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                           objPortData, objBLANK_FREESWITCH_DATA, 
                                                           objBLANK_ALI_DATA, ANI_MESSAGE_323f, strAsteriskANI,
                                                           ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                                enQueue_Message(ANI,objMessage);
                               }
                              break;              
                         case 14:                                         // format 10 digit ANI audiocodes *40NPANXXXXXXB
                              CallData.fSetANIformat(10, CallData.intTrunk); 
                              boolPANIloaded = CallData.fSet_PhoneNumberFromANI(TEN_DIGIT, PSUEDO_ANI ,strAsteriskANI);
                              if (!boolPANIloaded) 
                               {
                                CallData.fSet_Phone_Numbers_From_Bad_ANI(strAsteriskANI);
                                objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                           objPortData, objBLANK_FREESWITCH_DATA,
                                                           objBLANK_ALI_DATA,ANI_MESSAGE_323f, strAsteriskANI,
                                                           ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                                enQueue_Message(ANI,objMessage);
                               }
                              break;
                                                                  // format Granite County 10 digit *NPAXXXXXXX#
                         case 12:
                              CallData.fSetANIformat(10, CallData.intTrunk);
                              boolPANIloaded = CallData.fSet_PhoneNumberFromANI(TEN_DIGIT_NON_STANDARD, PSUEDO_ANI ,strAsteriskANI);
                              if (!boolPANIloaded) 
                               {
                                CallData.fSet_Phone_Numbers_From_Bad_ANI(strAsteriskANI);
                                objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                           objPortData, objBLANK_FREESWITCH_DATA,
                                                           objBLANK_ALI_DATA,ANI_MESSAGE_323f, strAsteriskANI,
                                                           ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                                enQueue_Message(ANI,objMessage);
                               }
                              break;

                         default:
                              // at this point the data is not correct but we may be able to "find" the phone number
                              if(CallData.fSet_Phone_Numbers_From_Bad_ANI(strAsteriskANI))   {

                               objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                          objPortData, objBLANK_FREESWITCH_DATA, 
                                                          objBLANK_ALI_DATA, ANI_MESSAGE_323f, strAsteriskANI,
                                                          ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                              }
                              else {
                               objMessage.fMessage_Create(LOG_WARNING, 323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                               objPortData, objBLANK_FREESWITCH_DATA,
                                                               objBLANK_ALI_DATA,ANI_MESSAGE_323e, strAsteriskANI,
                                                               ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                              }
                              enQueue_Message(ANI,objMessage);

                              break;
                       }// end switch (strAsteriskANI.length())
                      break;
                 } // end switch (boolAUDIOCODES_911_ANI_REVERSED)
                break;
           case CLID:
                if(boolLineRoll){cout << "TBC CLID DATACLASS" << endl;}
                else
                 {
                  strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_CALLER_ID_NUMBER_WO_COLON));
                  strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                  strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");
                 }
 
                switch (strAsteriskANI.length())
                 {
                  case 11:                                          // format 11 digit FXO audiocodes CLID 1NPANXXXXXX
                       if (!CallData.fLoadCallbackNumber(strAsteriskANI.substr(1,10))) {return false;}
                       CallData.stringPANI = "";
                       CallData.stringInfoCode = "40";
                       break;        
                  case 10:                                         // format 10 digit FXO audiocodes CLID NPANXXXXXX 
                       if (!CallData.fLoadCallbackNumber(strAsteriskANI.substr(0,10))) {return false;}
                       CallData.stringPANI = "";
                       CallData.stringInfoCode = "40";
                       break;
                  default: 
                       CallData.fSetCallInfoToSticks();      // Blocked Call ID  FXO audiocodes CLID     
                       break;
                 }// end switch (strAsteriskANI.length())


                break;

           case NG911_SIP: case MSRP_TRUNK: case REFER:
                 // //cout << "ng911 sip" << endl;
                  if (boolLineRoll)
                   {
                    strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_VARIABLE_RING_ANI_WO_COLON));
                    strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_VARIABLE_RING_CHANNEL_NAME_WO_COLON));
                    strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");
                   }
                  else
                   {
                    strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON));
                    strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                    strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");
                   }
                 
                switch (strAsteriskANI.length())
                 {
                  case 10:
                        if (ValidTenDigitNumber(strAsteriskANI)) 
                         {
                          CallData.stringPANI                = strAsteriskANI;
                          break;
                         }
                       
                       // if not a valid 10 digit number do default action
                  default: 
                        CallData.strSIPphoneNumber         = strAsteriskSIPaddr;
                        found = CallData.strSIPphoneNumber.find(";");               //erase "; and beyond"
                        if (found != string::npos){CallData.strSIPphoneNumber.erase(found,string::npos);}
                        found = CallData.strSIPphoneNumber.find("sip:");
                        if (found != string::npos){ CallData.strSIPphoneNumber.erase(found,4);}
 /*                       
                        if (!spc_email_isvalid(strTest.c_str())) 
                         {
                          objMessage.fMessage_Create(LOG_WARNING, 323, objBLANK_CALL_RECORD, objPortData, ANI_MESSAGE_323h, CallData.strSIPphoneNumber,
                                                     ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                          enQueue_Message(ANI,objMessage);
     
                         }
 */                              
                        if (ValidTenDigitNumber(strAsteriskANI)) {CallData.stringPANI = strAsteriskANI;}
                        CallData.stringInfoCode            = "40";
                 } 

         //     //cout << "ANI = " <<  strAsteriskANI << endl <<  "SIP addr = " << CallData.strSIPphoneNumber << endl;                   
                break;





           case SIP_TRUNK:
                if (boolLineRoll)
                 {
            //      //cout << "loading siptrunk line roll" << endl;
                  strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_VARIABLE_RING_ANI_WO_COLON));
                  strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_VARIABLE_RING_CHANNEL_NAME_WO_COLON));
                  strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");
                 }
                else if (boolNG_SIP_911_NON_AUDIOCODES)
                 {
                  strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON));
                  strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                  strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");

                 }
                else
                 {
             //     //cout << "here in SIP_TRUNK load telephone numbers" << endl;
             //     strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON));
                  strAsteriskANI     = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_CALLER_ANI_WO_COLON));
                  strAsteriskSIPaddr = URLdecode(ParseFreeswitchData(sArg,FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                  strAsteriskSIPaddr = FindandReplace(strAsteriskSIPaddr,FREESWITCH_CHANNEL_PREFIX,"");
                  

                 }
                strAsteriskANI = StripPlusOne(strAsteriskANI);             

                switch (strAsteriskANI.length())
                 {
                  case 10:
                 //       //cout << "length id 10" << endl;
                        if (ValidTenDigitNumber(strAsteriskANI)) 
                         {
                          CallData.stringTenDigitPhoneNumber = strAsteriskANI;
                          CallData.stringSevenDigitPhoneNumber.assign(CallData.stringTenDigitPhoneNumber,3,7);
                          CallData.stringThreeDigitAreaCode.assign(CallData.stringTenDigitPhoneNumber,0,3);
                          CallData.intCallbackNumber = char2int(CallData.stringTenDigitPhoneNumber.c_str());
                          break;
                         }
                       
                  // if not a valid 10 digit number do default action
                  default: 
                        ////cout << "doing default" << endl;
                        CallData.strSIPphoneNumber         = strAsteriskSIPaddr;
                        found = CallData.strSIPphoneNumber.find(";");               //erase "; and beyond"
                        if (found != string::npos){CallData.strSIPphoneNumber.erase(found,string::npos);}
                        found = CallData.strSIPphoneNumber.find("sip:");
                        if (found != string::npos){ CallData.strSIPphoneNumber.erase(found,4);}
 /*                       
                        if (!spc_email_isvalid(strTest.c_str())) 
                         {
                          objMessage.fMessage_Create(LOG_WARNING, 323, objBLANK_CALL_RECORD, objPortData, ANI_MESSAGE_323h, CallData.strSIPphoneNumber,
                                                     ASCII_String(sArg.c_str(), sArg.length()),"","","","", NORMAL_MSG, NEXT_LINE);
                          enQueue_Message(ANI,objMessage);
     
                         }
 */  
                        if (ValidTenDigitNumber(strAsteriskANI)) {CallData.stringPANI = strAsteriskANI;}
                        CallData.stringInfoCode            = "40";
                 } 

             // //cout << "ANI = " <<  strAsteriskANI << endl <<  "SIP addr = " << CallData.strSIPphoneNumber << endl;                   
                break;
           default:
      //          //cout << "return false" << endl;
                return false;
          }// end switch (TRUNK_TYPE_MAP.TrunkType[intTrunk])

 //  //cout << "return true" << endl;



   return true;  
}


bool ExperientDataClass::fLoad_PhoneNumbers(data_format enumArg, threadorPorttype enumArg2, int intPortNum, string sArg, string sArg2, string sArg3, string sArg4, string strXMLData, bool boolLineRoll, bool boolNG_SIP_911_NON_AUDIOCODES)
{
 string         sPhoneNumber;
// string 	sPANI;
// string 	sNPD;
 MessageClass   objMessage;
 Port_Data	objPortData;
 string         strRawData = sArg;
 int            intLogCode = 323;
 string         strTemp;
 

 objPortData.fLoadPortData(enumArg2, intPortNum);
 switch (enumArg)
  {
  
   case XML_DATA_FIELD:
        CallData.stringTenDigitPhoneNumber = sArg;
        CallData.stringPANI	           = sArg2;
        CallData.stringInfoCode            = sArg3;
        strRawData   = strXMLData;
        intLogCode   = 710;
        if ((fLoad_CallbackNumber(sArg))&&(fLoad_pANI(enumArg2, sArg2, strXMLData))) {return true;}
        break;
 
 //  case SOFTWARE:
 //       CallData.stringTenDigitPhoneNumber = sArg;
 //       CallData.stringPANI	           = sArg2;   
 //       CallData.stringInfoCode            = sArg3;
 //       strRawData   = "NO DATA SOFTWARE LOAD"; 
 //       if (TRUNK_TYPE_MAP.TrunkType[CallData.intTrunk] == SIP_TRUNK) {CallData.strSIPphoneNumber = sArg4;}
 //       else if (CallData.stringPANI == strBLANK_PANI)                {CallData.stringPANI = "";} 
 //       break;
   case ASTERSIK_AMI_LIST:
        if (fLoad_SIP_PhoneNumbers(enumArg, intPortNum, sArg, boolLineRoll,boolNG_SIP_911_NON_AUDIOCODES)) {return true;}
        break;
        
   default:
        // Coding error
        SendCodingError("ExperientDataClass.cpp - Coding Error in  ExperientDataClass::fLoad_PhoneNumbers()");
        return false;

  }// end switch (enumArg)

 objMessage.fMessage_Create(LOG_WARNING, intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, 
                            objBLANK_ALI_DATA, ANI_MESSAGE_323e, sPhoneNumber,
                            ASCII_String(strRawData.c_str(), strRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(enumArg2,objMessage);
 return false;

}


bool ExperientDataClass::fLoad_CallbackVerfied(threadorPorttype enumArg, string stringArg, string strXMLData) 
{
 if      (stringArg == "true") {CallData.boolCallBackVerified = true;}
 else if (stringArg == "True") {CallData.boolCallBackVerified = true;}
 else if (stringArg == "TRUE") {CallData.boolCallBackVerified = true;}
 else if (stringArg == "1")    {CallData.boolCallBackVerified = true;}
 else                          {CallData.boolCallBackVerified = false;} 


 return true;
}


bool ExperientDataClass::fLoad_pANI(threadorPorttype enumArg, string stringArg, string strXMLData)
{
// MessageClass   objMessage;

 this->CallData.stringPANI = this->ALIData.I3Data.strPANI = stringArg;
 if (CallData.stringPANI.empty()) {return true;}

 if (!ValidateFieldAndRange(enumArg, CallData.stringPANI, strXMLData, 1, "pANI", 9999999999ULL, 0)){return false;}

 return true;
}

void ExperientDataClass::fUpdateGUILineViewOnly(int iLineView)
{
 size_t                                  sz;
 extern Telephone_Devices                TelephoneEquipment;

 if (iLineView < 0)                                          {return;}
 if (iLineView > (int) TelephoneEquipment.NumberofLineViews) {return;}

 sz = FreeswitchData.vectPostionsOnCall.size();
 for (unsigned int i = 0; i < sz; i++) {FreeswitchData.vectPostionsOnCall[i].iGUIlineView = iLineView;}

 sz = FreeswitchData.vectDestChannelsOnCall.size();
 for (unsigned int i = 0; i < sz; i++) {FreeswitchData.vectDestChannelsOnCall[i].iGUIlineView = iLineView;}

 sz = CallData.ConfData.vectConferenceChannelData.size();
 for (unsigned int i = 0; i < sz; i++) {CallData.ConfData.vectConferenceChannelData[i].iGUIlineView = iLineView;}

}






void ExperientDataClass::fUpdateGUILineViewandLineNumber(int iLineView)
{
 size_t                                  sz;
 extern Telephone_Devices                TelephoneEquipment;

 if (iLineView < 0)                                          {return;}
 if (iLineView > (int) TelephoneEquipment.NumberofLineViews) {return;}

 sz = FreeswitchData.vectPostionsOnCall.size();
 for (unsigned int i = 0; i < sz; i++) {FreeswitchData.vectPostionsOnCall[i].iGUIlineView = FreeswitchData.vectPostionsOnCall[i].iLineNumber = iLineView;}

 sz = FreeswitchData.vectDestChannelsOnCall.size();
 for (unsigned int i = 0; i < sz; i++) {FreeswitchData.vectDestChannelsOnCall[i].iGUIlineView = FreeswitchData.vectDestChannelsOnCall[i].iLineNumber = iLineView;}

 sz = CallData.ConfData.vectConferenceChannelData.size();
 for (unsigned int i = 0; i < sz; i++) {CallData.ConfData.vectConferenceChannelData[i].iGUIlineView = CallData.ConfData.vectConferenceChannelData[i].iLineNumber = iLineView;}

}

bool ExperientDataClass::fLoad_ConferenceData(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortorTrunk, bool boolMaybeBlank, string strXMLData)
{
 MessageClass objMessage;
 Port_Data    objPortData;

 
 objPortData.fLoadPortData(enumArg2, intPortorTrunk);
 switch(enumArg)
  {
   case SOFTWARE:
        if (stringArg.empty()) {return false;}
        CallData.strConferenceNumber = stringArg;
        if(!Validate_Integer(CallData.strConferenceNumber)) 
         {
          objMessage.fMessage_Create(LOG_WARNING,323, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, 
                                     objBLANK_ALI_DATA, ANI_MESSAGE_323i, CallData.strConferenceNumber,
                                     "NO DATA SOFTWARE LOAD","","","","", NORMAL_MSG, NEXT_LINE);
          enQueue_Message(ANI,objMessage);
          return false;
         }
        CallData.intConferenceNumber = char2int( CallData.strConferenceNumber.c_str());
        return true;

   case XML_DATA_FIELD:
        CallData.strConferenceNumber = stringArg;
        if(!ValidateFieldAndRange(enumArg2, stringArg, strXMLData, intPortorTrunk, "ConferenceNumber", 999, 100)) { return false;}
        CallData.intConferenceNumber = char2int( CallData.strConferenceNumber.c_str());
        return true;

   default:
        CallData.strConferenceNumber = CallData.stringTrunk;
        CallData.intConferenceNumber = CallData.intTrunk;
        return true; 
  }

 return false;
}

void ExperientDataClass::fSetNENA_ID_Vars() {
 if (this->ALIData.I3Data.strNenaCallId.length()) {
  this->strNenaCallId = this->ALIData.I3Data.strNenaCallId;
 }

 if (this->ALIData.I3Data.strNenaIncidentId.length()) {
  this->strNenaIncidentId = this->ALIData.I3Data.strNenaIncidentId;
 }
 return;
}



bool ExperientDataClass::fLoad_Trunk(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortorTrunk, bool boolMaybeBlank, string strXMLData)
{
 unsigned long long int         intTemp;
 MessageClass   		objMessage;
 int            		intLogCode;
 Port_Data                      objPortData;
 string                         stringRawData = stringArg;
 string                         stringTemp;

 objPortData.fLoadPortData(enumArg2, intPortorTrunk);
 if (enumArg2 == ANI){ intLogCode = 323;}
 else                { intLogCode = 710;}
 
 switch (enumArg)
  {
   case XML_DATA_FIELD:
        CallData.stringTrunk = stringArg;
        stringRawData = strXMLData;
        break;
   case SOFTWARE:
        CallData.intTrunk = intPortorTrunk;
        CallData.stringTrunk = int2strLZ(CallData.intTrunk);
        return true;
   case ASTERSIK_AMI_LIST:
        CallData.stringTrunk = ParseFreeswitchData(stringArg, ASTERISK_AMI_USEREVENT_TRUNK_HEADER);
        // if a 3 digit number erase 1st digit.
        if(CallData.stringTrunk.length() == 3) {CallData.stringTrunk.erase(0,1);}
        break;

   default:
        // ERROR
        SendCodingError("ExperientDataClass.cpp - Coding Error ExperientDataClass::fLoad_Trunk()");
        return false;

  } // end switch

 if ((boolMaybeBlank)&&(CallData.stringTrunk == "")){return true;}
 if (CallData.stringTrunk.length() > 3)             {CallData.stringTrunk="Length Error";}

 if (!Validate_Integer(CallData.stringTrunk))  {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, 
                              objBLANK_ALI_DATA, KRN_MESSAGE_129a, CallData.stringTrunk,
                              ASCII_String(stringRawData.c_str(), stringRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   CallData.stringTrunk = "00"; CallData.intTrunk = 0;
   return false;
 }

 intTemp = char2int(CallData.stringTrunk.c_str());

 if(!Validate_Trunk_Number(intTemp))   {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA,
                              objBLANK_ALI_DATA, KRN_MESSAGE_129b, CallData.stringTrunk,
                              ASCII_String(stringRawData.c_str(), stringRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   CallData.stringTrunk = "00"; CallData.intTrunk = 0;
   return false;
 }

 CallData.intTrunk = intTemp;
 CallData.stringTrunk = int2strLZ(CallData.intTrunk);

 return true;

}


bool ExperientDataClass::fLoad_Position(data_format enumArg, threadorPorttype enumArg2, string stringArg,  int intPortNum, string strXMLData, bool boolMayBeBlank, bool boolMayBeZero)
{
 string  stringTemp = stringArg;
 string  strData;
 int     iLowRange;

 if (boolMayBeZero) {iLowRange = 0;}
 else               {iLowRange = 1;} 
 
 switch (enumArg)
  {
   case XML_DATA_FIELD:
        CallData.stringPosn = stringArg;
        stringTemp = strXMLData;
        break;
   case SOFTWARE:
        CallData.stringPosn = stringArg;
        stringTemp = "NO DATA -SOFTWARE INVOKATION OF FUNCTION";
        break;

   default:
        // ERROR
        SendCodingError("ExperientDataClass.cpp - Coding Error ExperientDataClass::fLoad_Position()");
        return false;

  } // end switch

 if ((boolMayBeBlank)&&(CallData.stringPosn == "")){return true;}
 if (CallData.stringPosn.length() > 3)             {CallData.stringPosn = "Length Error";}

 if (!ValidateFieldAndRange(enumArg2, CallData.stringPosn, stringTemp, intPortNum, "Position", intNUM_WRK_STATIONS, iLowRange)){return false;}

 CallData.intPosn = fInt_of_Posn();

 return true;

}


bool ExperientDataClass::fLoad_OnHold_Positions(threadorPorttype enumArg, string stringArg,  int intPortNum, string strXMLData)
{
 MessageClass objMessage;
 int          intLogCode;
 Port_Data    objPortData;
 size_t       found;

  // will need to decode later ... TBC ....
 objPortData.fLoadPortData(enumArg, intPortNum);
 switch(enumArg)
  {
   case WRK:
            intLogCode =710; break;
   default:
            intLogCode = 999;
  }

 if (stringArg.empty()){return true;}

 found = stringArg.find_first_not_of("0123456789\tPCD");
 if (found == string::npos) {CallData.ConfData.strOnHoldPositions = stringArg; return true;}

 objMessage.fMessage_Create(LOG_WARNING, intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objPortData, objBLANK_FREESWITCH_DATA,
                                objBLANK_ALI_DATA, WRK_MESSAGE_710h, stringArg,
                                ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(enumArg,objMessage);

 return false;
}

bool ExperientDataClass::fLoad_Conference_Positions(threadorPorttype enumArg, string stringArg,  int intPortNum, string strXMLData, bool boolLoadHistory)
{
 MessageClass objMessage;
 int          intLogCode;
 Port_Data    objPortData;
 size_t       found;

 // will need to decode later ... TBC ....

 objPortData.fLoadPortData(enumArg, intPortNum);
 switch(enumArg)
  {
   case WRK:
            intLogCode =710; break;
   default:
            intLogCode = 999;
  }
 
 if (boolLoadHistory) { CallData.ConfData.strHistoryConferencePositions = stringArg;}
 else                 { CallData.ConfData.strActiveConferencePositions  = stringArg;}

 if (stringArg.empty()){return true;}
 
 found = stringArg.find_first_not_of("0123456789\tPCD");
 if (found == string::npos) {return true;}

 objMessage.fMessage_Create(LOG_WARNING, intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, 
                            objBLANK_ALI_DATA, WRK_MESSAGE_710f, stringArg,
                            ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(enumArg,objMessage);

 return false;
}

bool ExperientDataClass::VerifyCallBack()
{
// if (CallData.boolCallBackVerified) {return true;}

//fLoadCallbackfromALI() strBLANK_PANI
 switch (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType)
  {
   case CAMA: case WIRELESS: case INDIGITAL_WIRELESS: case INTRADO:
        if ((boolANI_20_DIGIT_VALID_CALLBACK) && (CallData.intANIformat == 20) )                                   {CallData.boolCallBackVerified = true; break;}
 //       if ((CallData.stringPANI != strBLANK_PANI) && (CallData.stringPANI != CallData.stringTenDigitPhoneNumber)) {CallData.boolCallBackVerified = true; break;}
        CallData.boolCallBackVerified = fLoadCallbackfromALI();

        break;

   case CLID: case LANDLINE: case SIP_TRUNK: case NG911_SIP: case MSRP_TRUNK: case REFER:
        CallData.boolCallBackVerified = true;
        break;
 
  }
 return CallData.boolCallBackVerified;
}

bool ExperientDataClass::fLoad_MSG_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{
 int            intLogCode = 710;
 MessageClass   objMessage;
 Port_Data      objPortData;

 objPortData.fLoadPortData(enumArg2, intPortNum);

// stringWRKStationMSGNumber = stringArg;

 if (!Validate_Integer(stringArg))   {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, 
                              objBLANK_ALI_DATA,KRN_MESSAGE_129c, stringArg,
                              ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   return false;
 }

 intLogMessageNumber = char2int(stringArg.c_str());

  if (!ValidateFieldAndRange(enumArg2, stringArg, stringArg, 0, "Message Number", 799, 750))  {
   
    objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objPortData, objBLANK_FREESWITCH_DATA, 
                               objBLANK_ALI_DATA, KRN_MESSAGE_129d, stringArg,
                               ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
    enQueue_Message(enumArg2,objMessage);
    return false;
  }
 return true;

}



bool ExperientDataClass::fLoad_MSG_Type(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{
 int            intLogCode = 710;
 MessageClass   objMessage;
 Port_Data      objPortData;

 objPortData.fLoadPortData(enumArg2, intPortNum);
// stringWRKStationMSGType = stringArg;

 if      (stringArg == "Alarm")    {intLogType = LOG_ALARM;   return true;}
 else if (stringArg == "Warning")  {intLogType = LOG_WARNING; return true;}
 else if (stringArg == "Info")     {intLogType = LOG_INFO;    return true;}
 else if (stringArg == "TDD")      {intLogType = LOG_TDD;     return true;}

 objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, 
                            objBLANK_ALI_DATA, KRN_MESSAGE_129e, stringArg,
                            ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(enumArg2,objMessage);
 return false;

}


bool ExperientDataClass::fLoad_MSG_Text(data_format enumArg, threadorPorttype enumArg2, string stringArg, bool boolEncodedText)
{
 MessageClass           objMessage;
 string                 stringTemp = "";
 string                 strSuffix = stringArg;
 XMLParserBase64Tool    b64;
 unsigned char*         ptrUcharDecodedTXT; 
 int                    intLength;
 XMLError               xe;
 string                 stringError;
 Port_Data              objPortData;

 
 if (stringArg == "")  {
  
   stringWRKStationMSGText	 = "";
   return true;
 }
 if (boolEncodedText)   {
  
   ptrUcharDecodedTXT    = b64.decode(stringArg.c_str(),&intLength, &xe);

   if(xe == eXMLErrorNone)  {
    
    for (int i=0; i < intLength; i++){strSuffix += ptrUcharDecodedTXT[i];}
   }
   else  {
    
     objPortData.fLoadPortData(enumArg2, 1);
     stringError = ConvertXMLError2String(xe);
     objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objPortData, objBLANK_FREESWITCH_DATA,
                                objBLANK_ALI_DATA, WRK_MESSAGE_710g, stringError, stringArg);
     enQueue_Message(WRK,objMessage); 
   }
 } // boolencoded text

 // note these: do not use other types !
 switch (intLogType)
  {
   case LOG_ALARM:    stringTemp = ">[ALARM] "   + strSuffix; break;
   case LOG_WARNING:  stringTemp = ">[WARNING] " + strSuffix; break;
   case LOG_INFO:     stringTemp = ">[INFO] "    + strSuffix; break;
   case LOG_TDD:      stringTemp = ">[TDD] "     + strSuffix; break;
   default:           stringTemp = strSuffix;
  }
 b64.freeBuffer();
 stringWRKStationMSGText = stringTemp;

 return true;
}

bool ExperientDataClass::fLoad_RCC_State(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{
 MessageClass                   objMessage;
 Port_Data                      objPortData;
 string                         strData;
 int                            intLogCode = 710;

 objPortData.fLoadPortData(enumArg2, intPortNum);

 if      (stringArg == "Radio")     {enumRCCstate = RADIO;    return true;}
 else if (stringArg == "Telephone") {enumRCCstate = TELEPHONE;return true;}

 objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, 
                            objBLANK_ALI_DATA,KRN_MESSAGE_129l, stringArg,
                            ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(enumArg2,objMessage);                 
 return false;
}

bool ExperientDataClass::fLoad_Participant(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{

 int                            intLogCode = 710;
 MessageClass                   objMessage;
 Port_Data                      objPortData;
 size_t                         found;
 string                         strTemp;
 string                         A = ANI_CONF_MEMBER_DEST_PREFIX;
 string                         B = ANI_CONF_MEMBER_CALLER_PREFIX;
 string                         C = ANI_CONF_MEMBER_CALLER_PREFIX;
 
 objPortData.fLoadPortData(enumArg2, intPortNum);
 
 CallData.ConfData.strParticipant = stringArg;

 if (stringArg.empty())  {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, 
                              objBLANK_ALI_DATA,KRN_MESSAGE_129h, stringArg,
                              ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   return false;
 }

 if (!((stringArg[0] == A[0])||(stringArg[0] == B[0])||(stringArg[0] == C[0])))  {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA,
                              objBLANK_ALI_DATA,KRN_MESSAGE_129h, stringArg,
                              ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   return false;
 }

 if (stringArg.length() < 2) {SendCodingError( "ExperientDataClass.cpp - Participant length Less than 2 in fn ExperientDataClass::fLoad_Participant"); return false;}
 strTemp.assign(stringArg,1,string::npos);

 found = strTemp.find_first_not_of("0123456789");

 if (found != string::npos)   {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, 
                              objBLANK_ALI_DATA,KRN_MESSAGE_129h, stringArg,
                              ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   return false;
 }

 return true;

}

bool ExperientDataClass::fLoad_DTMF_Char(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData) { 

 int                            intLogCode = 710;
 MessageClass                   objMessage;
 Port_Data                      objPortData;
 size_t				found;
 bool				boolCheck;

 objPortData.fLoadPortData(enumArg2, intPortNum);

 found = stringArg.find_first_not_of("*#0123456789ABCD");
 boolCheck = (found != string::npos);
// //cout << "check -> " << boolCheck << endl;
 if ((stringArg == "")||(stringArg.length() > 1)||(boolCheck)) {

  objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objPortData, objBLANK_FREESWITCH_DATA, 
                             objBLANK_ALI_DATA,KRN_MESSAGE_129m, stringArg,
                             ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
  enQueue_Message(enumArg2,objMessage);

  this->strDTMFChar.clear();
  return false;
 }
 this->strDTMFChar    = stringArg;
 return true;
}

bool ExperientDataClass::fLoad_Call_Id(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{
 
 int                            intLogCode = 710;
 MessageClass                   objMessage;
 Port_Data                      objPortData;

 objPortData.fLoadPortData(enumArg2, intPortNum);


 if (stringArg == ""){CallData.intUniqueCallID = 0; CallData.stringUniqueCallID = ""; return true;}

 if (!Validate_Integer(stringArg))   {
  
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, 
                              objBLANK_ALI_DATA,KRN_MESSAGE_129h, stringArg,
                              ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg2,objMessage);
   return false;
 }
 CallData.intUniqueCallID    = char2int(stringArg.c_str());
 CallData.stringUniqueCallID = stringArg;
 return true;

}

bool ExperientDataClass::fLoad_Encoding(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{
 
 int                            intLogCode = 710;
 MessageClass                   objMessage;
 Port_Data                      objPortData;

 objPortData.fLoadPortData(enumArg2, intPortNum);

 if (stringArg == "Base64"){return true;}

 
 objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA,
                            objBLANK_ALI_DATA,KRN_MESSAGE_129g,stringArg,
                            ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(enumArg2,objMessage);
 return false;

}







bool ExperientDataClass::fLoad_Transfer_Type(threadorPorttype enumArg, string stringArg, string strRawData, int intPortNum)
{
 MessageClass	                objMessage;
 int                            intLogCode = 710;
 Port_Data                      objPortData;
 
 CallData.TransferData.eTransferMethod = DetermineTransferMethod(stringArg);
 CallData.TransferData.strTransferType = stringArg;
 // INDIGITAL AND INTRADO ARE TANDEM ...

 switch(CallData.TransferData.eTransferMethod)
  {
   case mTANDEM:
        break;
   case mATTENDED_TRANSFER:
        break;       
   case mBLIND_TRANSFER:
        break;
   case mGUI_TRANSFER:
        break;
   case mNG911_TRANSFER:
        break;
   case mFLASHOOK:
        break; 
   default:
        objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objPortData, objBLANK_FREESWITCH_DATA,
                                   objBLANK_ALI_DATA,KRN_MESSAGE_129k,stringArg,
                                   ASCII_String(strRawData.c_str(), strRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
        enQueue_Message(enumArg,objMessage);
        return false;

  }


return true;
}


void ExperientDataClass::fSetUniqueCallID(int intTag)
{
 struct timespec                timespecTimeNow;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 CallData.intUniqueCallID = Julian_Date(timespecTimeNow)*1000+intTag;
 CallData.stringUniqueCallID = int2str(CallData.intUniqueCallID);
}

/*
void ExperientDataClass::fSetEventUniqueID(int intTag)
{
 struct timespec                timespecTimeNow;
 unsigned long long int         intID;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 intID = Julian_Date(timespecTimeNow)*1000+intTag;
 strEventUniqueID = int2str(intID);
}

*/



void ExperientDataClass::fCheckOnHoldMessage(string strTime)
{
 bool bPreviousALI = this->ALIData.boolALIRecordReceived;
// ExperientDataClass TestObj = this;

 this->stringTimeOfEvent = strTime;
 if ((this->CallData.fNumPositionsInConference() <= this->CallData.fNumPositionsOnHold())&&(this->CallData.fNumPositionsInConference() > 0))
  {      
   this->WindowButtonData.boolTransferWindow = false;
   this->TDDdata.fSetWindow(false);                    
   this->TextData.fSetWindow(false);
   this->fLoad_CallState(MAIN,XML_STATUS_MSG_ON_HOLD,"SOFTWARE LOAD");
   this->fCallInfo_XML_Message(XML_STATUS_CODE_ON_HOLD, bPreviousALI);
//  //cout << "A" << endl;
  }
 else
  {
   this->fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
   this->fCallInfo_XML_Message(XML_STATUS_CODE_OFF_HOLD_CONNECTED, bPreviousALI);
//  //cout << "B" << endl;
  }
 
 Queue_WRK_Event(this);
}


/*

void ExperientDataClass::fSetANICommand(int intArg)
{
 switch(intArg)
  {
   case 1: enumANIFunction  = CONNECT;                  return;
   case 2: enumANIFunction  = DISCONNECT;               return;
   case 3: enumANIFunction  = TRANSFER;                 return;
   case 4: enumANIFunction  = CONFERENCE;               return;
   case 5: enumANIFunction  = HOLD_ON;                  return;
   case 6: enumANIFunction  = HOLD_OFF;                 return;
   case 7: enumANIFunction  = ABANDONED;                return;
//   case 8: enumANIFunction  = NO_CORRELATION;           return;
//   case 9: enumANIFunction  = CONFIG_CHANGE;            return;
   case 10: enumANIFunction = ALARM;                    return;
   case 11: enumANIFunction = RINGING;                  return;
   case 12: enumANIFunction = ALI_REQUEST;              return;
   case 13: enumANIFunction = TIME_DATE_CHANGE;         return;
//   case 14: enumANIFunction = SIXTY_SEC_NO_ANSWER;      return;
   case 15: enumANIFunction = TRANSFER_NUMBER_PROGRAM;  return;
   case 16: enumANIFunction = HEARTBEAT;                return;
   case 17: enumANIFunction = ALI_REPEAT_SCROLL_BACK;   return;
   case 18: enumANIFunction = SILENT_ENTRY_LOG_OFF;     return;
   default:
   SendCodingError("ExperientDataClass.cpp - Coding Error in ExperientDataClass::fSetANICommand(int intArg)\n");
   return;
  }
}

*/



freeswitch_event Find_Freeswitch_Event(string strInput)
{
 size_t EventPosition;
 string strEvent;

 EventPosition = strInput.find(FREESWITCH_CLI_REPLY_OK);
 if (EventPosition != string::npos){return REPLY_OK;}

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CALL_UPDATE);
 if (EventPosition != string::npos){return FREESWITCH_CALL_UPDATE;}

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CHANNEL_PROGRESS_MEDIA);
 if (EventPosition != string::npos){return FREESWITCH_CHANNEL_PROGRESS;}


 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CHANNEL_PROGRESS);
 if (EventPosition != string::npos){return FREESWITCH_CHANNEL_PROGRESS;}

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_MEDIA_BUG_STOP);
 if (EventPosition != string::npos){return FREESWITCH_MEDIA_BUG_STOP;}

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_MEDIA_BUG_START);
 if (EventPosition != string::npos){ return FREESWITCH_MEDIA_BUG_START;}

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_HEARTBEAT);
 if (EventPosition != string::npos){return FREESWITCH_HEARTBEAT;} 

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CUSTOM);
 if (EventPosition != string::npos){return FREESWITCH_CUSTOM;}

 EventPosition = strInput.find(FREESWITCH_CLI_EVENT_PRESENCE_IN);
 if (EventPosition != string::npos){return FREESWITCH_PRESENCE_IN;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_HOLD);
 if (EventPosition != string::npos){return HOLD;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_UNHOLD);
 if (EventPosition != string::npos){return UNHOLD;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_HANGUP_COMPLETE);
 if (EventPosition != string::npos){return HANGUP;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CHANNEL_ANSWER);
 if (EventPosition != string::npos){return FREESWITCH_CHANNEL_ANSWER;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CHANNEL_BRIDGE);
 if (EventPosition != string::npos){return FREESWITCH_CHANNEL_BRIDGE;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_CHANNEL_ORIGINATE);
 if (EventPosition != string::npos){return FREESWITCH_CHANNEL_ORIGINATE;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_MSRP_TEXT_MESSAGE);
 if (EventPosition != string::npos){return FREESWITCH_MSRP_TEXT_MESSAGE;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_SMS_MESSAGE);
 if (EventPosition != string::npos){return FREESWITCH_SMS_MESSAGE;}

EventPosition = strInput.find(FREESWITCH_CLI_EVENT_NOTIFY_IN);
 if (EventPosition != string::npos){
  EventPosition = strInput.find(FREESWITCH_CLI_EVENT_MESSAGE_SUMMARY);
  if (EventPosition != string::npos){return FREESWITCH_MWI_SUMMARY;}
 }

 return NOTFOUND;

}


void ExperientDataClass::UPdate_CallData_Conference_Data_Vector()
{
 size_t       sizePosnOnCallVector, sizeDestOnCallVector, sizeConfDataVector;
 int          index;
 int          iPosition;
 string       stringPosition;
    
 sizeDestOnCallVector = FreeswitchData.vectDestChannelsOnCall.size(); 
 sizePosnOnCallVector = FreeswitchData.vectPostionsOnCall.size();

 


 // Add records that have not been previously added to the Conference Data Vector 
 for (unsigned int i = 0; i < sizeDestOnCallVector; i++)
  {
   index = CallData.fLegendInConferenceChannelDataVector(FreeswitchData.vectDestChannelsOnCall[i].strConfDisplay);
   if (index < 0) {CallData.ConfData.vectConferenceChannelData.push_back(FreeswitchData.vectDestChannelsOnCall[i]);}                 
  }
 for (unsigned int i = 0; i < sizePosnOnCallVector; i++)
  {
   index = CallData.fLegendInConferenceChannelDataVector(FreeswitchData.vectPostionsOnCall[i].strConfDisplay);
   if (index < 0) {CallData.ConfData.vectConferenceChannelData.push_back(FreeswitchData.vectPostionsOnCall[i]);}
  }

 sizeConfDataVector   = CallData.ConfData.vectConferenceChannelData.size();
 
 //update position records in the conference with that of those in Pos Vector
 for (unsigned int i = 0; i < sizeConfDataVector; i++)
  {
   // Mark Records that have no match in the current Dest or Posn Vector as disconnected ....
   // Mark /replace Position Records that are on Hold
   index = FreeswitchData.fFindLegendInPosnVector(CallData.ConfData.vectConferenceChannelData[i].strConfDisplay);
   if (index >= 0)
    {
     iPosition = CallData.ConfData.vectConferenceChannelData[i].iPositionNumber;
     if (iPosition <= 0) {continue;}
     CallData.ConfData.vectConferenceChannelData[i] = FreeswitchData.vectPostionsOnCall[index];
     CallData.ConfData.vectConferenceChannelData[i].boolOnHold = CallData.fIsOnHold(iPosition);
    }
   else { index = FreeswitchData.fFindLegendInDestVector(CallData.ConfData.vectConferenceChannelData[i].strConfDisplay);}

   if (index < 0) { CallData.ConfData.vectConferenceChannelData[i].boolConnected = false;}
   else           { CallData.ConfData.vectConferenceChannelData[i].boolConnected = true;}

  }
 


}
freeswitch_event DeterminePresenceEvent(string strArg)
{
 size_t SearchPosition;


 //    these lines of code were used 
 //    to counter a freeswitch Bug which was fixed

  SearchPosition =  strArg.find(FREESWITCH_CLI_STATUS_ON_HOLD);
  if (SearchPosition != string::npos) {return HOLD;}
  
  SearchPosition =  strArg.find(FREESWITCH_CLI_STATUS_UN_HOLD);
  if (SearchPosition != string::npos) {return UNHOLD;}


  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_PRESENCE_CALL_INFO_STATE_PROG);
  if (SearchPosition != string::npos) {return FREESWITCH_PRESENCE_PROGRESSING;}  

//  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_PRESENCE_CALL_INFO_STATE_IDLE);
//  if (SearchPosition != string::npos) {return FREESWITCH_PRESENCE_IDLE;}

  
  return NOTFOUND;
}

freeswitch_event DetermineCustomEvent(string strArg)
{
 size_t SearchPosition;
 
  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SUBCLASS_MEMORY_USAGE);
  if (SearchPosition != string::npos) {return FREESWITCH_MEM_USAGE;}

  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SUBCLASS_REGISTER);
  if (SearchPosition != string::npos) {return FREESWITCH_REGISTER;}

  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SUBCLASS_PRE_REGISTER);
  if (SearchPosition != string::npos) {return FREESWITCH_REGISTER;}

  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SUBCLASS_REGISTER_ATTEMPT);
  if (SearchPosition != string::npos) {return FREESWITCH_REGISTER;}
 
  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SUBCLASS_CONFERENCE);
  if (SearchPosition != string::npos) {return FREESWITCH_CONFERENCE;} 
 
  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SUBCLASS_MYEVENT_MESSAGE);
  if (SearchPosition != string::npos) {return FREESWITCH_MYEVENT_MESSAGE;}

  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_TDD_RECV_MESSAGE);
  if (SearchPosition != string::npos) {return FREESWITCH_TDD_CHAR;} 

   SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_SOFIA_ERROR_MESSAGE);
  if (SearchPosition != string::npos) {return FREESWITCH_SOFIA_ERROR;}  

   SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_MSRP_TEXT_MESSAGE_RING);
   if (SearchPosition != string::npos) {return FREESWITCH_MSRP_TEXT_MSG_RING;}
//  if (SearchPosition != string::npos) {//cout << "custom msrp event" << endl;return FREESWITCH_MSRP_TEXT_MESSAGE;}

  
  SearchPosition =  strArg.find(FREESWITCH_CLI_EVENT_VALET_PARKING_INFO_MESSAGE);
  if (SearchPosition != string::npos) {return FREESWITCH_VALET_PARK_MSG;}

  return NOTFOUND;
}


ani_functions DetermineMyEvent(string strArg)
{
 size_t SearchPosition;
// note search order is important for vars that are prefix's for other vars
// put the longest first i.e. ATTXFER and ATTXFER_FAIL put ATTXFER_FAIL
// first
  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_RINGDATA);
  if (SearchPosition != string::npos) {return RINGING;}
 
  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_ATT_TRANSFER_FAIL);
  if (SearchPosition != string::npos) {return ATT_TRANSFER_FAIL;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_ATT_TRANSFER_REJECTED);
  if (SearchPosition != string::npos) {return ATT_TRANSFER_REJ;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_ATTXFER);
  if (SearchPosition != string::npos) {return ATTENDED_TRANSFER;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_BLIND_XFER);
  if (SearchPosition != string::npos) {return BLIND_TRANSFER;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_CONF_ATT_XFER);
  if (SearchPosition != string::npos) {return CONFERENCE_ATT_XFER;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_DIAL_OUT);
  if (SearchPosition != string::npos) {return DIAL_DEST;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_RING_BACK);
  if (SearchPosition != string::npos) {return RING_BACK;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_TRANSFER_FAIL);
  if (SearchPosition != string::npos) {return TRANSFER_FAIL;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_CONF_ATT_TRANSFER_FAIL);
  if (SearchPosition != string::npos) {return CONFERENCE_ATT_TRANSFER_FAIL;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_POSITION_BARGE);
  if (SearchPosition != string::npos) {return BARGE_ON_POSITION;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_CONFERENCE_BARGE);
  if (SearchPosition != string::npos) {return BARGE_ON_CONFERENCE;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_LOCATION_DATA);
  if (SearchPosition != string::npos) {return LOCATION_URI;}

  SearchPosition =  strArg.find(FREESWITCH_MYEVENT_IRR_RECORDING_FILE_LOCATION);
  if (SearchPosition != string::npos) {return IRR_FILE_DATA;}

  return UNCLASSIFIED;
}

ani_functions DetermineConferenceEvent(string strArg)
{
 size_t SearchPosition;

  SearchPosition =  strArg.find(FREESWITCH_CONFERENCE_ADD_MEMBER);
  if (SearchPosition != string::npos) {return CONFERENCE_JOIN;}

  SearchPosition =  strArg.find(FREESWITCH_CONFERENCE_DEL_MEMBER);
  if (SearchPosition != string::npos) {return CONFERENCE_LEAVE;}

  return UNCLASSIFIED;
}

int ChannelStateNumber(string strInput)
{
 string strData;
 size_t found;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CHANNEL_STATE_NUMBER_WO_COLON );
 strData = RemoveLeadingSpaces(RemoveTrailingSpaces(strData));

 found =  strData.find_first_not_of("0123456789");
 if (found != string::npos)         {return 0;}

 return char2int(strData.c_str());
}

bool ChannelStateSeven(string strInput)
{
 size_t SearchPosition;

 SearchPosition =  strInput.find(FREESWITCH_CHANNEL_STATE_NUMBER_SEVEN);
 if (SearchPosition != string::npos) {return true;}

 return false;
}
bool ChannelStateFour(string strInput)
{
 size_t SearchPosition;

 SearchPosition =  strInput.find(FREESWITCH_CHANNEL_STATE_NUMBER_FOUR);
 if (SearchPosition != string::npos) {return true;}

 return false;
}

bool CallFromThreeWayContext(string strArg)
{
 size_t SearchPosition;
 string strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_CONTEXT_WO_COLON);

 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_THREEWAY_CONTEXT);
 if (SearchPosition != string::npos) {return true;}


 SearchPosition =  strArg.find(FREESWITCH_CLI_CALLER_CONTEXT_THREEWAY_CONTEXT);
 if (SearchPosition != string::npos) {return true;}

 return false;

}

bool CallFromBlindTransferContext(string strArg)
{
 size_t SearchPosition;
 string strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_CONTEXT_WO_COLON);

 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_BLIND_XFER_HANDLER);
 if (SearchPosition != string::npos) {return true;}
 
 SearchPosition =  strArg.find(FREESWITCH_CLI_CALLER_CONTEXT_BLIND_TRANSFER_CONTEXT);
 if (SearchPosition != string::npos) {return true;}

 return false;

}

bool CallFromFreeswitchTransfer(string strArg)
{
 size_t SearchPosition;

 SearchPosition = strArg.find(FREESWITCH_CLI_VARIABLE_TRANSFER_FROM_POSITION_WITH_COLON);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_VARIABLE_TRANSFER_TO_WITH_COLON);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_VARIABLE_TRANSFER_METHOD_WO_COLON);
 if (SearchPosition != string::npos) {return true;}


 return false;
}




bool CallFromMSRPContext(string strArg)
{
 size_t SearchPosition;
 string strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_CONTEXT_WO_COLON);
  
 SearchPosition =  strArg.find(FREESWITCH_CLI_CALLER_CONTEXT_MSRP_DEFAULT);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_MSRP_DEFAULT);
 if (SearchPosition != string::npos) {return true;}

 return false;

}


bool CallFromDefaultContext(string strArg)
{
 size_t SearchPosition;
 string strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_CONTEXT_WO_COLON);

 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_DEFAULT);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_CALLER_CONTEXT_DEFAULT_CONTEXT);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_CALLER_CONTEXT_INBOUND_FXO_SLA_CONTEXT);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_CALLER_CONTEXT_INBOUND_FXO_CONTEXT);
 if (SearchPosition != string::npos) {return true;}

 return false;

}


bool CallFromTransferContext(string strArg, bool bOtherLeg=true)
{
 size_t SearchPosition;
 string strData;

 if (bOtherLeg) {strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_OTHER_LEG_CONTEXT_WO_COLON);}
 else           {strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_CONTEXT_WO_COLON);}

 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_BLIND_XFER_HANDLER);
 if (SearchPosition != string::npos) {return true;}
 
 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_BRIDGED_ATT_XFER_HANDLER);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strData.find(FREESWITCH_CLI_UNDERSCORE_CONFERENCE_ATT_XFER);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_OTHER_LEG_CONTEXT_BLIND_XFER_HANDLER);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_OTHER_LEG_CONTEXT_BRIDGED_ATT_XFER_HANDLER);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_OTHER_LEG_ROLLOVER_AUDIOCODES_TWO);
 if (SearchPosition != string::npos) {return true;}

 SearchPosition =  strArg.find(FREESWITCH_CLI_OTHER_LEG_CONTEXT_CONFERENCE_ATT_XFER);
 if (SearchPosition != string::npos) {return true;}

 return false;
}

bool   CallerDirectionOutbound(string strArg)
{
 size_t SearchPosition;
 string strData;

 strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_DIRECTION_WO_COLON);
 if (strData != "outbound") {return false;}

 strData = ParseFreeswitchData(strArg, FREESWITCH_CLI_CALLER_LOGICAL_DIRECTION_WO_COLON);
 if (strData != "outbound") {return false;}

 return true;
}

bool CallerIDNameIsFrom911Trunk(string strArg)
{
 size_t SearchPosition;

 SearchPosition =  strArg.find(FREESWITCH_CALLER_CALLER_ID_NAME_911_TRUNK);
 if (SearchPosition != string::npos) {return true;}

 return false;

}

bool ExperientDataClass::fLoad_FREESWITCH_Data(const char* charString, int intPortNum)
{
 MessageClass                   objMessage;
 string                         sAlarmCode, sAlarmText;
 string                         strError;
 Port_Data                      objPortData;
 string                         strData;
 string                         strKey;
 string                         strInput = charString;
 string                         strTemp, strTemptwo, strTempthree, strTempfour, strTempfive;
 string                         strNumInConference;
 string                         strDirection;
 string                         strSearchKey;
 string                         strMWIgateway;
 string                         strMWImessage;
 string                         strMWIwaiting;
 string                         strMWIaccount;
 int                            index;
 int                            iConfIndex;
 int                            iPosition;
 int                            integerPosition;
 int                            iswitch = 0;
 bool                           boolLineRoll = false;
 bool                           boolNG911SIPnonAudiocodes = false;
 bool                           boolA, boolB, boolC, boolD;
 bool                           boolE, boolF, boolG, boolH;
 bool                           boolI, boolJ;
 string                         strPositionChannelName;
 string                         strOne, strTwo;
 size_t                         found;
 extern Telephone_Devices       TelephoneEquipment;
 extern Conference_Number       objConferenceList; 
 extern bool                    bool_BCF_ENCODED_IP;
 extern Memory_Data             MemoryData; 

 FreeswitchData.fClear();
 objPortData.fLoadPortData(ANI, intPortNum);


 FreeswitchData.enumFreeswitchEvent = Find_Freeswitch_Event(charString);
 switch (FreeswitchData.enumFreeswitchEvent)  {
   case REPLY_OK:
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 364, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, 
                                   objBLANK_ALI_DATA,ANI_MESSAGE_364, ASCII_String(strInput.c_str(),strInput.length() ) );
        enQueue_Message(ANI,objMessage);       
        return false;

   case FREESWITCH_HEARTBEAT:
        enumANIFunction = HEARTBEAT;
        return true;

   case FREESWITCH_MEDIA_BUG_START: case FREESWITCH_MEDIA_BUG_STOP:

        FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_MEDIA_BUG_TARGET);
        if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}

        return true;

   case FREESWITCH_MWI_SUMMARY:
        // here we got a MWI summary to notify a telephone..... 

        strMWIgateway = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_GATEWAY_NAME_WO_COLON));
        if (strMWIgateway.empty()) {return false;}
        strData = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_PL_DATA_WO_COLON));

        strData = FindandReplaceALL(strData, "\r\n", "\t");
        strData = FindandReplaceALL(strData, "\n", "\t");

        strMWIwaiting = ParseFreeswitchData(strData,FREESWITCH_CLI_MESSAGES_WAITING_WO_COLON);
        strMWImessage = ParseFreeswitchData(strData,FREESWITCH_CLI_VOICE_MESSAGE_WO_COLON);
        strMWIwaiting = RemoveLeadingSpaces(strMWIwaiting);
        strMWIwaiting = RemoveTrailingSpaces(strMWIwaiting);
        strMWImessage = RemoveLeadingSpaces(strMWImessage);
        strMWImessage = RemoveTrailingSpaces(strMWImessage);

        strMWIaccount = TelephoneEquipment.fMWIaccountofDevicewithExternGateway(strMWIgateway);
        if (strMWIaccount.empty()) {return false;}

        strData =  "sendevent message_waiting\n";
        strData += "MWI-Messages-Waiting: " + strMWIwaiting + "\n";
        strData += "MWI-Message-Account: " + strMWIaccount + "\n";
        if ((strMWIwaiting == "yes")||(strMWIwaiting == "YES")||(strMWIwaiting == "Yes") ) {
         strData += "MWI-Voice-Message: " + strMWImessage + "\n";
        }
        //pass the string in Transfer Data string
        FreeswitchData.objChannelData.strTranData = strData;
        
        return true;

   case FREESWITCH_SMS_MESSAGE:
// This may be completely removed .............
// To remove ..
        if (!TDDdata.SMSdata.fLoadLocationData(strInput))                                                                   {return false;}
        if (!TDDdata.SMSdata.fLoadBidID(strInput))                                                                          {return false;}
        if (!TDDdata.SMSdata.fLoadSMSmessage(strInput))                                                                     {return false;}
        if (!TDDdata.SMSdata.fLoadTo(strInput))                                                                             {return false;}
        if (!TDDdata.SMSdata.fLoadFrom(strInput))                                                                           {return false;}
//to remove ..
        if (!TextData.SMSdata.fLoadLocationData(strInput))                                                                   {return false;}
        if (!TextData.SMSdata.fLoadBidID(strInput))                                                                          {return false;}
        if (!TextData.SMSdata.fLoadSMSmessage(strInput))                                                                     {return false;}
        if (!TextData.SMSdata.fLoadTo(strInput))                                                                             {return false;}
        if (!TextData.SMSdata.fLoadFrom(strInput))                                                                           {return false;}
        return true;

   case FREESWITCH_MSRP_TEXT_MESSAGE:
// Location Data ?????? will it ever change with each message ?
  //      //cout << "MSRP a Before" << endl;
      //  //cout << strInput << endl;
        ALIData.I3Data.fClear();
        FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
        if (!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                  {return false;}
////cout << "a" << endl;
        FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON )));
        FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON ));
        strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
        if(!strData.empty()) {strData = strData.erase(0,1);}
        if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);}
        TextData.objLocationURI.fLoadLocationURI(strInput);
        ALIData.I3Data.objLocationURI = TextData.objLocationURI;
        if (!TextData.SMSdata.fLoadSMSmessage(strInput))                                                                     {return false;}
////cout << "b" << endl;
        if (!TextData.SMSdata.fLoadTo(strInput))                                                                             {return false;}
////cout << "c" << endl;
        if (!TextData.SMSdata.fLoadFrom(strInput))                                                                           {return false;}
        TextData.TextType = MSRP_MESSAGE;       
        FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON ));
        FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
        FreeswitchData.objChannelData.strPresenceID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_PRESENCE_ID_WO_COLON));
        iPosition = PositionNumberFromMSRPpresenceID(FreeswitchData.objChannelData.strPresenceID);   
        FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);
        FreeswitchData.objChannelData.boolMSRP= true;
 //        //cout << "MSRP After" << endl;
        //load NENA Callid & incidentid
        ALIData.I3Data.fLoadGeoLocationURI(strInput);
        ALIData.I3Data.fLoadNenaCallId(strInput);
        ALIData.I3Data.fLoadNenaIncidentId(strInput);
        ALIData.I3Data.fLoadCallInfoEmergencyCallData(strInput);
        ALIData.I3Data.fLoadPidfloEntity(strInput);
        //ALIData.I3Data.fLoadServiceList(strInput);
        //cout << ALIData.I3Data.objGeoLocationHeaderURI.strURI << endl;
        this->objGeoLocation = this->ALIData.I3Data.objGeoLocationHeaderURI; 
        return true;


   case FREESWITCH_CHANNEL_BRIDGE:
        // we primarily look at CHANNEL_ANSWER for connect information, In the case of an SLA on Hold transfer (pickoff)
        // there is not enough information to process.  We will be processing this record in the
        // SLA section in ANI
        // We also get this for Valet Park Pickup. Check to the Park app and the Dest Number .... 

        if(!FreeswitchData.fLoadBridgeDataFromFreeswitchSLA(strInput))                  {return false;}

        found = strInput.find(FREESWITCH_CLI_VARIABLE_CURRENT_APPLICATION_VALET_PARK);
        if (found != string::npos)
         {
          // This is a Valet Park pickup ....
          // Load the transfer Data
         // //cout << strInput << endl;
          if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_VARIABLE_VALET_LOT_EXTENSION)),  intPortNum)){return false;}
          FreeswitchData.enumFreeswitchEvent = FREESWITCH_VALET_PICKUP;
          return true;
         }

        // otherwise ...
        FreeswitchData.enumFreeswitchEvent = FREESWITCH_SLA_JOIN_CALL;

        
        return true; 

 
   case FREESWITCH_CHANNEL_ANSWER:
        //For each Channel we will get an CHANNEL_ANSWER event (A->B and B->A) one will have Other-Leg Channel Variables and one will not
        // Channel Answer assosicated with a "connect" (comes from the default context)
        // Channel Answer associated with a transfer will come from another context.


        // this is an Answer frpm a conference join GDIT NG911
/*
        if ((bool_BCF_ENCODED_IP)&&(FreeswitchCurrentApplicationIsConference(strInput))&&(FreeswitchDestNumberIsConfJoin(strInput)))
         {
          //cout << "GDIT conf join" << endl;
          strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_PARAMS_WO_COLON));
          strKey  = Determine_BCF_Key(strData);
          FreeswitchData.objChannelData.IPaddress.fParseIPaddressUsingValueKey(strKey, strData);

          CallData.strFreeswitchConfName = ParseFreeswitchData(strInput, FREESWITCH_VARIABLE_CONFERENCE_NAME_LC_WO_COLON );
          FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
          if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
          FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
          if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
          index = TelephoneEquipment.fIndexWithIPaddress(FreeswitchData.objChannelData.IPaddress);         
          if (index < 0)                                                                                                        {return false;}

          FreeswitchData.objChannelData.fLoadPositionNumber(TelephoneEquipment.Devices[index].intPositionNumber);
          FreeswitchData.objChannelData.boolIsPosition = true;
          FreeswitchData.objChannelData.fLoadConferenceDisplayP(FreeswitchData.objChannelData.iPositionNumber);
          FreeswitchData.objChannelData.iLineNumber = TelephoneEquipment.fFindTelephoneLineNumber(FreeswitchUsernameFromChannel(FreeswitchData.objChannelData.strChannelName));  
          FreeswitchData.objChannelData.strCallerID = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON ));
          FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON ));
          FreeswitchData.enumFreeswitchEvent = FREESWITCH_BARGE_WITH_BCF;
          return true;
         }

*/

         ////cout << "CHANNEL_ANSWER Experientdataclass" << endl;
        // this is an answer from a conference transfer .....
        boolA = bool_FREESWITCH_VERSION_212_PLUS;
        boolB = (ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON ).empty());
        boolC = (NG911_TRANSFER_TYPE_FROM_CLI(strInput) == SIP_URI_EXTERNAL_EXPERIENT);
        boolD = (NG911_TRANSFER_TYPE_FROM_CLI(strInput) == SIP_URI_INTERNAL);
        boolF = DestinationNumberisCONF_JOIN(strInput);
        boolG = BLIND_TRANSFER_GUI_FROM_CLI(strInput);
        boolH = PARK_PICKUP(strInput);
        boolI = BLIND_TRANSFER_FAIL_RINGBACK(strInput);

//  //cout << "A -> " << boolA << endl; //cout << "B -> " << boolB << endl; //cout << "C -> " << boolC << endl; //cout << "D -> " << boolD << endl; //cout << "F -> " << boolF <<endl ;
//  //cout << "G -> " << boolG << endl; //cout << "H -> " << boolH << endl; //cout << "I -> " << boolI << endl;
// //cout << strInput << endl << endl;;
////cout << "A" << endl;


        if (boolI) {
         //failed blind transfer reconnect
         if(!FreeswitchData.fLoadBridgeDataFromFreeswitchSLA(strInput))                  {return false;}
         FreeswitchData.enumFreeswitchEvent = FREESWITCH_TRANSFER_RINGBACK_CONNECT;         
         return true;
        }

        if (boolH){
   //      //cout << "Non Valet Parked, Park Pickup" << endl;
         if(!FreeswitchData.fLoadChannelDataFromNonValetParkPickup(strInput))                                                                                            {return false;}
   //      //cout << "valet A" << endl;
         if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_NUMBER_WO_COLON)),  intPortNum)){return false;}
    //     //cout << "valet B" << endl;
         FreeswitchData.enumFreeswitchEvent = FREESWITCH_NON_VALET_PICKUP;
         return true;
        }

        if (boolF){
        // //cout << "other leg destination is conf_join" << endl;
         //this is a Phone Barge that has occurred after a GUI Barge after all positions have left the call Franklin County GA issue.
         // the media bug will not precede this SLA barge !!!!!
         if(!ChannelStateFour(strInput))       {return false;}
         FreeswitchData.enumFreeswitchEvent = FREESWITCH_SLA_JOIN_CALL;
         if(FreeswitchData.fLoadBridgeDataFromFreeswitchSLA(strInput))                  {return true;}
        // //cout << "returning false" << endl;
         return false;
        }


        if (boolG) {
          // BLIND GUI TRANSFER        
          ////cout << "gui blind transfer connect" << endl;
          ////cout << strInput << endl;
          //there will be two state seven's we are looking for a position IP, it will be rejected in the ANI thread if not a position IP.
          // or we get a single state 4
          CallData.strFreeswitchConfName = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON);
          FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON);
          FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON ));
          FreeswitchData.objChannelData.strChannelID   = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON);

          if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON )))                   {return false;}
          CallData.strFreeswitchConfName = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON);
          FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON);
          CallData.TransferData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
          if (!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                     {return false;}
          //exclude freeswitch domain IP
          if ( CallData.TransferData.IPaddress.stringAddress == strASTERISK_AMI_IP_ADDRESS)                                                               {return false;}
 //         //cout << ChannelStateNumber(strInput) << endl;
          switch (ChannelStateNumber(strInput))
           {
            case 7: 
             if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON)),  intPortNum)){return false;}
             strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
             if(!strData.empty()) {strData = strData.erase(0,1);}
             if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);} 
             break;

            case 4:
             FreeswitchData.objChannelData.IPaddress = CallData.TransferData.IPaddress;
             if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON)),  intPortNum)){return false;}
             FreeswitchData.objChannelData.strTranData = CallData.TransferData.strTransfertoNumber;
             break;
 
            default:
             return false;
           }// switch (ChannelStateNumber(strInput)) 

          FreeswitchData.enumFreeswitchEvent = FREESWITCH_CHANNEL_ANSWER_UPDATE_GUI_BLIND_TRANSFER; 
          return true;


        } // if (boolG) 
         
 
       // Internal NG911 conference transfer
       if ((boolA)&&(!boolB)&&(boolD)) {
         
          if(!ChannelStateSeven(strInput))       {return false;}
         // //cout << "Internal NG911 transfer" << endl;
          CallData.strFreeswitchConfName = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON);
          FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON);
          FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON ));
          FreeswitchData.objChannelData.strChannelID   = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON);

          if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_TRANSFER_FROM_POSITION_WO_COLON )))                         {return false;}
 //         //cout << strInput << endl;
          strOne = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_PSAP_URI_WO_COLON));
          if(!CallData.TransferData.fLoad_Transfer_Number(XML_DATA_FIELD, ANI, strOne,  intPortNum))                                                                     {return false;}

          strTwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_DESTINATION_URL_WO_COLON));
          //if the dest url == transfer PSAP this is ESRP data we want the phone sip data.
          if (strOne == strTwo)                                                                                                                                          {return false;}
          CallData.TransferData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
          if (!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                                    {return false;}
          strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
          if(!strData.empty()) {strData = strData.erase(0,1);}
          if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);}
 
          FreeswitchData.enumFreeswitchEvent = FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER;
          return true;
       } //if ((boolA)&&(!boolB)&&(boolD))
    

        //Conference Transfer
        if ((boolA)&&(!boolB)&&(!boolC)) {
    
         // //cout << "Conference Transfer Answer" << endl;
            if (!FreeswitchData.fLoadChannelFromFreeswitchConferenceSLA(strInput))                                                                                              {break;}
            //exclude domain IP channel

            if ( FreeswitchData.objChannelData.IPaddress.stringAddress == strASTERISK_AMI_IP_ADDRESS)                                                                           {return false;}

            //Check for transfer Number PSAP or Regular
            if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_PSAP_URI_WO_COLON )), intPortNum)){ 
               //RDNIS does not always show up ...
               if (!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_TO_USER_WO_COLON), intPortNum)){
                if (!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_RDNIS_WO_COLON), intPortNum)){
                 return false;
                }               
               } 
            }

            if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_FROM_POSITION_WO_COLON)))                                {return false;}

            CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
            if(CallData.TransferData.strTransferTargetUUID.empty()) { 
            SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data(): case FREESWITCH_CHANNEL_ANSWER (A!B!C) -> TRANSFER_TARGET_UUID missing");
            return false;
            }
            CallData.TransferData.fLoad_RDNIS(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_RDNIS_WO_COLON));
            FreeswitchData.enumFreeswitchEvent = FREESWITCH_ANSWER_CONFERENCE_TRANSFER;
            return true;
        }//if ((boolA)&&(!boolB)&&(!boolC))

        // MSRP Connect (needs to be called before Default)
        if (CallFromMSRPContext(strInput))
         {
   //       //cout << "from msrp context" << endl;

          boolE = FreeswitchData.fLoadBridgeDataFromFreeswitchSLA(strInput);
          if (!boolE) {boolE = FreeswitchData.fLoadBridgeDataFromFreeswitchLineRoll(strInput);}
          if (!boolE) {return false;}
         
          //Position Data is derived from Presence Data this will not be correct
 //         FreeswitchData.objLinkData.fDisplay();
 //         //cout << strInput << endl;
          FreeswitchData.objChannelData.strPresenceID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_PRESENCE_ID_WO_COLON));
          iPosition = PositionNumberFromMSRPpresenceID(FreeswitchData.objChannelData.strPresenceID);   
          FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);
          FreeswitchData.objChannelData.boolMSRP= true;
 //         FreeswitchData.objChannelData.fDisplay();
          FreeswitchData.enumFreeswitchEvent = FREESWITCH_MSRP_TEXT_MSG_CONNECT;
          return true;
         }
////cout << "b" << endl;
        if (CallFromDefaultContext(strInput))
         {
////cout << "C" << endl;
          if(FreeswitchData.fLoadBridgeDataFromFreeswitchSLA(strInput))                  {return true;}

          if(FreeswitchData.fLoadBridgeDataFromFreeswitchLineRoll(strInput))             {return true;}

         }
        else if (CallFromTransferContext(strInput))
         {
////cout << "D" << endl;
          if (FreeswitchData.fLoadChannelFromFreeswitchTransferSLA(strInput))
           {
////cout << "E" << endl;
            if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON )),  intPortNum)) {return false;}

            if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_FROM_POSITION_WO_COLON)))                             {return false;}

            FreeswitchData.enumFreeswitchEvent = FREESWITCH_CALL_UPDATE;
             
            return true;
           }
          else if (FreeswitchData.fLoadLinkDataFromFreeswitchTransferLineRoll(strInput))
           {
////cout << "F" << endl;
            if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON )),  intPortNum)) {return false;}

            return true;
           }
         } 
        else if (CallFromTransferContext(strInput, false))
         {
////cout << "G" << endl;
          //Here we have a poor man blind transfer connect to an Audiocodes call.
          if (FreeswitchData.fLoadChannelFromFreeswitchTransferSLA(strInput))
           {
////cout << "H" << endl;
            if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON )),  intPortNum)) {return false;}

            FreeswitchData.enumFreeswitchEvent = FREESWITCH_CHANNEL_ANSWER_UPDATE_UNATTENDED_BLIND_TRANSFER;
            return true;
           }
          else if (FreeswitchData.fLoadLinkDataFromFreeswitchTransferLineRoll(strInput))
           {
////cout << "I" << endl;
            if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON )),  intPortNum)) {return false;}
            FreeswitchData.enumFreeswitchEvent = FREESWITCH_CHANNEL_ANSWER_UPDATE_UNATTENDED_BLIND_TRANSFER;
            return true;
           }
         }
////cout << "returned false ...." << endl;
        return false;
   
   case FREESWITCH_CHANNEL_ORIGINATE:
        //We are looking for channel Data to add to a GUI transfer
        // can also be a shoretel outbound call ....       
        // //cout << "FREESWITCH_CHANNEL_ORIGINATE" << endl;
         ////cout << "call from default    context ->" << CallFromDefaultContext(strInput) << endl;
         ////cout << "call from transfer   context ->" << CallFromTransferContext(strInput) << endl;
         ////cout << "call from blind xfer context ->" << CallFromBlindTransferContext(strInput) << endl; 
         ////cout << "call from FreeSwitch xfer    ->" << CallFromFreeswitchTransfer(strInput) << endl;
        boolA = CallFromDefaultContext(strInput);
        boolB = CallFromTransferContext(strInput);
        boolC = CallFromBlindTransferContext(strInput);
        boolD = CallFromFreeswitchTransfer(strInput); 
        if ((!(boolB||boolC||boolD)) && (boolA)) { iswitch = 1;}
        
        ////cout << "iswitch -> " << iswitch << endl;
        switch  (iswitch)                      
         {
          case 0:
          ////cout << "add transfer data" << endl;
          // add transfer data ....
          if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON )),  intPortNum)) {return false;}
          ////cout << "A" << endl;
          if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_FROM_POSITION_WO_COLON)))                             {return false;}
          ////cout << "B" << endl;
          CallData.TransferData.strTransferTargetUUID = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_TARGET_UUID_WO_COLON);
          if (CallData.TransferData.strTransferTargetUUID.empty())                                                                                                         {return false;}
          ////cout << "C" << endl;
 //        no longer use ... need to track all       
 //        if (!CallData.fLoadCallUniqueID(ParseFreeswitchData(strInput,  FREESWITCH_VARIABLE_CONTROLLER_UNIQUE_ID_WO_COLON)))                                              {return false;}

          CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
          if(CallData.TransferData.strTransferFromChannel.empty())                                                                                                         {return false;}
          
          ////cout << "D" << endl;
          CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
          if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                                        {return false;}
          ////cout << "E" << endl;

          return true;

         case 1:
          ////cout << "shoretel originate" << endl;
          //shoretel outbound call ..... from default context
          if (!CallerDirectionOutbound(strInput))									{return false;} 
          if(!FreeswitchData.fLoadChannelDataFromOutboundDial(strInput))                                            	{return false;}
          strData = int2str(objConferenceList.AssignConferenceNumber());
          if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                               			{return false;}
          if (!fLoad_Trunk(SOFTWARE, ANI, "", FreeswitchData.objChannelData.iTrunkNumber,intPortNum)) 			{return false;} 
          CallData.fLoadCallBackDisplay(FreeswitchData.objChannelData.strCallerID);
          CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
          CallData.fLoadPosn(FreeswitchData.objChannelData.iPositionNumber);
          fSetUniqueCallID(CallData.intConferenceNumber); 

          FreeswitchData.objChannelData.strTranData=URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_RDNIS_WO_COLON));
                    
          CallData.strDialedNumber = CallData.TransferData.strTransfertoNumber = FreeswitchData.objChannelData.strTranData;
          FreeswitchData.objChannelData.boolIsOutbound = true;
          //FreeswitchData.objChannelData.fDisplay(0,true);
          FreeswitchData.enumFreeswitchEvent = NOTFOUND; //reset so it passes that section.
          enumANIFunction =  DIAL_DEST;
          return true;         


         default:
                 //error
                 return false;
        }

   case FREESWITCH_CALL_UPDATE:
         FreeswitchData.fClear();
       //  //cout << "FREESWITCH_CALL_UPDATE" << endl;

         if((CallFromTransferContext(strInput))&&(Freeswitch_Current_Application(strInput)!= "sofia_sla")) {return false;}
       //  //cout << "1.01" << endl;
        // //cout << strInput << endl;
         if(!ChannelStateFour(strInput))       {return false;}
////cout << "1" << endl;
        FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
        if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}

        FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
        if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
////cout << "2" << endl;

        FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON);
        if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
////cout << "3" << endl;

        strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);             
        FreeswitchData.objChannelData.boolIsOutbound = (strData == "outbound");

        FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EFF_CALLER_ID_NUM_WO_COLON)); 
        FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));

        if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON)));}
        else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(FreeswitchData.objChannelData.IPaddress.stringAddress); } 
        FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);


        if (iPosition) 
         {
          index = TelephoneEquipment.fIndexWithPositionNumber(iPosition);
          if (TelephoneEquipment.Devices[index].intPositionNumber)
           {
            FreeswitchData.objChannelData.fLoadLineNumberFromChannel(FreeswitchData.objChannelData.strChannelName , iPosition);
            FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.Devices[index].strCallerIDnumber;
            FreeswitchData.objChannelData.strCustName = TelephoneEquipment.Devices[index].strCallerIDname;
            FreeswitchData.objChannelData.fLoadConferenceDisplayP(FreeswitchData.objChannelData.iPositionNumber); 
           }

         }

        // determine if a Position Barge In or a GUI Conference Transfer or a Conference Barge In or Blind GUI Transfer
        FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_BARGING_UUID_WO_COLON);

        iswitch = 0;

        if      (CallFromThreeWayContext(strInput))                                                                      {iswitch = 1;} 
        else if (!(ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON )).empty())     {iswitch = 2;}
        else if (ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_TRANSFER_METHOD_WO_COLON )== "BLIND_TRANSFER")   {iswitch = 3;}

        FreeswitchData.enumFreeswitchEvent = FREESWITCH_CALL_UPDATE;

////cout << "iswitch-> " << iswitch << endl;
        switch (iswitch)
         {
          case 1:
                 // Conference Barge Eavesdrop
                 FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_THREEWAY_TARGET_UUID_WO_COLON);
           //      //cout << "confernce barge eavesdrop" << endl;
                 break;
          case 2:
             //    //cout << "case 2" << endl;
                 // GUI Conference Transfer
                 FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON);
            //     //cout << "tran data =" << FreeswitchData.objChannelData.strTranData << endl;
                 if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON )))                               {return false;}
            //     //cout << "transfer from position = " << CallData.TransferData.strTransferFromPosition << endl;
                 if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
            //     //cout << "transfer number = " << CallData.TransferData.strTransfertoNumber << endl;

                 FreeswitchData.enumFreeswitchEvent = FREESWITCH_CALL_UPDATE;
                 break;
          case 3:
                 // GUI Blind Transfer
                 // not used here any more .......
                 return false;
        //         //cout << "Parsed GUI Blind transfer" << endl;
                 FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON);
         //        //cout << "tran data =" << FreeswitchData.objChannelData.strTranData << endl;
                 if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON )))                                  {return false;}
         //        //cout << "transfer from position = " << CallData.TransferData.strTransferFromPosition << endl;
                 if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON)),  intPortNum)){return false;}
          //       //cout << "transfer number = " << CallData.TransferData.strTransfertoNumber << endl;
                 CallData.TransferData.IPaddress.stringAddress = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON));
                 if (!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                                    {return false;}
         //        //cout << "IP Address = " << CallData.TransferData.IPaddress.stringAddress << endl; 
                 FreeswitchData.enumFreeswitchEvent = FREESWITCH_CALL_UPDATE;
                 break;
          default:
     //            //cout << "Position Barge Position:" << iPosition << " IP address ->" << FreeswitchData.objChannelData.IPaddress.stringAddress << endl;
                 
                 // Position Barge Eavesdrop
                 if (!iPosition) {return false;}
                 break;
         }

 //       FreeswitchData.objChannelData.fDisplay(0,true);
        return true;

   case FREESWITCH_CUSTOM:
        switch(DetermineCustomEvent(strInput))  {
          case FREESWITCH_MEM_USAGE:
               MemoryData.fLoadFreeswitchMemoryData(strInput);
               return false; 
          case FREESWITCH_REGISTER:
               enumANIFunction = HEARTBEAT;           
               TelephoneEquipment.fRegisterDevice(strInput, intPortNum); 
               return true; 
          case FREESWITCH_VALET_PARK_MSG:
               ////cout << "valet park Message" << endl;
               FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
               if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
               FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));
               if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
               FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
               if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
               FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DESTINATION_NUMBER_WO_COLON);
               if (FreeswitchData.objChannelData.strTranData.empty())                                                               {return false;}
               strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);
               FreeswitchData.objChannelData.boolIsOutbound = (strData == "outbound");
               CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
               FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
               FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));
               if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON)));}
               else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(FreeswitchData.objChannelData.IPaddress.stringAddress); }
               FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON);
               FreeswitchData.objChannelData.strValetExtension = ParseFreeswitchData(strInput, FREESWITCH_CLI_VALET_EXTENSION_WO_COLON);
               strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_ACTION_WO_COLON);
               ////cout << "valet action: " << strData << endl;
               if      (strData == "hold")   {FreeswitchData.enumFreeswitchEvent = FREESWITCH_VALET_PARK_MSG; return true;}
               else if (strData == "exit")   {FreeswitchData.enumFreeswitchEvent = FREESWITCH_VALET_EXIT_MSG; return true;} 
               else if (strData == "bridge") {return false;} 
               else                          {return false;}              


          case FREESWITCH_MSRP_TEXT_MSG_RING:
               //cout << "Got custom ringing text message... check Discarded Message " << endl;

//        //cout << strInput << endl;
        
        FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
        if (!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                  {return false;}
////cout << "a" << endl;
        FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON )));
        FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON ));
        strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
        if(strData.empty()) {
         strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRUNK_WO_COLON);
        }
        if(!strData.empty()) {
          strData = strData.erase(0,1);
        }
        if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);}
        TextData.objLocationURI.fLoadLocationURI(strInput);
        ALIData.I3Data.objLocationURI = TextData.objLocationURI;
        // if To or from are missing we have the first message discarded error so if they load correctly exit..... another section will process the call
        TextData.SMSdata.strSMSmessage.clear();
        TextData.SMSdata.fLoadSMSmessage(strInput);
        if (!TextData.SMSdata.strSMSmessage.empty()) { return false;}
        //cout << "message is empty! " << TextData.SMSdata.strSMSmessage << endl;
        
        if ((TextData.SMSdata.fLoadTo(strInput)) || (TextData.SMSdata.fLoadFrom(strInput)))                                    {return false;}
 //       TextData.SMSdata.strSMSmessage = "911 caller 1st Text Message Discarded ERROR ...";

        TextData.SMSdata.strFromHost = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_FROM_HOST_WO_COLON);
        TextData.SMSdata.strToHost   = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_TO_HOST_WO_COLON);
        TextData.SMSdata.strToUser   = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_TO_USER_WO_COLON);
        TextData.SMSdata.strFromUser = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_FROM_USER_WO_COLON);

        TextData.TextType = MSRP_MESSAGE;       
        FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON ));
        FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
        FreeswitchData.objChannelData.strPresenceID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_PRESENCE_ID_WO_COLON));
    //    iPosition = PositionNumberFromMSRPpresenceID(FreeswitchData.objChannelData.strPresenceID);   
    //    FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);
        FreeswitchData.objChannelData.boolMSRP= true;
        FreeswitchData.enumFreeswitchEvent = FREESWITCH_MSRP_TEXT_MESSAGE;
//        //cout << "MSRP LUA After" << endl;
        ALIData.I3Data.fLoadGeoLocationURI(strInput);                        
        return true;

/*
               FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
               if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
               FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));
               if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
               FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
               if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
               strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);
               FreeswitchData.objChannelData.boolIsOutbound = (strData == "outbound");
               FreeswitchData.objChannelData.strCallerID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON); 
               FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));
               iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON)));
               FreeswitchData.objChannelData.boolMSRP= true;
               enumfree
               //cout << "Ringing after" << endl;
               return false; 
*/
        //   //cout << "Ringing after" << endl;
           return false;

          case FREESWITCH_CONFERENCE:
         // //cout << "freeswitch_conference" << endl;
  //  GDIT patch removed     if (bool_BCF_ENCODED_IP)                                                                                             {return false;}
        //       //cout << strInput << endl;
               FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);

               if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
               //exclude domain IP channel
               if ( FreeswitchData.objChannelData.IPaddress.stringAddress == strASTERISK_AMI_IP_ADDRESS)                            {return false;}

               FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
               if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}

               FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_CALL_UUID_WO_COLON);
               if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}

               CallData.strFreeswitchConfName = ParseFreeswitchData(strInput, FREESWITCH_CONFERENCE_NAME_WO_COLON);
               if (CallData.strFreeswitchConfName.empty())                                                                          {return false;}
               
               CallData.strFreeswitchConfID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
               if (CallData.strFreeswitchConfID.empty())                                                                            {return false;}
               
               strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);
               FreeswitchData.objChannelData.boolIsOutbound = (strData == "outbound");
              
               if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON)));}
               else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(FreeswitchData.objChannelData.IPaddress.stringAddress); }
             //  //cout <<  iPosition << " " << FreeswitchData.objChannelData.IPaddress.stringAddress << endl;
               FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);


               FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
               FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));
               if      (FreeswitchData.objChannelData.iPositionNumber) 
                {
                 FreeswitchData.objChannelData.fLoadConferenceDisplayP(FreeswitchData.objChannelData.iPositionNumber);
                 FreeswitchData.objChannelData.iLineNumber = TelephoneEquipment.fFindTelephoneLineNumber(FreeswitchUsernameFromChannel(FreeswitchData.objChannelData.strChannelName));
                }
               
               switch(DetermineConferenceEvent(strInput))      
                {
                 case CONFERENCE_JOIN: 
                      if (DestinationNumberisVALET_JOIN(strInput)){enumANIFunction = VALET_JOIN;}
                      else                                        {enumANIFunction = CONFERENCE_JOIN;}
                      return true;
                 case CONFERENCE_LEAVE:
                      enumANIFunction = CONFERENCE_LEAVE; return true;       
                 default: return false;
                }
                
               return false;

          case FREESWITCH_TDD_CHAR:
               FreeswitchData.objChannelData.fClear();
               FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON );
               if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
     
               FreeswitchData.objChannelData.strTranData = TDD_Decode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TDD_DATA_WO_COLON ));
               if (FreeswitchData.objChannelData.strTranData.empty())                                                              {return false;}

               enumANIFunction = TDD_CHAR_RECEIVED;       
               return true;

          case FREESWITCH_SOFIA_ERROR:
               
               strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_ERROR_TYPE_WO_COLON);

               if (strData != FREESWITCH_CLI_VALUE_BLIND_TRANSFER) {return false;}

               // Disable this section of code v2.7.59 we do not blind transfer from a conference anymore
               return false;

               if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_EXTENSION_WO_COLON)),  intPortNum)){return false;}
               CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));                           
               if(CallData.TransferData.strTransferFromChannel.empty())                                                                                                           {return false;}
                                         
               CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_CALL_UUID_WO_COLON));
               if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                                          {return false;} 
               CallData.TransferData.IPaddress.stringAddress =  ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);                   
               if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                                         {return false;}
 
               if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON)));}
               else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
               CallData.fLoadPosn(iPosition);
               if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                                    {return false;}

               CallData.TransferData.eTransferMethod = mBLIND_TRANSFER;
            
               enumANIFunction = RETRY_CONF_BLIND_TRANSFER;               
               return true;
     
          case FREESWITCH_MYEVENT_MESSAGE:
              // //cout << "Myevent" << endl;
               switch(DetermineMyEvent(strInput))
                {
/*
                 case RINGING:
                      if(!FreeswitchData.fLoadChannelDataFromOutboundDial(strInput))                                {return false;}
                      FreeswitchData.objChannelData.boolIsOutbound = false;
                      strData = int2str(objConferenceList.AssignConferenceNumber());
                      if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                               {return false;}
                      if (!fLoad_Trunk(SOFTWARE, ANI, "", FreeswitchData.objChannelData.iTrunkNumber,intPortNum)) {return false;} 
                      if(!fLoad_PhoneNumbers(ASTERSIK_AMI_LIST, ANI, intPortNum, charString))	                  {return false;}
                      CallData.fLoadCallBackDisplay(FreeswitchData.objChannelData.strCallerID);
                      CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
                      fSetUniqueCallID(CallData.intConferenceNumber);                                                
                      enumANIFunction = RINGING;
                      return true;

                      //cout << "FREESWITCH_MYEVENT_MESSAGE .. return false" << endl;
                      return false;
*/
                 case TRANSFER_FAIL:

                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON )),  intPortNum)) {return false;}
                      if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON )))                                    {return false;}
                      CallData.TransferData.fLoadHangupCause( ParseFreeswitchData(strInput, FREESWITCH_HANGUP_CAUSE_WO_COLON));
                   //   CallData.strFreeswitchConfName = ParseFreeswitchData(strInput, FREESWITCH_CONFERENCE_NAME_WO_COLON);
                   //   if (CallData.strFreeswitchConfName.empty())                                                                                                                      {return false;}
                      if (!CallData.fLoadCallUniqueID(ParseFreeswitchData(strInput, FREESWITCH_CONTROLLER_UNIQUE_ID_WO_COLON)))                                                        {return false;}
                      enumANIFunction =  TRANSFER_FAIL;
                      return true;

                 case ATTENDED_TRANSFER: 

                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                           {return false;}
                                     
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                          {return false;}
                      CallData.TransferData.strTransferMatchingChannelId = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferMatchingChannelId.empty())                                                                                     {return false;}
                      CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferTargetUUID.empty()) { 
                       SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data(): case ATTENDED_TRANSFER -> TRANSFER_TARGET_UUID missing");
                       return false;
                      }
                      
                  
                      CallData.TransferData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON);
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                         {return false;}

                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                    {return false;}

                      CallData.TransferData.eTransferMethod = mATTENDED_TRANSFER;
                      enumANIFunction =  ATTENDED_TRANSFER;

                      //CallData.TransferData.fDisplay(0);
                      return true;

                 case ATT_TRANSFER_FAIL:

                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                           {return false;}
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                          {return false;}
                      CallData.TransferData.strTransferMatchingChannelId = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferMatchingChannelId.empty())                                                                                     {return false;}
                      CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferTargetUUID.empty()) { 
                       SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data(): case ATT_TRANSFER_FAIL -> TRANSFER_TARGET_UUID missing");
                       return false;
                      }
                      CallData.TransferData.strHangupCause = ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_ORIGINATE_DISPOSITION_WO_COLON);                 
                      CallData.TransferData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON);
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                         {return false;}
                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                    {return false;}

                      CallData.TransferData.eTransferMethod = mATTENDED_TRANSFER;
                      enumANIFunction =  ATT_TRANSFER_FAIL;

                     // CallData.TransferData.fDisplay(0);
                      return true;
                 case ATT_TRANSFER_REJ:

                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                           {return false;}
                      CallData.strFreeswitchConfName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_VARIABLE_CONFERENCE_UUID_WO_COLON)); 
                      if (CallData.strFreeswitchConfName.empty())                                         								 {return false;} 
                      CallData.TransferData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON);
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                         {return false;}
                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                    {return false;}
                      CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferTargetUUID.empty()) { 
                       SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data(): case ATT_TRANSFER_REJ -> TRANSFER_TARGET_UUID missing");
                       return false;
                      }
                      CallData.TransferData.eTransferMethod = mATTENDED_TRANSFER;
                      enumANIFunction =  ATT_TRANSFER_REJ;

                      return true;
                 case CONFERENCE_ATT_TRANSFER_FAIL:
                   //   //cout << "conf att xfer fail" << endl;
                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                           {return false;}                                     
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                          {return false;}
                      CallData.TransferData.strTransferMatchingChannelId = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CONFERENCE_NAME_WO_COLON));
                      if(CallData.TransferData.strTransferMatchingChannelId.empty())                                                                                     {return false;}
                      CallData.TransferData.strHangupCause = ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_ORIGINATE_DISPOSITION_WO_COLON);                 
                      CallData.TransferData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON);
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                         {return false;}
                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                    {return false;}

                      CallData.TransferData.eTransferMethod = mATTENDED_TRANSFER;
                      enumANIFunction =  CONFERENCE_ATT_TRANSFER_FAIL;
                      return true;

                 case BLIND_TRANSFER:
                      ////cout << "blind transfer my event in dataclass" << endl;
                      ////cout << strInput << endl;
                      // Blind transfer comes from a position we need to find the position either from the SDP or the Bridge channel
                      //apparently it can vary.
                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}

                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                           {return false;}

                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                          {return false;}

                      CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferTargetUUID.empty()) { 
                       SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data(): case BLIND_TRANSFER -> TRANSFER_TARGET_UUID missing");
                       return false;
                      }
                      CallData.TransferData.strTransferMatchingChannelId = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_LAST_BRIDGE_TO_WO_COLON));
                      if(CallData.TransferData.strTransferMatchingChannelId.empty())                                                                                     {return false;}

                      CallData.TransferData.eTransferMethod = mBLIND_TRANSFER;
                      enumANIFunction =  BLIND_TRANSFER;
                      return true;                  

// Dead Code vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

/*

                      //We need to determine which channel is a position channel.  it varies based on whether the initial call was inbound, outbound or position to position call
                      //We also Know that there were only two channels on the call previously.  We throw an error for blind transfers out of a conference.  We need to be able
                      //to calculate who was the last channel.  We can find the call through the TransferTarget for connect.  
                      //cout << "BRIDGE_CHANNEL  -> " << IPaddressFromChannelName( URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_BRIDGE_CHANNEL_WO_COLON))) << endl;
                      //cout << "SWITCH-M-SDP    -> " << Ip4addressFromSDP(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SWITCH_M_SDP_WO_COLON))) << endl;
                      //cout << "BoolBCF         -> " << bool_BCF_ENCODED_IP << endl;
                      //cout << "Transfer From   -> " << CallData.TransferData.strTransferFromChannel << endl;
                      //cout << "Transfer From   -> " << CallData.TransferData.strTransferFromUniqueid << endl;
                      //cout << "Transfer Number -> " << CallData.TransferData.strTransfertoNumber << endl;
                      //cout << "Transfer source -> " << URLdecode(ParseFreeswitchData(strInput, "Caller-Transfer-Source")) << endl;
                      //cout << "Last Bridge to  -> " << URLdecode(ParseFreeswitchData(strInput, "variable_last_bridge_to")) << endl;
                      // one or both of these will be associated with a position. if none then Possibly the BCF.  Else Error !
                      // they may also both be from the same position.
                      iswitch = 0;
                      iPosition = 0;
                      integerPosition = 0;
                      // find position number from the last_bridge_to UUID match to position in conference vector
                      strTemp = URLdecode(ParseFreeswitchData(strInput, "variable_last_bridge_to"));
                     


                      // added sip_h_X-Bridge_Channel header to see if we can ensure that it gets added to the event message ........(can be removed ... sleep 500 in dialplan fixed it.)
          //            CallData.TransferData.IPaddress.stringAddress = IPaddressFromChannelName( URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_H_X_BRIDGE_CHANNEL_WO_COLON)));
          //            if (CallData.TransferData.IPaddress.fIsValid_IP_Address()) {
          //             iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress);
          //             integerPosition = iPosition; 
          //            }
          //            else {

                       // check for Bridge channel if lua script in place or dialplan var set it will be caught second.
                       CallData.TransferData.IPaddress.stringAddress = IPaddressFromChannelName( URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_BRIDGE_CHANNEL_WO_COLON)));
                       if (CallData.TransferData.IPaddress.fIsValid_IP_Address()) {
                        iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress);
                        integerPosition = iPosition; 
                       }
                       else {
                        CallData.TransferData.IPaddress.stringAddress = IPaddressFromChannelName( URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_POSITION_BLIND_FROM_CH_WO_COLON)));
                        if (CallData.TransferData.IPaddress.fIsValid_IP_Address()) {
                         iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress);
                         integerPosition = iPosition; 
                        }
                       }
                      if (iPosition > 0) {boolA = true; }
                      iPosition = 0;
                      CallData.TransferData.IPaddress.stringAddress = Ip4addressFromSDP(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SWITCH_M_SDP_WO_COLON)));                       
                      if (CallData.TransferData.IPaddress.fIsValid_IP_Address()) {iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress);}
                      if (iPosition > 0) {boolB = true; }
                      
                      // Do not modify / insert any code above the last 3 lines !
                      if      (bool_BCF_ENCODED_IP) { iswitch = 1;}
                      else if (boolA && boolB)      { iswitch = 2;}
                      else if (boolA)               { iswitch = 3;}
                      else if (boolB)               { iswitch = 4;}
                      else                          { iswitch = 0;}
                      //cout << iswitch << endl;
                      if (boolA) {//cout << "Bridge channel position -> " << integerPosition << endl;}
                      switch (iswitch)
                       {
                        case 1: 
                               iPosition = TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchUsernameFromReferedBy
                                      (URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_REFERRED_BY_WO_COLON))));
                               if (iPosition) {break;}
                        case 2: case 4:
                               //Go with the SDP it will be the correct Position and is true (it is still loaded);
                //              //cout << "SDP" << endl;
                              //cout << "blind transfer from -> " << CallData.TransferData.IPaddress.stringAddress << endl;
                              //cout << "position is -> " << iPosition << endl;
                               break; 
                        case 3:
                               // We got the Bridge channel !  
                               iPosition = integerPosition;
                               break;
                        default:
                               SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data() Blind-Transfer Pos Undeterminable!");
                               ////cout << strInput << endl;
                               return false;
                       }
                      
                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn)) {return false;}
////cout <<"E"<<endl;
                      CallData.TransferData.eTransferMethod = mBLIND_TRANSFER;
                      enumANIFunction =  BLIND_TRANSFER;
                      return true;
*/
                 case CONFERENCE_ATT_XFER:

                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.IPaddress.stringAddress = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON));                   
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                                {return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                                  {return false;} 
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                                 {return false;}
                      CallData.TransferData.strTransferMatchingChannelId = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CONFERENCE_NAME_WO_COLON));
                      if(CallData.TransferData.strTransferMatchingChannelId.empty())                                                                                            {return false;}
                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                    {return false;}
                      CallData.TransferData.eTransferMethod = mATTENDED_TRANSFER;
                      enumANIFunction =  CONFERENCE_ATT_XFER;
                      return true;

                 case RING_BACK:
                      ////cout << "RING BACK DATACLASS" << endl;
                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_TO_WO_COLON)),  intPortNum)){return false;}
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      if(CallData.TransferData.strTransferFromChannel.empty())                                                                                         {return false;}                       
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                          {return false;}               
                      CallData.TransferData.IPaddress.stringAddress = Ip4addressFromSDP(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SWITCH_M_SDP_WO_COLON)));                   
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                         {return false;}
                      CallData.TransferData.strHangupCause = ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_ORIGINATE_DISPOSITION_WO_COLON);
                      CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferTargetUUID.empty()) { 
                       SendCodingError( "ExperientDataClass.cpp - fLoad_FREESWITCH_Data(): case RING_BACK -> TRANSFER_TARGET_UUID missing");
                       return false;
                      }
                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NUM_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 

                      CallData.fLoadPosn(iPosition);
                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                    {return false;}
                      CallData.TransferData.eTransferMethod = mRING_BACK;
                      enumANIFunction =  RING_BACK;
                      return true;

                 case DIAL_DEST:
                     // //cout << "this dial dest" << endl;
/*
                      if(!FreeswitchData.fLoadChannelDataFromOutboundDial(strInput))                                {return false;}
                      strData = int2str(objConferenceList.AssignConferenceNumber());
                      if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                               {return false;}
                      if (!fLoad_Trunk(SOFTWARE, ANI, "", FreeswitchData.objChannelData.iTrunkNumber,intPortNum)) {return false;} 
                      CallData.fLoadCallBackDisplay(FreeswitchData.objChannelData.strCallerID);
                      CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
                      CallData.fLoadPosn(FreeswitchData.objChannelData.iPositionNumber);
                      fSetUniqueCallID(CallData.intConferenceNumber);                  
                      FreeswitchData.objChannelData.strTranData=URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_DIAL_TO_WO_COLON));                    
                      CallData.strDialedNumber = CallData.TransferData.strTransfertoNumber = FreeswitchData.objChannelData.strTranData;
                      FreeswitchData.objChannelData.boolIsOutbound = true;
             //         FreeswitchData.objChannelData.fDisplay(0,true);
                      enumANIFunction =  DIAL_DEST;
                      return true;
*/
                      return false;

                 case BARGE_ON_POSITION:

                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                                         {return false;} 
                     
                      CallData.TransferData.IPaddress.stringAddress = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_POSITION_IP_WO_COLON)); 

                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                                        {return false;}

                      if(!CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_POSITION_TO_BARGE_WO_COLON )),  intPortNum)){return false;}

                      if (!ValidateFieldAndRange(ANI, CallData.TransferData.strTransfertoNumber, strInput, 1, "BargeToPosition", 99, 1))                                                {return false;}
      
                      FreeswitchData.objChannelData.iGUIlineView = Determine_GUI_LINE_VIEW_Number(strInput);

                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_FROM_USER_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);
                      FreeswitchData.objChannelData.fLoadLineNumberFromChannel(CallData.TransferData.strTransferFromChannel, iPosition);
                      CallData.fSetSharedLineFromLineNumberPlusPosition(FreeswitchData.objChannelData.iLineNumber);

                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                                  {return false;}
 
                      if (CallData.CTIsharedLine) {enumANIFunction =  BARGE_ON_POSITION;}
                      else                            {enumANIFunction =  BARGE_ON_POSITION_LINE_ROLL;}
                      return true;
                      
                 case BARGE_ON_CONFERENCE:
                      //cout << "BARGE_ON_CONFERENCE" << endl;
                      CallData.TransferData.strTransferFromChannel = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));                           
                      CallData.TransferData.strTransferFromUniqueid = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
                      if(CallData.TransferData.strTransferFromUniqueid.empty())                                                                                                         {return false;} 
 
                      CallData.TransferData.IPaddress.stringAddress = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON)); 
                      if (bool_BCF_ENCODED_IP)
                       {
                        strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_PARAMS_WO_COLON));
                        strKey  = Determine_BCF_Key(strData);
                        CallData.TransferData.IPaddress.fParseIPaddressUsingValueKey(strKey, strData);
                       }
                      else
                       {
                        CallData.TransferData.IPaddress.stringAddress = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_NETWORK_IP_WO_COLON)); 
                       }

                  
                      if(!CallData.TransferData.IPaddress.fIsValid_IP_Address())                                                                                                        {return false;}
                      if(!CallData.fLoad_Conference_Number(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CONFERENCE_TO_BARGE_WO_COLON ))))                                     {return false;}

                      FreeswitchData.objChannelData.iGUIlineView = Determine_GUI_LINE_VIEW_Number(strInput);
               //       FreeswitchData.objChannelData.CTIsharedLine=TelephoneEquipment.fIndexofSharedLine(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_ANI_WO_COLON));



                      FreeswitchData.objChannelData.fLoadGUILineView(CallData.TransferData.strTransferFromChannel);


                      if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_FROM_USER_WO_COLON)));}
                      else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(CallData.TransferData.IPaddress.stringAddress); } 
                      CallData.fLoadPosn(iPosition);
                      
                      FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);
                      FreeswitchData.objChannelData.fLoadLineNumberFromChannel(CallData.TransferData.strTransferFromChannel, iPosition);
                      CallData.fSetSharedLineFromLineNumberPlusPosition(FreeswitchData.objChannelData.iLineNumber);


                      if (!CallData.TransferData.fLoadFromPosition(CallData.intPosn))                                                                                                  {return false;}
// //cout << "shared line -> " << CallData.CTIsharedLine << endl;
                      if (CallData.CTIsharedLine) {enumANIFunction =  BARGE_ON_CONFERENCE;}
                      else                        {enumANIFunction =  BARGE_ON_CONFERENCE_LINE_ROLL;}

                      return true;

                 case LOCATION_URI:
                    //cout << "location URI experientdataclass.cpp" << endl;
                    // //cout << URLdecode(strInput) << endl;
                      ALIData.I3Data.fClear();
                  //    //cout << "got location Data" << endl << URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LOCATION_DATA_WO_COLON)) << endl;
                      
                      ALIData.I3Data.fLoadNenaCallId(strInput);
                      ALIData.I3Data.fLoadNenaIncidentId(strInput);
                      ALIData.I3Data.fLoadCallInfoEmergencyCallData(strInput);
                      ALIData.I3Data.fLoadPidfloEntity(strInput);
                      //ALIData.I3Data.fLoadServiceList(strInput);

                      //cout << "NOW nena callid?      -> " << ALIData.I3Data.strNenaCallId << endl;
                      //cout << "NOW nena incident id? -> " << ALIData.I3Data.strNenaIncidentId << endl;
                      //cout << "NOW pidflo entity     -> " << ALIData.I3Data.strEntity << endl;
                      //cout << "NOW ECD Provider URL  -> " << ALIData.I3Data.strCallInfoProviderURL << endl;
                      //cout << "NOW ECD Subscriber URL-> " << ALIData.I3Data.strCallInfoSubscriberURL << endl;
                      //cout << "NOW ECD Service URL   -> " << ALIData.I3Data.strCallInfoServiceURL << endl;

                      boolA = ALIData.I3Data.fLoadLocationURI(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LOCATION_URI_WO_COLON)));
                       //cout << "Location URI -> " << URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LOCATION_URI_WO_COLON)) << endl;
                      if (!boolA){
                                            // //cout << URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_GEOLOCATION_WO_COLON)) << endl;
                       boolA = ALIData.I3Data.fLoadLocationURI(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_GEOLOCATION_WO_COLON)));
                      }
                       
                      //cout << "ALIData.I3Data.strBidId -> "<< ALIData.I3Data.strBidId << endl;                    
                      boolB = ALIData.I3Data.fLoadBidID(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LOCATION_BID_ID_WO_COLON)));
                      //cout << "location uri -> loaded Bid ID ->" << ALIData.I3Data.strBidId << endl;
                //      //cout << URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LOCATION_DATA_WO_COLON)) << endl;
                      boolC = ALIData.I3Data.fLoadLocationData(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LOCATION_DATA_WO_COLON)), &strError); 
                      boolD = ALIData.fLoadEncodedALItext(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LEGACY_ALI_DATA_B64_WO_COLON))); 
                      FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_CALL_UUID_WO_COLON );
                      
                      

                      cout << "boolA->" << boolA << " boolB->" << boolB << " boolC->" << boolC << "boolD->" << boolD << endl;
                      if (! ( ((boolA && boolB) || boolC) || boolD) )                                                                        {return false;cout << "Location Parse Failure" << endl;}

                      if (FreeswitchData.objChannelData.strChannelID.empty())                                                                {cout << "location URI no channel info " << endl; return false;}

                      if      (boolD) {enumANIFunction =  LEGACY_ALI_DATA;}
                      else if (boolC) {enumANIFunction =  LOCATION_DATA;}
                      else            { 
                                        enumANIFunction =  LOCATION_URI;
                                        strData = ParseFreeswitchData(strInput, ASTERISK_AMI_USEREVENT_TRUNK_HEADER);
                                        if (strData.length() == 3) {strData.erase(0,1);}                     
                                        if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);}
                                      }

                      
                      return true;

                 case IRR_FILE_DATA:

                      FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CONTROLLER_UNIQUE_ID_WO_COLON);
                      if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}

                      FreeswitchData.objChannelData.strCallRecording = ParseFreeswitchData(strInput, FREESWITCH_RECORDING_FILE_LOCATION_WO_COLON);
                      if (FreeswitchData.objChannelData.strCallRecording.empty())                                                          {return false;}
                      enumANIFunction = IRR_FILE_DATA;
                      // //cout << "got irr data file! " << FreeswitchData.objChannelData.strCallRecording << endl;
                      return true;

                 default:
                      return false;

               }
      
               return false;

          case NOTFOUND:
               break;
          default:
               break;
         }
        return false;

    case FREESWITCH_CHANNEL_PROGRESS:
       // //cout << "FREESWITCH_CHANNEL_PROGRESS" << endl;
        // This is where we detect ringing line on each phone
         ALIData.I3Data.fClear();

         if(CallFromTransferContext(strInput)) {
          if (!BLIND_TRANSFER_FAIL_RINGBACK(strInput)) {return false;} 
         }
         if(!ChannelStateSeven(strInput))      { return false;}

         //If we are transferring back into the same system disregard the ring unless a Blind xfer ringback!!!!!
         if ((!BLIND_TRANSFER_FAIL_RINGBACK(strInput)) && (IsControllerBlindTransfer(strInput))){
          return false;
         }

         if ((SIP_TRANSFER_FROM_POSTION(strInput))&&(NG911_TRANSFER_TYPE_FROM_CLI(strInput) != SIP_URI_EXTERNAL_EXPERIENT)) { return false;}

         if (NG911_TRANSFER_TYPE_FROM_CLI(strInput) == SIP_URI_INTERNAL) { return false;} //may come back to that one ...... tbd

         if (ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_NETWORK_ADDRESS_WO_COLON).length())
          {
           iswitch = TelephoneEquipment.fPositionNumberFromChannelName(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_CHANNEL_NAME_WO_COLON));
           boolLineRoll = true;
          }
         else
          {
           if (bool_BCF_ENCODED_IP) {iswitch = TelephoneEquipment.fPositionNumberFromChannelName(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));}
           else                     {iswitch = TelephoneEquipment.fPostionNumberFromIPaddress  (ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON));}
         // //cout << strInput << endl;
          }

         // check for Legacy ALI in headers
         strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_LEGACY_ALI_DATA_SIPX_B64_WO_COLON));
         if (strData.length()) {ALIData.boolALIRecordReceived = ALIData.fLoadEncodedALItext(strData); }     

         //check for Ringback from Outbound call that was Parked ....
         strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_CALLPARK_RINGBACK_WO_COLON);
         CallData.TransferData.boolIsRingBack = (strData == "true");

         //check for GeoLocation      
         ALIData.I3Data.fLoadGeoLocationURI(strInput);

         //check for NENA CAllId and IncidentId
         ALIData.I3Data.fLoadNenaCallId(strInput);
         ALIData.I3Data.fLoadNenaIncidentId(strInput);
         ALIData.I3Data.fLoadCallInfoEmergencyCallData(strInput);
         ALIData.I3Data.fLoadPidfloEntity(strInput);
        // ALIData.I3Data.fLoadServiceList(strInput);              
    
         switch (boolLineRoll)
          {
           case true:

                switch (iswitch)
                {

                 case 0:
                     // //cout << "Inbound Call Line Roll" << endl;

                      FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_CHANNEL_NAME_WO_COLON));
                      if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}

                      FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_UUID_WO_COLON);
                      if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}

                      FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_NETWORK_ADDRESS_WO_COLON);
                      if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}

                      FreeswitchData.objChannelData.boolIsOutbound = false;
                      
                      FreeswitchData.objChannelData.fLoadPositionNumber(TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchData.objChannelData.strChannelName));

                      FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
                      FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));

                      strData = Freeswitch_Channel_Progress_Determine_Trunk_Number(boolLineRoll, FreeswitchData, strInput);
                      if (!FreeswitchData.objChannelData.fLoadTrunkNumber(strData,strInput)) {return false;}

                      FreeswitchData.objChannelData.strCallRecording = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CALL_RECORDING_WO_COLON);

                      boolNG911SIPnonAudiocodes = (TRUNK_TYPE_MAP.Trunk[FreeswitchData.objChannelData.iTrunkNumber].TrunkType == NG911_SIP)&&
                                                  (!TelephoneEquipment.fIsAnAudiocodes(FreeswitchData.objChannelData.IPaddress));

                      FreeswitchData.objChannelData.intTrunkType=TRUNK_TYPE_MAP.Trunk[FreeswitchData.objChannelData.iTrunkNumber].TrunkType;
                      FreeswitchData.objChannelData.fLoadConferenceDisplayC(FreeswitchData.objChannelData.iTrunkNumber);

                      FreeswitchData.objChannelData.iRingingPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput,
                                                                                                                        FREESWITCH_CLI_VARIABLE_SIP_TO_USER_WO_COLON)));
                      FreeswitchData.objChannelData.iLineNumber = TelephoneEquipment.fFindTelephoneLineNumber(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON));

                      FreeswitchData.objChannelData.fLoadGUILineView(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON));

                      // CTIsharedLine gets set after connect and is only valid prior to sending to CTI (valid position number needed)
                      CallData.CTIsharedLine = 0;



                      if (!fLoad_Trunk(SOFTWARE, ANI, strData, FreeswitchData.objChannelData.iTrunkNumber,false, strInput)) {return false;}
                      strData = int2str(objConferenceList.AssignConferenceNumber());

                      if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                                         {return false;}

                      if(!fLoad_PhoneNumbers(ASTERSIK_AMI_LIST, ANI, intPortNum, charString,"","","","",boolLineRoll, boolNG911SIPnonAudiocodes))	{return false;}

                      CallData.fLoadCallBackDisplay(FreeswitchData.objChannelData.strCallerID);
                      CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
                      fSetUniqueCallID(CallData.intConferenceNumber); 
                      //FreeswitchData.objChannelData.fDisplay(0,true);

                      enumANIFunction = RINGING;
                      ////cout << "ringing channel progress line roll" << endl;
                      ALIData.I3Data.fLoadNenaCallId(strInput);
                      ALIData.I3Data.fLoadNenaIncidentId(strInput);
                      ALIData.I3Data.fLoadCallInfoEmergencyCallData(strInput);
                      ALIData.I3Data.fLoadPidfloEntity(strInput);
                      ////cout << strInput << endl;
                      return true;

                 default:
                     //  Outbound Call from Position to Position .. this could also be a ringback from Park
                    //  //cout << "Outbound Call from Position to Position" << endl;
                      FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_CHANNEL_NAME_WO_COLON));
                      if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}

                      FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_UUID_WO_COLON);
                      if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}

                      FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_NETWORK_ADDRESS_WO_COLON);
                      if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
 
                      FreeswitchData.objChannelData.iRingingPosition = TelephoneEquipment.fPositionNumberFromChannelName(URLdecode(ParseFreeswitchData(strInput,
                                                                                                                 	FREESWITCH_CLI_VARIABLE_SIP_TO_USER_WO_COLON)));                    
                      FreeswitchData.objChannelData.boolIsOutbound = true;

                                          // iswitch is position number ....
                      FreeswitchData.objChannelData.fLoadPositionNumber(iswitch);

                      FreeswitchData.objChannelData.fLoadLineNumberFromChannel();

                      FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
                      FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON)); 
                      FreeswitchData.objChannelData.strCallRecording = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CALL_RECORDING_WO_COLON);

                      strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
                      if(!strData.empty()) {strData = strData.erase(0,1);}

                      if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);}
                      FreeswitchData.objChannelData.intTrunkType=SIP_TRUNK;
                      strData = int2str(objConferenceList.AssignConferenceNumber());
                      if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                               {return false;}
                      FreeswitchData.objChannelData.fLoadConferenceDisplayP(FreeswitchData.objChannelData.iPositionNumber);

                      CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
                      CallData.fLoadPosn(FreeswitchData.objChannelData.iPositionNumber);

                      fSetUniqueCallID(CallData.intConferenceNumber);
                      FreeswitchData.objChannelData.fLoadGUILineView(FreeswitchData.objChannelData.strChannelName);

                      // CTIsharedLine gets set after connect and is only valid prior to sending to CTI (valid position number needed)
                      CallData.CTIsharedLine = 0;


                      FreeswitchData.objChannelData.strTranData=URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON));                    
                      CallData.strDialedNumber = CallData.TransferData.strTransfertoNumber = FreeswitchData.objChannelData.strTranData;
                      FreeswitchData.objChannelData.boolIsOutbound = true;
                      //FreeswitchData.objChannelData.fDisplay(0,true);

                      // If a ringback need to set ringing .....
                      if (CallData.TransferData.boolIsRingBack) {
                       enumANIFunction =  RINGING;
                      } 
                      else {
                       enumANIFunction =  DIAL_DEST;
                      }

                //      //cout << "ringing channel progress position ->" << FreeswitchData.objChannelData.iPositionNumber << endl;
                      
                      return true;
                }        


                 break;

           case false:

                switch (iswitch)
                {
                 case 0:
                 //     //cout << "ringing channel progress" << endl;
                 //     //cout << "Inbound non rolled/single channelname maybe shared Line Call here" << endl;
                 //     //cout << ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON) << endl;
                 //     //cout << URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON)) << endl;
                      // Inbound Call

                      FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                      if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}

                      FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_UUID_WO_COLON);
                      if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}

                      FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON);
                      if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}

                      FreeswitchData.objChannelData.boolIsOutbound = false;
                      FreeswitchData.objChannelData.fLoadPositionNumber(TelephoneEquipment.fPostionNumberFromIPaddress(FreeswitchData.objChannelData.IPaddress.stringAddress));

                      FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CALLER_ID_NUMBER_WO_COLON)); 
                      FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CALLER_ID_NAME_WO_COLON)); 

                      strData = Freeswitch_Channel_Progress_Determine_Trunk_Number(boolLineRoll, FreeswitchData, strInput);
                      if (!FreeswitchData.objChannelData.fLoadTrunkNumber(strData,strInput)) {return false;}

                      boolNG911SIPnonAudiocodes = (TRUNK_TYPE_MAP.Trunk[FreeswitchData.objChannelData.iTrunkNumber].TrunkType == NG911_SIP)
                                                   &&(!TelephoneEquipment.fIsAnAudiocodes(FreeswitchData.objChannelData.IPaddress));
         //             //cout << "ng911SIP non audiocodes =" <<  boolNG911SIPnonAudiocodes << endl;
                      
                      FreeswitchData.objChannelData.strCallRecording = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CALL_RECORDING_WO_COLON);
                      FreeswitchData.objChannelData.iRingingPosition = TelephoneEquipment.fPostionNumberFromIPaddress(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON)));
                      FreeswitchData.objChannelData.iLineNumber = TelephoneEquipment.fFindTelephoneLineNumber(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON));

                      FreeswitchData.objChannelData.intTrunkType=TRUNK_TYPE_MAP.Trunk[FreeswitchData.objChannelData.iTrunkNumber].TrunkType;
                      FreeswitchData.objChannelData.fLoadConferenceDisplayC(FreeswitchData.objChannelData.iTrunkNumber);

                      FreeswitchData.objChannelData.iGUIlineView=TelephoneEquipment.fIndexofLineView(ParseFreeswitchData(strInput,FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON));
                      // CTIsharedLine gets set after connect and is only valid prior to sending to CTI (valid position number needed)
                      CallData.CTIsharedLine = 0;


                      if (!fLoad_Trunk(SOFTWARE, ANI, strData, FreeswitchData.objChannelData.iTrunkNumber,false, strInput)) 			{return false;}
                      strData = int2str(objConferenceList.AssignConferenceNumber());
                      if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                                         			{return false;}  
                      if(!fLoad_PhoneNumbers(ASTERSIK_AMI_LIST, ANI, intPortNum, charString,"","","","",false, boolNG911SIPnonAudiocodes))	{return false;}
                      CallData.fLoadCallBackDisplay(FreeswitchData.objChannelData.strCallerID);
                      CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
                      fSetUniqueCallID(CallData.intConferenceNumber); 
                  
              //      FreeswitchData.objChannelData.fDisplay(0,true);                                               
                      enumANIFunction = RINGING;
                      ALIData.I3Data.fLoadNenaCallId(strInput);
                      ALIData.I3Data.fLoadNenaIncidentId(strInput);
                      ALIData.I3Data.fLoadCallInfoEmergencyCallData(strInput);
                      ALIData.I3Data.fLoadPidfloEntity(strInput);
                      return true;

                 default:
                      // Outbound Call
                      ////cout << "Outbound Call from Position to Position Non Line roll" << endl;
                      ////cout << strInput << endl;
                      FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
                      if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
                      FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_UUID_WO_COLON);
                      if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
                      FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON);
                      if(!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
                      FreeswitchData.objChannelData.boolIsOutbound = false; // ???

                      FreeswitchData.objChannelData.iRingingPosition = TelephoneEquipment.fPostionNumberFromIPaddress(URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON)));
 
                     // iswitch is position number ....
                      FreeswitchData.objChannelData.fLoadPositionNumber(iswitch);

                      FreeswitchData.objChannelData.fLoadLineNumberFromChannel();

                      FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
                      FreeswitchData.objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON)); 
                      FreeswitchData.objChannelData.strCallRecording = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CALL_RECORDING_WO_COLON);

                      strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
                      if(!strData.empty()) {strData = strData.erase(0,1);}

                      if(!FreeswitchData.objChannelData.fLoadTrunkNumber(strData, strInput)) {fLoad_Trunk(SOFTWARE, ANI, "00", 0,false, strInput);}
                      FreeswitchData.objChannelData.intTrunkType=SIP_TRUNK;
                      strData = int2str(objConferenceList.AssignConferenceNumber());
                      if(!fLoad_ConferenceData(SOFTWARE, ANI, strData, intPortNum))                               {return false;}
                      FreeswitchData.objChannelData.fLoadConferenceDisplayP(FreeswitchData.objChannelData.iPositionNumber);

                      CallData.fLoadCustName(FreeswitchData.objChannelData.strCustName);
                      CallData.fLoadPosn(FreeswitchData.objChannelData.iPositionNumber);

                      fSetUniqueCallID(CallData.intConferenceNumber);
                      FreeswitchData.objChannelData.iGUIlineView = Determine_GUI_LINE_VIEW_Number(strInput);
                      // CTIsharedLine gets set after connect and is only valid prior to sending to CTI (valid position number needed)
                      CallData.CTIsharedLine = 0;

                      FreeswitchData.objChannelData.strTranData=URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON));                    
                      CallData.strDialedNumber = CallData.TransferData.strTransfertoNumber = FreeswitchData.objChannelData.strTranData;
                      FreeswitchData.objChannelData.boolIsOutbound = true;
                     // FreeswitchData.objChannelData.fDisplay(0,true);
              //        //cout << "ringing channel progress shared position ->" << FreeswitchData.objChannelData.iPositionNumber << endl;
                       // if coming back fro call park
                      if (CallData.TransferData.boolIsRingBack) {
                       enumANIFunction =  RINGING;
                      } 
                      else {
                       enumANIFunction =  DIAL_DEST;
                      }
                      

                      return true;
                    
                }
               break;
          }// end switch (boolLineRoll)
        

    case FREESWITCH_PRESENCE_IN:
         switch(DeterminePresenceEvent(strInput)) 
         {
          // turn this code off once unhold/hold bug is fixed ... put in ini fix to to one or the other .....
          case HOLD:
               ////cout << "presence HOLD" << endl;
                if (ChannelStateNumber(strInput) == 2 ) {
                 cout << "presence HOLD ON channel state 2!" << endl;
                 return false;
                }
                FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
                if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
                FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
                if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}   
                enumANIFunction = HOLD_ON;
                return true;      
               
              // return false;
          case UNHOLD:
               ////cout << "presence UnHold" << endl;
                if (ChannelStateNumber(strInput) == 2 ) {
                 //cout << "presence HOLD OFF channel state 2!" << endl; 
                 return false;
                }
                FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
                if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
                FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
                if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}   
                enumANIFunction = HOLD_OFF;
                return true;  
               break;
              
             // return false;
          case FREESWITCH_PRESENCE_IDLE:

            //   strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_STATE_CALL_STATE_HANGUP);
            //   if (strData.empty()){return false;}
           //   //cout << "GOT THE IDLE /....................................................................." << endl;
               return false;

          case FREESWITCH_PRESENCE_PROGRESSING:
               return false;
   

          default:
              return false;

         }
        return false; 
  
    case HOLD:
         ////cout << "hold ON -> state num ->" <<  ChannelStateNumber(strInput) << endl;
         if (ChannelStateNumber(strInput) == 2 ) {
          cout << "HOLD ON channel state 2!" << endl; 
          return false;
         }
         FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
         if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
         FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
         if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}   
         enumANIFunction = HOLD_ON;
         return true;

    case UNHOLD:
         ////cout << "Unhold -> state num ->" <<  ChannelStateNumber(strInput) << endl;
         if (ChannelStateNumber(strInput) == 2 ) {
           cout << "HOLD OFF channel state 2!" << endl; 
           return false;
         }
         FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
         if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
         FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);
         if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}   
         enumANIFunction = HOLD_OFF;      
         return true;

   case HANGUP:
        // //cout << "Hangup in Data class" << endl;    
         FreeswitchData.objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
         if (FreeswitchData.objChannelData.strChannelName.empty())                                                            {return false;}
        // //cout << "a" << endl;
         FreeswitchData.objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_UUID_WO_COLON);            
         if (FreeswitchData.objChannelData.strChannelID.empty())                                                              {return false;}
        // //cout << "b" << endl;  
         FreeswitchData.objChannelData.strHangupCause = ParseFreeswitchData(strInput,  FREESWITCH_CLI_VARIABLE_HANGUP_CAUSE_WO_COLON);
         FreeswitchData.objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON);
         FreeswitchData.objChannelData.strEndpointDisposition = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_ENDPOINT_DISPOSITION_WO_COLON);
         FreeswitchData.objChannelData.strHangupDisposition   = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HANGUP_DISPOSITION_WO_COLON);
         FreeswitchData.objChannelData.strTransferDisposition = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_DISPOSITION_WO_COLON);

         FreeswitchData.objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
         if (!FreeswitchData.objChannelData.IPaddress.fIsValid_IP_Address()){
          if (FreeswitchData.objChannelData.strHangupCause != "NORMAL_CLEARING") {
           // Something went wrong with this hangup i.e. could not determine the IP ....... Have only seen this for NG911 transfers .....
           boolC = (NG911_TRANSFER_TYPE_FROM_CLI(strInput) == SIP_URI_EXTERNAL_EXPERIENT);
           boolD = (NG911_TRANSFER_TYPE_FROM_CLI(strInput) == SIP_URI_INTERNAL);
          // //cout << "boolC -> " << boolC << endl;
         //  //cout << "boolD -> " << boolD << endl;
           if (boolC||boolD){
          //  //cout << "HERE" << endl;
            
           // //cout << "parse -> " << ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON) << endl;
            CallData.TransferData.strTransferMatchingChannelId = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON);            
            if (CallData.TransferData.strTransferMatchingChannelId.empty())                                                               {return false;}           
            if(!CallData.TransferData.fLoadFromPosition(ParseFreeswitchData(strInput,  FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON ))) {return false;}
            CallData.TransferData.strHangupCause = FreeswitchData.objChannelData.strHangupCause;
            CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, FreeswitchData.objChannelData.strTranData, 1);
            CallData.TransferData.strTransferTargetUUID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON));
            if(CallData.TransferData.strTransferTargetUUID.empty()) { return false;}
            
            FreeswitchData.enumFreeswitchEvent = NOTFOUND; //reset so it passes that section.
            enumANIFunction = TRANSFER_FAIL;
            return true;
           }
          }
          return false;
         }
         if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchData.objChannelData.strChannelName);}
         else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(FreeswitchData.objChannelData.IPaddress.stringAddress); }
         FreeswitchData.objChannelData.fLoadPositionNumber(iPosition);         

	 
         return true;



   case NOTFOUND:               
        return false;

   default:         return false;

 
  }

 return false;
}









     bool ExperientDataClass::fCreateAliRequestKey(ali_format enumArg)
        {
         int                    intALiRequestLength;
         string                 stringNPD;
         string                 string7DigitNumber;
         MessageClass		objMessage;
         

         ALIData.stringAliRequest = "";
         ALIData.stringAutoRepeatAliRequest = "";
         
         switch(enumArg)
          {
           case FOURTEEN_DIGIT:
                intALiRequestLength     = 14;

                // check if trunk is CLID and if we must bid the test key
                if((TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType == CLID)&&(boolCLID_BID_TEST_KEY))   {
                 
                  ALIData.boolALI_Test_Key_Was_Bid = true;
                  ALIData.stringALiRequestKey = strANI_14_DIGIT_REQ_TEST_KEY;
                  break;
 
                }
                else if (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType == CLID)    {
                 
                  if (CallData.stringSevenDigitPhoneNumber.empty() ) {return false;}
                  string7DigitNumber =  CallData.stringSevenDigitPhoneNumber; 
 
                }
                else   {
                 
                  if (CallData.stringPANI.length() != 10 ) {return false;}
                  string7DigitNumber = CallData.stringPANI.substr(3,7);
                }
               
    
                // if stringOneDigitAreaCode == "" then we have 10 digit ANI to 8 digit ALI
                if (CallData.stringOneDigitAreaCode == "")   {
                 
                  // check if bid will be pANI or Callback number
                  if (CallData.stringPANI.empty()){stringNPD = CallData.stringThreeDigitAreaCode;}
                  else                            {stringNPD.assign(CallData.stringPANI,0,3);     string7DigitNumber.assign(CallData.stringPANI,3,7);}

                  for (int i = 0; i < 4; i++){if (NPD[i] == stringNPD) {CallData.stringOneDigitAreaCode = int2str(i); break; }}

                }

                // check if area code was in NPD list or was 7 digit ANI
                if (CallData.stringOneDigitAreaCode != "") {ALIData.stringALiRequestKey = CallData.stringOneDigitAreaCode+string7DigitNumber;}
                else   {
                 
                  // condition is no NPD for callback number,  create special record key to bid to generate no record found
                  objMessage.fMessage_Create(LOG_WARNING,261, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , CallData, objBLANK_TEXT_DATA,
                                             objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA,
                                             objBLANK_ALI_DATA,ALI_MESSAGE_261, stringNPD); 
                  enQueue_Message(ALI,objMessage);
                  ALIData.boolALI_Test_Key_Was_Bid = true;
                  ALIData.stringALiRequestKey = strANI_14_DIGIT_REQ_TEST_KEY;
                }
                
                break;
           case SIXTEEN_DIGIT:
                intALiRequestLength     = 16;
                // check if trunk is CLID and if we must bid the test key
                if((TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType == CLID)&&(boolCLID_BID_TEST_KEY))
                 {
                  ALIData.boolALI_Test_Key_Was_Bid = true;
                  ALIData.stringALiRequestKey = strANI_16_DIGIT_REQ_TEST_KEY;
                  break; 
                 }
                else if (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType == CLID)
                 {
                  if (CallData.stringTenDigitPhoneNumber.empty() ) {return false;}
                  ALIData.stringALiRequestKey =  CallData.stringTenDigitPhoneNumber;  
                 }
                else
                 {
                  if (CallData.stringPANI.length() != 10 ) {return false;}
                  ALIData.stringALiRequestKey = CallData.stringPANI;
                 }
                
        //        if (CallData.stringPANI == strBLANK_PANI)  {ALIData.stringALiRequestKey = CallData.stringTenDigitPhoneNumber;}
        //        else                                       {ALIData.stringALiRequestKey = CallData.stringPANI;}
                
                break;
           case E2_PROTOCOL:
                
                boolIsHeartbeat	        = false;

            //    //cout << ASCII_String(ALIData.chE2BidRequest,  ALIData.intE2RequestLength) << endl;


                return true;
           default:
                SendCodingError("ExperientDataClass.cpp - CODING ERROR in ExperientDataClass::fCreateAliRequestKey()" );
                return false;
                break;
          }//end switch
 
         ALIData.stringAutoRepeatAliRequest  = ALIData.stringALiRequestKey + CallData.stringPosn + strREPEAT_ALI_BID_TRUNK;
         
         switch (ALIData.enumALIBidType)
          {
           case INITIAL: 
               ALIData.stringAliRequest = ALIData.stringALiRequestKey + CallData.stringPosn + CallData.stringTrunk;
               break;
          case REBID:
          case AUTO:
               ALIData.stringAliRequest = ALIData.stringALiRequestKey + CallData.stringPosn + strREPEAT_ALI_BID_TRUNK;
               break;
          case MANUAL:
               ALIData.stringAliRequest = ALIData.stringALiRequestKey + CallData.stringPosn + strPREFERRED_MANUAL_ALI_BID_TRUNK;
               break;
               
          default:
            SendCodingError("ExperientDataClass.cpp - Error in ANI bid type in ExperientDataClass::fCreateAliRequestKey() ");
            return false;
      }

       //cout << "Create RequestKey -> " << ALIData.stringAliRequest << endl;
         boolIsHeartbeat	        = false;

         ALIData.stringAliRequest       = ALIData.stringAliRequest       + CheckSumGenerate( ALIData.stringAliRequest, ALIData.stringAliRequest.length())+charCR;
         ALIData.stringAutoRepeatAliRequest = ALIData.stringAutoRepeatAliRequest + CheckSumGenerate( ALIData.stringAutoRepeatAliRequest, ALIData.stringAutoRepeatAliRequest.length())+charCR;
         return true;
 //         //cout << ALIData.stringAliRequest << endl;

        } // end  void ExperientDataClass::fCreateAliRequestKey(ali_format enumArg)


    int ExperientDataClass::fFindMatchestoALIDatabases()
       {
        int                            intCount = 0;
        unsigned long long int         intSearchTelnoNumber;
        string                         stringSearchTelno;
        MessageClass                   objMessage;
        extern ALISteering             ALI_Steering[intMAX_ALI_DATABASES + 1 ];


  //      if   ((!CallData.boolWirelessVoipCall)||(CallData.stringPANI == strBLANK_PANI)) {stringSearchTelno = CallData.stringTenDigitPhoneNumber; intSearchTelnoNumber = char2int(CallData.stringTenDigitPhoneNumber.c_str());}
  //      else                                                          {stringSearchTelno = CallData.stringPANI; intSearchTelnoNumber = char2int(CallData.stringPANI.c_str());}
        stringSearchTelno = CallData.stringPANI; 
intSearchTelnoNumber = char2int(CallData.stringPANI.c_str());
        

        for (int i = 1; i <= intNUM_ALI_DATABASES; i++)
          {   
           if( ALI_Steering[i].fCanBidDatabase(TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType) )        
            {
             for (int j = 1; j <= ALI_Steering[i].intNumPANIRangePairs ; j++)
              {
               if ((intSearchTelnoNumber >= ALI_Steering[i].intPANILowRange[j])&&
                  (intSearchTelnoNumber <= ALI_Steering[i].intPANIHighRange[j]))  
                {
                 ALIData.intFoundinDatabase[i] = j;
                 intCount++; break;
                }
           
              }// end for (int j = 1; j <= ALI_Steering[intOffset].intNumPANIRangePairs; j++)
            }


          }// end for (int i = 1; i <= intNUM_ALI_DATABASES; i++) 
        
      //   if (!intCount)
      //    {
      //     objMessage.fMessage_Create(LOG_WARNING,204, fCallData(), objBLANK_ALI_PORT, ALI_MESSAGE_204, stringSearchTelno); 
     //      enQueue_Message(ALI,objMessage);
     //     }
         return intCount;

        }// end ExperientDataClass::fFindInDatabases()
    



   
    int  ExperientDataClass::fSelectALiDatabase(int intArg) {
         int                            intOffset;        
         MessageClass                   objMessage;
         extern ALISteering             ALI_Steering[intMAX_ALI_DATABASES + 1 ];
         bool                           boolFound = false;
//  cout << "start select database int arg = " << intArg << endl;
         if (!fFindMatchestoALIDatabases()) {return 0;}
//  cout << "there is a match " << endl;

         if (ALIData.intNumberOfInitialBids > ALIData.fHowManyDatabasesCanbeBid()) {return 0;}
//  cout << "Passed intial bid check" << endl;
            
         if (ALIData.intLastDatabaseBid) {intOffset = ALIData.intLastDatabaseBid + 1;}
         else                            {intOffset = intArg;}

         for (int i = 1; i <= intNUM_ALI_DATABASES; i++)
          {      
           if (intOffset > intNUM_ALI_DATABASES) {intOffset = 1;}         
           if (ALIData.intFoundinDatabase[intOffset]) {boolFound = true; break;}
           intOffset++;
          }
        if (!boolFound) {return 0;}
 //       //cout << "found in database number " << intOffset << endl;
        ALIData.intALITimeoutSec   = ALI_Steering[intOffset].intPANIRebidDelay[ALIData.intFoundinDatabase[intOffset]];
  //      ALIData.intLastDatabaseBid = intOffset; 
 //       ALIData.intNumberOfInitialBids++;
        return intOffset;

        }// end ExperientDataClass::fSelectALiDatabase(int intArg)







     void ExperientDataClass::fClear_Position_History()
        {
         CallData.ConfData.strActiveConferencePositions            = "";
         CallData.ConfData.strHistoryConferencePositions           = "";
        }


     void ExperientDataClass::fClear_Active_Flags() {
         boolRinging                                      = false;
         boolConnect                                      = false;
         boolRingBack                                     = false;
         CallData.ConfData.strActiveConferencePositions   = "";
         CallData.ConfData.strOnHoldPositions             = "";
         boolAbandoned                                    = false;
         boolDisconnect                                   = true;
         WindowButtonData.fClear();
        // TDDdata.fClear(); //erases the conversation .....
        // TextData.fClear();
     } 




    void ExperientDataClass::fDisplay(int i)
        {
         cout << setw(6) << left << i  <<  setw(20) << left << CallData.intUniqueCallID 	<< endl;
	 
//		cout << "Ali Text	    =" << ALIData.ALIData.stringAliText					<< endl;
//                cout << "Ali Text Message   =" << ALIData.stringAliTextMessage                          << endl;
//		cout << "Time rcvd sec      =" << timespecTimeRecordReceivedStamp.tv_sec 	<< endl;
//		cout << "Time rcvd nsec     =" << timespecTimeRecordReceivedStamp.tv_nsec 	<< endl;
         cout <<                  stringWRKStationIPAddr << endl;
        cout <<                   stringWRKStationListenPort << endl;
        cout <<                   stringWRKStationMSGText << endl;;             
	}// end fDisplay



    int  ExperientDataClass::fInt_of_Trunk()
        { 
         int 			i;
         istringstream 	istringstreamStr(CallData.stringTrunk);

         istringstreamStr >> i;
         return i;
        }


    int  ExperientDataClass::fInt_of_Posn()
        { 
         int 			i;
         istringstream 	istringstreamStr(CallData.stringPosn);

         istringstreamStr >> i;
         return i;
        }



    int ExperientDataClass::fCAD_Xmit_ALI(int i) 
        {
         int 		intRC;
         MessageClass	objMessage;
         Port_Data      objPortData;
         string         strDataToSend;

         objPortData.fLoadPortData(CAD, i);

            clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeXmitStamp);
            CADPort[i].fSetPortActive();

            switch (CADPort[i].boolNENA_CADheader)
             {
              case true: 
                   switch (CADPort[i].boolCAD_EraseFirstALI_CR)
                    {
                     case true:
                          strDataToSend = CADPort[i].PositionAliases.CADmessageWithAlias(ALIData.stringALiTestMessageWOCR);break;    
                     case false:
                          strDataToSend = CADPort[i].PositionAliases.CADmessageWithAlias(ALIData.stringAliTextMessage);break;    
                    }
                   break;
              case false:
                   switch (CADPort[i].boolCAD_EraseFirstALI_CR)
                    {
                     case true:
                          strDataToSend = CADPort[i].PositionAliases.CADmessageWithAlias(ALIData.strCADmessageWithoutHeaderWOCR);break;
                     case false: 
                          strDataToSend = CADPort[i].PositionAliases.CADmessageWithAlias(ALIData.strCADmessageWithoutHeader);break;
                    }
                   break;    
             }
             
            ALIData.strCADmessageSent = strDataToSend;
            intRC = Transmit_Data(CAD, i, (char*) strDataToSend.c_str(), strDataToSend.length()); 
            if (intRC)   {
              
              // record send failure
              this->ALIData.eJSONEventCode = CAD_SEND_FAIL;		    
              objMessage.fMessage_Create(LOG_WARNING,415, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), objBLANK_TEXT_DATA, objPortData, 
                                         objBLANK_FREESWITCH_DATA, ALIData, CAD_MESSAGE_415, CallData.stringTrunk , CallData.fTenDigitCallbackGUIformat() );
	      enQueue_Message(CAD,objMessage);
             
              // make sure ACK is required to force timeout error
            }
            else   {
             
              // Cad Transmit Record
              this->ALIData.eJSONEventCode = SEND_TO_CAD;		    
              objMessage.fMessage_Create(LOG_CONSOLE_FILE,402, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), objBLANK_TEXT_DATA, 
                                         objPortData, objBLANK_FREESWITCH_DATA, ALIData, CAD_MESSAGE_402);
              enQueue_Message(CAD,objMessage);             	
              if ((CADPort[i].boolRecordACKRequired)&&(!CADPort[i].boolEmergencyShutdown)){CADPort[i].fSetPortActive();}
              else                                                                        {CADPort[i].fSetPortInactive();}

            }//end if (intRC) else

          
         return CallData.intTrunk;
	}// end fCAD_Xmit_ALI()


    int ExperientDataClass::fCAD_Xmit_Erase(int i) 
        {
         int 		intRC;
         MessageClass	objMessage;
         Port_Data      objPortData;
         string         strDataToSend;

         objPortData.fLoadPortData(CAD, i);

 
            clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeXmitStamp);
            CADPort[i].fSetPortActive();
            strDataToSend = CADPort[i].PositionAliases.CADmessageWithAlias(stringCADEraseMsg);
            intRC = Transmit_Data(CAD, i, (char*) strDataToSend.c_str(), strDataToSend.length() );
            if (intRC)   {
              
              // record send failure
              objMessage.fMessage_Create(LOG_WARNING,436, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), objBLANK_TEXT_DATA, objPortData, 
                                         objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_436, int2strLZ(i), CallData.stringTrunk , CallData.fTenDigitCallbackGUIformat() );
	      enQueue_Message(CAD,objMessage);
             
              // make sure ACK is required to force timeout error
            }
            else   {
             
              // Cad Transmit Record		    
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 437, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                         CAD_MESSAGE_437, CallData.stringTrunk, CallData.stringPosn, CallData.fTenDigitCallbackGUIformat());
              enQueue_Message(CAD,objMessage);
             	
              if ((CADPort[i].boolRecordACKRequired)&&(!CADPort[i].boolEmergencyShutdown)){CADPort[i].fSetPortActive();}
              else                                                                        {CADPort[i].fSetPortInactive();}

            }//end if (intRC) else

          
         return CallData.intTrunk;
	}// end fCAD_Xmit_Erase()


bool ExperientDataClass::fCad_ReXmit(int i, bool boolACKrequired)     
{
 int		intRC;
 MessageClass	objMessage;
 int            intLogCode = LOG_WARNING;
 string         stringTransmit;
 int            intLength;
 int            intMessageCode, intFailCode;
 string         stringLogMessage, stringFailMessage;
 Port_Data      objPortData;

 objPortData.fLoadPortData(CAD, i);

 if (!CADPort[i].boolXmitRetry)       { return false; } 
 if (CADPort[i].boolEmergencyShutdown){ return false; } 
 if (CADPort[i].ePortConnectionType == TCP) {if(!CADPort[i].TCP_Server_Port.GetConnectionCount()){ return false; } } 

 if (boolIsHeartbeat)
  {
   stringTransmit       = charCADHEARTBEATMSG;
   intLength            = stringTransmit.length();
   intMessageCode       = 403;
   stringLogMessage     = CAD_MESSAGE_403;
   intFailCode          = 429;
   stringFailMessage    = CAD_MESSAGE_429;
   if (CADPort[i].boolReducedHeartBeatMode){intLogCode = LOG_CONSOLE_FILE * boolDEBUG;}
   else                                    {intLogCode = LOG_CONSOLE_FILE;}
  }
 else if (boolCADEraseMsg)
  {
   stringTransmit       = CADPort[i].PositionAliases.CADmessageWithAlias(stringCADEraseMsg);
   intLength            = stringTransmit.length();
   intMessageCode       = 438;
   stringLogMessage     = CAD_MESSAGE_438;
   intFailCode          = 441;
   stringFailMessage    = CAD_MESSAGE_441;
  }
 else if (CADPort[i].boolNENA_CADheader)
  {
   stringTransmit       = CADPort[i].PositionAliases.CADmessageWithAlias(ALIData.stringAliTextMessage);
   intLength            = stringTransmit.length();
   intMessageCode       = 430;
   stringLogMessage     = CAD_MESSAGE_430;
   intFailCode          = 441;
   stringFailMessage    = CAD_MESSAGE_441;
  }
 else
  {
   stringTransmit       = CADPort[i].PositionAliases.CADmessageWithAlias(ALIData.strCADmessageWithoutHeader);
   intLength            = stringTransmit.length();
   intMessageCode       = 430;
   stringLogMessage     = CAD_MESSAGE_430;
   intFailCode          = 441;
   stringFailMessage    = CAD_MESSAGE_441;
  }


 CADPort[i].fSetPortActive();
 clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeLastHeartbeat);
 clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeXmitStamp);
 intRC = Transmit_Data(CAD, i, (char *)stringTransmit.c_str(), intLength);

 if      ((boolIsHeartbeat)&&(!CADPort[i].boolHeartBeatACKrequired)){CADPort[i].fSetPortInactive();}
 else if (boolIsHeartbeat)                                          {CADPort[i].fSetPortActive();}
 else if (!CADPort[i].boolRecordACKRequired)                        {CADPort[i].fSetPortInactive();}

 if (intRC)   {
  
   //  Retransmit Failure
   objMessage.fMessage_Create(LOG_WARNING,intFailCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              stringFailMessage, CallData.stringTrunk, CallData.fTenDigitCallbackGUIformat());
   enQueue_Message(CAD,objMessage);
   
   CADPort[i].boolXmitRetry = false;
   return false;

 }
 else   {
  
   // retransmit OK
   objMessage.fMessage_Create(intLogCode,intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), 
                              objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              stringLogMessage, CallData.stringTrunk, CallData.fTenDigitCallbackGUIformat()); 
   enQueue_Message(CAD,objMessage);
             
 }//end if (intRC) else

 // set to prevent 2nd retransmit on port
 CADPort[i].boolXmitRetry = false;
 return true;

}




// will not need ........ TBC
bool ExperientDataClass::fLoad_ALI_Exists( string stringArg)
{
 //int intTrunk = fInt_of_Trunk();

 if (!((CallData.intTrunk > 94)&&(CallData.intTrunk < 100))){return true;}
 
 if      (stringArg == "true"){ALIData.boolALIRecordReceived = true;}
 else if (stringArg == "True"){ALIData.boolALIRecordReceived = true;}
 else if (stringArg == "TRUE"){ALIData.boolALIRecordReceived = true;}
 else                         {ALIData.boolALIRecordReceived = false;}
// //cout << "SET TRUE IN XML LOAD" << endl;
 return true;
}

bool ExperientDataClass::fLoad_RingDateTime( string stringArg)
{
 // error check ???????? TBC
 stringRingingTimeStamp = stringArg;
 return true;
}

// may need to do something with position/ portnum ..... they are different TBC
bool ExperientDataClass::fLoad_ANIPort(threadorPorttype enumArg, string stringArg,string strXMLData)
{
 MessageClass 						objMessage;
 Port_Data    						objPortData;
 extern ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1]; 




 objPortData.fLoadPortData(enumArg, intWorkStation);
  if (!Validate_Integer(stringArg))   {
  
   // may need to sub wkstation into calldata ....
   objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              KRN_MESSAGE_129i, stringArg, ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(WRK,objMessage);
   return false;

  }
 
 intANIPortCallReceived = char2int(stringArg.c_str());
 if (intANIPortCallReceived > intNUM_ANI_PORTS)   {
  
   objMessage.fMessage_Create(LOG_WARNING,727, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_727, stringArg);
   enQueue_Message(WRK,objMessage);
   return false;

 }

 enumANISystem = ANIPort[intANIPortCallReceived].UDP_Port.enumANISystem;
 return true;
}


bool ExperientDataClass::fLoad_WRK_Listen_Port(threadorPorttype enumArg, string stringArg, string strXMLData)
{
 MessageClass objMessage;
 Port_Data    objPortData;

 objPortData.fLoadPortData(enumArg, intWorkStation);
 if (!ValidateFieldAndRange(enumArg, stringArg, strXMLData, 1, "ListenPort", 65535, 0))     {return false;}
  
 stringWRKStationListenPort = stringArg;
 return true;
}  




bool ExperientDataClass::fLoad_CallState(threadorPorttype enumArg, string stringArg, string strXMLData)
{
 // note this function must be called after fXML_Load_ALI_Data() 
 MessageClass  	objMessage;
 Port_Data    	objPortData;

 objPortData.fLoadPortData(enumArg, intWorkStation);
 intCallStateCode =  CallStateCode(stringArg);
 if (intCallStateCode == 0)   {
  
   objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              WRK_MESSAGE_710e, stringArg, ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg,objMessage);
   return false;

 }

 return true;
}


bool ExperientDataClass::fLoad_ALIState(threadorPorttype enumArg, string stringArg, string strXMLData)
{

 MessageClass  	objMessage;
 Port_Data    	objPortData;
 

 objPortData.fLoadPortData(enumArg, intWorkStation);
 
if(!ValidateFieldAndRange(enumArg, stringArg, strXMLData, 1, "ALIState", 99, 0)) {return false;}
  
 ALIData.intALIState = char2int(stringArg.c_str());
 return true;
}





      
string ExperientDataClass::fLoad_InBound_XML_Message( const char* charString, int intWorkstation)
    {
     // note this function will only properly parse a single xml command not multiple ..

     XMLResults         xe;
     XMLNode            MainNode, EventNode, WorkingNode, CallDataNode, TDDNode, TestNode, TextNode;
     struct timespec    timespecTimeNow;
     string             sPfx,sSfx;
     WorkStation        NotifyWorkstation;
     string             strMsgText;
     string             stringError;
     Port_Data          objPortData;
     MessageClass       objMessage;
     string             ReturnString;
     string             strError = "Bad Data";
     string             strXMLversion = "";
     string             strValue;
     bool               boolEncodedText = false;
     extern Telephone_Devices                       TelephoneEquipment;  

  //   //cout << "Loading XML message ...." << endl;
     MainNode=XMLNode::parseString(charString,NULL,&xe);
     
     if (xe.error) {return XMLNode::getError(xe.error);}
       
     EventNode = MainNode.getChildNode(XML_NODE_EVENT);
     if (EventNode.isEmpty()) {return "XML Event Node Missing"; } 

     if (EventNode.getAttribute(XML_FIELD_PACKET_ID))
      {strEventUniqueID = EventNode.getAttribute(XML_FIELD_PACKET_ID);}

     if (EventNode.getAttribute(XML_FIELD_XML_SPEC))
      {strXMLversion =  EventNode.getAttribute(XML_FIELD_XML_SPEC);}

     if (EventNode.getAttribute(XML_FIELD_WINDOWS_USER))
      {strWindowsUser =  EventNode.getAttribute(XML_FIELD_WINDOWS_USER);}
     else     {return "XML WindowsUser attribute missing"; }           

     intWorkStation = intWorkstation; 
     enumANISystem =  enumANI_SYSTEM;

     sPfx = int2strLZ(intWorkStation)+" XML Message Missing ";
     sSfx = " Attribute";    

     for (int i = 0; i < NUM_WRK_XML_KEYS; i++) {

       WorkingNode = EventNode.getChildNode( vstringWRK_XML_KEYS[i].c_str() );

/*
#define NUM_WRK_XML_KEYS                              30                         // number of keys !!!!
#define WRK_XML_NODE_KEY_0                            "Heartbeat"
#define WRK_XML_NODE_KEY_1                            "ALIRebid"
#define WRK_XML_NODE_KEY_2                            "ManualALIBid"
#define WRK_XML_NODE_KEY_3                            "SendToCAD"
#define WRK_XML_NODE_KEY_4                            "InitiateTransfer"
#define WRK_XML_NODE_KEY_5                            "CancelTransfer"
#define WRK_XML_NODE_KEY_6                            "Log"
#define WRK_XML_NODE_KEY_7                            "SendInitializationXML"
#define WRK_XML_NODE_KEY_8                            "InitiateConferenceJoin"
#define WRK_XML_NODE_KEY_9                            "InitiateConferenceLeave"
#define WRK_XML_NODE_KEY_10                           "TakeControl"
#define WRK_XML_NODE_KEY_11                           "TDDModeOn"
#define WRK_XML_NODE_KEY_12                           "TDDchangeMute"
#define WRK_XML_NODE_KEY_13                           "TDDSendString"
#define WRK_XML_NODE_KEY_14                           "TDDSendCharacter"
#define WRK_XML_NODE_KEY_15                           "Dial"
#define WRK_XML_NODE_KEY_16                           "InitiateBlindTransfer"
#define WRK_XML_NODE_KEY_17                           "SendEraseToCAD"
#define WRK_XML_NODE_KEY_18                           "ReloadConsole"
#define WRK_XML_NODE_KEY_19                           "ForceDisconnect"
#define WRK_XML_NODE_KEY_20                           "RadioContactClosure"
#define WRK_XML_NODE_KEY_21                           "Answer"
#define WRK_XML_NODE_KEY_22                           "Pickup"
#define WRK_XML_NODE_KEY_23                           "Hold"
#define WRK_XML_NODE_KEY_24                           "Hangup"
#define WRK_XML_NODE_KEY_25                           "Mute"
#define WRK_XML_NODE_KEY_26                           "VolumeUp"
#define WRK_XML_NODE_KEY_27                           "KillChannel"
#define WRK_XML_NODE_KEY_28                           "FlashHook"
#define WRK_XML_NODE_KEY_29                           "Barge"
#define WRK_XML_NODE_KEY_30                           "ActivateAttendedTransfer"
#define WRK_XML_NODE_KEY_31                           "TextSendString"
#define WRK_XML_NODE_KEY_32                           "DisconnectRequest"
#define WRK_XML_NODE_KEY_33                           "SendDTMF"
#define WRK_XML_NODE_KEY_34                           "VolumeDown"
*/

  //     //cout << charString << endl;

       if (!WorkingNode.isEmpty())
        {
         
         switch (i)
          {
          // //cout << " node found = " <<  vstringWRK_XML_KEYS[i] << endl; 
           case 0:
                  // heartbeat        
                  enumWrkFunction      = WRK_HEARTBEAT;
                  boolIsHeartbeat = true;
                  if (WorkingNode.getAttribute(XML_FIELD_LISTEN_PORT))
                   {if(!fLoad_WRK_Listen_Port(WRK, WorkingNode.getAttribute(XML_FIELD_LISTEN_PORT), charString)) {return strError;} }
                  else                                                                                           {return sPfx+XML_FIELD_LISTEN_PORT+sSfx;}
                  if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                   {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString, false, false)) {return strError;} }
                  else                                                                                                                  {return sPfx+XML_FIELD_POSITION+sSfx;}
                  if(WorkingNode.getAttribute(XML_FIELD_GUI_VERSION))
                   {strGUIVersion = WorkingNode.getAttribute(XML_FIELD_GUI_VERSION);}
                  if(WorkingNode.getAttribute(XML_FIELD_WINDOWS_USER))
                   {strWindowsUser = WorkingNode.getAttribute(XML_FIELD_WINDOWS_USER);}
                  if(WorkingNode.getAttribute(XML_FIELD_WINDOWS_OS))
                   {strWindowsOS = WorkingNode.getAttribute(XML_FIELD_WINDOWS_OS);}
                  strGUIxmlVersion = strXMLversion;



                  return "";

           case 1:
                  // Rebid
                  ALIData.enumALIBidType = REBID;
                  enumWrkFunction        = WRK_REBID;

      //            CallDataNode = WorkingNode.getChildNode(XML_NODE_CALL_DATA);
      //            if (CallDataNode.isEmpty()) {return "XML CallData Node Missing"; } 
     //            ReturnString = fXML_Load_CallData(CallDataNode, charString);
     //             if (!ReturnString.empty()){return ReturnString;}

                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}
                  clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
                  stringTimeOfEvent = get_time_stamp(timespecTimeNow);
                  return "";

           case 2: 
                    // MANUAL Bid
                    ALIData.enumALIBidType = MANUAL;
                    enumWrkFunction      = WRK_REBID;
                  
                    //Trunk loaded before phone numbers
                 //   stringTrunk = int2strLZ(PREFERRED_MANUAL_ALI_BID_TRUNK);
                 //   intTrunk    = PREFERRED_MANUAL_ALI_BID_TRUNK;
                    fLoad_Trunk(SOFTWARE, WRK, "", PREFERRED_MANUAL_ALI_BID_TRUNK);
                    if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                     {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString, false, false )) {return strError;} }
                    else                                                                                                                   {return sPfx+XML_FIELD_POSITION+sSfx;}
                   
                    if (WorkingNode.getAttribute(XML_FIELD_CALLBACK_NUMBER))
                     {if(!fLoad_PhoneNumbers(XML_DATA_FIELD, WRK, 1, WorkingNode.getAttribute(XML_FIELD_CALLBACK_NUMBER),"","","",charString)) {return strError;} }
                    else                                                                                                                    {return sPfx+XML_FIELD_CALLBACK_NUMBER+sSfx;}
                    CallData.stringPANI = CallData.stringTenDigitPhoneNumber;
                    clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
                    stringTimeOfEvent = get_time_stamp(timespecTimeNow);
                    return "";
            
           case 3:
                  // Update CAD
                  enumWrkFunction      = UPDATE_CAD;

                  // need ALI if from Manual Bid  
                  if (WorkingNode.getAttribute(XML_FIELD_ALI_RECORD_ENCODING))
                   {
                    ReturnString = fXML_Load_ALI_Data(WorkingNode, charString);
                    if (!ReturnString.empty()){return ReturnString;}
                   } 
                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                  return "";

           case 4:
                  // Transfer
                  enumWrkFunction      = WRK_TRANSFER;
                 //type must be loaded before number!  

                  if (WorkingNode.getAttribute(XML_FIELD_TRANSFER_TYPE))
                   {if(!fLoad_Transfer_Type(WRK, WorkingNode.getAttribute(XML_FIELD_TRANSFER_TYPE),charString, 1))      {return strError;} }
                  else                                                                                                  {return sPfx+XML_FIELD_TRANSFER_TYPE+sSfx;}
                  
                  ReturnString = fXML_Load_Destination_Data(WorkingNode, charString);
                  if (!ReturnString.empty()){return ReturnString;} 
 
                  CallData.TransferData.fLoadFromPosition(intWorkStation);

                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;} 
                  return "";

           case 5:
                  // Cancel Transfer
                  enumWrkFunction      = WRK_CNX_TRANSFER;
                  //type must be loaded before number!
  
                  if (WorkingNode.getAttribute(XML_FIELD_TRANSFER_TYPE))
                   {if(!fLoad_Transfer_Type(WRK, WorkingNode.getAttribute(XML_FIELD_TRANSFER_TYPE),charString, 1))      {return strError;} }
                  else                                                                                                  {return sPfx+XML_FIELD_TRANSFER_TYPE+sSfx;}
                  
                  ReturnString = fXML_Load_Destination_Data(WorkingNode, charString);
                  if (!ReturnString.empty()){return ReturnString;} 

                  CallData.TransferData.fLoadFromPosition(intWorkStation);
                  CallData.TransferData.boolGUIcancel =true;
                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}                             
                  return "";

           case 6:
                  //Log
                  enumWrkFunction      = WRK_LOG;

                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
            
                  if (WorkingNode.getAttribute(XML_FIELD_CONF_NUMBER))
                   {if(!fLoad_ConferenceData(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CONF_NUMBER),1, false ,charString)) {return strError;} }                  

                  // trunk loaded before phone numbers
                  if (WorkingNode.getAttribute(XML_FIELD_TRUNK))        
                   {if(!fLoad_Trunk(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_TRUNK), 1, false, charString)) {return strError;} }

                  if (WorkingNode.getAttribute(XML_FIELD_CALLBACK_NUMBER))
                   {if(!fLoad_PhoneNumbers(XML_DATA_FIELD, WRK, 1, WorkingNode.getAttribute(XML_FIELD_CALLBACK_NUMBER),"","","",charString)) {return strError;} }

                  if (WorkingNode.getAttribute(XML_FIELD_PSUEDO_ANI))
                   {if(!fLoad_pANI(WRK, WorkingNode.getAttribute(XML_FIELD_PSUEDO_ANI),charString ) )                      {return strError;}}
                  

                  if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                   {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString, false, false)) {return strError;} }
                  

                  if (WorkingNode.getAttribute(XML_FIELD_MESSAGE_NUMBER))
                   {if(!fLoad_MSG_Number(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_MESSAGE_NUMBER), 1, charString)) {return strError;}}
                  else                                                                                                            {return sPfx+XML_FIELD_MESSAGE_NUMBER+sSfx;}

                  if (WorkingNode.getAttribute(XML_FIELD_LOG_TYPE))
                   {if(!fLoad_MSG_Type(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_LOG_TYPE), 1, charString)){return strError;} }
                  else                                                                                                   {return sPfx+XML_FIELD_LOG_TYPE+sSfx;}


                  if (WorkingNode.getAttribute(XML_FIELD_ENCODING)) { boolEncodedText = true;}
                  if (WorkingNode.getAttribute(XML_FIELD_MESSAGE_TEXT))
                   {
                    switch(intLogType)
                     {
                      case LOG_TDD:
                           if(TextData.TDDdata.fLoadTDDReceived(WorkingNode.getAttribute(XML_FIELD_MESSAGE_TEXT),true, boolEncodedText))
                            {stringWRKStationMSGText = TextData.TDDdata.strTDDstring; break;}
                      default:
                           strMsgText = WorkingNode.getAttribute(XML_FIELD_MESSAGE_TEXT);
                           if(!fLoad_MSG_Text(XML_DATA_FIELD, WRK, ASCII_String(strMsgText.c_str(), strMsgText.length()), boolEncodedText)) {return strError;}
                     }// end switch
                   } 
                  else                                                                                                                 {return sPfx+XML_FIELD_MESSAGE_TEXT+sSfx;}

                  return "";

           case 7:
                 // Init XML Request
                 enumWrkFunction      = WRK_SEND_INIT_DATA;
              //   //cout << "got send init xml" << endl; 
                 if (WorkingNode.getAttribute(XML_FIELD_LISTEN_PORT))
                  {if(!fLoad_WRK_Listen_Port(WRK, WorkingNode.getAttribute(XML_FIELD_LISTEN_PORT), charString))  {return strError;} }
                 else                                                                                            {return sPfx+XML_FIELD_LISTEN_PORT+sSfx;}

                 if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                  {
                   if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString,false,false))
                    {
                      // send Workstation Shutdown command ..                     
                      stringError  = FindandReplace(WRK_MESSAGE_716w, "%%%", int2str(CallData.intPosn));
                   //   NotifyWorkstation.fCreateAlert(stringError, objCallData, stringWRKStationIPAddr, char2int(stringWRKStationListenPort.c_str()), true);
                      return stringError;
                    } 
                  }
                 else                                                                                            {return sPfx+XML_FIELD_POSITION+sSfx;}
                 if(WorkingNode.getAttribute(XML_FIELD_WINDOWS_USER))
                   {strWindowsUser = WorkingNode.getAttribute(XML_FIELD_WINDOWS_USER);}
                 if(WorkingNode.getAttribute(XML_FIELD_WINDOWS_OS))
                   {strWindowsOS = WorkingNode.getAttribute(XML_FIELD_WINDOWS_OS);}
                 return "";

           case 8:
                  // InitiateConferenceJoin (Unused at this time !!!! TBC)
                  enumWrkFunction      = WRK_CONFERENCE_JOIN;
                  SendCodingError("ExperientDataClass.cpp - coding error in ExperientDataClass::fLoad_InBound_XML_Message() case 8"); 
                  return "";

           case 9:
                  // InitiateConferenceLeave (do we need this ??? TBC)
                  enumWrkFunction      = WRK_CONFERENCE_LEAVE;
                  SendCodingError("ExperientDataClass.cpp - coding error in ExperientDataClass::fLoad_InBound_XML_Message() case 9");                
                  return "";

           case 10:
                   // Take Control
                   enumWrkFunction      = WRK_TAKE_CONTROL;

           //       CallDataNode = WorkingNode.getChildNode(XML_NODE_CALL_DATA);
           //       if (CallDataNode.isEmpty()) {return "XML CallData Node Missing"; } 
           //       ReturnString = fXML_Load_CallData(CallDataNode, charString);
           //       if (!ReturnString.empty()){return ReturnString;} 
                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}          
                  return ""; 

           case 11: 
                   // TDDModeOn
                   enumWrkFunction      = WRK_TDD_MODE_ON;
                    if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                    {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                   else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}               
                   return "";

           case 12:
                   // TDDchangeMute (asterisk systems only)
                   enumWrkFunction      = WRK_TDD_MUTE;

           //        CallDataNode = WorkingNode.getChildNode(XML_NODE_CALL_DATA);
           //        if (CallDataNode.isEmpty()) {return "XML CallData Node Missing"; } 
           //        ReturnString = fXML_Load_CallData(CallDataNode, charString);
           //        if (!ReturnString.empty()){return ReturnString;} 
                   if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                    {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                   else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}               
                   return "";

           case 13:
                   // TDDSendString
                  enumWrkFunction      = WRK_TDD_STRING;
              //    CallDataNode = WorkingNode.getChildNode(XML_NODE_CALL_DATA);
              //   if (CallDataNode.isEmpty()) {return "XML CallData Node Missing"; } 
              //    ReturnString = fXML_Load_CallData(CallDataNode, charString);
              //    if (!ReturnString.empty()){return ReturnString;}
                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}

                  TDDNode = WorkingNode.getChildNode(XML_NODE_TDD_DATA);
                  if (TDDNode.isEmpty()) {return "XML TDDdata Node Missing"; } 
                  ReturnString = fXML_Load_TDDdata(TDDNode, charString);
                  if (!ReturnString.empty()){return ReturnString;}
                 
                  return "";
           case 31:
                   //TextSendString
                   enumWrkFunction      = WRK_TEXT_STRING;
                   if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))     {return strError;} }
                   else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}

                  TextNode = WorkingNode.getChildNode(XML_NODE_TEXT_DATA);
                  if (TextNode.isEmpty()) {return "XML TextData Node Missing"; } 
                  ReturnString = fXML_Load_TextData(TextNode, charString);
                  if (!ReturnString.empty()){return ReturnString;}
                  return "";
           case 14:
                   // TDDSendCharacter (note this is no longer used ...)
                   SendCodingError("ExperientDataClass.cpp - coding error in ExperientDataClass::fLoad_InBound_XML_Message() case 14");                
                   return "";
           case 15:
                   // Dial                  
                   enumWrkFunction      = WRK_DIAL_OUT;                
                   if (WorkingNode.getAttribute(XML_FIELD_SPEED_DIAL_NUMBER))
                    { if (!CallData.TransferData.fLoad_Transfer_Number(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_SPEED_DIAL_NUMBER), 0)) {return strError;}  }
                   else {return sPfx + XML_FIELD_SPEED_DIAL_NUMBER +  sSfx;}
                   CallData.boolIsOutbound = true;

                   CallData.TransferData.strTransfertoNumber = TelephoneEquipment.fRemoveRoutingPrefix(CallData.TransferData.strTransfertoNumber);
                   CallData.TransferData.strTransfertoNumber = AddNineForOutgoingCalls(CheckForLongDistance( CallData.TransferData.strTransfertoNumber));
                   return "";

           case 16:
                   /*
                  // InitiateBlindTransfer 
                  enumWrkFunction      =  WRK_BLIND_TRANSFER;                
                  ReturnString = fXML_Load_Destination_Data(WorkingNode, charString);
                  if (!ReturnString.empty()){return ReturnString;} 

                  if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                   {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                  else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;} 
                 return "";
                 */

           case 17:
                // SendEraseToCAD
                enumWrkFunction      = WRK_CAD_ERASE;
                return "";

           case 18:
                // Send clear message back to workstation
                enumWrkFunction      = WRK_RELOAD_CONSOLE;

                return "";

           case 19:
                // Force Disconnect
                enumWrkFunction      = WRK_FORCE_DISCONNECT;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";

           case 20:
                //Radio Contact Closure
                enumWrkFunction      = WRK_RADIO_CONTACT_CLOSURE;
                if (WorkingNode.getAttribute(XML_FIELD_SET_STATE))
                  {if(!fLoad_RCC_State(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_SET_STATE), 1, charString)) {return strError;} }
                else                                                                                                       {return sPfx+XML_FIELD_SET_STATE+sSfx;} 
                 
                return "";
           case 21:
                //Answer
                enumWrkFunction      = WRK_ANSWER;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";

           case 22:
                //Pickup
                enumWrkFunction      = WRK_PICKUP;
                return "";
           case 23:
                //Hold
                enumWrkFunction      = WRK_TOGGLE_HOLD;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";
           case 24:
                //Hangup
                enumWrkFunction      = WRK_HANGUP;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";
           case 25:
                 //Mute
                 enumWrkFunction      = WRK_MUTE;
                 if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                  {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString,false,false)){return strError;} }
                 else                                                                                                               {return sPfx+XML_FIELD_POSITION+sSfx;}    
                    
                return "";
           case 26:
                //VolumeUp
                 if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                  {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString,false,false)){return strError;} }
                 else                                                                                                               {return sPfx+XML_FIELD_POSITION+sSfx;}               
                 enumWrkFunction  = WRK_VOLUME_UP;
/*
                 if (WorkingNode.getAttribute(XML_FIELD_VALUE))
                  {
                   strValue =  WorkingNode.getAttribute(XML_FIELD_VALUE);
                   if      (strValue == XML_VALUE_UP)   { enumWrkFunction  = WRK_VOLUME_UP;}
                   else if (strValue == XML_VALUE_DOWN) { enumWrkFunction  = WRK_VOLUME_DOWN;}
                   else                                {return strError;}
                  }

                 else                                                                                                               {return sPfx+XML_FIELD_POSITION+sSfx;} 
*/
                 return "";
           case 27:
                //KillChannel
                enumWrkFunction      = WRK_KILL_CHANNEL;
 
                if (WorkingNode.getAttribute(XML_FIELD_PARTICIPANT))
                  {if(!fLoad_Participant(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_PARTICIPANT), 1, charString)){return strError;} }
                else                                                                                                          {return sPfx+XML_FIELD_PARTICIPANT+sSfx;}

                 
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";

           case 28:
                //FlashTandem
                enumWrkFunction      = WRK_FLASH_TANDEM;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";

           case 29:
                enumWrkFunction      = WRK_BARGE;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return "";
           case 30:
                enumWrkFunction      = WRK_COMPLETE_ATT_XFER;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
                return ""; 
           // case 31 is up in the code              
           case 32:
                enumWrkFunction = WRK_DISCONNECT_REQUEST;
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   

                if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                  {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString,false,false)){return strError;} }
                 else                                                                                                               {return sPfx+XML_FIELD_POSITION+sSfx;} 
                return "";
           case 33:
                enumWrkFunction   = WRK_SEND_DTMF;
/*
                if (WorkingNode.getAttribute(XML_FIELD_CALL_ID))
                 {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_CALL_ID), 1, charString))    {return strError;} }
                else                                                                                                     {return sPfx+XML_FIELD_CALL_ID+sSfx;}   
*/
                if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                  {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString,false,false)){return strError;} }
                 else                                                                                                               {return sPfx+XML_FIELD_POSITION+sSfx;} 
          
                if (WorkingNode.getAttribute(XML_FIELD_DTMF_CHAR))
                 {if(!fLoad_DTMF_Char(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_DTMF_CHAR), 1, charString))           {return strError;} }
                 else                                                                                                               {return sPfx+XML_FIELD_DTMF_CHAR+sSfx;} 
                return "";
           case 34:
                //VolumeDown
                 if (WorkingNode.getAttribute(XML_FIELD_POSITION))
                  {if(!fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString,false,false)){return strError;} }
                 else                                                                                                               {return sPfx+XML_FIELD_POSITION+sSfx;}               
                 enumWrkFunction  = WRK_VOLUME_DOWN;

                 return "";

           default:
                  // ERROR
                  Abnormal_Exit(WRK, EX_SOFTWARE, "*Coding Error in switch in function  ExperientDataClass::fLoad_XML_Fields"  );
          }// end switch

        }// end if (!xmlNode[i].isEmpty())
     
      }// end  for for (int i = 1; i <= NUM_XML_KEYS; i++)
    
    // Node value was not found ..
    stringError = " ";                  // just to make the function call work
    objPortData.fLoadPortData(WRK, 1);
    objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                               objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_710b, stringError, 
                               ASCII_String(charString, strlen(charString)),"","","","", NORMAL_MSG, NEXT_LINE);
    enQueue_Message(WRK,objMessage);
    return strError;

   }

string  ExperientDataClass::fXML_Load_Destination_Data(XMLNode XMLarg, const char* charString)
{
 string                         sPfx, sSfx;
 string				strError = "Bad Data";
 extern Telephone_Devices       TelephoneEquipment; 
                      
 sPfx = "XML Message Missing ";
 sSfx = " Attribute";      

   // Note Transfer Type i.e. CallData.TransferData.eTransferMethod must be loaded before calling this function !
   if (XMLarg.getAttribute(XML_FIELD_TRANSFER_TO_NUMBER))
    {if(!CallData.TransferData.fLoad_Transfer_Number(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_TRANSFER_TO_NUMBER), 1)) {return strError;} }
   else                                                                                                    {return sPfx+XML_FIELD_TRANSFER_TO_NUMBER+sSfx;}
    //cout << "ExperientDataClass::fXML_Load_Destination_Data before .." << CallData.TransferData.strTransfertoNumber << endl;
   switch(CallData.TransferData.eTransferMethod)
    {
     case mTANDEM:
          break;
     case mREFER:
          //cout << "IN MREFER..........................." << endl;
          break;
     case mGUI_TRANSFER:
          if (!CallData.TransferData.strNG911PSAP_TransferNumber.empty()) 
           {
            CallData.TransferData.strTransfertoNumber = "911"; 
            CallData.TransferData.strTransferName = CallData.TransferData.strNG911PSAP_TransferNumber;
            break;
           }
     case mNG911_TRANSFER:
          if (!CallData.TransferData.strNG911PSAP_TransferNumber.empty()) 
           {
            CallData.TransferData.strTransfertoNumber = "911"; 
            CallData.TransferData.strTransferName = CallData.TransferData.strNG911PSAP_TransferNumber;
            break;
           }
          // go through default otherwise ..
     default:  
                   CallData.TransferData.strTransfertoNumber = TelephoneEquipment.fRemoveRoutingPrefix(CallData.TransferData.strTransfertoNumber);
                   CallData.TransferData.strTransfertoNumber = AddNineForOutgoingCalls(CheckForLongDistance(CallData.TransferData.strTransfertoNumber));
   //    //cout << "ExperientDataClass::fXML_Load_Destination_Data after .." << CallData.TransferData.strTransfertoNumber << endl; 
    }
   
 return "";
}



string ExperientDataClass::fXML_Load_ALI_Data(XMLNode XMLarg, const char* charString)
{
 string				strError = "Bad Data";                      


 if (XMLarg.getAttribute(XML_FIELD_ALI_RECORD_ENCODING))
  {if(!fLoad_Encoding(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_ALI_RECORD_ENCODING), 1, charString)) {return strError;} }

 if (XMLarg.getAttribute(XML_FIELD_ALI_RECORD))
  {if(!fLoad_Decode_ALI(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_ALI_RECORD), 1, charString)) {return strError;} }

 return "";
}




string ExperientDataClass::fXML_Load_TDDdata(XMLNode XMLarg, const char* charString)
{
 string                         ReturnString;
 string                         sPfx, sSfx;
 string				strError        = "Bad Data";
                     
 sPfx = "XML Message Missing ";
 sSfx = " Attribute";    
               
  if (XMLarg.getAttribute(XML_FIELD_TDD_DISPATCHER_SAYS))
   {
    if (XMLarg.getAttribute(XML_FIELD_TDD_ENCODING))
     {
      if(!TextData.TDDdata.fLoadTDDReceived(XMLarg.getAttribute(XML_FIELD_TDD_DISPATCHER_SAYS),true,true)){return strError;}
     }
    else
     {
      if(!TextData.TDDdata.fLoadTDDReceived(XMLarg.getAttribute(XML_FIELD_TDD_DISPATCHER_SAYS),true,false)){return strError;}
     }
   }
  else                                                                                                 
   {return sPfx+XML_FIELD_TDD_DISPATCHER_SAYS+sSfx;}


  return "";
}


string ExperientDataClass::fXML_Load_TextData(XMLNode XMLarg, const char* charString)
{
 string                         ReturnString;
 string                         sPfx, sSfx;
 string				strError     = "Bad Data";
 string                         strEncoding  = "";
 bool                           boolEncoding = false;
 string                         strText;
                   
 sPfx = "XML Message Missing ";
 sSfx = " Attribute"; 
  
  if (XMLarg.getAttribute(XML_FIELD_TEXT_TYPE ))           {TextData.fLoadTextType(XMLarg.getAttribute(XML_FIELD_TEXT_TYPE));}
  else                                                     {return sPfx+XML_FIELD_TEXT_TYPE+sSfx;}

  if (XMLarg.getAttribute(XML_FIELD_TEXT_ENCODING))        {strEncoding = XMLarg.getAttribute(XML_FIELD_TEXT_ENCODING);}
  if (strEncoding.length())                                {boolEncoding = true;}
 
  if (XMLarg.getAttribute(XML_FIELD_TEXT_DISPATCHER_SAYS)) {strText = XMLarg.getAttribute(XML_FIELD_TEXT_DISPATCHER_SAYS);}
  else                                                     {return sPfx+XML_FIELD_TEXT_DISPATCHER_SAYS+sSfx; }

 // strText = TextData.fMSRPreceived(strText, boolEncoding);

  switch (TextData.TextType)
   {
    case NO_TEXT_TYPE: return strError;
    case TDD_MESSAGE:
         if(!TextData.TDDdata.fLoadTDDReceived(strText, true, boolEncoding)) {return strError;}
         break;
    case MSRP_MESSAGE:
         if(!TextData.SMSdata.fLoadMSRPdispatcherSays(strText,boolEncoding)) {return strError;} 
         break;
   }              


  return "";
}





string ExperientDataClass::fXML_Load_CallData(XMLNode XMLarg, const char* charString)
{
 string                         sPfx, sSfx;
 string				strError = "Bad Data";                      
 sPfx = "XML Message Missing ";
 sSfx = " Attribute";    

 //RingDateTime
 if (XMLarg.getAttribute(XML_FIELD_RING_DATE_TIME))
  {if(!fLoad_RingDateTime(XMLarg.getAttribute(XML_FIELD_RING_DATE_TIME)))  {return strError;}}
 else                                                                      {return sPfx+XML_FIELD_RING_DATE_TIME+sSfx;}
 //EventDataTime ... Not Needed
 //TransmitDateTime ... not Needed

 //Call-ID
 if (XMLarg.getAttribute(XML_FIELD_CALL_ID))
  {if(!fLoad_Call_Id(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_CALL_ID), 1, charString)) {return strError;}}
 else                                                                                             {return sPfx+XML_FIELD_CALL_ID+sSfx;}

//Conference Number
// if (XMLarg.getAttribute(XML_FIELD_CONF_NUMBER))
//  {if(!fLoad_ConferenceData(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_CONF_NUMBER), 1, false, charString)) {return strError;}}
// else                                                                                                               {return sPfx+XML_FIELD_CONF_NUMBER+sSfx;} 
 

/*
 // Trunk  .... loaded before phone number!!!
 if (XMLarg.getAttribute(XML_FIELD_TRUNK))
  {fLoad_Trunk(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_TRUNK), 1, charString );}      

 // Position
 if (XMLarg.getAttribute(XML_FIELD_POSITION))
  {fLoad_Position(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_POSITION), 1, charString ); }

 // PositionsActive
 if (XMLarg.getAttribute(XML_FIELD_POSITIONS_ACTIVE))
  {fLoad_Conference_Positions( WRK, XMLarg.getAttribute(XML_FIELD_POSITIONS_ACTIVE), 1, charString ); }

 // PositionsHistory
 if (XMLarg.getAttribute(XML_FIELD_POSITIONS_HISTORY))
  {fLoad_Conference_Positions( WRK, XMLarg.getAttribute(XML_FIELD_POSITIONS_HISTORY); }

 //PositionsOnHold
 if (XMLarg.getAttribute(XML_FIELD_POSITIONS_ONHOLD))
  {fLoad_OnHold_Positions( WRK, XMLarg.getAttribute(XML_FIELD_POSITIONS_ONHOLD), 1, charString)); }

*/


 // Encoding ALI (optional)
 if (XMLarg.getAttribute(XML_FIELD_ENCODING))
  {if(!fLoad_Encoding(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_ENCODING), 1, charString)) {return strError;} }
 
 // ALIRecord (optional)
 if (XMLarg.getAttribute(XML_FIELD_ALI_RECORD))
  {if(!fLoad_Decode_ALI(XML_DATA_FIELD, WRK, XMLarg.getAttribute(XML_FIELD_ALI_RECORD), 1, charString)) {return strError;} }


/*
 // Callback
 if (XMLarg.getAttribute(XML_FIELD_CALLBACK_NUMBER))
  {if(!fLoad_PhoneNumbers(XML_DATA_FIELD, WRK, 1, XMLarg.getAttribute(XML_FIELD_CALLBACK_NUMBER),"","","",charString)) {return strError;}}
 else                                                                                                               {return sPfx+XML_FIELD_CALLBACK_NUMBER+sSfx;}
*/

/*

 // CallBackVerified
 if (XMLarg.getAttribute(XML_FIELD_CALLBACK_VALIDATED))
  {if(!fLoad_CallbackVerfied(WRK, XMLarg.getAttribute(XML_FIELD_CALLBACK_VALIDATED), charString))     {return strError;}}
 else                                                                                                 {return sPfx+XML_FIELD_CALLBACK_VALIDATED+sSfx;}

 // pANI
 if (XMLarg.getAttribute(XML_FIELD_PSUEDO_ANI))
  {if(!fLoad_pANI(WRK, XMLarg.getAttribute(XML_FIELD_PSUEDO_ANI),charString ) ) {return strError;}}
 else                                                                           {return sPfx+XML_FIELD_PSUEDO_ANI+sSfx;}              
*/



 // TDDmute (ASTERISK only)
 if(enumANI_SYSTEM == ASTERISK)
  { 
   if (XMLarg.getAttribute(XML_FIELD_TDD_MUTE)) {TextData.TDDdata.fLoad_TDDmute(XMLarg.getAttribute(XML_FIELD_TDD_MUTE));}
   else                                         {return sPfx+XML_FIELD_TDD_MUTE+sSfx;}
  }

 // Set ANISytem to Global ignore from workstation.
 enumANISystem =  enumANI_SYSTEM;

 //ANIPort
 if (XMLarg.getAttribute(XML_FIELD_ANI_PORT))
  {if(!fLoad_ANIPort(WRK, XMLarg.getAttribute(XML_FIELD_ANI_PORT), charString ) ) {return strError;}}
 else                                                                             {return sPfx+XML_FIELD_ANI_PORT+sSfx;} 

/*
 //CallState
 if (XMLarg.getAttribute(XML_FIELD_CALL_STATE))
  {if(!fLoad_CallState(WRK, XMLarg.getAttribute(XML_FIELD_CALL_STATE), charString))   {return strError;}}
 else                                                                                 {return sPfx+XML_FIELD_CALL_STATE+sSfx;}
*/
 //CallStatus ... 

 //CallStatusCode ...
/*
 //ALIState
 if (XMLarg.getAttribute(XML_FIELD_ALI_STATE))
  {if(!fLoad_ALIState(WRK, XMLarg.getAttribute(XML_FIELD_ALI_STATE), charString))     {return strError;}}
 else                                                                                 {return sPfx+XML_FIELD_ALI_STATE+sSfx;}
*/   
 return ""; 
}




bool ExperientDataClass::fUpdateGUIXMLMessage()
{
 XMLNode       zMainNode;
 XMLNode       xNode,yNode;
 char*         cptrResponse = NULL;

 // XML Header
 zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
 zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

  // GENERAL
 xNode=zMainNode.addChild(XML_NODE_EVENT);
 xNode.addAttribute(XML_FIELD_PACKET_ID, "");
 xNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 yNode=xNode.addChild(XML_NODE_UPDATE_GUI);

 cptrResponse =  zMainNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 return true;
}

void ExperientDataClass::fCreateDisconnectResponceXML(bool boolResponse)
{
  XMLNode                MainNode, EventNode, ResponseNode;
  string                 strResponse;
  char*                  cptrResponse = NULL;

   // XML Header
  MainNode=XMLNode::createXMLTopNode("xml",TRUE);
  MainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
  MainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
  // Event Tag
  EventNode      = MainNode.addChild(XML_NODE_EVENT);
  EventNode.addAttribute(XML_FIELD_PACKET_ID, (char*)"");
  EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
  // ResponseNode node
  ResponseNode        = EventNode.addChild(XML_NODE_DISCONNECT_RESPONSE);
  if (boolResponse){
   ResponseNode.addAttribute(XML_FIELD_DISCONNECT_SUCCESSFUL, XML_VALUE_TRUE); 
  }
  else {
   ResponseNode.addAttribute(XML_FIELD_DISCONNECT_SUCCESSFUL, XML_VALUE_FALSE); 
  }
  ResponseNode.addAttribute(XML_FIELD_CALL_ID, this->CallData.stringUniqueCallID.c_str()); 
  ResponseNode.addAttribute(XML_FIELD_POSITION, this->CallData.stringPosn.c_str());  

  cptrResponse = MainNode.createXMLString();
  if (cptrResponse != NULL)
   {
    this->stringXMLString = cptrResponse;
    free(cptrResponse);
   }
  cptrResponse = NULL;

 // //cout << this->stringXMLString << endl;
 this->boolBroadcastXMLtoSinglePosition = true;
 return;
} 




bool ExperientDataClass::fClear_Grid_XML_Message(int intPosition)
{
 XMLNode       zMainNode;
 XMLNode       xNode,yNode;
 string        stringPosition;
 char*         cptrResponse = NULL;

 stringPosition = int2strLZ(intPosition);

 // XML Header
 zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
 zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

  // GENERAL
 xNode=zMainNode.addChild(XML_NODE_EVENT);
 xNode.addAttribute(XML_FIELD_PACKET_ID, "");
 xNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 yNode=xNode.addChild(XML_NODE_CLEAR_CONSOLE );
 yNode.addAttribute(XML_FIELD_POSITION, stringPosition.c_str());

 cptrResponse =  zMainNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 return true;
}

string ExperientDataClass::fXML_MessageDeleteRecord()
{
 XMLNode       zMainNode;
 XMLNode       xNode,yNode;
 char*         cptrResponse = NULL;

 // XML Header
 zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
 zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

  // GENERAL
 xNode=zMainNode.addChild(XML_NODE_EVENT);
 xNode.addAttribute(XML_FIELD_PACKET_ID, "");
 xNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 yNode=xNode.addChild(XML_NODE_DELETE_RECORD);
 yNode.addAttribute(XML_FIELD_U_CALL_ID, CallData.stringUniqueCallID.c_str());

 cptrResponse =  zMainNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;


 return stringXMLString;
}

bool ExperientDataClass::fALI_ServiceBid_XML_Message()
{
 XMLNode       ALIDataNode, xNode,yNode;
 char*         cptrResponse = NULL;
 string        strBidIndex;
 string        strValue;

 strBidIndex = int2str(ALIData.intALIBidIndex);
 
 if ((CallData.stringTenDigitPhoneNumber.empty())&&(CallData.stringPANI.empty())) {return false;}

 ALIDataNode = XMLNode::createXMLTopNode("xml",TRUE);
 ALIDataNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 ALIDataNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

 xNode = ALIDataNode.addChild(XML_NODE_EVENT);
 xNode.addAttribute(XML_FIELD_PACKET_ID, strBidIndex.c_str());
 yNode=xNode.addChild(XML_NODE_SEND_ALI_BID);

 yNode.addAttribute(XML_FIELD_U_CALL_ID, CallData.stringUniqueCallID.c_str());

 if (CallData.stringPANI.empty()) { yNode.addAttribute(XML_FIELD_PSUEDO_ANI, CallData.stringTenDigitPhoneNumber.c_str());}
 else                             { yNode.addAttribute(XML_FIELD_PSUEDO_ANI, CallData.stringPANI.c_str());}

 yNode.addAttribute(XML_FIELD_TRUNK, CallData.stringTrunk.c_str());
 if (CallData.boolWirelessVoipCall)    {strValue = XML_VALUE_TRUE;}
 else                                  {strValue = XML_VALUE_FALSE;}
 yNode.addAttribute(XML_FIELD_WIRELESS_CALL, strValue.c_str());
 if (CallData.boolLandLineCall)        {strValue = XML_VALUE_TRUE;}
 else                                  {strValue = XML_VALUE_FALSE;}
 yNode.addAttribute(XML_FIELD_LANDLINE_CALL, strValue.c_str());
 if (ALIData.enumALIBidType == MANUAL) {strValue = XML_VALUE_TRUE;}
 else                                  {strValue = XML_VALUE_FALSE;}
 yNode.addAttribute(XML_FIELD_MANUAL_BID, strValue.c_str());
 
 cptrResponse =  ALIDataNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 return true;
}


bool ExperientDataClass::fTDDdata_XML_Tag(XMLNode*  TDDdataNode, int intTDDcode)
{
 string strEncodedTDDmessage;

 if (intTDDcode) {TDDdataNode->addAttribute(XML_FIELD_TDD_ENCODING, XML_VALUE_BASE_64);}
 else            {return false;}

 switch (intTDDcode)
  {
   case 1: // (boolWithTDDCharacter)
          strEncodedTDDmessage = EncodeBase64(this->TextData.TDDdata.strTDDcharacter);
          TDDdataNode->addAttribute(XML_FIELD_TDD_SEND_CHARACTER, strEncodedTDDmessage.c_str());
          break;
  case 2: // (boolWithTDDdispatcher)
          strEncodedTDDmessage = EncodeBase64(this->TextData.TDDdata.strTDDstring);
          TDDdataNode->addAttribute(XML_FIELD_TDD_DISPATCHER_SAYS, strEncodedTDDmessage.c_str());
          break;
  case 3: // (boolWithTDDcaller) 
      //    //cout << "CALLER" << endl;
          strEncodedTDDmessage = EncodeBase64(this->TextData.TDDdata.strTDDstring);
          TDDdataNode->addAttribute(XML_FIELD_TDD_CALLER_SAYS, strEncodedTDDmessage.c_str());
          break;
  case 4: // SMS Message (this is for testing ........)
       //   //cout << "SMS" << endl;
          strEncodedTDDmessage = EncodeBase64(this->TextData.SMSdata.strSMSmessage);
          TDDdataNode->addAttribute(XML_FIELD_TDD_CALLER_SAYS, strEncodedTDDmessage.c_str());
          break;
  case 5:
          strEncodedTDDmessage = EncodeBase64(this->TextData.TDDdata.strTDDstring);
          TDDdataNode->addAttribute(XML_FIELD_TDD_DISPATCHER_SAYS, strEncodedTDDmessage.c_str());
          break;
  default: return false;
 }


 return true;
}

bool ExperientDataClass::fTextData_XML_Tag(XMLNode*  TextDataNode, int intMessagecode)
{
 string strEncodedTextmessage;
// //cout << "text Tag messagecode ->" << intMessagecode << endl;
 switch (TextData.TextType)
  {
   case NO_TEXT_TYPE:
       // SendCodingError("NO TEXT TYPE IN fn ExperientDataClass::fTextData_XML_Tag()"); 
  //      //cout << "no text type" << endl;
        return false;
        break;
   case TDD_MESSAGE:
        TextDataNode->addAttribute(XML_FIELD_TEXT_TYPE, "TDD");
        break;
   case MSRP_MESSAGE:
        TextDataNode->addAttribute(XML_FIELD_TEXT_TYPE, "MSRP");
        break;
  }

 if (intMessagecode) {TextDataNode->addAttribute(XML_FIELD_TEXT_ENCODING, XML_VALUE_BASE_64);}
 else                {return false;}


 switch (intMessagecode)
  {
   case 1: // (boolWithTDDCharacter)
          strEncodedTextmessage = EncodeBase64(TextData.TDDdata.strTDDcharacter);
          TextDataNode->addAttribute(XML_FIELD_TEXT_SEND_CHARACTER, strEncodedTextmessage.c_str());
          // Do we add the conversation here ??????
          break;
  case 2: // (boolWithTDDdispatcher)
          strEncodedTextmessage = EncodeBase64(TextData.TDDdata.strTDDstring);
          TextDataNode->addAttribute(XML_FIELD_TEXT_DISPATCHER_SAYS, strEncodedTextmessage.c_str());
          strEncodedTextmessage = EncodeBase64(TextData.strConversation);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CONVERSATION , strEncodedTextmessage.c_str());
          break;
  case 3: // (boolWithTDDcaller) 
          strEncodedTextmessage = EncodeBase64(TextData.TDDdata.strTDDstring);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CALLER_SAYS, strEncodedTextmessage.c_str());
          strEncodedTextmessage = EncodeBase64(TextData.strConversation);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CONVERSATION , strEncodedTextmessage.c_str());
          break;
  case 4: // SMS Message
          strEncodedTextmessage = EncodeBase64(TextData.SMSdata.strSMSmessage);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CALLER_SAYS, strEncodedTextmessage.c_str());
          strEncodedTextmessage = EncodeBase64(TextData.strConversation);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CONVERSATION , strEncodedTextmessage.c_str());
          break;
  case 5:  
          strEncodedTextmessage = EncodeBase64(TextData.TDDdata.strTDDstring);
          TextDataNode->addAttribute(XML_FIELD_TEXT_DISPATCHER_SAYS, strEncodedTextmessage.c_str());
          strEncodedTextmessage = EncodeBase64(TextData.strConversation);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CONVERSATION , strEncodedTextmessage.c_str());
          break;
  case 6:
          //End of Conversation
          strEncodedTextmessage = EncodeBase64(TextData.strConversation);
          TextDataNode->addAttribute(XML_FIELD_TEXT_CONVERSATION , strEncodedTextmessage.c_str());
          break;
  default: return false;
 }


 return true;
}

bool ExperientDataClass::fStreamInfo_XML_Tag(XMLNode*  StreamInfoNode)
{
 XMLNode 	StreamIDnode;
 Stream_ID 	objStreamID;

 if (objStreamID.fLoadStreamID(this->CallData.strCallRecording))
  {
   StreamIDnode = StreamInfoNode->addChild(XML_NODE_STREAM_ID);
   StreamIDnode.addAttribute(XML_FIELD_ID, objStreamID.strUUID.c_str());
   StreamIDnode.addAttribute(XML_FIELD_EPOCH, objStreamID.strEpoch.c_str());
   StreamIDnode.addAttribute(XML_FIELD_CALLER, objStreamID.strCallerID.c_str());
  }
 else {return false;}

 return true;
}

bool ExperientDataClass::fPSAPappletData_XML_Message(string strIndex)
{
 XMLNode        MainNode, EventNode, PSAPStatusNode;
 string         strReturnString = "";
 char*          cptrResponse = NULL;

 MainNode = XMLNode::createXMLTopNode("xml",TRUE);
 MainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 MainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

 EventNode = MainNode.addChild(XML_NODE_EVENT);
 EventNode.addAttribute(XML_FIELD_PACKET_ID, strIndex.c_str());
 EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 PSAPStatusNode = EventNode.addChild(XML_NODE_PSAP_APPLET_DATA);

 if(!fPSAPappletData_XML_Tag(&PSAPStatusNode)) {return false;};
 
 cptrResponse =  MainNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 return true;
}

bool ExperientDataClass::fPSAPappletData_XML_Tag(XMLNode*  PSAPappletDataNode)
{
 size_t 	sz;
 XMLNode        PSAPStatusNode;
 if (vPSAPstatus.empty()) {return false;}
 sz = vPSAPstatus.size();
 
 for (unsigned int i = 0; i< sz; i++)
  {
   PSAPStatusNode = PSAPappletDataNode->addChild(XML_NODE_PSAP_DATA);
   PSAPStatusNode.addAttribute(XML_FIELD_NAME, vPSAPstatus[i].strPSAPname.c_str());
   PSAPStatusNode.addAttribute(XML_FIELD_POSITIONS_ONLINE, int2str(vPSAPstatus[i].iPositionsOnline).c_str());
   PSAPStatusNode.addAttribute(XML_FIELD_POSITIONS_ON_911_CALL, int2str(vPSAPstatus[i].iPositionsOnEmergencyCall).c_str());
   PSAPStatusNode.addAttribute(XML_FIELD_POSITIONS_ON_ADMIN_CALL, int2str(vPSAPstatus[i].iPositionsOnAdminCall).c_str());
  }
 return true;
}

bool ExperientDataClass::fWindowData_XML_Tag(XMLNode*  WindowDataNode)
{
  extern Trunk_Type_Mapping     TRUNK_TYPE_MAP;

  if(WindowButtonData.boolTransferWindow)
   {
    WindowDataNode->addAttribute(XML_FIELD_TRANSFER_WINDOW, XML_VALUE_ENABLE);



    switch (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType)
     {
      case CAMA: case LANDLINE: case WIRELESS: case NG911_SIP: case INDIGITAL_WIRELESS: case INTRADO: case REFER:
              WindowDataNode->addAttribute(XML_FIELD_TANDEM_XFER_BUTTON, XML_VALUE_ON);
              if (bool911_TRANSFER_TANDEM_ONLY)
               {
                WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_OFF);
                WindowDataNode->addAttribute(XML_FIELD_CONF_XFER_BUTTON, XML_VALUE_OFF);
                WindowDataNode->addAttribute(XML_FIELD_BLIND_XFER_BUTTON, XML_VALUE_OFF);
               }
              else
               {
                if(boolUSE_POLYCOM_CTI) {WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_ON);}
                else                    {WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_OFF);}
                WindowDataNode->addAttribute(XML_FIELD_CONF_XFER_BUTTON, XML_VALUE_ON);
                WindowDataNode->addAttribute(XML_FIELD_BLIND_XFER_BUTTON, XML_VALUE_ON);
               }
              break;
      case CLID:
              WindowDataNode->addAttribute(XML_FIELD_FLASH_XFER_BUTTON, XML_VALUE_ON);
              WindowDataNode->addAttribute(XML_FIELD_TANDEM_XFER_BUTTON, XML_VALUE_OFF); 
             if(boolUSE_POLYCOM_CTI) {WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_ON);}
             else                    {WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_OFF);}
             WindowDataNode->addAttribute(XML_FIELD_CONF_XFER_BUTTON, XML_VALUE_ON);
             WindowDataNode->addAttribute(XML_FIELD_BLIND_XFER_BUTTON, XML_VALUE_ON);
             break;
      case MSRP_TRUNK:
              WindowDataNode->addAttribute(XML_FIELD_TANDEM_XFER_BUTTON, XML_VALUE_OFF);
              WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_OFF);
              WindowDataNode->addAttribute(XML_FIELD_CONF_XFER_BUTTON, XML_VALUE_OFF);
              WindowDataNode->addAttribute(XML_FIELD_BLIND_XFER_BUTTON, XML_VALUE_OFF);
              WindowButtonData.boolALIRebidButton = true;
              break;
      default:
              WindowDataNode->addAttribute(XML_FIELD_TANDEM_XFER_BUTTON, XML_VALUE_OFF); 
             if(boolUSE_POLYCOM_CTI) {WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_ON);}
             else                    {WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_OFF);}
             WindowDataNode->addAttribute(XML_FIELD_CONF_XFER_BUTTON, XML_VALUE_ON);
             WindowDataNode->addAttribute(XML_FIELD_BLIND_XFER_BUTTON, XML_VALUE_ON);
             break; 
     }// end switch 
      
   }
  else                                     
  {
   WindowDataNode->addAttribute(XML_FIELD_TRANSFER_WINDOW, XML_VALUE_DISABLE);
   WindowDataNode->addAttribute(XML_FIELD_TANDEM_XFER_BUTTON, XML_VALUE_OFF);
   WindowDataNode->addAttribute(XML_FIELD_ATT_XFER_BUTTON, XML_VALUE_OFF);
   WindowDataNode->addAttribute(XML_FIELD_CONF_XFER_BUTTON, XML_VALUE_OFF);
   WindowDataNode->addAttribute(XML_FIELD_BLIND_XFER_BUTTON, XML_VALUE_OFF);
   WindowDataNode->addAttribute(XML_FIELD_FLASH_XFER_BUTTON, XML_VALUE_OFF);
  }
//TextData.TDDdata.boolTDDWindowActive
//TextData.TDDdata.fSetTDDSendButton()
//######################################################################################################################
//REMOVE LATER ......................need to resolve that TDD is active on all connected calls .........
  if (TextData.boolTextWindowActive||TextData.TDDdata.boolTDDWindowActive)        {WindowDataNode->addAttribute(XML_FIELD_TDD_WINDOW, XML_VALUE_ENABLE);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_TDD_WINDOW, XML_VALUE_DISABLE);}

  if (TextData.fSetTextSendButton()||TextData.TDDdata.fSetTDDSendButton())        {WindowDataNode->addAttribute(XML_FIELD_TDD_SEND_BUTTON, XML_VALUE_ON);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_TDD_SEND_BUTTON, XML_VALUE_OFF);}
//######################################################################################################################
  if (TextData.boolTextWindowActive)        {WindowDataNode->addAttribute(XML_FIELD_TEXT_WINDOW, XML_VALUE_ENABLE);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_TEXT_WINDOW, XML_VALUE_DISABLE);}

  if (TextData.fSetTextSendButton())        {WindowDataNode->addAttribute(XML_FIELD_TEXT_SEND_BUTTON, XML_VALUE_ON);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_TEXT_SEND_BUTTON, XML_VALUE_OFF);}

  if((CallData.intPosn == 0)&&(intCallStateCode > 1)&&(this->boolAbandoned) )
                                            {WindowDataNode->addAttribute(XML_FIELD_TAKE_CONTROL_BUTTON, XML_VALUE_ON);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_TAKE_CONTROL_BUTTON, XML_VALUE_OFF);}

  if (WindowButtonData.boolCADSendButton)   {WindowDataNode->addAttribute(XML_FIELD_CAD_BUTTON, XML_VALUE_ON);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_CAD_BUTTON, XML_VALUE_OFF);}

  if (WindowButtonData.boolALIRebidButton) {WindowDataNode->addAttribute(XML_FIELD_ALI_BUTTON, XML_VALUE_ON);}
  else                                     {WindowDataNode->addAttribute(XML_FIELD_ALI_BUTTON, XML_VALUE_OFF);}

  this->WindowButtonData.fSetRapidLiteButton(ValidTenDigitNumber(this->CallData.stringTenDigitPhoneNumber)); 
  if (WindowButtonData.boolRapidLiteButton) {WindowDataNode->addAttribute(XML_FIELD_RAPID_LITE_BUTTON, XML_VALUE_ON);}
  else                                      {WindowDataNode->addAttribute(XML_FIELD_RAPID_LITE_BUTTON, XML_VALUE_OFF);}
return true;

}

PhoneBookEntry ExperientDataClass::CallerIDandNamefromConference()
{
 vector <Channel_Data>::size_type   sz;
 PhoneBookEntry                     objReturn;
 bool                               boolSet = false;
 string                             strLegend;
 unsigned int                       i =0;
 sz = CallData.ConfData.vectConferenceChannelData.size();

 
 while ((i < sz)&&(!boolSet))
  {
   if(!CallData.ConfData.vectConferenceChannelData[i].boolConnected) {i++; continue;}
   else                                                              {boolSet = true;}
   strLegend = CallData.ConfData.vectConferenceChannelData[i].strConfDisplay;
   if (!ExtensionFromConferenceDisplay(strLegend).empty())
    {
     objReturn.strCallerID = ExtensionFromConferenceDisplay(strLegend);
     objReturn.strCustName = "PSAP " + stringPSAP_Name;
     boolSet = true;
    }
   else if (IsCallerInConferenceDisplay(strLegend))
    {
     if (CallData.intCallbackNumber)           { objReturn.strCallerID = CallData.fTenDigitCallbackGUIformat();}
     else                                      { objReturn.strCallerID = fCallerIDGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCallerID);}
     if (objReturn.strCallerID.empty())        { objReturn.strCallerID = fCallerIDGUIformat(CallData.stringPANI);}
     objReturn.strCustName = CustNameGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCustName,false);
    }
   else
    {
     objReturn.strCallerID = fCallerIDGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCallerID);
     objReturn.strCustName = CustNameGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCustName,false);  
    }
  }

 return objReturn;
}

bool ExperientDataClass::fConferenceData_XML_Tag(XMLNode*  ConferenceDataNode)
{

 vector <Channel_Data>::size_type   sz;
 XMLNode                            WorkingNode;
 string                             strLegend;
 string                             strCallerID;
 string                             strCustName;

 sz = CallData.ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < sz; i++)
  { 
   WorkingNode =  ConferenceDataNode->addChild(XML_NODE_CONFERENCE_MEMBER);

   strLegend = CallData.ConfData.vectConferenceChannelData[i].strConfDisplay;

 //  if (!ExtensionFromConferenceDisplay(strLegend).empty())
  //  {
 //    strCallerID = ExtensionFromConferenceDisplay(strLegend);
 //    strCustName = "PSAP " + stringPSAP_Name;
   
//    }

    if (IsCallerInConferenceDisplay(strLegend))
    {
     if (CallData.intCallbackNumber) { strCallerID = CallData.fTenDigitCallbackGUIformat();}
     else                            { strCallerID = fCallerIDGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCallerID);}
     if (strCallerID.empty())        { strCallerID = fCallerIDGUIformat(CallData.stringPANI);}
     strCustName = CustNameGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCustName,false);
    }
   else
    {
     strCallerID = fCallerIDGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCallerID);
     strCustName = CustNameGUIformat(CallData.ConfData.vectConferenceChannelData[i].strCustName,false);  
    }

   WorkingNode.addAttribute(XML_FIELD_LEGEND, strLegend.c_str());
   WorkingNode.addAttribute(XML_FIELD_CALLBACK_NUMBER , strCallerID.c_str());
   WorkingNode.addAttribute(XML_FIELD_NAME , strCustName.c_str());


   if( CallData.ConfData.vectConferenceChannelData[i].boolConnected)
    { WorkingNode.addAttribute(XML_FIELD_CONNECTED , XML_VALUE_TRUE);}
   else
    { WorkingNode.addAttribute(XML_FIELD_CONNECTED , XML_VALUE_FALSE);}

   if( CallData.ConfData.vectConferenceChannelData[i].boolOnHold)
    { WorkingNode.addAttribute(XML_FIELD_ONHOLD , XML_VALUE_TRUE);}
   else
    { WorkingNode.addAttribute(XML_FIELD_ONHOLD , XML_VALUE_FALSE);}

   if( CallData.ConfData.vectConferenceChannelData[i].boolParked){ 
    WorkingNode.addAttribute(XML_FIELD_CALLPARK , XML_VALUE_TRUE);
    WorkingNode.addAttribute(XML_FIELD_CALLPARK_LOT , CallData.ConfData.vectConferenceChannelData[i].strValetExtension.c_str());
   }
   else { WorkingNode.addAttribute(XML_FIELD_CALLPARK , XML_VALUE_FALSE);
   }


  }
/*
  sz = CallData.vTransferData.size();
  for (unsigned int i = 0; i < sz; i++)
   {
    WorkingNode =  ConferenceDataNode->addChild(XML_NODE_CONFERENCE_MEMBER);   
    WorkingNode.addAttribute(XML_FIELD_LEGEND, CallData.vTransferData[i].strConfDisplay.c_str());
    WorkingNode.addAttribute(XML_FIELD_CALLBACK_NUMBER , CallData.vTransferData[i].strTransfertoNumber.c_str());
    WorkingNode.addAttribute(XML_FIELD_NAME , CallData.vTransferData[i].strTransferName.c_str());
    WorkingNode.addAttribute(XML_FIELD_CONNECTED , XML_VALUE_FALSE);
    WorkingNode.addAttribute(XML_FIELD_ONHOLD , XML_VALUE_FALSE);

   }
*/

 return true;

}

bool ExperientDataClass::fLoadCallOriginator(conference_member eConf, int i)
{
 strCallOriginator.clear();
 switch (eConf)
  {
   case POSITION_CONF:
         if (!i) {return false;}
         strCallOriginator += ANI_CONF_MEMBER_POSITION_PREFIX; 
         strCallOriginator+= int2str(i);
        break;
   case CALLER_CONF:
        if (!i) {strCallOriginator += ANI_CONF_MEMBER_CALLER_PREFIX ;}
        else    {strCallOriginator += ANI_CONF_MEMBER_CALLER_PREFIX ; strCallOriginator += int2str(i);}
        break;
   default: return false;
        ;
  }
 return true;
}

void   ExperientDataClass::fSetANIfunction(ani_functions eANIfunction) {
 this->enumANIFunction = eANIfunction;
 this->CallData.eJSONEventCode = eANIfunction;
}      

void   ExperientDataClass::fSetANIfunction(ani_functions eANIfunction,transfer_method eTransfer_Method  ) {

 this->enumANIFunction = eANIfunction;
 this->CallData.eJSONEventCode = eANIfunction;
 this->CallData.eJSONEventTransferMethod = eTransfer_Method; 
}      

void   ExperientDataClass::fSetANIfunction(ani_functions eANIfunction,string strUUID ) {

 this->enumANIFunction = eANIfunction;
 this->CallData.eJSONEventCode = eANIfunction;
 this->CallData.strJSONEventMergedCallID = strUUID; 
}      

bool ExperientDataClass::ShowRingDialogBox() {

extern int 				intSHOW_RING_DIALOG;
extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;

/*
#                               0: Disabled
#                               1: SMS Only
#                               2: 911 Only
#				4: Admin Only
*/

 if (intSHOW_RING_DIALOG == 0) { return false;}

 if (intSHOW_RING_DIALOG > 7) {
  SendCodingError("ExperientDataClass.cpp - coding error ExperientDataClass::ShowRingDialogBox() > 7");        
  return false;
 }

 switch (TRUNK_TYPE_MAP.Trunk[this->CallData.intTrunk].TrunkType) {
                    
  case CLID:  case SIP_TRUNK:
       switch (intSHOW_RING_DIALOG) {
        case 1: case 2: case 3:         return false;
        case 4: case 5: case 6: case 7: return true;
        default:
         return false;
       }                        
       break;
  case MSRP_TRUNK:
       switch (intSHOW_RING_DIALOG) {
        case 1: case 3: case 5: case 7: return true;
        case 2: case 4: case 6:         return false;
        default:
         return false;
       }            
       break;
  case NG911_SIP: case REFER: case CAMA: case INDIGITAL_WIRELESS: case LANDLINE: case WIRELESS: case INTRADO:
       switch (intSHOW_RING_DIALOG) {
        case 2: case 3: case 6: case 7: return true;
        case 1: case 4: case 5:         return false;
        default:
         return false;
       }            
       break;
  default:
       SendCodingError("ExperientDataClass.cpp - Un-programmed trunk type error ExperientDataClass::ShowRingDialogBox() ");
       break;
 }
 return false;
}


bool ExperientDataClass::fCallData_XML_Tag(XMLNode*  CallDataNode, int StatusCode, bool boolWithALI) {
  string                	stringTemp;
  string                	stringRingDateTime ="";
  string                	stringEventDateTime ="";
  string                	stringTransmitDateTime ="";
  string                	stringStatusCode ="";
  string                        stringStatus ="";
  string                        stringEncodedALI ="";
  string                        strEncodedTDDmessage ="";
  MessageClass          	objMessage;
  string                        strOutput;
  string                        strFormattedCustName ="";
  bool                          boolA, boolB, boolC, boolD;
  bool                          boolE = false;
  Stream_ID                     objStreamID;

  extern string			strALI_BID_IN_PROGRESS_MESSAGE;
  extern string                 strALI_NOT_ALLOWED_MESSAGE;

  if    (CallData.stringPosn == "") {stringTemp = "00";}
  else                              {stringTemp = CallData.stringPosn;}

  //
  stringStatusCode = int2strLZ(StatusCode);
  
  // set basic call status may be changed later in switch ...
  stringStatus = CallStateString(this->intCallStateCode);

  switch (StatusCode)
   { 

    case 11: case 12: case 87:
          // RINGING, DIALING, MAN_BID
          stringEventDateTime.append(stringRingingTimeStamp,0,27);break;

    case 1: 
          // CONNECT, ABANDONED
          stringEventDateTime.append(stringConnectTimeStamp,0,27);break;
    
    case 2:
          // DISCONNECT
          stringEventDateTime.append(stringDisConnectTimeStamp,0,27);break;
    case 5: case 6:  
          // ONHOLD, OFFHOLD, TDD_MUTE TDD_UNMUTE TDD_RECEIVED,  SEND2CAD, CONFJOIN, CONFLEAVE
          stringStatus = CallStatusString(StatusCode);
          stringEventDateTime.append(stringTimeOfEvent,0,27); break;

    case 7: //Abandoned
          stringEventDateTime.append(stringConnectTimeStamp,0,27);
          break;
    case 76: //Ringback
          stringStatus = CallStatusString(11);
          stringStatusCode = int2strLZ(11);
          intCallStateCode = 1;
         break;      
    case 83: case 97: case 98: case 99:
           // TDD MUTE CHANGE, SEND2CAD, CONFJOIN, CONFLEAVE
           stringEventDateTime.append(stringTimeOfEvent,0,27); break;
    case 88: case 89: case 90:
          // MAN ALI FAIL, AUTO ALI REQ, AUTO ALI FAIL
          stringEventDateTime.append(stringTimeOfEvent,0,27); break;
    case 84: case 85: case 86: 
          // TDD DispatcherSays TDD CallerSays TDD_CHAR_RECEIVED SMS_MSG_RECEIVED
          stringEventDateTime.append(stringTimeOfEvent,0,27); break;
    case 77: case 78: // SMS data sent received
          stringEventDateTime.append(stringTimeOfEvent,0,27);
          stringStatus ="Text Message";
          break;
    case 91:
          // ALI REBID
          stringEventDateTime.append(ALIData.stringALIRebidTimeStamp,0,27); break;
    case 92:
          // ALI TIMEOUT
          stringEventDateTime.append(ALIData.stringALITimeoutTimeStamp,0,27); break;
    case 93: case 94: case 95:
          // ALIRECEIVE, MANALIRECEIVE, AUTOALIRECEIVE, 
          stringEventDateTime.append(ALIData.stringALIReceivedTimeStamp,0,27); break;
    case 96:
         // ALI NO UPDATE (convert StatusCode to present call state)
         StatusCode = ConvertCallStateToStatus(intCallStateCode);
         stringStatusCode = int2strLZ(StatusCode);
         stringStatus = CallStateString(intCallStateCode);
         stringEventDateTime.append(ALIData.stringALIReceivedTimeStamp,0,27); break;
    case 60:
         //ANI UPDATE (convert StatusCode to present call state)
         StatusCode = ConvertCallStateToStatus(intCallStateCode);
         stringStatusCode = int2strLZ(StatusCode);
         stringStatus = CallStateString(intCallStateCode);
         stringEventDateTime.append(stringTimeOfEvent,0,27);break;
    default:
         SendCodingError("ExperientDataClass.cpp - Coding Error in ExperientDataClass:::fCallData_XML_Tag()"); return false;  

   }
 // stringStatus+=charLF;
 // stringStatus.append(ALIData.fALIStateString());

  if      (boolSMSmessage&& (StatusCode==2))        {stringStatus ="Text Message Disconnected";}
  else if (boolSMSmessage&& (intCallStateCode == 1)){stringStatus ="Text Message Ringing";}
  else if (boolSMSmessage&& (intCallStateCode == 2)){stringStatus ="Text Message Connected";}
  else if (boolSMSmessage&& (StatusCode == 7))      {stringStatus ="Text Message Abandoned";}
  else if (boolSMSmessage)                          {stringStatus ="Text Message";}
  
  if (this->CallData.ConfData.strParkTime.length() ) {stringStatus ="Call Parked\nLot "+ CallData.ConfData.strParkLot;}

  stringRingDateTime.append(stringRingingTimeStamp,0,27);  
  objMessage.fMessage_Create(0,0, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                             objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "");
  stringTransmitDateTime.append(objMessage.stringTimeStamp,0,27); 

  if (this->ShowRingDialogBox()) {
     CallDataNode->addAttribute(XML_FIELD_SHOW_RING_DIALOG, XML_VALUE_TRUE);
  }
  else {
     CallDataNode->addAttribute(XML_FIELD_SHOW_RING_DIALOG, XML_VALUE_FALSE);
  }
  CallDataNode->addAttribute(XML_FIELD_RING_DATE_TIME,stringRingDateTime.c_str());
  CallDataNode->addAttribute(XML_FIELD_CONNECTED_DATE_TIME,stringConnectTimeStamp.c_str());
  CallDataNode->addAttribute(XML_FIELD_EVENT_DATE_TIME ,stringEventDateTime.c_str());
  CallDataNode->addAttribute(XML_FIELD_TRANSMIT_DATE_TIME ,stringTransmitDateTime.c_str());
  CallDataNode->addAttribute(XML_FIELD_CALL_ID, int2str(CallData.intUniqueCallID).c_str());
 
  CallDataNode->addAttribute(XML_FIELD_CONF_NUMBER, CallData.strConferenceNumber.c_str());
  CallDataNode->addAttribute(XML_FIELD_TRUNK, int2str(CallData.intTrunk).c_str()); 
  CallDataNode->addAttribute(XML_FIELD_POSITION, stringTemp.c_str());
  CallDataNode->addAttribute(XML_FIELD_CALL_ORIGINATOR, strCallOriginator.c_str());
  CallDataNode->addAttribute(XML_FIELD_POSITIONS_ACTIVE, CallData.ConfData.strActiveConferencePositions.c_str());
  CallDataNode->addAttribute(XML_FIELD_POSITIONS_HISTORY, CallData.ConfData.strHistoryConferencePositions.c_str());
  CallDataNode->addAttribute(XML_FIELD_POSITIONS_ONHOLD, CallData.ConfData.strOnHoldPositions.c_str());
  CallDataNode->addAttribute(XML_FIELD_TIMESTAMPS_ONHOLD, CallData.ConfData.fOnHoldTimeStampsStrippedOfPositions().c_str());
  CallDataNode->addAttribute(XML_FIELD_POSITION_PARKED, CallData.ConfData.strParkParticipant.c_str());
  CallDataNode->addAttribute(XML_FIELD_PARKED_TIME, CallData.ConfData.strParkTime.c_str());
  CallDataNode->addAttribute(XML_FIELD_ALI_STATE, int2strLZ(ALIData.intALIState).c_str());
  if ((boolWithALI)&&(ALIData.stringAliText.length()))   { 
    stringEncodedALI = EncodeBase64(ALIData.stringAliText);
    if (stringEncodedALI.length() == 0){return false;}
    CallDataNode->addAttribute( XML_FIELD_ALI_RECORD_ENCODING , XML_VALUE_BASE_64);
    CallDataNode->addAttribute(XML_FIELD_ALI_RECORD, stringEncodedALI.c_str());
  }
  else {
    switch (ALIData.intALIState)    {
      case 79 :     // XML_STATUS_CODE_ALI_BID_NOT_ALLOWED:
           stringEncodedALI = EncodeBase64(strALI_NOT_ALLOWED_MESSAGE);
           CallDataNode->addAttribute( XML_FIELD_ALI_RECORD_ENCODING , XML_VALUE_BASE_64);
           CallDataNode->addAttribute(XML_FIELD_ALI_RECORD, stringEncodedALI.c_str());
           break;
      case 87: case 91:
           // Leave out 89 auto ALI
           stringEncodedALI = EncodeBase64(strALI_BID_IN_PROGRESS_MESSAGE);
           CallDataNode->addAttribute( XML_FIELD_ALI_RECORD_ENCODING , XML_VALUE_BASE_64);
           CallDataNode->addAttribute(XML_FIELD_ALI_RECORD, stringEncodedALI.c_str());               
           break;
      case  88: case 90: case 92: 
            //90: XML_STATUS_MSG_AUTO_ALI_TIMEOUT 92: XML_STATUS_MSG_ALI_TIMEOUT : 88: XML_STATUS_MSG_MAN_ALI_TIMEOUT:
           stringEncodedALI = EncodeBase64(strALI_FAILED_MESSAGE);
           CallDataNode->addAttribute( XML_FIELD_ALI_RECORD_ENCODING , XML_VALUE_BASE_64);
           CallDataNode->addAttribute(XML_FIELD_ALI_RECORD, stringEncodedALI.c_str());
           break;
      case 81:
           //XML_STATUS_CODE_ALI_NO_GEOLOCATION_URI
           stringEncodedALI = EncodeBase64(strALI_NO_GEOLOCATION_URI);
           CallDataNode->addAttribute( XML_FIELD_ALI_RECORD_ENCODING , XML_VALUE_BASE_64);
           CallDataNode->addAttribute(XML_FIELD_ALI_RECORD, stringEncodedALI.c_str());
           break;
      default:
           break;
   }
  }

  if (CheckforManualBidTrunk(CallData.intTrunk))                     {CallData.strCallTypeDisplay = "Manual Bid";}
  if (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType==NG911_SIP)  {CallData.strCallTypeDisplay = "9-1-1";}
  if (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType==REFER)      {CallData.strCallTypeDisplay = "9-1-1";}

  CallDataNode->addAttribute(XML_FIELD_CALL_TYPE, CallData.strCallTypeDisplay.c_str());


  if ((!this->CallData.fNumPositionsInConference())&&(this->boolConnect)) {
   // Supress lineview if call transfered away 
   CallDataNode->addAttribute(XML_FIELD_LINE_NUMBER, "0");
  }
  else if (this->FreeswitchData.objRing_Dial_Data.iGUIlineView) {
   CallDataNode->addAttribute(XML_FIELD_LINE_NUMBER, int2str(this->FreeswitchData.objRing_Dial_Data.iGUIlineView).c_str());
  }
  else {
   CallDataNode->addAttribute(XML_FIELD_LINE_NUMBER, int2str(this->FreeswitchData.objRing_Dial_Data.iLineNumber).c_str());
  }

  
  if       (CallData.intCallbackNumber)        {
   stringTemp = CallData.fTenDigitCallbackGUIformat();
   CallDataNode->addAttribute(XML_FIELD_CALLBACK_NUMBER, int2str(CallData.intCallbackNumber).c_str());
   boolE=true;
  }
  else if (!CallData.strSIPphoneNumber.empty())          {
   if ( SIPnumberhasAllNumbers(CallData.strSIPphoneNumber)) {stringTemp = RemoveIPaddressFromPhoneNumber(CallData.strSIPphoneNumber);}
   else                                                     {stringTemp = CallData.strSIPphoneNumber;}
   CallDataNode->addAttribute(XML_FIELD_CALLBACK_NUMBER, stringTemp.c_str()); 
   boolE=true;
  }
  else if (!CallData.stringPANI.empty())       {
   stringTemp = fCallerIDGUIformat(CallData.stringPANI); 
   CallDataNode->addAttribute(XML_FIELD_CALLBACK_NUMBER, CallData.stringPANI.c_str());
   boolE=true;
  }
  else                                         {
   stringTemp = CallData.strCallBackDisplay;
  }

  boolA = CallData.fIsOutboundCall();
  boolB = CheckforManualBidTrunk(CallData.intTrunk);
  boolC = (TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType==CLID);
  boolD = ((TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType==SIP_TRUNK)||((TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType==NG911_SIP)||(TRUNK_TYPE_MAP.Trunk[CallData.intTrunk].TrunkType==REFER) ));
 
  if ((boolA)&&(!boolE)) {CallDataNode->addAttribute(XML_FIELD_CALLBACK_NUMBER, CallData.strDialedNumber.c_str());}
  if ((!boolA)&&((boolC)||(boolD))&&(!boolB))
   {stringTemp.append(CustNameGUIformat(CallData.strCustName, true));}

  CallDataNode->addAttribute(XML_FIELD_CALLBACK_DISPLAY, stringTemp.c_str());
 
  if (VerifyCallBack())             {CallDataNode->addAttribute(XML_FIELD_CALLBACK_VALIDATED, XML_VALUE_TRUE);}
  else                              {CallDataNode->addAttribute(XML_FIELD_CALLBACK_VALIDATED, XML_VALUE_FALSE);} 
  
  CallDataNode->addAttribute(XML_FIELD_PSUEDO_ANI, CallData.stringPANI.c_str());

  if(TextData.TDDdata.boolTDDmute) {CallDataNode->addAttribute(XML_FIELD_TDD_MUTE, XML_VALUE_ON);}
  else                             {CallDataNode->addAttribute(XML_FIELD_TDD_MUTE, XML_VALUE_OFF);} 


  CallDataNode->addAttribute(XML_FIELD_ANI_PORT, int2strLZ(intANIPortCallReceived).c_str());
  CallDataNode->addAttribute(XML_FIELD_CALL_STATUS, stringStatus.c_str());
  CallDataNode->addAttribute(XML_FIELD_CALL_STATUS_CODE, stringStatusCode.c_str());
  CallDataNode->addAttribute(XML_FIELD_CALL_STATE_CODE, int2strLZ(intCallStateCode).c_str());
 // CallDataNode->addAttribute(XML_FIELD_CALL_RECORDING, this->CallData.strCallRecording.c_str());

 return true;
}




void ExperientDataClass::fUpdateTrunkTypeStringwithTDD()
{
 size_t found;

// if (!TDDdata.boolTDDthresholdMet) {return;} //remove
 if (!TextData.TDDdata.boolTDDthresholdMet) {return;} 

 found = CallData.strCallTypeDisplay.find("[TDD]");
 if (found != string::npos) {return;}

 found = CallData.strCallTypeDisplay.find("(");
 if (found == string::npos) {CallData.strCallTypeDisplay.append(" [TDD]"); return;}
 else                       {CallData.strCallTypeDisplay.insert(found-1," [TDD]"); return;}

}

void ExperientDataClass::fResetTransferWindow(bool boolTransferSuccessful, bool boolSendtoSingleWorkstation)
{
 XMLNode               	zMainNode;
 XMLNode               	EventNode, ResetTransferWindowNode;
 char*                  cptrResponse = NULL; 
 ExperientDataClass     objData;

 // set intWorkStation before calling
// //cout << "called reset transfer Window -> boolTransferSuccessful      -> " << boolTransferSuccessful << endl;
// //cout << "called reset transfer Window -> boolSendtoSingleWorkstation -> " << boolSendtoSingleWorkstation << endl;
 // XML Header
  zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
  zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
  zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
  // Event Tag
  EventNode      = zMainNode.addChild(XML_NODE_EVENT);
  EventNode.addAttribute(XML_FIELD_PACKET_ID, "");
  EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
  // ResetTransferWindow Tag
  ResetTransferWindowNode   = EventNode.addChild(XML_NODE_RESET_TRANSFER_WINDOW);
  if (boolTransferSuccessful){ ResetTransferWindowNode.addAttribute( XML_FIELD_TRANSFER_SUCCESSFUL, XML_VALUE_TRUE);}
  else                       { ResetTransferWindowNode.addAttribute( XML_FIELD_TRANSFER_SUCCESSFUL, XML_VALUE_FALSE);}
  ResetTransferWindowNode.addAttribute(XML_FIELD_CALL_ID, int2str(CallData.intUniqueCallID).c_str()); 
  cptrResponse = zMainNode.createXMLString();
  if (cptrResponse != NULL)
   { 
    stringXMLString = cptrResponse;
    free(cptrResponse); 
   }
  cptrResponse = NULL;

  objData = this;
  objData.intWorkStation = this->CallData.intPosn;
  objData.boolBroadcastXMLtoSinglePosition = boolSendtoSingleWorkstation;

//  //cout << stringXMLString << endl; //cout << "pos: " << this->CallData.intPosn << endl;
  Queue_WRK_Event(objData);
}



bool ExperientDataClass::fCallInfo_XML_Message(int StatusCode, bool boolWithALI)
{
  XMLNode               	zMainNode, StreamInfoNode;
  XMLNode               	EventNode,CallInfoNode,CallDataNode,WindowDataNode,PSAPappletDataNode,TDDdataNode,TextDataNode, ConferenceDataNode;
  char*                         cptrResponse = NULL;

  int                           intTDDtypeCode = 0;
 
   switch (StatusCode)
    {
     case 77:
          // SMS Message Sent
          intTDDtypeCode = 5;
          break;
     case 78:
          // SMS Message Received
          intTDDtypeCode = 4;
          break;
     case 84:
          // TDD DispatcherSays
           intTDDtypeCode = 2;
           break;
     case 86:
          //TDD_CHAR_RECEIVED
          intTDDtypeCode = 1;
          break;
     case 85:
          // TDD CallerSays
          intTDDtypeCode = 3;
          break;
     case 02:
          //Disconnect
          intTDDtypeCode = 6;
          break;
     default:
          break;
    }

  enumANISystem = enumANI_SYSTEM;
  // XML Header
  zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
  zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
  zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
  // Event Tag
  EventNode      = zMainNode.addChild(XML_NODE_EVENT);
  EventNode.addAttribute(XML_FIELD_PACKET_ID, "");
  EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
  // CallInfo Tag
  CallInfoNode   = EventNode.addChild(XML_NODE_CALL_INFO);

  //PSAPAppletData Tag
//  PSAPappletDataNode = CallInfoNode.addChild(XML_NODE_PSAP_APPLET_DATA); 
//  fPSAPappletData_XML_Tag(&PSAPappletDataNode);

  // WindowData Tag
  WindowDataNode = CallInfoNode.addChild(XML_NODE_WINDOW_DATA);
  fWindowData_XML_Tag(&WindowDataNode);

  // TDDdata Tag
  TDDdataNode    = CallInfoNode.addChild(XML_NODE_TDD_DATA);
  fTDDdata_XML_Tag(&TDDdataNode, intTDDtypeCode);

  // TextData Tag
  TextDataNode  = CallInfoNode.addChild(XML_NODE_TEXT_DATA);
  fTextData_XML_Tag(&TextDataNode, intTDDtypeCode);

  // CallData Tag
  CallDataNode   = CallInfoNode.addChild(XML_NODE_CALL_DATA); 
  fCallData_XML_Tag(&CallDataNode, StatusCode, boolWithALI);

  //StreamInfo Tag
  StreamInfoNode = CallInfoNode.addChild(XML_NODE_STREAM_INFO);
  fStreamInfo_XML_Tag(&StreamInfoNode);

  //ConferenceData Node
  ConferenceDataNode = CallInfoNode.addChild(XML_NODE_CONFERENCE_DATA);
  fConferenceData_XML_Tag(&ConferenceDataNode); 

  cptrResponse = zMainNode.createXMLString(); 
  if (cptrResponse != NULL)   {
   stringXMLString = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
//if (intTDDtypeCode) {  //cout << "SMS/TDD -> " << endl << stringXMLString << endl;} 

//  //cout << stringXMLString << endl;
  return true;
}

void ExperientDataClass::fLoadLegacyTDDobjectWithTextData()
{
 // Load Textdata object into TDD object .......
 switch (this->TextData.TextType)
  {
   case NO_TEXT_TYPE:
        SendCodingError("ExperientDataClass::fLoadLegacyTDDobjectWithTextData() -NO_TEXT_TYPE");
        break;
   case TDD_MESSAGE: 
        this->TDDdata                     = this->TextData.TDDdata;
        this->TDDdata.strTDDconversation  = this->TextData.strConversation;
        this->TDDdata.boolTDDWindowActive = this->TextData.boolTextWindowActive;
        this->TDDdata.boolTDDSendButton   = this->TextData.boolTextSendButton;                
        break;
  case MSRP_MESSAGE:
        this->TDDdata.SMSdata             = this->TextData.SMSdata;     
        this->TDDdata.strTDDconversation  = this->TextData.strConversation;
        this->TDDdata.boolTDDWindowActive = this->TextData.boolTextWindowActive;
        this->TDDdata.boolTDDSendButton   = this->TextData.boolTextSendButton;  
        this->boolSMSmessage              = true;
        break;
  }

}

bool ExperientDataClass::fLoad_Decode_ALI(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData)
{
 MessageClass           objMessage;
 XMLParserBase64Tool    b64;
 unsigned char*         ptrUcharDecodedALI;
 int                    intLength;
 XMLError               xe;
 string                 stringError;
 Port_Data              objPortData;
 

 if (stringArg == "")   {
  
   ALIData.boolALIRecordReceived = false;
   ALIData.stringAliText	 = "";
   return true;

 }

 ptrUcharDecodedALI    = b64.decode(stringArg.c_str(),&intLength, &xe);


 if(xe == eXMLErrorNone)   {
   
    for (int i=0; i < intLength; i++){ALIData.stringAliText += ptrUcharDecodedALI[i];}
    ALIData.boolALIRecordReceived = true;
    b64.freeBuffer();
    return true;

  }
  else   {
   
    objPortData.fLoadPortData(enumArg2, intPortNum);
    stringError = ConvertXMLError2String(xe);
    objMessage.fMessage_Create(LOG_WARNING,710, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_710a, stringError, 
                               ASCII_String(strXMLData.c_str(), strXMLData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
    enQueue_Message(WRK,objMessage); 

  }

 b64.freeBuffer();
 return false;
}






string EncodeBase64(string strInput)
{
 XMLParserBase64Tool   	b64;
 string                 strEncoded;
 unsigned char*         ucharTemp;
 char*                  charTemp;

    charTemp     = (char*) strInput.c_str();
    ucharTemp    = (unsigned char*) charTemp;
    strEncoded   = b64.encode(ucharTemp, strInput.length());
    b64.freeBuffer();
    if (strEncoded.length() == 0){return "";}
    else                         {return strEncoded;}
}


bool ExperientDataClass::fCreateE2ALIPhoneData()
{
// we may need to get more creative here in case a callback is actually a pANI ...... TBC ...


if (!CallData.stringPANI.empty())
 {
  ALIData.E2Data.EmergencyServicesRoutingDigits.chTypeofDigits             = 0x04; 
  ALIData.E2Data.EmergencyServicesRoutingDigits.chNatureofNumber           = 0x00; 
  ALIData.E2Data.EmergencyServicesRoutingDigits.chNumberingPlanandEncoding = 0x21;
  ALIData.E2Data.EmergencyServicesRoutingDigits.chNumberofDigits           = 0x0A;  
  ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitTwoandOne           = EncodeTwoDigits(CallData.stringPANI.substr(0,2));
  ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitFourandThree        = EncodeTwoDigits(CallData.stringPANI.substr(2,2));
  ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitSixandFive          = EncodeTwoDigits(CallData.stringPANI.substr(4,2));
  ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitEightandSeven       = EncodeTwoDigits(CallData.stringPANI.substr(6,2));
  ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitTenandNine          = EncodeTwoDigits(CallData.stringPANI.substr(8,2));  
 }

if (CallData.intCallbackNumber)
 {
  ALIData.E2Data.EmergencyServicesRoutingDigits.chTypeofDigits               = 0x02; 
  ALIData.E2Data.CallbackNumber.chNatureofNumber                             = 0x00; 
  ALIData.E2Data.CallbackNumber.chNumberingPlanandEncoding                   = 0x21;
  ALIData.E2Data.CallbackNumber.chNumberofDigits                             = 0x0A;  
  ALIData.E2Data.CallbackNumber.chDigitTwoandOne                             = EncodeTwoDigits(CallData.stringTenDigitPhoneNumber.substr(0,2));
  ALIData.E2Data.CallbackNumber.chDigitFourandThree                          = EncodeTwoDigits(CallData.stringTenDigitPhoneNumber.substr(2,2));
  ALIData.E2Data.CallbackNumber.chDigitSixandFive                            = EncodeTwoDigits(CallData.stringTenDigitPhoneNumber.substr(4,2));
  ALIData.E2Data.CallbackNumber.chDigitEightandSeven                         = EncodeTwoDigits(CallData.stringTenDigitPhoneNumber.substr(6,2));
  ALIData.E2Data.CallbackNumber.chDigitTenandNine                            = EncodeTwoDigits(CallData.stringTenDigitPhoneNumber.substr(8,2));
 }



 return true;
}

void ExperientDataClass::fSendHangup()
{
 // must be semaphore protected before calling this function
 size_t                         sz;

 enumANIFunction                = CANCEL_TRANSFER;
 CallData.TransferData.eTransferMethod = mBLANK;

 sz = FreeswitchData.vectDestChannelsOnCall.size();
 for (unsigned int i = 0; i< sz; i++) 
 {
  FreeswitchData.objChannelData = FreeswitchData.vectDestChannelsOnCall[i];
  FreeswitchData.objChannelData.strChannelID = FreeswitchData.vectDestChannelsOnCall[i].strChannelID;
  Queue_AMI_Input(this);
  sem_post(&sem_tAMIFlag);
 }

 sz = FreeswitchData.vectPostionsOnCall.size();
 for (unsigned int i = 0; i< sz; i++) 
 {
  FreeswitchData.objChannelData = FreeswitchData.vectPostionsOnCall[i]; 
  FreeswitchData.objChannelData.strChannelID = FreeswitchData.vectPostionsOnCall[i].strChannelID;
  Queue_AMI_Input(this);
  sem_post(&sem_tAMIFlag);
 }

 
}




unsigned long int  ExperientDataClass::fGenerateE2ALIRequest(unsigned char* chRequest, int* intLength, bid_type eBid)
{
 int               j = E2_TRANSACTIONID_LENGTH+3;
 unsigned long int iResult, iTransactionID;
 int               intESMElength;

 // Zero out the string
 for ( int i= 0; i< *intLength ; i++) {chRequest[i] = 0x00;}

 // Package Type Query
 chRequest[0] = 0xE2;

 // Create TransactionId
 chRequest[2] = 0xC7;
 chRequest[3] = 0x04;
 iResult = iTransactionID = NextTransactionID();

 do
  {
   if ( j < 4 ) {SendCodingError( "ExperientDataClass.cpp - error in transaction id hex conversion in fn ExperientDataClass::fGenerateE2ALIRequest()"); return false;}
   chRequest[j] = (unsigned char) iResult%256;
   iResult/= 256;
   j--;
  
  }while (iResult > 0);

 // Componet Sequence
 chRequest[8] = 0xE8;

 // Component Type
 chRequest[10] = 0xE9; 

 //Component ID
 chRequest[12] = 0xCF; 
 chRequest[13] = 0x01;
 chRequest[14] = 0x01; 

 //Operation Code
 chRequest[15] = 0xD1; 
 chRequest[16] = 0x02;
 chRequest[17] = 0x01; 
 chRequest[18] = 0x01;

 //Parameter Set
 chRequest[19] = 0xF2; 

 //ESME Identification
 chRequest[21] = 0xC0;
 intESMElength = strESME_ID_STRING.length();
 chRequest[22] = (unsigned char) intESMElength;
 for (int i = 0; i < intESMElength; i++) { chRequest[23+i] = (unsigned char) strESME_ID_STRING[i];}

 //Position Type Request
 chRequest[23+intESMElength] = 0xC1;
 chRequest[24+intESMElength] = 0x01;
 switch (eBid)
  {
   case INITIAL:  chRequest[25+intESMElength] = 0x01;break;
   case AUTO:     chRequest[25+intESMElength] = 0x02;break;
   case H_BEAT:   chRequest[25+intESMElength] = 0x04;break;
   default:       chRequest[25+intESMElength] = 0x03;break;
  }
 

 switch (eBid)
  {
   case H_BEAT:
        // Emergency Services Routing Key
        chRequest[26+intESMElength] = 0xC2;   
        chRequest[27+intESMElength] = 0x00;
         *intLength = 28 + intESMElength;
        break;

   default:

        if (!CallData.stringPANI.empty())
         {
          // Emergency Services Routing Key
          chRequest[26+intESMElength] = 0xC2;       
          chRequest[27+intESMElength] = 0x09;       //length  9                  (section 9.2.3) (we should have this info from the SIP Header)
          chRequest[28+intESMElength] = 0x04;       //Routing Number
          chRequest[29+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chNatureofNumber; 
          chRequest[30+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chNumberingPlanandEncoding;
          chRequest[31+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chNumberofDigits;
          if (chRequest[31+intESMElength] != 0x0A){SendCodingError("ExperientDataClass.cpp - 1. length not ten in fn  ExperientDataClass::fGenerateE2ALIRequest()"); return false;}             
          chRequest[32+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitTwoandOne;
          chRequest[33+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitFourandThree;
          chRequest[34+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitSixandFive;
          chRequest[35+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitEightandSeven;
          chRequest[36+intESMElength] = ALIData.E2Data.EmergencyServicesRoutingDigits.chDigitTenandNine;
         }
        else
         {
          // Callback Number
          chRequest[26+intESMElength] = 0xC3;
          chRequest[27+intESMElength] = 0x09;       //length  9                  (section 9.2.3) (we should have this info from the SIP Header)
          chRequest[28+intESMElength] = 0x02;       //Calling party Number
          chRequest[29+intESMElength] = ALIData.E2Data.CallbackNumber.chNatureofNumber; 
          chRequest[30+intESMElength] = ALIData.E2Data.CallbackNumber.chNumberingPlanandEncoding;
          chRequest[31+intESMElength] = ALIData.E2Data.CallbackNumber.chNumberofDigits;
          if (chRequest[31+intESMElength] != 0x0A){SendCodingError("ExperientDataClass.cpp - 2. length not ten in fn  ExperientDataClass::fGenerateE2ALIRequest()"); return false;}              
          chRequest[32+intESMElength] = ALIData.E2Data.CallbackNumber.chDigitTwoandOne;
          chRequest[33+intESMElength] = ALIData.E2Data.CallbackNumber.chDigitFourandThree;
          chRequest[34+intESMElength] = ALIData.E2Data.CallbackNumber.chDigitSixandFive;
          chRequest[35+intESMElength] = ALIData.E2Data.CallbackNumber.chDigitEightandSeven;
          chRequest[36+intESMElength] = ALIData.E2Data.CallbackNumber.chDigitTenandNine;
         }
              
        *intLength = 37 + intESMElength;
        break;
  }




 // Set length fields
 chRequest[1]  = (unsigned char) (*intLength - 2);
 chRequest[9]  = (unsigned char) (*intLength - 10);
 chRequest[11] = (unsigned char) (*intLength - 12);
 chRequest[20] = (unsigned char) (*intLength - 21);

 return iTransactionID;
}

void  WorkStation::fSendACK(ExperientDataClass objData)
{
  XMLNode                MainNode, EventNode, ACKnode;
  string                 strResponse;
  char*                  cptrResponse = NULL;

   // XML Header
  MainNode=XMLNode::createXMLTopNode("xml",TRUE);
  MainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
  MainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
  // Event Tag
  EventNode      = MainNode.addChild(XML_NODE_EVENT);
  EventNode.addAttribute(XML_FIELD_PACKET_ID, (char*)"");
  EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
  // ACK node
  ACKnode        = EventNode.addChild(XML_NODE_ACK);
  ACKnode.addAttribute(XML_FIELD_PACKET_ID, objData.strEventUniqueID.c_str());

  cptrResponse = MainNode.createXMLString();
  if (cptrResponse != NULL)
   {
    strResponse = cptrResponse;
    free(cptrResponse);
   }
  cptrResponse = NULL;
  fTransmit( strResponse.c_str(), strResponse.length());
 // //cout << strResponse << endl;

} 


string WorkStation::fDisplay()
{
 ostringstream  strOutput;

 strOutput << "Position                       "  <<   intPosition << endl;
 strOutput << "OS                             "  <<   strWindowsOS << endl;
 strOutput << "IP address                     ";
 strOutput << RemoteIPAddress.fDisplay();
 strOutput << "Remote Port                    "  <<   intRemotePortNumber << endl;
 strOutput << "Last HB sec                    "  <<   timespecTimeLastHeartbeat.tv_sec << endl;
 strOutput << "Last HB nsec                   "  <<   timespecTimeLastHeartbeat.tv_nsec << endl;
 strOutput << "Bool Active                    "  <<   boolActive << endl;
 strOutput << "GUI version                    "  <<   strGUIVersion << endl;
 strOutput << "XML version                    "  <<   strGUIxmlVersion << endl;
 strOutput << "Windows User                   "  <<   strWindowsUser << endl;
 strOutput << "TCP Id                         "  <<   intTCP_ConnectionID << endl;
 strOutput << "Contact Relay IP Address       ";
 strOutput << RCCremoteIPaddress.fDisplay();  
 strOutput << "Bool Contact Relay Installed   "  <<   boolContactRelayInstalled << endl;
 strOutput << "Bool Contact Relay PIN         "  <<   intRCCrelayPin << endl;
 strOutput << "Bool Contact Relay Remote Port "  <<   intRCCremotePortNumber << endl;
 strOutput << "Bool Contact Relay Local Port  "  <<   intRCClocalPortNumber << endl;
 strOutput << "Bool Contact Relay Connected   "  <<   boolRCCconnected << endl;
 strOutput << "Bool Send Delayed Signal       "  <<   boolRCCSendDelayedSignal << endl;


 return strOutput.str();
}   


//copy constructor
Position_Alias::Position_Alias(const Position_Alias *a) {
 iPosition = a->iPosition;
 iAlias    = a->iAlias;
}

//assignment operator
Position_Alias& Position_Alias::operator=(const Position_Alias& a) {
 if (&a == this ) {return *this;}
 iPosition = a.iPosition;
 iAlias    = a.iAlias;
 return *this;
}
//compare operator
bool Position_Alias::fCompare(const Position_Alias& a) const {

 bool boolRunningCheck = true;

 boolRunningCheck = boolRunningCheck && (this->iPosition == a.iPosition);
 boolRunningCheck = boolRunningCheck && (this->iAlias    == a.iAlias);

 return boolRunningCheck;
}

void Position_Alias::fClear() {
 iPosition = 0;
 iAlias = 0;
}

bool Position_Alias::fLoadPositionAlias(string strData) {
 // Data should be in the format "<int>|<int>"
 size_t         position = 0;
 size_t         found;
 string         strPosition;
 string         strAlias;
 
 position = strData.find( "|");
 if (position == string::npos)         {return false;}
 if (strData.length() <= position+1)   {return false;}
 strPosition = RemoveLeadingSpaces(strData.substr(0,position));
 strPosition = RemoveTrailingSpaces(strPosition);
 strAlias    = RemoveLeadingSpaces(strData.substr(position+1,string::npos));
 strAlias    = RemoveTrailingSpaces(strAlias);

 found =  strPosition.find_first_not_of("0123456789");
 if (found != string::npos)            {return false;}
 found =  strAlias.find_first_not_of("0123456789");
 if (found != string::npos)            {return false;}
 iPosition = char2int(strPosition.c_str());
 iAlias    = char2int(strAlias.c_str());
 return true;
}


//copy constructor
Position_Aliases::Position_Aliases(const Position_Aliases *a) {

 this->vPositionAliases.clear();
 
 for (unsigned int i=0; i< a->vPositionAliases.size(); i++) {
  this->vPositionAliases.push_back(a->vPositionAliases[i]);
 }

}

//assignment operator
Position_Aliases& Position_Aliases::operator=(const Position_Aliases& a) {
 if (&a == this ) {return *this;}

 this->vPositionAliases.clear();

 for (unsigned int i=0; i< a.vPositionAliases.size(); i++) {
  this->vPositionAliases.push_back(a.vPositionAliases[i]);
 }

 return *this;
}

//compare operator
bool Position_Aliases::fCompare(const Position_Aliases& a) const {

 bool 	boolRunningCheck = true;
 size_t sizeThis, sizeThat;
 
 sizeThis = this->vPositionAliases.size();
 sizeThat = a.vPositionAliases.size();

 if (sizeThis != sizeThat) {return false;}

 for (unsigned int i=0; i< sizeThis; i++) {
  boolRunningCheck = boolRunningCheck && (this->vPositionAliases[i].fCompare(a.vPositionAliases[i]));
 }

 return boolRunningCheck;
}


void Position_Aliases::fClear() {
 vPositionAliases.clear();
}

string Position_Aliases::fDisplay() {
  string strReturn = "";
  
  if (vPositionAliases.empty()) {return "";}
  for (std::vector<Position_Alias>::iterator it = vPositionAliases.begin() ; it != vPositionAliases.end(); ++it) {
   strReturn += int2str(it->iPosition);
   strReturn += "|";
   strReturn += int2str(it->iAlias);
   strReturn += " "; 
  } 
  return  strReturn; 
}

string Position_Aliases::fAliasOfPosition(int i) {
 ////cout << "size in this function ->" << vPositionAliases.size() << endl;
 if (vPositionAliases.empty()) {return int2strLZ(i);}

 for (std::vector<Position_Alias>::iterator it = vPositionAliases.begin() ; it != vPositionAliases.end(); ++it)
  {
   if (i == it->iPosition) {return int2strLZ(it->iAlias);}
  } 

 return int2strLZ(i);
}

string Position_Aliases::CADmessageWithAlias(string strData) {
    
 int    iPostion;
 string strPosition;
 size_t found;
 string strReturn = strData;

 
 if (strData.length() < 5 ) {return strData;}
 strReturn.resize(strReturn.size () - 1);  // remove BCC
 strPosition = strData.substr(2,2);
 found       = strPosition.find_first_not_of("0123456789");
 if (found != string::npos) {return strData;}

 iPostion = char2int(strPosition.c_str());
 strReturn.replace(2,2, fAliasOfPosition(iPostion));
 strReturn += BlockCheckCharacter( strReturn, strReturn.length());

 return strReturn;

}





bool Position_Aliases::fLoadPositionAliases(string strData) {
 size_t         position = 0;
 string         stringTemp;
 string         strAliasAssignment;
 Position_Alias objPositionAlias;

 stringTemp = strData;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   strAliasAssignment.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   strAliasAssignment = RemoveLeadingSpaces(strAliasAssignment);
   strAliasAssignment = RemoveTrailingSpaces(strAliasAssignment);
   if (!objPositionAlias.fLoadPositionAlias(strAliasAssignment)) {return false;}
   vPositionAliases.push_back(objPositionAlias);
  } while (position != string::npos);

 //at this point there will be a alias left no comma .....

 strAliasAssignment.assign(stringTemp,0 ,string::npos);
 strAliasAssignment = RemoveLeadingSpaces(strAliasAssignment);
 strAliasAssignment = RemoveTrailingSpaces(strAliasAssignment);
 if (!objPositionAlias.fLoadPositionAlias(strAliasAssignment)) {return false;}
 vPositionAliases.push_back(objPositionAlias);

 return true;
}

//Constructor
Workstation_Group::Workstation_Group() {
 strGroupName.clear();
 strPSAP_ID.clear();
 WorkstationsInGroup.clear();
 TrunksinGroup.clear();
 bALLOW_MANUAL_ALI_BIDS     = false;
 bSHOW_GOOGLE_MAP           = false;
 bCONTACT_RELAY_INSTALLED   = false;
 bSHOW_CALLBACK_BUTTON      = false;
 bSHOW_SPEED_DIAL_BUTTON    = false;
 bSHOW_DIAL_PAD_BUTTON      = false;
 bSHOW_ANSWER_BUTTON        = false;
 bSHOW_BARGE_BUTTON         = false;
 bSHOW_HOLD_BUTTON          = false;
 bSHOW_HANGUP_BUTTON        = false;
 bSHOW_MUTE_BUTTON          = false;
 bSHOW_VOLUME_BUTTON        = false;
 bSHOW_ATTENDED_XFER_BUTTON = false;
 bTDD_AUTO_DETECT           = false;
 bSHOW_RAPID_LITE_BUTTON    = false;
 strTDD_DEFAULT_LANGUAGE.clear();
 iTDD_SEND_DELAY            = 0;
 strTDD_SEND_TEXT_COLOR.clear();
 strTDD_RECEIVED_TEXT_COLOR.clear();
 iTDD_CPS_THRESHOLD         = 0;
 strDISPATCHER_SAYS_PREFIX_EXPORT.clear();
 strCALLER_SAYS_EXPORT.clear();
 strPSAPURI.clear();
 vTDD_MESSAGE_LIST.clear();
 //XMLNode            PhoneBookNode;
 //XMLNode            SharedLineNode;
 PSAPStatus.fClear();
}

//copy constructor
Workstation_Group::Workstation_Group(const Workstation_Group*a) {
 strGroupName			= a->strGroupName;
 strPSAP_ID			= a->strPSAP_ID;
 WorkstationsInGroup.clear();
 for (unsigned int i=0; i < a->WorkstationsInGroup.size(); i++) {
  WorkstationsInGroup.push_back(a->WorkstationsInGroup[i]);
 }                         
 TrunksinGroup.clear();
 for (unsigned int i=0; i < a->TrunksinGroup.size(); i++) {
  TrunksinGroup.push_back(a->TrunksinGroup[i]);
 }   
 bALLOW_MANUAL_ALI_BIDS           = a->bALLOW_MANUAL_ALI_BIDS;
 bSHOW_GOOGLE_MAP                 = a->bSHOW_GOOGLE_MAP;
 bCONTACT_RELAY_INSTALLED         = a->bCONTACT_RELAY_INSTALLED;
 bSHOW_CALLBACK_BUTTON            = a->bSHOW_CALLBACK_BUTTON;
 bSHOW_SPEED_DIAL_BUTTON          = a->bSHOW_SPEED_DIAL_BUTTON;
 bSHOW_DIAL_PAD_BUTTON            = a->bSHOW_DIAL_PAD_BUTTON;
 bSHOW_ANSWER_BUTTON              = a->bSHOW_ANSWER_BUTTON;
 bSHOW_BARGE_BUTTON               = a->bSHOW_BARGE_BUTTON;
 bSHOW_HOLD_BUTTON                = a->bSHOW_HOLD_BUTTON;
 bSHOW_HANGUP_BUTTON              = a->bSHOW_HANGUP_BUTTON;
 bSHOW_MUTE_BUTTON                = a->bSHOW_MUTE_BUTTON;
 bSHOW_VOLUME_BUTTON              = a->bSHOW_VOLUME_BUTTON;
 bSHOW_ATTENDED_XFER_BUTTON       = a->bSHOW_ATTENDED_XFER_BUTTON;
 bTDD_AUTO_DETECT                 = a->bTDD_AUTO_DETECT;
 bSHOW_RAPID_LITE_BUTTON          = a->bSHOW_RAPID_LITE_BUTTON;
 strTDD_DEFAULT_LANGUAGE          = a->strTDD_DEFAULT_LANGUAGE;
 iTDD_SEND_DELAY                  = a->iTDD_SEND_DELAY;
 strTDD_SEND_TEXT_COLOR           = a->strTDD_SEND_TEXT_COLOR;
 strTDD_RECEIVED_TEXT_COLOR       = a->strTDD_RECEIVED_TEXT_COLOR;
 iTDD_CPS_THRESHOLD               = a->iTDD_CPS_THRESHOLD;
 strDISPATCHER_SAYS_PREFIX_EXPORT = a->strDISPATCHER_SAYS_PREFIX_EXPORT;
 strCALLER_SAYS_EXPORT            = a->strCALLER_SAYS_EXPORT;
 strPSAPURI                       = a->strPSAPURI;
 vTDD_MESSAGE_LIST.clear();
 for (unsigned int i=0; i < a->vTDD_MESSAGE_LIST.size(); i++) {
  vTDD_MESSAGE_LIST.push_back(a->vTDD_MESSAGE_LIST[i]);
 }                         
 PhoneBookNode                     = a->PhoneBookNode.deepCopy();
 SharedLineNode                    = a->SharedLineNode.deepCopy();
 PSAPStatus                        = a->PSAPStatus;
}

//assignment operator
Workstation_Group& Workstation_Group::operator=(const Workstation_Group& a) {

 if (&a == this ) {return *this;}

 strGroupName			= a.strGroupName;
 strPSAP_ID			= a.strPSAP_ID;
 WorkstationsInGroup.clear();
 for (unsigned int i=0; i < a.WorkstationsInGroup.size(); i++) {
  WorkstationsInGroup.push_back(a.WorkstationsInGroup[i]);
 }                         
 TrunksinGroup.clear();
 for (unsigned int i=0; i < a.TrunksinGroup.size(); i++) {
  TrunksinGroup.push_back(a.TrunksinGroup[i]);
 }   
 bALLOW_MANUAL_ALI_BIDS           = a.bALLOW_MANUAL_ALI_BIDS;
 bSHOW_GOOGLE_MAP                 = a.bSHOW_GOOGLE_MAP;
 bCONTACT_RELAY_INSTALLED         = a.bCONTACT_RELAY_INSTALLED;
 bSHOW_CALLBACK_BUTTON            = a.bSHOW_CALLBACK_BUTTON;
 bSHOW_SPEED_DIAL_BUTTON          = a.bSHOW_SPEED_DIAL_BUTTON;
 bSHOW_DIAL_PAD_BUTTON            = a.bSHOW_DIAL_PAD_BUTTON;
 bSHOW_ANSWER_BUTTON              = a.bSHOW_ANSWER_BUTTON;
 bSHOW_BARGE_BUTTON               = a.bSHOW_BARGE_BUTTON;
 bSHOW_HOLD_BUTTON                = a.bSHOW_HOLD_BUTTON;
 bSHOW_HANGUP_BUTTON              = a.bSHOW_HANGUP_BUTTON;
 bSHOW_MUTE_BUTTON                = a.bSHOW_MUTE_BUTTON;
 bSHOW_VOLUME_BUTTON              = a.bSHOW_VOLUME_BUTTON;
 bSHOW_ATTENDED_XFER_BUTTON       = a.bSHOW_ATTENDED_XFER_BUTTON;
 bTDD_AUTO_DETECT                 = a.bTDD_AUTO_DETECT;
 bSHOW_RAPID_LITE_BUTTON          = a.bSHOW_RAPID_LITE_BUTTON;
 strTDD_DEFAULT_LANGUAGE          = a.strTDD_DEFAULT_LANGUAGE;
 iTDD_SEND_DELAY                  = a.iTDD_SEND_DELAY;
 strTDD_SEND_TEXT_COLOR           = a.strTDD_SEND_TEXT_COLOR;
 strTDD_RECEIVED_TEXT_COLOR       = a.strTDD_RECEIVED_TEXT_COLOR;
 iTDD_CPS_THRESHOLD               = a.iTDD_CPS_THRESHOLD;
 strDISPATCHER_SAYS_PREFIX_EXPORT = a.strDISPATCHER_SAYS_PREFIX_EXPORT;
 strCALLER_SAYS_EXPORT            = a.strCALLER_SAYS_EXPORT;
 strPSAPURI                       = a.strPSAPURI;
 vTDD_MESSAGE_LIST.clear();
 for (unsigned int i=0; i < a.vTDD_MESSAGE_LIST.size(); i++) {
  vTDD_MESSAGE_LIST.push_back(a.vTDD_MESSAGE_LIST[i]);
 }                         
 PhoneBookNode                     = a.PhoneBookNode.deepCopy();
 SharedLineNode                    = a.SharedLineNode.deepCopy();
 PSAPStatus                        = a.PSAPStatus;

 return *this;
}

//compare operator
bool Workstation_Group::fCompare(const Workstation_Group& a) const {

 bool boolRunningCheck = true;
 size_t sizethis, sizethat;
 string strPhonebookNode, strPhonebookNodeA, strSharedLineNode, strSharedLineNodeA;
 extern string MakeXMLstring(XMLNode XMLData);

 strPhonebookNode = strPhonebookNodeA = strSharedLineNode = strSharedLineNodeA = "";

 boolRunningCheck = boolRunningCheck && (this->strGroupName         == a.strGroupName);
 boolRunningCheck = boolRunningCheck && (this->strPSAP_ID           == a.strPSAP_ID);
 sizethis = WorkstationsInGroup.size();
 sizethat = a.WorkstationsInGroup.size();
 if (sizethis != sizethat) {return false;}

 for (unsigned int i=0; i < sizethis ; i++) {
  boolRunningCheck = boolRunningCheck && (this->WorkstationsInGroup[i]          == a.WorkstationsInGroup[i]);
 }   
 sizethis = TrunksinGroup.size();
 sizethat = a.TrunksinGroup.size();
 if (sizethis != sizethat) {return false;}

 for (unsigned int i=0; i < sizethis ; i++) {
  boolRunningCheck = boolRunningCheck && (this->TrunksinGroup[i]                == a.TrunksinGroup[i]);
 } 
  
 boolRunningCheck = boolRunningCheck && (this->bALLOW_MANUAL_ALI_BIDS           == a.bALLOW_MANUAL_ALI_BIDS);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_GOOGLE_MAP                 == a.bSHOW_GOOGLE_MAP);
 boolRunningCheck = boolRunningCheck && (this->bCONTACT_RELAY_INSTALLED         == a.bCONTACT_RELAY_INSTALLED);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_CALLBACK_BUTTON            == a.bSHOW_CALLBACK_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_SPEED_DIAL_BUTTON          == a.bSHOW_SPEED_DIAL_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_DIAL_PAD_BUTTON            == a. bSHOW_DIAL_PAD_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_ANSWER_BUTTON              == a.bSHOW_ANSWER_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_BARGE_BUTTON               == a.bSHOW_BARGE_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_HOLD_BUTTON                == a.bSHOW_HOLD_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_HANGUP_BUTTON              == a.bSHOW_HANGUP_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_MUTE_BUTTON                == a.bSHOW_MUTE_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_VOLUME_BUTTON              == a.bSHOW_VOLUME_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_ATTENDED_XFER_BUTTON       == a.bSHOW_ATTENDED_XFER_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->bTDD_AUTO_DETECT	                == a.bTDD_AUTO_DETECT);
 boolRunningCheck = boolRunningCheck && (this->bSHOW_RAPID_LITE_BUTTON          == a.bSHOW_RAPID_LITE_BUTTON);
 boolRunningCheck = boolRunningCheck && (this->strTDD_DEFAULT_LANGUAGE          == a.strTDD_DEFAULT_LANGUAGE);
 boolRunningCheck = boolRunningCheck && (this->iTDD_SEND_DELAY                  == a.iTDD_SEND_DELAY);
 boolRunningCheck = boolRunningCheck && (this->strTDD_SEND_TEXT_COLOR           == a.strTDD_SEND_TEXT_COLOR);
 boolRunningCheck = boolRunningCheck && (this->strTDD_RECEIVED_TEXT_COLOR       == a.strTDD_RECEIVED_TEXT_COLOR);
 boolRunningCheck = boolRunningCheck && (this->iTDD_CPS_THRESHOLD               == a.iTDD_CPS_THRESHOLD);
 boolRunningCheck = boolRunningCheck && (this->strDISPATCHER_SAYS_PREFIX_EXPORT == a.strDISPATCHER_SAYS_PREFIX_EXPORT);
 boolRunningCheck = boolRunningCheck && (this->strCALLER_SAYS_EXPORT            == a.strCALLER_SAYS_EXPORT);
 boolRunningCheck = boolRunningCheck && (this->strPSAPURI                       == a.strPSAPURI);
 sizethis = vTDD_MESSAGE_LIST.size();
 sizethat = a.vTDD_MESSAGE_LIST.size();
 if (sizethis != sizethat) {return false;}

 for (unsigned int i=0; i < sizethis ; i++) {
  boolRunningCheck = boolRunningCheck && (this->vTDD_MESSAGE_LIST[i]		== a.vTDD_MESSAGE_LIST[i]);
 }   

 strPhonebookNode   = MakeXMLstring(PhoneBookNode);
 strPhonebookNodeA  = MakeXMLstring(a.PhoneBookNode);
 strSharedLineNode  = MakeXMLstring(SharedLineNode);
 strSharedLineNodeA = MakeXMLstring(a.SharedLineNode);

 boolRunningCheck = boolRunningCheck && (strPhonebookNode   			== strPhonebookNodeA  );
 boolRunningCheck = boolRunningCheck && (strSharedLineNode  			== strSharedLineNodeA );

 boolRunningCheck = boolRunningCheck && (this->PSAPStatus.fCompare(a.PSAPStatus));

 return boolRunningCheck;
}

bool Workstation_Group::TrunkInGroup(int iTrunk) {
 for (std::vector<int>::iterator it = TrunksinGroup.begin() ; it != TrunksinGroup.end(); ++it)  {
  if (iTrunk == *it) {return true;}
 }

 return false;
}

bool Workstation_Group::WorkstationInGroup(int iWorkstation) {
 for (std::vector<int>::iterator it = WorkstationsInGroup.begin() ; it != WorkstationsInGroup.end(); ++it)  {
  if (iWorkstation == *it) {return true;}
 }

 return false;
}

bool Workstation_Group::CheckAbandonedTrunkList(string strData)
{
 size_t start, end, found;
 string strStart = "\t";
 string strNumber;
 int    iTrunk;

 strStart += ANI_CONF_MEMBER_CALLER_PREFIX;
 start = 0;

 if (strData.empty()) {return false;}

 while (end != string::npos)
  {
   start = strData.find(strStart, start);
   if (start == string::npos) {break;}
   start += strStart.length();
   end = strData.find_first_of("\t", start);
   if (end == string::npos) {continue;} 

   strNumber = strData.substr(start, (end - start));

   found =  strNumber.find_first_not_of("0123456789");
   if (found != string::npos) { SendCodingError("ExperientDataClass.cpp: Non Number found in fn CheckAbandonedTrunkList() Number-> " + strNumber + " Data ->" + strData); break;}
   iTrunk = char2int(strNumber.c_str());

   if (TrunkInGroup(iTrunk)) {return true;}
   
   start = end;
  }

 return false;
}

int Workstation_Groups::IndexOfGroupWithWorkstation(int iWorkstation)
{
 int index = -1;
 for (std::vector< Workstation_Group>::iterator it = WorkstationGroups.begin() ; it != WorkstationGroups.end(); ++it)  {
   index ++;
   if (it->WorkstationInGroup(iWorkstation)) {return index;}
  }

return -1;
}


int Workstation_Groups::IndexOfGroupWithTrunk(int iTrunk) {
 int index = -1;
 for (std::vector< Workstation_Group>::iterator it = WorkstationGroups.begin() ; it != WorkstationGroups.end(); ++it)   {
   index ++;
   if (it->TrunkInGroup(iTrunk)) {return index;}
 }

 return -1;
}

bool Workstation_Group::fLoadFromGlobalVARS() {

 extern string			stringPSAP_ID;
 extern string			stringTDD_DEFAULT_LANGUAGE;
 extern string			strDISPATCHER_SAYS_PREFIX_EXPORT;
 extern string			strCALLER_SAYS_EXPORT;
 extern string			stringPSAP_Name;
 extern string         	 	stringPSAP_ID;
 extern bool			boolSHOW_CALLBACK_BUTTON;
 extern bool			boolSHOW_SPEED_DIAL_BUTTON;
 extern bool			boolSHOW_DIAL_PAD_BUTTON;
 extern bool			boolSHOW_ANSWER_BUTTON;
 extern bool			boolSHOW_BARGE_BUTTON;
 extern bool			boolSHOW_HOLD_BUTTON;
 extern bool			boolSHOW_HANGUP_BUTTON;
 extern bool			boolSHOW_MUTE_BUTTON;
 extern bool			boolSHOW_VOLUME_BUTTON;
 extern bool			boolSHOW_ATTENDED_XFER_BUTTON;
 extern Trunk_Type_Mapping	TRUNK_TYPE_MAP;

 size_t 			sz;

 this->strGroupName	= stringPSAP_Name;
 this->strPSAP_ID       = stringPSAP_ID;
 
 for (int i = 0; i <= intNUM_WRK_STATIONS; i++) {
  this->WorkstationsInGroup.push_back(i);
 }
 for (int i = 0; i <= intNUM_TRUNKS_INSTALLED; i++) {
  if (!TRUNK_TYPE_MAP.Trunk[i].boolTrunkNumberInUse) {continue;}
  this->TrunksinGroup.push_back(i);
 } 
 
 this->bALLOW_MANUAL_ALI_BIDS		= boolALLOW_MANUAL_ALI_BIDS ;
 this->bSHOW_GOOGLE_MAP			= boolSHOW_GOOGLE_MAP;
 this->bCONTACT_RELAY_INSTALLED		= boolCONTACT_RELAY_INSTALLED;
 this->bSHOW_CALLBACK_BUTTON		= boolSHOW_CALLBACK_BUTTON;
 this->bSHOW_RAPID_LITE_BUTTON		= boolSHOW_RAPID_LITE_BUTTON;
 this->bSHOW_SPEED_DIAL_BUTTON		= boolSHOW_SPEED_DIAL_BUTTON;
 this->bSHOW_DIAL_PAD_BUTTON		= boolSHOW_DIAL_PAD_BUTTON;
 this->bSHOW_ANSWER_BUTTON		= boolSHOW_ANSWER_BUTTON;
 this->bSHOW_BARGE_BUTTON		= boolSHOW_BARGE_BUTTON;
 this->bSHOW_HOLD_BUTTON		= boolSHOW_HOLD_BUTTON;
 this->bSHOW_HANGUP_BUTTON		= boolSHOW_HANGUP_BUTTON;
 this->bSHOW_MUTE_BUTTON		= boolSHOW_MUTE_BUTTON;
 this->bSHOW_VOLUME_BUTTON		= boolSHOW_VOLUME_BUTTON;
 this->bSHOW_ATTENDED_XFER_BUTTON	= boolSHOW_ATTENDED_XFER_BUTTON;
 this->bTDD_AUTO_DETECT			= boolTDD_AUTO_DETECT;
 this->bSHOW_RAPID_LITE_BUTTON		= boolSHOW_RAPID_LITE_BUTTON;
 this->strTDD_DEFAULT_LANGUAGE		= stringTDD_DEFAULT_LANGUAGE;
 this->iTDD_SEND_DELAY      		= intTDD_SEND_DELAY;
 this->strTDD_SEND_TEXT_COLOR		= stringTDD_SEND_TEXT_COLOR;
 this->strTDD_RECEIVED_TEXT_COLOR	= stringTDD_RECEIVED_TEXT_COLOR;
 this->iTDD_CPS_THRESHOLD		= intTDD_CPS_THRESHOLD;
 this->strDISPATCHER_SAYS_PREFIX_EXPORT	= strDISPATCHER_SAYS_PREFIX_EXPORT;
 this->strCALLER_SAYS_EXPORT		= strCALLER_SAYS_EXPORT;
 this->strPSAPURI			= "";
 this->vTDD_MESSAGE_LIST.clear();
 this->PhoneBookNode.deleteNodeContent();
 this->SharedLineNode.deleteNodeContent();


 this->PSAPStatus.fClear();






 return true;
}

bool Workstation_Group::fLoadTrunks( string stringArg)
{
 size_t         position = 0;
 string         stringTemp;
 string         stringNumber;
 int            intTrunk;

 stringTemp = stringArg;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   stringNumber.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   stringNumber = RemoveLeadingSpaces(stringNumber);
   stringNumber = RemoveTrailingSpaces(stringNumber);
   if(!Validate_Integer(stringNumber))       {return false;}   
   intTrunk = char2int(stringNumber.c_str());
   if (intTrunk > intMAX_NUM_TRUNKS)         {return false;}
   TrunksinGroup.push_back(intTrunk);
   

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 stringNumber.assign(stringTemp,0 ,string::npos);
 stringNumber = RemoveLeadingSpaces(stringNumber);
 stringNumber = RemoveTrailingSpaces(stringNumber);
 if(!Validate_Integer(stringNumber))       {return false;}   
 intTrunk = char2int(stringNumber.c_str());
 if (intTrunk > intMAX_NUM_TRUNKS)         {return false;}

 TrunksinGroup.push_back(intTrunk);
 return true;
}



void Workstation_Group::fClear()
{
 strGroupName.clear();
 strPSAP_ID.clear();
 WorkstationsInGroup.clear();
 TrunksinGroup.clear();
 strPSAPURI.clear();
 vTDD_MESSAGE_LIST.clear();
}

bool Workstation_Group::HasThisURI( string stringArg)
{
 bool   boolSame;

 if (this->strPSAPURI.empty()) {return false;}
 
 boolSame = (strPSAPURI == FQDNfromURI(stringArg));

 return boolSame;
}

bool Workstation_Group::AnyPositionInGroupActive(Conf_Data objConfData)
{
extern int intNUM_WRK_STATIONS;

 for (int i = 1; i <= intNUM_WRK_STATIONS; i++) {
  if (!this->WorkstationInGroup(i)) {continue;}

  if (objConfData.fPositionActive(i)) {return true;}
 }

 return false;
}

bool Workstation_Group::AnyPositionInGroupHistory(Conf_Data objConfData)
{
extern int intNUM_WRK_STATIONS;

 for (int i = 1; i <= intNUM_WRK_STATIONS; i++) {
  if (!this->WorkstationInGroup(i)) {continue;}

  if (objConfData.fPositionInHistory(i)) {return true;}
 }

 return false;
}



bool Workstation_Group::fLoadWorkstations( string stringArg)
{
 size_t         position = 0;
 string         stringTemp;
 string         stringNumber;
 int            intWorkstation;

 stringTemp = stringArg;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   stringNumber.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   stringNumber = RemoveLeadingSpaces(stringNumber);
   stringNumber = RemoveTrailingSpaces(stringNumber);
   if(!Validate_Integer(stringNumber))                {return false;}   
   intWorkstation = char2int(stringNumber.c_str());
   if (intWorkstation > NUM_WRK_STATIONS_MAX)         {return false;}
   WorkstationsInGroup.push_back(intWorkstation);
   

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 stringNumber.assign(stringTemp,0 ,string::npos);
 stringNumber = RemoveLeadingSpaces(stringNumber);
 stringNumber = RemoveTrailingSpaces(stringNumber);
 if(!Validate_Integer(stringNumber))                {return false;}   
 intWorkstation = char2int(stringNumber.c_str());
 if (intWorkstation > NUM_WRK_STATIONS_MAX)         {return false;}

 WorkstationsInGroup.push_back(intWorkstation);
 return true;
}



void WorkStation::fRegisterWorkStation(ExperientDataClass objData) {
// int            intRC;
 MessageClass   objMessage;
 Call_Data      objCallData;


 RemoteIPAddress.stringAddress = objData.stringWRKStationIPAddr;
 intRemotePortNumber = char2int(objData.stringWRKStationListenPort.c_str());
 intTCP_ConnectionID = objData.intWorkstation_TCPconnectionID;
 strWindowsUser      = objData.strWindowsUser;
 strWindowsOS        = objData.strWindowsOS;
 strGUIVersion       = objData.strGUIVersion;
 strGUIxmlVersion    = objData.strGUIxmlVersion;
 boolActive = true;
 objCallData.fLoadPosn(intPosition);
 objCallData.eJSONEventWRKcode = WORKSTATION_LOG_IN;
 objMessage.fMessage_Create(LOG_CONSOLE_FILE, 709, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                            objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            WRK_MESSAGE_709, strWindowsUser, int2strLZ(intPosition), RemoteIPAddress.stringAddress ); 
 enQueue_Message(WRK,objMessage);
}



void WorkStation::fRefreshWorkStationList(struct timespec timespecTimeNow)   
{
 MessageClass objMessage;
// int          intRC;
 long double  TimeDiff  = time_difference(timespecTimeNow, timespecTimeLastHeartbeat);
 Call_Data    objCallData;

 if (TimeDiff > intWRK_HEARTBEAT_INTERVAL_SEC * WRK_NUMBER_MISSED_HEARTBEATS_DELIST)   {
  
   objCallData.fLoadPosn(intPosition);
   objCallData.eJSONEventWRKcode = WORKSTATION_LOG_OUT;
   objMessage.fMessage_Create(LOG_CONSOLE_FILE,717, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                              objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,WRK_MESSAGE_717, strWindowsUser, 
                              objCallData.stringPosn, RemoteIPAddress.stringAddress ); 
   boolActive                    = false;
   RemoteIPAddress.stringAddress = "";
   intRemotePortNumber           = 0;
   enQueue_Message(WRK,objMessage);

 }

 return;
}







string WorkStation::fUpdateTransmitTime(string strData) {
 size_t found;
 string strNewTimeStamp;
 struct timespec timespecTimeNow; 
 string strOutput;
 string strRestofData;
 string strField = XML_FIELD_TRANSMIT_DATE_TIME;

 
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 strNewTimeStamp = get_time_stamp(timespecTimeNow);
 strNewTimeStamp = strNewTimeStamp.substr(0,27); 

 found = strData.find(XML_FIELD_TRANSMIT_DATE_TIME);
 if (found == string::npos) {return strData;}

 strOutput = strData.substr(0,found);
 if ((found + strField.length() + 30) > strData.length()) {return strData;}

 strRestofData = strData.substr( (found + strField.length() + 30), string::npos);
 strOutput += XML_FIELD_TRANSMIT_DATE_TIME;
 strOutput += "=\"";
 strOutput += strNewTimeStamp;
 strOutput += "\"";
 strOutput += strRestofData;
 return strOutput;
}

string WorkStation::fString_To_Transmit(const char* charData, int intLength) {
 Call_Data                      objCallData;
 string                         strContentHeader;
 string                         strDatatoSend = "";
 string                         strSize;
 string                         strContentType;
 string                         strReplace;
 string                         strData;

 strData = fUpdateTransmitTime(charData);

 strSize.assign(10, ' ');
 strContentType.assign(20, ' ');
 strReplace = int2str(intLength);
 strSize.replace(1, strReplace.length(),strReplace);
 strReplace = "text/xml";
 strContentType.replace( 1, strReplace.length(),strReplace);
 strContentHeader = "\nContent-Length: " + strSize + "\nContent-Type: " + strContentType +"\n\n";
 strDatatoSend    = strContentHeader + strData;

 return strDatatoSend;
}

void WorkStation::fInitXMLSend()
{
 bool                               bDisplayColorPalette = false;
 string                             strXML;
 string                             strFilter;
 XMLNode                            EventNode, InitializationNode, GeneralNode, AppletSetupNode, LineViewNode;
 extern Initialize_Global_Variables INIT_VARS;

 //determine if User is authorized to use Color Editor Applet

 if (find(INIT_VARS.WRKinitVariable.vColorPaletteUsers.begin(), INIT_VARS.WRKinitVariable.vColorPaletteUsers.end(), this->strWindowsUser) != INIT_VARS.WRKinitVariable.vColorPaletteUsers.end() ) {
  bDisplayColorPalette = true;  
 } 
 //Zero out Position node
 INIT_VARS.MainNode[this->intPosition].deleteNodeContent();
 INIT_VARS.MainNode[this->intPosition] = XMLNode::emptyNode();
 //reload with master
 sem_wait(&INIT_VARS.sem_tMutexMasterNode);
 INIT_VARS.MainNode[this->intPosition] = INIT_VARS.MasterNode.deepCopy();
 sem_post(&INIT_VARS.sem_tMutexMasterNode);
 EventNode          = INIT_VARS.MainNode[this->intPosition].getChildNode(XML_NODE_EVENT); 
 InitializationNode = EventNode.getChildNode(XML_NODE_INITIALIZATION);
 GeneralNode        = InitializationNode.getChildNode(XML_NODE_GENERAL);
 AppletSetupNode    = GeneralNode.getChildNode(XML_NODE_APPLET_SETUP);

 if (AppletSetupNode.isEmpty()) {SendCodingError( "WorkStation::fInitXMLSend() empty ButtonSetupNode"); return;}
 AppletSetupNode.deleteAttribute(XML_FIELD_SHOW_COLOR_EDITOR_APPLET );
 
 if (bDisplayColorPalette) {AppletSetupNode.addAttribute( XML_FIELD_SHOW_COLOR_EDITOR_APPLET, XML_VALUE_TRUE  );}
 else                      {AppletSetupNode.addAttribute( XML_FIELD_SHOW_COLOR_EDITOR_APPLET, XML_VALUE_FALSE );}

 if (!this->strLineViewFilter.empty()) {
  LineViewNode       = InitializationNode.getChildNode(XML_NODE_LINE_VIEW);
  if (!LineViewNode.isEmpty()) {
   if (LineViewNode.getAttribute(XML_FIELD_FILTER)) {
    SendCodingError( "WorkStation::fInitXMLSend() Line View Filter already present"); 
   }
   else {
    LineViewNode.addAttribute(XML_FIELD_FILTER, this->strLineViewFilter.c_str());         
   }
  }
 else { SendCodingError( "WorkStation::fInitXMLSend() Line View node not present"); }
 }


// if  (!INIT_VARS.fLoadPhonebook() ){SendCodingError("ExperientDataClass.cpp - WorkStation::fInitXMLSend() Unable to update Phonebook!");return;}

 // later we need to make the init var to each position i.e. -> each postition needs to load it's own phonebook and vars.....
 // i.e. fLoadPhonebook(int intPosition) vs bool firstload plus ->
 //  strXML = INIT_VARS.fInitXMLString(this->intPosition);

 strXML = INIT_VARS.fInitXMLString(this->intPosition);
 this->fTransmit((char*)strXML.c_str(), strXML.length());

 //cout << strXML << endl;
}

void WorkStation::fTransmit(const char* charData, int intLength, bool addHeader)
{
 MessageClass                   objMessage;
 Call_Data                      objCallData;
 ExperientUDPPort               WRKtxPort;
 int                            intRC;
 string                         strAddress;
 bool                           boolShowRawData = ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC));
 extern ExperientCommPort       WRKThreadPort;
 string                         strContentHeader;
 string                         strDatatoSend;
 string                         strSize;
 string                         strContentType;
 string                         strReplace;
 string                         strData;

 //strData = fUpdateTransmitTime(charData);

 if (addHeader) { strDatatoSend = fString_To_Transmit(charData, intLength);}
 else           { strDatatoSend = charData;}
 //strSize.assign(10, ' ');
 //strContentType.assign(20, ' ');
 objCallData = objBLANK_CALL_RECORD ;
 objCallData.fLoadPosn(intPosition);

// strReplace = int2str(intLength);
// strSize.replace(1, strReplace.length(),strReplace);
 //strReplace = "text/xml";
// strContentType.replace( 1, strReplace.length(),strReplace);
// strContentHeader = "\nContent-Length: " + strSize + "\nContent-Type: " + strContentType +"\n\n";
// strDatatoSend    = strContentHeader + strData;



 switch(enumWORKSTATION_CONNECTION_TYPE)
  {
   case UDP:
 
        WRKtxPort.enumPortType                       = WRK;
        WRKtxPort.intPortNum                         = 2;
        WRKtxPort.boolPortActive                     = true;
        WRKtxPort.Config((char*)"QOSFlags=16||160");
        WRKtxPort.Config((char*)"MaxPacketSize=655360");
        WRKtxPort.Config((char*)"OutBufferSize=655360");
        WRKtxPort.SetLocalHost(NULL);
#ifdef IPWORKS_V16
        WRKtxPort.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif 
        WRKtxPort.SetRemoteHost((char*)RemoteIPAddress.stringAddress.c_str());
        WRKtxPort.SetRemotePort(intRemotePortNumber);
        WRKtxPort.SetActive(true);

        intRC = WRKtxPort.Send(strDatatoSend.c_str(), strDatatoSend.length());
        if(intRC) {
          cout<< "WRK port send error->"<<intRC<<endl;
        }
        if (intRC) {WRKtxPort.Error_Handler(); return;}
        strAddress = WRKtxPort.GetRemoteHost();
        strAddress += ":";
        strAddress += int2str(WRKtxPort.GetRemotePort());
     //   //cout << "sending ->" << RemoteIPAddress.stringAddress << ":" << intRemotePortNumber << endl;
        break;
 
   case TCP:

        WRKtxPort.enumPortType                       = WRK;
        WRKtxPort.intPortNum                         = intTCP_ConnectionID;
        WRKtxPort.boolPortActive                     = true;
        strAddress                                   = WRKThreadPort.TCP_Server_Port.GetRemoteHost(intTCP_ConnectionID);
/*
        strReplace = int2str(intLength);
        strSize.replace(1, strReplace.length(),strReplace);
        strReplace = "text/xml";
        strContentType.replace( 1, strReplace.length(),strReplace);
        strContentHeader = "\nContent-Length: " + strSize + "\nContent-Type: " + strContentType +"\n\n";
        strDatatoSend    = strContentHeader + charData;
*/
////cout << strDatatoSend << endl;
        intRC = WRKThreadPort.TCP_Server_Port.Send(intTCP_ConnectionID, strDatatoSend.c_str(), strDatatoSend.length());
        switch (intRC) 
         {
          //0: Successful, 107: The socket is not connected, 32: Broken Pipe, 104: Connection Reset by Peer
          case 0: case 107: case 32: case 104:
                  break;
          case 11:
                  //Try Again      Erie County Stack overflow issue 9/2018 let the inherent process try it again ......
             //     usleep(5000);
            //      //cout << "try again" << endl;
            //      fTransmit( charData, intLength);
            //      break;
          default:
                  objMessage.fMessage_Create(LOG_WARNING, 738, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                             WRKtxPort.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,WRK_MESSAGE_738, 
                                             int2str(intTCP_ConnectionID),WRKThreadPort.TCP_Server_Port.GetLastError()); 
                  enQueue_Message(WRK,objMessage);
         }
        break;
   default:
        SendCodingError("ExperientDataClass.cpp - coding error WorkStation::fTransmit() No Connection Type Defined");
        break;
  }

 if (boolShowRawData)   {
   
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 731, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                              WRKtxPort.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LOG_MESSAGE_531, strAddress, 
                              ASCII_String(strDatatoSend.c_str(), strDatatoSend.length()),"","","","",  DEBUG_MSG, NEXT_LINE);
   enQueue_Message(WRK,objMessage);

 }      

}

string WorkStation::fUpdateMuteButton(string stringText) {
// MessageClass                   objMessage;
// extern ExperientCommPort       WRKThreadPort;
 XMLNode                        zMainNode;
 XMLNode                        xNode,yNode;
 string                         strReturnString;
 int                            intLength;
 char*                          cptrResponse = NULL;


  // XML Header
 zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
 zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
 // GENERAL
 xNode=zMainNode.addChild(XML_NODE_EVENT);
 xNode.addAttribute(XML_FIELD_PACKET_ID, "");
 xNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 yNode=xNode.addChild(XML_NODE_MUTE_BUTTON );
 yNode.addAttribute(XML_FIELD_STATE, stringText.c_str());
 
 strReturnString.clear();

 cptrResponse = zMainNode.createXMLString();
 if (cptrResponse != NULL)  {
   strReturnString   = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;

/*
 stringTemp = fString_To_Transmit(stringTemp.c_str(), stringTemp.length() );
 //cout << stringTemp << endl;
 intLength = stringTemp.length();
 if (this->boolActive) {
  WRKThreadPort.TCP_Server_Port.Send(this->intTCP_ConnectionID, stringTemp.c_str(), intLength);
 }
*/
 
 return strReturnString;
}

string WorkStation::fCreateAlert( string stringText, Call_Data objCallData, string stringIPAddress, int intRemotePort, bool boolShutdown)
{
 MessageClass                   objMessage;
 extern ExperientCommPort       WRKThreadPort;
 extern int                     intALARM_POPUP_DURATION_MS;
 XMLNode                        zMainNode;
 XMLNode                        xNode,yNode;
 string                         stringType;
 string                         stringTemp;

 int                            intLength;
 char*                          cptrResponse = NULL;
 string                         strTimeStamp;

  // XML Header
 zMainNode=XMLNode::createXMLTopNode("xml",TRUE);
 zMainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 zMainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);
 // GENERAL
 xNode=zMainNode.addChild(XML_NODE_EVENT);
 xNode.addAttribute(XML_FIELD_PACKET_ID, "");
 xNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 yNode=xNode.addChild(XML_NODE_ALARM );
 
 if(boolShutdown)  {stringType = XML_VALUE_SHUTDOWN;}
 else              {stringType = XML_VALUE_POPUP;}

 yNode.addAttribute(XML_FIELD_ALARM_TYPE, stringType.c_str());
 yNode.addAttribute(XML_FIELD_ALARM_TEXT, stringText.c_str());
 yNode.addAttribute(XML_FIELD_POPUP_DURATION_MS, int2str(intALARM_POPUP_DURATION_MS).c_str());

 if (!objCallData.stringUniqueCallID.empty()){
  yNode.addAttribute(XML_FIELD_CALL_ID, objCallData.stringUniqueCallID.c_str()); 
 }
 
 cptrResponse = zMainNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringTemp   = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 if (!boolShutdown) {return stringTemp;}

 
 // this will broadcast stringText(error message) the offending IP Address ..
 // intRemotePort will have the connectionID ..... No longer UDP for this connection .....

  stringTemp = fString_To_Transmit(stringTemp.c_str(), stringTemp.length() );
  intLength = stringTemp.length();
  WRKThreadPort.TCP_Server_Port.Send(intRemotePort, stringTemp.c_str(), intLength);
 // WRKThreadPort.TCP_Server_Port.fSendKillSocket( intRemotePort);
 
 return "";
}


void WorkStation::fTransmitMainWorktable(bool boolReset)
{
 int                                            intRC;
 struct timespec			        timespecDelay;
 struct timespec			        timespecRemainder;
 extern deque <ExperientDataClass>              vWorkstationHistory;
 extern sem_t                                   sem_tMutexWorkstationHistory;
 extern vector <ExperientDataClass>             vMainWorkTable;
 deque <ExperientDataClass>::size_type          sz;
 ExperientDataClass                             objData;
 bool				                boolWithALI;
 bool                                  		boolSkip;
 int						index;
 extern Workstation_Groups                      WorkstationGroups;

 timespecDelay.tv_sec 	= 0;
 timespecDelay.tv_nsec 	= 50000000;  // 0.05 sec
 string                                         stringtosend = "";

 
 // both workstation and main worktable have been locked prior to this function call ......

 if (boolReset)
  {
   objData.fClear_Grid_XML_Message(intPosition);
   fTransmit(objData.stringXMLString.c_str(), objData.stringXMLString.length());
  }

  //Lock History Work Table
   
 intRC = sem_wait(&sem_tMutexWorkstationHistory);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexWorkstationHistory, "sem_wait@sem_tMutexWorkstationHistory in WorkStation::fTransmitMainWorktable", 1);}

  sz = vWorkstationHistory.size();
  for (unsigned int i = 0; i < sz; i++)
   {
    boolSkip = false;
    if (!WorkstationGroups.WorkstationGroups.empty())
     {
      index = WorkstationGroups.IndexOfGroupWithTrunk(vWorkstationHistory[i].CallData.intTrunk);
      if (index < 0) {continue;}
      switch (vWorkstationHistory[i].CallData.intTrunk)
       {
        case 98:
                boolSkip = true; break;
        default:
                boolSkip = (!WorkstationGroups.WorkstationGroups[index].WorkstationInGroup(this->intPosition));
       }       

      if(boolSkip) {boolSkip = (!vWorkstationHistory[i].CallData.ConfData.fPositionInHistory(this->intPosition) );}
     }
    if(boolSkip) {continue;}

 //    nanosleep(&timespecDelay, &timespecRemainder);
    stringtosend += fString_To_Transmit(vWorkstationHistory[i].stringXMLString.c_str(), vWorkstationHistory[i].stringXMLString.length());
 //   //cout << stringtosend << endl;
 //   fTransmit(vWorkstationHistory[i].stringXMLString.c_str(), vWorkstationHistory[i].stringXMLString.length());
   }
  if (!stringtosend.empty()) {fTransmit(stringtosend.c_str(), stringtosend.length(), false);}

  sem_post(&sem_tMutexWorkstationHistory);

  
  // load abandoned call list
  stringtosend.clear();
  sz = vMainWorkTable.size();
  for (unsigned int i = 0; i < sz; i++)
   {
    if (vMainWorkTable[i].boolAbandoned)
     {
      boolSkip = false;
      if (!WorkstationGroups.WorkstationGroups.empty())
       {
        index = WorkstationGroups.IndexOfGroupWithTrunk(vMainWorkTable[i].CallData.intTrunk);
        if (index < 0) {continue;}       
        boolSkip = (!WorkstationGroups.WorkstationGroups[index].WorkstationInGroup(this->intPosition));
       }
     if(boolSkip) {continue;}
      nanosleep(&timespecDelay, &timespecRemainder);
      boolWithALI     = (vMainWorkTable[i].ALIData.boolALIRecordReceived);  
      vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_ABANDONED, boolWithALI);
      stringtosend += fString_To_Transmit(vWorkstationHistory[i].stringXMLString.c_str(), vWorkstationHistory[i].stringXMLString.length());
     }
   }
  if (!stringtosend.empty()) {fTransmit(stringtosend.c_str(), stringtosend.length(), false);}

 

 //Lock Main Work Table
// intRC = sem_wait(&sem_tMutexMainWorkTable);
// if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in WorkStation::fTransmitMainWorktable", 1);}
/*
  sz = vMainWorkTable.size();
  for (unsigned int i = 0; i < sz; i++)
   {
    boolWithALI = vMainWorkTable[i].ALIData.boolALIRecordReceived;

    switch(vMainWorkTable[i].intCallStateCode)
     {
      
      case 0: continue;
      case 1: vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_RINGING, boolWithALI); break;
      case 2: case 7:
              if (vMainWorkTable[i].TDDdata.strTDDconversation.empty())         {vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, boolWithALI);}
              else                                                              {vMainWorkTable[i].TDDdata.strTDDstring = vMainWorkTable[i].TDDdata.strTDDconversation;
                                                                                 vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_TDD_CALLER_SAYS, boolWithALI);}
              break;
      case 3: continue;
      case 4: vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_ABANDONED, boolWithALI);break;
      default: continue;
     }
    
    fTransmit(vMainWorkTable[i].stringXMLString.c_str(), vMainWorkTable[i].stringXMLString.length());

  }// end for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
 
// sem_post(&sem_tMutexMainWorkTable);
*/
}



bool WorkStation::fCheckForDuplicatePosition(ExperientDataClass objData)
{
 string		stringError;
 string 	stringTemp;
 unsigned int   intPort;
 string 	stringMessage;
 Call_Data      objCallData;
 MessageClass   objMessage;

 intPort = char2int(objData.stringWRKStationListenPort.c_str());
 if (RemoteIPAddress.stringAddress.empty()) {return false;}
 if (!boolActive)                           {return false;}
 if (objData.stringWRKStationIPAddr.empty()) {SendCodingError( "ExperientDataClass.cpp - WorkStation::fCheckForDuplicatePosition -> blank IP address encountered"); return false;}

 //check if IP Address / Port Number is different from previously registered position
 if ( objData.stringWRKStationIPAddr != RemoteIPAddress.stringAddress)  {
  
   switch(objData.enumWrkFunction)
    {
     case WRK_HEARTBEAT: 	stringMessage = "Heartbeat"     ; 		break;
     case WRK_REBID:     	stringMessage = "Rebid XML"     ;		break;
     case UPDATE_CAD:    	stringMessage = "Update CAD XML"; 		break;
     case WRK_TRANSFER:  	stringMessage = "Initiate Transfer XML"  ; 	break;
     case WRK_CNX_TRANSFER:     stringMessage = "Cancel Transfer XML";		break;
     case WRK_LOG:    		stringMessage = "Send to Log XML";		break;
     case WRK_SEND_INIT_DATA: 	stringMessage = "Init Data Request XML";	break;
     case WRK_CONFERENCE_JOIN:  stringMessage = "Initate Conference Join XML";	break;
     case  WRK_CONFERENCE_LEAVE:stringMessage = "Initate Conference Leave XML";	break;  
     default:             	stringMessage = "Unknown XML";
    }

   stringError = WRK_MESSAGE_714a;
   stringTemp  = FindandReplace(WRK_MESSAGE_714b, "%%%", RemoteIPAddress.stringAddress);
   stringTemp  = FindandReplace(stringTemp, "%%%", int2str(intPosition));
   stringError+= stringTemp;
   stringTemp =  fCreateAlert( stringError , objCallData, objData.stringWRKStationIPAddr, objData.intWorkstation_TCPconnectionID, true);
   objCallData.fLoadPosn(intPosition);
   objMessage.fMessage_Create(LOG_WARNING,713, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                              objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_713, 
                              stringMessage, objCallData.stringPosn,  objData.stringWRKStationIPAddr ); 
   enQueue_Message(WRK,objMessage);

   return true;

 }// end if (WorkStationTable[intPosition].Remote............|| 

 return false;
}

bool  WorkStation::fCheckUserNameChange(ExperientDataClass objData)
{
 Call_Data      objCallData;
 MessageClass   objMessage;
 string		stringError;
 string 	stringTemp;
 string 	stringMessage;

 if (!this->boolActive)              {return false;}
 if (objData.strWindowsUser.empty()) {return false;}
 if (objData.strWindowsUser != this->strWindowsUser)   {
  
  switch(objData.enumWrkFunction)
    {
     case WRK_HEARTBEAT: 	stringMessage = "Heartbeat"     ; 		break;
     case WRK_REBID:     	stringMessage = "Rebid XML"     ;		break;
     case UPDATE_CAD:    	stringMessage = "Update CAD XML"; 		break;
     case WRK_TRANSFER:  	stringMessage = "Initiate Transfer XML"  ; 	break;
     case WRK_CNX_TRANSFER:     stringMessage = "Cancel Transfer XML";		break;
     case WRK_LOG:    		stringMessage = "Send to Log XML";		break;
     case WRK_SEND_INIT_DATA: 	stringMessage = "Init Data Request XML";	break;
     case WRK_CONFERENCE_JOIN:  stringMessage = "Initate Conference Join XML";	break;
     case WRK_CONFERENCE_LEAVE: stringMessage = "Initate Conference Leave XML";	break;  
     default:             	stringMessage = "Unknown XML";
    }

   stringError  = FindandReplace(WRK_MESSAGE_714c, "%%%", this->strWindowsUser);
   stringError  = FindandReplace(stringError, "%%%", int2str(intPosition));
   stringTemp   = FindandReplace(WRK_MESSAGE_714d, "%%%", this->strWindowsUser);
   stringError+= stringTemp;
   stringTemp =  fCreateAlert( stringError , objCallData, objData.stringWRKStationIPAddr, objData.intWorkstation_TCPconnectionID, true);
   objCallData.fLoadPosn(intPosition);
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 714, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                              objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              WRK_MESSAGE_714, objData.strWindowsUser, int2strLZ(intPosition), this->strWindowsUser); 
   enQueue_Message(WRK,objMessage);
   return true;
  
 } //endif 

 return false;
}

bool ExperientDataClass::fUpdateMuteButton(bool boolmuteactive) {
 int 					index;
 WorkStation				NotifyWorkstation;
 extern Telephone_Devices       	TelephoneEquipment;
 extern bool				boolSHOW_MUTE_BUTTON;

// //cout << "mute position -> " << this->intWorkStation << endl;
 if (!boolSHOW_MUTE_BUTTON) {return false;}
 if (!this->intWorkStation) {return false;}

 if(!boolmuteactive) {
  this->stringXMLString = NotifyWorkstation.fUpdateMuteButton("disabled");
 // //cout << this->stringXMLString << endl; 
  return true;
 }

 index = TelephoneEquipment.fIndexWithPositionNumber(this->intWorkStation);
 if (index < 0) {SendCodingError("ExperientDataClass.cpp - fUpdateMuteButton() index < 0" ); return false;}
 experient_nanosleep(THREE_QUARTER_SEC_IN_NSEC);
 TelephoneEquipment.Devices[index].fUpdateLinesInUse();
 if (TelephoneEquipment.Devices[index].vPhoneLines.size() > 1) {
  this->boolBroadcastXMLtoSinglePosition = true;

  switch (TelephoneEquipment.Devices[index].vPhoneLines[1].boolMuted) {
   case true:
       this->stringXMLString = NotifyWorkstation.fUpdateMuteButton("on");
      // //cout << this->stringXMLString << endl;  
       return true;
   case false:
       this->stringXMLString = NotifyWorkstation.fUpdateMuteButton("off");
      // //cout << this->stringXMLString << endl; 
       return true;
  }

 }
 return false;
}


void ExperientDataClass::fAddSentenceToTDDConversation(string strInput)
{
 size_t       						etxfound;
 string       						strOverBuffer;
 MessageClass 						objMessage;
 extern ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1];

 if (strInput.empty()){return;}
 TextData.TDDdata.strTDDconversation += strInput;
 TextData.strConversation += charETX;
/*
 while (TextData.TDDdata.strTDDconversation.length() > MAX_TDD_CONVERSATION_SIZE)
  {
   // find <etx> and delete to that point if not found clear the string ...
   etxfound = TextData.TDDdata.strTDDconversation.find(charETX,0);
   if (etxfound == string::npos){TextData.TDDdata.strTDDconversation.clear();}
   else 
    {
     if (enumThreadSending == MAIN)
      {
       //main has all the data .....
       strOverBuffer = TextData.TDDdata.strTDDconversation.substr(0, etxfound);
       objMessage.fMessage_Create(LOG_WARNING, 361, fCallData(), ANIPort[intANIPortCallReceived].UDP_Port.fPortData(), ANI_MESSAGE_361w, strOverBuffer,"","","","","", NORMAL_MSG, TDD_CONVERSATION );
       enQueue_Message(ANI,objMessage);
      }
     TextData.TDDdata.strTDDconversation.erase(0,etxfound+1);
    }
  }
*/
}

void ExperientDataClass::fAddSentenceToTextConversation(string strInput)
{
 size_t       						etxfound;
 string       						strOverBuffer;
 MessageClass 						objMessage;
 extern ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1];

 if (strInput.empty()){return;}

 TextData.strConversation += strInput;
 TextData.strConversation += charETX;
/*
 while (TextData.strConversation.length() > MAX_TDD_CONVERSATION_SIZE)
  {
   // find <etx> and delete to that point if not found clear the string ...
   etxfound = TextData.strConversation.find(charETX,0);
   if (etxfound == string::npos){TextData.strConversation.clear();}
   else 
    {
     if (enumThreadSending == MAIN)
      {
       //main has all the data .....
       strOverBuffer = TextData.strConversation.substr(0, etxfound);
       objMessage.fMessage_Create(LOG_WARNING, 361, fCallData(), ANIPort[intANIPortCallReceived].UDP_Port.fPortData(), ANI_MESSAGE_361w, strOverBuffer,"","","","","", NORMAL_MSG, TDD_CONVERSATION );
       enQueue_Message(ANI,objMessage);
      }
     TextData.strConversation.erase(0,etxfound+1);
    }
  }
*/
}


bool ExperientDataClass::fCheckTDDbuffer()
{
 if (!TextData.TDDdata.fSetTDDSendButton())      {return false;}
 if (TextData.TDDdata.strTDDCallerBuffer.empty()){return false;}

// TextData.TDDdata.strTDDstring = Create_Message(TDD_CALLER_SAYS_PREFIX, get_Local_Time_Stamp());
// TextData.TDDdata.strTDDstring += TextData.TDDdata.strTDDCallerBuffer;
 //TextData.TDDdata.strTDDstring += charETX;
 TextData.TDDdata.strTDDstring = TextData.TDDdata.strTDDCallerBuffer;
 //fAddSentenceToTextConversation(TextData.TDDdata.strTDDstring);
 if(TextData.TDDdata.strTDDstring.length() > ((size_t)intTDD_CPS_THRESHOLD)) {TextData.TDDdata.boolTDDthresholdMet = true;}
 TextData.TDDdata.strTDDCallerBuffer.clear();
 
 return true;
}

void ExperientDataClass::fTurnOnTDDmode()
{
 this->enumANIFunction = TDD_MODE_ON;
 Queue_ANI_Event(this); 

 return;
}

void ExperientDataClass::fLogTDDConversation()
{
 MessageClass 						objMessage;
 extern ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1];

 if (TDDdata.strTDDconversation.empty()){return;}
 if (!intANIPortCallReceived)           {return;}

 objMessage.fMessage_Create(LOG_CONSOLE_FILE, 361, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , fCallData(), objBLANK_TEXT_DATA,
                            ANIPort[intANIPortCallReceived].UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                            ANI_MESSAGE_361, TDDdata.strTDDconversation,"","","","","", NORMAL_MSG, TDD_CONVERSATION );
 enQueue_Message(ANI,objMessage);

 return;
}

void ExperientDataClass::fLogTextConversation()
{
 MessageClass 						objMessage;
 int							intRC;
 string							strEncodedTextmessage;
 size_t                                         	sz;
 extern ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1];
 extern deque <ExperientDataClass>              	vWorkstationHistory;
 extern sem_t						sem_tMutexWorkstationHistory;

 if (!intANIPortCallReceived)           {return;}
 switch (this->TextData.TextType)
  {
   case TDD_MESSAGE: case NO_TEXT_TYPE:
        if (TextData.strConversation.empty()){return;}

        this->CallData.eJSONEventCode = TEXT_LOG_CONVERSATION;
        this->TextData.TextType = TDD_MESSAGE;
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 361, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , this->fCallData(), this->TextData,
                                   ANIPort[intANIPortCallReceived].UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                   ANI_MESSAGE_361, this->TextData.strConversation,"","","","","", NORMAL_MSG, TDD_CONVERSATION );
        enQueue_Message(ANI,objMessage);
        break;
   case MSRP_MESSAGE:
        if (TextData.strConversation.empty()){return;}
        this->CallData.eJSONEventCode = TEXT_LOG_CONVERSATION;        
        // //cout << "before -> " << TextData.strConversation << endl;
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 361, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , this->fCallData(), this->TextData,
                                   ANIPort[intANIPortCallReceived].UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                   ANI_MESSAGE_361t, this->TextData.strConversation,"","","","","", NORMAL_MSG, TDD_CONVERSATION );
        enQueue_Message(ANI,objMessage);
        break;
  }
// Update History ->
 intRC = sem_wait(&sem_tMutexWorkstationHistory);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexWorkstationHistory, "sem_wait@sem_tMutexWorkstationHistory in Queue_WRK_Event()", 1);}

  sz = vWorkstationHistory.size();
  for(unsigned int i = 0; i < sz; i++){
   if (vWorkstationHistory[i].CallData.intUniqueCallID == this->CallData.intUniqueCallID){
    vWorkstationHistory[i].TextData = this->TextData;
    vWorkstationHistory[i].TextData.boolTextWindowActive             = false;                   
    vWorkstationHistory[i].TextData.boolTextSendButton               = false;
/*
    switch(TextData.TextType)
     {
      case TDD_MESSAGE:
       vWorkstationHistory[i].TextData.TDDdata.strTDDstring = this->TextData.strConversation;
       break;
      case MSRP_MESSAGE:
       vWorkstationHistory[i].TextData.SMSdata.strSMSmessage = this->TextData.strConversation;
     //  //cout << "HERE " << i << endl;
       //cout << this->TextData.strConversation << endl;
     //  //cout << "HERE " << i << endl;
       break;
      default:
       break;
     }
*/
   }
  }
 sem_post(&sem_tMutexWorkstationHistory);
 return;
}

string ExperientDataClass::ECRF_URN_Service_Request(string strService, bool boolListService) {
 string                 ReturnString;
 string                 ServiceString;
 XMLNode  		MainNode, CurrentNode, PresenceNode, TupleNode, StatusNode, GeoPrivNode, locationinfoNode, pointNode, circleNode, civicNode, EllipseNode, ArcBandNode, PolygonNode;
 XMLNode                SphereNode, EllipsoidNode, PrismNode;
 XMLResults         	xe;
 string                 strName;
 string                 strValue;
 string                 strPoint, strCircle, strCivic, strEllipse, strArcBand, strPolygon;
 string                 strSphere, strEllipsoid, strPrism;
 int                    iNumChildNodes;
 int                    iNumAttributes;
 string                 strXMLNS = "";
 char *                 cptrResponse = NULL;
 
 size_t                 found; 
 string                 strSRSName;
 bool                   boolPoint2D = false;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 MainNode = XMLNode::parseString(this->ALIData.I3Data.strPidfXMLData.c_str(),NULL,&xe);
 
 if (xe.error) {return "";} 
 // check for Top Node ..
 if (MainNode.getName())   {strName = MainNode.getName();}
 else                      {return "";}

// //cout << "ECRF_URN_Service_Request main node is -> " << strName << endl;

 if (RemoveXMLnamespace(strName) == "presence" ) {
  PresenceNode = MainNode;
 }
 else {
  PresenceNode =  LoadChildNode(MainNode, "presence");
 }

 if (PresenceNode.isEmpty())              {
  //cout <<  "ExperientDataClass::ECRF_URN() PresenceNode Missing" << endl; 
  return ""; 
 }

 // get all the namespaces .....
 
 iNumAttributes = PresenceNode.nAttribute();

 for (int i = 0; i < iNumAttributes; i++) {
  strName = PresenceNode.getAttributeName(i);
  //cout << strName << endl;
  found = strName.find("xmlns:");
  //cout << found << endl;
  if (found == 0) {
   strXMLNS += strName;
   strXMLNS += "=\"";
   strValue = PresenceNode.getAttributeValue(i);
   strXMLNS += strValue;
   strXMLNS += "\" ";
  }
 }

 //cout << "NAMESPACES -> " << strXMLNS << endl;

 TupleNode =  LoadChildNode(PresenceNode, "tuple");
 if (TupleNode.isEmpty())              {
  //cout <<  "ExperientDataClass::ECRF_URN() TupleNode Missing" << endl; 
  return ""; 
 }

 StatusNode =  LoadChildNode(TupleNode, "status");
 if (StatusNode.isEmpty())              {
  //cout <<  "ExperientDataClass::ECRF_URN() StatusNode Missing" << endl; 
  return ""; 
 }

 GeoPrivNode =  LoadChildNode(StatusNode, "geopriv");
 if (GeoPrivNode.isEmpty())              {
  //cout <<  "ExperientDataClass::ECRF_URN() GeoPrivNode Missing" << endl; 
  return ""; 
 }

 locationinfoNode = LoadChildNode(GeoPrivNode, "location-info");
 if (locationinfoNode.isEmpty())              {
  //cout <<  "ExperientDataClass::ECRF_URN() locationinfoNode Missing" << endl; 
  return ""; 
 }

// there can be more than 1 but we will send the first one .....
// Possible shapes Point 2 + 3D, Polygon 2D, Circle 2D, Ellipse, 2D Arc band 2D, Sphere 3D, Ellipsoid 3D, Prism 3D
 
 circleNode    = LoadChildNode(locationinfoNode, "Circle");
 pointNode     = LoadChildNode(locationinfoNode, "Point");
 civicNode     = LoadChildNode(locationinfoNode, "civicAddress");
 EllipseNode   = LoadChildNode(locationinfoNode, "Ellipse");
 ArcBandNode   = LoadChildNode(locationinfoNode, "ArcBand");
 PolygonNode   = LoadChildNode(locationinfoNode, "Polygon");
 SphereNode    = LoadChildNode(locationinfoNode, "Sphere");
 EllipsoidNode = LoadChildNode(locationinfoNode, "Ellipsoid");
 PrismNode     = LoadChildNode(locationinfoNode, "Prism");


 strPoint = strCircle = strCivic = strSRSName = strEllipse = strArcBand = strPolygon = strSphere = strEllipsoid = strPrism ="";
 
 ReturnString.reserve(2048);

 if(!circleNode.isEmpty()) {
  if (circleNode.getAttribute("srsName")) {
   strSRSName = circleNode.getAttribute("srsName");
  }
  cptrResponse = circleNode.createXMLString();
  if (cptrResponse != NULL) {
   strCircle = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!pointNode.isEmpty()) {
  if (pointNode.getAttribute("srsName")) {
   strSRSName = pointNode.getAttribute("srsName");
   found = strSRSName.find("EPSG::4326");
   if (found != string::npos) {
    boolPoint2D = true;
   }
  }
  cptrResponse = pointNode.createXMLString();
  if (cptrResponse != NULL) {
   strPoint = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!civicNode.isEmpty()) {
  cptrResponse = civicNode.createXMLString();
  if (cptrResponse != NULL) {
   strCivic = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!EllipseNode.isEmpty()) {
  cptrResponse = EllipseNode.createXMLString();
  if (cptrResponse != NULL) {
   strEllipse = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!ArcBandNode.isEmpty()) {
  cptrResponse = ArcBandNode.createXMLString();
  if (cptrResponse != NULL) {
   strArcBand = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!PolygonNode.isEmpty()) {
  cptrResponse = PolygonNode.createXMLString();
  if (cptrResponse != NULL) {
   strPolygon = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!PrismNode.isEmpty()) {
  cptrResponse = PrismNode.createXMLString();
  if (cptrResponse != NULL) {
   strPrism = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!SphereNode.isEmpty()) {
  cptrResponse = SphereNode.createXMLString();
  if (cptrResponse != NULL) {
   strSphere = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if(!EllipsoidNode.isEmpty()) {
  cptrResponse = EllipsoidNode.createXMLString();
  if (cptrResponse != NULL) {
   strEllipsoid = cptrResponse;
   free(cptrResponse); 
  }
  cptrResponse = NULL;
 }

 if ((!strPoint.length())&&(!strCivic.length())&&(!strCircle.length())&&(!strEllipse.length())&&(!strArcBand.length())&&(!strPolygon.length())&&
    (!strSphere.length())&&(!strEllipsoid.length())&&(!strPrism.length())) {
  //cout <<  "ExperientDataClass::ECRF_URN() No Location Data" << endl; 
  return "";
 }

 ReturnString =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
 if (boolListService) {
  ReturnString += "<listServicesByLocation ";
 } else {
  ReturnString += "<findService ";
 }
 ReturnString += "xmlns=\"urn:ietf:params:xml:ns:lost1\" ";
 if (strXMLNS.length()) {
   ReturnString += strXMLNS;
 }
 ReturnString += "recursive=\"true\">\n";
 if (strPoint.length()) {
   ReturnString += "<location id=\"";
   ReturnString += this->ALIData.I3Data.strTupleID;
   if (boolPoint2D) {
    ReturnString += "\" profile=\"geodetic-2d\">\n";
   } 
   else {
    ReturnString += "\" profile=\"geodetic-3d\">\n";
   }
   ReturnString += strPoint;
   ReturnString += "</location>\n";     
 }
 else if (strCivic.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID;
  ReturnString += "\" profile=\"civic\">\n";
  ReturnString += strCivic;
  ReturnString += "</location>\n";
 }
 else if (strCircle.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-2d\">\n";
  ReturnString += strCircle;
  ReturnString += "</location>\n";
 }
 else if (strPolygon.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-2d\">\n";
  ReturnString += strPolygon;
  ReturnString += "</location>\n";
 }
 else if (strEllipse.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-2d\">\n";
  ReturnString += strEllipse;
  ReturnString += "</location>\n";
 }
 else if (strArcBand.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-2d\">\n";
  ReturnString += strArcBand;
  ReturnString += "</location>\n";
 }
 else if (strSphere.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-3d\">\n";
  ReturnString += strSphere;
  ReturnString += "</location>\n";
 }
 else if (strEllipsoid.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-3d\">\n";
  ReturnString += strEllipsoid;
  ReturnString += "</location>\n";
 }
 else if (strPrism.length()) {
  ReturnString += "<location id=\"";
  ReturnString += this->ALIData.I3Data.strTupleID; 
  ReturnString += "\" profile=\"geodetic-3d\">\n";
  ReturnString += strPrism;
  ReturnString += "</location>\n";
 }

 ReturnString += "<service>";
 ReturnString += strService; 
 ReturnString += "</service>\n";

 if (boolListService) {
  ReturnString += "</listServicesByLocation>\n";
 } else {
  ReturnString += "</findService>\n";
 }

 //cout << ReturnString << endl;
 return ReturnString;
}
        

/*

bool ExperientDataClass::fLoadTransferExtensionIntoPositionAndConference()
{
 string strData;

 if(CallData.TransferData.strTransfertoNumber.length() != 4)             {return false;}
 if(CallData.TransferData.intTransferNumberType != SIP_LOCAL)            {return false;}
 
 strData = CallData.TransferData.strTransfertoNumber.substr(2,2);
 if (!fLoad_Position(SOFTWARE,ANI,strData,0)){return false;}
 CallData.fConferenceStringAdd(POSITION_CONF, CallData.intPosn);
 return true;
}
*/

string Conf_Data::fOnHoldTimeStampsStrippedOfPositions()
{
 string strReturn = this->strOnHoldPosAndTime;
 string strPeriod = ".";
 size_t foundPrefix, foundPeriod;

 if (this->strOnHoldPosAndTime.empty()) {return "";}

 do {
  foundPrefix = strReturn.find_first_of (ANI_CONF_MEMBER_POSITION_PREFIX, 0);
  if (foundPrefix==string::npos) {break;}
  foundPeriod = strReturn.find_first_of(strPeriod, foundPrefix);
  if (foundPeriod == string::npos) {break;}

  strReturn.erase(foundPrefix, (foundPeriod - foundPrefix)+1);

 } while (foundPrefix!=string::npos);

 return strReturn;
}


bool Conf_Data::fAnyPositionsActiveOnCall()
{
 size_t found;

 found = strActiveConferencePositions.find(ANI_CONF_MEMBER_POSITION_PREFIX);

 return (found != string::npos);

}
bool Conf_Data::fAnyPositionsOnHold()
{
 size_t found;

 found = strOnHoldPositions.find(ANI_CONF_MEMBER_POSITION_PREFIX);

 return (found != string::npos);

}

void Conf_Data::fClear()
{
 strParticipant.clear();
 strActiveConferencePositions.clear();
 strHistoryConferencePositions.clear();
 strOnHoldPositions.clear();
 strOnHoldPosAndTime.clear();
 strParkParticipant.clear();
 strParkTime.clear();
 vectConferenceChannelData.clear();
}

bool Conf_Data::fPositionActive(int i)
{
 size_t found;
 string strKey = ANI_CONF_MEMBER_POSITION_PREFIX;

 strKey += int2str(i);

 found = strActiveConferencePositions.find(strKey);
 if (found == string::npos) {return false;}
 else                       {return true;}

}

bool Conf_Data::fPositionInHistory(int i)
{
 size_t found;
 string strKey = ANI_CONF_MEMBER_POSITION_PREFIX;

 strKey += int2str(i);
 found = strHistoryConferencePositions.find(strKey);
 if (found == string::npos) {return false;}
 else                       {return true;}

}

void Conf_Data::fSetPositionOnHoldinConfVector(int i) {
 int index;

 index = this->fFindConferenceDataFromPositionNumber(i);
 if (index < 0) {return;}

 this->vectConferenceChannelData[index].boolOnHold = true;

 return;
}

void Conf_Data::fSetPositionOffHoldinConfVector(int i) {
 int index;

 index = this->fFindConferenceDataFromPositionNumber(i);
 if (index < 0) {return;}

 this->vectConferenceChannelData[index].boolOnHold = false;

 return;
}

int Conf_Data::fNumofPositionsInHistory() {
int             iNumberinHistory = 0;
extern int 	intNUM_WRK_STATIONS;

 for (int i = 1; i <= intNUM_WRK_STATIONS; i++) {
  if (this->fPositionInHistory(i)) {
   iNumberinHistory++;
  }
 }
 return iNumberinHistory;
}

bool Conf_Data::fRemoveTransferFromHistory(string strConfData)
{
 size_t found;
 string strRemove;

 strRemove = charTAB;
 strRemove += strConfData;
 strRemove += charTAB;
 found = strHistoryConferencePositions.find(strRemove);

 if (found == string::npos) {return false;}

 strHistoryConferencePositions.erase(found , strRemove.length());
 
 return true;
}

bool Conf_Data::fPositionInHistory()
{
 size_t found;
 
 found = strHistoryConferencePositions.find(ANI_CONF_MEMBER_POSITION_PREFIX);
 if (found == string::npos) {return false;}
 else                       {return true;}
}

int Conf_Data::fFindConferenceData(string strUniqueId)
{
 vector<Channel_Data>::size_type        sz = vectConferenceChannelData.size();

 if (vectConferenceChannelData.empty())                           {return -1;}
 if (strUniqueId.empty())                                         {return -1;}
 for( unsigned int i = 0; i < sz; i++) 
   {
     if (vectConferenceChannelData[i].strChannelID == strUniqueId) {return i;}
   }
 return -1;
}

int Conf_Data::fFindConferenceDataFromCallIDNumber(string strNumber)
{
 vector<Channel_Data>::size_type        szConfVector = vectConferenceChannelData.size();
 int                                     i = szConfVector-1;
 vector<Channel_Data>::reverse_iterator rit;

 if (vectConferenceChannelData.empty())                                             {return -1;}
 for(  rit=vectConferenceChannelData.rbegin() ; rit < vectConferenceChannelData.rend(); ++rit) 
   {
    if (rit->strCallerID == strNumber)                  {return i;}
    i--;
   }
 return -1;
}

int Conf_Data::fFindConferenceDataFromConfDisplay(string strLegend)
{
 vector<Channel_Data>::size_type        szConfVector = vectConferenceChannelData.size();
 int                                     i = szConfVector-1;
 vector<Channel_Data>::reverse_iterator rit;

 if (vectConferenceChannelData.empty())                                             {return -1;}
 for(  rit=vectConferenceChannelData.rbegin() ; rit < vectConferenceChannelData.rend(); ++rit) 
   {
    if (rit->strConfDisplay == strLegend)                  {return i;}
    i--;
   }
 return -1;
}

int Conf_Data::fFindConferenceDataFromPositionNumber(int intPosition)
{
 vector<Channel_Data>::size_type        szConfVector = vectConferenceChannelData.size();
 int                                     i = szConfVector-1;
 vector<Channel_Data>::reverse_iterator rit;

 if (vectConferenceChannelData.empty())                                             {return -1;}
 for(  rit=vectConferenceChannelData.rbegin() ; rit < vectConferenceChannelData.rend(); ++rit) 
   {
    if (rit->iPositionNumber == intPosition)                  {return i;}
    i--;
   }
 return -1;
}

void Conf_Data::fConfDataVectorRemoveDuplicateUUID() {
 vector<Channel_Data>::size_type        szConfVector = vectConferenceChannelData.size();
 string                                 strChannelID;

// Erase the first record keep the second (only one erase is allowed in this function !

 if (vectConferenceChannelData.empty())                                             {return;}

 for (unsigned int i = 0; i < szConfVector; i++) {
  
  strChannelID = vectConferenceChannelData[i].strChannelID;
 // //cout << strChannelID << endl;
  for ( unsigned int j = i+1; j < szConfVector; j++) { 
     ////cout << " equal to ? " << vectConferenceChannelData[j].strChannelID << endl;
     if (vectConferenceChannelData[j].strChannelID == strChannelID) {
       vectConferenceChannelData.erase(vectConferenceChannelData.begin()+i);
      // //cout << "erase !!!!!" << endl;
       //only one erase ..... or boom
       return;         
     }
  }  
 }   
 return;
}








void Conf_Data::fRemoveDuplicatePositionChannels(int intPosition)
{
 vector<Channel_Data>::size_type        szConfVector = vectConferenceChannelData.size();
 int                                     i = szConfVector-1;
 int                                     j;
 vector<Channel_Data>::reverse_iterator rit, rit_dup;

 // keep the last Position in the list
 if (intPosition <= 0)                                                              {return;}
 if (vectConferenceChannelData.empty())                                             {return;}

 for(  rit=vectConferenceChannelData.rbegin() ; rit < vectConferenceChannelData.rend(); ++rit) {
    if (rit->iPositionNumber == intPosition) {
     rit_dup = rit; ++rit_dup; j = i;
     while (rit_dup < vectConferenceChannelData.rend() ) {
      j--;
      if (rit_dup->iPositionNumber == intPosition) { 
       vectConferenceChannelData.erase(vectConferenceChannelData.begin()+j);
       //only one erase ..... or boom
      // //cout << "erased pos " << intPosition << " at index " << j << endl;
       return;
      }


      ++rit_dup;
     }    
     
    }
 i--;
 }
 return;
}


/*
bool Conf_Data::fFindSamePositionInConfvector(string strChannelID)
{
 vector<Channel_Data>::size_type        sz = vectConferenceChannelData.size();
 int                                    index;

 if (vectConferenceChannelData.empty())                           {return false;}
 if (strChannelID.empty())                                        {return false;}

 index = fFindConferenceData(strChannelID);
 if (index < 0)                                                   {return false;}





 return false;
}

*/



void ALI_Data::fClear()
{
   eJSONEventCode                     = NO_ALI_FUNCTION_DEFINED;
   enumALIBidType                     = NO_BID;
   stringALiRequestKey                = "";
   stringType                         = "";
   intALIState                        = 0;
   stringAliText                      = "";
   stringAliTextMessage               = "";
   intAliTextMessageLength            = 0;
   strCADmessageWithoutHeader         = "";
   intCADmessageWithoutHeaderLength   = 0;
   stringALiTestMessageWOCR           = "";
   intAliTextMessageWOCRLength        = 0;
   strCADmessageWithoutHeaderWOCR     = "";
   intCADmessageWithoutHeaderWOCRLength = 0;
   strCADmessageSent                  = "";
   stringAliRequest                   = "";
   stringAutoRepeatAliRequest         = "";
   strALIRequestTrunk                 = "";
   boolALI_Test_Key_Was_Bid           = false;
   intALIPortPairUsedtoTX             = 0;
   intALIBidIndex                     = 0;
   intTransactionIDBid[1]             = 0;
   intTransactionIDBid[2]             = 0;
   intE2RequestLength                 = 0;
   intLastDatabaseBid                 = 0;
   bool_LF_AS_CR                      = false;
   for (int i = 1; i <= MAX_NUMBER_ALI_DATABASES; i++){ intFoundinDatabase[i] = 0;}
   for (int i = 0; i < E2_MAX_REQUEST_LENGTH; i++)    { chE2BidRequest[i] = 0x00;}
   intNumberOfInitialBids             = 0;
   intNumberOfRetryBids               = 0;  
   boolALIRecordActive                = false;
   boolALIRecordFail                  = false;
   boolALIRecordACKReceived[1]        = false;
   boolALIRecordACKReceived[2]        = false;
   boolALIRecordRetransmit[1]         = true;
   boolALIRecordRetransmit[2]         = true;
   stringALIReceivedTimeStamp         = "";
   stringALITimeoutTimeStamp          = "";
   stringALIRebidTimeStamp            = "";   
   boolALIRecordReceived              = false;
   timespecTimeAliAutoRebidOK.tv_sec  = 0;
   timespecTimeAliAutoRebidOK.tv_nsec = 0;
   intALITimeoutSec                   = 0;
   E2Data.fClear();
   I3Data.fClear();
}

bool ALI_Data::fIsPhaseTwoALI() {
 string                  strSearch;
 size_t                  found;

 strSearch = CLASS_OF_SERVICE_ABBV_H;
 
 //first check if we have a class of service var set
 if (this->strClassOfService.length()) {

  if (this->strClassOfService == strSearch ) { return true;}
  else                                       { return false;}

 }

 found = this->stringAliText.find(strSearch);
 if (found == string::npos) {return false;}
 else                       {return true;}
}

int ALI_Data::fHowManyDatabasesCanbeBid()
{
 int intCount = 0;
 for (int i = 1; i <= MAX_NUMBER_ALI_DATABASES; i++){ if(intFoundinDatabase[i]){intCount++;}}
 return intCount;
}

bool Phone_ALI::fupdatePhoneALIFile(int i, string strInput) {

 string  strFilePathName;
 bool    boolBadOpen;
 bool    boolBadWrite;
 string  strErrorMessagePrefix = "ExperientDataClass.cpp - ";  
 string  strErrorSuffix        = " in fn Phone_ALI::fupdatePhoneALIFile()";
 string  strError;
 string  strErrorNum;
 string  strIssue;

 if (i > NUM_WRK_STATIONS_MAX+1) {return false;}
 if (i < 0)                      {return false;}

 strFilePathName = charPHONE_ALI_FILE_PATH_PREFIX;
 strFilePathName += "/";
 strFilePathName += int2strLZ(i);
 strFilePathName += "/";
 strFilePathName += "ali.html";

 this->PhoneALIfile[i].clear(); 
 this->PhoneALIfile[i].open( strFilePathName.c_str(), ios_base::trunc);
 boolBadOpen = (!this->PhoneALIfile[i].is_open());

 if (boolBadOpen) { 
  strIssue    = "unable to open file errno-";
  strErrorNum = int2strLZ(errno); 
  strError  = strErrorMessagePrefix;
  strError += strIssue;
  strError += strErrorNum;
  strError += " ";
  strError += strFilePathName;
  strError += strErrorSuffix;
  SendCodingError(strError);
  this->PhoneALIfile[i].close();
  return false;
 }

 this->PhoneALIfile[i] << strInput << endl;
 boolBadWrite = this->PhoneALIfile[i].bad();

 if (boolBadWrite) { 
 strIssue    = "unable to write file errno-";
  strErrorNum = int2strLZ(errno); 
  strError  = strErrorMessagePrefix;
  strError += strIssue;
  strError += strErrorNum;
  strError += " ";
  strError += strFilePathName;
  strError += strErrorSuffix;
  SendCodingError(strError);
  this->PhoneALIfile[i].close();
  return false;
 }

  this->PhoneALIfile[i].close();
  return true;
}

string ALI_Data::fALItoHTML(int iPosition) {

 XMLNode                        MainNode, HTMLnode, HeadNode, BodyNode, PreNode, UlNode, LiNode, TitleNode, MetaNode;
 string                 	strResponse;
 char*                  	cptrResponse = NULL;
 string                         strALIresponse;
 Phone_ALI                      PhoneALIfile;
 string                         strALIrow ="";
 extern ALISteering             ALI_Steering[intMAX_ALI_DATABASES + 1 ];

 //fred
 if ((this->intLastDatabaseBid > intMAX_ALI_DATABASES)||(this->intLastDatabaseBid < 0)) {
      SendCodingError("ExperientDataClass.cpp ALI_Data::fALItoHTML() intLastDatabaseBid out of range!"); 
  return "";
 }

 MainNode=XMLNode::createXMLTopNode("!DOCTYPE html", false);
 HTMLnode  = MainNode.addChild("html");
 HeadNode  = HTMLnode.addChild("head");
 BodyNode  = HTMLnode.addChild("body");
// PreNode   = BodyNode.addChild("pre");
// UlNode    = BodyNode.addChild("ul");
// UlNode.addAttribute("style","list-style: none;");
 TitleNode = HeadNode.addChild("title");
 TitleNode.addText("Location Info");
 MetaNode  = HeadNode.addChild("meta");
 MetaNode.addAttribute("http-equiv", "refresh");
 MetaNode.addAttribute("content","10"); 

   if (this->stringAliText.length()) {
   // strALIresponse = ASCII_String((char*)this->stringAliText.c_str(), this->stringAliText.length()); 
    strALIresponse = this->stringAliText; 
    strALIresponse += charETX;
    strALIresponse = ReformatALIOutputToHTML(strALIresponse, this->bool_LF_AS_CR);
    strALIresponse = ConvertWhiteSpacestoHTML(strALIresponse);
   }
   else if ((this->I3Data.strNGALIRecord.length())&&(!boolLEGACY_ALI_ONLY)) {
    strALIresponse = this->I3Data.strNGALIRecord; /*//cout << "fALItoHTML -B" << endl; //cout << this->I3Data.strNGALIRecord << endl; //cout <<"length ->" << this->I3Data.strNGALIRecord.length() << endl;*/

   }
   else if ((this->I3Data.strALI30WRecord.length())&&(!boolLEGACY_ALI_ONLY)) {
   strALIresponse = this->I3Data.strALI30WRecord; /*//cout << "fALItoHTML -C" << endl; //cout << this->I3Data.strALI30WRecord << endl; //cout <<"length ->" << this->I3Data.strALI30WRecord.length() << endl;*/
   }
   else if ((this->I3Data.strALI30XRecord.length())&&(!boolLEGACY_ALI_ONLY)) {
   strALIresponse = this->I3Data.strALI30XRecord; /*//cout << "fALItoHTML -C" << endl; //cout << this->I3Data.strALI30WRecord << endl; //cout <<"length ->" << this->I3Data.strALI30WRecord.length() << endl;*/
   }   
   else if ((this->I3Data.strPidfXMLData.length())&&(!boolLEGACY_ALI_ONLY)) {
   strALIresponse = this->I3Data.strPidfXMLData; /*//cout << "fALItoHTML -D" << endl; //cout << this->I3Data.strPidfXMLData << endl; //cout <<"length ->" << this->I3Data.strPidfXMLData.length() << endl; */
   }
   else {
    if(!boolLEGACY_ALI_ONLY) {/*SendCodingError("ExperientDataClass.cpp ALI_Data::fALItoHTML() No ALI Data to send!"); */} // probably put no record ....
   }
/*
 int i = 1;
 strALIrow = this->fRowOfALIdata(i, ALI_Steering[this->intLastDatabaseBid]);
 while (!strALIrow.empty()) {

  LiNode = UlNode.addChild("li");
  LiNode.addText(strALIrow.c_str());
  i++;
  strALIrow = ConvertWhiteSpacestoHTML(this->fRowOfALIdata(i, ALI_Steering[this->intLastDatabaseBid])); 
 }
*/
 BodyNode.addText(strALIresponse.c_str());

 cptrResponse = MainNode.createXMLString();
 if (cptrResponse != NULL)   {
  strResponse = cptrResponse;
  free(cptrResponse);
 }
 cptrResponse = NULL;

 strResponse = FindandReplaceALL(strResponse, "&lt;br&gt;", "<br>");
 strResponse = FindandReplaceALL(strResponse, "&amp;", "&");

 // write to file .....
 PhoneALIfile.fupdatePhoneALIFile(iPosition, strResponse);

 return strResponse;
}

void ExperientDataClass::fUpdatePhoneALI() {

 for (int i = 1; i < intNUM_WRK_STATIONS; i++) {
  if (this->CallData.ConfData.fPositionActive(i)) {
   this->ALIData.fALItoHTML(i);
  }
 }
 return;
}


/****************************************************************************************************
*
* Name:
*  Function: string ALI_Data::fALIStateString();
*
* Description:
*   A ALI_Data Utility Function 
*
*
* Details:
*   This function converts an ALI state code into a ALI Status string. If the code
*   does not exist the NULL string is returned.
*          
*    
* Parameters:
*   none                           
*
* Variables:
*  intALIState                                Local  - <ALI_Data> integer   Coded ALI state
*  intNumberOfRetryBids                       Local  - <ALI_Data> integer   count of ALI rebid attempts
*  strSuffix                                  Local  - <cstring>            used for string manipulation
*  XML_STATUS_MSG_ALI_BID_RETRY               Global - <defines.h>
*  XML_STATUS_MSG_ALI_MANUAL_BID              Global - <defines.h>
*  XML_STATUS_MSG_MAN_ALI_TIMEOUT             Global - <defines.h>
*  XML_STATUS_MSG_AUTO_ALI_REBID              Global - <defines.h> 
*  XML_STATUS_MSG_AUTO_ALI_TIMEOUT            Global - <defines.h>
*  XML_STATUS_MSG_ALI_REBID                   Global - <defines.h>
*  XML_STATUS_MSG_ALI_TIMEOUT                 Global - <defines.h>
*  XML_STATUS_MSG_ALI_RECEIVED                Global - <defines.h>
*  XML_STATUS_MSG_MAN_ALI_RECEIVED            Global - <defines.h>
*  XML_STATUS_MSG_AUTO_ALI_RECEIVED           Global - <defines.h>
*                                                                          
* Functions:
*   int2str()                                 Global - <globalfunctions.h>              (string)
*
* AUTHOR: 2/9/2009 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2009 Experient Corporation
****************************************************************************************************/
string ALI_Data::fALIStateString()
{
 string  strSuffix;

 switch(intALIState)
  {
   case 79: return XML_STATUS_MSG_ALI_NOT_ALLOWED;
   case 80:
            if (intNumberOfRetryBids < 2){return XML_STATUS_MSG_ALI_BID_RETRY;}
            strSuffix = " (" + int2str(intNumberOfRetryBids) + ")";
            return XML_STATUS_MSG_ALI_BID_RETRY + strSuffix;
   case 87: return XML_STATUS_MSG_ALI_MANUAL_BID;
   case 88: return XML_STATUS_MSG_MAN_ALI_TIMEOUT;
   case 89: return XML_STATUS_MSG_AUTO_ALI_REBID ;
   case 90: return XML_STATUS_MSG_AUTO_ALI_TIMEOUT;
   case 91: return XML_STATUS_MSG_ALI_REBID;
   case 92: return XML_STATUS_MSG_ALI_TIMEOUT;
   case 93: return XML_STATUS_MSG_ALI_RECEIVED;
   case 94: return XML_STATUS_MSG_MAN_ALI_RECEIVED;
   case 95: return XML_STATUS_MSG_AUTO_ALI_RECEIVED;
   default:
           return "";
  }// end switch
}

bool ALI_Data::fLoadEncodedALItext(string strInput)
{
 if (!strInput.length()) {return false;}
 MessageClass           objMessage;
 XMLParserBase64Tool    b64;
 unsigned char*         ptrUcharDecodedTXT;
 int                    intLength;
 XMLError               xe;
 string                 stringError;
 Port_Data              objPortData;
 bool                   boolError = false;

 ptrUcharDecodedTXT    = b64.decode(strInput.c_str(),&intLength, &xe);

 if(xe == eXMLErrorNone)   {
  
   for (int i=0; i < intLength; i++){this->stringAliText += ptrUcharDecodedTXT[i];}
 }
 else   {
  
   objPortData.fLoadPortData(ALI, 1);
   stringError = ConvertXMLError2String(xe);
   objMessage.fMessage_Create(LOG_WARNING,276, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              ALI_MESSAGE_276, stringError, strInput);
   enQueue_Message(ALI,objMessage); 
   boolError = true;

 }//end if else

// //cout << "ALI->" << stringAliText << endl;
 b64.freeBuffer();
 return (!boolError);
}

string ALI_Data::fALIbidType()
{
 switch (this->enumALIBidType) {
  case INITIAL: 	return "INITIAL";
  case AUTO:    	return "AUTO";
  case REBID:   	return "REBID";
  case MANUAL:  	return "MANUAL";
  case LATE_RECORD: 	return "LATE_RECORD";
  default:      	return "UNDEFINED";
 }
}


int ALI_Data::fFindRowFromClassofService(ALISteering ALIDB)
{
  int                    intRowCOS =  0;
  string                 strRow    = "";
  size_t                 found;
  string                 strMessage;

//  extern ALISteering     ALI_Steering[intMAX_ALI_DATABASES + 1 ];


  intRowCOS = ALIDB.iClassOfServiceRow;

  if (ALIDB.vCOSCallbackRow.empty())                            {return -1;}
  if (intRowCOS <= 0)                                           {return -1;}

  strRow = fRowOfALIdata(intRowCOS, ALIDB);

  if (strRow.empty())                                           {return -1;}
  if (strRow.length() < 4)                                      {return -1;}

  for (vector<COSCallbackRow>::iterator it = ALIDB.vCOSCallbackRow.begin() ; it != ALIDB.vCOSCallbackRow.end(); ++it)
  {
   found =  strRow.find(it->strClassOfService);
   if (found != string::npos) {
    this->strClassOfService = it->strClassOfService;
    return  it->iRow;
   }
  }

  found =  strRow.find("NO RECORD FOUND");  if (found != string::npos) {return  -1;}
  found =  strRow.find("No Record Found");  if (found != string::npos) {return  -1;}

  strMessage = "ng911.cfg: No Class of Service found in ->" + strRow;
  SendCodingError(strMessage); 
 
 return -1;
}

string ALI_Data::fRowOfALIdata(int iRow, ALISteering ALIDB)
{
 int                    r = 1;
 unsigned int           i = 0;
 int                    w = 1;
 size_t                 pos;
 string                 strRow ="";
              
 if (iRow <= 0)             {return "";}
 if (stringAliText.empty()) {return "";}

 while ((r < iRow)&&(i < stringAliText.length()-1))
  {
   if      ((stringAliText[i] == charCR)&&(stringAliText[i+1] == charLF))  {r++; w=0;i++;}
   else if (((stringAliText[i] == charLF)&&(w < intALI_RECORD_LINE_WRAP))&&
           (ALIDB.boolTreatLinefeedasCR))                                  {r++; w=0;}
   else if ((stringAliText[i] == charCR)&&(w < intALI_RECORD_LINE_WRAP))   {r++; w=0;}
   else if (w == intALI_RECORD_LINE_WRAP)                                  {r++; w=0;}
   i++;w++;
  }

 
 if ( r < iRow)                                   {return "";}    // did not get to row number

 if (i >= (stringAliText.length()-1))             {return "";}    // not enough characters

 if ((intALI_RECORD_LINE_WRAP +1 - w) < 1)        {return "";}    // should not get this but just in case ....

 strRow = stringAliText.substr(i, intALI_RECORD_LINE_WRAP +1 - w);

 pos = strRow.find_first_of (charCR);
 if (pos != string::npos) {strRow = strRow.substr(0,pos);}
// //cout << iRow << strRow << endl;
 return strRow;
}


unsigned long long int ALI_Data::fFindCallback(int index, ALISteering ALIDB)
{
 int                    intRow = 0;
 int                    intColumn = 0;
 unsigned int           i = 0;
 size_t                 pos;
 int                    r = 1;
 int                    w = 1;
 string                 strRow;
 unsigned long long int TelephoneNumber = 0;
 extern bool            boolNG_ALI;

 ////cout << "find callback" << endl;

 intRow = fFindRowFromClassofService(ALIDB);
// //cout << "COS row is ->" << intRow << endl;
 if (intRow <= 0)
  {
   intRow    =  ALIDB.intCallbackLocationRow[index];
   intColumn =  ALIDB.intCallbackLocationColumn[index];
  }
 ////cout << "row is ->" << intRow << endl;
 if (!stringAliText.length()) {return 0;}
 if (intRow <= 0) {return 0;}

 ////cout << "here" << endl;
 while ((r < intRow)&&(i < stringAliText.length()-1))
  {
   if      ((stringAliText[i] == charCR)&&(stringAliText[i+1] == charLF))  {r++; w=0;i++;}
   else if (((stringAliText[i] == charLF)&&(w < intALI_RECORD_LINE_WRAP))&&
           (ALIDB.boolTreatLinefeedasCR))                                  {r++; w=0;}
   else if ((stringAliText[i] == charCR)&&(w < intALI_RECORD_LINE_WRAP))   {r++; w=0;}
   else if (w == intALI_RECORD_LINE_WRAP)                                  {r++; w=0;}
   i++;w++;
  }

 if ( r < intRow)  {return 0;}                                   // did not get to row number
// //cout << "1" << endl;
 if ((i+intColumn) >= (stringAliText.length()-1)) {return 0;}    // not enough characters
// //cout << "2" << endl;
 if ((intALI_RECORD_LINE_WRAP +1 - w) < 1)        {return 0;}    // should not get this but just in case ....
// //cout << "3" << endl;
 strRow = stringAliText.substr(i+intColumn, intALI_RECORD_LINE_WRAP +1 - w);

 pos = strRow.find_first_of (charCR);
 if (pos != string::npos) {strRow = strRow.substr(0,pos);}

 ////cout << "row data is -> " << strRow << endl;

 for (int i = 0; i <= int (strRow.length()-10); i++) {
  TelephoneNumber = Find_10_Digit_Telephone_Number(strRow,i);
  ////cout << "Telno -> " << TelephoneNumber << endl;
  if (TelephoneNumber > 0) {break;}
 }
 if ((TelephoneNumber == 0)&&(index==0)) {TelephoneNumber = fFindCallback(1, ALIDB);}
 return TelephoneNumber;
}






int Conference_Number::ConferencesInUse()
{
 int iConferencesInUse = 0;

 for ( int i = MIN_CONFERENCE_NUMBER; i <=  MAX_CONFERENCE_NUMBER; i++) { if (bConferenceNumberInUse[i]) {iConferencesInUse++;}} 

 return iConferencesInUse;
}

int Conference_Number::OldestRecordInUse()
{
 int             iOldestRecord = -1;
 long double     ldTimeDiff = 0.0;
 struct timespec timespecTimeNow;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 for (unsigned long int i = MIN_CONFERENCE_NUMBER; i <=  MAX_CONFERENCE_NUMBER; i++) 
  { 
   if (bConferenceNumberInUse[i])
    {
     if (time_difference(timespecTimeNow, timespecTimePutinUse[i])  > ldTimeDiff)
      {
       ldTimeDiff = time_difference(timespecTimeNow, timespecTimePutinUse[i]);
       iOldestRecord = i;
      } // end (time_difference(timespecTimeNow, timespecTimePutinUse[i])  > ldTimeDiff)
    } // end  if (bConferenceNumberInUse[i])
   
  }// end for (int i = 100; i <= intMAX_CONFERENCE_NUMBER; i++) 

 return iOldestRecord;
}


int Conference_Number::AssignConferenceNumber()
{
 long long int iOutput = -1;
 long long int i;
 int index;

 i = intMIN_CONFERENCE_NUMBER;

// to activate random conference numbers un comment the next 2 lines of code ..
 srand(time(NULL));
 do
  {    
   i = intMIN_CONFERENCE_NUMBER + rand() % (MAX_CONFERENCE_NUMBER - MIN_CONFERENCE_NUMBER);

  } while ((i == 911)||(i == 411)); // no conference eq to 911 or 411

 index = i;
 for (long long int j = 1; j < (MAX_CONFERENCE_NUMBER - MIN_CONFERENCE_NUMBER) ; j++)
  {
   if (index > MAX_CONFERENCE_NUMBER) {index = MIN_CONFERENCE_NUMBER;}

   if (!bConferenceNumberInUse[index]) 
    {
     bConferenceNumberInUse[index] = true; 
     iOutput = index;
     clock_gettime(CLOCK_REALTIME, &timespecTimePutinUse[index]);
     break;
    } 
  index++;
  }

 return iOutput;
}

bool Conference_Number::ReleaseConferenceNumber(int i)
{
 if ((i < 0)||(i > MAX_CONFERENCE_NUMBER)) {return false;}
 
 bConferenceNumberInUse[i] = false;

 return true;
}


long double Conference_Number::TimeInUse(unsigned int i)
{
 struct timespec timespecTimeNow;
 if (i < MIN_CONFERENCE_NUMBER)             {return 0;}
 if (i > MAX_CONFERENCE_NUMBER)             {return 0;}
 if (!bConferenceNumberInUse[i])            {return 0;}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 return time_difference(timespecTimeNow, timespecTimePutinUse[i]);
}

string DataPacketIn::fDisplay()
{
 ostringstream  strOutput;

 strOutput << "Port Number    = "     << intPortNum << endl;
 strOutput << "Workstation Number = " << intWorkstation << endl;
 strOutput << "Side A or B (int)  = " << intSideAorB << endl;
 strOutput << "Text Length        = " << intLength << endl;
 strOutput << "IP Address         = " << IPAddress.fDisplay();
 strOutput << "Data Recieved      = " << stringDataIn << endl;
 strOutput << "TransactionID      = " << intTransactionID << endl;
 strOutput << "Buffer Exceeded    = " << boolStringBufferExceeded << endl;
 strOutput << "ALI_E2_Data        = Not implemented at this view" << endl;
 strOutput << "ALI Service Data   = " << boolALIservicePacket << endl;   //          E2Data;

 return strOutput.str();
}




void ANI_Data_Format::fSetANIformatReceived(int iFormat, int i)
{
 sem_wait(&sem_tMutexANIformatReceived);
 iANIformatReceived[i] = iFormat;
 sem_post(&sem_tMutexANIformatReceived);
}

void ANI_Data_Format::fDisplayANIformatDetected()
{
 //cout << "Trunk" << endl;
 for (int i = 1; i <= intNUM_TRUNKS_INSTALLED; i++)
  {
   switch (TRUNK_TYPE_MAP.Trunk[i].TrunkType)
    {
     case CAMA: case LANDLINE: case WIRELESS: case INDIGITAL_WIRELESS: case INTRADO:
          switch (iANIformatReceived[i])
           {
            case 0:
                   //cout << setw(5)  << left << i   <<" No ANI data detected" << endl;
                   break;
            default:
                   //cout << setw(5)  << left << i   << iANIformatReceived[i] << " Digit ANI Format Detected ->" << endl;
                   break;
           }// end switch (iANIformatReceived[i])
     default: break;
    }
  }
 //cout << endl;
}


void Window_and_Button_Data::fSetNoALICadBid() {

 boolALIRebidButton = false;
 boolCADSendButton  = false;
}

void Window_and_Button_Data::fClear()
{
 boolTransferWindow = false;
// boolALIRebidButton = false;
// boolCADSendButton  = false;
}

void Window_and_Button_Data::fDisplay()
{
 //cout << "TRANSFER WINDOW: " << boolTransferWindow << endl;
 //cout << "ALI REBID      : " << boolALIRebidButton << endl;
 //cout << "SEND TO CAD    : " << boolCADSendButton << endl;
}
void Window_and_Button_Data::fSetButtonsForALIbid()
{
 boolALIRebidButton = false;
 boolCADSendButton  = false;
}

void Window_and_Button_Data::fSetButtonsForALIReceived()
{
 boolALIRebidButton = true;
 boolCADSendButton  = true;
}

void Window_and_Button_Data::fSetButtonsForALItimeout(bool bALIreceived)
{
 boolALIRebidButton = true;
 if (bALIreceived) { boolCADSendButton  = true;}
 else              { boolCADSendButton  = false;}
}

void Window_and_Button_Data::fSetButtonsForAbandoned()
{
 boolALIRebidButton = false;
 boolCADSendButton  = false;
}

void Window_and_Button_Data::fSetButtonsForManualALIReceived()
{
 boolALIRebidButton = false;
 boolCADSendButton  = true;
}

void Window_and_Button_Data::fSetButtonsForManualALITimeout() 
{
 boolALIRebidButton = false;
 boolCADSendButton  = false;
}

void Window_and_Button_Data::fSetRapidLiteButton(bool boolValidCallback) 
{
 extern bool boolSHOW_RAPID_LITE_BUTTON;
 if (!boolSHOW_RAPID_LITE_BUTTON) { boolRapidLiteButton = false;}
 else                             { boolRapidLiteButton = boolValidCallback;}
}




string ConvertXMLError2String(XMLError xe)
{
 // note: the commented lines are for a newer version of XMLParser TBC ...
  switch (xe)
  {
   case eXMLErrorNone:                            return "No Error";
   case eXMLErrorMissingEndTag:                   return "Missing End Tag";
   case eXMLErrorEmpty:                           return "Empty";
//   case eXMLErrorFirstNotStartTag:                return "First Not Start Tag";
   case eXMLErrorMissingTagName:                  return "Missing Tag Name";
   case eXMLErrorMissingEndTagName:               return "Missing End Tag Name";
//   case eXMLErrorNoMatchingQuote:                 return "No Matching Quote";
   case eXMLErrorUnmatchedEndTag:                 return "Unmatched End Tag";
   case eXMLErrorUnmatchedEndClearTag:            return "Unmatched End Clear Tag";
   case eXMLErrorUnexpectedToken:                 return "Unexpected Token";
//   case eXMLErrorInvalidTag:                      return "Invalid Tag";
   case eXMLErrorNoElements:                      return "No Elements";
   case eXMLErrorFileNotFound:                    return "File Not Found";
   case eXMLErrorFirstTagNotFound:                return "First Tag Not Found";
   case eXMLErrorUnknownCharacterEntity:          return "Unknown Character Entity";
   case eXMLErrorCharConversionError:             return "Char Conversion Error";
   case eXMLErrorCannotOpenWriteFile:             return "Cannot Open Write File";
   case eXMLErrorCannotWriteFile:                 return "Cannot Write File";
   case eXMLErrorBase64DataSizeIsNotMultipleOf4:  return "Base64 Data Size Is Not a Multiple of 4"; 
   case eXMLErrorBase64DecodeIllegalCharacter:    return "Base64 Decode Illegal Character";
   case eXMLErrorBase64DecodeTruncatedData:       return "Base64 Decode Truncated Data";
   case eXMLErrorBase64DecodeBufferTooSmall:      return "Base64 Decode Buffer Too Small";
   
   default:                                       return "Unknown Error";
  }
}

int FXO_TrunkNumber(string strInput)
{
 size_t found;
 int    iTrunk;
 string strData;
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_ANI_WO_COLON);
 if (strData.empty()) {strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON);}
 if (strData.length() != 3)                            {return -1;}
 found = strData.find_first_not_of("0123456789");
 if (found != string::npos)                          {return -1;}
 strData = strData.erase(0,1);
 iTrunk = char2int(strData.c_str());
 if (!Validate_Trunk_Number(iTrunk))                 {return -1;}
 if (TRUNK_TYPE_MAP.Trunk[iTrunk].TrunkType != CLID) {return -1;}


 return char2int(strData.c_str());

}


int Determine_GUI_LINE_VIEW_Number(string strInput)
 {
  string 				strData;
  extern Telephone_Devices		TelephoneEquipment; 

  //Data contained in ANI var (older version of freeswitch )
  strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_ANI_WO_COLON);
  
  //Data contained in Caller-Username in newer version ANI var not present ...
  if (strData.empty()) { strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_USER_NAME_WO_COLON); }

  return TelephoneEquipment.fIndexofLineView(strData);
 }

string Freeswitch_Channel_Progress_Determine_Trunk_Number(bool boolLineRoll, Freeswitch_Data objFreeswitchData, string strInput)
 {

  // note IP address from objFreeswitchData should be loaded into object !!!!

  bool 					boolCallfromAudiocodesGateway;
  string 				strData;
  enum   Audiocodes_Port_Type           {FXS, FXO};
  Audiocodes_Port_Type                  eAudiocodesPortType;
  extern Telephone_Devices		TelephoneEquipment;


  //Check if Trunk Came from an ESRP
  strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
  if (!strData.empty()) { strData = strData.erase(0,1); return strData; }

  // Determine if Call is From Audiocodes FXS (911) or FXO (admin Call) ... FXS ANI will be 3 digits, FXO ANI will be callerID number
  boolCallfromAudiocodesGateway = TelephoneEquipment.fIsAnAudiocodes(objFreeswitchData.objChannelData.IPaddress);


  if (!boolCallfromAudiocodesGateway) {return "00";}

  // Determine if FXS or FXO
  if      (CallerIDNameIsFrom911Trunk(strInput))  {eAudiocodesPortType = FXS;}
  else if (FXO_TrunkNumber(strInput) > 0 )        {eAudiocodesPortType = FXO;}
  else                                            {eAudiocodesPortType = FXS;}  // may need to make trunk 00


  switch (boolLineRoll)
   {
    case true:
            
         SendCodingError( "ExperientDataClass.cpp - Freeswitch_Channel_Progress_Determine_Trunk_Number() should not get here in function");
         break;

    case false:

         switch (eAudiocodesPortType)
          {
           case FXO:
                //Trunk contained in OTHER LEG ANI var (older version of freeswitch )                 
                strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_ANI_WO_COLON);
                if (!strData.empty()) { strData = strData.erase(0,1); return strData; }
 
                //Trunk contained in OTHER_LEG DEST_NUMBER in newer version, ANI var not present ...
                strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON);
                if (!strData.empty()) { strData = strData.erase(0,1); return strData; }            
                break;
         
           case FXS:

                //Trunk contained in ANI var (older version of freeswitch )
                strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_ANI_WO_COLON);
                if (!strData.empty()) { strData = strData.erase(0,1); return strData; }

                //Trunk contained in Caller-Username in newer version ANI var not present ...
                strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_USER_NAME_WO_COLON);
                if (!strData.empty()) { strData = strData.erase(0,1); return strData; }
                break;
           }
      
         break;
   }



 return "NO DATA FOUND";
 }

bool PARK_PICKUP(string strInput)
{
 string strData;
 size_t found;
 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON)); 
// //cout << "string data -> " << strData << endl << endl;
// //cout << strInput << endl;
 found = strData.find(FREESWITCH_PARK_PICKUP_PREFIX_WO_COLON);
 if (found == string::npos) {return false;}

 return true;
}

bool BLIND_TRANSFER_FAIL_RINGBACK(string strInput) {

string strData;
bool   returnvalue;

if (!CallFromBlindTransferContext(strInput))  {return false;}


 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_FAIL_RINGBACK_WO_COLON); 

 returnvalue  = ((strData == "true")||(strData == "True")||(strData == "TRUE"));

 return returnvalue;
}


bool BLIND_TRANSFER_GUI_FROM_CLI(string strInput)
{
 bool 	boolBlindtransfer;
 bool 	boolConferenceName;
 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_METHOD_WO_COLON); 
 boolBlindtransfer = ( strData == "BLIND_TRANSFER");

 strData = ParseFreeswitchData(strInput, FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON); 
 boolConferenceName = (!strData.empty());

 return (boolBlindtransfer&&boolConferenceName);
}


bool DestinationNumberisVALET_JOIN(string strInput)
{
 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_DEST_NUM_WO_COLON);

 if (strData == "VALET_JOIN") {return true;}
 return false;
}
bool DestinationNumberisCONF_JOIN(string strInput)
{
 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON);

 if (strData == "CONF_JOIN") {return true;}
 return false;
}

string Freeswitch_Current_Application(string strInput)
{
 size_t found;
 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_CURRENT_APPLICATION_WITH_COLON);
 strData = RemoveLeadingSpaces(strData);
 strData = RemoveTrailingSpaces(strData);
 return strData;
}

void Last_On_Hold::IintializeVector()
{
 Channel_Data objData;

 for (int i =0; i <=intNUM_WRK_STATIONS; i++){vLastonHold.push_back(objData);}

}

bool Last_On_Hold::Set_Last_OnHold(string strChannel, string strUniqueId, unsigned int i)
{
 vector<Channel_Data>::size_type         sz =  vLastonHold.size();

if (i > sz) {return false;}
vLastonHold[i].strChannelName = strChannel;
vLastonHold[i].strChannelID = strUniqueId;
return true;
}




//end class function implementation
