/*****************************************************************************
* FILE: error_routines.cpp
*  
*
* DESCRIPTION:
*   
*
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

using namespace std;

extern ExperientCommPort                       	CADPort                                 [NUM_CAD_PORTS_MAX+1];
extern ExperientCommPort                       	ALIPort                                 [intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
extern ExperientCommPort                       	ALIServicePort                          [intMAX_ALI_PORT_PAIRS  + 1 ];
extern IP_Address                              	HOST_IP;
extern IP_Address			       	CAD_PORT_REMOTE_HOST                    [NUM_CAD_PORTS_MAX+1]; 
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data			       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern Port_Data                               	objBLANK_ALI_PORT;
extern Port_Data                               	objBLANK_ANI_PORT;
extern Port_Data                               	objBLANK_CAD_PORT;
extern Port_Data                               	objBLANK_WRK_PORT;
extern Port_Data                               	objBLANK_KRN_PORT;
extern Port_Data                               	objBLANK_SYC_PORT; 
extern Port_Data                               	objBLANK_LOG_PORT;                            
/****************************************************************************************************
*
* Name:
*  Function: void Semaphore_Error()
*
*
* Description:
*   An Error Handler  
*
*
* Details:
*   This function attempts to set a mutex semaphore following an error.  it will recursively
*   call itself for a preset (intNUM_SEM_LOCK_RETRIES) number of times.  if the function 
*   ultimately fails a reboot shall occur. 
*          
*
* Parameters:
*   enumSemFunc                         - type of semaphore operation that failed
*   enumThdCallng                       - enumeration of thread calling this function
*   intNumFails                         - int counter of number of failures 				
*   intRetCde			     	- int error code
*   sem_tSemptr                         - pointer to the failed semaphore
*   stringMssge                         - string message
*
* Variables:
*   boolSTOP_LOG_THREAD                 Global  - <globals.h> used to shut down the log thread
*   charEMAIL_SEND_TO_WARNING           Global  - <defines.h>
*   charMessageText                     Local   - used to set message to email function
*   intEMAIL_MESSAGE_MAX_LENGTH         Global  - <defines.h>
*   intEMAIL_TIMEOUT_SEC                Global  - <defines.h>
*   intRC                               Local   - int return code for functions
*   intNUM_SEM_LOCK_RETRIES             Global  - <defines.h> int number of recursions allowed
*   intSEMAPHORE_ERRORS                 Global  - <globals.h> counter
*   intTHREAD_SHUTDOWN_DELAY_SEC        Global  - <defines.h>
*   LOG_WARNING                         Global  - <defines.h> code for log thread to email, display to console and file to disk
*   objEmailMsg                         Global  - <ExperientEmail> object
*   objMessage          		Local	- <MessageClass> object
*   RB_AUTOBOOT                         Library - <sys/reboot.h> constant
*   stringOutputMessage                 Global  - <MessageClass> member
*   stringThreadCalling			Local	- string for used for message output
*                                                                          
* Functions:
*   Console_Message()                   Global  - <globalfunctions.cpp>
*   convert_string2char_array()         Global  - <globalfunctions.h>
*   .Connect()                          Library - <ExperientEmail><ipworks.h><FileMailer>
*   enQueue_Message()                   Global  - <globalfunctions.h>
*   .fIntializeHeaders()                Global  - <ExperientEmail>
*   .fMessage_Create()                  Global  - <MessageClass>
*   int2strLZ()                         Global  - <globalfunctions.cpp>
*   reboot()                            Library - <sys/reboot.h>
*   sem_post()                          Library - <semaphore.h>
*   sem_wait()                          Library - <semaphore.h>
*   .Send()                             Library - <ExperientEmail><ipworks.h><FileMailer>
*   .SetMessageText()                   Library - <ExperientEmail><ipworks.h><FileMailer>
*   .SetSendTo()                        Library - <ExperientEmail><ipworks.h><FileMailer>
*   .SetSubject()                       Library - <ExperientEmail><ipworks.h><FileMailer>
*   sleep()                             Library - <unistd.h>
*
* Author(s):
*   Bob McCarthy 			Original: 3/10/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
void Semaphore_Error(int intRetCde, threadorPorttype enumThdCallng, sem_t *sem_tSemptr, string stringMssge, int intNumFails )
{
 
 string         stringThreadCalling;
 MessageClass   objMessage;
 int            intRC = 0;

 intSEMAPHORE_ERRORS++;
 
 if (intNumFails <= intNUM_SEM_LOCK_RETRIES)
  {
   //try it again........ 
   intRC = sem_wait(sem_tSemptr);
// intRC = 1;              // test tbc
  }// end if
 else
  {
   // need to shut down from here......
   Abnormal_Exit(enumThdCallng, EX_OSERR, KRN_MESSAGE_198);
  }
 
 switch (enumThdCallng)
  {
   case MAIN:
    objMessage.fMessage_Create(LOG_WARNING,197, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               KRN_MESSAGE_197, int2strLZ(intRetCde), stringMssge );
    enQueue_Message(MAIN,objMessage);
    break;

   case CAD:
    objMessage.fMessage_Create(LOG_WARNING,498, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               CAD_MESSAGE_498, int2strLZ(intRetCde), stringMssge );
    enQueue_Message(CAD,objMessage);
    break;
    
   case ALI:
    objMessage.fMessage_Create(LOG_WARNING,298, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               ALI_MESSAGE_298, int2strLZ(intRetCde), stringMssge );
    enQueue_Message(ALI,objMessage);
    break;
    break;

   case ANI:
    objMessage.fMessage_Create(LOG_WARNING,398, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               ANI_MESSAGE_398, int2strLZ(intRetCde), stringMssge );
    enQueue_Message(ANI,objMessage);
    break;

   case WRK:
    objMessage.fMessage_Create(LOG_WARNING,748, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               WRK_MESSAGE_748, int2strLZ(intRetCde), stringMssge );
    enQueue_Message(WRK,objMessage);
    break;

   case LOG:
    objMessage.fMessage_Create(LOG_WARNING,598, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               LOG_MESSAGE_598, int2strLZ(intRetCde), stringMssge);                   
    enQueue_Message(LOG,objMessage);
    break;

//   case SYC:
//    objMessage.fMessage_Create(LOG_WARNING,698, objBLANK_CALL_RECORD, objBLANK_SYC_PORT, SYC_MESSAGE_698, int2strLZ(intRetCde), stringMssge);                   
//    enQueue_Message(SYC,objMessage);
//    break;

   default:
    //should not get here.......
    SendCodingError( "error_routines.cpp - coding error in function Semaphore_Error()");
    
  }// end switch
  
     
  if (intRC){Semaphore_Error(intRC, enumThdCallng, sem_tSemptr, stringMssge, intNumFails+1);}
   
}// end Semaphore_Error()


/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::Error_Handler()
*
*
* Description:
*   An ExperientUDPPort Class Function Implementation  
*
*
* Details:
*   This function forwards error codes & descriptions returned from the library ipworks 
*   and attempts to recover
*          
*
* Parameters: 				
*   errorcode			     	- int error code
*   stringErrorText                     - string Text of error
*
* Variables:
*   boolReducedHeartBeatMode            Global  - <ExperientCommPort> member
*   CADPort[]                           Global  - <ExperientCommPort> object array
*   intRC                               Local   - int return code for functions
*   enumPortType                        Local   - <ExperientUDPPort> member
*   HOST_IP                             Global  - <IP_Address> object
*   intLogCode                          Local   - code for log thread
*   intPING_TIMEOUT                     Global  - <defines.h>
*   intPortNum                          Local   - <ExperientUDPPort> member
*   LOG_INFO                            Global  - <defines.h> code to email, display to console and file to disk
*   LOG_WARNING                         Global  - <defines.h> code to email, display to console and file to disk
*   objMessage          		Local	- <MessageClass> object
*   objPing                             Local   - <ipworks.h><Ping> object
*                                                                          
* Functions:
*   enQueue_Message()                   Global  - <globalfunctions.h>
*   .erase()                            Library - <cstring>
*   .fMessage_Create()                  Global  - <MessageClass>
*   .GetActive()                        Library - <ipworks.h><UDPPort>
*   GetLastError()                      Library - <ipworks.h><UDPPort>
*   GetLastErrorCode()                  Library - <ipworks.h><UDPPort>
*   .GetRemoteHost()                    Library - <ipworks.h><UDPPort>
*   .GetResponseTime()                  Library - <ipworks.h><Ping>
*   int2strLZ()                         Global  - <globalfunctions.h> 
*   .PingHost()                         Library - <ipworks.h><Ping> 
*   .SetActive()                        Library - <ipworks.h><UDPPort>
*   .SetLocalHost()                     Library - <ipworks.h><Ping>
*   .SetTimeout()                       Library - <ipworks.h><Ping>
*
*
* Author(s):
*   Bob McCarthy 			Original: 2/18/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
void ExperientUDPPort::Error_Handler(int intErrorCode , string stringErrorMessage)
{
 MessageClass           objMessage;
 int                    intRC;
 int                    intLogCode = LOG_WARNING;
 Ping                   objPing;
 int                    intMessageCode = 0;
 string                 stringSideAorB;
#ifdef IPWORKS_V16
 objPing.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 objPing.SetLocalHost((char*)HOST_IP.stringAddress.c_str());
 objPing.SetTimeout(intPING_TIMEOUT);

 
 if      (intSideAorB == 1) {stringSideAorB = "-A";}
 else if (intSideAorB == 2) {stringSideAorB = "-B";}
 else if (intSideAorB == 3) {stringSideAorB = "-S";}
 else                       {stringSideAorB = "";}

 switch (enumPortType)
  {
   case CAD:
    if (CADPort[intPortNum].boolReducedHeartBeatMode){intLogCode = LOG_INFO*boolDEBUG;}
    intMessageCode = 465;
    break;
   case ANI:
    //ANI Does not broadcast should not recieve any errors ....
    SendCodingError( "error_routines.cpp - ExperientUDPPort::Error_Handler ANI UDPPORT ERROR.....................");;           // test TBC
    intMessageCode = 329;
    break;
   case ALI:
    switch (intSideAorB)
     {
      case 1: case 2:
             if (ALIPort[intPortNum][intSideAorB].boolReducedHeartBeatMode){intLogCode = LOG_INFO*boolDEBUG;}
             break;
      case 3:
             if (ALIServicePort[intPortNum].boolReducedHeartBeatMode){intLogCode = LOG_INFO*boolDEBUG;}
             break;
     }
    intMessageCode = 233;
    break;
   case WRK:
    if (!boolPortActive){return;}
 //   fSet_Port_Down();
    intMessageCode = 719;
    break;
//   case SYC:
//    if (!boolPortActive){return;}
//    fSet_Port_Down();
//    intMessageCode = 606;
//    break;
   case MAIN:
    if (!boolPortActive){return;}
//    fSet_Port_Down();
    intMessageCode = 160;
    break;
   default:
    //error
    SendCodingError( "error_routines.cpp - coding error in ExperientUDPPort::Error_Handler() Error Code =" + int2str(intErrorCode) + " Port Code = " + int2str(enumPortType));
   
    break;

  }//end switch

 
 intRC = GetLastErrorCode();

 // will be zero if error trapped by ipworks internal FireError Function and will have been previously logged. 
 if (intRC) 
  {
   if(intLogCode)
    {   
     objMessage.fMessage_Create(intLogCode,intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                KRN_MESSAGE_160, int2strLZ(intRC), GetLastError());
     enQueue_Message(enumPortType, objMessage);
    }
  }
 else
  {
   if (intLogCode)
    {
     objMessage.fMessage_Create(intLogCode,intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                KRN_MESSAGE_160, int2strLZ(intErrorCode), stringErrorMessage);
      enQueue_Message(enumPortType, objMessage);
    }
  }//end if (intRC) else
 

 if ((intRC == 98)||(intErrorCode == 98))
  {
   Abnormal_Exit(enumPortType, EX_UNAVAILABLE, KRN_MESSAGE_161 );   
  }
       
 
}//end ExperientUDPPort::Error_Handler




