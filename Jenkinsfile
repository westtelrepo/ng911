pipeline {
    agent none

    stages {
        stage('Build') {
            parallel {
                stage('Build on stretch/amd64') {
                    agent any
                    environment {
                        ARTIFACT_CREDS = credentials('artifact_server')
                    }
                    steps {
                        script {
                            // build dockerfile ourselves because the credentials don't appear when using dockerfile agent
                            def image = docker.build("ng911-build-stretch-amd64", '--build-arg ARTIFACT_USER=$ARTIFACT_CREDS_USR --build-arg ARTIFACT_PSW=$ARTIFACT_CREDS_PSW Jenkins');
                            image.inside {
                                sh 'ln -s $PWD /datadisk1/src/ng911'
                                sh 'cd /datadisk1/src/ng911 && make'
                                sh 'cp /usr/lib/libipworks.so.9.0 .'
                                // TODO: use time of commit
                                sh 'tar --sort=name --mtime="2018-12-17 21:04:23Z" --owner=0 --group=0 --numeric-owner -cjf ng911.stretch.amd64.tbz ng911.exe libipworks.so.9.0'
                            }
                        }
                        // NOTE:
                        // libipworks.so -> libipworks.so.9
                        // libipworks.so.9 -> libipworks.so.9.0
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'ng911.stretch.amd64.tbz', fingerprint: true
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }

                stage('Build on stretch/i386') {
                    agent any
                    environment {
                        ARTIFACT_CREDS = credentials('artifact_server')
                    }
                    steps {
                        script {
                            // build dockerfile ourselves because the credentials don't appear when using dockerfile agent
                            def image = docker.build("ng911-build-stretch-i386", '--build-arg ARTIFACT_USER=$ARTIFACT_CREDS_USR --build-arg ARTIFACT_PSW=$ARTIFACT_CREDS_PSW -f Jenkins/Dockerfile.i386 Jenkins');
                            image.inside {
                                sh 'ln -s $PWD /datadisk1/src/ng911'
                                sh 'cd /datadisk1/src/ng911 && make'
                                sh 'cp /usr/lib/libipworks.so.9.0 .'
                                // TODO: use time of commit
                                sh 'tar --sort=name --mtime="2018-12-17 21:04:23Z" --owner=0 --group=0 --numeric-owner -cjf ng911.stretch.i386.tbz ng911.exe libipworks.so.9.0'
                            }
                        }
                        // NOTE:
                        // libipworks.so -> libipworks.so.9
                        // libipworks.so.9 -> libipworks.so.9.0
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'ng911.stretch.i386.tbz', fingerprint: true
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }

                stage('Build on centos6/amd64') {
                    agent any
                    environment {
                        ARTIFACT_CREDS = credentials('artifact_server')
                    }
                    steps {
                        script {
                            // build dockerfile ourselves because the credentials don't appear when using dockerfile agent
                            def image = docker.build("ng911-build-centos6-amd64", '--build-arg ARTIFACT_USER=$ARTIFACT_CREDS_USR --build-arg ARTIFACT_PSW=$ARTIFACT_CREDS_PSW -f Jenkins/Dockerfile.centos6 Jenkins');
                            image.inside {
                                sh 'ln -s $PWD /datadisk1/src/ng911'
                                sh 'cd /datadisk1/src/ng911 && make'
                                sh 'cp /usr/lib/libipworks.so.9.0 .'
                            }
                        }
                        // TODO: use time of commit
                        // centos6 tar doesn't support --sort=name, so we tar outside
                        sh 'tar --sort=name --mtime="2018-12-17 21:04:23Z" --owner=0 --group=0 --numeric-owner -cjf ng911.centos6.amd64.tbz ng911.exe libipworks.so.9.0'
                        // NOTE:
                        // libipworks.so -> libipworks.so.9
                        // libipworks.so.9 -> libipworks.so.9.0
                    }
                    post {
                        success {
                            archiveArtifacts artifacts: 'ng911.centos6.amd64.tbz', fingerprint: true
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
            }
        }
    }
}
