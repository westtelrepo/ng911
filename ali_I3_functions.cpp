/*****************************************************************************
* FILE: ali_I3_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the ALI_I3_Data Class 
*
*
*
* AUTHOR: 08/10/2012 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"                          // XML Software


extern Call_Data	                       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern Text_Data				objBLANK_TEXT_DATA;

//functions 
string SevenDigitTelno(string strInput);

//constructor
ALI_I3_StreetAddress::ALI_I3_StreetAddress() {
 strHouseNum= strHouseNumSuffix= strPrefixDirectional= strStreetName= strStreetSuffix= strPostDirectional= strTextualAddress= strMSAGCommunity= strPostalCommunity=
 strStateProvince= strCountyID= strCountry= strTARCode= strPostalZipCode= strBuilding= strFloor= strUnitNum= strUnitType= strLocationDescription= strLandmarkAddress= "";
}
// copy constuctor
ALI_I3_StreetAddress::ALI_I3_StreetAddress(const ALI_I3_StreetAddress *a) {
 strHouseNum 			= a->strHouseNum;
 strHouseNumSuffix 		= a->strHouseNumSuffix;
 strPrefixDirectional		= a->strPrefixDirectional;
 strStreetName 			= a->strStreetName;
 strStreetSuffix 		= a->strStreetSuffix;
 strPostDirectional		= a->strPostDirectional;
 strTextualAddress		= a->strTextualAddress;
 strMSAGCommunity 		= a->strMSAGCommunity;
 strPostalCommunity 		= a->strPostalCommunity;
 strStateProvince		= a->strStateProvince;
 strCountyID 			= a->strCountyID;
 strCountry			= a->strCountry;
 strTARCode 			= a->strTARCode;
 strPostalZipCode		= a->strPostalZipCode;
 strBuilding 			= a->strBuilding;
 strFloor			= a->strFloor;
 strUnitNum 			= a->strUnitNum;
 strUnitType			= a->strUnitType;
 strLocationDescription 	= a->strLocationDescription;
 strLandmarkAddress		= a->strLandmarkAddress;
}
//assignment operator
ALI_I3_StreetAddress& ALI_I3_StreetAddress::operator=(const ALI_I3_StreetAddress& a)    {
 if (&a == this ) {return *this;}
 strHouseNum 			= a.strHouseNum;
 strHouseNumSuffix 		= a.strHouseNumSuffix;
 strPrefixDirectional		= a.strPrefixDirectional;
 strStreetName 			= a.strStreetName;
 strStreetSuffix 		= a.strStreetSuffix;
 strPostDirectional		= a.strPostDirectional;
 strTextualAddress		= a.strTextualAddress;
 strMSAGCommunity 		= a.strMSAGCommunity;
 strPostalCommunity 		= a.strPostalCommunity;
 strStateProvince		= a.strStateProvince;
 strCountyID 			= a.strCountyID;
 strCountry			= a.strCountry;
 strTARCode 			= a.strTARCode;
 strPostalZipCode		= a.strPostalZipCode;
 strBuilding 			= a.strBuilding;
 strFloor			= a.strFloor;
 strUnitNum 			= a.strUnitNum;
 strUnitType			= a.strUnitType;
 strLocationDescription 	= a.strLocationDescription;
 strLandmarkAddress		= a.strLandmarkAddress;
 return *this;
}



//constructor
ALI_I3_CallInfo::ALI_I3_CallInfo() {
 strCallbackNum= strCallingPartyNum= strClassOfService = strClassOfServiceCode= strTypeOfService = strTypeOfServiceCode = strSourceOfService=
 strMainTelNum= strCustomerName= strCustomerCode= strAttentionIndicator = strAttentionIndicatorCode= strSpecialMessage= strAlsoRingsAtAddress= "";
}
// copy constuctor
ALI_I3_CallInfo::ALI_I3_CallInfo(const ALI_I3_CallInfo *a) {
 strCallbackNum 		= a->strCallbackNum;
 strCallingPartyNum 		= a->strCallingPartyNum;
 strClassOfService		= a->strClassOfService;
 strClassOfServiceCode 		= a->strClassOfServiceCode;
 strTypeOfService 		= a->strTypeOfService;
 strTypeOfServiceCode		= a->strTypeOfServiceCode;
 strSourceOfService		= a->strSourceOfService;
 strMainTelNum 			= a->strMainTelNum;
 strCustomerName 		= a->strCustomerName;
 strCustomerCode		= a->strCustomerCode;
 strAttentionIndicator 		= a->strAttentionIndicator;
 strAttentionIndicatorCode	= a->strAttentionIndicatorCode;
 strSpecialMessage 		= a->strSpecialMessage;
 strAlsoRingsAtAddress		= a->strAlsoRingsAtAddress;
}
//assignment operator
ALI_I3_CallInfo& ALI_I3_CallInfo::operator=(const ALI_I3_CallInfo& a)    {
 if (&a == this ) {return *this;}
 strCallbackNum 		= a.strCallbackNum;
 strCallingPartyNum 		= a.strCallingPartyNum;
 strClassOfService		= a.strClassOfService;
 strClassOfServiceCode 		= a.strClassOfServiceCode;
 strTypeOfService 		= a.strTypeOfService;
 strTypeOfServiceCode		= a.strTypeOfServiceCode;
 strSourceOfService		= a.strSourceOfService;
 strMainTelNum 			= a.strMainTelNum;
 strCustomerName 		= a.strCustomerName;
 strCustomerCode		= a.strCustomerCode;
 strAttentionIndicator 		= a.strAttentionIndicator;
 strAttentionIndicatorCode	= a.strAttentionIndicatorCode;
 strSpecialMessage 		= a.strSpecialMessage;
 strAlsoRingsAtAddress		= a.strAlsoRingsAtAddress;
 return *this;
}

//constructor
ALI_I3_GeoLocation::ALI_I3_GeoLocation() {
 strLatitude= strLongitude= strElevation = strDatum = strHeading= strSpeed = strPositionSource = strPositionSourceCode=
 strUncertainty= strConfidence= strDateStamp= strLocationDescription= "";
}
// copy constuctor
ALI_I3_GeoLocation::ALI_I3_GeoLocation(const ALI_I3_GeoLocation *a) {
 strLatitude 		= a->strLatitude;
 strLongitude 		= a->strLongitude;
 strElevation		= a->strElevation;
 strDatum 		= a->strDatum;
 strHeading 		= a->strHeading;
 strSpeed		= a->strSpeed;
 strPositionSource	= a->strPositionSource;
 strPositionSourceCode 	= a->strPositionSourceCode;
 strUncertainty 	= a->strUncertainty;
 strConfidence		= a->strConfidence;
 strDateStamp 		= a->strDateStamp;
 strLocationDescription	= a->strLocationDescription;
}
//assignment operator
ALI_I3_GeoLocation& ALI_I3_GeoLocation::operator=(const ALI_I3_GeoLocation& a)    {
 if (&a == this ) {return *this;}
 strLatitude 		= a.strLatitude;
 strLongitude 		= a.strLongitude;
 strElevation		= a.strElevation;
 strDatum 		= a.strDatum;
 strHeading 		= a.strHeading;
 strSpeed		= a.strSpeed;
 strPositionSource	= a.strPositionSource;
 strPositionSourceCode 	= a.strPositionSourceCode;
 strUncertainty 	= a.strUncertainty;
 strConfidence		= a.strConfidence;
 strDateStamp 		= a.strDateStamp;
 strLocationDescription	= a.strLocationDescription;
 return *this;
}

//constructor
ALI_I3_CellSite::ALI_I3_CellSite() {
 strCellID = strSectorID = strLocationDescription = ""; 
}
// copy constuctor
ALI_I3_CellSite::ALI_I3_CellSite(const ALI_I3_CellSite *a) {
 strCellID 		= a->strCellID;
 strSectorID 		= a->strSectorID;
 strLocationDescription	= a->strLocationDescription; 
}
//assignment operator
ALI_I3_CellSite& ALI_I3_CellSite::operator=(const ALI_I3_CellSite& a)    {
 if (&a == this ) {return *this;}
 strCellID 		= a.strCellID;
 strSectorID 		= a.strSectorID;
 strLocationDescription	= a.strLocationDescription; 
 return *this;
}



// constructor
ALI_I3_LocationInfo::ALI_I3_LocationInfo() {
 strComment = "";
 strMethod = "";
 StreetAddress.fClear(); 
 GeoLocation.fClear();
 CellSite.fClear();
}

// copy constuctor
ALI_I3_LocationInfo::ALI_I3_LocationInfo(const ALI_I3_LocationInfo *a) {
 strComment 	= a->strComment;
 strMethod 	= a->strMethod;
 StreetAddress  = a->StreetAddress; 
 GeoLocation	= a->GeoLocation;
 CellSite	= a->CellSite;
}
//assignment operator
ALI_I3_LocationInfo& ALI_I3_LocationInfo::operator=(const ALI_I3_LocationInfo& a)    {
 if (&a == this ) {return *this;}
 strComment 	= a.strComment;
 strMethod 	= a.strMethod;
 StreetAddress  = a.StreetAddress; 
 GeoLocation	= a.GeoLocation;
 CellSite	= a.CellSite;
 return *this;
}


//constructor
ALI_I3_Police::ALI_I3_Police() {
 strName = "";
 strTN ="0000000";
}
// copy constuctor
ALI_I3_Police::ALI_I3_Police(const ALI_I3_Police *a) {
 strName = a->strName;
 strTN   = a->strTN;
}
//assignment operator
ALI_I3_Police& ALI_I3_Police::operator=(const ALI_I3_Police& a)    {
 if (&a == this ) {return *this;}
 strName = a.strName;
 strTN   = a.strTN;
 return *this;
}

//constructor
ALI_I3_Fire::ALI_I3_Fire(){
 strName = "";
 strTN ="0000000";
}
// copy constuctor
ALI_I3_Fire::ALI_I3_Fire(const ALI_I3_Fire *a) {
 strName = a->strName;
 strTN   = a->strTN;
}
//assignment operator
ALI_I3_Fire& ALI_I3_Fire::operator=(const ALI_I3_Fire& a)    {
 if (&a == this ) {return *this;}
 strName = a.strName;
 strTN   = a.strTN;
 return *this;
}

//constructor
ALI_I3_EMS::ALI_I3_EMS(){
 strName = "";
 strTN ="0000000";
}
// copy constuctor
ALI_I3_EMS::ALI_I3_EMS(const ALI_I3_EMS *a) {
 strName = a->strName;
 strTN   = a->strTN;
}
//assignment operator
ALI_I3_EMS& ALI_I3_EMS::operator=(const ALI_I3_EMS& a)    {
 if (&a == this ) {return *this;}
 strName = a.strName;
 strTN   = a.strTN;
 return *this;
}


//constructor
ALI_I3_Agency::ALI_I3_Agency(){
 strKind = "";
 strName = "";
 strTN   = "0000000";
}

// copy constructor
ALI_I3_Agency::ALI_I3_Agency(const ALI_I3_Agency *a) {
 strKind = a->strKind;
 strName = a->strName;
 strTN   = a->strTN;
}
//assignment operator
ALI_I3_Agency& ALI_I3_Agency::operator=(const ALI_I3_Agency& a)    {
 if (&a == this ) {return *this;}
 strKind = a.strKind;
 strName = a.strName;
 strTN   = a.strTN;
 return *this;
}

// constructor
ALI_I3_Agencies::ALI_I3_Agencies() {
 Police.fClear(); 
 Fire.fClear();
 EMS.fClear();
 vOtherAgencies.clear();
 strAdditionalInfo.clear();
 strESN            = "000";
}

// copy constructor
ALI_I3_Agencies::ALI_I3_Agencies(const ALI_I3_Agencies *a) {
 Police		= a->Police; 
 Fire		= a->Fire;
 EMS		= a->EMS;
 this->vOtherAgencies.clear();
 for (size_t i= 0; i < a->vOtherAgencies.size(); i++)  {
  this->vOtherAgencies.push_back(a->vOtherAgencies[i]);
 }
 strAdditionalInfo	= a->strAdditionalInfo;
 strESN            	= a->strESN;
}
// assignment operator
ALI_I3_Agencies& ALI_I3_Agencies::operator=(const ALI_I3_Agencies& a)    {
 if (&a == this ) {return *this;}
 Police		= a.Police; 
 Fire		= a.Fire;
 EMS		= a.EMS;
 this->vOtherAgencies.clear();
 for (size_t i= 0; i < a.vOtherAgencies.size(); i++)  {
  this->vOtherAgencies.push_back(a.vOtherAgencies[i]);
 }
 strAdditionalInfo	= a.strAdditionalInfo;
 strESN            	= a.strESN;
 return *this;
}

void ALI_I3_Agencies::fClear() {
 Police.fClear(); 
 Fire.fClear();
 EMS.fClear();
 vOtherAgencies.clear();
 strAdditionalInfo.clear();
 strESN            = "000";
}

// constructor
ALI_I3_DataProvider::ALI_I3_DataProvider() {
 strDataProviderID = "";
 strName           = "";
 strTN             = "0000000000";
 strTelURI         = "";
}
// copy constructor
ALI_I3_DataProvider::ALI_I3_DataProvider(const ALI_I3_DataProvider *a) {
 strDataProviderID 	= a->strDataProviderID;
 strName 		= a->strName;
 strTN   		= a->strTN;
 strTelURI		= a->strTelURI;
}
//assignment operator
ALI_I3_DataProvider& ALI_I3_DataProvider::operator=(const ALI_I3_DataProvider& a)    {
 if (&a == this ) {return *this;}
 strDataProviderID 	= a.strDataProviderID;
 strName 		= a.strName;
 strTN   		= a.strTN;
 strTelURI		= a.strTelURI;
 return *this;
}

//constructor
ALI_I3_AccessProvider::ALI_I3_AccessProvider() {
 strAccessProviderID = "";
 strName             = "";
 strTN               = "0000000000";
 strTelURI           = "";
}
// copy constructor
ALI_I3_AccessProvider::ALI_I3_AccessProvider(const ALI_I3_AccessProvider *a) {
 strAccessProviderID 	= a->strAccessProviderID;
 strName 		= a->strName;
 strTN   		= a->strTN;
 strTelURI		= a->strTelURI;
}
//assignment operator
ALI_I3_AccessProvider& ALI_I3_AccessProvider::operator=(const ALI_I3_AccessProvider& a)    {
 if (&a == this ) {return *this;}
 strAccessProviderID 	= a.strAccessProviderID;
 strName 		= a.strName;
 strTN   		= a.strTN;
 strTelURI		= a.strTelURI;
 return *this;
}


//constructor
ALI_I3_GeneralUse::ALI_I3_GeneralUse() {
 strID         = "";
 strGeneralUse = "";
}
// copy constructor
ALI_I3_GeneralUse::ALI_I3_GeneralUse(const ALI_I3_GeneralUse *a) {
 strID 			= a->strID;
 strGeneralUse 		= a->strGeneralUse;
}
//assignment operator
ALI_I3_GeneralUse& ALI_I3_GeneralUse::operator=(const ALI_I3_GeneralUse& a)    {
 if (&a == this ) {return *this;}
 strID 			= a.strID;
 strGeneralUse 		= a.strGeneralUse;
 return *this;
}




// constructor
ALI_I3_SourceInfo::ALI_I3_SourceInfo() {
 DataProvider.fClear();
 AccessProvider.fClear();	
 strALIUpdateGMT.clear();
 strALIRetrievalGMT.clear();
 vGeneralUses.clear();
}

//copy constructor
ALI_I3_SourceInfo::ALI_I3_SourceInfo(const ALI_I3_SourceInfo *a) {
 DataProvider		= a->DataProvider;
 AccessProvider		= a->AccessProvider;	
 strALIUpdateGMT	= a->strALIUpdateGMT;
 strALIRetrievalGMT	= a->strALIRetrievalGMT;
 this->vGeneralUses.clear();
 for (size_t i= 0; i < a->vGeneralUses.size(); i++)  {
  this->vGeneralUses.push_back(a->vGeneralUses[i]);
 }
}
//assignment operator
ALI_I3_SourceInfo& ALI_I3_SourceInfo::operator=(const ALI_I3_SourceInfo& a)    {
 if (&a == this ) {return *this;}
 DataProvider		= a.DataProvider;
 AccessProvider		= a.AccessProvider;	
 strALIUpdateGMT	= a.strALIUpdateGMT;
 strALIRetrievalGMT	= a.strALIRetrievalGMT;
 this->vGeneralUses.clear();
 for (size_t i= 0; i < a.vGeneralUses.size(); i++)  {
  this->vGeneralUses.push_back(a.vGeneralUses[i]);
 }
 return *this;
}

void ALI_I3_SourceInfo::fClear(){
 DataProvider.fClear();
 AccessProvider.fClear();	
 strALIUpdateGMT.clear();
 strALIRetrievalGMT.clear();
 vGeneralUses.clear();
}

//constructor
ALI_I3_NetworkInfo::ALI_I3_NetworkInfo() {
 strPSAPALIHost= strRespALIHost= strPSAPID= strPSAPName= strRouterID= strExchange= strCLLI = "";
}
//copy constructor
ALI_I3_NetworkInfo::ALI_I3_NetworkInfo(const ALI_I3_NetworkInfo *a) {
 strPSAPALIHost		= a->strPSAPALIHost;
 strRespALIHost		= a->strRespALIHost;	
 strPSAPID		= a->strPSAPID;
 strPSAPName		= a->strPSAPName;
 strRouterID		= a->strRouterID;	
 strExchange		= a->strExchange;
 strCLLI		= a->strCLLI;
}
//assignment operator
ALI_I3_NetworkInfo& ALI_I3_NetworkInfo::operator=(const ALI_I3_NetworkInfo& a)    {
 if (&a == this ) {return *this;}
 strPSAPALIHost		= a.strPSAPALIHost;
 strRespALIHost		= a.strRespALIHost;	
 strPSAPID		= a.strPSAPID;
 strPSAPName		= a.strPSAPName;
 strRouterID		= a.strRouterID;	
 strExchange		= a.strExchange;
 strCLLI		= a.strCLLI;
 return *this;
}

//constructor
ALI_I3_Extension::ALI_I3_Extension() {
 strName    = "";
 strSource  = "";
 strVersion = "0.0";
}
//copy constructor
ALI_I3_Extension::ALI_I3_Extension(const ALI_I3_Extension *a) {
 strName		= a->strName;
 strSource		= a->strSource;	
 strVersion		= a->strVersion;
}
//assignment operator
ALI_I3_Extension& ALI_I3_Extension::operator=(const ALI_I3_Extension& a)    {
 if (&a == this ) {return *this;}
 strName		= a.strName;
 strSource		= a.strSource;	
 strVersion		= a.strVersion;
 return *this;
}




// Constructor
ALI_I3_Data::ALI_I3_Data() {

  enumLISfunction           = NO_LIS_FUNCTION;
  objLocationURI.fClear();
  objGeoLocationHeaderURI.fClear();
  ServiceURNList.fClear();
  strPANI.clear();
  strNenaCallId.clear();
  strNenaIncidentId.clear();
  strRawCallinfoHeader.clear();
  strEntity.clear();
  boolPidflobyValue = false;
  strCallInfoProviderCID.clear();
  strCallInfoProviderURL.clear();
  strCallInfoServiceCID.clear();
  strCallInfoServiceURL.clear();
  strCallInfoSubscriberCID.clear();
  strCallInfoSubscriberURL.clear();
  I3XMLData.fClear();
  ADRdata.fClear();
  strBidId.clear();
  strPidfXMLData.clear();
  strNGALIRecord.clear();
  boolByValue = false;
  strALI30WRecord.clear();
  strALI30XRecord.clear();
  strTimeStamp.clear(); 
  strTupleID.clear();
  tsTimeStamp.tv_sec  = 0;
  tsTimeStamp.tv_nsec = 0;
  boolEmergencyCallDataRequestFlag = false;
  boolProviderInfoRXflag = false;
  boolServiceInfoRXflag =false;
  boolSubsciberInfoRXflag= false;
  boolPidfLoRXflag = false;
  boolADRreadyFlag = false;
  boolServiceURIreadyFlag = false;
  boolServiceFireRXflag = false;   //may go away ..
  boolServiceEmsRXflag = false;    //may go away ..
  boolServicePoliceRXflag = false; //may go away ..
 }

// copy constructor
ALI_I3_Data::ALI_I3_Data(const ALI_I3_Data *a) {

 enumLISfunction             		= a->enumLISfunction;
 objLocationURI             		= a->objLocationURI;
 objGeoLocationHeaderURI      		= a->objGeoLocationHeaderURI;
 ServiceURNList             		= a->ServiceURNList;
 strPANI             			= a->strPANI;
 strNenaCallId             		= a->strNenaCallId;
 strNenaIncidentId             		= a->strNenaIncidentId;
 strRawCallinfoHeader                   = a->strRawCallinfoHeader;
 strEntity             			= a->strEntity;
 boolPidflobyValue             		= a->boolPidflobyValue;
 strCallInfoProviderCID        		= a->strCallInfoProviderCID;
 strCallInfoProviderURL        		= a->strCallInfoProviderURL;
 strCallInfoServiceCID      		= a->strCallInfoServiceCID;
 strCallInfoServiceURL        		= a->strCallInfoServiceURL;
 strCallInfoSubscriberCID    		= a->strCallInfoSubscriberCID;
 strCallInfoSubscriberURL               = a->strCallInfoSubscriberURL;
 I3XMLData                              = a->I3XMLData;
 ADRdata                                = a->ADRdata;
 strBidId             			= a->strBidId;
 strPidfXMLData             		= a->strPidfXMLData;
 boolByValue             		= a->boolByValue;
 strALI30WRecord             		= a->strALI30WRecord;
 strALI30XRecord             		= a->strALI30XRecord;
 strNGALIRecord                         = a->strNGALIRecord;
 strTimeStamp        			= a->strTimeStamp;
 strTupleID             		= a->strTupleID;
 tsTimeStamp.tv_sec                     = a->tsTimeStamp.tv_sec;
 tsTimeStamp.tv_nsec                    = a->tsTimeStamp.tv_nsec;
 boolEmergencyCallDataRequestFlag      	= a->boolEmergencyCallDataRequestFlag;
 boolProviderInfoRXflag             	= a->boolProviderInfoRXflag;
 boolServiceInfoRXflag             	= a->boolServiceInfoRXflag;
 boolSubsciberInfoRXflag        	= a->boolSubsciberInfoRXflag;
 boolPidfLoRXflag        		= a->boolPidfLoRXflag;
 boolADRreadyFlag                       = a->boolADRreadyFlag;
 boolServiceURIreadyFlag             	= a->boolServiceURIreadyFlag;
 boolServiceFireRXflag      		= a->boolServiceFireRXflag;
 boolServiceEmsRXflag             	= a->boolServiceEmsRXflag;
 boolServicePoliceRXflag             	= a->boolServicePoliceRXflag;
}

ALI_I3_Data& ALI_I3_Data::operator=(const ALI_I3_Data& a)    {
 if (&a == this ) {return *this;}

 enumLISfunction             		= a.enumLISfunction;
 objLocationURI             		= a.objLocationURI;
 objGeoLocationHeaderURI      		= a.objGeoLocationHeaderURI;
 ServiceURNList             		= a.ServiceURNList;
 strPANI             			= a.strPANI;
 strNenaCallId             		= a.strNenaCallId;
 strNenaIncidentId             		= a.strNenaIncidentId;
 strRawCallinfoHeader                   = a.strRawCallinfoHeader;
 strEntity             			= a.strEntity;
 boolPidflobyValue             		= a.boolPidflobyValue;
 strCallInfoProviderCID        		= a.strCallInfoProviderCID;
 strCallInfoProviderURL        		= a.strCallInfoProviderURL;
 strCallInfoServiceCID      		= a.strCallInfoServiceCID;
 strCallInfoServiceURL        		= a.strCallInfoServiceURL;
 strCallInfoSubscriberCID    		= a.strCallInfoSubscriberCID;
 strCallInfoSubscriberURL               = a.strCallInfoSubscriberURL;
 I3XMLData                              = a.I3XMLData;
 ADRdata                                = a.ADRdata;
 strBidId             			= a.strBidId;
 strPidfXMLData             		= a.strPidfXMLData;
 boolByValue             		= a.boolByValue;
 strALI30WRecord             		= a.strALI30WRecord;
 strALI30XRecord                        = a.strALI30XRecord;
 strNGALIRecord                         = a.strNGALIRecord;
 strTimeStamp        			= a.strTimeStamp;
 strTupleID             		= a.strTupleID;
 tsTimeStamp.tv_sec                     = a.tsTimeStamp.tv_sec;
 tsTimeStamp.tv_nsec                    = a.tsTimeStamp.tv_nsec;
 boolEmergencyCallDataRequestFlag      	= a.boolEmergencyCallDataRequestFlag;
 boolProviderInfoRXflag             	= a.boolProviderInfoRXflag;
 boolServiceInfoRXflag             	= a.boolServiceInfoRXflag;
 boolSubsciberInfoRXflag        	= a.boolSubsciberInfoRXflag;
 boolPidfLoRXflag        		= a.boolPidfLoRXflag;
 boolADRreadyFlag                       = a.boolADRreadyFlag;
 boolServiceURIreadyFlag             	= a.boolServiceURIreadyFlag;
 boolServiceFireRXflag      		= a.boolServiceFireRXflag;
 boolServiceEmsRXflag             	= a.boolServiceEmsRXflag;
 boolServicePoliceRXflag             	= a.boolServicePoliceRXflag;
 return *this;
}


void ALI_I3_Data::fClear() {
  enumLISfunction           = NO_LIS_FUNCTION;
  objLocationURI.fClear();
  objGeoLocationHeaderURI.fClear();
  ServiceURNList.fClear();
  strPANI.clear();
  strNenaCallId.clear();
  strNenaIncidentId.clear();
  strRawCallinfoHeader.clear();
  strEntity.clear();
  boolPidflobyValue = false;
  strCallInfoProviderCID.clear();
  strCallInfoProviderURL.clear();
  strCallInfoServiceCID.clear();
  strCallInfoServiceURL.clear();
  strCallInfoSubscriberCID.clear();
  strCallInfoSubscriberURL.clear();
  I3XMLData.fClear();
  ADRdata.fClear();
  strBidId.clear();
  strPidfXMLData.clear();
  strNGALIRecord.clear();
  boolByValue = false;
  strALI30WRecord.clear();
  strALI30XRecord.clear();
  strTimeStamp.clear(); 
  strTupleID.clear();
  tsTimeStamp.tv_sec  = 0;
  tsTimeStamp.tv_nsec = 0;
  boolEmergencyCallDataRequestFlag = false;
  boolProviderInfoRXflag = false;
  boolServiceInfoRXflag =false;
  boolSubsciberInfoRXflag= false;
  boolPidfLoRXflag = false;
  boolADRreadyFlag = false;
  boolServiceURIreadyFlag = false;
  boolServiceFireRXflag = false;   //may go away ..
  boolServiceEmsRXflag = false;    //may go away ..
  boolServicePoliceRXflag = false; //may go away ..
}

void ALI_I3_Data::fDisplay() {

 cout << "I3 INFORMATION <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl<<endl;
 cout << "LocationURI				" << objLocationURI.strURI << endl;
 cout << "GeoLocationHeader                     " << objGeoLocationHeaderURI.strURI << endl;
 cout << "NENA CallID                           " << strNenaCallId << endl;
 cout << "NENA IncidentId                       " << strNenaIncidentId << endl;
 cout << "entity                                " << strEntity << endl;
 cout << "BidID                                 " << strBidId << endl;
 cout << "TupleID                               " << strTupleID << endl;
 cout << "I3 INFORMATION <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl<<endl;
}


//Copy Constructor
SERVICELIST::SERVICELIST(const SERVICELIST *a) {
 sem_init(&sem_tMutexVServiceURN,0,1);
 ECRF_Server                       = a->ECRF_Server;

 this->vServiceURN.clear();
 for (size_t i= 0; i < a->vServiceURN.size(); i++)  {
  this->vServiceURN.push_back(a->vServiceURN[i]);
 }
 boolBidMade                       = a->boolBidMade;
 boolListBuilt                     = a->boolListBuilt;
}

SERVICELIST& SERVICELIST::operator=(const SERVICELIST& b) {
 if (&b == this ) {return *this;}
 ECRF_Server                       = b.ECRF_Server;
 sem_init(&sem_tMutexVServiceURN,0,1);
 this->vServiceURN.clear();
 for (size_t i= 0; i < b.vServiceURN.size(); i++)  {
  this->vServiceURN.push_back(b.vServiceURN[i]);
 }
 boolBidMade                       = b.boolBidMade;
 boolListBuilt                     = b.boolListBuilt;

 return *this;
}


void SERVICELIST::fClear() {

 ECRF_Server.clear();
 vServiceURN.clear();
 boolBidMade = false;
 boolListBuilt = false;
}

void SERVICELIST::fSetBid() { 
 this->boolBidMade   = true;
 this->boolListBuilt = false;
 return;
}

bool SERVICELIST::fAll_URIS_RX() {
 bool 		boolRunningCheck = true;
 extern string 	strNG911_ECRF_SERVER; 

// //cout << " bid -> " << this->boolBidMade << " list built -> " << this->boolListBuilt << " list empty -> " << this->vServiceURN.empty() << endl;

 if (strNG911_ECRF_SERVER == DEFAULT_VALUE_LIS_INI_KEY_9)                     {return true;}
 if ((this->boolBidMade)&&(this->boolListBuilt)&&(this->vServiceURN.empty())) {return true;}
 if ((this->boolBidMade)&&(!this->boolListBuilt))                             {return false;}

 sem_wait(&sem_tMutexVServiceURN);
 for (std::vector<SERVICE_URN>::iterator it = vServiceURN.begin() ; it != vServiceURN.end(); ++it)  {
   boolRunningCheck = boolRunningCheck && it->boolServiceURIreceivedFlag;
 }
 sem_post(&sem_tMutexVServiceURN); 
 return boolRunningCheck;
}

int SERVICELIST::ServiceBidIndex(int iNumber) {
 size_t sz; 
 sem_wait(&sem_tMutexVServiceURN); //-----------------------------------------------------------

 sz = this->vServiceURN.size();
 for (size_t i = 0; i < sz; i++) {
  if (this->vServiceURN[i].iBid == iNumber) { sem_post(&sem_tMutexVServiceURN);/*--------*/ return i;}
 }
 sem_post(&sem_tMutexVServiceURN); //-----------------------------------------------------------
 return -1;
}

int SERVICELIST::fRemoveIndexWithBid(int iNumber) {
 size_t sz;

 sem_wait(&sem_tMutexVServiceURN); //-----------------------------------------------------------

 sz = this->vServiceURN.size();
 for (size_t i = 0; i < sz; i++) {
  if (this->vServiceURN[i].iBid == iNumber) {
   this->vServiceURN.erase(this->vServiceURN.begin()+i);
   sem_post(&sem_tMutexVServiceURN); //-----------------------------------------------------------
   return 1;
  }
 }
 sem_post(&sem_tMutexVServiceURN); //-----------------------------------------------------------
 return -1;
}

int SERVICELIST::PoliceIndex() {
 // ensure semaphore pre called
 size_t sz = this->vServiceURN.size();
 size_t found;

 for (size_t i = 0; i < sz; i++) {

  found = this->vServiceURN[i].strURN.find("police");
  if (found != string::npos) {
   return i;
  }
 }
 return -1;
}

int SERVICELIST::FireIndex() {
 // ensure semaphore pre called
 size_t sz = this->vServiceURN.size();
 size_t found;

 for (size_t i = 0; i < sz; i++) {

  found = this->vServiceURN[i].strURN.find("fire");
  if (found != string::npos) {
   return i;
  }
 }
 return -1;
}

int SERVICELIST::EMSIndex() {
 size_t sz = this->vServiceURN.size();
 size_t found;

 for (size_t i = 0; i < sz; i++) {

  found = this->vServiceURN[i].strURN.find("ems");
  if (found != string::npos) {
   return i;
  }
 found = this->vServiceURN[i].strURN.find("ambulance");
  if (found != string::npos) {
   return i;
  }  
 }
 return -1;
}

SERVICE_URN::SERVICE_URN() {
  strURN.clear();
  iBid                       = 0;
  strPurpose.clear();
  boolServiceURIreceivedFlag = false;
  strServiceURI.clear();
  strServiceNumber.clear();
  strDisplayName.clear();
  strDataRX.clear();
}

SERVICE_URN::SERVICE_URN(const SERVICE_URN *a) {

 strURN				= a->strURN;
 iBid                           = a->iBid;
 strPurpose			= a->strPurpose;
 boolServiceURIreceivedFlag	= a->boolServiceURIreceivedFlag;
 strServiceURI			= a->strServiceURI;
 strServiceNumber               = a->strServiceNumber;
 strDisplayName                 = a->strDisplayName;
 strDataRX                      = a->strDataRX;
}

SERVICE_URN& SERVICE_URN::operator=(const SERVICE_URN& a) {
 if (&a == this ) {return *this;}
 strURN				= a.strURN;
 iBid                           = a.iBid;
 strPurpose			= a.strPurpose;
 boolServiceURIreceivedFlag	= a.boolServiceURIreceivedFlag;
 strServiceURI			= a.strServiceURI;
 strServiceNumber               = a.strServiceNumber;
 strDisplayName                 = a.strDisplayName;
 strDataRX                      = a.strDataRX;
 return *this;
}

bool SERVICE_URN::fDetermineURI() {

 XMLNode  		MainNode, ServiceResponceNode, MappingNode, DisplayNameNode, ServiceNode, ServiceNumberNode, URInode, ErrorNode, RedirectNode;
 XMLResults         	xe;
 string                 strName;
 bool                   boolError = false;
 bool                   boolRedirect = false;
 bool                   boolService = false;
 enum MessageType       {uriUNDEF, uriERROR, uriREDIRECT, uriSERVICE};
 MessageType            Message = uriUNDEF;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 MainNode = XMLNode::parseString(this->strDataRX.c_str(),NULL,&xe);
 if (xe.error) {
  SendCodingError("ali_i3.functions.cpp - fDetermineURI() Unable to parse xml!");
  return false;
 } 

 if (MainNode.getName())   {strName = MainNode.getName();}
 else                      {return false;}

// //cout << "fDetermineURI() main node is -> " << strName << endl; 

 if (RemoveXMLnamespace(strName) == "findServiceResponse" ) {
  ServiceResponceNode = MainNode;
 }
 else {
  ServiceResponceNode =  LoadChildNode(MainNode, "findServiceResponse");
 }
 if (!ServiceResponceNode.isEmpty()) {
   Message = uriSERVICE;
 }
 
 if (Message == uriUNDEF) {
  if (RemoveXMLnamespace(strName) == "error" ) {
   ErrorNode = MainNode;
  }
  else {
   ErrorNode =  LoadChildNode(MainNode, "error");
  }
 }
 if (!ErrorNode.isEmpty()) {
   Message = uriERROR;
 }

 if (Message == uriUNDEF) {
  if (RemoveXMLnamespace(strName) == "errors" ) {
   ErrorNode = MainNode;
  }
  else {
   ErrorNode =  LoadChildNode(MainNode, "errors");
  }
 }
 if (!ErrorNode.isEmpty()) {
   Message = uriERROR;
 }

 if (Message == uriUNDEF) {
  if (RemoveXMLnamespace(strName) == "redirect" ) {
   RedirectNode = MainNode;
  }
  else {
   RedirectNode =  LoadChildNode(MainNode, "redirect");
  }
 }
 if (!RedirectNode.isEmpty()) {
   Message = uriREDIRECT;
 }


 switch (Message) {

 case uriUNDEF:
      SendCodingError("ali_i3.functions.cpp - fDetermineURI() Unable to determine xml!");
      return false;

 case uriERROR:
      //cout << "SERVICE_URN::fDetermineURI() ERROR FOUND" <<endl;
      return false;

 case uriREDIRECT:
      //cout << "SERVICE_URN::fDetermineURI() REDIRECT" << endl;
      return false;

 case uriSERVICE:
      MappingNode     = LoadChildNode(ServiceResponceNode, "mapping");
 
      DisplayNameNode 	= LoadChildNode(MappingNode, "displayName");
      ServiceNode     	= LoadChildNode(MappingNode, "service");
      URInode         	= LoadChildNode(MappingNode, "uri");
      ServiceNumberNode = LoadChildNode(MappingNode, "serviceNumber");

      if (DisplayNameNode.getText())  	{this->strDisplayName = DisplayNameNode.getText();}
      if (URInode.getText())          	{this->strServiceURI = URInode.getText();}
      if (ServiceNumberNode.getText())	{this->strServiceNumber = ServiceNumberNode.getText();}
/*
      //cout << "SERVICE_URN::fDetermineURI() SERVICE DATA ->" << endl;
      //cout << this->strDisplayName << endl;
      //cout << this->strServiceURI << endl;
      //cout << this->strServiceNumber << endl;
*/
      return true;

 }
 // should not get here ....
 return false;
}

void SERVICE_URN::fClear() {

 strURN.clear();
 iBid                       = 0;
 strPurpose.clear();
 boolServiceURIreceivedFlag = false;
 strServiceURI.clear();
 strServiceNumber.clear();
 strDisplayName.clear();
 strDataRX.clear();
}

// Constructor
ALI_I3_XML_Data::ALI_I3_XML_Data(){
 XMLstring.clear();
 vExtension.clear();
 CallInfo.fClear();
 LocationInfo.fClear();
 Agencies.fClear();
 SourceInfo.fClear();
 NetworkInfo.fClear();
}

ALI_I3_XML_Data::ALI_I3_XML_Data(const ALI_I3_XML_Data *a) {

 XMLstring				= a->XMLstring;
 CallInfo                               = a->CallInfo;
 LocationInfo				= a->LocationInfo;
 Agencies				= a->Agencies;
 SourceInfo				= a->SourceInfo;
 NetworkInfo				= a->NetworkInfo;
 this->vExtension.clear();
 for (size_t i= 0; i < a->vExtension.size(); i++)  {
  this->vExtension.push_back(a->vExtension[i]);
 }
}

ALI_I3_XML_Data& ALI_I3_XML_Data::operator=(const ALI_I3_XML_Data& a) {
 if (&a == this ) {return *this;}
 XMLstring				= a.XMLstring;
 CallInfo                               = a.CallInfo;
 LocationInfo				= a.LocationInfo;
 Agencies				= a.Agencies;
 SourceInfo				= a.SourceInfo;
 NetworkInfo				= a.NetworkInfo;
 this->vExtension.clear();
 for (size_t i= 0; i < a.vExtension.size(); i++)  {
  this->vExtension.push_back(a.vExtension[i]);
 }
 return *this;
}

LocationURI::LocationURI(const LocationURI *a) {

 strURI				= a->strURI;
 boolDataRecieved		= a->boolDataRecieved;
 strExpires			= a->strExpires;
}

LocationURI& LocationURI::operator=(const LocationURI& a) {
 if (&a == this ) {return *this;}
 strURI				= a.strURI;
 boolDataRecieved		= a.boolDataRecieved;
 strExpires			= a.strExpires;
 return *this;
}


void LocationURI::fClear()
{
 strURI.clear();
 boolDataRecieved = false;
 strExpires.clear();
}

bool LocationURI::fDataRecieved()
{
 if (this->strURI.length() > 0) { this->boolDataRecieved = true;}
 else                           { this->boolDataRecieved = false;}
 return this->boolDataRecieved;
}

bool ALI_I3_Data::fLoadGeoLocationURI(string strInput) {

 size_t found;
 string strData;

 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_GEOLOCATION_WO_COLON));
 if (strData.length())  {
   found = strData.find("<");
   if (found == string::npos) 	{return fLoadGeoLocationURINoLTGT(strData);}

   strData = strData.substr(found+1,string::npos);

   found = strData.find(">");
   if (found == string::npos) 	{return false;}
 
   strData = strData.substr(0,found);
   this->objGeoLocationHeaderURI.strURI = strData;
   this->objGeoLocationHeaderURI.boolDataRecieved = true;
   return true;
 }


 return false;
}

bool ALI_I3_Data::fLoadGeoLocationURINoLTGT(string strInput) {

   this->objGeoLocationHeaderURI.strURI = strInput;
   this->objGeoLocationHeaderURI.boolDataRecieved = true;
 return false;
}

bool LocationURI::fLoadLocationURI(string strInput)
{
 size_t 		start, end, found;
 string 		strMultipart, strXMLData, strMessage, strName;
 XMLResults         	xe;
 XMLNode  		MainNode, LocationResponseNode, LocationURINode, LocationUriSetNode;
 extern string 		extractPidfXMLData(string strInput);

 boolDataRecieved = false;
 if (strInput.empty())		{return false;}

// Extract locationURI from Geolocation Header
 this->strURI = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_GEOLOCATION_WO_COLON));

 if (strURI.length())  {
   found = this->strURI.find("<");
   if (found == string::npos) 	{this->strURI.clear();return false;}

   this->strURI = this->strURI.substr(found+1,string::npos);

   found = this->strURI.find(">");
   if (found == string::npos) 	{this->strURI.clear();return false;}
 
   this->strURI = this->strURI.substr(0,found);
   this->boolDataRecieved = true;
   return true;
  }


// otherwise Extract locationURI from Pidflo data
 strMultipart = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_MULTIPART_WO_COLON ));
 strXMLData   = extractPidfXMLData(strMultipart);
 strXMLData   = RemoveXMLHeader(strXMLData);
 this->strURI.clear();

 if (strXMLData.empty()) { return false;}
 MainNode=XMLNode::parseString(strXMLData.c_str(),NULL,&xe);
 
 if (xe.error) {return false;}  

// if (MainNode.getName())   {strName = MainNode.getName();}
// //cout << strName << endl;

 LocationUriSetNode= MainNode.getChildNode("locationUriSet");
 if (LocationUriSetNode.isEmpty())      { return false;}

 LocationURINode= LocationUriSetNode.getChildNode("locationURI");
 if (LocationURINode.isEmpty())      { return false;}

 if (LocationURINode.getText())  {this->strURI = LocationURINode.getText();}

 ////cout << "uri in fn -> " << this->strURI << endl;
 if (this->strURI.length()) { this->boolDataRecieved = true; return true;}

return false;
}








void ALI_I3_StreetAddress::fDisplay() {
 cout << "HoueNum             = " << strHouseNum << endl;
 cout << "HouseNumSuffix      = " << strHouseNumSuffix << endl;
 cout << "PrefixDirectional   = " <<  strPrefixDirectional << endl;
 cout << "StreetName          = " << strStreetName << endl;
 cout << "StreetSuffix        = " << strStreetSuffix << endl;
 cout << "PostDirectional     = " <<  strPostDirectional << endl;
 cout << "TextualAddress      = " << strTextualAddress << endl;
 cout << "MSAGCommunity       = " << strMSAGCommunity << endl;
 cout << "PostalCommunity     = " << strPostalCommunity << endl;
 cout << "StateProvince       = " << strStateProvince << endl;
 cout << "CountyID            = " << strCountyID << endl;
 cout << "Country             = " << strCountry << endl;
 cout << "TARCode             = " << strTARCode << endl;
 cout << "PostalZIPCode       = " << strPostalZipCode << endl;
 cout << "Building            = " << strBuilding << endl;
 cout << "Floor               = " << strFloor << endl;
 cout << "UnitNum             = " << strUnitNum << endl;
 cout << "UnitType            = " << strUnitType << endl;
 cout << "LocationDescription = " << strLocationDescription << endl;
 cout << "LandmarkAddress     = " << strLandmarkAddress << endl;

}




bool ALI_I3_Data::fLoadBidID(string strInput) {

 if (strInput.empty())		{return false;}

 strBidId = strInput;
 return true;

}


bool ALI_I3_Data::fLoadLocationURI(string strInput)
{
 size_t found;

 if (strInput.empty())		{return false;}

 objLocationURI.strURI = strInput;
 found = objLocationURI.strURI.find("<");
 if (found == string::npos) 	{return false;}

 objLocationURI.strURI = objLocationURI.strURI.substr(found+1,string::npos);

 found = objLocationURI.strURI.find(">");
 if (found == string::npos) 	{return false;}

 objLocationURI.strURI = objLocationURI.strURI.substr(0,found);
 objLocationURI.boolDataRecieved = true;
return true;
}



string ALI_I3_Data::fBidId()
{
 string strOutput = strBidId;

 strOutput = FindandReplaceALL(strOutput, ":", "");
 strOutput = FindandReplaceALL(strOutput, "-", "");
 strOutput = FindandReplaceALL(strOutput, "(", "");
 strOutput = FindandReplaceALL(strOutput, ")", "");
 strOutput = FindandReplaceALL(strOutput, " ", "");

return strOutput;
}


string  ALI_I3_Data::fTimeStamp()
{
 clock_gettime(CLOCK_REALTIME, &tsTimeStamp);

 strTimeStamp = get_ALI_Record_Local_TimeStamp(tsTimeStamp);

 return strTimeStamp;
}


bool ALI_I3_Data::fLoadServiceList(string strInput) {

 string 		strData, line;
 string 		strOutputData;
 size_t 		found;
 XMLNode       		MainNode, listServicesResponseNode, ServiceListNode;
 XMLResults         	xe;
 string                 strName;
 SERVICE_URN 		objServiceData;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);


 //cout << "fLoadServiceList() ->" << endl;

 strData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<listServicesByLocationResponse xmlns=\"urn:ietf:params:xml:nd:lost1\">\n<serviceList>urn:nena:responder.fire\nurn:nena:responder.police\n</serviceList>\n<path>\n<via source=\"ecrf.a9-1-1.net\"/>\n</path>\n<locationUsed id=\"233273322\"/>\n</listServicesByLocationResponse>\n";

 if (strInput.empty()) { return false;}
 MainNode=XMLNode::parseString(strInput.c_str(),NULL,&xe);
 
 if (xe.error) {return false;}  
 
 this->ServiceURNList.vServiceURN.clear();

 
 // check for Top Node ..
 if (MainNode.getName())   {strName = MainNode.getName();}

 //check for listServicesResponse
 found = RemoveXMLnamespace(strName).find("listServicesByLocationResponse");
 if (found == string::npos) {
  listServicesResponseNode = LoadChildNode(MainNode, "listServicesByLocationResponse");
 }
 else {
  listServicesResponseNode = MainNode;
 }

 ServiceListNode = LoadChildNode(listServicesResponseNode, "serviceList");
 if (ServiceListNode.isEmpty()) {
  SendCodingError("ALI_I3_Data::fLoadServiceList() serviceList is empty!");
  return false;
 } 

 if (ServiceListNode.getText()) {
  strData = ServiceListNode.getText();
 }        
 else {
  SendCodingError("ALI_I3_Data::fLoadServiceList() no serviceList no Text!");
  return false;
 }   

 stringstream ss(strData);

 while (getline(ss, line, '\n')) {
   strOutputData = FindandReplaceALL(line,"\r", "");
   strOutputData = RemoveLeadingSpaces(strOutputData);
   strOutputData = RemoveTrailingSpaces(strOutputData);
     
   objServiceData.strURN = strOutputData;
   //cout << "a. " << strOutputData << endl;

   sem_wait(&this->ServiceURNList.sem_tMutexVServiceURN);//---------------------------------------
   this->ServiceURNList.vServiceURN.push_back(objServiceData);
   sem_post(&this->ServiceURNList.sem_tMutexVServiceURN);//---------------------------------------
 }

 //cout << "ENDING SIZE -> " << this->ServiceURNList.vServiceURN.size() << endl;
 return true;

}

bool ALI_I3_Data::fLoadNenaCallId(string strInput) {

 string strData;
 string strOutputData;
 size_t StartPosition, EndPosition;
 string strKey = FREESWITCH_CLI_VARIABLE_NENA_UID_CALLID_WITH_COLON; 
// //cout << "fLoadNenaCallId() ->" << endl;
 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CALL_INFO_WO_COLON));
 
 if (strData.empty()) {
  return false;
 }
// //cout << "PARSING a" << endl;
 StartPosition = strData.find(strKey);
 if (StartPosition == string::npos)                   {return false;}
// //cout << "PARSING b" << endl;
 StartPosition += strKey.length();
 if (StartPosition >= strData.length())               {return false;}
// //cout << "PARSING c" << endl;
 EndPosition = strData.find(">;", StartPosition);
 if (EndPosition == string::npos)                     {return false;}
// //cout << "PARSING d" << endl;
 strOutputData.assign(strData,StartPosition,(EndPosition - StartPosition));
 //remove leading blanks ...
 strOutputData = RemoveLeadingSpaces(strOutputData);
 strOutputData = RemoveTrailingSpaces(strOutputData);
 this->strNenaCallId = strOutputData;
 this->strRawCallinfoHeader = strData;
 return true;
}

bool ALI_I3_Data::fLoadPidfloEntity(string strInput) {

 string strData;
 string strOutputData;

 strOutputData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_PIDFLO_ENTITY_WO_COLON));

 if (strOutputData.length()) {
  this->strEntity = strOutputData;
  boolPidflobyValue = true;
  return true;
 }
 else {
  return false;
 }
}

bool ALI_I3_Data::fLoadNenaIncidentId(string strInput) {

 string strData;
 string strOutputData;
 size_t StartPosition, EndPosition;
 string strKey = FREESWITCH_CLI_VARIABLE_NENA_UID_INCIDENTID_WITH_COLON; 

 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CALL_INFO_WO_COLON));

 if (strData.empty()) {
  return false;
 }

 StartPosition = strData.find(strKey);
 if (StartPosition == string::npos)                   {return false;}
 StartPosition += strKey.length();
 if (StartPosition >= strData.length())               {return false;}
 EndPosition = strData.find(">;", StartPosition);
 if (EndPosition == string::npos)                     {return false;}
 strOutputData.assign(strData,StartPosition,(EndPosition - StartPosition));
 //remove leading blanks ...
 strOutputData = RemoveLeadingSpaces(strOutputData);
 strOutputData = RemoveTrailingSpaces(strOutputData);
 this->strNenaIncidentId = strOutputData;

 return true;
}

bool ALI_I3_Data::fLoadCallInfoEmergencyCallData(string strInput, string strType) {


 size_t StartPosition, EndPosition, StartCID, StartURL;
 bool   boolIsCID, boolIsURL;
 int    iswitch;
 string strOutputData;
 string strKey = "<cid:"; 
 boolIsCID = boolIsURL = false;

// //cout << "fLoadCallInfoEmergencyCallData -> " << endl;
// //cout << strInput << endl;

 if      (strType == "SERVICE")    {iswitch = 1;}
 else if (strType == "PROVIDER")   {iswitch = 2;}
 else if (strType == "SUBSCRIBER") {iswitch = 3;}
 else                              {return false;}

 StartPosition = strInput.find(strKey);
 if (StartPosition != string::npos) {
  boolIsCID = true;
  StartCID = StartPosition+strKey.length();
 }
 else {
  string strKey = "<http"; 
  StartPosition = strInput.find(strKey);
  if (StartPosition != string::npos) {
   boolIsURL = true;
   StartURL = StartPosition + 1;
  }
 }

 if ((!boolIsCID)&&(!boolIsURL)) { return false;}

 EndPosition = strInput.find(">;", 0);
 if (EndPosition == string::npos)                     {return false;}


 if (boolIsCID) {
  StartPosition = StartCID;
  if (StartPosition >= strInput.length())               {return false;}
  strOutputData.assign(strInput,StartPosition,(EndPosition - StartPosition));
  strOutputData = RemoveLeadingSpaces(strOutputData);
  strOutputData = RemoveTrailingSpaces(strOutputData);
//  //cout << strOutputData << endl;
  switch (iswitch) {
   case 1:
          this->strCallInfoServiceCID    = strOutputData; break;
   case 2:
          this->strCallInfoProviderCID   = strOutputData; break;
   case 3:
          this->strCallInfoSubscriberCID = strOutputData; break;
   default:
    return false;
  }
 }

 if (boolIsURL) {
  StartPosition = StartURL;
  if (StartPosition >= strInput.length())               {return false;}
  strOutputData.assign(strInput,StartPosition,(EndPosition - StartPosition));
  strOutputData = RemoveLeadingSpaces(strOutputData);
  strOutputData = RemoveTrailingSpaces(strOutputData);
//  //cout << strOutputData << endl;
  switch (iswitch) {
   case 1:
          this->strCallInfoServiceURL    = strOutputData; break;
   case 2:
          this->strCallInfoProviderURL   = strOutputData; break;
   case 3:
          this->strCallInfoSubscriberURL = strOutputData; break;
   default:
    return false;
  }
 }
 
 return true;
}




bool ALI_I3_Data::fLoadCallInfoEmergencyCallData(string strInput) {
/* Data will be <array:  blah blah |: blah blah |: blah blah |: blah blah >
 it must be in an array or return false (we are pulling 2 portions)

 loop through till we find both

*/

 string strData, strSubstring;
 string strOutputData;
 size_t StartPosition, EndPosition, found;
 string strKey       = FREESWITCH_CARAT_ARRAY_COLON_COLON;
 string strKeyTwo    = FREESWITCH_ARRAY_COLON_COLON;
 string strDelimeter = "|:";

//FREESWITCH_PURPOSE_PROVIDER_INFO_WO_COLON; 

 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CALL_INFO_WO_COLON));

 if (strData.empty()) {
  return false;
 }

 ////cout << " Load Emergency Call Data " << endl;
 ////cout << strData << endl;

 // remove "<ARRAY::" or "ARRAY::"
 StartPosition = strData.find(strKey);
 if (StartPosition != string::npos) {
  strData.erase(0, (StartPosition+strKey.length()) );
 }
 else {
   StartPosition = strData.find(strKeyTwo);
   if (StartPosition != string::npos) {
    strData.erase(0, (StartPosition+strKeyTwo.length()) );
   }
   else {
    cout << "ALI_I3_Data::fLoadCallInfoEmergencyCallData -> No Array found!" << endl;
    //cout << strData << endl;
    return false;
   }
 }

 do {
 StartPosition = 0;
 
 EndPosition = strData.find(strDelimeter);
 
 strSubstring = strData.substr(0, EndPosition); 

 found = strSubstring.find(FREESWITCH_PURPOSE_PROVIDER_INFO_WO_COLON);
 if (found != string::npos) {
  this->fLoadCallInfoEmergencyCallData(strSubstring, "PROVIDER");
 }

 found = strSubstring.find(FREESWITCH_PURPOSE_SUBSCRIBER_INFO_WO_COLON);
 if (found != string::npos) {
  this->fLoadCallInfoEmergencyCallData(strSubstring, "SUBSCRIBER");
 }

 found = strSubstring.find(FREESWITCH_PURPOSE_SERVICE_INFO_WO_COLON);
 if (found != string::npos) {
  this->fLoadCallInfoEmergencyCallData(strSubstring, "SERVICE");
 }

 strData.erase(0, EndPosition+strDelimeter.length());
 
 } while (EndPosition != string::npos);

 return true;
}

string  ALI_I3_Data::fCreateNextGenALIrecord()
{
 vector <string> vLine;
 string strLine = "";;
 string strBLANK, strCR, strSPACE, strDASH, strPPOUND = "";
 string strLPAREN, strRPAREN, strFWDSLASH;
 string strStreetNameCombined;
 size_t                sz; 

 strBLANK.assign(32, ' ');
 strCR    += charCR;
 strSPACE += charSPACE; 
 strDASH     = "-";
 strPPOUND   = "P#";
 strLPAREN   = "(";
 strRPAREN   = ")";
 strFWDSLASH = "/";

// Check GeneralUses ........................................ Set Line One .................................................................................................................
 sz = I3XMLData.SourceInfo.vGeneralUses.size();

 strLine = "   ";

 if (!this->I3XMLData.SourceInfo.strALIRetrievalGMT.empty()) {
   strTimeStamp = this->fGMTtoLocalTimeStamp(this->I3XMLData.SourceInfo.strALIRetrievalGMT);
 }
 for (unsigned int i = 0; i < sz; i++)  {
   if      (I3XMLData.SourceInfo.vGeneralUses[i].strID == "type")     {strLine.replace(0,1, I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse);}
   else if (I3XMLData.SourceInfo.vGeneralUses[i].strID == "index")    {strLine.replace(1,2, I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse);}
   else if (I3XMLData.SourceInfo.vGeneralUses[i].strID == "norecord") {
    if (I3XMLData.CallInfo.strCallbackNum.length() >= 10) {
     strLine += I3XMLData.CallInfo.strCallbackNum.substr(0,3);
     strLine += strDASH;
     strLine += I3XMLData.CallInfo.strCallbackNum.substr(3,3);
     strLine += strDASH;
     strLine += I3XMLData.CallInfo.strCallbackNum.substr(6,4);
    }
    strLine += strSPACE;
    strLine += I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse;
    strNGALIRecord = strLine; return strNGALIRecord;
   } 
 } //end for usigned int i

 if (!sz) {strLine = "200";}
 strLine += charCR;
 
 vLine.push_back(strLine);
// ......................................................... Set Line Two .................................................................................................................

// Callback Class of service timestamp <CR>

 if (I3XMLData.CallInfo.strCallbackNum.length() >= 10)
  {
   strLine = "(";
   strLine += I3XMLData.CallInfo.strCallbackNum.substr(0,3);
   strLine += ") ";
   strLine += I3XMLData.CallInfo.strCallbackNum.substr(3,3);;
   strLine += strDASH;
   strLine += I3XMLData.CallInfo.strCallbackNum.substr(6,4);
  }
 strLine += strSPACE;
 strLine += I3XMLData.CallInfo.strClassOfService;
 strLine += strSPACE;
 strLine += strTimeStamp;
 strLine += strCR;
 vLine.push_back(strLine);

 strLine = "Telco: ";
 strLine += I3XMLData.SourceInfo.AccessProvider.strName;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = "pANI: ";
 strLine += I3XMLData.CallInfo.strCallingPartyNum;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

// ......................................................... Set Line Three ...............................................................................................................
//Customer Name 

 strLine = "Nm: ";
 strLine += I3XMLData.CallInfo.strCustomerName;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = "Attention Code: ";
 strLine += I3XMLData.CallInfo.strAttentionIndicatorCode;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = "Attention Indicator: ";
 strLine += I3XMLData.CallInfo.strAttentionIndicator;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

strLine = "Special Message: ";
 strLine += I3XMLData.CallInfo.strSpecialMessage;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

// ......................................................... Set Line Four ................................................................................................................
//House Number

 strLine = "Hn: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strHouseNum;
 strLine += strSPACE;
 strLine += I3XMLData.LocationInfo.StreetAddress.strHouseNumSuffix;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Five ................................................................................................................
// Street 

 strLine = "St: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strPrefixDirectional;
 strLine += strSPACE;
 
 strStreetNameCombined = RemoveTrailingSpaces(I3XMLData.LocationInfo.StreetAddress.strStreetName);
 strStreetNameCombined = RemoveLeadingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += I3XMLData.LocationInfo.StreetAddress.strStreetSuffix;
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += I3XMLData.LocationInfo.StreetAddress.strPostDirectional;
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);

 strLine += strStreetNameCombined;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

// ......................................................... Set Line Six ................................................................................................................. 
// City

 strLine = "City:  ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Seven ............................................................................................................... 
// State

 strLine = "State: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strStateProvince;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Eight ............................................................................................................... 
// CountyID

 strLine = "CountyID: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strCountyID;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Nine ............................................................................................................... 
// Country

 strLine = "Country: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strCountry;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Ten ............................................................................................................... 
// TAR Code Taxing authority

 strLine = "TAR code: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strTARCode;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Eleven ............................................................................................................ 
// ZIP Code 

 strLine = "ZIP code: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strPostalZipCode;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Tweleve ........................................................................................................... 
// Building 

 strLine = "Building: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strBuilding;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Thirteen .......................................................................................................... 
// Floor 

 strLine = "Floor: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strFloor;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Fourteen .......................................................................................................... 
// Unit Number 

 strLine = "Unit Num: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strUnitNum;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Fifteen ........................................................................................................... 
// Unit Type 

 strLine = "Unit Type: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strUnitType;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Sixteen ........................................................................................................... 
// Location Desc 

 strLine = "Loc Desc: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strLocationDescription;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Set Line Seventeen ......................................................................................................... 
// Landmark address 

 strLine = "Landmark Addr: ";
 strLine += I3XMLData.LocationInfo.StreetAddress.strLandmarkAddress;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

// ......................................................... Set Line Eighteen .......................................................................................................... 
// Geolocation Lat/Long 

 strLine = "Lat/Long: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strLatitude;
 strLine += strSPACE;
 strLine += I3XMLData.LocationInfo.GeoLocation.strLongitude;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = "Elevation: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strElevation;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = "Datum: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strDatum;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = "Heading: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strHeading;
 strLine += strSPACE;
 strLine += "Speed: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strSpeed;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

// ......................................................... Set Line Nineteen .......................................................................................................... 
// Geolocation Uncertanty / Confidence

 strLine = "Uncertainty: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strUncertainty;
 strLine += strSPACE;
 strLine += "Confidence: ";
 strLine += I3XMLData.LocationInfo.GeoLocation.strConfidence;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);





// ......................................................... Set Line Twenty ............................................................................................................ 
// CellSite/Sector

 strLine = "CellID: ";
 strLine += I3XMLData.LocationInfo.CellSite.strCellID;
 strLine += strSPACE;
 strLine += "Sector: ";
 strLine += I3XMLData.LocationInfo.CellSite.strSectorID;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

// ......................................................... Set Line Twentytwo to Twentyfive............................................................................................ 
// Agencies

 strLine = "ESN: ";
 strLine += I3XMLData.Agencies.strESN;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
 
 strLine = strBLANK;
 strLine = InsertString(strLine, I3XMLData.Agencies.Police.strName, 0, 29);
 strLine = InsertString(strLine, SevenDigitTelno(I3XMLData.Agencies.Police.strTN), 23, 8);
 strLine = InsertString(strLine, strCR, 31, 1);
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = strBLANK;
 strLine = InsertString(strLine, I3XMLData.Agencies.Fire.strName, 0, 29);
 strLine = InsertString(strLine, SevenDigitTelno(I3XMLData.Agencies.Fire.strTN), 23, 8);
 strLine = InsertString(strLine, strCR, 31, 1);
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strLine = strBLANK;
 strLine = InsertString(strLine, I3XMLData.Agencies.EMS.strName, 0, 29);
 strLine = InsertString(strLine, SevenDigitTelno(I3XMLData.Agencies.EMS.strTN), 23, 8);
 strLine = InsertString(strLine, strCR, 31, 1);
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);
// ......................................................... Variable ............................................................................................ 

 sz = I3XMLData.Agencies.vOtherAgencies.size();

 for (unsigned int i = 0; i<sz; i++) {

  strLine = strBLANK;
  strLine = InsertString(strLine, I3XMLData.Agencies.vOtherAgencies[i].strName, 0, 29);
  strLine = InsertString(strLine, SevenDigitTelno(I3XMLData.Agencies.vOtherAgencies[i].strTN), 23, 8);
  strLine = InsertString(strLine, strCR, 31, 1);
  strLine = RemoveTrailingSpaces(strLine);
  vLine.push_back(strLine);

 }

 strLine = "Additional Info: ";
 strLine += I3XMLData.Agencies.strAdditionalInfo;
 strLine += strCR;
 strLine = RemoveTrailingSpaces(strLine);
 vLine.push_back(strLine);

 strNGALIRecord.clear();

 sz = vLine.size();
 for (unsigned int i = 0; i<sz; i++) {
  strNGALIRecord += vLine[i];
 }


return strNGALIRecord;
}

bool ALI_I3_Data::fConvertServiceListToALIbody() {
 int		index1, index2, index3;
 ALI_I3_Agency  Agency;
 size_t         sz; 
 extern bool 	boolUSE_URI_LEGACY_ALI;

 sem_wait(&this->ServiceURNList.sem_tMutexVServiceURN); //-------------------------------------------------------------------------------------------
 sz = this->ServiceURNList.vServiceURN.size();

 index1 = this->ServiceURNList.PoliceIndex();
 if (index1 >= 0) {
  //cout << "found police" << endl;
  this->I3XMLData.Agencies.Police.strName = this->ServiceURNList.vServiceURN[index1].strDisplayName;
  if (boolUSE_URI_LEGACY_ALI) {this->I3XMLData.Agencies.Police.strTN = this->ServiceURNList.vServiceURN[index1].strServiceURI;}
  else                        {this->I3XMLData.Agencies.Police.strTN = this->ServiceURNList.vServiceURN[index1].strServiceNumber;} 
 }

 index2 = this->ServiceURNList.FireIndex();
 if (index2 >= 0) {
  //cout << "found fire" << endl;
  this->I3XMLData.Agencies.Fire.strName = this->ServiceURNList.vServiceURN[index2].strDisplayName;
  if (boolUSE_URI_LEGACY_ALI) {this->I3XMLData.Agencies.Fire.strTN = this->ServiceURNList.vServiceURN[index2].strServiceURI;}
  else                        {this->I3XMLData.Agencies.Fire.strTN = this->ServiceURNList.vServiceURN[index2].strServiceNumber;} 
 }

 index3 = this->ServiceURNList.EMSIndex();
 if (index3 >= 0) {
  //cout << "found ems" << endl;
  this->I3XMLData.Agencies.EMS.strName = this->ServiceURNList.vServiceURN[index3].strDisplayName;
  if (boolUSE_URI_LEGACY_ALI) {this->I3XMLData.Agencies.EMS.strTN = this->ServiceURNList.vServiceURN[index3].strServiceURI;}
  else                        {this->I3XMLData.Agencies.EMS.strTN = this->ServiceURNList.vServiceURN[index3].strServiceNumber;} 
 }

 this->I3XMLData.Agencies.vOtherAgencies.clear();

 for (int i = 0; i < (int) sz; i++) {
  if (i==index1) {continue;}
  if (i==index2) {continue;}
  if (i==index3) {continue;}
  //cout << "found other agency" << endl;
  Agency.fClear();
  Agency.strName = this->ServiceURNList.vServiceURN[i].strDisplayName;
  if (boolUSE_URI_LEGACY_ALI) {Agency.strTN = this->ServiceURNList.vServiceURN[i].strServiceURI;}
  else                        {Agency.strTN = this->ServiceURNList.vServiceURN[i].strServiceNumber;} 
  Agency.strKind = this->ServiceURNList.vServiceURN[i].strURN;
   this->I3XMLData.Agencies.vOtherAgencies.push_back(Agency);
 }
 sem_post(&this->ServiceURNList.sem_tMutexVServiceURN); //-----------------------------------------------------------------------------------------------
 return true;
}

string  ALI_I3_Data::fCreateLegacyALIrecord() {
 
// ALI_E2_XML_Data    objXMLData;
// Port_Data          objPortData;
// MessageClass       objMessage;
size_t                	sz; 
extern string		ClassofServiceFromCode(string strInput);

 string strLineOne,strLineTwo,strLineThree, strLineFour,strLineFive, strLineSix, strLineSeven = "";;
 string strLineEight, strLineNine, strLineTen, strLineEleven ="";
 string strLineTwelve,strLineThirteen,strLineFourteen, strLineFifteen, strLineSixteen ="", strLineSeventeen ="";
 string strBLANK, strCR, strSPACE, strDASH, strPPOUND = "";
 string strLPAREN, strRPAREN, strFWDSLASH;
 string strStreetNameCombined;
 string strTemp;

 string strModState, strModPrefixDirectional, strModPostDirectional, strModStreetSuffix;


 strBLANK.assign(32, ' ');
 strCR    += charCR;
 strSPACE += charSPACE; 
 strDASH     = "-";
 strPPOUND   = "P#";
 strLPAREN   = "(";
 strRPAREN   = ")";
 strFWDSLASH = "/";

 //objPortData.fLoadPortData(ALI, intPortNum, intSideAorB);
 //objXMLData.fLoadLocationData((const char*) E2Data.chLocationDescription, objPortData);

  if (!this->I3XMLData.SourceInfo.strALIRetrievalGMT.empty()) 
  {
   strTimeStamp = this->fGMTtoLocalTimeStamp(this->I3XMLData.SourceInfo.strALIRetrievalGMT);
  }



 // Check GeneralUses
 sz = I3XMLData.SourceInfo.vGeneralUses.size();

 strLineOne = "   ";
 for (unsigned int i = 0; i < sz; i++)
  {
   if      (I3XMLData.SourceInfo.vGeneralUses[i].strID == "type")     {strLineOne.replace(0,1, I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse);}
   else if (I3XMLData.SourceInfo.vGeneralUses[i].strID == "index")    {strLineOne.replace(1,2, I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse);}
   else if (I3XMLData.SourceInfo.vGeneralUses[i].strID == "norecord") {
                                                                       if (I3XMLData.CallInfo.strCallbackNum.length() >= 10)
                                                                        {
                                                                         strLineOne += I3XMLData.CallInfo.strCallbackNum.substr(0,3);
                                                                         strLineOne += strDASH;
                                                                         strLineOne += I3XMLData.CallInfo.strCallbackNum.substr(3,3);
                                                                         strLineOne += strDASH;
                                                                         strLineOne += I3XMLData.CallInfo.strCallbackNum.substr(6,4);
                                                                        }
                                                                       strLineOne += strSPACE;
                                                                       strLineOne += I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse;
                                                                       strALI30WRecord = strLineOne; return strLineOne;
                                                                      } 
  }
 if (!sz) {strLineOne = "200";}

 //Modify Piflo entries to MSAG Format
 strModState             = StateAbbreviation(I3XMLData.LocationInfo.StreetAddress.strStateProvince);
 strModPrefixDirectional = CardinalHeading(I3XMLData.LocationInfo.StreetAddress.strPrefixDirectional);
 strModPostDirectional   = CardinalHeading(I3XMLData.LocationInfo.StreetAddress.strPostDirectional); 
 strModStreetSuffix      = PostalStreetSuffix(I3XMLData.LocationInfo.StreetAddress.strStreetSuffix);



 strLineOne += charCR;
 
 if (I3XMLData.CallInfo.strCallbackNum.length() >= 10) {
   strLineTwo = "(";
   strLineTwo += I3XMLData.CallInfo.strCallbackNum.substr(0,3);
   strLineTwo += ") ";
   strLineTwo += I3XMLData.CallInfo.strCallbackNum.substr(3,3);;
   strLineTwo += strDASH;
   strLineTwo += I3XMLData.CallInfo.strCallbackNum.substr(6,4);
 }
 else {
 strLineTwo = "(   )    -    ";
 }
 strLineTwo += strSPACE;

 if (!I3XMLData.CallInfo.strClassOfServiceCode.empty()) {
  strLineTwo += ClassofServiceFromCode(I3XMLData.CallInfo.strClassOfServiceCode);
 }
 else if (I3XMLData.CallInfo.strClassOfService.empty()) {
    strLineTwo += "None"; 
 }
 else {
  strLineTwo += I3XMLData.CallInfo.strClassOfService;
 }
 strLineTwo += strSPACE;
 strLineTwo += strTimeStamp;
 strLineTwo += strCR;
 

 strLineThree = strBLANK;
 strLineThree = InsertString(strLineThree, I3XMLData.CallInfo.strCustomerName, 0, 29);
 strLineThree = InsertString(strLineThree, strCR, 29, 1);
 strLineThree = RemoveTrailingSpaces(strLineThree);

 strLineFour = strBLANK ;
 strLineFour = InsertString(strLineFour, I3XMLData.LocationInfo.StreetAddress.strHouseNum, 0, 10);                   // right justify this field ....
 strLineFour = InsertString(strLineFour, strSPACE, 10, 1);
 strLineFour = InsertString(strLineFour, I3XMLData.LocationInfo.StreetAddress.strHouseNumSuffix, 11, 4);
 strLineFour = InsertString(strLineFour, strSPACE, 15, 1);
 strLineFour = InsertString(strLineFour, strSPACE, 16, 1);
 strLineFour = InsertString(strLineFour, strPPOUND, 17, 2);
 if (this->strPANI.length()) {
  strLineFour = InsertString(strLineFour, strPANI, 19, 10);
 } else {
  strLineFour = InsertString(strLineFour, fBidId(), 19, 12);
 }
 strLineFour = InsertString(strLineFour, strCR, 31, 1);
 strLineFour = RemoveTrailingSpaces(strLineFour);

 strLineFive = strBLANK ;
 strLineFive = InsertString(strLineFive, strModPrefixDirectional, 0, 2);
 strLineFive = InsertString(strLineFive, strSPACE, 2, 1);
 
 strStreetNameCombined = RemoveTrailingSpaces(I3XMLData.LocationInfo.StreetAddress.strStreetName);
 strStreetNameCombined = RemoveLeadingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += strModStreetSuffix;
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += strModPostDirectional;
 
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);
 

 strLineFive = InsertString(strLineFive, strStreetNameCombined, 3, 28);
 strLineFive = InsertString(strLineFive, strCR, 31, 1);
 strLineFive = RemoveTrailingSpaces(strLineFive);


 strLineSix = strBLANK ;

 if (strStreetNameCombined.length() > 28)
  {
   strLineSix = InsertString(strLineSix, strStreetNameCombined.substr(28,string::npos), 0, 30);
   strLineSix = InsertString(strLineSix, strCR, 31, 1);
  }


 strLineSeven = strBLANK ;
 strLineSeven = InsertString(strLineSeven, I3XMLData.LocationInfo.StreetAddress.strLocationDescription, 0, 21);
 strLineSeven = InsertString(strLineSeven, strSPACE, 21, 1);
 strLineSeven = InsertString(strLineSeven, I3XMLData.NetworkInfo.strPSAPID, 22, 3); //PS#
 strLineSeven = InsertString(strLineSeven, strSPACE, 25, 1);
 strLineSeven = InsertString(strLineSeven, I3XMLData.Agencies.strESN, 26, 5); 
 strLineSeven = InsertString(strLineSeven, strCR, 31, 1);
 strLineSeven = RemoveTrailingSpaces(strLineSeven);

 strLineEight = strBLANK ;
 strLineEight = InsertString(strLineEight, strModState, 0, 2);
 strLineEight = InsertString(strLineEight, strSPACE, 2, 1);
 strLineEight = InsertString(strLineEight, I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity, 3, 28);
 strLineEight = InsertString(strLineEight, strCR, 31, 1);
 strLineEight = RemoveTrailingSpaces(strLineEight);
 
 strLineNine  = strBLANK;
 //OTC Field 0-18
 strLineNine  = InsertString(strLineNine, "Tel=", 18, 4);
 strLineNine  = InsertString(strLineNine, I3XMLData.SourceInfo.AccessProvider.strName, 22, 5);
 strLineNine  = InsertString(strLineNine, strCR, 27, 1);
 strLineNine = RemoveTrailingSpaces(strLineNine);

 strLineTen  = strBLANK;
 strLineTen = InsertString(strLineTen, I3XMLData.LocationInfo.GeoLocation.Latitude(), 0, 11);
 strLineTen = InsertString(strLineTen, strSPACE, 11, 1);
 strLineTen = InsertString(strLineTen, I3XMLData.LocationInfo.GeoLocation.Longitude(), 12, 11);
 strLineTen = InsertString(strLineTen, strSPACE, 23, 1);
 strLineTen = InsertString(strLineTen, I3XMLData.LocationInfo.GeoLocation.strUncertainty, 24, 7);
 strLineTen = InsertString(strLineTen, strCR, 31, 1);
 strLineTen = RemoveTrailingSpaces(strLineTen);

 strLineEleven = strBLANK;
 strLineEleven = InsertString(strLineEleven, I3XMLData.LocationInfo.GeoLocation.strConfidence, 0, 7);
 strLineEleven = InsertString(strLineEleven, strCR, 31, 1);

 strLineTwelve = strBLANK;
 strLineTwelve = InsertString(strLineTwelve, "CELLID=", 0, 7);
 strLineTwelve = InsertString(strLineTwelve, I3XMLData.LocationInfo.CellSite.strCellID, 7, 6);
 strLineTwelve = InsertString(strLineTwelve, strSPACE, 13, 1);
 strLineTwelve = InsertString(strLineTwelve, "SECTOR=", 14, 7);
 strLineTwelve = InsertString(strLineTwelve, I3XMLData.LocationInfo.CellSite.strSectorID, 21, 2);
 strLineTwelve = InsertString(strLineTwelve, strCR, 23, 1);
 strLineTwelve = RemoveTrailingSpaces(strLineTwelve);
 
 strLineThirteen = strBLANK;

 strLineFourteen = strBLANK;
// strLineFourteen = InsertString(strLineFourteen, "LAW=", 0, 4); 
 strLineFourteen = InsertString(strLineFourteen, I3XMLData.Agencies.Police.strName, 0, 29);
 if ((I3XMLData.Agencies.Police.strName.length() < 24) && (I3XMLData.Agencies.Police.fTNnotZero())){
   strLineFourteen = InsertString(strLineFourteen, SevenDigitTelno(I3XMLData.Agencies.Police.strTN), 23, 8);
 }

 strLineFourteen = InsertString(strLineFourteen, strCR, 31, 1);
 strLineFourteen = RemoveTrailingSpaces(strLineFourteen);


 strLineFifteen = strBLANK;
// strLineFifteen = InsertString(strLineFifteen, "FIRE=", 0, 5); 
 strLineFifteen = InsertString(strLineFifteen, I3XMLData.Agencies.Fire.strName, 0, 29);
 if ((I3XMLData.Agencies.Police.strName.length() < 24) && (I3XMLData.Agencies.Fire.fTNnotZero()))
  {
   strLineFifteen = InsertString(strLineFifteen, SevenDigitTelno(I3XMLData.Agencies.Fire.strTN), 23, 8);
  }
 strLineFifteen = InsertString(strLineFifteen, strCR, 31, 1);
 strLineFifteen = RemoveTrailingSpaces(strLineFifteen);

 strLineSixteen = strBLANK;
// strLineSixteen = InsertString(strLineSixteen, "EMS=", 0, 4); 
 strLineSixteen = InsertString(strLineSixteen, I3XMLData.Agencies.EMS.strName, 0, 29);
 if ((I3XMLData.Agencies.Police.strName.length() < 24) && (I3XMLData.Agencies.EMS.fTNnotZero()))
  {
   strLineSixteen = InsertString(strLineSixteen, SevenDigitTelno(I3XMLData.Agencies.EMS.strTN), 23, 8);
  }
 strLineSixteen = InsertString(strLineSixteen, strCR, 31, 1);
 strLineSixteen = RemoveTrailingSpaces(strLineSixteen);

 strTemp = I3XMLData.CallInfo.strSpecialMessage;
 strTemp = FindandReplace(strTemp, "\n", "");
 strLineSeventeen = strBLANK;
 strLineSeventeen = InsertString(strLineSeventeen, strTemp, 0, 29);
 strLineSeventeen = InsertString(strLineSeventeen, strCR, 31, 1);
 strLineSeventeen = RemoveTrailingSpaces(strLineSeventeen);

 strALI30WRecord.clear();

 strALI30WRecord +=strLineOne;
 strALI30WRecord +=strLineTwo;
 strALI30WRecord +=strLineThree;
 strALI30WRecord +=strLineFour;
 strALI30WRecord +=strLineFive;
 strALI30WRecord +=strLineSix;
 strALI30WRecord +=strLineSeven;
 strALI30WRecord +=strLineEight;
 strALI30WRecord +=strLineNine;
 strALI30WRecord +=strLineTen;
 strALI30WRecord +=strLineEleven;
 strALI30WRecord +=strLineTwelve;
 strALI30WRecord +=strLineThirteen;
 strALI30WRecord +=strLineFourteen;
 strALI30WRecord +=strLineFifteen;
 strALI30WRecord +=strLineSixteen;
 strALI30WRecord +=strLineSeventeen;
 



 return strALI30WRecord;
}

string  ALI_I3_Data::fCreateLegacyALI30Xrecord() {
 
// ALI_E2_XML_Data    objXMLData;
// Port_Data          objPortData;
// MessageClass       objMessage;
size_t                	sz; 
extern string		ClassofServiceFromCode(string strInput);

 string strLineOne,strLineTwo,strLineThree, strLineFour,strLineFive, strLineSix, strLineSeven = "";;
 string strLineEight, strLineNine, strLineTen, strLineEleven ="";
 string strLineTwelve,strLineThirteen,strLineFourteen, strLineFifteen, strLineSixteen ="", strLineSeventeen ="";
 string strBLANK, strCR, strSPACE, strDASH, strPPOUND = "";
 string strLPAREN, strRPAREN, strFWDSLASH;
 string strStreetNameCombined;
 string strTemp;
 string strNPA, strNXX, strXXXX;
 string strModState, strModPrefixDirectional, strModPostDirectional, strModStreetSuffix;
 

 strBLANK.assign(32, ' ');
 strCR    += charCR;
 strSPACE += charSPACE; 
 strDASH     = "-";
 strPPOUND   = "P#(";
 strLPAREN   = "(";
 strRPAREN   = ")";
 strFWDSLASH = "/";

 //objPortData.fLoadPortData(ALI, intPortNum, intSideAorB);
 //objXMLData.fLoadLocationData((const char*) E2Data.chLocationDescription, objPortData);

  if (!this->I3XMLData.SourceInfo.strALIRetrievalGMT.empty()) 
  {
   strTimeStamp = this->fGMTtoLocalTimeStamp(this->I3XMLData.SourceInfo.strALIRetrievalGMT);
  }



 // Check GeneralUses
 sz = I3XMLData.SourceInfo.vGeneralUses.size();

 strLineOne = "   ";
 for (unsigned int i = 0; i < sz; i++)
  {
   if      (I3XMLData.SourceInfo.vGeneralUses[i].strID == "type")     {strLineOne.replace(0,1, I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse);}
   else if (I3XMLData.SourceInfo.vGeneralUses[i].strID == "index")    {strLineOne.replace(1,2, I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse);}
   else if (I3XMLData.SourceInfo.vGeneralUses[i].strID == "norecord") {
                                                                       if (I3XMLData.CallInfo.strCallbackNum.length() >= 10)
                                                                        {
                                                                         strLineOne += I3XMLData.CallInfo.strCallbackNum.substr(0,3);
                                                                         strLineOne += strDASH;
                                                                         strLineOne += I3XMLData.CallInfo.strCallbackNum.substr(3,3);
                                                                         strLineOne += strDASH;
                                                                         strLineOne += I3XMLData.CallInfo.strCallbackNum.substr(6,4);
                                                                        }
                                                                       strLineOne += strSPACE;
                                                                       strLineOne += I3XMLData.SourceInfo.vGeneralUses[i].strGeneralUse;
                                                                       strALI30XRecord = strLineOne; return strLineOne;
                                                                      } 
  }
 if (!sz) {strLineOne = "200";}

 //Modify Piflo entries to MSAG Format
 strModState             = StateAbbreviation(I3XMLData.LocationInfo.StreetAddress.strStateProvince);
 strModPrefixDirectional = CardinalHeading(I3XMLData.LocationInfo.StreetAddress.strPrefixDirectional);
 strModPostDirectional   = CardinalHeading(I3XMLData.LocationInfo.StreetAddress.strPostDirectional); 
 strModStreetSuffix      = PostalStreetSuffix(I3XMLData.LocationInfo.StreetAddress.strStreetSuffix);



 strLineOne += charCR;
 
 if (I3XMLData.CallInfo.strCallbackNum.length() >= 10) {
   strLineTwo = "(";
   strLineTwo += I3XMLData.CallInfo.strCallbackNum.substr(0,3);
   strLineTwo += ") ";
   strLineTwo += I3XMLData.CallInfo.strCallbackNum.substr(3,3);;
   strLineTwo += strDASH;
   strLineTwo += I3XMLData.CallInfo.strCallbackNum.substr(6,4);
 }
 else {
 strLineTwo = "(   )    -    ";
 }
 strLineTwo += strSPACE;

 if (!I3XMLData.CallInfo.strClassOfServiceCode.empty()) {
  strLineTwo += ClassofServiceFromCode(I3XMLData.CallInfo.strClassOfServiceCode);
 }
 else if (I3XMLData.CallInfo.strClassOfService.empty()) {
    strLineTwo += "None"; 
 }
 else {
  strLineTwo += I3XMLData.CallInfo.strClassOfService;
 }
 strLineTwo += strSPACE;
 strLineTwo += strTimeStamp;
 strLineTwo += strCR;
 

 strLineThree = strBLANK;
 strLineThree = InsertString(strLineThree, I3XMLData.CallInfo.strCustomerName, 0, 29);
 strLineThree = InsertString(strLineThree, strCR, 29, 1);
 strLineThree = RemoveTrailingSpaces(strLineThree);

 strLineFour = strBLANK ;
 strLineFour = InsertString(strLineFour, I3XMLData.LocationInfo.StreetAddress.strHouseNum, 0, 8);                   // right justify this field ....
 strLineFour = InsertString(strLineFour, strSPACE, 8, 1);
 strLineFour = InsertString(strLineFour, I3XMLData.LocationInfo.StreetAddress.strHouseNumSuffix, 9, 4);
 strLineFour = InsertString(strLineFour, strCR, 13, 1);
 strLineFour = RemoveTrailingSpaces(strLineFour);

/*
 strLineFour = InsertString(strLineFour, strSPACE, 15, 1);
 strLineFour = InsertString(strLineFour, strSPACE, 16, 1);
 strLineFour = InsertString(strLineFour, strPPOUND, 17, 2);
 if (this->strPANI.length()) {
  strLineFour = InsertString(strLineFour, strPANI, 19, 10);
 } else {
  strLineFour = InsertString(strLineFour, fBidId(), 19, 12);
 }
 strLineFour = InsertString(strLineFour, strCR, 31, 1);
 strLineFour = RemoveTrailingSpaces(strLineFour);
*/

 strLineFive = strBLANK ;
 strLineFive = InsertString(strLineFive, strModPrefixDirectional, 0, 2);
 strLineFive = InsertString(strLineFive, strSPACE, 2, 1);
 
 strStreetNameCombined = RemoveTrailingSpaces(I3XMLData.LocationInfo.StreetAddress.strStreetName);
 strStreetNameCombined = RemoveLeadingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += strModStreetSuffix;
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += strModPostDirectional;
 
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);
 

 strLineFive = InsertString(strLineFive, strStreetNameCombined, 3, 28);
 strLineFive = InsertString(strLineFive, strCR, 31, 1);
 strLineFive = RemoveTrailingSpaces(strLineFive);

/*
 strLineSix = strBLANK ;


 if (strStreetNameCombined.length() > 28)
  {
   strLineSix = InsertString(strLineSix, strStreetNameCombined.substr(28,string::npos), 0, 30);
   strLineSix = InsertString(strLineSix, strCR, 31, 1);
  }
*/





 strLineSix = strBLANK ;
 strLineSix = InsertString(strLineSix, I3XMLData.LocationInfo.StreetAddress.strLocationDescription, 0, 31);
 strLineSix = InsertString(strLineSix, strCR, 31, 1);
 strLineSix = RemoveTrailingSpaces(strLineSix);

/*
 strLineSix = InsertString(strLineSix, strSPACE, 21, 1);
 strLineSix = InsertString(strLineSix, I3XMLData.NetworkInfo.strPSAPID, 22, 3); //PS#
 strLineSix = InsertString(strLineSix, strSPACE, 25, 1);
 strLineSix = InsertString(strLineSix, I3XMLData.Agencies.strESN, 26, 5); 
 strLineSix = InsertString(strLineSix, strCR, 31, 1);
 strLineSix = RemoveTrailingSpaces(strLineSix);
*/
 strLineSeven = strBLANK ;
 strLineSeven = InsertString(strLineSeven, I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity, 0, 31);
 strLineSeven = InsertString(strLineSeven, strCR, 31, 1);
 strLineSeven = RemoveTrailingSpaces(strLineSeven);



 strLineEight = strBLANK ;
 strLineEight = InsertString(strLineEight, strModState, 0, 2);
 strLineEight = InsertString(strLineEight, strSPACE, 2, 1);
 strLineEight = InsertString(strLineEight, I3XMLData.LocationInfo.StreetAddress.strCountyID, 3, 12);
 strLineEight = InsertString(strLineEight, strSPACE, 15, 1);
 strLineEight = InsertString(strLineEight, strPPOUND, 16, 3);
 if (this->strPANI.length()==10) {
  strNPA  = this->strPANI.substr(0,3);
  strNXX  = this->strPANI.substr(3,3);
  strXXXX = this->strPANI.substr(6,4);
  strLineEight = InsertString(strLineEight, strNPA, 19, 3);
  strLineEight = InsertString(strLineEight, strNXX, 23, 3);
  strLineEight = InsertString(strLineEight, strXXXX, 27, 4);
 }
 strLineEight = InsertString(strLineEight, strRPAREN, 22, 1);
 strLineEight = InsertString(strLineEight, strDASH, 26, 1);
 strLineEight = InsertString(strLineEight, strCR, 31, 1);
 strLineEight = RemoveTrailingSpaces(strLineEight);

 strLineNine  = strBLANK;
 //OTC Field


 strLineTen = strBLANK;
 strLineTen = InsertString(strLineTen, "CO=", 0, 3);
 strTemp    = StripURNnenaCompanyID(I3XMLData.SourceInfo.AccessProvider.strAccessProviderID);
 strLineTen = InsertString(strLineTen, strTemp, 3, 5);
 strLineTen = InsertString(strLineTen, "ESN=", 17, 4);
 strLineTen = InsertString(strLineTen, I3XMLData.Agencies.strESN, 21, 4); 
 strLineTen = InsertString(strLineTen, strCR, 25, 1);
 strLineTen = RemoveTrailingSpaces(strLineTen);

 strLineEleven = strBLANK;
// strLineFourteen = InsertString(strLineFourteen, "LAW=", 0, 4); 
 strLineEleven = InsertString(strLineEleven, I3XMLData.Agencies.Police.strName, 0, 29);
 if ((I3XMLData.Agencies.Police.strName.length() < 24) && (I3XMLData.Agencies.Police.fTNnotZero())){
   strLineEleven = InsertString(strLineEleven, SevenDigitTelno(I3XMLData.Agencies.Police.strTN), 23, 8);
 }

 strLineEleven = InsertString(strLineEleven, strCR, 31, 1);
 strLineEleven = RemoveTrailingSpaces(strLineEleven);


 strLineTwelve = strBLANK;
// strLineFifteen = InsertString(strLineFifteen, "FIRE=", 0, 5); 
 strLineTwelve = InsertString(strLineTwelve, I3XMLData.Agencies.Fire.strName, 0, 29);
 if ((I3XMLData.Agencies.Police.strName.length() < 24) && (I3XMLData.Agencies.Fire.fTNnotZero()))  {
  strLineTwelve = InsertString(strLineTwelve, SevenDigitTelno(I3XMLData.Agencies.Fire.strTN), 23, 8);
 }
 strLineTwelve = InsertString(strLineTwelve, strCR, 31, 1);
 strLineTwelve = RemoveTrailingSpaces(strLineTwelve);

 strLineThirteen = strBLANK;
// strLineSixteen = InsertString(strLineSixteen, "EMS=", 0, 4); 
 strLineThirteen = InsertString(strLineThirteen, I3XMLData.Agencies.EMS.strName, 0, 29);
 if ((I3XMLData.Agencies.Police.strName.length() < 24) && (I3XMLData.Agencies.EMS.fTNnotZero()))  {
  strLineThirteen = InsertString(strLineThirteen, SevenDigitTelno(I3XMLData.Agencies.EMS.strTN), 23, 8);
 }
 strLineThirteen = InsertString(strLineThirteen, strCR, 31, 1);
 strLineThirteen = RemoveTrailingSpaces(strLineThirteen);


 strLineFourteen  = strBLANK;
 strLineFourteen = InsertString(strLineFourteen, strCR, 17, 1);
 strLineFourteen = RemoveTrailingSpaces(strLineFourteen);



 strLineFifteen  = strBLANK;
 strLineFifteen = InsertString(strLineFifteen, I3XMLData.LocationInfo.GeoLocation.Latitude(), 0, 11);
 strLineFifteen = InsertString(strLineFifteen, strSPACE, 11, 1);
 strLineFifteen = InsertString(strLineFifteen, I3XMLData.LocationInfo.GeoLocation.Longitude(), 12, 11);
 strLineFifteen = InsertString(strLineFifteen, strSPACE, 23, 1);
 strLineFifteen = InsertString(strLineFifteen, "CF=", 24, 3);
 strLineFifteen = InsertString(strLineFifteen, I3XMLData.LocationInfo.GeoLocation.strConfidence, 27, 3);
 strLineFifteen = InsertString(strLineFifteen, "%", 30, 1);
 strLineFifteen = InsertString(strLineFifteen, strCR, 31, 1);
 strLineFifteen = RemoveTrailingSpaces(strLineFifteen);

/*
 strLineFourteen = InsertString(strLineFourteen, I3XMLData.LocationInfo.GeoLocation.strUncertainty, 24, 7);
*/

 

 strALI30XRecord.clear();

 strALI30XRecord +=strLineOne;
 strALI30XRecord +=strLineTwo;
 strALI30XRecord +=strLineThree;
 strALI30XRecord +=strLineFour;
 strALI30XRecord +=strLineFive;
 strALI30XRecord +=strLineSix;
 strALI30XRecord +=strLineSeven;
 strALI30XRecord +=strLineEight;
 strALI30XRecord +=strLineNine;
 strALI30XRecord +=strLineTen;
 strALI30XRecord +=strLineEleven;
 strALI30XRecord +=strLineTwelve;
 strALI30XRecord +=strLineThirteen;
 strALI30XRecord +=strLineFourteen;
 strALI30XRecord +=strLineFifteen;

 



 return strALI30XRecord;
}


bool ALI_I3_Data::fLoadLocationData(string strInput, string* strError)
{
 DataPacketIn objData;
 
 boolByValue = false;

 if (strInput.empty()) {return false;}
 
 objData.stringDataIn = strInput;
 boolByValue = fLoadLocationData(objData, strError);

return boolByValue;
}

string RemoveXMLHeader(string strInput)
{
  size_t found;
  string strEnd   = "?>";
  string strReturn = strInput;

 
  found = strReturn.find(strEnd);
  if (found == string::npos)       {return strInput;}
 
  strReturn = strReturn.substr(found+2, string::npos);
  return strReturn;
 }




string extractPidfXMLData(string strInput)
 {
  size_t found;
  string strStart = "application/pidf+xml:";
  string strEnd   = "|:";
  string strReturn;

  found = strInput.find(strStart);
  if (found == string::npos)       {return "";}
  strReturn = strInput.substr(found + strStart.length(),string::npos);

  found = strReturn.find(strEnd);
 // if (found == string::npos)       {return "";}
 
  strReturn = strReturn.substr(0, found);
  return strReturn;
 }


void ALI_I3_Data::fLoadSimulationData(string strBidIdValue)
{
// CIDB Hardcoded Values fro DEMO
// County ID is FIPS
  
     if (strBidIdValue == "5033782911")
      {
       //cout << "Scenario 1" << endl;
       I3XMLData.CallInfo.strCallbackNum    = strBidIdValue;     
       I3XMLData.CallInfo.strClassOfService = "BUSN";
       I3XMLData.SourceInfo.AccessProvider.strName = "QWEST";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "AFRC Building";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "41047";
       I3XMLData.Agencies.Police.strName = "Salem PD 503-588-6245"; 
       I3XMLData.Agencies.Fire.strName = "Salem SFD 503-588-6297"; 
       I3XMLData.Agencies.EMS.strName = "Metro AMB 503-588-4312 "; 
      }
     else if  (strBidIdValue == "5412820337")
      {
       //cout << "Scenario 2" << endl;

       I3XMLData.CallInfo.strCallbackNum    = "5416354366";     
       I3XMLData.CallInfo.strClassOfService = "WPH2";
       I3XMLData.CallInfo.strCustomerName = "AT&T Mobility 800-331-0500";
       I3XMLData.SourceInfo.AccessProvider.strName = "ATT";
       I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity = "Medford";
       I3XMLData.LocationInfo.StreetAddress.strStateProvince = "OR";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "E of RR trks";
       I3XMLData.LocationInfo.StreetAddress.strHouseNum = "2222";
       I3XMLData.LocationInfo.StreetAddress.strStreetName = "Table Rock";
       I3XMLData.LocationInfo.StreetAddress.strStreetSuffix = "Rd";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "41029";
       I3XMLData.LocationInfo.CellSite.strCellID = "3XCF";
       I3XMLData.LocationInfo.CellSite.strSectorID = "SW";
       I3XMLData.Agencies.Police.strName = "MPD 541-774-2350"; 
       I3XMLData.Agencies.Fire.strName = "MFR 541-774-2306"; 
       I3XMLData.Agencies.EMS.strName = "JCEMS 541-774-5063";


      }
     else if (strBidIdValue == "5414431234")
      {
       //cout << "Scenario 3" << endl;

       I3XMLData.CallInfo.strCallbackNum    = "5417855871";
       I3XMLData.CallInfo.strClassOfService = "WPH2";     
       I3XMLData.CallInfo.strCustomerName = "SBA Towers II LLC 800-487-7483";
       I3XMLData.SourceInfo.AccessProvider.strName = "SBA";
       I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity = "Medford";
       I3XMLData.LocationInfo.StreetAddress.strStateProvince = "OR";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "S of Int Pecks Rd & Lakeway Dr";
       I3XMLData.LocationInfo.CellSite.strCellID = "456C";
       I3XMLData.LocationInfo.CellSite.strSectorID = "SE";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "41029";
       I3XMLData.Agencies.Police.strName = "MPD 541-774-2350"; 
       I3XMLData.Agencies.Fire.strName = "MFR 541-774-2306"; 
       I3XMLData.Agencies.EMS.strName = "JCEMS 541-774-5063";

      }
     else if (strBidIdValue == "A0-12-34-56-78-90")
      {
       //cout << "Scenario 4" << endl;

       I3XMLData.CallInfo.strCallbackNum    = "7408258888";     
       I3XMLData.CallInfo.strClassOfService = "WPH2";
       I3XMLData.CallInfo.strCustomerName = "Cingular Wrless 818-800-6000";
       I3XMLData.SourceInfo.AccessProvider.strName = "NCW";
       I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity = "Malta";
       I3XMLData.LocationInfo.StreetAddress.strStateProvince = "OH";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "";
       I3XMLData.LocationInfo.StreetAddress.strHouseNum = "";
       I3XMLData.LocationInfo.StreetAddress.strStreetName = "County Rd 4";
       I3XMLData.LocationInfo.StreetAddress.strStreetSuffix = "";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "39115";
       I3XMLData.LocationInfo.CellSite.strCellID = "27AW";
       I3XMLData.LocationInfo.CellSite.strSectorID = "NE";
       I3XMLData.Agencies.Police.strName = "MPD 740-962-3163"; 
       I3XMLData.Agencies.Fire.strName = "MMFR 740-962-2222"; 
       I3XMLData.Agencies.EMS.strName = "EMS 740-962-2222";

      }
     else if (strBidIdValue == "B0-12-34-56-78-90")
      {
       //cout << "Scenario 5" << endl;

       I3XMLData.CallInfo.strCallbackNum    = "9378258888";     
       I3XMLData.CallInfo.strClassOfService = "WPH2";
       I3XMLData.CallInfo.strCustomerName = "F&L Electronics 304-523-6069";
       I3XMLData.SourceInfo.AccessProvider.strName = "FLE";
       I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity = "McConnelsville";
       I3XMLData.LocationInfo.StreetAddress.strStateProvince = "OH";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "4.8 mi NNW";
       I3XMLData.LocationInfo.StreetAddress.strHouseNum = "";
       I3XMLData.LocationInfo.StreetAddress.strStreetName = "";
       I3XMLData.LocationInfo.StreetAddress.strStreetSuffix = "";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "39115";
       I3XMLData.LocationInfo.CellSite.strCellID = "27029";
       I3XMLData.LocationInfo.CellSite.strSectorID = "NW";
       I3XMLData.Agencies.Police.strName = "MPD 740-962-3163"; 
       I3XMLData.Agencies.Fire.strName = "MMFR 740-962-2222"; 
       I3XMLData.Agencies.EMS.strName = "EMS 740-962-2222";

      }
     else if (strBidIdValue == "C0-12-34-56-78-90")
      {
       //cout << "Scenario 6" << endl;

       I3XMLData.CallInfo.strCallbackNum    = "9378223204";     
       I3XMLData.CallInfo.strClassOfService = "WPH2";
       I3XMLData.CallInfo.strCustomerName = "SBA Towers II LLC 800-487-7483";
       I3XMLData.SourceInfo.AccessProvider.strName = "SBA";
       I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity = "Sidney";
       I3XMLData.LocationInfo.StreetAddress.strStateProvince = "OH";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "";
       I3XMLData.LocationInfo.StreetAddress.strHouseNum = "223";
       I3XMLData.LocationInfo.StreetAddress.strStreetName = "Walnut";
       I3XMLData.LocationInfo.StreetAddress.strStreetSuffix = "St";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "39149";
       I3XMLData.LocationInfo.CellSite.strCellID = "41029";
       I3XMLData.LocationInfo.CellSite.strSectorID = "SE";
       I3XMLData.Agencies.Police.strName = "SPD 937-498-2351"; 
       I3XMLData.Agencies.Fire.strName = "SFR 937-498-2346"; 
       I3XMLData.Agencies.EMS.strName = "AAMB 866-604-8307";

      }
     else if (strBidIdValue == "D0-12-34-56-78-90")
      {
      //cout << "Scenario 7" << endl;

       I3XMLData.CallInfo.strCallbackNum    = "9372814306";
       I3XMLData.CallInfo.strClassOfService = "WPH2";     
       I3XMLData.CallInfo.strCustomerName = "Crown Comm LLC 336-643-2524 ";
       I3XMLData.SourceInfo.AccessProvider.strName = "CCC";
       I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity = "Anna";
       I3XMLData.LocationInfo.StreetAddress.strStateProvince = "OH";
       I3XMLData.LocationInfo.StreetAddress.strLocationDescription = "";
       I3XMLData.LocationInfo.CellSite.strCellID = "456C";
       I3XMLData.LocationInfo.CellSite.strSectorID = "NW";
       I3XMLData.LocationInfo.StreetAddress.strHouseNum = "12555";
       I3XMLData.LocationInfo.StreetAddress.strStreetName = "Mera";
       I3XMLData.LocationInfo.StreetAddress.strStreetSuffix = "St";
       I3XMLData.LocationInfo.StreetAddress.strCountyID= "39149";
       I3XMLData.Agencies.Police.strName = "SPD 937-498-1111"; 
       I3XMLData.Agencies.Fire.strName = "SFR 937-498-2346"; 
       I3XMLData.Agencies.EMS.strName = "AAMB 866-604-8307"; 

      }




}
/*
//Copy Constructor
ALI_I3_CallInfo::ALI_I3_CallInfo(const ALI_I3_CallInfo *a) {
 strCallbackNum			= a->strCallbackNum;
 strCallingPartyNum		= a->strCallingPartyNum;
 strClassOfService		= a->strClassOfService;
 strClassOfServiceCode		= a->strClassOfServiceCode;
 strTypeOfService		= a->strTypeOfService;
 strTypeOfServiceCode		= a->strTypeOfServiceCode;
 strSourceOfService		= a->strSourceOfService;
 strMainTelNum			= a->strMainTelNum;
 strCustomerName		= a->strCustomerName;
 strCustomerCode		= a->strCustomerCode;
 strAttentionIndicator		= a->strAttentionIndicator;
 strAttentionIndicatorCode	= a->strAttentionIndicatorCode;
 strSpecialMessage		= a->strSpecialMessage;
 strAlsoRingsAtAddress		= a->strAlsoRingsAtAddress;
}

//assignment operator
ALI_I3_CallInfo& ALI_I3_CallInfo::operator=(const ALI_I3_CallInfo& a)    {
 if (&a == this ) {return *this;}
 strCallbackNum			= a.strCallbackNum;
 strCallingPartyNum		= a.strCallingPartyNum;
 strClassOfService		= a.strClassOfService;
 strClassOfServiceCode		= a.strClassOfServiceCode;
 strTypeOfService		= a.strTypeOfService;
 strTypeOfServiceCode		= a.strTypeOfServiceCode;
 strSourceOfService		= a.strSourceOfService;
 strMainTelNum			= a.strMainTelNum;
 strCustomerName		= a.strCustomerName;
 strCustomerCode		= a.strCustomerCode;
 strAttentionIndicator		= a.strAttentionIndicator;
 strAttentionIndicatorCode	= a.strAttentionIndicatorCode;
 strSpecialMessage		= a.strSpecialMessage;
 strAlsoRingsAtAddress		= a.strAlsoRingsAtAddress;
 return *this;
}
*/
void ALI_I3_CallInfo::fClear() {
 strCallbackNum.clear();strCallingPartyNum.clear();
 strClassOfService.clear();strClassOfServiceCode.clear();strTypeOfService.clear();strTypeOfServiceCode.clear();strSourceOfService.clear();
 strMainTelNum.clear();strCustomerName.clear();strCustomerCode.clear();strAttentionIndicator.clear();strAttentionIndicatorCode.clear();
 strSpecialMessage.clear();strAlsoRingsAtAddress.clear(); 
}

int ALI_I3_CallInfo::fLoadTypeofService(string strInput) {
string strCompare;
int    iReturnCode;

strCompare = RemoveLeadingSpaces(strInput);
strCompare = RemoveTrailingSpaces(strCompare);

if      (strCompare == "FX in 911 serving area")         		{ iReturnCode = 1;}
else if (strCompare == "FX outside of 911 serving area") 		{ iReturnCode = 2;}
else if (strCompare == "Non-Published")                  		{ iReturnCode = 3;}
else if (strCompare == "Non-Published FX in 911 serving area")		{ iReturnCode = 4;}
else if (strCompare == "Non-Published FX outside of 911 serving area")	{ iReturnCode = 5;}
else if (strCompare == "Local Ported Number (LNP)")			{ iReturnCode = 6;}
else if (strCompare == "Local Ported Number")				{ iReturnCode = 6;}
else if (strCompare == "Interim Ported Number")				{ iReturnCode = 7;}
else if (strCompare == "PS/ALI Published")				{ iReturnCode = 8;}
else if (strCompare == "PSALI Published")				{ iReturnCode = 8;}
else if (strCompare == "PS/ALI Non-Published")				{ iReturnCode = 9;}
else if (strCompare == "PSALI Non-Published")				{ iReturnCode = 9;}
else if (strCompare == "Not FX nor Non-Published")			{ iReturnCode = 0;}
else if (strCompare == "NotFX nor Non-Published")			{ iReturnCode = 0;}
else if (strCompare == "NotFX or Non-Published")			{ iReturnCode = 0;}
else {
 SendCodingError("ALI_I3_CallInfo::fLoadTypeofServiceCode() Unable to decode -> "+ strCompare); 
 iReturnCode = 0;
}
this->strTypeOfServiceCode = int2str(iReturnCode);
this->strTypeOfService     = strCompare;
return iReturnCode;
}


string ClassofServiceFromCode(string strClassOfServiceCode) {

 if (strClassOfServiceCode.length() > 1) {return "";}
 if (strClassOfServiceCode.empty())      {return "";}

 if      (strClassOfServiceCode == "1")       {return "RESD";}
 else if (strClassOfServiceCode == "2")       {return "BUSN";}
 else if (strClassOfServiceCode == "3")       {return "RPBX";}
 else if (strClassOfServiceCode == "4")       {return "BPBX";}
 else if (strClassOfServiceCode == "5")       {return "CNTX";}
 else if (strClassOfServiceCode == "6")       {return "PAY1";}
 else if (strClassOfServiceCode == "7")       {return "PAY2";}
 else if (strClassOfServiceCode == "8")       {return "MOBL";}
 else if (strClassOfServiceCode == "9")       {return "ROPX";}
 else if (strClassOfServiceCode == "0")       {return "BOPX";}
 else if (strClassOfServiceCode == "A")       {return "COCT";}
 else if (strClassOfServiceCode == "B")       {return "    ";}
 else if (strClassOfServiceCode == "C")       {return "VRES";}
 else if (strClassOfServiceCode == "D")       {return "VBUS";}
 else if (strClassOfServiceCode == "E")       {return "VPAY";}
 else if (strClassOfServiceCode == "F")       {return "VMBL";}
 else if (strClassOfServiceCode == "J")       {return "VNOM";}
 else if (strClassOfServiceCode == "K")       {return "VENT";}
 else if (strClassOfServiceCode == "G")       {return "WPH1";}
 else if (strClassOfServiceCode == "H")       {return "WPH2";}
 else if (strClassOfServiceCode == "I")       {return "WPH1";}
 else if (strClassOfServiceCode == "V")       {return "VOIP";}
 else if (strClassOfServiceCode == "W")       {return "WRLS";}
 else if (strClassOfServiceCode == "T")       {return "TEXT";}
 return "";
}


bool ALI_I3_CallInfo::fLoadCallInfo(XMLNode  objXML)
{
/*
  string  strCallbackNum, strCallingPartyNum; 
  string  strClassOfService , strClassOfServiceCode, strTypeOfService , strTypeOfServiceCode , strSourceOfService;
  string  strMainTelNum, strCustomerName, strCustomerCode, strAttentionIndicator , strAttentionIndicatorCode;
  string  strSpecialMessage, strAlsoRingsAtAddress;
*/
 XMLNode CallbackNumNode, CallingPartyNumNode, ClassOfServiceNode, TypeOfServiceNode, SourceOfServiceNode, MainTelNumNode;
 XMLNode CustomerNameNode, CustomerCodeNode, AttentionIndicatorNode, SpecialMessageNode, AlsoRingsAtAddressNode;
 string strTemp;
 
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 CallbackNumNode = LoadChildNode(objXML, "CallbackNum");

// CallbackNumNode = objXML.getChildNode("CallbackNum");
 if (CallbackNumNode.isEmpty()){strCallbackNum.clear();}
 else  {
  if (CallbackNumNode.getText()) {
   strCallbackNum = CallbackNumNode.getText();
   strCallbackNum = StripPlusOne(strCallbackNum);
   strCallbackNum = FindandReplace(strCallbackNum, "+", "");
  } else {strCallbackNum.clear();}}  

 CallingPartyNumNode = LoadChildNode(objXML, "CallingPartyNum");
// CallingPartyNumNode = objXML.getChildNode("CallingPartyNum");
 if (CallingPartyNumNode.isEmpty()){strCallingPartyNum.clear();}
 else {
  if (CallingPartyNumNode.getText()) {
      strCallingPartyNum = CallingPartyNumNode.getText();
      // Need to QA the number i.e. 10 digit no +1 etc.....
      strCallingPartyNum = StripPlusOne(strCallingPartyNum);
      strCallingPartyNum = FindandReplace(strCallingPartyNum, "+", "");      
  } 
  else {
   strCallingPartyNum.clear();
  }
 } 

 ClassOfServiceNode = LoadChildNode(objXML, "ClassOfService");
 //ClassOfServiceNode = objXML.getChildNode("ClassOfService");
 if (ClassOfServiceNode.isEmpty()){strClassOfService.clear();strClassOfServiceCode.clear();}
 else                             
  {
   if (ClassOfServiceNode.getText())              {strClassOfService     = ClassOfServiceNode.getText();}            else {strClassOfService.clear();}
   if (ClassOfServiceNode.getAttribute("code"))   {strClassOfServiceCode = ClassOfServiceNode.getAttribute("code");} else {strClassOfServiceCode.clear();}
  }

 // check Class of Service Code

// //cout << "class of service code -> " << strClassOfServiceCode << endl;
 strTemp = ClassofServiceFromCode(strClassOfServiceCode);
 if (!strTemp.empty()) {
  strClassOfService = strTemp;
 }

 TypeOfServiceNode = LoadChildNode(objXML, "TypeOfService");
 //TypeOfServiceNode = objXML.getChildNode("TypeOfService");
 if (TypeOfServiceNode.isEmpty()){strTypeOfService.clear();strTypeOfServiceCode.clear();}
 else                             
  {
   if (TypeOfServiceNode.getText())               {strTypeOfService     = TypeOfServiceNode.getText();}            else {strTypeOfService.clear();}
   if (TypeOfServiceNode.getAttribute("code"))    {strTypeOfServiceCode = TypeOfServiceNode.getAttribute("code");} else {strTypeOfServiceCode.clear();}
  }
 SourceOfServiceNode = LoadChildNode(objXML, "SourceOfService");
// SourceOfServiceNode = objXML.getChildNode("SourceOfService");
 if (SourceOfServiceNode.isEmpty()){strSourceOfService.clear();}
 else                              {if (SourceOfServiceNode.getText()) {strSourceOfService = SourceOfServiceNode.getText();} else {strSourceOfService.clear();}}

 MainTelNumNode = LoadChildNode(objXML, "MainTelNum");
// MainTelNumNode = objXML.getChildNode("MainTelNum");
 if (MainTelNumNode.isEmpty()){strMainTelNum.clear();}
 else                         {if (MainTelNumNode.getText()) {strMainTelNum = MainTelNumNode.getText();} else {strMainTelNum.clear();}}  
 
 CustomerNameNode = LoadChildNode(objXML, "CustomerName");
 //CustomerNameNode = objXML.getChildNode("CustomerName");
 if (CustomerNameNode.isEmpty()){strCustomerName.clear();}
 else                           {if (CustomerNameNode.getText()) {strCustomerName = CustomerNameNode.getText();} else {strCustomerName.clear();}}  

 CustomerCodeNode = LoadChildNode(objXML, "CustomerCode");
// CustomerCodeNode = objXML.getChildNode("CustomerCode");
 if (CustomerCodeNode.isEmpty()){strCustomerCode.clear();}
 else                           {if (CustomerCodeNode.getText()) {strCustomerCode = CustomerCodeNode.getText();} else {strCustomerCode.clear();}}  

 AttentionIndicatorNode = LoadChildNode(objXML, "AttentionIndicator");
// AttentionIndicatorNode = objXML.getChildNode("AttentionIndicator");
 if (AttentionIndicatorNode.isEmpty()){strAttentionIndicator.clear();strAttentionIndicatorCode.clear();}
 else                             
  {
   if (AttentionIndicatorNode.getText())               {strAttentionIndicator     = AttentionIndicatorNode.getText();}            else {strAttentionIndicator.clear();}
   if (AttentionIndicatorNode.getAttribute("code"))    {strAttentionIndicatorCode = AttentionIndicatorNode.getAttribute("code");} else {strAttentionIndicatorCode.clear();}
  }

 AlsoRingsAtAddressNode = LoadChildNode(objXML, "AlsoRingsAtAddress");
// AlsoRingsAtAddressNode = objXML.getChildNode("AlsoRingsAtAddress");
 if (AlsoRingsAtAddressNode.isEmpty()){strAlsoRingsAtAddress.clear();}
 else                                 {if (AlsoRingsAtAddressNode.getText()) {strAlsoRingsAtAddress = AlsoRingsAtAddressNode.getText();} else {strAlsoRingsAtAddress.clear();}} 

 return true;
}





void ALI_I3_LocationInfo::fClear() {
 StreetAddress.fClear();
 GeoLocation.fClear();
 CellSite.fClear();
 strComment.clear();
 strMethod.clear(); 
}
bool ALI_I3_LocationInfo::fLoadLocationInfo(XMLNode  objXML)
{
/*
  ALI_I3_StreetAddress	StreetAddress; 
  ALI_I3_GeoLocation	GeoLocation;
  ALI_I3_CellSite	CellSite;
  string                strComment;
*/
 XMLNode StreetAddressNode, GeoLocationNode, CellSiteNode, CommentNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 StreetAddressNode = LoadChildNode(objXML, "StreetAddress");
// StreetAddressNode = objXML.getChildNode("StreetAddress");
 if (StreetAddressNode.isEmpty()) {SendCodingError("ALI_I3_LocationInfo::fLoadLocationInfo() I3 StreetAddress XML Tag Missing!"); return false;}   
 if(!StreetAddress.fLoadStreetAddress(StreetAddressNode))                                                                        {return false;}

 GeoLocationNode = LoadChildNode(objXML, "GeoLocation");
// GeoLocationNode = objXML.getChildNode("GeoLocation");
 if (GeoLocationNode.isEmpty()) {SendCodingError("ALI_I3_LocationInfo::fLoadLocationInfo() I3 GeoLocation XML Tag Missing!"); return false;}   
 if(!GeoLocation.fLoadGeoLocation(GeoLocationNode))                                                                          {return false;}

 CellSiteNode = LoadChildNode(objXML, "CellSite");
// CellSiteNode = objXML.getChildNode("CellSite");
 if (CellSiteNode.isEmpty()) {SendCodingError("ALI_I3_LocationInfo::fLoadLocationInfo() I3 CellSite XML Tag Missing!"); return false;}   
 if(!CellSite.fLoadCellSite(CellSiteNode))                                                                             {return false;}

 CommentNode = LoadChildNode(objXML, "Comment");
 //CommentNode = objXML.getChildNode("Comment");
 if (CommentNode.isEmpty())        {strComment.clear();} 
 else                              {if (CommentNode.getText()) {strComment = CommentNode.getText();} else {strComment.clear();}}


 return true;
}
void ALI_I3_CellSite::fClear()
{
 strCellID.clear();strSectorID.clear();strLocationDescription.clear();
}
bool ALI_I3_CellSite::fLoadCellSite(XMLNode  objXML)
{
 /*
  string  strCellID, strSectorID, strLocationDescription; 
*/
 XMLNode CellIDNode, SectorIDNode, LocationDescriptionNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 CellIDNode = LoadChildNode(objXML, "CellID");
// CellIDNode = objXML.getChildNode("CellID");
 if (CellIDNode.isEmpty())        {strCellID.clear();} 
 else                             {if (CellIDNode.getText()) {strCellID = CellIDNode.getText();} else {strCellID.clear();}}

 SectorIDNode = LoadChildNode(objXML, "SectorID");
// SectorIDNode = objXML.getChildNode("SectorID");
 if (SectorIDNode.isEmpty())        {strSectorID.clear();} 
 else                               {if (SectorIDNode.getText()) {strSectorID = SectorIDNode.getText();} else {strSectorID.clear();}}

 LocationDescriptionNode = LoadChildNode(objXML, "LocationDescription");
 //LocationDescriptionNode = objXML.getChildNode("LocationDescription");
 if (LocationDescriptionNode.isEmpty())        {strLocationDescription.clear();} 
 else                               {if (LocationDescriptionNode.getText()) {strLocationDescription = LocationDescriptionNode.getText();} else {strLocationDescription.clear();}}


 return true;
}
void ALI_I3_GeoLocation::fClear()
{
 strLatitude.clear();strLongitude.clear();
 strElevation.clear();strDatum.clear();strHeading.clear();strSpeed.clear();strPositionSource.clear();strPositionSourceCode.clear();
 strUncertainty.clear();strConfidence.clear();strDateStamp.clear();strLocationDescription.clear();
}
bool ALI_I3_GeoLocation::fLoadGeoLocation(XMLNode  objXML)
{
 /*
  string  strLatitude, strLongitude; 
  string  strElevation , strDatum , strHeading, strSpeed , strPositionSource , strPositionSourceCode;
  string  strUncertainty, strConfidence, strDateStamp, strLocationDescription ;
*/
 XMLNode LatitudeNode, LongitudeNode, ElevationNode, DatumNode, HeadingNode, SpeedNode, PositionSourceNode;
 XMLNode UncertaintyNode, ConfidenceNode, DateStampNode, LocationDescriptionNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 LatitudeNode = LoadChildNode(objXML, "Latitude");
 //LatitudeNode = objXML.getChildNode("Latitude");
 if (LatitudeNode.isEmpty())        {strLatitude.clear();} 
 else                               {if (LatitudeNode.getText()) {strLatitude = LatitudeNode.getText();} else {strLatitude.clear();}}

 LongitudeNode = LoadChildNode(objXML, "Longitude"); 
// LongitudeNode = objXML.getChildNode("Longitude");
 if (LongitudeNode.isEmpty())        {strLongitude.clear();} 
 else                                {if (LongitudeNode.getText()) {strLongitude = LongitudeNode.getText();} else {strLongitude.clear();}}
 
 ElevationNode = LoadChildNode(objXML, "Elevation"); 
// ElevationNode = objXML.getChildNode("Elevation");
 if (ElevationNode.isEmpty())        {strElevation.clear();} 
 else                                {if (ElevationNode.getText()) {strElevation = ElevationNode.getText();} else {strElevation.clear();}}

 DatumNode = LoadChildNode(objXML, "Datum"); 
 //DatumNode = objXML.getChildNode("Datum");
 if (DatumNode.isEmpty())        {strDatum.clear();} 
 else                            {if (DatumNode.getText()) {strDatum = DatumNode.getText();} else {strDatum.clear();}}

 HeadingNode = LoadChildNode(objXML, "Heading"); 
// HeadingNode = objXML.getChildNode("Heading");
 if (HeadingNode.isEmpty())        {strHeading.clear();} 
 else                              {if (HeadingNode.getText()) {strHeading = HeadingNode.getText();} else {strHeading.clear();}}

 SpeedNode = LoadChildNode(objXML, "Speed"); 
// SpeedNode = objXML.getChildNode("Speed");
 if (SpeedNode.isEmpty())        {strSpeed.clear();} 
 else                            {if (SpeedNode.getText()) {strSpeed = SpeedNode.getText();} else {strSpeed.clear();}}

 PositionSourceNode = LoadChildNode(objXML, "PositionSource"); 
 //PositionSourceNode = objXML.getChildNode("PositionSource");
 if (PositionSourceNode.isEmpty()){strPositionSource.clear();strPositionSourceCode.clear();}
 else                             
  {
   if (PositionSourceNode.getText())              {strPositionSource     = PositionSourceNode.getText();}            else {strPositionSource.clear();}
   if (PositionSourceNode.getAttribute("code"))   {strPositionSourceCode = PositionSourceNode.getAttribute("code");} else {strPositionSourceCode.clear();}
  }

 UncertaintyNode = LoadChildNode(objXML, "Uncertainty"); 
// UncertaintyNode = objXML.getChildNode("Uncertainty");
 if (UncertaintyNode.isEmpty())        {strUncertainty.clear();} 
 else                                  {if (UncertaintyNode.getText()) {strUncertainty = UncertaintyNode.getText();} else {strUncertainty.clear();}}

 ConfidenceNode = LoadChildNode(objXML, "Confidence"); 
// ConfidenceNode = objXML.getChildNode("Confidence");
 if (ConfidenceNode.isEmpty())        {strConfidence.clear();} 
 else                                 {if (ConfidenceNode.getText()) {strConfidence = ConfidenceNode.getText();} else {strConfidence.clear();}}

 DateStampNode = LoadChildNode(objXML, "DateStamp"); 
// DateStampNode = objXML.getChildNode("DateStamp");
 if (DateStampNode.isEmpty())        {strDateStamp.clear();} 
 else                                {if (DateStampNode.getText()) {strDateStamp = DateStampNode.getText();} else {strDateStamp.clear();}}

 LocationDescriptionNode = LoadChildNode(objXML, "LocationDescription"); 
 LocationDescriptionNode = objXML.getChildNode("LocationDescription");
 if (LocationDescriptionNode.isEmpty())        {strLocationDescription.clear();} 
 else                                          {if (LocationDescriptionNode.getText()) {strLocationDescription = LocationDescriptionNode.getText();} else {strLocationDescription.clear();}}



 return true;
}

void ALI_I3_StreetAddress::fClear() {
 strHouseNum.clear();strHouseNumSuffix.clear();strPrefixDirectional.clear();strStreetName.clear();strStreetSuffix.clear();strPostDirectional.clear();strTextualAddress.clear();
 strMSAGCommunity.clear();strPostalCommunity.clear();
 strStateProvince.clear();strCountyID.clear();strCountry.clear();strTARCode.clear();strPostalZipCode.clear();strBuilding.clear();strFloor.clear();strUnitNum.clear();strUnitType.clear();
 strLocationDescription.clear();strLandmarkAddress.clear();

}

void ALI_I3_StreetAddress::fLegacyHouseNum() {
//Max Length 10

 this->strHouseNum = RemoveLeadingSpaces(this->strHouseNum);
 this->strHouseNum = RemoveTrailingSpaces(this->strHouseNum);

 if (this->strHouseNum.length() > 10) {
  SendCodingError("ALI_I3_StreetAddress::fLegacyHouseNum() strHouseNum length > 10 .. Truncated!");
  strHouseNum.assign(strHouseNum, 0, 10); 
 }
 return;
}

void ALI_I3_StreetAddress::fLegacyHouseNumSuffix() {
/*
 Max Length 4
 House number extension (e.g. /2). The field should be spaced filled if no 
 suffix applies.
*/
 if (this->strHouseNumSuffix.empty() ) {
  this->strHouseNumSuffix = "    ";
  return;
 }
 this->strHouseNumSuffix = RemoveLeadingSpaces(this->strHouseNumSuffix);
 this->strHouseNumSuffix = RemoveTrailingSpaces(this->strHouseNumSuffix);


 if (this->strHouseNumSuffix.length() > 4) {
  SendCodingError("ALI_I3_StreetAddress::fLegacyHouseNumSuffix() strHouseNumSuffix length > 4 .. Truncated!");
  strHouseNumSuffix.assign(strHouseNumSuffix, 0, 4); 
 }
 return;
}

void ALI_I3_StreetAddress::fLegacyPrefixDirectional() {
/*
 Max Length 2
 Leading street direction prefix. The field should be space filled if no 
 prefix applies. Valid entries:
 N S E W
 NE NW SE SW
*/
 extern string CardinalHeading(string strInput);

 this->strPrefixDirectional = RemoveLeadingSpaces(this->strPrefixDirectional);
 this->strPrefixDirectional = RemoveTrailingSpaces(this->strPrefixDirectional);


 this->strPrefixDirectional = CardinalHeading(this->strPrefixDirectional);

 return;
}

void ALI_I3_StreetAddress::fLegacyStreetName() {
/*
 Max Length 40
 Valid service address of the Calling Number.
*/

 this->strStreetName = RemoveLeadingSpaces(this->strStreetName);
 this->strStreetName = RemoveTrailingSpaces(this->strStreetName);

 if (this->strStreetName.length() > 40) {
  SendCodingError("ALI_I3_StreetAddress::fLegacyStreetName() strStreetName length > 40 .. Truncated!");
  strStreetName.assign(strStreetName, 0, 40); 
 }
 return;
}

void ALI_I3_StreetAddress::fLegacyStreetSuffix() {
/*
 Max Length 4
 Valid street abbreviation, as defined by the U.S. Postal Service 
 Publication 28. (e.g. AVE)
*/

 extern string PostalStreetSuffix(string strInput);

 this->strStreetSuffix = RemoveLeadingSpaces(this->strStreetSuffix);
 this->strStreetSuffix = RemoveTrailingSpaces(this->strStreetSuffix);


 this->strStreetSuffix= PostalStreetSuffix(this->strStreetSuffix);

 return;
}


bool ALI_I3_StreetAddress::fLoadStreetAddress(XMLNode  objXML) {
/*
 string  strHouseNum, strHouseNumSuffix, strPrefixDirectional, strStreetName, strStreetSuffix, strPostDirectional, strTextualAddress, strMSAGCommunity, strPostalCommunity;
 string  strStateProvince, strCountyID, strCountry, strTARCode, strPostalZipCode, strBuilding, strFloor, strUnitNum, strUnitType, strLocationDescription, strLandmarkAddress;
*/

 XMLNode HouseNumNode, HouseNumSuffixNode, PrefixDirectionalNode, StreetNameNode, StreetSuffixNode, PostDirectionalNode;
 XMLNode TextualAddressNode, MSAGCommunityNode, PostalCommunityNode, StateProvinceNode, CountyIDNode, CountryNode;
 XMLNode TARCodeNode, PostalZipCodeNode, BuildingNode, FloorNode, UnitNumNode, UnitTypeNode, LocationDescriptionNode;
 XMLNode LandmarkAddressNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 HouseNumNode = LoadChildNode(objXML, "HouseNum");
 //HouseNumNode = objXML.getChildNode("HouseNum");
 if (HouseNumNode.isEmpty())        {strHouseNum.clear();} 
 else                               {if (HouseNumNode.getText()) {strHouseNum = HouseNumNode.getText();} else {strHouseNum.clear();}}

 HouseNumSuffixNode = LoadChildNode(objXML, "HouseNumSuffix");
 //HouseNumSuffixNode = objXML.getChildNode("HouseNumSuffix");
 if (HouseNumSuffixNode.isEmpty())  {strHouseNumSuffix.clear();} 
 else                               {if (HouseNumSuffixNode.getText()) {strHouseNumSuffix = HouseNumSuffixNode.getText();} else {strHouseNumSuffix.clear();}}

 PrefixDirectionalNode = LoadChildNode(objXML, "PrefixDirectional");
// PrefixDirectionalNode = objXML.getChildNode("PrefixDirectional");
 if (PrefixDirectionalNode.isEmpty())  {strPrefixDirectional.clear();} 
 else                                  {if (PrefixDirectionalNode.getText()) {strPrefixDirectional = PrefixDirectionalNode.getText();} else {strPrefixDirectional.clear();}}
 
 StreetNameNode = LoadChildNode(objXML, "StreetName");
// StreetNameNode = objXML.getChildNode("StreetName");
 if (StreetNameNode.isEmpty())  {strStreetName.clear();} 
 else                           {if (StreetNameNode.getText()) {strStreetName = StreetNameNode.getText();} else {strStreetName.clear();}}

 StreetSuffixNode = LoadChildNode(objXML, "StreetSuffix");
 //StreetSuffixNode = objXML.getChildNode("StreetSuffix");
 if (StreetSuffixNode.isEmpty())  {strStreetSuffix.clear();} 
 else                             {if (StreetSuffixNode.getText()) {strStreetSuffix = StreetSuffixNode.getText();} else {strStreetSuffix.clear();}}

 PostDirectionalNode = LoadChildNode(objXML, "PostDirectional");
// PostDirectionalNode = objXML.getChildNode("PostDirectional");
 if (PostDirectionalNode.isEmpty())  {strPostDirectional.clear();} 
 else                                {if (PostDirectionalNode.getText()) {strPostDirectional = PostDirectionalNode.getText();} else {strPostDirectional.clear();}}

 TextualAddressNode = LoadChildNode(objXML, "TextualAddress");
// TextualAddressNode = objXML.getChildNode("TextualAddress");
 if (TextualAddressNode.isEmpty())  {strTextualAddress.clear();} 
 else                               {if (TextualAddressNode.getText()) {strTextualAddress = TextualAddressNode.getText();} else {strTextualAddress.clear();}}

 MSAGCommunityNode = LoadChildNode(objXML, "MSAGCommunity");
// MSAGCommunityNode = objXML.getChildNode("MSAGCommunity");
 if (MSAGCommunityNode.isEmpty())  {strMSAGCommunity.clear(); cout << "Cleared MSAG Community a" << endl;} 
 else                              {if (MSAGCommunityNode.getText()) {strMSAGCommunity = MSAGCommunityNode.getText();} else {strMSAGCommunity.clear();cout << "Cleared MSAG Community b" << endl;}}

 PostalCommunityNode = LoadChildNode(objXML, "PostalCommunity");
 //PostalCommunityNode = objXML.getChildNode("PostalCommunity");
 if (PostalCommunityNode.isEmpty())  {strPostalCommunity.clear();} 
 else                                {if (PostalCommunityNode.getText()) {strPostalCommunity = PostalCommunityNode.getText();} else {strPostalCommunity.clear();}}

 StateProvinceNode = LoadChildNode(objXML, "StateProvince");
 //StateProvinceNode = objXML.getChildNode("StateProvince");
 if (StateProvinceNode.isEmpty())  {strStateProvince.clear();} 
 else                              {if (StateProvinceNode.getText()) {strStateProvince = StateProvinceNode.getText();} else {strStateProvince.clear();}}

 CountyIDNode = LoadChildNode(objXML, "CountyIDNode");
// CountyIDNode = objXML.getChildNode("CountyID");
 if (CountyIDNode.isEmpty())  {strCountyID.clear();} 
 else                         {if (CountyIDNode.getText()) {strCountyID = CountyIDNode.getText();} else {strCountyID.clear();}}

 CountryNode = LoadChildNode(objXML, "Country");
 //CountryNode = objXML.getChildNode("Country");
 if (CountryNode.isEmpty())  {strCountry.clear();} 
 else                        {if (CountryNode.getText()) {strCountry = CountryNode.getText();} else {strCountry.clear();}}

 TARCodeNode = LoadChildNode(objXML, "TARCode");
// TARCodeNode = objXML.getChildNode("TARCode");
 if (TARCodeNode.isEmpty())  {strTARCode.clear();} 
 else                        {if (TARCodeNode.getText()) {strTARCode = TARCodeNode.getText();} else {strTARCode.clear();}}

 PostalZipCodeNode = LoadChildNode(objXML, "PostalZipCode");
 //PostalZipCodeNode = objXML.getChildNode("PostalZipCode");
 if (PostalZipCodeNode.isEmpty())  {strPostalZipCode.clear();} 
 else                              {if (PostalZipCodeNode.getText()) {strPostalZipCode = PostalZipCodeNode.getText();} else {strPostalZipCode.clear();}}

 BuildingNode = LoadChildNode(objXML, "Building");
 //BuildingNode = objXML.getChildNode("Building");
 if (BuildingNode.isEmpty())  {strBuilding.clear();} 
 else                         {if (BuildingNode.getText()) {strBuilding = BuildingNode.getText();} else {strBuilding.clear();}}

 FloorNode = LoadChildNode(objXML, "Floor");
// FloorNode = objXML.getChildNode("Floor");
 if (FloorNode.isEmpty())  {strFloor.clear();} 
 else                      {if (FloorNode.getText()) {strFloor = FloorNode.getText();} else {strFloor.clear();}}

 UnitNumNode = LoadChildNode(objXML, "UnitNum");
 //UnitNumNode = objXML.getChildNode("UnitNum");
 if (UnitNumNode.isEmpty())  {strUnitNum.clear();} 
 else                        {if (UnitNumNode.getText()) {strUnitNum = UnitNumNode.getText();} else {strUnitNum.clear();}}

 UnitTypeNode = LoadChildNode(objXML, "UnitType");
 //UnitTypeNode = objXML.getChildNode("UnitType");
 if (UnitTypeNode.isEmpty())  {strUnitType.clear();} 
 else                         {if (UnitTypeNode.getText()) {strUnitType = UnitTypeNode.getText();} else {strUnitType.clear();}}

 LocationDescriptionNode = LoadChildNode(objXML, "LocationDescription");
// LocationDescriptionNode = objXML.getChildNode("LocationDescription");
 if (LocationDescriptionNode.isEmpty())  {strLocationDescription.clear();} 
 else                                    {if (LocationDescriptionNode.getText()) {strLocationDescription = LocationDescriptionNode.getText();} else {strLocationDescription.clear();}}

 LandmarkAddressNode = LoadChildNode(objXML, "LandmarkAddress");
// LandmarkAddressNode = objXML.getChildNode("LandmarkAddress");
 if (LandmarkAddressNode.isEmpty())  {strLandmarkAddress.clear();} 
 else                                {if (LandmarkAddressNode.getText()) {strLandmarkAddress = LandmarkAddressNode.getText();} else {strLandmarkAddress.clear();}}

 return true;
}
bool ALI_I3_Police::fTNnotZero() {
 size_t found;

 found = this->strTN.find_first_not_of("0");
 if (found == string::npos) {return false;}

 return true;
}
void ALI_I3_Police::fClear()
{
 strName.clear();strTN.clear();
}
bool ALI_I3_Police::fLoadPolice(XMLNode  objXML)
{
/*
  string strName, strTN;
*/
 XMLNode NameNode, TNNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 NameNode = LoadChildNode(objXML, "Name");
// NameNode = objXML.getChildNode("Name");
 if (NameNode.isEmpty())  {strName.clear();} 
 else                     {if (NameNode.getText()) {strName = NameNode.getText();} else {strName.clear();}}

 TNNode = LoadChildNode(objXML, "TN");
// TNNode = objXML.getChildNode("TN");
 if (TNNode.isEmpty())  {strTN.clear();} 
 else                   {if (TNNode.getText()) {strTN = TNNode.getText();} else {strTN.clear();}}


 return true;
}
void ALI_I3_Fire::fClear()
{
 strName.clear();strTN.clear();
}
bool ALI_I3_Fire::fLoadFire(XMLNode  objXML)
{
/*
  string strName, strTN;
*/
 XMLNode NameNode, TNNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 NameNode = LoadChildNode(objXML, "Name");
// NameNode = objXML.getChildNode("Name");
 if (NameNode.isEmpty())  {strName.clear();} 
 else                     {if (NameNode.getText()) {strName = NameNode.getText();} else {strName.clear();}}

 TNNode = LoadChildNode(objXML, "TN");
// TNNode = objXML.getChildNode("TN");
 if (TNNode.isEmpty())  {strTN.clear();} 
 else                   {if (TNNode.getText()) {strTN = TNNode.getText();} else {strTN.clear();}}


 return true;
}

bool ALI_I3_Fire::fTNnotZero()
{
 size_t found;

 found = this->strTN.find_first_not_of("0");
 if (found == string::npos) {return false;}

 return true;
}
void ALI_I3_EMS::fClear()
{
 strName.clear();strTN.clear();
}
bool ALI_I3_EMS::fLoadEMS(XMLNode  objXML)
{
/*
  string strName, strTN;
*/
 XMLNode NameNode, TNNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 NameNode = LoadChildNode(objXML, "Name");
 //NameNode = objXML.getChildNode("Name");
 if (NameNode.isEmpty())  {strName.clear();} 
 else                     {if (NameNode.getText()) {strName = NameNode.getText();} else {strName.clear();}}

 TNNode = LoadChildNode(objXML, "TN");
 //TNNode = objXML.getChildNode("TN");
 if (TNNode.isEmpty())  {strTN.clear();} 
 else                   {if (TNNode.getText()) {strTN = TNNode.getText();} else {strTN.clear();}}


 return true;
}
bool ALI_I3_EMS::fTNnotZero()
{
 size_t found;

 found = this->strTN.find_first_not_of("0");
 if (found == string::npos) {return false;}

 return true;
}
bool ALI_I3_Agency::fTNnotZero()
{
 size_t found;

 found = this->strTN.find_first_not_of("0");
 if (found == string::npos) {return false;}

 return true;
}

void ALI_I3_Agency::fClear()
{
 strName.clear();strTN.clear();strKind.clear();
}
bool ALI_I3_Agency::fLoadAgency(XMLNode  objXML)
{
/*
  string strName, strTN, strKind;
*/
 XMLNode NameNode, TNNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 if (objXML.getAttribute("kind"))   {strKind = objXML.getAttribute("kind");} else {strKind.clear();}

 NameNode = LoadChildNode(objXML, "Name");
 //NameNode = objXML.getChildNode("Name");
 if (NameNode.isEmpty())  {strName.clear();} 
 else                     {if (NameNode.getText()) {strName = NameNode.getText();} else {strName.clear();}}

 TNNode = LoadChildNode(objXML, "TN");
 //TNNode = objXML.getChildNode("TN");
 if (TNNode.isEmpty())  {strTN.clear();} 
 else                   {if (TNNode.getText()) {strTN = TNNode.getText();} else {strTN.clear();}}


 return true;
}


bool ALI_I3_Agencies::fLoadAgencies(XMLNode  objXML)
{
 /*
  ALI_I3_Police			Police;
  ALI_I3_Fire			Fire;
  ALI_I3_EMS  			EMS;
  vector <ALI_I3_Agency> 	vOtherAgencies;
  string        		strAdditionalInfo;
  string			strESN;
*/

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 XMLNode PoliceNode, FireNode, EMSNode, OtherAgenciesNode, AdditionalInfoNode, ESNNode;

 PoliceNode = LoadChildNode(objXML, "Police");
 //PoliceNode = objXML.getChildNode("Police");
 if (!PoliceNode.isEmpty())       { if(!Police.fLoadPolice(PoliceNode))        {return false;}}

 FireNode = LoadChildNode(objXML, "Fire");
 //FireNode = objXML.getChildNode("Fire");
 if (!FireNode.isEmpty())         { if(!Fire.fLoadFire(FireNode))              {return false;}}

 EMSNode = LoadChildNode(objXML, "EMS");
 //EMSNode = objXML.getChildNode("EMS");
 if (!EMSNode.isEmpty())          { if(!EMS.fLoadEMS(EMSNode))                 {return false;}}

 OtherAgenciesNode = LoadChildNode(objXML, "OtherAgencies");
// OtherAgenciesNode = objXML.getChildNode("OtherAgencies");
 if (!OtherAgenciesNode.isEmpty()){ if(!fLoadOtherAgencies(OtherAgenciesNode)) {return false;}}

 AdditionalInfoNode = LoadChildNode(objXML, "AdditionalInfo");
 //AdditionalInfoNode = objXML.getChildNode("AdditionalInfo");
 if (AdditionalInfoNode.isEmpty())  {strAdditionalInfo.clear();} 
 else                               {if (AdditionalInfoNode.getText()) {strAdditionalInfo = AdditionalInfoNode.getText();} else {strAdditionalInfo.clear();}}

 ESNNode = LoadChildNode(objXML, "ESN");
// ESNNode = objXML.getChildNode("ESN");
 if (ESNNode.isEmpty())  {strESN.clear();} 
 else                    {if (ESNNode.getText()) {strESN = ESNNode.getText();} else {strESN.clear();}}



 return true;
}

bool ALI_I3_Agencies::fLoadOtherAgencies(XMLNode  objXML)
{
 //vector <ALI_I3_Agency> 	vOtherAgencies;
 ALI_I3_Agency  Agency;
 int            iNumberofNodes;
 XMLNode        AgencyNode;
 string         strNodeName;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);
 
 iNumberofNodes = objXML.nChildNode();

 for (int i = 0; i < iNumberofNodes; i++)
  {
   AgencyNode = objXML.getChildNode(i);
   strNodeName =  AgencyNode.getName();
   if (RemoveXMLnamespace(strNodeName) == "Agency") {
    Agency.fLoadAgency(AgencyNode);
    vOtherAgencies.push_back(Agency);
   }
  }

 return true;
}

void ALI_I3_DataProvider::fClear() {
 strDataProviderID.clear();strName.clear();strTN.clear();strTelURI.clear();
}

void ALI_I3_DataProvider::fConvertTelURItoTN() {
 size_t found;
 string strTemp;

 if (strTelURI.length()) {
  strTemp = strTelURI;
  found = strTemp.find(";");
  if (found != string::npos) {
   strTemp = strTemp.substr(0,found); 
  }
  strTemp = RemoveTelColon(strTemp);
  strTemp = StripPlusOne(strTemp);
  strTemp = StripOnePlus(strTemp);
  strTemp = FindandReplaceALL(strTemp, "-", "");
  if (strTemp.length() == 10){
   found = strTemp.find_first_not_of("0123456789");
   if (found == string::npos) {
    this->strTN = strTemp;
   }
  }
 }
 return;
}
bool ALI_I3_DataProvider::fLoadDataProvider(XMLNode  objXML)
{
/*
  string strDataProviderID, strName, strTN;
*/
 XMLNode DataProviderIDNode, NameNode, TNNode, TELURInode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 DataProviderIDNode = LoadChildNode(objXML, "DataProviderID");
// DataProviderIDNode = objXML.getChildNode("DataProviderID");
 if (DataProviderIDNode.isEmpty())  {strDataProviderID.clear();} 
 else                     {if (DataProviderIDNode.getText()) {strDataProviderID = DataProviderIDNode.getText();} else {strDataProviderID.clear();}}

 NameNode = LoadChildNode(objXML, "Name");
 //NameNode = objXML.getChildNode("Name");
 if (NameNode.isEmpty())  {strName.clear();} 
 else                     {if (NameNode.getText()) {strName = NameNode.getText();} else {strName.clear();}}

 TNNode = LoadChildNode(objXML, "TN");
// TNNode = objXML.getChildNode("TN");
 if (TNNode.isEmpty())  {strTN.clear();} 
 else                   {if (TNNode.getText()) {strTN = TNNode.getText();} else {strTN.clear();}}

 TELURInode = LoadChildNode(objXML, "TelURI");
// TNNode = objXML.getChildNode("TN");
 if (TELURInode.isEmpty())  {strTelURI.clear();} 
 else                   {if (TELURInode.getText()) {strTelURI = TELURInode.getText();} else {strTelURI.clear();}}


 return true;
}
void ALI_I3_AccessProvider::fClear()
{
 strAccessProviderID.clear(); strName.clear(); strTN.clear(), strTelURI.clear();
}

void ALI_I3_AccessProvider::fConvertTelURItoTN() {
 size_t found;
 string strTemp;

 if (strTelURI.length()) {
  strTemp = strTelURI;
  found = strTemp.find(";");
  if (found != string::npos) {
   strTemp = strTemp.substr(0,found); 
  }
  strTemp = RemoveTelColon(strTemp);
  strTemp = StripPlusOne(strTemp);
  strTemp = StripOnePlus(strTemp);
  strTemp = FindandReplaceALL(strTemp, "-", "");
  if (strTemp.length() == 10){
   found = strTemp.find_first_not_of("0123456789");
   if (found == string::npos) {
    this->strTN = strTemp;
   }
  }
 }
 return;
}

bool ALI_I3_AccessProvider::fLoadAccessProvider(XMLNode  objXML)
{
/*
  string strAccessProviderID, strName, strTN;
*/
 XMLNode AccessProviderIDNode, NameNode, TNNode, TelURInode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 AccessProviderIDNode = LoadChildNode(objXML, "AccessProviderID");
 AccessProviderIDNode = objXML.getChildNode("AccessProviderID");
 if (AccessProviderIDNode.isEmpty())  {strAccessProviderID.clear();} 
 else                     {if (AccessProviderIDNode.getText()) {strAccessProviderID = AccessProviderIDNode.getText();} else {strAccessProviderID.clear();}}

 NameNode = LoadChildNode(objXML, "Name");
 //NameNode = objXML.getChildNode("Name");
 if (NameNode.isEmpty())  {strName.clear();} 
 else                     {if (NameNode.getText()) {strName = NameNode.getText();} else {strName.clear();}}

 TNNode = LoadChildNode(objXML, "TN");
// TNNode = objXML.getChildNode("TN");
 if (TNNode.isEmpty())  {strTN.clear();} 
 else                   {if (TNNode.getText()) {strTN = TNNode.getText();} else {strTN.clear();}}

 TelURInode = LoadChildNode(objXML, "TelURI");
// TNNode = objXML.getChildNode("TN");
 if (TelURInode.isEmpty())  {strTelURI.clear();} 
 else                   {if (TelURInode.getText()) {strTelURI = TelURInode.getText();} else {strTelURI.clear();}}

 return true;
}
void ALI_I3_GeneralUse::fClear()
{
  strID.clear(); strGeneralUse.clear();
}
bool ALI_I3_GeneralUse::fLoadGeneralUse(XMLNode  objXML)
{
 /*
  string strID, strGeneralUse,;
*/
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 if (objXML.isEmpty()){strID.clear();strGeneralUse.clear();}
 else                             
  {
   if (objXML.getText())          {strGeneralUse = objXML.getText();}  else {strGeneralUse.clear();}
   if (objXML.getAttribute("ID")) {strID = objXML.getAttribute("ID");} else {strID.clear();}
  }

 return true;
}

bool ALI_I3_SourceInfo::fLoadGeneralUses(XMLNode  objXML)
{
 //vector <ALI_I3_GeneralUse> 	vGeneralUses;
 ALI_I3_GeneralUse  GeneralUse;
 int                iNumberofNodes;
 XMLNode            GeneralUseNode;
 string             strNodeName;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 iNumberofNodes = objXML.nChildNode();
 
 for (int i = 0; i < iNumberofNodes; i++)
  {
   GeneralUseNode = objXML.getChildNode(i);
   strNodeName =  GeneralUseNode.getName();
   if (RemoveXMLnamespace(strNodeName) == "GeneralUse") {
    GeneralUse.fLoadGeneralUse(GeneralUseNode);
    vGeneralUses.push_back(GeneralUse);
   }
  }

 return true;
}


bool ALI_I3_SourceInfo::fLoadSourceInfo(XMLNode  objXML)
{
/*
  ALI_I3_DataProvider		DataProvider;
  ALI_I3_AccessProvider		AccessProvider;	
  string			strALIUpdateGMT;
  string			strALIRetrievalGMT;
  vector <ALI_I3_GeneralUse>   	vGeneralUses;
*/
 XMLNode DataProviderNode, AccessProviderNode, ALIUpdateGMTNode, ALIRetrievalGMTNode, GeneralUsesNode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 DataProviderNode = LoadChildNode(objXML, "DataProvider" ); 
 //DataProviderNode = objXML.getChildNode( "DataProvider" ); 
 if (!DataProviderNode.isEmpty()) {if (!DataProvider.fLoadDataProvider(DataProviderNode)) {return false;}} 

 AccessProviderNode = LoadChildNode(objXML, "AccessProvider" ); 
 //AccessProviderNode = objXML.getChildNode( "AccessProvider" ); 
 if (!AccessProviderNode.isEmpty()) {if (!AccessProvider.fLoadAccessProvider(AccessProviderNode)) {return false;}} 

 ALIUpdateGMTNode = LoadChildNode(objXML, "ALIUpdateGMT" ); 
 //ALIUpdateGMTNode = objXML.getChildNode( "ALIUpdateGMT" ); 
 if (ALIUpdateGMTNode.isEmpty())  {strALIUpdateGMT.clear();} 
 else                             {if (ALIUpdateGMTNode.getText()) {strALIUpdateGMT = ALIUpdateGMTNode.getText();} else {strALIUpdateGMT.clear();}}

 ALIRetrievalGMTNode = LoadChildNode(objXML, "ALIRetrievalGMT" ); 
// ALIRetrievalGMTNode = objXML.getChildNode( "ALIRetrievalGMT" ); 
 if (ALIRetrievalGMTNode.isEmpty())  {strALIRetrievalGMT.clear();} 
 else                                {if (ALIRetrievalGMTNode.getText()) {strALIRetrievalGMT = ALIRetrievalGMTNode.getText();} else {strALIRetrievalGMT.clear();}}

 GeneralUsesNode = LoadChildNode(objXML, "GeneralUsesNode" ); 
// GeneralUsesNode = objXML.getChildNode( "GeneralUses" ); 
 if (!GeneralUsesNode.isEmpty()) {if (!fLoadGeneralUses(GeneralUsesNode)) {return false;}}

 return true;
}

string ALI_I3_Data::fGMTtoLocalTimeStamp(string strData)
{
 // Time coming in format = 2014-01-07T13:53:27.0Z
 struct tm 		tmGMTtime;
 time_t          	timeGMT;

 strptime(strData.c_str(), "%FT%T.%Z", &tmGMTtime);
 timeGMT = timegm(&tmGMTtime);

return get_ALI_Record_Local_TimeStamp(timeGMT);
}



void ALI_I3_NetworkInfo::fClear()
{
 strPSAPALIHost.clear(); strRespALIHost.clear(); strPSAPID.clear(); strPSAPName.clear(); strRouterID.clear(); strExchange.clear(); strCLLI.clear();
}
bool ALI_I3_NetworkInfo::fLoadNetworkInfo(XMLNode  objXML)
{
 /*
  string strPSAPALIHost, strRespALIHost, strPSAPID, strPSAPName, strRouterID, strExchange, strCLLI;
*/
 XMLNode PSAPALIHostNode, RespALIHostNode, PSAPIDNode, PSAPNameNode, RouterIDNode, ExchangeNode, CLLINode;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 PSAPALIHostNode = LoadChildNode(objXML, "PSAPALIHost" ); 
// PSAPALIHostNode = objXML.getChildNode( "PSAPALIHost" ); 
 if (PSAPALIHostNode.isEmpty())  {strPSAPALIHost.clear();} 
 else                            {if (PSAPALIHostNode.getText()) {strPSAPALIHost = PSAPALIHostNode.getText();} else {strPSAPALIHost.clear();}}

 RespALIHostNode = LoadChildNode(objXML, "RespALIHost" ); 
 //RespALIHostNode = objXML.getChildNode( "RespALIHost" ); 
 if (RespALIHostNode.isEmpty())  {strRespALIHost.clear();} 
 else                            {if (RespALIHostNode.getText()) {strRespALIHost = RespALIHostNode.getText();} else {strRespALIHost.clear();}}

 PSAPIDNode = LoadChildNode(objXML, "PSAPID" ); 
// PSAPIDNode = objXML.getChildNode( "PSAPID" ); 
 if (PSAPIDNode.isEmpty())  {strPSAPID.clear();} 
 else                       {if (PSAPIDNode.getText()) {strPSAPID = PSAPIDNode.getText();} else {strPSAPID.clear();}} 

 PSAPNameNode = LoadChildNode(objXML, "PSAPName" ); 
 //PSAPNameNode = objXML.getChildNode( "PSAPName" ); 
 if (PSAPNameNode.isEmpty())  {strPSAPName.clear();} 
 else                         {if (PSAPNameNode.getText()) {strPSAPName = PSAPNameNode.getText();} else {strPSAPName.clear();}} 

 RouterIDNode = LoadChildNode(objXML, "RouterID" ); 
 //RouterIDNode = objXML.getChildNode( "RouterID" ); 
 if (RouterIDNode.isEmpty())  {strRouterID.clear();} 
 else                         {if (RouterIDNode.getText()) {strRouterID = RouterIDNode.getText();} else {strRouterID.clear();}} 

 ExchangeNode = LoadChildNode(objXML, "Exchange" ); 
 //ExchangeNode = objXML.getChildNode( "Exchange" ); 
 if (ExchangeNode.isEmpty())  {strExchange.clear();} 
 else                         {if (ExchangeNode.getText()) {strExchange = ExchangeNode.getText();} else {strExchange.clear();}} 

 CLLINode = LoadChildNode(objXML, "CLLI" ); 
 //CLLINode = objXML.getChildNode( "CLLI" ); 
 if (CLLINode.isEmpty())  {strCLLI.clear();} 
 else                         {if (CLLINode.getText()) {strCLLI = CLLINode.getText();} else {strCLLI.clear();}}

 return true;
}

void ALI_I3_XML_Data::fClear()
{
 CallInfo.fClear();
 LocationInfo.fClear();
 Agencies.fClear();
 SourceInfo.fClear();
 NetworkInfo.fClear();
 vExtension.clear();
 XMLstring.clear();
}
bool   ALI_I3_XML_Data::fLoadExtensions(XMLNode  objXML)
{
 //vector <ALI_I3_GeneralUse> 	vExtension;
 ALI_I3_Extension   Extension;
 int                iNumberofNodes;
 XMLNode            ExtensionNode;
 string             strNodeName;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 iNumberofNodes = objXML.nChildNode();

 for (int i = 0; i < iNumberofNodes; i++)
  {
   ExtensionNode = objXML.getChildNode(i);
   strNodeName =  ExtensionNode.getName();
   if (RemoveXMLnamespace(strNodeName) == "Extension") {   
    Extension.fLoadExtension(ExtensionNode);
    vExtension.push_back(Extension);
   }
  }

 return true; 
}

void ALI_I3_Extension::fClear()
{
 strName.clear(); strSource.clear(); strVersion.clear();
}
bool ALI_I3_Extension::fLoadExtension(XMLNode  objXML)
{
 /*
  string strName, strSource, strVersion;
*/
 if (objXML.getAttribute("name"))   {strName = objXML.getAttribute("name");}       else {strName.clear();}

 if (objXML.getAttribute("source")) {strSource = objXML.getAttribute("source");}   else {strSource.clear();}

 if (objXML.getAttribute("version")){strVersion = objXML.getAttribute("version");} else {strVersion.clear();}

 return true;
}

bool   ALI_I3_Data::fLoadI3ALIData(XMLNode  objXML)
{
 XMLNode        ALIBodyNode, CallInfoNode, LocationInfoNode, AgenciesNode, SourceInfoNode, NetworkInfoNode, ExtensionNode;
 char* 		cptrResponse = NULL;
 int*           iptrSize = 0;
 //cout << "FOUND ALIBODY" << endl;
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 ALIBodyNode = objXML;
 if (ALIBodyNode.isEmpty())  {SendCodingError("ALI_I3_Data::fLoadI3ALIData() I3 ALIBody XML Tag Missing!"); return false;} 

 CallInfoNode = LoadChildNode(ALIBodyNode, XML_NODE_CALL_INFO ); 
// CallInfoNode = ALIBodyNode.getChildNode( XML_NODE_CALL_INFO ); 
 if (CallInfoNode.isEmpty()) {SendCodingError("ALI_I3_Data::fLoadI3ALIData() I3 CallInfo XML Tag Missing!"); return false;} 
 if (!I3XMLData.CallInfo.fLoadCallInfo(CallInfoNode))                                                       {return false;}

 LocationInfoNode = LoadChildNode(ALIBodyNode, XML_NODE_LOCATION_INFO ); 
// LocationInfoNode = ALIBodyNode.getChildNode( XML_NODE_LOCATION_INFO ); 
 if (LocationInfoNode.isEmpty()) {SendCodingError("ALI_I3_Data::fLoadI3ALIData() I3 LocationInfo XML Tag Missing!"); return false;} 
 if (!I3XMLData.LocationInfo.fLoadLocationInfo(LocationInfoNode))                                                   {return false;}

 AgenciesNode = LoadChildNode(ALIBodyNode, XML_NODE_AGENCIES ); 
// AgenciesNode = ALIBodyNode.getChildNode( XML_NODE_AGENCIES ); 
 if (AgenciesNode.isEmpty()) {SendCodingError("ALI_I3_Data::fLoadI3ALIData() I3 Agencies XML Tag Missing!"); return false;} 
 if (!I3XMLData.Agencies.fLoadAgencies(AgenciesNode))                                                       {return false;}

 SourceInfoNode = LoadChildNode(ALIBodyNode, XML_NODE_SOURCE_INFO ); 
// SourceInfoNode = ALIBodyNode.getChildNode( XML_NODE_SOURCE_INFO ); 
 if (SourceInfoNode.isEmpty()) {SendCodingError("ALI_I3_Data::fLoadI3ALIData() I3 SourceInfo XML Tag Missing!"); return false;} 
 if (!I3XMLData.SourceInfo.fLoadSourceInfo(SourceInfoNode))                                                     {return false;}

 NetworkInfoNode = LoadChildNode(ALIBodyNode, XML_NODE_NETWORK_INFO ); 
// NetworkInfoNode = ALIBodyNode.getChildNode( XML_NODE_NETWORK_INFO ); 
 if (NetworkInfoNode.isEmpty()) {SendCodingError("ALI_I3_Data::fLoadI3ALIData() I3 NetworkInfo XML Tag Missing!"); return false;} 
 if (!I3XMLData.NetworkInfo.fLoadNetworkInfo(NetworkInfoNode))                                                    {return false;}

 if (!I3XMLData.fLoadExtensions(ALIBodyNode))                                                                     {return false;}

 cptrResponse = ALIBodyNode.createXMLString(0, iptrSize);

 if (cptrResponse != NULL)
  {
   I3XMLData.XMLstring = cptrResponse;
   free(cptrResponse); 
  }
 cptrResponse = NULL;
 return true;
}


bool ALI_I3_Data::fMapNotestoI3xml() { 
size_t sz;
string strData = "";

 sz = this->ADRdata.SubscriberInfo.vcardSubscriberData.vNote.size();
 // //cout << "note size -> " << this->ADRdata.SubscriberInfo.vcardSubscriberData.vNote.size() << endl;

 if (sz == 0) {return false;}

 for (unsigned int i = 0; i < sz; i++) {
  strData += this->ADRdata.SubscriberInfo.vcardSubscriberData.vNote[i].Text.strText; 
  strData += "\n";
 }

 this->I3XMLData.CallInfo.strSpecialMessage = strData;
 return true;
}

bool ALI_I3_Data::fMapCustomerNametoI3xml() {

 this->I3XMLData.CallInfo.strCustomerName = this->ADRdata.SubscriberInfo.vcardSubscriberData.fullname.fn.strText;

 return true;
}


bool ALI_I3_Data::fMapMainTelNumbertoI3xml(string strData) {
int index = -1;
string strTenDigitNumber = strData;


 strTenDigitNumber = RemoveTelColon(strTenDigitNumber);
 strTenDigitNumber = StripPlusOne(strTenDigitNumber);
 strTenDigitNumber = StripOnePlus(strTenDigitNumber);
 strTenDigitNumber = RemoveSemicolonToEnd(strTenDigitNumber);
 if (ValidTenDigitNumber(strTenDigitNumber)) {
  this->I3XMLData.CallInfo.strMainTelNum = strTenDigitNumber;
  this->I3XMLData.CallInfo.strCallbackNum = strTenDigitNumber;  
 }
 else {
  SendCodingError("ali_I3_functions.cpp fMapMainTelNumbertoI3xml() Tel not vaild in fn fMapMainTelNumbertoI3xml() -> "+strTenDigitNumber);
  return false; 
 }
 return true;
}



bool ALI_I3_Data::fMapMainTelNumbertoI3xml() {
int index = -1;
string strTenDigitNumber;

 index = this->ADRdata.SubscriberInfo.vcardSubscriberData.fMainTelnumber();

 if (index < 0) {
   SendCodingError("ali_I3_functions.cpp fMapMainTelNumbertoI3xml() No Main Tel number in SubsciberInfo"); 
   return false;
 }

 strTenDigitNumber = RemoveTelColon(this->ADRdata.SubscriberInfo.vcardSubscriberData.vTel[index].URI.strURI);
 strTenDigitNumber = StripPlusOne(strTenDigitNumber);
 strTenDigitNumber = StripOnePlus(strTenDigitNumber);
 strTenDigitNumber = RemoveSemicolonToEnd(strTenDigitNumber);
 if (ValidTenDigitNumber(strTenDigitNumber)) {
  this->I3XMLData.CallInfo.strMainTelNum = strTenDigitNumber;
  this->I3XMLData.CallInfo.strCallbackNum = strTenDigitNumber;  
 }
 else {
  SendCodingError("ali_I3_functions.cpp fMapMainTelNumbertoI3xml() Main Tel not vaild in SubsciberInfo -> "+strTenDigitNumber);
  return false; 
 }
 return true;
}

bool ALI_I3_Data::fMapATTtypeofServiceToI3xml() {
/*
 the first Tel number Text should have the TOS data:

*/


 //check size of tel vector in vcard ....

 // //cout << "tel size -> " << this->ADRdata.SubscriberInfo.vcardSubscriberData.vTel.size() << endl;
 switch (this->ADRdata.SubscriberInfo.vcardSubscriberData.vTel.size() ) {
  case 0:
   SendCodingError("ali_I3_functions.cpp fMapATTtypeofServiceToI3xml() No Tel Data in SubsciberInfo"); 
   return false;

  case 1:
    SendCodingError("ali_I3_functions.cpp fMapATTtypeofServiceToI3xml() Only 1 Tel record in SubsciberInfo");   
    return false;
  default:
    // 1st tel record text should be Type of Service .....
   // //cout << this->ADRdata.SubscriberInfo.vcardSubscriberData.vTel[0].Text.strText << endl;
    this->I3XMLData.CallInfo.fLoadTypeofService(this->ADRdata.SubscriberInfo.vcardSubscriberData.vTel[0].Text.strText);
   // //cout << this->I3XMLData.CallInfo.strTypeOfService << " " << this->I3XMLData.CallInfo.strTypeOfServiceCode << endl;
 }

 return true;
}

bool ALI_I3_Data::fMapADRServiceInfoToI3xml(string strCOS, string strCode) { 
/* Function used for DDTI using LegacyClassOfService

   We assume that the "type" is for the class of service code
   this could change .... TBD

*/
 string strMessage;

 extern string		ClassofServiceFromCode(string strInput);

 if ( (strCOS.empty()) && (strCode.empty()) ) {
  SendCodingError("ali_I3_functions.cpp fMapADRServiceInfoToI3xml() COS and Code blank!");
  return false;
 }
 if (!strCode.empty()) { 
  //determine COS from code 
  this->I3XMLData.CallInfo.strClassOfServiceCode = strCode;
  this->I3XMLData.CallInfo.strClassOfService     = ClassofServiceFromCode(strCode);
  if (!strCOS.empty()) {
   if (strCOS != ClassofServiceFromCode(strCode)) {
    strMessage = "ali_I3_functions.cpp: Code Mismatch in class of service in fn ALI_I3_Data::fMapADRServiceInfoToI3xml() -> Type: ";
    strMessage += strCOS;
    strMessage += " Code: ";
    strMessage += strCode;     
    SendCodingError(strMessage);
   }
  }
 }  
 else {
 //Just use what was provided we cannot reverse because WPH1 has 2 values for code ....
  this->I3XMLData.CallInfo.strClassOfServiceCode = strCode;
  this->I3XMLData.CallInfo.strClassOfService     = strCOS;
 }
 return true;
}

bool ALI_I3_Data::fMapADRServiceInfoToI3xml() {
// 
/*
 string		strDataProviderReference;
 string		strServiceEnvironment;
 string 	strServiceType;
 string		strServiceMobility;

 We also will have previously determined the method variable from the Pidflo
 in this->LocationInfo

 reference NENA-STA-010.2

*/

bool bMTHmanual, bMTHother, bMTHcell, bMTHwireless;
bool bMOBother, bMOBfixed, bMOBmobile, bMOBnomadic;
bool bSEother, bSEresidence, bSEbusiness;
bool bSTother, bSTpots, bSTpotsremote, bSTcoin, bstCOCoin, bSTvoip, bSTvoipwireless, bSTvoipcoin, bSTwireless, bSToneway, bSTmltshosted, bSTmltslocal;
string 				strLegacyCOScode;
string				strError;
nena_geopriv_method 		eMethod;
nena_adr_service_type		eServiceType;
nena_adr_service_mobility       eServiceMobility;
nena_adr_service_environment	eServiceEnvironment;

extern nena_geopriv_method 	 	DetermineMethod(string strInput);
extern nena_adr_service_type 	 	DetermineServiceType(string strInput);
extern nena_adr_service_mobility 	DetermineServiceMobility(string strInput);
extern nena_adr_service_environment 	DetermineServiceEnvironment(string strInput);

 eMethod      		= DetermineMethod(this->I3XMLData.LocationInfo.strMethod); 
 eServiceType 		= DetermineServiceType(this->ADRdata.ServiceInfo.strServiceType);
 eServiceMobility 	= DetermineServiceMobility(this->ADRdata.ServiceInfo.strServiceMobility);
 eServiceEnvironment    = DetermineServiceEnvironment(this->ADRdata.ServiceInfo.strServiceEnvironment);

switch (eMethod) {

 case ngManual: 
      bMTHmanual = true; bMTHcell = false; bMTHwireless = false; bMTHother = false; break;
 case ngCell:
      bMTHmanual = false; bMTHcell = true; bMTHwireless = false; bMTHother = false; break;
 case ngWireless:
      bMTHmanual = false; bMTHcell = false; bMTHwireless = true; bMTHother = false; break;      
 case ngOther: 
      bMTHmanual = false; bMTHcell = false; bMTHwireless = false; bMTHother = true; break;
}

switch (eServiceType) {

 case stOther:
      bSTother = true; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stPOTS:
      bSTother = false; bSTpots = true; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stPOTSremote:
      bSTother = false; bSTpots = false; bSTpotsremote = true; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stCoin:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = true; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted =false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stOneWay:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = true; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stCOCoin:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = true;
      break;
 case stVOIP:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = true; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stVOIPwireless:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = true; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stVOIPcoin:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = true;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stWireless:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = true; bSTmltshosted = false; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stMLTShosted:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = true; bSTmltslocal = false; bstCOCoin = false;
      break;
 case stMLTSlocal:
      bSTother = false; bSTpots = false; bSTpotsremote = false; bSTcoin = false; bSToneway = false; bSTvoip = false; bSTvoipcoin = false;
      bSTvoipwireless = false; bSTwireless = false; bSTmltshosted = false; bSTmltslocal = true; bstCOCoin = false;
      break;


}         

switch (eServiceMobility) {

 case smOther:
      bMOBother = true; bMOBfixed = false, bMOBmobile = false; bMOBnomadic = false; break;
 case smFixed:
      bMOBother = false; bMOBfixed = true, bMOBmobile = false; bMOBnomadic = false; break;
 case smMobile:
      bMOBother = false; bMOBfixed = false, bMOBmobile = true; bMOBnomadic = false; break;
 case smNomadic:
      bMOBother = false; bMOBfixed = false, bMOBmobile = false; bMOBnomadic = true; break;

}

switch (eServiceEnvironment) {

 case seOther:
      bSEother = true; bSEresidence = false; bSEbusiness = false; break;
 case seResidence:
      bSEother = false; bSEresidence = true; bSEbusiness = false; break;
 case seBusiness:
      bSEother = false; bSEresidence = false; bSEbusiness = true; break;
}


if       ((bMTHmanual) && (bSTpots) && (bSEresidence) && (bMOBfixed) ) {
//Legacy 1 Residence
strLegacyCOScode = "1";
}
else if ((bMTHmanual) && (bSTpots) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy 2 Business
strLegacyCOScode = "2";
}
else if ((bMTHmanual) && (bSTmltslocal) && (bSEresidence) && (bMOBfixed) ) {
//Legacy 3 Residence PBX
strLegacyCOScode = "3";
}
else if ((bMTHmanual) && (bSTmltslocal) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy 4 Business PBX
strLegacyCOScode = "4";
}
else if ((bMTHmanual) && (bSTmltshosted) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy 5 Centrex
strLegacyCOScode = "5";
}
else if ((bMTHmanual) && (bSToneway) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy 6 Coin 1 way out
strLegacyCOScode = "6";
}
else if ((bMTHmanual) && (bSTcoin) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy 7 Coin 2 way
strLegacyCOScode = "7";
}
else if ((bMTHmanual) && (bSTwireless) && (bMOBmobile) ) {
//Legacy 8 Wireless Phase 0
strLegacyCOScode = "8";
}
else if ((bMTHmanual) && (bSTpotsremote) && (bSEresidence) && (bMOBfixed) ) {
//Legacy 9 Residence OPX
strLegacyCOScode = "9";
}
else if ((bMTHmanual) && (bSTpotsremote) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy 0 Business OPX
strLegacyCOScode = "0";
}
else if ((bMTHmanual) && (bstCOCoin) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy A Customer Owned Coin
strLegacyCOScode = "A";
}
else if ((bMTHmanual) && (bSTvoip) && (bSEresidence) && (bMOBfixed) ) {
//Legacy C VOIP Residence .... STA-010.2 says Unknown Mobilty ADR spec says fixed ...
strLegacyCOScode = "C";
}
else if ((bMTHmanual) && (bSTvoip) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy D VOIP Business .... STA-010.2 says Unknown Mobilty ADR spec says fixed ...
strLegacyCOScode = "D";
}
else if ((bMTHmanual) && (bSTvoipcoin) && (bSEbusiness) && (bMOBfixed) ) {
//Legacy E VOIP Coin or Pay Phone .... STA-010.2 says Unknown Mobilty ADR spec says fixed ...
strLegacyCOScode = "E";
}
else if ((bMTHmanual) && (bSTvoipwireless) && (bMOBmobile) ) {
//Legacy F VOIP Wireless 
strLegacyCOScode = "F";
}
else if ((bMTHmanual) && (bSTvoip) && (bMOBnomadic) ) {
//Legacy J VOIP Nomadic
strLegacyCOScode = "J";
}
else if ((bMTHmanual) && (bSTvoip) && (bSEbusiness) && (bMOBother) ) {
//Legacy K VOIP Enterprise ... 
strLegacyCOScode = "K";
}
else if ((bMTHcell) && (bSTwireless) && (bMOBmobile) ) {
//Legacy G Wireless Phase I
strLegacyCOScode = "G";
}
else if ((bMTHother) && (bSTwireless) && (bMOBmobile) ) {
//Legacy H Wireless Phase II
strLegacyCOScode = "H";
}
else if ((bMTHcell) && (bSTwireless) && (bMOBmobile) ) {
//Legacy I Wireless Phase II returning Phase I ... This will never get selected !!!! Same criteria as G
strLegacyCOScode = "I";
}
else if ((bMTHmanual) && (bSTvoip) && (bSEother) && (bMOBother))  {
//Legacy V VOICE Over IP 
strLegacyCOScode = "V";
}
else {
strLegacyCOScode = "B";
strError =  "Method      : " + this->I3XMLData.LocationInfo.strMethod + "\n";
strError += "ServiceType : " + this->ADRdata.ServiceInfo.strServiceType + "\n";
strError += "ServiceMob  : " + this->ADRdata.ServiceInfo.strServiceMobility + "\n";
strError += "ServiceEnv  : " + this->ADRdata.ServiceInfo.strServiceEnvironment + "\n";
SendCodingError("ali_I3_functions.cpp fMapADRServiceInfoToI3xml() Unable to generate COS!\n" + strError); 
}


this->I3XMLData.CallInfo.strClassOfServiceCode = strLegacyCOScode;
this->I3XMLData.CallInfo.strClassOfService     = ClassofServiceFromCode(strLegacyCOScode);

// deal in subscriber info for privacy requested ....
/*
Type of Service 0-9 code
0 "NotFX or Non-Published"
1 "FX in 911 Seving area"
2 "FX outside of 911 serving area"
3 "Non-Published"
4 "Non-Published FX in 911 serving area"
5 "Non-Published FX outside 911 serving area"
8 "PS/ALI Published"
9 "PS/ALI Non-Published"

*/


 return true;
}

void ALI_I3_Data::fMapADRProviderInfoToI3xml() {
// we have data provider and access provider
//how do we split ?
/*
 string		strDataProviderReference;
 string		strDataProviderString;
 string		strProviderID;
 string		strProviderIDSeries;
 string		strTypeOfProvider;
 string		strContactURI;
 string		strLanguage;
 vcard		vcardDataProviderContact;
 string		strSubcontractorPrincipal;
 string		strSubcontractorPriority;
*/

//  ALI_I3_DataProvider
//  string strDataProviderID, strName, strTN;

//  ALI_I3_AccessProvider
//  string strAccessProviderID, strName, strTN;

//this->I3XMLData.SourceInfo.DataProvider
//this->I3XMLData.SourceInfo.AccessProvider

this->I3XMLData.SourceInfo.AccessProvider.strAccessProviderID = after_last_colon(this->ADRdata.ProviderInfo.strProviderID);
this->I3XMLData.SourceInfo.AccessProvider.strName             = after_last_colon(this->ADRdata.ProviderInfo.strDataProviderString);
this->I3XMLData.SourceInfo.AccessProvider.strTelURI           = this->ADRdata.ProviderInfo.strContactURI;
this->I3XMLData.SourceInfo.AccessProvider.fConvertTelURItoTN();

//cout << this->I3XMLData.SourceInfo.AccessProvider.strAccessProviderID << endl;
//cout << this->I3XMLData.SourceInfo.AccessProvider.strName  << endl;
//cout << this->I3XMLData.SourceInfo.AccessProvider.strTN << endl;
//cout << this->I3XMLData.SourceInfo.AccessProvider.strTelURI << endl;
}


bool ALI_I3_Data::fLoadATTlocationData(XMLNode PresenceNode) {
 XMLNode            TupleNode, StatusNode, GeoprivNode, locationinfoNode, CivicAddressNode, PointNode, posNode, MethodNode;
 XMLNode            CountryNode, A1Node, A2Node, A3Node, PRDnode, RDnode, STSnode, PODnode, HNOnode, HNSnode, LMKnode, LOCnode, FLRnode, NAMnode, PCnode;
 XMLNode            BLDnode, UNITnode, PLCnode, PCNnode, ALINode, LocationURInode, LocationURIsetNode, ErrorMessageNode, CircleNode, RadiusNode, ConfidenceNode;
 XMLNode            COMMnode;
 XMLNode            rootNode, TimeStampNode, TempNode, ProvidedByNode, DataProviderNode, TelURInode;
 string             strLatitudeLongitude;
 size_t             found;
 //string             strTupleID;
 string             strName = "";
 char *             cptrResponse = NULL;
 int *              iptrSize = NULL;
 int                iNumChildNodes;
 string             strMessage;
 string             strXMLData;
 string             strNodeName;
 bool               boolFoundNodeName = false;
 location_type      elocationtype   = NO_TYPE;
 bool               boolCivicData   = false;
 bool               boolCircleData  = false;
 bool               boolPointData   = false;
 string             stringXMLString = "";

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

 cptrResponse =  PresenceNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 ////cout << "fLoadATTlocationData -> " << endl << stringXMLString << endl;

/*
 //Try Entity as Bid ID ........
 if (PresenceNode.getAttribute("entity"))   {
  this->strBidId = PresenceNode.getAttribute("entity");
  this->strEntity = this->strBidId;
 }
 else                                  {cout << "entity missing" << endl;  return false;}
*/
 TupleNode = LoadChildNode(PresenceNode, "tuple");
 if (TupleNode.isEmpty())              {cout <<  "tupleNode Node Missing" << endl; return false; }

 if (TupleNode.getAttribute("id"))   {this->strTupleID = TupleNode.getAttribute("id");}
 else                                {cout << "Tuple id missing" << endl;  return false;}
 
 TimeStampNode = LoadChildNode(TupleNode, "timestamp");
 if (TimeStampNode.isEmpty())                  {
  //cout <<  "TimeStampNode Node Missing fn fLoadATTlocationData()" << endl;
  // default is to place time now in I3Data ..
 }
 else if (TimeStampNode.getText())  {I3XMLData.LocationInfo.GeoLocation.strDateStamp   = TimeStampNode.getText();} 

 StatusNode = LoadChildNode(TupleNode, "status");
 if (StatusNode.isEmpty())              {cout <<  "status Node Missing" << endl; return false; } 
 GeoprivNode = LoadChildNode(StatusNode, "geopriv");
 if (GeoprivNode.isEmpty())            {cout <<  "geopriv Node Missing" << endl; return false; } 

 // method is in AT&T pidflo and DDTI..... NGA ???
 MethodNode = LoadChildNode(GeoprivNode, "method");
 if (MethodNode.isEmpty())                  {cout <<  "MethodNode Node Missing" << endl; return false; } 
 if (MethodNode.getText())                  {I3XMLData.LocationInfo.strMethod  = MethodNode.getText();} 

 // provided-by is in AT&T pidflo and DDTI ..... NGA ????
 ProvidedByNode = LoadChildNode(GeoprivNode, "provided-by");
 if (!ProvidedByNode.isEmpty()) { 
  DataProviderNode = LoadChildNode(ProvidedByNode, "DataProviderID");
  if (!DataProviderNode.isEmpty()) { 
   this->I3XMLData.SourceInfo.DataProvider.fLoadDataProvider(DataProviderNode);
   this->I3XMLData.SourceInfo.DataProvider.strName = this->I3XMLData.SourceInfo.DataProvider.strDataProviderID;
   this->I3XMLData.SourceInfo.DataProvider.fConvertTelURItoTN();  
  }              
 }
 else {
  //cout <<  "provided-by Node Missing" << endl; 
  return false; 
 }

 locationinfoNode = LoadChildNode(GeoprivNode, "location-info");
 if (locationinfoNode.isEmpty())       {cout <<  "location-info" << endl; return false; }

 if (!fLoadLocationInfoNode(GeoprivNode)) {return false;}

return true;
}


bool   ALI_I3_Data::fLoadLocationData(DataPacketIn objData, string* strError)
{
 XMLResults         xe;
 XMLNode            MainNode, LocationResponseNode, PresenceNode, TupleNode, StatusNode, GeoprivNode, locationinfoNode, CivicAddressNode, PointNode, posNode;
 XMLNode            CountryNode, A1Node, A2Node, A3Node, PRDnode, RDnode, STSnode, PODnode, HNOnode, HNSnode, LMKnode, LOCnode, FLRnode, NAMnode, PCnode;
 XMLNode            BLDnode, UNITnode, PLCnode, PCNnode, ALINode, LocationURInode, LocationURIsetNode, ErrorMessageNode, CircleNode, RadiusNode, ConfidenceNode;
 XMLNode            rootNode, TimeStampNode, TempNode, MethodNode, ProvidedbyNode, EmergencyCallDataValueNode, ProviderInfoNode, SubscriberInfoNode, ServiceInfoNode;
 XMLNode            ContactNode, LegacyClassOfServiceNode, ESNnode, COMMnode;
 string             strLatitudeLongitude;
 size_t             found;
 //string             strTupleID;
 string             strName = "";
 char *             cptrResponse = NULL;
 int *              iptrSize = NULL;
 int                iNumChildNodes;
 string             strMessage;
 string             strXMLData;
 string             strNodeName;
 bool               boolFoundNodeName = false;
 location_type      elocationtype = NO_TYPE;
 bool               boolCivicData  = false;
 bool               boolCircleData = false;
 bool               boolPointData  = false;
 string             strPresenceCallBack ="";
 string             strCOS = "";
 string		    strCOScode = "";
 bool               boolLegacyCodeFound = false;

 if (objData.strRequestID.length()) {
  this->fLoadBidID(objData.strRequestID);
  //cout << "ALI_I3_Data::fLoadLocationData -> Request ID -> load bid id" << objData.strRequestID << endl;
 }
 rootNode=XMLNode::createXMLTopNode("xml",TRUE);

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

 strXMLData     = RemoveXMLHeader(objData.stringDataIn);
 strPidfXMLData = extractPidfXMLData(objData.stringDataIn);

 strPidfXMLData = RemoveXMLHeader(strPidfXMLData);

 if (strPidfXMLData.empty()) { MainNode=XMLNode::parseString(strXMLData.c_str(),NULL,&xe);}
 else                        { MainNode=XMLNode::parseString(strPidfXMLData.c_str(),NULL,&xe);}
 
 if (xe.error) {strMessage = "ALI_I3_Data::fLoadLocationData() XML error -> ";strMessage += XMLNode::getError(xe.error);SendCodingError(strMessage);       return false;}  
 fTimeStamp();
 
 // check for Top Node ..
 if (MainNode.getName())   {strName = MainNode.getName();}
 //cout << "Main Node Name is -> " << RemoveXMLnamespace(strName) << endl;

 //check for ADR EmergencyCallData
 found = RemoveXMLnamespace(strName).find("EmergencyCallData");
 if (found != string::npos) {
  this->boolEmergencyCallDataRequestFlag = true;
  return this->ADRdata.fLoadEmergencyCallData(MainNode, strError);
 }

 //check for ALIBody
 if (RemoveXMLnamespace(strName) == "ALIBody") {
   rootNode = rootNode.addChild(MainNode);
   //cout << "look for ALIbody B" << endl;
   return fLoadI3ALIData(rootNode);
 }
 else if (RemoveXMLnamespace(strName) == "locationResponse"){
   rootNode = rootNode.addChild(MainNode);
   MainNode = rootNode; 
 }
 else if (RemoveXMLnamespace(strName) == "error"){
  strMessage.clear();
  ErrorMessageNode = LoadChildNode(MainNode, "message");
  if (!ErrorMessageNode.isEmpty()) {
   strMessage.clear();
   if (ErrorMessageNode.getText())  {strMessage = ErrorMessageNode.getText();}
   if (strError) {*strError = "HTML Response Error -> "+ strMessage;}
   SendCodingError("ali_I3.functions.cpp - HTML Response Error -> "+ strMessage );
  } 
  return false; 
 }
 
 // Check first for ALI I3 Data
 ALINode = LoadChildNode(MainNode, "ALIBody");
 //cout << "look for ALIbody A" << endl;
 if (!ALINode.isEmpty()) { return fLoadI3ALIData(MainNode);}   

 // Check next for PIDFLo Data (this is complicated because of the different formats we can recieve PidfLo locationResponse .......) 
 LocationResponseNode = LoadChildNode(MainNode, "locationResponse");
 if (LocationResponseNode.isEmpty())   {  
  LocationResponseNode = MainNode;
  strName = LocationResponseNode.getName();
  if (RemoveXMLnamespace(strName) != "locationResponse") {

   //This is an AT&T Pidflo by value which has no LocationResponse node ......
   if(!fLoadATTlocationData(MainNode)) {return false;}
   // Load additional data ....
   this->ADRdata.fLoadProviderInfo(objData , strError);
   this->fMapADRProviderInfoToI3xml();
   this->ADRdata.fLoadServiceInfo(objData , strError);
   this->fMapADRServiceInfoToI3xml();
   this->ADRdata.fLoadSubscriberInfo(objData , strError);
   this->fMapATTtypeofServiceToI3xml();
   this->fMapMainTelNumbertoI3xml();
   this->fMapCustomerNametoI3xml();
   this->fMapNotestoI3xml();

   return true;
   //SendCodingError("ali_I3.functions.cpp - No locationResponse in ALI_I3_Data::fLoadLocationData() ->\n"+objData.stringDataIn ); 
   //return false;
  }
 }



 // Save Pidflo remove spaces
 cptrResponse = MainNode.createXMLString(0, iptrSize);
 if (cptrResponse != NULL)
  {
   strPidfXMLData = cptrResponse;
   free(cptrResponse); 
  }
 cptrResponse = NULL;

 
 LocationURIsetNode = LocationResponseNode.getChildNode("locationURIset");
 if (!LocationURIsetNode.isEmpty()) {
  LocationURInode = LocationURIsetNode.getChildNode("locationURI");
  if (!LocationURInode.isEmpty()) {
   if (LocationURInode.getText())  {
    this->objLocationURI.strURI           = LocationURInode.getText();
    this->objLocationURI.boolDataRecieved = true; 
   }
  }
  if (LocationURIsetNode.getAttribute("expires"))   {objLocationURI.strExpires = LocationURIsetNode.getAttribute("expires");}
  else                                              {cout << "locationURIset attribute expires missing" << endl;  return false;}
 }
  

 PresenceNode = LoadChildNode(LocationResponseNode, "presence");
 if (PresenceNode.isEmpty())           {cout <<  "presence Node Missing" << endl; return false; }

 //Pull Presence Callback if it is there ........
 if (PresenceNode.getAttribute("entity"))   {
  strPresenceCallBack = PresenceNode.getAttribute("entity");
  this->strEntity = strPresenceCallBack;
  //remove sip: and @address
  strPresenceCallBack = RemoveSIPcolonPlusATaddress(strPresenceCallBack); 
  // check if a "tel:"
  strPresenceCallBack = FindandReplace(strPresenceCallBack, "tel:", "");
  strPresenceCallBack = StripPlusOne(strPresenceCallBack);
  if (strPresenceCallBack.length() == 11) {
   strPresenceCallBack.erase(0,1);
  }
  if (!ValidTenDigitNumber(strPresenceCallBack)) {
   strPresenceCallBack.clear();
  }  
 }

 TupleNode = LoadChildNode(PresenceNode, "tuple");
 if (TupleNode.isEmpty())              {cout <<  "tupleNode Node Missing" << endl; return false; }

 if (TupleNode.getAttribute("id"))   {this->strTupleID = TupleNode.getAttribute("id");}
 else                                {cout << "Tuple id missing" << endl;  return false;}

 //check for multiple tuple nodes .....
 strName = TupleNode.getName();
 iNumChildNodes = PresenceNode.nChildNode(strName.c_str());

 //cout << "NUMBER OF TUPLE NODES -> " << iNumChildNodes << endl;

 // reverse iterate the tuples ....
 for (int i = iNumChildNodes; i > 0; i--) {
  TupleNode = XMLNode::emptyNode();
  TupleNode = LoadChildNode( PresenceNode , "tuple", i);
  if (TupleNode.isEmpty())       {cout <<  "tuple node empty in for loop" << endl; return false; } 

 
  TimeStampNode = LoadChildNode(TupleNode, "timestamp");
  if (TimeStampNode.isEmpty())                  {
   //cout <<  "TimeStampNode Node Missing in fn fLoadLocationData()" << endl;
 // Place current time in I3 data when converting ALI record
  }
  if (TimeStampNode.getText())  {I3XMLData.LocationInfo.GeoLocation.strDateStamp   = TimeStampNode.getText();} 

  StatusNode = LoadChildNode(TupleNode, "status");
  if (StatusNode.isEmpty())              {cout <<  "status Node Missing" << endl; return false; } 
  GeoprivNode = LoadChildNode(StatusNode, "geopriv");
  if (GeoprivNode.isEmpty())            {cout <<  "geopriv Node Missing" << endl; return false; } 

  //NGA911 puts ALIBody Here if we get it this is what we use ......
  //cout << "before look for ALIBody" << endl;
  ALINode = LoadChildNode(GeoprivNode, "ALIBody");
  if (!ALINode.isEmpty()) { return fLoadI3ALIData(ALINode);}   


  locationinfoNode = LoadChildNode(GeoprivNode, "location-info");
  if (locationinfoNode.isEmpty())       {cout <<  "location-info" << endl; return false; }

  if (!fLoadLocationInfoNode(GeoprivNode)) {return false;}

  if (strPresenceCallBack.length()) {I3XMLData.CallInfo.strCallbackNum = strPresenceCallBack;} 
 
  //NGA911 

  //DDTI check for manual method ......
  ProvidedbyNode = LoadChildNode(GeoprivNode, "provided-by");
  if (!ProvidedbyNode.isEmpty())            {
   //cout <<  " found ProvidedbyNode Node" << endl;
   EmergencyCallDataValueNode = LoadChildNode(ProvidedbyNode, "EmergencyCallDataValue");
   if (!EmergencyCallDataValueNode.isEmpty())            {
    //cout <<  " found EmergencyCallDataValueNode Node" << endl;
    ProviderInfoNode = LoadChildNode(EmergencyCallDataValueNode, "EmergencyCallData.ProviderInfo");
    if (!ProviderInfoNode.isEmpty())            {
     //cout <<  " found ProviderInfoNode Node" << endl;
     this->ADRdata.fLoadProviderInfo(ProviderInfoNode, strError);
     this->fMapADRProviderInfoToI3xml();
    }
    SubscriberInfoNode = LoadChildNode(EmergencyCallDataValueNode, "EmergencyCallData.SubscriberInfo");
    if (!SubscriberInfoNode.isEmpty())            {
     //cout <<  " found SubscriberInfoNode Node" << endl;
     this->ADRdata.fLoadSubscriberInfo(SubscriberInfoNode, strError);
    }

    ServiceInfoNode = LoadChildNode(EmergencyCallDataValueNode, "EmergencyCallData.ServiceInfo");
    if (!ServiceInfoNode.isEmpty())            {
     //cout <<  " found ServiceInfoNode Node" << endl;
     this->ADRdata.fLoadServiceInfo(ServiceInfoNode, strError);

     //DDTI has LegacyClassOfService Node also they leave method blank which renders the fn fMapADRServiceInfoToI3xml() useless
     LegacyClassOfServiceNode = LoadChildNode(ServiceInfoNode, "LegacyClassOfService");
     if (!LegacyClassOfServiceNode.isEmpty())  {
  
      if (LegacyClassOfServiceNode.getText())              {strCOS = LegacyClassOfServiceNode.getText();}            
      if (LegacyClassOfServiceNode.getAttribute("type"))   {strCOScode = LegacyClassOfServiceNode.getAttribute("type");}
      boolLegacyCodeFound = this->fMapADRServiceInfoToI3xml(strCOS,strCOScode);
     }
     //NGA911 provides legacy_class_of_service (they are reversed from DDTI)
     LegacyClassOfServiceNode = LoadChildNode(ServiceInfoNode, "legacy_class_of_service");
     if (!LegacyClassOfServiceNode.isEmpty())  {  
      if (LegacyClassOfServiceNode.getText())              {strCOScode = LegacyClassOfServiceNode.getText();}            
      if (LegacyClassOfServiceNode.getAttribute("type"))   {strCOS = LegacyClassOfServiceNode.getAttribute("type");}
      boolLegacyCodeFound = this->fMapADRServiceInfoToI3xml(strCOS,strCOScode);
     }     

     if(!boolLegacyCodeFound) {
      this->fMapADRServiceInfoToI3xml();
     }

     //DDTI also provides an ESN ....
     ESNnode = LoadChildNode(ServiceInfoNode, "ESN");
     if (!ESNnode.isEmpty())  { 
      if (ESNnode.getText())              {this->I3XMLData.Agencies.strESN = ESNnode.getText();}                
     }  
    }
   
 //   this->fMapATTtypeofServiceToI3xml();
    this->fMapCustomerNametoI3xml();
    this->fMapNotestoI3xml();
   }
   //DDTI Call back number is in contact node
   ContactNode = LoadChildNode(TupleNode, "contact");
   if (!ContactNode.isEmpty())            {
    //cout <<  " found ContactNode Node" << endl;
    if (ContactNode.getText())  {this->fMapMainTelNumbertoI3xml(ContactNode.getText());}
    else                        {cout << "No Text in contact node" << endl;} 
   } 
   else { cout << "No contact Node found" << endl; return false;} 

  }

 }   

 return true;
}

void ExperientDataClass::SetADRflag() {
 extern string strNG911_ECRF_SERVER;

 if      (this->strCallInfoProviderURL.length())    		{ this->ALIData.I3Data.boolEmergencyCallDataRequestFlag = true;}
 else if (this->strCallInfoServiceURL.length())     		{ this->ALIData.I3Data.boolEmergencyCallDataRequestFlag = true;}
 else if (this->strCallInfoSubscriberURL.length())  		{ this->ALIData.I3Data.boolEmergencyCallDataRequestFlag = true;}
 else if (strNG911_ECRF_SERVER != DEFAULT_VALUE_LIS_INI_KEY_9)  { this->ALIData.I3Data.boolEmergencyCallDataRequestFlag = true;}
 else                                               		{ this->ALIData.I3Data.boolEmergencyCallDataRequestFlag = false;}
}    

bool ALI_I3_Data::SetADRflags(bool boolInput) {
 this->boolProviderInfoRXflag  = ((!this->strCallInfoProviderURL.length())||(boolInput));
 this->boolServiceInfoRXflag   = ((!this->strCallInfoServiceURL.length())||(boolInput));
 this->boolSubsciberInfoRXflag = ((!this->strCallInfoSubscriberURL.length())||(boolInput));
 this->boolPidfLoRXflag        = boolInput;

 return true;
} 

/*
bool ALI_I3_Data::SetServiceURIflags(bool boolInput) {
 this->boolServiceURIreadyFlag = boolInput;
 this->boolServiceFireRXflag   = boolInput;
 this->boolServiceEmsRXflag    = boolInput;
 this->boolServicePoliceRXflag = boolInput;
 return true;
}
*/

bool ALI_I3_Data::fNoADRdataReceived() {
 return (!(this->boolProviderInfoRXflag || this->boolServiceInfoRXflag || this->boolSubsciberInfoRXflag || this->boolPidfLoRXflag));
}

/*
bool ALI_I3_Data::fServiceURIDataBidComplete() {

 if (this->ServiceURNList.fAll_URIS_RX()
 this->boolServiceURIreadyFlag = (this->boolServiceFireRXflag && this->boolServiceEmsRXflag && this->boolServicePoliceRXflag);
 return this->boolServiceURIreadyFlag;
}
*/

bool ALI_I3_Data::fAllADRdataReceived() {
 return (this->boolProviderInfoRXflag && this->boolServiceInfoRXflag && this->boolSubsciberInfoRXflag && this->boolPidfLoRXflag);
}

bool ALI_I3_Data::fLoadLocationInfoNode(XMLNode GeoprivNode) {
 XMLNode            locationinfoNode, CivicAddressNode, PointNode, posNode;
 XMLNode            CountryNode, A1Node, A2Node, A3Node, PRDnode, RDnode, STSnode, PODnode, HNOnode, HNSnode, LMKnode, LOCnode, FLRnode, NAMnode, PCnode;
 XMLNode            BLDnode, UNITnode, PLCnode, PCNnode, ALINode, LocationURInode, LocationURIsetNode, ErrorMessageNode, CircleNode, RadiusNode, ConfidenceNode;
 XMLNode            rootNode, TimeStampNode, TempNode;
 XMLNode            COMMnode;
 string             strLatitudeLongitude;
 size_t             found;
 //string             strTupleID;
 string             strName = "";
 char *             cptrResponse = NULL;
 int *              iptrSize = NULL;
 int                iNumChildNodes;
 string             strMessage;
 string             strXMLData;
 string             strNodeName;
 bool               boolFoundNodeName = false;
 location_type      elocationtype = NO_TYPE;
 bool               boolCivicData  = false;
 bool               boolCircleData = false;
 bool               boolPointData  = false;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

 locationinfoNode = LoadChildNode(GeoprivNode, "location-info");
 if (locationinfoNode.isEmpty())       {cout <<  "location-info" << endl; return false; }

 ////cout << "location info node name -> " << locationinfoNode.getName() << endl;

 //Complex Pidflo need to see how many locationinfo's there are ....

 strName = locationinfoNode.getName();

 iNumChildNodes = GeoprivNode.nChildNode(strName.c_str());
 ////cout << "number of location_info child nodes -> " << iNumChildNodes << endl;
 for (int i = 1; i <= iNumChildNodes; i++) {
  ////cout << "i -> " << i << endl;
  locationinfoNode = XMLNode::emptyNode();
  locationinfoNode = LoadChildNode( GeoprivNode , "location-info", i);
  if (locationinfoNode.isEmpty())       {cout <<  "location-info empty in for loop" << endl; return false; }   
  boolCivicData = boolPointData = boolCircleData = false;
 
  // Civic Point or Circle
  CivicAddressNode = LoadChildNode(locationinfoNode, "civicAddress");
  PointNode        = LoadChildNode(locationinfoNode, "Point");
  CircleNode       = LoadChildNode(locationinfoNode, "Circle");

  if (!CivicAddressNode.isEmpty()) {
   elocationtype = CIVIC_ADDRESS; 
   boolCivicData = true;
  } 


  if (!PointNode.isEmpty()) {
   elocationtype = POINT;
   boolPointData = true;
   posNode = LoadChildNode(PointNode, "pos");   
   if (posNode.isEmpty()) {
    //cout <<  "posNode Node Missing" << endl; 
    boolPointData = false;  
    if(!boolCivicData) { 
     return false; 
    }
   }
  }

  if (!CircleNode.isEmpty()) {
   boolCircleData = true;
   elocationtype = CIRCLE;
   posNode = LoadChildNode(CircleNode, "pos");   
   if (posNode.isEmpty()) {
    //cout <<  "posNode Node Missing" << endl;
    boolCircleData = false;
    if (!boolCivicData) { return false; }
    RadiusNode = LoadChildNode(CircleNode, "radius");
    if (RadiusNode.isEmpty()) {
     //cout <<  "RadiusNode Node Missing" << endl; 
     if (!boolCivicData) { return false; }
    }
    ConfidenceNode = LoadChildNode(locationinfoNode, "confidence");
    if (ConfidenceNode.isEmpty()) {
     //cout <<  "ConfidenceNode Node Missing" << endl;
     if (!boolCivicData) { return false; }
    }
   }
  }
  if ((!boolCivicData)&&(!boolCircleData)&&(!boolPointData)) {
   SendCodingError("ali_I3.functions.cpp - no civicAddress or Point or Circle Node in ALI_I3_Data::fLoadLocationInfoNode()" ); 
   return false; 
  }    

  if (boolCivicData) {
   //      //cout << "parsing civic !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
        CountryNode = LoadChildNode(CivicAddressNode, "country");
        if (!CountryNode.isEmpty())                       {if (CountryNode.getText())  {I3XMLData.LocationInfo.StreetAddress.strCountry             = CountryNode.getText();}}
        A1Node = LoadChildNode(CivicAddressNode, "A1");
        if (!A1Node.isEmpty())                            {if (A1Node.getText())       {I3XMLData.LocationInfo.StreetAddress.strStateProvince       = A1Node.getText();}}
        A2Node = LoadChildNode(CivicAddressNode, "A2");
        if (!A2Node.isEmpty())                            {if (A2Node.getText())       {I3XMLData.LocationInfo.StreetAddress.strCountyID            = A2Node.getText();}}   //FIPS Code
        A3Node = LoadChildNode(CivicAddressNode, "A3");
        if (!A3Node.isEmpty())                            {if (A3Node.getText())       {I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity       = A3Node.getText();}}    
        //DDTI special extension Node COMM
        COMMnode = LoadChildNode(CivicAddressNode, "COMM");
        if (!COMMnode.isEmpty())                          {if (COMMnode.getText())     {I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity       = COMMnode.getText();}}
        // A4 -A6 Node not programmed at this time
        // PRM road premodifier not programmed
        PRDnode = LoadChildNode(CivicAddressNode, "PRD");
        if (!PRDnode.isEmpty())                            {if (PRDnode.getText())     {I3XMLData.LocationInfo.StreetAddress.strPrefixDirectional   = PRDnode.getText();}}
        RDnode = LoadChildNode(CivicAddressNode, "RD");
        if (!RDnode.isEmpty())                             {if (RDnode.getText())      {I3XMLData.LocationInfo.StreetAddress.strStreetName          = RDnode.getText();}}
        STSnode = LoadChildNode(CivicAddressNode, "STS");
        if (!STSnode.isEmpty())                            {if (STSnode.getText())     {I3XMLData.LocationInfo.StreetAddress.strStreetSuffix        = STSnode.getText();}}
        PODnode = LoadChildNode(CivicAddressNode, "POD");
        if (!PODnode.isEmpty())                            {if (PODnode.getText())     {I3XMLData.LocationInfo.StreetAddress.strPostDirectional     = PODnode.getText();}}
        // POM     road post modifier
        // RDSEC   Road Section
        // RDBR    Road Branch
        // RDSUBBR Road sub-branch 
        HNOnode = LoadChildNode(CivicAddressNode, "HNO");
        if (!HNOnode.isEmpty())                              {if (HNOnode.getText())   {I3XMLData.LocationInfo.StreetAddress.strHouseNum            = HNOnode.getText();}}
        HNSnode = LoadChildNode(CivicAddressNode, "HNS");
        if (!HNSnode.isEmpty())                              {if (HNSnode.getText())   {I3XMLData.LocationInfo.StreetAddress.strHouseNumSuffix      = HNSnode.getText();}} 
        LMKnode = LoadChildNode(CivicAddressNode, "LMK");
        if (!LMKnode.isEmpty())                              {if (LMKnode.getText())   {I3XMLData.LocationInfo.StreetAddress.strLandmarkAddress     = LMKnode.getText();}}  
        LOCnode = LoadChildNode(CivicAddressNode, "LOC");
        if (!LOCnode.isEmpty())                              {if (LOCnode.getText())   {I3XMLData.LocationInfo.StreetAddress.strLocationDescription = LOCnode.getText();}}  
        FLRnode = LoadChildNode(CivicAddressNode, "FLR");
        if (!FLRnode.isEmpty())                              {if (FLRnode.getText())   {I3XMLData.LocationInfo.StreetAddress.strFloor               = FLRnode.getText();}}  
        NAMnode = LoadChildNode(CivicAddressNode, "NAM");
        if (!NAMnode.isEmpty())                              {if (NAMnode.getText())   {I3XMLData.CallInfo.strCustomerName                          = NAMnode.getText();}}  
        PCnode = LoadChildNode(CivicAddressNode, "PC");
        if (!PCnode.isEmpty())                               {if (PCnode.getText())    {I3XMLData.LocationInfo.StreetAddress.strPostalZipCode       = PCnode.getText();}} 
        BLDnode = LoadChildNode(CivicAddressNode, "BLD");
        if (!BLDnode.isEmpty())                              {if (BLDnode.getText())   {I3XMLData.LocationInfo.StreetAddress.strBuilding            = BLDnode.getText();}}  
        UNITnode = LoadChildNode(CivicAddressNode, "UNIT");
        if (!UNITnode.isEmpty())                             {if (UNITnode.getText())  {I3XMLData.LocationInfo.StreetAddress.strUnitNum             = UNITnode.getText();}}  
        // ROOM
        // SEAT
        PLCnode = LoadChildNode(CivicAddressNode, "PLC");
        if (!PLCnode.isEmpty())                              {if (PLCnode.getText())  {I3XMLData.LocationInfo.StreetAddress.strUnitType            = PLCnode.getText();}}  
        PCNnode = LoadChildNode(CivicAddressNode, "PCN");
        if (!PCNnode.isEmpty())                              {if (PCNnode.getText())  {I3XMLData.LocationInfo.StreetAddress.strPostalCommunity     = PCNnode.getText();}}  
        // POBOX
        // ADDCODE

  }

  if (boolCircleData||boolPointData) {
     ////cout << "loading point or Circle Data" << endl;
      
          if (posNode.getText()) {strLatitudeLongitude = posNode.getText();}
          
          found = strLatitudeLongitude.find(" ");
          if (found != string::npos)  {
            I3XMLData.LocationInfo.GeoLocation.strLatitude  = strLatitudeLongitude.substr(0, found);
            I3XMLData.LocationInfo.GeoLocation.strLongitude = strLatitudeLongitude.substr(found+1, string::npos);
  //          //cout << I3XMLData.LocationInfo.GeoLocation.strLatitude << endl;
  //          //cout << I3XMLData.LocationInfo.GeoLocation.strLongitude << endl;
          }
         ConfidenceNode = LoadChildNode(locationinfoNode, "confidence");
         RadiusNode     = LoadChildNode(CircleNode, "radius");
         if (ConfidenceNode.getText()) {I3XMLData.LocationInfo.GeoLocation.strConfidence  = ConfidenceNode.getText();}
         if (RadiusNode.getText())     {I3XMLData.LocationInfo.GeoLocation.strUncertainty = RadiusNode.getText();}
  }

   // //cout << "Country -> " << I3XMLData.LocationInfo.StreetAddress.strCountry << endl;
   //cout << "MSAG CM leaving-> " << I3XMLData.LocationInfo.StreetAddress.strMSAGCommunity << endl;
   // //cout << "POS     -> " << strLatitudeLongitude << endl;


/*
 cptrResponse = locationinfoNode.createXMLString(0, iptrSize);

 if (cptrResponse != NULL)
  {
   //cout << cptrResponse << endl;
   free(cptrResponse); 
  }
 cptrResponse = NULL;
*/

 }// end  for (int i = 1; i <= iNumChildNodes; i++) 
 return true;
}

XMLNode LoadChildNode( XMLNode ParentNode , string strName)
{
 XMLNode ReturnNode;
 int     iNumChildNodes;
 bool    boolFoundNodeName = false;
 string  strNodeName;

 iNumChildNodes = ParentNode.nChildNode();
 for (int i = 0; i < iNumChildNodes; i++) {
  ReturnNode = ParentNode.getChildNode(i);
  strNodeName =  ReturnNode.getName();
  if (RemoveXMLnamespace(strNodeName) == strName) {boolFoundNodeName = true; break;}
 }
 if (!boolFoundNodeName) {return XMLNode::emptyNode(); }

 return ReturnNode;
}

XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j)
{
 XMLNode ReturnNode;
 int     iNumChildNodes;
 int     iNumNodes;
 bool    boolFoundNodeName = false;
 string  strNodeName;
 int     iCount = 0;

 iNumChildNodes = ParentNode.nChildNode();

 for (int i = 0; i < iNumChildNodes; i++) {
  ReturnNode = XMLNode::emptyNode();
  ReturnNode = ParentNode.getChildNode(i);
  strNodeName =  ReturnNode.getName();
  if (RemoveXMLnamespace(strNodeName) == strName) {
   iCount++;
   if (iCount == j) {
    boolFoundNodeName = true; break;
   }
  }
 }
 if (!boolFoundNodeName) {return XMLNode::emptyNode(); }

 return ReturnNode;
}


string ALI_I3_GeoLocation::Latitude()
{
 size_t found;

 if (strLatitude.empty()) {return "";}

 found = strLatitude.find("+");
 if (found != string::npos) {return strLatitude;}
 found = strLatitude.find("-");
 if (found != string::npos) {return strLatitude;}

return "+"+strLatitude;

}


string ALI_I3_GeoLocation::Longitude()
{
 size_t found;

 if (strLongitude.empty())  {return "";}
 found = strLongitude.find("+");
 if (found != string::npos) {return strLongitude;}
 found = strLongitude.find("-");
 if (found != string::npos) {return strLongitude;}

return "+"+strLongitude;
}

string SevenDigitTelno(string strInput)
{
 if      (strInput.length() == 7)  {return strInput.substr(0,3) + "-" + strInput.substr(3,4);}
 else if (strInput.length() == 10) {return strInput.substr(3,3) + "-" + strInput.substr(6,4);}
 else if (strInput.length() < 8)   {return strInput;}
 return "";

}





