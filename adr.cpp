/*****************************************************************************
* FILE: adr.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of ADR 
*
*
*
* AUTHOR: 1/22/2021 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "adr.h"
#include "globalfunctions.h"
#include "globals.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"                          // XML Software

extern Call_Data	                       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern Text_Data				objBLANK_TEXT_DATA;

//functions 





void ADR_REQUIREMENTS::fSetalltrue() {
 boolRequirePidfLo        = true;
 boolRequireProviderInfo  = true;
 boolRequireServiceInfo   = true;
 boolRequireSubsciberInfo = true;
}
bool ADR_REQUIREMENTS::fAreAllFalse() {
 
return (!(boolRequirePidfLo && boolRequireProviderInfo && boolRequireServiceInfo && boolRequireSubsciberInfo));
}

// constructor
ADR_Data::ADR_Data() {

 this->ProviderInfo.fClear();
 this->ServiceInfo.fClear();
 this->SubscriberInfo.fClear();
 this->boolRequireProviderInfo  = false;
 this->boolRequireServiceInfo   = false;
 this->boolRequireSubsciberInfo = false;
}

// copy constuctor
ADR_Data::ADR_Data(const ADR_Data *a) {
 this->ProviderInfo		= a->ProviderInfo;
 this->ServiceInfo		= a->ServiceInfo;
 this->SubscriberInfo		= a->SubscriberInfo;
 this->boolRequireProviderInfo  = a->boolRequireProviderInfo;
 this->boolRequireServiceInfo   = a->boolRequireServiceInfo;
 this->boolRequireSubsciberInfo = a->boolRequireSubsciberInfo;
}
//assignment operator
ADR_Data& ADR_Data::operator=(const ADR_Data& a)    {
 if (&a == this ) {return *this;}
 this->ProviderInfo		= a.ProviderInfo;
 this->ServiceInfo		= a.ServiceInfo;
 this->SubscriberInfo		= a.SubscriberInfo;
 this->boolRequireProviderInfo  = a.boolRequireProviderInfo;
 this->boolRequireServiceInfo   = a.boolRequireServiceInfo;
 this->boolRequireSubsciberInfo = a.boolRequireSubsciberInfo;
 return *this;
}
void ADR_Data::fClear() {
 this->ProviderInfo.fClear();
 this->ServiceInfo.fClear();
 this->SubscriberInfo.fClear();
 this->boolRequireProviderInfo  = false;
 this->boolRequireServiceInfo   = false;
 this->boolRequireSubsciberInfo = false;
}

bool ADR_Data::fLoadEmergencyCallData(XMLNode MainNode, string* strError) {
 string 	strName;
 char*          cptrResponse;
 string         stringXMLString ="";

 if (MainNode.getName())   {strName = MainNode.getName();}

/*
 cptrResponse =  MainNode.createXMLString();
 if (cptrResponse != NULL)
  {
   stringXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 ////cout << "fLoadEmergencyCallData -> " << endl << stringXMLString << endl;
*/

 if      (RemoveXMLnamespace(strName) == "EmergencyCallData.SubscriberInfo"){
  return this->fLoadSubscriberInfo(MainNode, strError);
 }
 else if (RemoveXMLnamespace(strName) == "EmergencyCallData.ProviderInfo"){
  return this->fLoadProviderInfo(MainNode, strError);
 }
 else if (RemoveXMLnamespace(strName) == "EmergencyCallData.ServiceInfo"){
  return this->fLoadServiceInfo(MainNode, strError);
 }
 else {
  *strError = "ADR_Data::fLoadEmergencyCallData -> Node Not recognized: " + strName;
  return false;
 }
 // should not get here .....
 return true;
}

bool ADR_Data::fLoadProviderInfo(XMLNode MainNode, string* strError) {

 XMLNode	ProviderNode, ProviderIdNode, ProviderIdSeriesNode, TypeofProviderNode, ContactURInode, ProviderReferenceNode;
 XMLNode	LanguageNode, DataProviderContactNode, XCardNode, SubContractorPrincipalNode, SubContractorPriorityNode; 
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);
 extern string  extractContent(string strInput, string strStart, string strEnd);
 extern string  DetermineBoundary(string strInput);


 ProviderReferenceNode = LoadChildNode( MainNode , "DataProviderReference");
 if (ProviderReferenceNode.isEmpty()) {
  *strError = "adr.cpp fLoadProviderInfo() DataProviderReference Node is empty!"; 
  return false;
 } 

 ProviderReferenceNode = LoadChildNode( MainNode , "DataProviderReference");
 if (ProviderReferenceNode.isEmpty()) {
  *strError = "adr.cpp fLoadProviderInfo() DataProviderReference Node is empty!"; 
  return false;
 } 

 if (!this->fLoadProviderReference(&ProviderInfo.strDataProviderReference, ProviderReferenceNode, strError)) {
  *strError = "adr.cpp fLoadProviderInfo() Error Loading strDataProviderReference!"; 
  return false;
 } 
 ////cout << "ProviderInfo Provider Reference -> " << this->ProviderInfo.strDataProviderReference << endl;

 ProviderNode = LoadChildNode( MainNode , "DataProviderString");
 this->ProviderInfo.strDataProviderString.clear();
 if (!ProviderNode.isEmpty()) {
  if (ProviderNode.getText())  {
   this->ProviderInfo.strDataProviderString = ProviderNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in DataProviderString in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo DataProviderString -> " << this->ProviderInfo.strDataProviderString << endl; 

 ProviderIdNode = LoadChildNode( MainNode , "ProviderID");
 this->ProviderInfo.strProviderID.clear();
 if (!ProviderIdNode.isEmpty()) {
  if (ProviderIdNode.getText())  {
   this->ProviderInfo.strProviderID = ProviderIdNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in ProviderID in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo ProviderID -> " << this->ProviderInfo.strProviderID << endl; 

 ProviderIdSeriesNode = LoadChildNode( MainNode , "ProviderIDSeries");
 this->ProviderInfo.strProviderIDSeries.clear();
 if (!ProviderIdSeriesNode.isEmpty()) {
  if (ProviderIdSeriesNode.getText())  {
   this->ProviderInfo.strProviderIDSeries = ProviderIdSeriesNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in ProviderIDSeries in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo ProviderIDSeries -> " << this->ProviderInfo.strProviderIDSeries << endl; 

 TypeofProviderNode = LoadChildNode( MainNode , "TypeOfProvider");
 this->ProviderInfo.strTypeOfProvider.clear();
 if (!TypeofProviderNode.isEmpty()) {
  if (TypeofProviderNode.getText())  {
   this->ProviderInfo.strTypeOfProvider = TypeofProviderNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in TypeOfProvider in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo strTypeOfProvider -> " << this->ProviderInfo.strTypeOfProvider << endl; 

 ContactURInode = LoadChildNode( MainNode , "ContactURI");
 this->ProviderInfo.strContactURI.clear();
 if (!ContactURInode.isEmpty()) {
  if (ContactURInode.getText())  {
   this->ProviderInfo.strContactURI = ContactURInode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in ContactURI in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo strContactURI -> " << this->ProviderInfo.strContactURI << endl; 

 LanguageNode = LoadChildNode( MainNode , "Language");
 this->ProviderInfo.strLanguage.clear();
 if (!LanguageNode.isEmpty()) {
  if (LanguageNode.getText())  {
   this->ProviderInfo.strLanguage = LanguageNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in Language in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo strLanguage -> " << this->ProviderInfo.strLanguage << endl; 

 DataProviderContactNode = LoadChildNode( MainNode , "DataProviderContact");
 if (!DataProviderContactNode.isEmpty()) {
  XCardNode = LoadChildNode( DataProviderContactNode , "vcard");
  if (XCardNode.isEmpty()) {
   SendCodingError("adr.cpp fLoadProviderInfo() -> vcard Node is empty!");
  }
  if (!this->fLoadXcard(&ProviderInfo.vcardDataProviderContact, XCardNode, strError)) {
   *strError = "adr.cpp fLoadProviderInfo() Error Loading XCard!"; 
   return false;
  }
 }

 SubContractorPrincipalNode = LoadChildNode( MainNode , "SubcontractorPrincipal");
 this->ProviderInfo.strSubcontractorPrincipal.clear();
 if (!SubContractorPrincipalNode.isEmpty()) {
  if (SubContractorPrincipalNode.getText())  {
   this->ProviderInfo.strSubcontractorPrincipal = SubContractorPrincipalNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in SubcontractorPrincipal in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo strSubcontractorPrincipal -> " << this->ProviderInfo.strSubcontractorPrincipal << endl; 

 SubContractorPrincipalNode = LoadChildNode( MainNode , "SubcontractorPrincipal");
 this->ProviderInfo.strSubcontractorPrincipal.clear();
 if (!SubContractorPrincipalNode.isEmpty()) {
  if (SubContractorPrincipalNode.getText())  {
   this->ProviderInfo.strSubcontractorPrincipal = SubContractorPrincipalNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in SubcontractorPrincipal in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo strSubcontractorPrincipal -> " << this->ProviderInfo.strSubcontractorPrincipal << endl; 

 SubContractorPriorityNode = LoadChildNode( MainNode , "SubcontractorPriority");
 this->ProviderInfo.strSubcontractorPriority.clear();
 if (!SubContractorPriorityNode.isEmpty()) {
  if (SubContractorPriorityNode.getText())  {
   this->ProviderInfo.strSubcontractorPriority = SubContractorPriorityNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in SubcontractorPriority in fn ADR_Data::fLoadProviderInfo()");
  }
 }
 ////cout << "ProviderInfo strSubcontractorPriority -> " << this->ProviderInfo.strSubcontractorPriority << endl; 

 return true;

}


bool ADR_Data::fLoadProviderInfo(DataPacketIn objData, string* strError) {
 XMLResults	xe;
 XMLNode	MainNode, ProviderNode, ProviderIdNode, ProviderIdSeriesNode, TypeofProviderNode, ContactURInode, ProviderReferenceNode;
 XMLNode	LanguageNode, DataProviderContactNode, XCardNode, SubContractorPrincipalNode, SubContractorPriorityNode;
 string		strXMLData;
 string		strMessage, strName, strBoundary;
 size_t         found;
 char*          cptrResponse;
 string         stringXMLString ="";
/*
 string		strDataProviderReference;
 string		strDataProviderString;
 string		strProviderID;
 string		strProviderIDSeries;
 string		strTypeOfProvider;
 string		strContactURI;
 string		strLanguage;
 vcard		vcardDataProviderContact;
 string		strSubcontractorPrincipal;
 string		strSubcontractorPriority;
*/

 
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);
 extern string  extractContent(string strInput, string strStart, string strEnd);
 extern string  DetermineBoundary(string strInput);

// strBoundary = DetermineBoundary(objData.stringDataIn);
// strXMLData = extractContent(objData.stringDataIn, CONTENT_TYPE_APP_PROVIDER_INFO, strBoundary);

 found = objData.stringDataIn.find(CONTENT_TYPE_APP_PROVIDER_INFO);
 if (found == string::npos) {
  *strError = "adr.cpp CONTENT_TYPE_APP_PROVIDER_INFO: not found in fLoadProviderInfo()";
  return false;
 }

 strXMLData = objData.stringDataIn.substr(found, string::npos);

 strXMLData = RemoveXMLHeader(strXMLData);

 found = strXMLData.find("|:");
 if (found != string::npos) {
  strXMLData = strXMLData.substr(0, found);
 }

 if (strXMLData.empty()) {
  *strError = "adr.cpp fLoadProviderInfo() empty xml string !"; 
  return false;
 }
 MainNode=XMLNode::parseString(strXMLData.c_str(),NULL,&xe);
 
 if (xe.error) {
  strMessage = "adr.cpp fLoadProviderInfo() XML error -> ";
  strMessage += XMLNode::getError(xe.error);SendCodingError(strMessage);
  return false;
 }  

 if (MainNode.getName())   {strName = MainNode.getName();}
 //////cout << "Main Node Name is -> " << strName << endl;

 if (RemoveXMLnamespace(strName) != "EmergencyCallData.ProviderInfo") { 
  *strError = "adr.cpp fLoadProviderInfo() Main Node is -> " + strName; 
  return false;
 }
 
 return this->fLoadProviderInfo( MainNode, strError);

}

bool ADR_Data::fLoadServiceInfo(XMLNode MainNode, string* strError) {
 XMLNode	ServiceEnvironmentNode, ServiceTypeNode, ServiceMobilityNode, ProviderReferenceNode;
 string		strXMLData;
 string		strName;
 string         strText;
 string         strMessage;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);
 extern string  ClassofServiceFromCode(string strClassOfServiceCode);

 if (MainNode.getName())   {strName = MainNode.getName();}
 ////cout << "Main Node Name is -> " << strName << endl;

 if (RemoveXMLnamespace(strName) != "EmergencyCallData.ServiceInfo") { 
  *strError = "adr.cpp fLoadServiceInfo() Main Node is -> " + strName; 
  return false;
 }
 
 ProviderReferenceNode = LoadChildNode( MainNode , "DataProviderReference");
 if (ProviderReferenceNode.isEmpty()) {
  *strError = "adr.cpp fLoadServiceInfo() DataProviderReference Node is empty!"; 
  return false;
 } 

 if (!this->fLoadProviderReference(&ServiceInfo.strDataProviderReference, ProviderReferenceNode, strError)) {
  *strError = "adr.cpp fLoadServiceInfo() Error Loading strDataProviderReference!"; 
  // return false;
 } 
 ////cout << "ServiceInfo Provider Reference -> " << this->ServiceInfo.strDataProviderReference << endl;

 ServiceEnvironmentNode = LoadChildNode( MainNode , "ServiceEnvironment");
 this->ServiceInfo.strServiceEnvironment.clear();
 if (!ServiceEnvironmentNode.isEmpty()) {
  if (ServiceEnvironmentNode.getText())  {
   this->ServiceInfo.strServiceEnvironment = ServiceEnvironmentNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in ServiceEnvironment in fn ADR_Data::fLoadServiceInfo()");
  }
 }
 ////cout << "ServiceInfo strServiceEnvironment -> " << this->ServiceInfo.strServiceEnvironment << endl;
 
 ServiceTypeNode = LoadChildNode( MainNode , "ServiceType");
 this->ServiceInfo.strServiceType.clear();
 if (!ServiceTypeNode.isEmpty()) {
  if (ServiceTypeNode.getText())  {
   this->ServiceInfo.strServiceType = ServiceTypeNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in ServiceType in fn ADR_Data::fLoadServiceInfo()");
  }
 }
 ////cout << "ServiceInfo strServiceType -> " << this->ServiceInfo.strServiceType << endl;

 ServiceMobilityNode = LoadChildNode( MainNode , "ServiceMobility");
 this->ServiceInfo.strServiceMobility.clear();
 if (!ServiceMobilityNode.isEmpty()) {
  if (ServiceMobilityNode.getText())  {
   this->ServiceInfo.strServiceMobility = ServiceMobilityNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in ServiceMobilityNode in fn ADR_Data::fLoadServiceInfo()");
  }
 }
 ////cout << "ServiceInfo strServiceMobility -> " << this->ServiceInfo.strServiceMobility << endl;

 return true;
}


bool ADR_Data::fLoadServiceInfo(DataPacketIn objData, string* strError) {
 XMLResults	xe;
 XMLNode	MainNode, ServiceEnvironmentNode, ServiceTypeNode, ServiceMobilityNode, ProviderReferenceNode;
 string		strXMLData;
 string		strMessage, strName, strBoundary;
 size_t         found;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);
 extern string  extractContent(string strInput, string strStart, string strEnd);
 extern string  DetermineBoundary(string strInput);

// strBoundary = DetermineBoundary(objData.stringDataIn);
// strXMLData = extractContent(objData.stringDataIn, CONTENT_TYPE_APP_SERVICE_INFO, strBoundary);
// strXMLData = RemoveXMLHeader(strXMLData);

 found = objData.stringDataIn.find(CONTENT_TYPE_APP_SERVICE_INFO);
 if (found == string::npos) {
  *strError = "adr.cpp CONTENT_TYPE_APP_SERVICE_INFO: not found in fLoadServiceInfo()";
  return false;
 }

 strXMLData = objData.stringDataIn.substr(found, string::npos);

 strXMLData = RemoveXMLHeader(strXMLData);

 found = strXMLData.find("|:");
 if (found != string::npos) {
  strXMLData = strXMLData.substr(0, found);
 }

 if (strXMLData.empty()) {
  *strError = "adr.cpp fLoadServiceInfo() empty string !"; 
  return false;
 }
 MainNode=XMLNode::parseString(strXMLData.c_str(),NULL,&xe);
 
 if (xe.error) {
  strMessage = "ADR_Data::fLoadServiceInfo() XML error -> ";
  strMessage += XMLNode::getError(xe.error);SendCodingError(strMessage);
  return false;
 }  

 return this->fLoadServiceInfo(MainNode, strError);

}

bool ADR_Data::fLoadSubscriberInfo(XMLNode MainNode, string* strError) {

 XMLNode	fnNode, NoteNode, TelNode, XCardNode, SubscriberDataNode, ProviderReferenceNode;
 string         strName;
 string         strPrivacy;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

 if (MainNode.getName())   {strName = MainNode.getName();}
 ////cout << "Main Node Name is -> " << strName << endl;

 if (RemoveXMLnamespace(strName) != "EmergencyCallData.SubscriberInfo") { 
  *strError = "adr.cpp fLoadSubscriberInfo() Main Node is -> " + strName; 
  return false;
 }

 if (MainNode.getAttribute("privacyRequested")) {
  strPrivacy = MainNode.getAttribute("privacyRequested");
  char2bool(this->SubscriberInfo.boolPrivacyRequested, strPrivacy);
 }

 ProviderReferenceNode = LoadChildNode( MainNode , "DataProviderReference");
 if (ProviderReferenceNode.isEmpty()) {
  *strError = "adr.cpp fLoadSubscriberInfo() DataProviderReference Node is empty!"; 
  return false;
 } 

 if (!this->fLoadProviderReference(&SubscriberInfo.strDataProviderReference, ProviderReferenceNode, strError)) {
  *strError = "adr.cpp fLoadSubscriberInfo() Error Loading strDataProviderReference!"; 
  return false;
 } 
 ////cout << "SubscriberInfo Provider Reference -> " << this->SubscriberInfo.strDataProviderReference << endl;

 SubscriberDataNode = LoadChildNode( MainNode , "SubscriberData");
 if (SubscriberDataNode.isEmpty()) {
  *strError = "adr.cpp fLoadSubscriberInfo() SubscriberData Node is empty!"; 
  return false;
 }

 ////cout << "Load vcard" << endl;
 XCardNode = LoadChildNode( SubscriberDataNode , "vcard");
 if (XCardNode.isEmpty()) {
  *strError = "adr.cpp fLoadSubscriberInfo() XCardNode Node is empty!"; 
  return false;
 }

 if (!this->fLoadXcard(&SubscriberInfo.vcardSubscriberData, XCardNode, strError)) {
  *strError = "adr.cpp fLoadSubscriberInfo() Error Loading XCard!"; 
  return false;
 }

 return true;

}

bool ADR_Data::fLoadSubscriberInfo(DataPacketIn objData, string* strError) {
 XMLResults	xe;
 XMLNode	MainNode, fnNode, NoteNode, TelNode, XCardNode, SubscriberDataNode, ProviderReferenceNode;
 string		strXMLData;
 string		strMessage, strName, strBoundary;
 size_t		found;
 char*		cptrResponse;
 string		stringXMLString = "";
 string         strPrivacy;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);
 extern string  extractContent(string strInput, string strStart, string strEnd);
 extern string  DetermineBoundary(string strInput);

// strBoundary = DetermineBoundary(objData.stringDataIn);
// strXMLData = extractContent(objData.stringDataIn, CONTENT_TYPE_APP_SUBSCRIBER_INFO, strBoundary);
// strXMLData = RemoveXMLHeader(strXMLData);

 this->SubscriberInfo.boolPrivacyRequested = false;
 this->SubscriberInfo.strDataProviderReference.clear();
 this->SubscriberInfo.vcardSubscriberData.fClear();
 
 found = objData.stringDataIn.find(CONTENT_TYPE_APP_SUBSCRIBER_INFO);
 if (found == string::npos) {
  *strError = "adr.cpp CONTENT_TYPE_APP_SUBSCRIBER_INFO: not found in fLoadSubscriberInfo()";
  return false;
 }

 strXMLData = objData.stringDataIn.substr(found, string::npos);

 strXMLData = RemoveXMLHeader(strXMLData);

 found = strXMLData.find("|:");
 if (found != string::npos) {
  strXMLData = strXMLData.substr(0, found);
 }

 if (strXMLData.empty()) {
  *strError = "adr.cpp fLoadSubscriberInfo() empty string !"; 
  return false;
 }
 MainNode=XMLNode::parseString(strXMLData.c_str(),NULL,&xe);
 
 if (xe.error) {
  strMessage = "ADR_Data::fLoadSubscriberInfo() XML error -> ";
  strMessage += XMLNode::getError(xe.error);SendCodingError(strMessage);
  return false;
 }  

 return fLoadSubscriberInfo(MainNode, strError);

}

bool ADR_Data::fLoadProviderReference(string* strProviderReference, XMLNode objProviderReferenceNode, string* strError) {
  strProviderReference->clear();
  if (objProviderReferenceNode.getText())  {
   *strProviderReference = objProviderReferenceNode.getText();
  } 
  else {
   SendCodingError("adr.cpp: No Text in DataProviderReference in fn ADR_Data::fLoadProviderReference()");
   return false;
  }
 return true;
}

bool ADR_Data::fLoadXcard(vcard* VCard, XMLNode objXCardNode, string* strError) {
 XMLNode	Node1, Node2, Node3, Node4, Node5, Node6, Node7, Node8, Node9, ParameterNode;
 XMLNode	Node10, Node11, Node12, Node13, Node14, Node15, Node16, Node17, Node18, Node19;
// char*         	cptrResponse = NULL;
// string         stringTemp;
 
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

/*
 cptrResponse = objXCardNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/

  //Load uid 0 or 1
  Node1 = LoadChildNode(objXCardNode, "uid");
  if (!Node1.isEmpty()) {
   VCard->uid.fLoadUID(Node1);
  }  

  //Load kind 0 or 1
  Node2 = LoadChildNode(objXCardNode, "kind");
  if (!Node2.isEmpty()) {
   VCard->kind.fLoadKind(Node2);
  }  

  //Load fn 
  // NENA Load only 1
//////cout << "load fn" << endl;
  Node3 = LoadChildNode(objXCardNode, "fn");
  if (!Node3.isEmpty()) {
   VCard->fullname.fLoadFullName(Node3);
  }
//////cout << "load n" << endl;
  //Load n
  Node4 = LoadChildNode(objXCardNode, "n");
  if (!Node4.isEmpty()) {
   VCard->name.fLoadName(Node4);
  }
//////cout << "load nickname" << endl;
 //Load nickname
 VCard->fLoadNicknames(objXCardNode.deepCopy());
//////cout << "load Gender" << endl;
 //Load Gender
  Node6 = LoadChildNode(objXCardNode, "gender");
  if (!Node6.isEmpty()) {
   VCard->gender.fLoadGender(Node6);
  }
//////cout << "load bday" << endl;
 //Load bday
  Node7 = LoadChildNode(objXCardNode, "bday");
  if (!Node7.isEmpty()) {
   VCard->birthday.fLoadBday(Node7);
  }
//////cout << "load anniversary" << endl;
 //Load anniversary
  Node8 = LoadChildNode(objXCardNode, "anniversary");
  if (!Node8.isEmpty()) {
   VCard->anniversary.fLoadAnniversary(Node8);
  }
//////cout << "load Lang" << endl;
 //Load Lang
  VCard->lang.fLoadLang(objXCardNode.deepCopy());
//////cout << "load org" << endl;
 //Load org
  Node9 = LoadChildNode(objXCardNode, "org");
  if (!Node9.isEmpty()) {
   VCard->org.fLoadOrg(Node9);
  }
//////cout << "load title" << endl;   
  //Load title
  VCard->fLoadTitle(objXCardNode.deepCopy());
//////cout << "load role" << endl;   
  //Load role
  VCard->fLoadRole(objXCardNode.deepCopy());
//////cout << "load photo" << endl;   
 //Load photo
  VCard->fLoadPhoto(objXCardNode.deepCopy());
//////cout << "load email" << endl;   
 //Load email
  VCard->fLoadEmail(objXCardNode.deepCopy());
//////cout << "load Impp" << endl;   
 //Load Impp
  VCard->fLoadImpp(objXCardNode.deepCopy());
//////cout << "load geo" << endl;   
 //Load geo
 // .... even though this is included in </adr> Intrado and Nena put it here also (limit 1)
  Node10 = LoadChildNode(objXCardNode, "geo");
  if (!Node10.isEmpty()) {
   VCard->Geo.fLoadGeo(Node10);
  }

 // TZ ignored by NENA

 // key ignored by NENA

 // url ignored by NENA
//////cout << "load Rev" << endl;   
 // Load Rev
  Node11 = LoadChildNode(objXCardNode, "rev");
  if (!Node11.isEmpty()) {
   VCard->Rev.fLoadRev(Node11);
  }
//////cout << "load Note" << endl;   
 //Load Note
  VCard->fLoadNote(objXCardNode.deepCopy());
//////cout << "load Related" << endl;   
 //Load Related
  VCard->fLoadRelated(objXCardNode.deepCopy());
// ////cout << "load Tel " << endl;
 //Load Tel
  VCard->fLoadTel(objXCardNode.deepCopy());
// ////cout << "load Adr " << endl;
 //Load Adr 
 // (.... note: Intrado does not seem to send this field ....)
  VCard->fLoadAddresses(objXCardNode.deepCopy());




 return true;
}

bool vc_uid::fLoadUID(XMLNode UIDNode) {
 XMLNode	URINode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 URINode = LoadChildNode(UIDNode, "uri");
 if (!URINode.isEmpty()) {
  if (URINode.getText())  {
    this->URI.strURI = URINode.getText();
  } 
 } 

 return true;
}

bool vc_kind::fLoadKind(XMLNode KindNode) {
 XMLNode	TextNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 TextNode = LoadChildNode(KindNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
   this->Text.strText = TextNode.getText();
  } 
 } 
 return true;
}

bool vc_note::fLoadNote(XMLNode NoteNode) {
 XMLNode	TextNode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(NoteNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 TextNode = LoadChildNode(NoteNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
   this->Text.strText = TextNode.getText();
  } 
 } 
 return true;
}


//Constuctor
vcard::vcard() {
 uid.fClear();
 kind.fClear();
 name.fClear();
 fullname.fClear();
 vNickname.clear();
 gender.fClear();
 vTitle.clear();
 vRole.clear();
 birthday.fClear();
 anniversary.fClear();
 lang.fClear();
 org.fClear();
 vPhoto.clear();
 vNote.clear();
 vEmail.clear();
 Geo.fClear();
 TZ.fClear();  // ignored
 vTel.clear();
 vAdr.clear();
 vImpp.clear();
 vRelated.clear();
 Rev.fClear();
}
//copy constuctor
vcard::vcard(const vcard *a) {
 uid				= a->uid;
 kind				= a->kind;
 name				= a->name;
 fullname			= a->fullname;
 vNickname			= a->vNickname;
 gender				= a->gender;
 vTitle				= a->vTitle;
 vRole				= a->vRole;
 birthday			= a->birthday;
 anniversary			= a->anniversary;
 lang				= a->lang;
 org				= a->org;
 vPhoto				= a->vPhoto;
 vNote				= a->vNote;
 vEmail				= a->vEmail;
 Geo				= a->Geo;
 TZ				= a->TZ;
 vTel				= a->vTel;
 vAdr				= a->vAdr;
 vImpp				= a->vImpp;
 vRelated			= a->vRelated;
 Rev				= a->Rev;
}
//assignment operator
vcard& vcard::operator=(const vcard& a) {
 if (&a == this ) {return *this;}
 uid				= a.uid;
 kind				= a.kind;
 name				= a.name;
 fullname			= a.fullname;
 vNickname			= a.vNickname;
 gender				= a.gender;
 vTitle				= a.vTitle;
 vRole				= a.vRole;
 birthday			= a.birthday;
 anniversary			= a.anniversary;
 lang				= a.lang;
 org				= a.org;
 vPhoto				= a.vPhoto;
 vNote				= a.vNote;
 vEmail				= a.vEmail;
 Geo				= a.Geo;
 TZ				= a.TZ;
 vTel				= a.vTel;
 vAdr				= a.vAdr;
 vImpp				= a.vImpp;
 vRelated			= a.vRelated;
 Rev				= a.Rev;
 return *this;
}

void vcard::fClear() {
 uid.fClear();
 kind.fClear();
 name.fClear();
 fullname.fClear();
 vNickname.clear();
 gender.fClear();
 vTitle.clear();
 vRole.clear();
 birthday.fClear();
 anniversary.fClear();
 lang.fClear();
 org.fClear();
 vPhoto.clear();
 vNote.clear();
 vEmail.clear();
 Geo.fClear();
 TZ.fClear();  // ignored
 vTel.clear();
 vAdr.clear();
 vImpp.clear();
 vRelated.clear();
 Rev.fClear();

}

int vcard::fMainTelnumber() {
size_t sz;
size_t TLsz;
bool   bMainNumber = false;

extern bool ISmain( string strInput);

 sz = vTel.size();

 if (sz == 0) {return -1;}

 for (unsigned int i = 0; i < sz; i++) {

  TLsz = this->vTel[i].Parameters.Type.TypeList.vTextList.size();
  
  for (unsigned int j = 0; j < TLsz; j++) { 
   if ((ISmain(this->vTel[i].Parameters.Type.TypeList.vTextList[j])) && (this->vTel[i].URI.strURI.length()) )    {return i;}
  }

 } 
 return -1;
} 

bool vcard::fLoadNote(XMLNode MainNode) {
 XMLNode	CurrentNode, NoteNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_note	objNote;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->vNote.clear();
 NoteNode = LoadChildNode(MainNode, "note");
 if (NoteNode.isEmpty()) {
  return false;
 } 
 strName = NoteNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objNote.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "note", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:note "+ int2str(i) + " empty in fn vcard::fLoadNote()");
   continue;
  }   
  if (objNote.fLoadNote(CurrentNode)) {
   this->vNote.push_back(objNote);
   ////cout << "push back Note -> " << objNote.Text.strText << endl;
  }
 }
 return true;
}

bool vc_impp::fLoadImpp(XMLNode ImppNode) {
 XMLNode	URINode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(ImppNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 URINode = LoadChildNode(ImppNode, "uri");
 if (!URINode.isEmpty()) {
  if (URINode.getText())  {
    this->URI.strURI = URINode.getText();
  } 
 } 

 return true;
}

bool vcard::fLoadImpp(XMLNode MainNode) {
 XMLNode	CurrentNode, ImppNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_impp	objImpp;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->vImpp.clear();
 ImppNode = LoadChildNode(MainNode, "impp");
 if (ImppNode.isEmpty()) {
  return false;
 } 
 strName = ImppNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objImpp.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "impp", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:impp "+ int2str(i) + " empty in fn vcard::fLoadImpp()");
   continue;
  }   
  if (objImpp.fLoadImpp(CurrentNode)) {
   this->vImpp.push_back(objImpp);
   ////cout << "push back impp -> " << objImpp.URI.strURI << endl;
  }
 }
 return true;
}


bool vc_geo::fLoadGeo(XMLNode GeoNode) {
 XMLNode	URINode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(GeoNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 URINode = LoadChildNode(GeoNode, "uri");
 if (!URINode.isEmpty()) {
  if (URINode.getText())  {
   this->URI.strURI = URINode.getText();
   ////cout << "Loaded geo -> " << this->URI.strURI << endl;
  } 
 } 

 return true;
}

/*
bool vcard::fLoadGeo(XMLNode MainNode) {
 XMLNode	CurrentNode, GeoNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_geo	objGeo;
//
//  note NENA allows 0 or 1 Geo but Xcard allows N
//
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->vGeo.clear();
 GeoNode = LoadChildNode(MainNode, "geo");
 if (GeoNode.isEmpty()) {
  return false;
 } 
 strName = GeoNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objGeo.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "geo", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:geo "+ int2str(i) + " empty in fn vcard::fLoadGeo()");
   continue;
  }   
  if (objGeo.fLoadGeo(CurrentNode)) {
   this->vGeo.push_back(objGeo);
   ////cout << "push back geo -> " << objGeo.URI.strURI << endl;
  }
 }
 return true;
}
*/

bool vc_email::fLoadEmail(XMLNode EmailNode) {
 XMLNode	TextNode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(EmailNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 TextNode = LoadChildNode(EmailNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
    this->Text.strText = TextNode.getText();
  } 
 } 

 return true;
}

bool vcard::fLoadEmail(XMLNode MainNode) {
 XMLNode	CurrentNode, EmailNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_email	objEmail;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 EmailNode = LoadChildNode(MainNode, "email");
 if (EmailNode.isEmpty()) {
  return false;
 } 
 strName = EmailNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objEmail.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "email", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:email "+ int2str(i) + " empty in fn vcard::fLoadEmail()");
   continue;
  }   
  if (objEmail.fLoadEmail(CurrentNode)) {
   this->vEmail.push_back(objEmail);
   ////cout << "push back email -> " << objEmail.Text.strText << endl;
  }
 }
 return true;
}

bool vc_photo::fLoadPhoto(XMLNode PhotoNode) {
 XMLNode	URINode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(PhotoNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 URINode = LoadChildNode(PhotoNode, "uri");
 if (!URINode.isEmpty()) {
  if (URINode.getText())  {
    this->URI.strURI = URINode.getText();
  } 
 } 

 return true;
}


bool vcard::fLoadPhoto(XMLNode MainNode) {
 XMLNode	CurrentNode, PhotoNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_photo	objPhoto;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->vPhoto.clear();
 PhotoNode = LoadChildNode(MainNode, "photo");
 if (PhotoNode.isEmpty()) {
  return false;
 } 
 strName = PhotoNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objPhoto.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "photo", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:photo "+ int2str(i) + " empty in fn vcard::fLoadPhoto()");
   continue;
  }   
  if (objPhoto.fLoadPhoto(CurrentNode)) {
   this->vPhoto.push_back(objPhoto);
   ////cout << "push back photo -> " << objPhoto.URI.strURI << endl;
  }
 }
 return true;
}

bool vc_role::fLoadRole(XMLNode RoleNode) {
 XMLNode	TextNode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(RoleNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 TextNode = LoadChildNode(RoleNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
    this->Text.strText = TextNode.getText();
  } 
 } 

 return true;
}

bool vcard::fLoadRole(XMLNode MainNode) {
 XMLNode	CurrentNode, RoleNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_role	objRole;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 RoleNode = LoadChildNode(MainNode, "role");
 if (RoleNode.isEmpty()) {
  return false;
 } 
 strName = RoleNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objRole.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "role", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:role "+ int2str(i) + " empty in fn vcard::fLoadRole()");
   continue;
  }   
  if (objRole.fLoadRole(CurrentNode)) {
   this->vRole.push_back(objRole);
   ////cout << "push back role -> " << objRole.Text.strText << endl;
  }
 }
 return true;
}

bool vc_title::fLoadTitle(XMLNode TitleNode) {
 XMLNode	TextNode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(TitleNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 TextNode = LoadChildNode(TitleNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
    this->Text.strText = TextNode.getText();
  } 
 } 

 return true;
}

bool vcard::fLoadTitle(XMLNode MainNode) {
 XMLNode	CurrentNode, TitleNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_title	objTitle;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 TitleNode = LoadChildNode(MainNode, "title");
 if (TitleNode.isEmpty()) {
  return false;
 } 
 strName = TitleNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objTitle.fClear(); 
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "title", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:title "+ int2str(i) + " empty in fn vcard::fLoadTitle()");
   continue;
  }   
  if (objTitle.fLoadTitle(CurrentNode)) {
   this->vTitle.push_back(objTitle);
   ////cout << "push back title -> " << objTitle.Text.strText << endl;
  }
 }
 return true;
}

void vc_org::fClear() {
 Parameters.fClear();
 Org.fClear();
}

bool vc_org::fLoadOrg(XMLNode OrgNode) {
 XMLNode	TextNode, ParameterNode; 
 int 		iNumChildNodes;
 string         strName;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->Parameters.fClear();
 ParameterNode = LoadChildNode(OrgNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 this->Org.strText.clear();
 this->Org.vTextList.clear();

 TextNode = LoadChildNode(OrgNode, "text");
 if (TextNode.isEmpty()) {
  return false;
 } 
 strName = TextNode.getName();

 iNumChildNodes = OrgNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  TextNode = XMLNode::emptyNode();
  TextNode = LoadChildNode(OrgNode, "text", i);
  if (!TextNode.isEmpty()) {
   if (TextNode.getText())  {
    this->Org.strText = TextNode.getText();
    this->Org.vTextList.push_back(this->Org.strText);
    ////cout << "add ORG ->" << this->Org.strText << endl;
   }
  }
 }
 return true;
}

void vc_lang::fClear() {
 vParameters.clear();
 vLanguageTag.clear();
}

bool vc_lang::fLoadLang(XMLNode MainNode) {
 XMLNode		CurrentNode, LangNode, ParameterNode; 
 int			iNumChildNodes;
 string 		strName;
 vc_parameters 		objParameters;
 vc_value_language_tag 	objLanguageTag;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 LangNode = LoadChildNode(MainNode, "lang");
 if (LangNode.isEmpty())  {return false;}
 strName = LangNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());

  this->vLanguageTag.clear();
  this->vParameters.clear();

  for (int i = 1; i <= iNumChildNodes; i++) {
   CurrentNode = XMLNode::emptyNode();
   CurrentNode = LoadChildNode(MainNode, "lang", i);
   if(CurrentNode.isEmpty()) {
    SendCodingError("adr.cpp: CurrentNode:lang "+ int2str(i) + " empty in fn vc_lang:fLoadLang()");
    continue;
   } 
   else {
    ParameterNode = XMLNode::emptyNode();    
    ParameterNode = LoadChildNode(CurrentNode, "parameters");
    if (!ParameterNode.isEmpty()) {
     if (objParameters.fLoadParameters(ParameterNode)) {
      this->vParameters.push_back(objParameters);
      ////cout << "push back lang.param -> " << i << endl;
     }
    }
    if (objLanguageTag.fLoadLanguageTag(CurrentNode)) {
      this->vLanguageTag.push_back(objLanguageTag);
      ////cout << "push back lang.tag   -> " << i << endl;
    }           
   } 
  } 

 return true;
}

bool vc_rev::fLoadRev(XMLNode RevNode) {
 XMLNode	TimeStampNode;
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 TimeStampNode = LoadChildNode(RevNode, "timestamp");
 if (TimeStampNode.isEmpty()) {
  SendCodingError("adr.cpp: No timestamp node fn vc_rev:fLoadRev()");
  return false;
 }
 if (TimeStampNode.getText())  {
    this->TimeStamp.strTimeStamp = TimeStampNode.getText();
    ////cout << "add Rev Timestamp ->" << this->TimeStamp.strTimeStamp << endl; 
 } 
 else {
  SendCodingError("adr.cpp: No timestamp in fn vc_rev:fLoadRev()");
  return false;
 }

 return true;
}
void vc_anniversary::fClear() {
 Parameters.fClear(); 
 Date.fClear();
 DateTime.fClear();
 Time.fClear();
 Text.fClear();
}

bool vc_anniversary::fLoadAnniversary(XMLNode AnvNode) {
 XMLNode	DateNode, TextNode, DateTimeNode, TimeNode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 ParameterNode = LoadChildNode(AnvNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }
 
 this->Date.strDate.clear();
 DateNode = LoadChildNode(AnvNode, "date");
 if (!DateNode.isEmpty()) {
  if (DateNode.getText())  {
    this->Date.strDate = DateNode.getText();
    ////cout << "add ANV date ->" << this->Date.strDate << endl;
  } 
 } 

 this->DateTime.strDateTime.clear();
 DateTimeNode = LoadChildNode(AnvNode, "date-time");
 if (!DateTimeNode.isEmpty()) {
  if (DateTimeNode.getText())  {
    this->DateTime.strDateTime = DateTimeNode.getText();
    ////cout << "add ANV date-time ->" << this->DateTime.strDateTime << endl;
  } 
 } 

 this->Time.strTime.clear();
 TimeNode = LoadChildNode(AnvNode, "time");
 if (!TimeNode.isEmpty()) {
  if (TimeNode.getText())  {
    this->Time.strTime = TimeNode.getText();
    ////cout << "add ANV time ->" << this->Time.strTime << endl;
  } 
 } 

 this->Text.strText.clear();
 TextNode = LoadChildNode(AnvNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
    this->Text.strText = TextNode.getText();
    ////cout << "add ANV text ->" << this->Text.strText << endl;
  } 
 } 

 return true;
}

void vc_tz::fClear() {
 Parameters.fClear();
 Text.fClear();
 URI.fClear();
 UTCoffset.fClear();
}


void vc_bday::fClear() {
 Parameters.fClear(); 
 Date.fClear();
 DateTime.fClear();
 Time.fClear();
 Text.fClear();
}

bool vc_bday::fLoadBday(XMLNode BdayNode) {
 XMLNode	DateNode, TextNode, DateTimeNode, TimeNode, ParameterNode; 

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 ParameterNode = LoadChildNode(BdayNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }
 
 this->Date.strDate.clear();
 DateNode = LoadChildNode(BdayNode, "date");
 if (!DateNode.isEmpty()) {
  if (DateNode.getText())  {
    this->Date.strDate = DateNode.getText();
    ////cout << "add bday date ->" << this->Date.strDate << endl;
  } 
 } 

 this->DateTime.strDateTime.clear();
 DateTimeNode = LoadChildNode(BdayNode, "date-time");
 if (!DateTimeNode.isEmpty()) {
  if (DateTimeNode.getText())  {
    this->DateTime.strDateTime = DateTimeNode.getText();
    ////cout << "add bday date-time ->" << this->DateTime.strDateTime << endl;
  } 
 } 

 this->Time.strTime.clear();
 TimeNode = LoadChildNode(BdayNode, "time");
 if (!TimeNode.isEmpty()) {
  if (TimeNode.getText())  {
    this->Time.strTime = TimeNode.getText();
    ////cout << "add bday time ->" << this->Time.strTime << endl;
  } 
 } 

 this->Text.strText.clear();
 TextNode = LoadChildNode(BdayNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
    this->Text.strText = TextNode.getText();
    ////cout << "add bday text ->" << this->Text.strText << endl;
  } 
 } 



 return true;
}

void vc_gender::fClear() {
 strSex.clear();
 strIdentity.clear();
}

bool vc_gender::fLoadGender(XMLNode GenderNode) {
 XMLNode	SexNode, IndentityNode; 
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->strSex.clear();
 SexNode = LoadChildNode(GenderNode, "sex");
 if (!SexNode.isEmpty()) {
  if (SexNode.getText())  {
    this->strSex = SexNode.getText();
    ////cout << "add sex ->" << this->strSex << endl;
   } 
 } 

 this->strIdentity.clear();
 IndentityNode = LoadChildNode(GenderNode, "identity");
 if (!IndentityNode.isEmpty()) {
  if (IndentityNode.getText())  {
    this->strIdentity = IndentityNode.getText();
    ////cout << "add identity ->" << this->strIdentity << endl;
   } 
 } 

 return true;
}

bool vc_nickname::fLoadName(XMLNode NameNode) {
 XMLNode	TextNode, ParameterNode; 
 string         strName;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 ParameterNode = LoadChildNode(NameNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 TextNode = LoadChildNode(NameNode, "text");
 if (TextNode.isEmpty()) {
  SendCodingError("adr.cpp: No Text Node in fn vc_nickname:fLoadName()"); 
  return false;
 } 

 if (TextNode.getText())  {
  this->NicknameList.strText = TextNode.getText();
  ////cout << "add nickname ->" << this->NicknameList.strText << endl;
 }
 else {
  SendCodingError("adr.cpp: No Text in fn vc_nickname:fLoadName()"); 
  return false;
 }

 return true;
}

bool vcard::fLoadNicknames(XMLNode MainNode) {
 XMLNode	CurrentNode, NickNameNode; 
 int 		iNumChildNodes;
 string         strName;
 vc_nickname    objNickname;
 
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->vNickname.clear();

 NickNameNode = LoadChildNode(MainNode, "nickname");
 if (NickNameNode.isEmpty()) {
  return false;
 } 
 strName = NickNameNode.getName();

 iNumChildNodes = MainNode.nChildNode(strName.c_str());
 for (int i = 1; i <= iNumChildNodes; i++) {
  objNickname.fClear();
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(MainNode, "nickname", i);
  if (CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode:given "+ int2str(i) + " empty in fn vc_name:fLoadName()");
   continue;
  }
  else {
   if (objNickname.fLoadName(CurrentNode)) {
    this->vNickname.push_back(objNickname);
   }
  }
 }
 return true;
}

void vc_name::fClear() {
 Parameters.fClear(); 
 vSurname.clear();
 vGiven.clear();
 vAdditional.clear();
 vPrefix.clear();
 vSuffix.clear();

}

bool vc_name::fLoadName(XMLNode NameNode) {
 XMLNode	CurrentNode, SurnameNode, GivenNode, AdditionalNode, PrefixNode, SuffixNode, ParameterNode;
 string		strName, strData;
 int		iNumChildNodes;
 
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

 ParameterNode = LoadChildNode(NameNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 this->vSurname.clear();
 SurnameNode = LoadChildNode(NameNode, "surname");
 if (!SurnameNode.isEmpty()) {
  strName = SurnameNode.getName();
  iNumChildNodes = NameNode.nChildNode(strName.c_str());

  for (int i = 1; i <= iNumChildNodes; i++) {
   CurrentNode = XMLNode::emptyNode();
   CurrentNode = LoadChildNode(NameNode, "surname", i);
   if(CurrentNode.isEmpty()) {
    SendCodingError("adr.cpp: CurrentNode:surname "+ int2str(i) + " empty in fn vc_name:fLoadName()");
    continue;
   } 
   else {
    if (CurrentNode.getText()) { 
     strData = CurrentNode.getText();
     this->vSurname.push_back(strData);
     ////cout << "Surname -> " << strData << endl;
    }
   }
  } 
 } 

 this->vGiven.clear();
 GivenNode = LoadChildNode(NameNode, "given");
 if (!GivenNode.isEmpty()) {
  strName = GivenNode.getName();
  iNumChildNodes = NameNode.nChildNode(strName.c_str());

  for (int i = 1; i <= iNumChildNodes; i++) {
   CurrentNode = XMLNode::emptyNode();
   CurrentNode = LoadChildNode(NameNode, "given", i);
   if(CurrentNode.isEmpty()) {
    SendCodingError("adr.cpp: CurrentNode:given "+ int2str(i) + " empty in fn vc_name:fLoadName()");
    continue;
   } 
   else {
    if (CurrentNode.getText()) { 
     strData = CurrentNode.getText();
     this->vGiven.push_back(strData);
     ////cout << "Given -> " << strData << endl;
    }
   }
  } 
 } 

 this->vAdditional.clear();
 AdditionalNode = LoadChildNode(NameNode, "additional");
 if (!AdditionalNode.isEmpty()) {
  strName = AdditionalNode.getName();
  iNumChildNodes = NameNode.nChildNode(strName.c_str());

  for (int i = 1; i <= iNumChildNodes; i++) {
   CurrentNode = XMLNode::emptyNode();
   CurrentNode = LoadChildNode(NameNode, "additional", i);
   if(CurrentNode.isEmpty()) {
    SendCodingError("adr.cpp: CurrentNode:additional "+ int2str(i) + " empty in fn vc_name:fLoadName()");
    continue;
   } 
   else {
    if (CurrentNode.getText()) { 
     strData = CurrentNode.getText();
     this->vAdditional.push_back(strData);
     ////cout << "Additional -> " << strData << endl;
    }
   }
  } 
 } 

 this->vPrefix.clear();
 PrefixNode = LoadChildNode(NameNode, "prefix");
 if (!PrefixNode.isEmpty()) {
  strName = PrefixNode.getName();
  iNumChildNodes = NameNode.nChildNode(strName.c_str());

  for (int i = 1; i <= iNumChildNodes; i++) {
   CurrentNode = XMLNode::emptyNode();
   CurrentNode = LoadChildNode(NameNode, "prefix", i);
   if(CurrentNode.isEmpty()) {
    SendCodingError("adr.cpp: CurrentNode:prefix "+ int2str(i) + " empty in fn vc_name:fLoadName()");
    continue;
   } 
   else {
    if (CurrentNode.getText()) { 
     strData = CurrentNode.getText();
     this->vPrefix.push_back(strData);
     ////cout << "Prefix -> " << strData << endl;
    }
   }
  } 
 } 

 this->vSuffix.clear();
 SuffixNode = LoadChildNode(NameNode, "suffix");
 if (!SuffixNode.isEmpty()) {
  strName = SuffixNode.getName();
  iNumChildNodes = NameNode.nChildNode(strName.c_str());

  for (int i = 1; i <= iNumChildNodes; i++) {
   CurrentNode = XMLNode::emptyNode();
   CurrentNode = LoadChildNode(NameNode, "suffix", i);
   if(CurrentNode.isEmpty()) {
    SendCodingError("adr.cpp: CurrentNode:suffix "+ int2str(i) + " empty in fn vc_name:fLoadName()");
    continue;
   } 
   else {
    if (CurrentNode.getText()) { 
     strData = CurrentNode.getText();
     this->vSuffix.push_back(strData);
     ////cout << "Suffix -> " << strData << endl;
    }
   }
  } 
 } 
 return true;
}

bool vc_related::fLoadRelated(XMLNode objNode) {
 XMLNode	UriNode, TextNode, ParameterNode;
// char*         	cptrResponse = NULL;
// string         stringTemp;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

/*
 cptrResponse = objNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/
 ParameterNode = LoadChildNode(objNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 UriNode = LoadChildNode(objNode, "uri");
 if (!UriNode.isEmpty()) {
  if (UriNode.getText())  { 
   this->URI.strURI = UriNode.getText();
   ////cout << "loaded Related URI -> " << this->URI.strURI << endl; 
  }
  else {
   SendCodingError("adr.cpp: No Related URI in fn vc_related::fLoadRelated()");
   return false; 
  }
 }
 else {
  // check for text node
  TextNode = LoadChildNode(objNode, "text");
  if (TextNode.isEmpty()) {
   SendCodingError("adr.cpp: No Related URI Node in fn vc_related::fLoadRelated()");
   return false;
  }
  if (TextNode.getText())  {  
   this->Text.strText = TextNode.getText();
   ////cout << "loaded Related Text -> " << this->Text.strText << endl;    
  }
  else {
   SendCodingError("adr.cpp: No Related Text in fn vc_related::fLoadRelated()");
   return false;   
  }
 }
 return true;
}

bool vcard::fLoadRelated(XMLNode objNode) {
 XMLNode	RelNode, UriNode, ParameterNode, CurrentNode;
 int 		iNumChildNodes;
 string         strName;
 vc_related     objRel;
// char*         	cptrResponse = NULL;
// string         stringTemp;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

/*
 cptrResponse = objNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/
 this->vRelated.clear();

 RelNode = LoadChildNode(objNode, "related");
 if (RelNode.isEmpty()) {
  SendCodingError("adr.cpp: Empty related Node fn vcard::fLoadRelated()"); 
  return false;
 } 

 strName = RelNode.getName();

 iNumChildNodes = objNode.nChildNode(strName.c_str());
 //////cout << "number of tel Nodes is " << iNumChildNodes << endl;;
 for (int i = 1; i <= iNumChildNodes; i++) {
  objRel.fClear();
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(objNode, "related", i);

  if(CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode "+ int2str(i) + " empty in fn vcard::fLoadRelated()");
   continue;
  } 

  if (objRel.fLoadRelated(CurrentNode.deepCopy())) { 
   this->vRelated.push_back(objRel);
  }
 }
 return true;
}


bool vc_tel::fLoadTel(XMLNode objNode) {
 XMLNode	UriNode, TextNode, ParameterNode;
// char*         	cptrResponse = NULL;
// string         stringTemp;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

/*
 cptrResponse = objNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/
 ParameterNode = LoadChildNode(objNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

 UriNode = LoadChildNode(objNode, "uri");
 if (!UriNode.isEmpty()) {
  if (UriNode.getText())  { 
   this->URI.strURI = UriNode.getText();
   ////cout << "loaded URI -> " << this->URI.strURI << endl; 
  }
  else {
   SendCodingError("adr.cpp: No Tel URI in fn vc_tel::fLoadTel()");
   return false; 
  }
 }
 else {
  // check for text node
  TextNode = LoadChildNode(objNode, "text");
  if (TextNode.isEmpty()) {
   SendCodingError("adr.cpp: No Tel URI Node in fn vc_tel::fLoadTel()");
   return false;
  }
  if (TextNode.getText())  {  
   this->Text.strText = TextNode.getText();
   ////cout << "loaded Text -> " << this->Text.strText << endl;    
  }
  else {
   SendCodingError("adr.cpp: No Tel Text in fn vc_tel::fLoadTel()");
   return false;   
  }
 }
 return true;
}

bool vcard::fLoadTel(XMLNode objNode) {
 XMLNode	TelNode, UriNode, ParameterNode, CurrentNode;
 int 		iNumChildNodes;
 string         strName;
 vc_tel         objTel;
// char*         	cptrResponse = NULL;
// string         stringTemp;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

/*
 cptrResponse = objNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/
 this->vTel.clear();

 TelNode = LoadChildNode(objNode, "tel");
 if (TelNode.isEmpty()) {
  SendCodingError("adr.cpp: Empty Tel Node fn vcard::fLoadTel()"); 
  return false;
 } 

 strName = TelNode.getName();

 iNumChildNodes = objNode.nChildNode(strName.c_str());
 //////cout << "number of tel Nodes is " << iNumChildNodes << endl;;
 for (int i = 1; i <= iNumChildNodes; i++) {
  objTel.fClear();
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(objNode, "tel", i);

  if(CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode "+ int2str(i) + " empty in fn vcard::fLoadTel()");
   continue;
  } 

  if (objTel.fLoadTel(CurrentNode.deepCopy())) { 
   this->vTel.push_back(objTel);
  }
 }
 return true;
}

bool vc_adr::fLoadAdr(XMLNode objNode) {
 XMLNode	TypeNode, LabelNode, POBOXnode, ExtNode, StreetNode, LocalityNode, RegionNode, CodeNode, CountryNode, TextNode, ParameterNode;


// char*         	cptrResponse = NULL;
// string         stringTemp;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

/*
 cptrResponse = objNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/
 this->Parameters.fClear();
 ParameterNode = LoadChildNode(objNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }

/*       ***** Included in parameters ******
 this->Type.fClear();
 TypeNode = LoadChildNode(objNode, "type");
 if (!TypeNode.isEmpty()) {
  this->Type.fLoadTypeList(TypeNode.deepCopy());
 }

 this->Label.fClear();
 LabelNode = LoadChildNode(objNode, "label");
 if (!LabelNode.isEmpty()) {
  this->Label.fLoadLabel(LabelNode.deepCopy());
 } 
*/

 this->pobox.clear();
 POBOXnode = LoadChildNode(objNode, "pobox");
 if (!POBOXnode.isEmpty()) {
  if (POBOXnode.getText())  {
   this->pobox = POBOXnode.getText();
   ////cout << "Loaded pobox -> " << this->pobox << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty pobox in fn vc_adr::fLoadAdr()"); 
  }
 } 

 this->ext.clear();
 ExtNode = LoadChildNode(objNode, "ext");
 if (!ExtNode.isEmpty()) {
  if (ExtNode.getText())  {
   this->ext = ExtNode.getText();
   ////cout << "Loaded ext -> " << this->ext << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty ext in fn vc_adr::fLoadAdr()"); 
  }
 } 
 
 this->street.clear();
 StreetNode = LoadChildNode(objNode, "street");
 if (!StreetNode.isEmpty()) {
  if (StreetNode.getText())  {
   this->street = StreetNode.getText();
   ////cout << "Loaded street -> " << this->street << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty street in fn vc_adr::fLoadAdr()"); 
  }
 } 
 
 this->locality.clear();
 LocalityNode = LoadChildNode(objNode, "locality");
 if (!LocalityNode.isEmpty()) {
  if (LocalityNode.getText())  {
   this->locality = LocalityNode.getText();
   ////cout << "Loaded locality -> " << this->locality << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty LocalityNode in fn vc_adr::fLoadAdr()"); 
  }
 } 

 this->region.clear();
 RegionNode = LoadChildNode(objNode, "region");
 if (!RegionNode.isEmpty()) {
  if (RegionNode.getText())  {
   this->region = RegionNode.getText();
   ////cout << "Loaded region -> " << this->region << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty region in fn vc_adr::fLoadAdr()"); 
  }
 } 

 this->code.clear();
 CodeNode = LoadChildNode(objNode, "code");
 if (!CodeNode.isEmpty()) {
  if (CodeNode.getText())  {
   this->code = CodeNode.getText();
   ////cout << "Loaded code -> " << this->code << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty region in fn vc_adr::fLoadAdr()"); 
  }
 } 

 this->country.clear();
 CountryNode = LoadChildNode(objNode, "country");
 if (!CountryNode.isEmpty()) {
  if (CountryNode.getText())  {
   this->country = CountryNode.getText();
   ////cout << "Loaded country -> " << this->country << endl;
  }
  else {
   //SendCodingError("adr.cpp: Empty country in fn vc_adr::fLoadAdr()"); 
  }
 } 


 return true;
}



bool vcard::fLoadAddresses(XMLNode objNode) {
 XMLNode	AdrNode, ParameterNode, CurrentNode;
 int 		iNumChildNodes;
 string         strName;
 vc_adr         objAdr;
// char*         	cptrResponse = NULL;
// string         stringTemp;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName, int j);

/*
 cptrResponse = objNode.createXMLString();

 if (cptrResponse != NULL) {
   stringTemp   = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 ////cout << stringTemp << endl;
*/
 this->vAdr.clear();

 AdrNode = LoadChildNode(objNode, "adr");
 if (AdrNode.isEmpty()) {
  SendCodingError("adr.cpp: Empty Adr Node fn vcard::fLoadAddresses()"); 
  return false;
 } 

 strName = AdrNode.getName();

 iNumChildNodes = objNode.nChildNode(strName.c_str());
 //////cout << "number of adr Nodes is " << iNumChildNodes << endl;;
 for (int i = 1; i <= iNumChildNodes; i++) {
  objAdr.fClear();
  CurrentNode = XMLNode::emptyNode();
  CurrentNode = LoadChildNode(objNode, "adr", i);

  if(CurrentNode.isEmpty()) {
   SendCodingError("adr.cpp: CurrentNode "+ int2str(i) + " empty in fn vcard::fLoadAddresses()");
   continue;
  } 

  if (objAdr.fLoadAdr(CurrentNode.deepCopy())) { 
   this->vAdr.push_back(objAdr);
  }
 }
 return true;
}

void vc_full_name::fClear() {
 Parameters.fClear(); 
 fn.fClear();
}

bool vc_full_name::fLoadFullName(XMLNode NameNode) {
 XMLNode	TextNode, ParameterNode;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 ParameterNode = LoadChildNode(NameNode, "parameters");
 if (!ParameterNode.isEmpty()) {
  this->Parameters.fLoadParameters(ParameterNode);
 }
 
 TextNode = LoadChildNode(NameNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  {
   this->fn.strText = TextNode.getText();
   ////cout << "fn-> " << this->fn.strText << endl;
  }
 }

 return true;
}

void vc_nickname::fClear() {
 this->Parameters.fClear();
 this->NicknameList.fClear();
 return;
}

void vc_kind::fClear() {
 this->Text.fClear();
 return;
}

void vc_uid::fClear() {
 this->URI.fClear();
 return;
}

void vc_value_timestamp::fClear() {
 this->strTimeStamp.clear();
 return;
}

void vc_value_text::fClear() {
 this->strText.clear();
 return;
}

void vc_value_text_list::fClear() {
 this->strText.clear();
 this->vTextList.clear();
 return;
}

void vc_value_time::fClear() {
 this->strTime.clear();
 return;
}

void vc_value_date_time::fClear() {
 this->strDateTime.clear();
 return;
}

void vc_value_date::fClear() {
 this->strDate.clear();
 return;
}

void vc_value_utc_offset::fClear() {
 this->strUTCoffset.clear();
 return;
}

void vc_value_language_tag::fClear() {
 this->strLangTag.clear();
 return;
}

void vc_value_uri::fClear() {
 this->strURI.clear();
 return;
}

void vc_media_type::fClear() {
 this->media.fClear();
 return;
}

void vc_pref::fClear() {
 this->iPref = 0;
 return;
}

void vc_altid::fClear() {
 this->id.fClear();
 return;
}

void vc_pid::fClear() {
 this->strPID.clear();
 return;
}

void vc_type::fClear() {
 this->TypeList.fClear();
 return;
}

void vc_param_lang::fClear() {
 this->LanguageTag.fClear();
 return;
}

void vc_calscale::fClear() {
 this->Text.fClear();
 return;
}

void vc_sort_as::fClear() {
 this->Text.fClear();
 return;
}

void vc_param_geo::fClear() {
 this->URI.fClear();
 return;
}

void vc_impp::fClear() {
 this->Parameters.fClear();
 this->URI.fClear();
 return;
}

void vc_param_tz::fClear() {
 this->Text.fClear();
 this->URI.fClear();
 return;
}

void vc_label::fClear() {
 this->Text.fClear();
 return;
}

void vc_parameters::fClear() {
 this->Mediatype.fClear();
 this->AltID.fClear();
 this->PID.fClear();
 this->Pref.fClear();
 this->Type.fClear();
 this->Language.fClear();
 this->Calscale.fClear();
 this->SortAs.fClear();
 this->Geo.fClear();
 this->TZ.fClear();
 this->Label.fClear();
 return;
}

bool vc_parameters::fLoadParameters(XMLNode ParamNode) {
 XMLNode	MediatypeNode, AltIDnode, PIDnode, PrefNode, TypeNode, LanguageNode, CalscaleNode, SortAsNode, GeoNode, TZnode, LabelNode; 

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

   MediatypeNode = LoadChildNode(ParamNode, "mediatype");
   if (!MediatypeNode.isEmpty()) {
    this->Mediatype.fLoadMediaType(MediatypeNode);
   }

   AltIDnode = LoadChildNode(ParamNode, "altid");
   if (!AltIDnode.isEmpty()) {
    this->AltID.fLoadAltID(AltIDnode);
   }

   PIDnode = LoadChildNode(ParamNode, "pid");
   if (!PIDnode.isEmpty()) {
    this->PID.fLoadPID(PIDnode);
   }

   PrefNode = LoadChildNode(ParamNode, "pref");
   if (!PrefNode.isEmpty()) {
    this->Pref.fLoadPref(PrefNode);
   }

   TypeNode = LoadChildNode(ParamNode, "type");
   if (!TypeNode.isEmpty()) {
    this->Type.fLoadTypeList(TypeNode);
   }

   LanguageNode = LoadChildNode(ParamNode, "language");
   if (!LanguageNode.isEmpty()) {
    this->Language.fLoadLanguageTag(LanguageNode);
   }

   CalscaleNode = LoadChildNode(ParamNode, "calscale");
   if (!CalscaleNode.isEmpty()) {
    this->Calscale.fLoadCalScale(CalscaleNode);
   }

   SortAsNode = LoadChildNode(ParamNode, "sort-as");
   if (!SortAsNode.isEmpty()) {
    this->SortAs.fLoadSortAs(SortAsNode);
   }

   GeoNode = LoadChildNode(ParamNode, "geo");
   if (!GeoNode.isEmpty()) {
    this->Geo.fLoadGeo(GeoNode);
   }

   TZnode = LoadChildNode(ParamNode, "tz");
   if (!TZnode.isEmpty()) {
    this->TZ.fLoadTZ(TZnode);
   }

   LabelNode = LoadChildNode(ParamNode, "label");
   if (!LabelNode.isEmpty()) {
    this->Label.fLoadLabel(LabelNode);
   }

 return true;
}

void vc_rev::fClear() {
 this->TimeStamp.fClear();
 return;
}

void vc_title::fClear() {
 this->Parameters.fClear();
 this->Text.fClear();
 return;
}

void vc_role::fClear() {
 this->Parameters.fClear();
 this->Text.fClear();
 return;
}

void vc_photo::fClear() {
 this->Parameters.fClear();
 this->URI.fClear();
 return;
}

void vc_note::fClear() {
 this->Parameters.fClear();
 this->Text.fClear();
 return;
}

void vc_email::fClear() {
 this->Parameters.fClear();
 this->Text.fClear();
 return;
}

void vc_geo::fClear() {
 this->Parameters.fClear();
 this->URI.fClear();
 return;
}

void vc_tel::fClear() {
 this->Parameters.fClear();
 this->URI.fClear();
 this->Text.fClear();
 return;
}

void vc_adr::fClear() {
 this->Parameters.fClear();
 this->Type.fClear();
 this->Label.fClear();
 this->pobox.clear();
 this->ext.clear();
 this->street.clear();
 this->locality.clear();
 this->region.clear();
 this->code.clear();
 this->country.clear();
 return;
}

void vc_related::fClear() {
 this->Parameters.fClear();
 this->URI.fClear();
 this->Text.fClear();
 return;
}

bool vc_label::fLoadLabel(XMLNode LabelNode) {
 XMLNode	TextNode;
 
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 TextNode = LoadChildNode(LabelNode, "text");
 if (TextNode.isEmpty()) {
  return false;
 }
  
 if (TextNode.getText())  { 
  this->Text.strText = TextNode.getText();
  ////cout << "Loaded Label -> " << this->Text.strText << endl;   
 }
 else{
  SendCodingError("adr.cpp: No Text in fn vc_label::fLoadLabel()");
  return false;
 }
 
 return true;
}

bool vc_param_tz::fLoadTZ(XMLNode TZnode) {
 XMLNode	TextNode;
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 
 this->URI.strURI.clear();
 this->Text.strText.clear();

  if (TZnode.getText())  { 
   this->URI.strURI = TZnode.getText();
   ////cout << "TZ loaded URI -> " << this->URI.strURI << endl; 
  }
  else {
  // check for text node
   TextNode = LoadChildNode(TZnode, "text");
   if (TextNode.isEmpty()) {
    SendCodingError("adr.cpp: No Text Node in fn vc_param_tz::fLoadTZ()");
    return false;
   }
   else {
    if (TextNode.getText())  {  
     this->Text.strText = TextNode.getText();
     ////cout << "TZ loaded Text -> " << this->Text.strText << endl;    
    }
    else {
     SendCodingError("adr.cpp: No TZ Text in fn vc_param_tz::fLoadTZ()");
     return false;   
    }
   }
  }

 return true;
}

bool vc_param_geo::fLoadGeo(XMLNode GeoNode) {
 XMLNode	TextNode;
 
 this->URI.strURI.clear();

 if (!GeoNode.isEmpty()) {
  if (GeoNode.getText())  { 
   this->URI.strURI = TextNode.getText();   
  }
 } 

 return true;
}

bool vc_sort_as::fLoadSortAs(XMLNode SortAsNode) {
 XMLNode	TextNode;
 
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 TextNode = LoadChildNode(SortAsNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  { 
   this->Text.strText = TextNode.getText();   
  }
 } 

 return true;
}

bool vc_calscale::fLoadCalScale(XMLNode CalscaleNode) {
 XMLNode	TextNode;
 
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->fClear();

 TextNode = LoadChildNode(CalscaleNode, "text");
 if (!TextNode.isEmpty()) {
  if (TextNode.getText())  { 
   this->Text.strText = TextNode.getText();   
  }
 } 

 return true;
}

bool vc_value_language_tag::fLoadLanguageTag(XMLNode LanguageNode) {
 XMLNode	LanguageTagNode;
 
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);

 this->strLangTag.clear();

 LanguageTagNode = LoadChildNode(LanguageNode, "language-tag");
 if (!LanguageTagNode.isEmpty()) {
  if (LanguageTagNode.getText())  { 
   this->strLangTag = LanguageTagNode.getText();
   ////cout << "vc_value_language_tag : Language-tag -> " << this->strLangTag << endl;   
  }
  else {
   SendCodingError("adr.cpp: No Text found in fn vc_value_language_tag::fLoadLanguageTag()");    
   return false;
  }
 } 
 else {
  SendCodingError("adr.cpp: No language-tag found in fn vc_value_language_tag::fLoadLanguageTag()");    
  return false;
 }

 return true;
}

bool vc_param_lang::fLoadLanguageTag(XMLNode LanguageNode) {
 
 return this->LanguageTag.fLoadLanguageTag(LanguageNode) ;
}

bool vc_type::fLoadTypeList(XMLNode TypeNode) {
 XMLNode	TextNode; 
 int 		iNumChildNodes;
 string         strName;

 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName);
 extern XMLNode LoadChildNode	( XMLNode ParentNode , string strName, int j);

 this->TypeList.strText.clear();
 this->TypeList.vTextList.clear();

 TextNode = LoadChildNode(TypeNode, "text");
 if (TextNode.isEmpty()) {
  return false;
 } 
 strName = TextNode.getName();

 iNumChildNodes = TypeNode.nChildNode(strName.c_str());
 //////cout << "num type nodes -> " << iNumChildNodes << endl;
 for (int i = 1; i <= iNumChildNodes; i++) {
  TextNode = XMLNode::emptyNode();
  TextNode = LoadChildNode(TypeNode, "text", i);
  if (!TextNode.isEmpty()) {
   if (TextNode.getText())  {
    this->TypeList.strText = TextNode.getText();
    this->TypeList.vTextList.push_back(this->TypeList.strText);
    ////cout << "add type ->" << this->TypeList.strText << endl;
   }
  }
 }
 return true;
}

bool vc_pref::fLoadPref(XMLNode PrefNode) {
  XMLNode	IntegerNode;
  string	strInteger;
  int           Integer;
  size_t        found;
 
/*

*/
  extern XMLNode 		LoadChildNode	( XMLNode ParentNode , string strName);
  extern unsigned long long int char2int  	(const char* CharArg); 
 
  this->iPref = 0;
  IntegerNode = LoadChildNode(PrefNode, "integer");
  if (!IntegerNode.isEmpty()) {
   if (IntegerNode.getText())  {
    strInteger = IntegerNode.getText();
    found =  strInteger.find_first_not_of("0123456789");
    if (found != string::npos) {
     SendCodingError("adr.cpp: Non Integer found in fn fLoadPref() Number-> " + strInteger);
     return false;
    }
    if (strInteger.length() > 5) {
     SendCodingError("adr.cpp: Integer out of Bounds in fn fLoadPref() Number-> " + strInteger);
     return false;
    }
    Integer = char2int(strInteger.c_str());
    if ((Integer < 1) || (Integer > 100)) {
     SendCodingError("adr.cpp: Integer out of Bounds in fn fLoadPref() Number-> " + strInteger);
     return false;
    }
    this->iPref = Integer;
    ////cout << "Loaded pref -> " << this->iPref << endl;  
   }
  }
 return true;
}


bool vc_pid::fLoadPID(XMLNode PIDnode) {
  XMLNode	TextNode; 
/*
 Not provided in examples .... not sure if child <text> .....
*/
  extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

  TextNode = LoadChildNode(PIDnode, "text");
  if (!TextNode.isEmpty()) {
   if (TextNode.getText())  {
    this->strPID = TextNode.getText();
   }
  }

 return true;
}

bool vc_media_type::fLoadMediaType(XMLNode MediaTypeNode) {
  XMLNode	TextNode; 

  extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

  TextNode = LoadChildNode(MediaTypeNode, "text");
  if (!TextNode.isEmpty()) {
   if (TextNode.getText())  {
    this->media.strText = TextNode.getText();
   }
  }

 return true;
}

bool vc_altid::fLoadAltID(XMLNode AltIDnode) {
  XMLNode	TextNode; 

  extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

  TextNode = LoadChildNode(AltIDnode, "text");
  if (!TextNode.isEmpty()) {
   if (TextNode.getText())  {
    this->id.strText = TextNode.getText();
   }
  }

 return true;
}


/*
string extractSubscriberInfoData(string strInput) {
  size_t found;
  string strStart = "application/EmergencyCallData.SubscriberInfo+xml";
  string strEnd   = "--addDataBoundary";
  string strReturn;

  found = strInput.find(strStart);
  if (found == string::npos)       {return "";}
  strReturn = strInput.substr(found + strStart.length(),string::npos);

  found = strReturn.find(strEnd);
 
  strReturn = strReturn.substr(0, found);
  return strReturn;
}
*/

string extractContent(string strInput, string strStart, string strEnd) {
  size_t found;
  string strReturn;

  found = strInput.find(strStart);
  if (found == string::npos)       {return "";}
  strReturn = strInput.substr(found + strStart.length(),string::npos);

  found = strReturn.find(strEnd);
 
  strReturn = strReturn.substr(0, found);
  return strReturn;
}

string DetermineBoundary(string strInput) {
 size_t found, start, end;
 string strReturn;

 found = strInput.find("Content-Type:");
 if (found == string::npos) {
  SendCodingError("adr.cpp Content-Type: not found in DetermineBoundary()");
  return "--addDataBoundary";
 }

 found = strInput.find("boundary=", found);
 if (found == string::npos) {
  SendCodingError("adr.cpp boundary= not found in DetermineBoundary()");
  return "--addDataBoundary";
 }

 start = found + 9;
 end = strInput.find_first_of(" \n\r\t\f\v", start);

 if (end == string::npos) {
  SendCodingError("adr.cpp Whitespace not found in DetermineBoundary()");
  return "--addDataBoundary";
 }

 strReturn = "--" + strInput.substr(start, (end-start));
 return strReturn;
}

//Constuctor
Provider_Info::Provider_Info() {
 strDataProviderReference.clear();
 strDataProviderString.clear();
 strProviderID.clear();
 strProviderIDSeries.clear();
 strTypeOfProvider.clear();
 strContactURI.clear();
 strLanguage.clear();
 vcardDataProviderContact.fClear();
 strSubcontractorPrincipal.clear();
 strSubcontractorPriority.clear();
}
//copy constuctor
Provider_Info::Provider_Info(const Provider_Info *a) {
 strDataProviderReference	= a->strDataProviderReference;
 strDataProviderString		= a->strDataProviderString;
 strProviderID			= a->strProviderID;
 strProviderIDSeries		= a->strProviderIDSeries;
 strTypeOfProvider		= a->strTypeOfProvider;
 strContactURI			= a->strContactURI;
 strLanguage			= a->strLanguage;
 vcardDataProviderContact	= a->vcardDataProviderContact;
 strSubcontractorPrincipal	= a->strSubcontractorPrincipal;
 strSubcontractorPriority	= a->strSubcontractorPriority;
}
//assignment operator
Provider_Info& Provider_Info::operator=(const Provider_Info& a) {
 if (&a == this ) {return *this;}
 strDataProviderReference	= a.strDataProviderReference;
 strDataProviderString		= a.strDataProviderString;
 strProviderID			= a.strProviderID;
 strProviderIDSeries		= a.strProviderIDSeries;
 strTypeOfProvider		= a.strTypeOfProvider;
 strContactURI			= a.strContactURI;
 strLanguage			= a.strLanguage;
 vcardDataProviderContact	= a.vcardDataProviderContact;
 strSubcontractorPrincipal	= a.strSubcontractorPrincipal;
 strSubcontractorPriority	= a.strSubcontractorPriority;
 return *this;
}

void Provider_Info::fClear() {
 strDataProviderReference.clear();
 strDataProviderString.clear();
 strProviderID.clear();
 strProviderIDSeries.clear();
 strTypeOfProvider.clear();
 strContactURI.clear();
 strLanguage.clear();
 vcardDataProviderContact.fClear();
 strSubcontractorPrincipal.clear();
 strSubcontractorPriority.clear();
}

//Constuctor
Subscriber_Info::Subscriber_Info() {
 strDataProviderReference.clear();
 vcardSubscriberData.fClear();
 boolPrivacyRequested = false;
}
//copy constuctor
Subscriber_Info::Subscriber_Info(const Subscriber_Info *a) {
 strDataProviderReference	= a->strDataProviderReference;
 vcardSubscriberData		= a->vcardSubscriberData;
 boolPrivacyRequested		= a->boolPrivacyRequested;
}
//assignment operator
Subscriber_Info& Subscriber_Info::operator=(const Subscriber_Info& a) {
 if (&a == this ) {return *this;}
 strDataProviderReference	= a.strDataProviderReference;
 vcardSubscriberData		= a.vcardSubscriberData;
 boolPrivacyRequested		= a.boolPrivacyRequested;
 return *this;
}

void Subscriber_Info::fClear() {
 strDataProviderReference.clear();
 vcardSubscriberData.fClear();
 boolPrivacyRequested = false;
}

//Constuctor
Service_Info::Service_Info() {
 strDataProviderReference.clear();
 strServiceEnvironment.clear();
 strServiceType.clear();
 strServiceMobility.clear();
// strLegacyClassOfService.clear();
// strLegacyClassOfServiceCode.clear();
}

//copy constuctor
Service_Info::Service_Info(const Service_Info *a) {
 strDataProviderReference	= a->strDataProviderReference;
 strServiceEnvironment		= a->strServiceEnvironment;
 strServiceType			= a->strServiceType;
 strServiceMobility		= a->strServiceMobility;
// strLegacyClassOfService	= a->strLegacyClassOfService;
// strLegacyClassOfServiceCode	= a->strLegacyClassOfServiceCode;
}
//assignment operator
Service_Info& Service_Info::operator=(const Service_Info& a) {
 if (&a == this ) {return *this;}
 strDataProviderReference	= a.strDataProviderReference;
 strServiceEnvironment		= a.strServiceEnvironment;
 strServiceType			= a.strServiceType;
 strServiceMobility		= a.strServiceMobility;
// strLegacyClassOfService	= a.strLegacyClassOfService;
// strLegacyClassOfServiceCode	= a.strLegacyClassOfServiceCode;
 return *this;
}

void Service_Info::fClear() {
 strDataProviderReference.clear();
 strServiceEnvironment.clear();
 strServiceType.clear();
 strServiceMobility.clear();
// strLegacyClassOfService.clear();
// strLegacyClassOfServiceCode.clear();
}



