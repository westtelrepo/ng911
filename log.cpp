/*****************************************************************************
* FILE: log.cpp
*
* DESCRIPTION: This file contains all of the functions integral to the LOG Thread
*
* Features:
*
*    1. Write error notification:           Email notification of write errors to log;
*                                           Ability to limit the notification;
*    2. Queue Sort delay :                  A seperate sort queue is added to ensure the proper order of log entries as the 
*                                           multi-threaded queues are merged.
*    3. Postfix Mail Server                 Utilizes the postfix mail server to send Controller Status Messages
*    4. Email/Alarm Notification:           Informational email messages on startup;
*                                           Warning email messages for unusual activity;
*                                           Alarm text messages for connection down/unavailable;
*                                           Reminder email messages that alarm condition still exists
*    5. Queued Email Delivery               Messages are queued in the event of Internet outages and sent later
*    6. Self-Diagnostics:                   Built in diagnostics to help troubleshoot line errors, etc.
*    7. Threaded Implementation:            Provides efficient utilization and reduces resource monopolization
*    8. NENA Compliant:                     Complies with the “NENA Recommended Generic Standards for E9-1-1 PSAP Equipment”
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"


// fwd function declaration
//void                    Log_Messaging_Monitor_Event(union sigval union_sigvalArg);
void 			Email_Filter_Update_Event(union sigval union_sigvalArg);
unsigned long long int  LostMessageIncrement(unsigned long long int intArg);
bool                    ResumeLoggingMessages();
void                    Check_Reset_Daily_Counters();
void*                   ECATS_TX(void *voidArg);
int                     ConnectToECATS_TCP_Port();
extern void             *Log_Messaging_Monitor_Event(void *voidArg);
//external class based variables
extern Thread_Trap                              OrderlyShutDown;
extern Thread_Trap                              UpdateVars;
extern queue <MessageClass>	                queue_objMainMessageQ;
extern queue <MessageClass>	                queue_objLogMessageQ;
extern IP_Address                               HOST_IP;
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data				objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_LOG_PORT;
extern Port_Data                                objBLANK_KRN_PORT;

// LOG Global Variables
timer_t				        timer_tLogMessageMonitorTimerId;
timer_t				        timer_tEmailFilterUpdateTimerId;
struct itimerspec			itimerspecLogMessageMonitorDelay;
struct itimerspec			itimerspecEmailFilterUpdateInterval;
ExperientEmail                          objEmailMsg;
unsigned int				intMONTHS_TO_KEEP_LOG_FILES;
unsigned long long int                  intLostMessages;
unsigned long long int                  MAX_UNSIGNED_LONG_LONG_INT;
volatile bool				boolLastFileOpenOperationFailed = false;
volatile bool				boolLastWriteOperationFailed    = false;
volatile bool				boolLastEmailFailedToSend	= false;
EmailSlam				objEmailSlam;
volatile bool                           boolEmailSupress[1000]          = {false};
pthread_t                  		ECATS_Thread;
ExperientCommPort                       ECATS_PORT;


/****************************************************************************************************
*
* Name:
*  Function: void *Log_Console_Email_Thread(void *voidArg)
*
*
* Description:
*   Handles all of the LOG related activity for the controller. 
*
*
* Details:
*   This function is implemented as detached thread from main().  It serves as the clearing house for
*   displaying (console output), logging (file output), and emailing messages. The function waits upon
*   a counting semaphore signal from main() to begin processing the queue of messages.
*
*   A timer event forwards thread specific messages onto the main queue which are then sent back
*   to this thread for processing.
*
*   Each message is recieved from the main message Q and is sorted into a vector container. The container holds
*   the message for a configured amount of time before it is removed and processed.  This is performed due to the fact that
*   messages can come into main out of order due to thread round robin scheduling.  
*
*   The Container is limited to a configured size and is protected by a "circut breaker threshold percentage". When that
*   percentage is reached the vector is emptied and those messages are lost.  A percentage is used because incoming messages into
*   the main queue may cause an overshoot of any hard number.
*
*   Each message contains a Log Code which is decoded to tell the thread what logging action(s) to
*   take. The codes assigned to each operation are: 
*                                                   0 = Nothing
*                                                   1 = Log to file
*                                                   2 = Display to Console
*                                                   4 = Email with priority INFO
*                                                   8 = Email with priority WARNING
*                                                  16 = Email with priority ALARM
*                                                  32 = Send to WorkSation
*                                                  64 = Write to AMI script
*
*   The code given is the sum of all the operations requested (i.e.) to email with priority ALARM and Display to
*   Console and Log to file the code would be 16 + 2 + 1 which is 19.
*   
*
*
* Parameters: 				
*   void *voidArg			        pointer to queue_objOutputMessageQ
*
* Variables:
*   boolDEBUG                                   Global  - show debug data if true
*   boolEMAIL_QUEUE_ACTIVE                      Global  - email(s) have been queued if true
*   .boolLogThreadReady                         Global  - <Thread_Trap> volatile bool to indicate the thread is ready to shut down
*   boolShowOnce                                Local   - bool used to limit multiple recurring messages
*   boolSTOP_EXECUTION                          Global  - volatile bool used to stop the program
*   boolSTOP_LOGGING                            Local   - bool used to stop logging
*   boolSTOP_LOG_THREAD                         Global  - volatile bool used to stop this thread
*   CLOCK_REALTIME                              Library - <ctime> constant
*   CPU_AFFINITY_ALL_OTHER_THREADS_SET          Global  - <globals.h><cpu_set_t>
*   dblLOG_VECTORCLASS_BREAKER_PERCENTAGE       Global  - <defines.h> % of max size when to stop logging
*   dblLOG_QUEUE_SORTDELAY_SEC                  Global  - <defines.h> x.x seconds to hold onto messages before processing
*   EMAILALARM                                  Global  - <header.h><email_type> enumeration member
*   EMAILINFO                                   Global  - <header.h><email_type> enumeration member
*   EMAILWARNING                                Global  - <header.h><email_type> enumeration member
*   errno                                       Library - <errno.h>
*   intDelayCounter                             Local   - counter used to delay/slow down mail queue checking
*   .intLogCode                                 Global  - <MessageClass> member
*   intLogDecoderArray[]                        Local   - used to decode intLogcode
*   intLOG_MESSAGE_MONITOR_INTERVAL_NSEC        Global  - <defines.h>
*   intMaxVectorClassElements                   Local   - used to hold the max size of vector class object
*   .intMessageCode                             Global  - <MessageClass> member
*   intLOG_MESSAGE_MONITOR_INTERVAL_NSEC        Global  - <defines.h> nsec time interval for Messaging timer 
*   intLostMessages                             Global  - number of lost (not logged) messages   
*   intRC                                       Local   - return code for functions
*   intTHREAD_SHUTDOWN_DELAY_SEC                Global  - <defines.h>
*   intX                                        Local   - temp integer
*   it                                          Local   - <vector> iterator
*   itimerspecLogMessageMonitorDelay            Global  - <globals.h>
*   KRN_MESSAGE_142			        Global  - <defines.h> CPU affinity Error message   
*   LOG                                         Global  - <header.h> <threadorPorttype> enumeration member
*   LOG_CONSOLE_FILE                            Global  - Log Code to display message to console and file to disk
*   LOG_MESSAGE_500                             Global  - <defines.h> LOG Thread Created output message
*   LOG_MESSAGE_509                             Global  - <defines.h> Message Monitor Timer Event Initialized output message
*   LOG_MESSAGE_518                             Global  - <defines.h> Signaling Semaphore failed to lock output message
*   LOG_MESSAGE_521                             Global  - <defines.h> LOG Message Queue Vector Container at Capacity output message
*   LOG_MESSAGE_522                             Global  - <defines.h> Max Size of Message Vector Container output message
*   LOG_SORTED_QUEUE_MAX_SIZE                   Global  - <defines.h>  max size sorted list is allowed to grow
*   LOG_WARNING                                 Global  - Log Code to email message display to console and file to disk
*   NUMBER_OF_LOG_FUNCTIONS                     Global  - <defines.h> total number of logging functions
*   objEmailMsg                                 Global  - <ExperientEmail> object
*   objEmailMsgLog                              Local   - <ExperientEmail> object
*   objMessage				        Local   - MessageClass object
*   OrderlyShutDown                             Global  - <Thread_Trap>
*   queue_objOutputMessageQ		        Local   - <queue> <MessageClass> objects 
*   sem_tLogConsoleEmailFlag                    Global  - <globals.h>
*   sem_tMutexLogMessageQ		        Global  - <globals.h> mutex to prevent multiple operations on the queue
*   sem_tMutexMainMessageQ		        Global  - <globals.h> mutex to prevent multiple operations on the queue
*   sigeventLogMessageMonitorEvent	        Local	- <csignal> struct sigevent defining the event timer
*   .sigev_notify                               Library - <csignal> <sig event> structure member
*   .sigev_notify_attributes                    Library - <csignal> <sig event> structure member
*   .sigev_notify_function                      Library - <csignal> <sig event> structure member
*   SIGEV_THREAD                                Library - <pthread.h> constant
*   sizeMaxLOGQSizeEncountered                  Local   - size_t .. Contains the largest size recorded of the log vector during operations
*   stringErrorMsg                              Local   - <cstring> contains error description 
*   timer_tLogMessageMonitorTimerId	        Global	- <ctime> timer associated with Log Message Monitor
*   timespecNanoDelay                           Global  - <ctime> delay in nsec for nanosleep function
*   timespecRemainingTime                       Global  - <ctime> remaining time for nanosleep function
*   timespecTimeNow                             Local   - <ctime><timespec> structure .. current time
*   .rtimespecTimeStamp                          Global  - <MessageClass> member
*   vobjSortedOutput                            Local   - <vector> <MessageClass> .. the sorted list of messages
*                                                                      
* Functions:
*   Abnormal_Exit()			Global  <globalfunctions.h>                     (void)
*   .begin()                            Library <vector>                                (vector::iterator)
*   .capacity()                         Library <vector>                                (size_type)
*   clock_gettime()                     Library <ctime>                                 (int)
*   .Connect()                          Global  <ipworks.h><FileMailer>                 (int)
*   Console_Message()			Global  <globalfunctions.h>                     (void)
*   .Disconnect()                       Global  <ipworks.h><FileMailer>                 (int)
*   .empty()                            Library <queue>                                 (bool)
*   .end()                              Library <vector>                                (vector::iterator)
*   enQueue_Message()   		Global  <globalfunctions.h>                     (void)
*   .erase()                            Library - <vector>                              (vector::iterator)
*   .fAllThreadsReady()                 Global  <Thread_Trap>                             (bool)
*   .fConsole()    			Global  <MessageClass>                          (void)
*   .fEmail()                           Global  <MessageClass>                          (void)
*   .fIntializeHeaders()                Global  <ExperientEmail>                        (void)
*   .fLog()                             Global  <MessageClass>                          (int)
*   .fMessage_Create()			Global  <MessageClass>                          (void)
*   .fProcessEmailQueue()               Global  <ExperientCommPort>                     (int)
*   .front()				Library <queue>                                 (*this)
*   getpid()				Library <unistd.h>                              (pid_t)
*   .insert()                           Library <vector>                                (vector::iterator)
*   int2strLZ()			        Global  <globalfunctions.h>                     (string)
*   Log_Messaging_Monitor_Event()       Local                                           (void)
*   LostMessageIncrement()              Local                                           (unsigned long long int)  
*   .max_size()                         Library <vector>                                (size_type)
*   nanosleep()                         Library <ctime>                                 (int) 
*   .pop()				Library <queue>                                 (void)
*   pthread_self()			Library <pthread.h>                             (pthread_t)
*   pthread_setaffinity_np()		Library <pthread.h>                             (int)
*   .push_back()                        Library <vector>                                (void)
*   .reserve()                          Library <vector>                                (void) 
*   ResumeLoggingMessages()             Local                                           (bool)                                
*   Semaphore_Error()                   Global  <globalfunctions.h>                     (void)
*   sem_wait()             		Library <semaphore.h>                           (int)
*   sem_post()				Library <semaphore.h>                           (int)
*   Set_Timer_Delay()			Global  <globalfunctions.h>                     (void)
*   .size()                             Library <vector>                                (size_type)
*   sleep()                             Library <unistd.h>                              (unsigned int)
*   TimeCheckBefore()                   Global  <globalfunctions.h>                     (bool)                      
*   timer_create()			Library <ctime>                                 (int)
*   timer_delete()                      Library <ctime>                                 (int)
*   timer_settime()			Library <ctime>                                 (int)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *Log_Console_Email_Thread(void *voidArg)
{

 int				intRC;							
 int                            intX;                                                   
 int                            intLogDecoderArray[NUMBER_OF_LOG_FUNCTIONS];
 unsigned long long int         intMaxVectorClassElements;
 size_t                         sizeMaxLOGQSizeEncountered = 0;
 MessageClass			objMessage;
 queue <MessageClass>*  	queue_objOutputMessageQ;
 vector <MessageClass>          vobjSortedOutput;
 vector<MessageClass>::iterator it;
 struct timespec                timespecTimeNow;
 struct sigevent		sigeventLogMessageMonitorEvent;
 struct sigevent		sigeventEmailFilterUpdateEvent;
 string                         stringErrorMsg;
 volatile bool                  boolSTOP_LOGGING = false;
 bool                           boolShowOnce     = true;
 ExperientEmail                 objEmailMsgLog;
 int                            intDelayCounter  = 0;    
 struct timespec                timespecRemainingTime; 
 Thread_Data                    ThreadDataObj;
 pthread_t			pthread_tLOGmessagingThread;
 pthread_attr_t                 LOGpthread_attr_tAttr; 
 pthread_attr_t                 ECATSpthread_attr_tAttr; 
 deque <string>                 q_ECATS_Messages;
 string                         strECATSmessage;

    
 extern vector <Thread_Data> 	ThreadData;

 queue_objOutputMessageQ 	= (queue <MessageClass>*) voidArg;			//recast voidArg
 
 // initialize Globals
 intLostMessages = 0;

/*
 // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,529, objBLANK_CALL_RECORD, objBLANK_LOG_PORT, KRN_MESSAGE_142); enQueue_Message(LOG,objMessage);}
*/

 ThreadDataObj.strThreadName ="LOG";
// ThreadDataObj.ThreadPID     = getpid();
 //ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in log.cpp::Log_Console_Email_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Init message ..
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,500, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_500, int2strLZ(getpid()), int2strLZ(pthread_self())); 
 enQueue_Message(LOG,objMessage);

 // set size of sorted vector output
 intMaxVectorClassElements = vobjSortedOutput.max_size();

 
 vobjSortedOutput.reserve( LOG_SORTED_QUEUE_MAX_SIZE );
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,522, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_522, int2strLZ(intMaxVectorClassElements)); 
 enQueue_Message(LOG,objMessage);

  // start ECATS thread

 if (BOOL_SEND_ECATS){
  ECATS_PORT.ePortConnectionType = ECATS_PORT_CONNECTION_TYPE;
  switch (ECATS_PORT.ePortConnectionType)   {
    case TCP: ECATS_PORT.TCP_Port.fInitializeECATSport(1); ConnectToECATS_TCP_Port(); break;
    case UDP: ECATS_PORT.fInitializeECATS_UDP_Port(); break;
    default: 
         SendCodingError("log.cpp *Log_Console_Email_Thread() -NO_CONNECTION_TYPE_DEFINED -1");
         break;
  }

  pthread_attr_init(&ECATSpthread_attr_tAttr);
  pthread_attr_setdetachstate(&ECATSpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
  intRC = pthread_create(&ECATS_Thread, &ECATSpthread_attr_tAttr, *ECATS_TX, (void*) 0);
  if (intRC) {SendCodingError("log.cpp - Unable to create ECATS thread" ); exit(1);}
 }

  pthread_attr_init(&LOGpthread_attr_tAttr);
  intRC = pthread_create(&pthread_tLOGmessagingThread, &LOGpthread_attr_tAttr, *Log_Messaging_Monitor_Event, NULL);
  if (intRC) {Abnormal_Exit(CTI, EX_OSERR, LOG_MESSAGE_595);}


/*
 // set up Log Message monitor timer event
 sigeventLogMessageMonitorEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventLogMessageMonitorEvent.sigev_notify_function 		        = Log_Messaging_Monitor_Event;
 sigeventLogMessageMonitorEvent.sigev_notify_attributes 		= NULL;
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventLogMessageMonitorEvent, &timer_tLogMessageMonitorTimerId);
 
 if (intRC){Abnormal_Exit(LOG, EX_OSERR, KRN_MESSAGE_180, "timer_tLogMessageMonitorTimerId", int2strLZ(errno));}

// Set_Timer_Delay(&itimerspecLogMessageMonitorDelay, 0,intLOG_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intLOG_MESSAGE_MONITOR_INTERVAL_NSEC );
 Set_Timer_Delay(&itimerspecLogMessageMonitorDelay, 0,intLOG_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0);              //One shot timer

 timer_settime( timer_tLogMessageMonitorTimerId, 0, &itimerspecLogMessageMonitorDelay, NULL);			// arm timer

 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG, 509, objBLANK_CALL_RECORD, objBLANK_LOG_PORT, LOG_MESSAGE_509); 
 enQueue_Message(LOG,objMessage);
*/
 // set up Log email filter update timer event
 sigeventEmailFilterUpdateEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventEmailFilterUpdateEvent.sigev_notify_function 		        = Email_Filter_Update_Event;
 sigeventEmailFilterUpdateEvent.sigev_notify_attributes 		= NULL;
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventEmailFilterUpdateEvent, &timer_tEmailFilterUpdateTimerId);
 
 if (intRC){Abnormal_Exit(LOG, EX_OSERR, KRN_MESSAGE_180, "timer_tEmailFilterUpdateTimerId", int2strLZ(errno));}

 Set_Timer_Delay(&itimerspecEmailFilterUpdateInterval, EMAIL_FILTER_UPDATE_INTERVAL_SEC, 0, EMAIL_FILTER_UPDATE_INTERVAL_SEC, 0 );

 timer_settime( timer_tEmailFilterUpdateTimerId, 0, &itimerspecEmailFilterUpdateInterval, NULL);			// arm timer


 //initialize decoder array
 intLogDecoderArray[0] = 0;
 intLogDecoderArray[1] = 1;
 for (int i = 2; i < NUMBER_OF_LOG_FUNCTIONS; i++) {intLogDecoderArray[i] = intLogDecoderArray[i-1] * 2;}

 
 // Log thread main do loop	
 do 
  {
  
   intRC = sem_wait(&sem_tLogConsoleEmailFlag);					// wait on signal

   // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
   if (intRC)
    {
     objMessage.fMessage_Create(LOG_WARNING,518, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_518); 
     enQueue_Message(LOG,objMessage);
    } 


  if (boolUPDATE_VARS) 
   {
    UpdateVars.boolLogThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    UpdateVars.boolLogThreadReady = false;       
   }



   // shut down trap area
   if (boolSTOP_EXECUTION||boolSTOP_LOG_THREAD)
    {
     // timer_delete( timer_tLogMessageMonitorTimerId);				// shut down timer events
      timer_delete( timer_tEmailFilterUpdateTimerId);
      while (!queue_objOutputMessageQ->empty()) {queue_objOutputMessageQ->pop();}
      objMessage.fMessage_Create(LOG_INFO, 181, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                 objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_181);
      objMessage.fLog(READABLE_LOG);
     // intRC = objMessage.fLog(XML_LOG);
      OrderlyShutDown.boolLogThreadReady = true;
      objEmailMsgLog.Interrupt();
      objEmailMsgLog.Disconnect();
      objEmailMsg.Interrupt();
      objEmailMsg.Disconnect();
      objEmailMsgLog.DoEvents();
      objEmailMsg.DoEvents();
      for (int i = 0; i <50;i++){experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC);}

      if (BOOL_SEND_ECATS) { pthread_join( ECATS_Thread, NULL); }
      pthread_join( pthread_tLOGmessagingThread, NULL); 
      while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
      
      break;
    }
  

   //empty Q and sort it into a vector
   while (!ThreadsafeEmptyQueueCheck(queue_objOutputMessageQ, &sem_tMutexLogMessageQ))
    {
     intRC = sem_wait(&sem_tMutexLogMessageQ);					// Lock LogConsoleEmailQ
     if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexLogMessageQ, "sem_wait@sem_tMutexLogMessageQ in Log_Console_Email_Thread", 1);}

     objMessage = queue_objOutputMessageQ->front();
     queue_objOutputMessageQ->pop();
     
     sem_post(&sem_tMutexLogMessageQ);

     // If logging has stopped .. resume when Q is emptied  
     if (((( vobjSortedOutput.size() == 0)&& boolSTOP_LOGGING) && boolShowOnce) ) { boolShowOnce = !ResumeLoggingMessages();}

     if (objMessage.intMessageCode == 523){boolSTOP_LOGGING = false;}

     // sort object into the vector
     if (!boolSTOP_LOGGING)
      {
       // Check size of queue vs capacity .. if % exceeded stop logging.. 
       if ( (((float)vobjSortedOutput.size()/vobjSortedOutput.capacity() ) > dblLOG_VECTORCLASS_BREAKER_PERCENTAGE) )
        {
         boolSTOP_LOGGING =  true;
         objMessage.fMessage_Create(LOG_WARNING, 521, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                    objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                    LOG_MESSAGE_521, int2strLZ(int(dblLOG_VECTORCLASS_BREAKER_PERCENTAGE*100)) );
         intLostMessages = LostMessageIncrement(intLostMessages);
        } 
       // insert object into the vector				       
       intX = 0;
       for ( it=vobjSortedOutput.begin() ; it < vobjSortedOutput.end(); it++ )
        {
         if (TimeCheckBefore(objMessage.rtimespecTimeStamp, it->rtimespecTimeStamp)){vobjSortedOutput.insert(it,objMessage);intX=1;break;}
        }
       if(intX == 0){vobjSortedOutput.push_back(objMessage);}
       
      }
     else 
      {
        intLostMessages = LostMessageIncrement(intLostMessages); 

      }//end if (!boolSTOP_LOGGING) else

     if (vobjSortedOutput.size() > sizeMaxLOGQSizeEncountered) {sizeMaxLOGQSizeEncountered = vobjSortedOutput.size();}

    }//while (!queue_objOutputMessageQ->empty()) 

  
   

   // empty vector objects if enough time has elapsed ...

   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

   while( ((!vobjSortedOutput.empty()) && ((time_difference(timespecTimeNow,vobjSortedOutput.front().rtimespecTimeStamp))> dblLOG_QUEUE_SORTDELAY_SEC)) )
    {
     objMessage = vobjSortedOutput.front();
     vobjSortedOutput.erase (vobjSortedOutput.begin());
     
     if(objMessage.intMessageCode == 523){boolShowOnce = true;}

     //Decode LogCode and perform actions........
     intX = objMessage.intLogCode;
     
     for (int i = NUMBER_OF_LOG_FUNCTIONS-1; i > 0; i--)
      {
       if (( intX / intLogDecoderArray[i]) >= 1.0)
        {
         switch (intLogDecoderArray[i])
          {
           case 1:
            switch(objMessage.enumMessageType)
             {
              case NORMAL_MSG: 
                     intRC = objMessage.fLog(READABLE_LOG);
                //     intRC = objMessage.fLog(XML_LOG);
                     if (boolDEBUG) {intRC = objMessage.fLog(DEBUG_LOG);}
                     break;
              case DEBUG_MSG:
                     if (boolDEBUG) {intRC = objMessage.fLog(DEBUG_LOG);}
                     break; 
             }
            if (BOOL_SEND_ECATS)  {
              switch (ECATS_PORT.ePortConnectionType) {
                case TCP: 
                     while (q_ECATS_Messages.size())  {
                        
                       intRC = ECATS_PORT.TCP_Port.SendLine(q_ECATS_Messages.front().c_str());
                       if (intRC) { break;}
                       else       { q_ECATS_Messages.pop_front();}                      
                     }

                     intRC = ECATS_PORT.TCP_Port.SendLine(objMessage.stringOutputMessage.c_str());
                     if (intRC)                       { 
                       if ( q_ECATS_Messages.size() > ECATS_MSG_BUFFER_SIZE) { cout << "ECATS MESSAGE" << endl;}
                       else                                                  {q_ECATS_Messages.push_back(objMessage.stringOutputMessage);}
                     }                      
                     break;
                case UDP:         
                     ECATS_PORT.UDP_Port.SetActive(true);
                     intRC = ECATS_PORT.UDP_Port.Send(objMessage.stringOutputMessage.c_str(), objMessage.stringOutputMessage.length());
                     if (intRC) { cout << "error sending to ECATS" << endl;}
                     break;
               default: 
                     SendCodingError("log.cpp *Log_Console_Email_Thread() -NO_CONNECTION_TYPE_DEFINED -2");
                     break;
              }
            }
            //if (intRC){cout << "error writing to file\n";}                      //TBC

            break;

           case 2:
            if((boolDISPLAY_CONSOLE_OUTPUT)||((objMessage.intLogCode == LOG_WARNING)||(objMessage.intLogCode == LOG_ALARM)||(objMessage.intLogCode == LOG_INFO)))
            {objMessage.fConsole();}
            break;

           case 4:
            if ((objMessage.intMessageCode > 1000)||(objMessage.intMessageCode < 0)) {SendCodingError( "log.cpp - Coding Error in Log_Console_Email_Thread in switch (intLogDecoderArray[i])");break;}
            if(!boolEmailSupress[objMessage.intMessageCode]){ objMessage.fEmail(EMAILINFO,false,false,(char*) "");}
         
            break;

           case 8:
            if ((objMessage.intMessageCode > 1000)||(objMessage.intMessageCode < 0)) {SendCodingError( "log.cpp - Coding Error in Log_Console_Email_Thread in switch (intLogDecoderArray[i])");break;}
            if(!boolEmailSupress[objMessage.intMessageCode]){ objMessage.fEmail(EMAILWARNING,false,false,(char*) "");}
                 
            break;

           case 16:
            if ((objMessage.intMessageCode > 1000)||(objMessage.intMessageCode < 0)) {SendCodingError( "log.cpp - Coding Error in Log_Console_Email_Thread in switch (intLogDecoderArray[i])");break;}
            if(!boolEmailSupress[objMessage.intMessageCode]){ objMessage.fEmail(EMAILALARM,false,false,(char*) "");}
      
            break;

           case 32:
            // Send to workstation TBC
            break;

           case 64:
            // Write to AMI script ....
            intRC = objMessage.fLog(AMI_SCRIPT); 
            break;

           default:
            //error
            SendCodingError("log.cpp - Coding Error in Logthread switch fn *Log_Console_Email_Thread()");
            break;
          }// end switch
        
         intX -= intLogDecoderArray[i];

        }// end if ((intX / intLogDecoderArray[i]) >= 1)
                                
      }// end for (int i = NUMBER_OF_LOG_FUNCTIONS-1; i > 0; i--)                   
                           
    
    }//end while (!queue_objOutputMessageQ->empty())


   // check for unsent mail in queue ... delay after 100 cycles if mail is in queue
   if (boolEMAIL_QUEUE_ACTIVE)
    {
     intDelayCounter++;
     if (intDelayCounter > 100){objEmailMsgLog.fIntializeHeaders(); objEmailMsgLog.fProcessEmailQueue(); intDelayCounter = 0;}
    }
 
 				
  } while (!boolSTOP_EXECUTION);






 objMessage.fMessage_Create(0, 599, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_599);
 objMessage.fConsole();

 
 return NULL;

}// end Log_Console_Email_Thread()




void *ECATS_TX(void *voidArg)
{
 Thread_Data                    ThreadDataObj; 
 int 				intRC;  
  
 extern vector <Thread_Data> 	ThreadData;
 extern Port_Connection_Type    ECATS_PORT_CONNECTION_TYPE;

 ThreadDataObj.strThreadName ="ECATS";
 //ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in log.cpp::ECATS_TX_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 while (!boolSTOP_EXECUTION) 
  {
    switch (ECATS_PORT_CONNECTION_TYPE)
     {
      case UDP:    
           ECATS_PORT.UDP_Port.DoEvents(); 
           break;
      case TCP:    
           if(!ECATS_PORT.TCP_Port.GetConnected()) 
            {
             ConnectToECATS_TCP_Port();
             for (int i = 1; i <= 3; i++) {if (!boolSTOP_EXECUTION) {sleep(1);}}
            }
           ECATS_PORT.TCP_Port.DoEvents(); 
           break;
      default:     
           break;
     }
  }

   switch (ECATS_PORT_CONNECTION_TYPE)
     {
      case UDP:    ECATS_PORT.UDP_Port.SetActive(false); break;
      case TCP:    ECATS_PORT.TCP_Port.Disconnect();     break;
      default:     break;
     }


 return NULL;								
}







/****************************************************************************************************
*
* Name:
*  Function: void *Log_Messaging_Monitor_Event()
*
*
* Description:
*   A thread spawned from Log_Console_Email_Thread() 
*
*
* Details:
*   This function is implemented as a timed loop thread from Log_Console_Email_Thread. It pauses on an interval and then
*
*   1. Checks for messages on the message Q, if none then sleep.
*
*   2. retrieves messages until Q is empty, processes them and pushes them onto the main message queue.
*
*
* Parameters: 				
*   void *voidArg			unused for now
*
* Variables:
*   intLogCode				Local   - priority of message received
*   intRC                               Local   - return code from functions
*   objMessage				Local   - MessageClass object
*   mq_attrMsgQAttr			Local   - <mqueue.h> structure holding the message Q attributes
*   sem_tMutexCadMessageQ		Global  - <semaphore.h> mutex to prevent multiple operations on the queue
*   sem_tMutexMainMessageQ		Global  - <semaphore.h> mutex to prevent multiple operations on the queue
*                                                                          
* Functions:
*   .empty()				Library <queue> 
*   .front()				Library <queue> 
*   .pop()				Library <queue>
*   .push()				Library <queue>
*   Semaphore_Error()                   Global  semaphore error handler
*   sem_wait()             		Library <semaphore.h>
*   sem_post()				Library <semaphore.h>
*
*
* Author(s):
*   Bob McCarthy 			Original: 12/08/2006
*                                       Updated : 3/2019
*
****************************************************************************************************/ 
void *Log_Messaging_Monitor_Event(void *voidArg)
{
 MessageClass			objMessage;
 int                            intRC;							
 Thread_Data                    ThreadDataObj;
 extern vector <Thread_Data>    ThreadData;
     
// queue <MessageClass>*  	queue_objOutputMessageQ;
// void 				*ptrVal 				= union_sigvalArg.sival_ptr;
  
// queue_objOutputMessageQ	= (queue <MessageClass>*) ptrVal;		// recast ptrVal

 // disable timer
// if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tLogMessageMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}

 ThreadDataObj.strThreadName = "LOG Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in log.cpp::Log_Messaging_Monitor_Event()", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

do
{

 experient_nanosleep(intLOG_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in Log_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexLogMessageQ);
 if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexLogMessageQ, "sem_wait@sem_tMutexLogMessageQ in Log_Messaging_Monitor_Event", 1);}

 while (!queue_objLogMessageQ.empty())
  {
    objMessage = queue_objLogMessageQ.front();
    queue_objLogMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 500:						
       //Log thread Created and running
       break;
 
      case 501:						
       // Port Down events
       break;

      case 502:						
       // Port Down Reminder events
       break;

      case 503:
       // Email failed to connect						
       break;

      case 504:						
       //Port Restored event
       break;

      case 505:						
       //Email Q emptied
       break;

      case 506:						
       //Email Q error
       break;

      case 507:
       //error opening log file						
       break;

      case 508:						
       //error writing to log file
       break;
      case 509:
       // Message Monitor Timer Event Initialized						
       break;

      case 510:						
       // Email failed to send
       break;
      case 511:						
       // Email failed to disconnect
       break;
      case 512:						
       //
       break;
      case 513:						
       //
       break;
      case 514:                                           
       //
       break;
      case 515:                                           
       //
       break;

      case 518:
       // signal semaphore failed to lock
       break;
      case 526:

       break;

      case 598:
       // Mutex semaphore error
       break;
      case 599:
       // thread complete
       break;
      case 999:
       // test area..................................
       
       //cout << "in log Message monitor test\n";
       
       break;
	
      default:
       // only trap the log messages 500 series pass along all others....
       break;
      // //cout << "test" << objMessage.intMessageCode << endl;                               //test
   }// end switch

    queue_objMainMessageQ.push(objMessage);
     
  }// end while
 sem_post(&sem_tMutexLogMessageQ);
 sem_post(&sem_tMutexMainMessageQ);


  // check counters .. At 00:00:01 Local
 int intTemp =  GetDayofMonth();
 if (intDayofMonth != intTemp) {intDayofMonth = intTemp; Check_Reset_Daily_Counters();}





/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tLogMessageMonitorTimerId, 0, &itimerspecLogMessageMonitorDelay, NULL);}
*/

}while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 596, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_596);
 objMessage.fConsole();
return NULL;
}// end Log_Messaging_Monitor_Event()



void Email_Filter_Update_Event(union sigval union_sigvalArg)
{
   // check to remove email message filtering
   objEmailSlam.fRefreshFilterData();
}


unsigned long long int LostMessageIncrement(unsigned long long int intArg)
{
 MessageClass                                   objMessage; 

 if (intArg == ULONG_LONG_MAX ) 
  {
   objMessage.fMessage_Create(LOG_WARNING, 524, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_524,  "Lost Message", int2strLZ(intArg));
   enQueue_Message(LOG,objMessage);
   return 0;
  }
 else {return ++intArg;}
}



bool ResumeLoggingMessages()
{
 MessageClass                                   objMessage; 
 extern unsigned long long  int                 intLostMessages;

 objMessage.fMessage_Create(LOG_WARNING, 523, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_523, int2strLZ(intLostMessages));
 enQueue_Message(LOG,objMessage);
 intLostMessages = 0;

return true;
}


/****************************************************************************************************
*
* Name:
*  Function: int ExperientEmail::fProcessEmailQueue()
*
*
* Description:
*  This is a <ExperientEmail> function implementation
*
* 
* Details:
*  The function processes an <ipworks><filemailer> class email queue. 
*  (the postfix queue is a seperate queue and is not handled by this program) 
*    
*   1. Check to see if the <ExperientEmail> class is connected.
*
*      (the ipworks function does not work properly if the class is not connected.  The
*       class is normally connected and there should not be any queued messages.  Since this
*       function has been called there is a non-normal situation in the email process.)
*
*   2. a. If the class is connected .  
*      
*         1. Process the Queue
*            if an error occurs then the email is lost.  Log the event.
*
*      b. If the class is not connected - Connect the <ExperientEmail> class.
*
*       (This is most likely the reason why we are getting messages queued. (i.e. the connection
*        was somehow terminated.)  Before we attempt to send messages in the queue, we need to demonstrate
*        that a connection can be established.  The function returns and if the connection is succcesful
*        the next call to this function will process the Queue IAW the above procedures.)
*
*    
* Parameters:
*   none
*
* Variables:
*   boolEMAIL_QUEUE_ACTIVE              Global - <globals.h> true if there are queued emails
*   charEMAIL_QUEUE_DIR                 Global - <defines.h> directory where the queued messages are stored
*   intRC                               Local  - integer for function return codes
*   LOG                                 Global - <header.h> threadorPorttype enumeration member
*   LOG_CONSOLE_FILE                    Global - <defines.h>
*   LOG_MESSAGE_505                     Global - <defines.h> 
*   LOG_MESSAGE_506                     Global - <defines.h> 
*   objMessage                          Local  - <MessageClass> object
*   stringErrorMsg                      Local  - <cstring> 
*                                                                          
* Functions:
*   Connect()                           <ipworks.h><filemailer>
*   enQueue_Message()                   <globalfunctions.h>
*   .fMessage_Create()                  <MessageClass>
*   GetConnected()                      <ipworks.h><filemailer>
*   GetLastError()                      <ipworks.h><filemailer>                 (char*)
*   ProcessQueue()                      <ipworks.h><filemailer>
*
* Author(s):
*   Bob McCarthy 			Original: 3/09/2007
*                                       Updated : N/A
*
****************************************************************************************************/

int ExperientEmail::fProcessEmailQueue()
{
 string         stringErrorMsg;
 int            intRC;
 MessageClass   objMessage;



// Connect();
// DoEvents();
 if (!GetConnected())  {Connect();}

 if (!GetConnected()) {return 1;} 
 
 // check if connected


   intRC = ProcessQueue((char*)charEMAIL_QUEUE_DIR);
   if (intRC)
    {
     
     if (!boolLastEmailFailedToSend)
      {
       stringErrorMsg = GetLastError();
       objMessage.fMessage_Create(LOG_CONSOLE_FILE,506, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                  objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_506, int2strLZ(intRC), stringErrorMsg); 
       enQueue_Message(LOG,objMessage);
      }
     boolLastEmailFailedToSend = true;
     return 1;
    }
   else 
    {
     // Old Email(s) in Queue Just Sent
   //  objMessage.fMessage_Create(LOG_WARNING,505,objBLANK_CALL_RECORD, objBLANK_LOG_PORT, LOG_MESSAGE_505); 
   //  enQueue_Message(LOG,objMessage);
     boolEMAIL_QUEUE_ACTIVE = false;

    }//end if (intRC) else


 
 return 0;
}// end fProcessEmailQueue()

bool  ExperientEmail::fSendTestMail()
{
 int    intRC;
 string strEmailAddress;
 string strKey = IPWORKS_RUNTIME_KEY;

 if (!boolEmailON) {
  cout << "Email is Disabled" << endl;
  return boolEmailON;
 }

 cout << "Enter Test Email Address :";
 getline (cin, strEmailAddress); 

 if (!spc_email_isvalid((const char*) strEmailAddress.c_str())){
  cout << endl << "INVALID EMAIL ADDRESS" << endl << endl; 
  return false;
 }

 fIntializeHeaders();
 SetSendTo((char*) strEmailAddress.c_str());
 SetCc(charEMAIL_SEND_TO_ALARM);
 SetSubject((char*)"TEST");
 SetMessageText((char*)"test");
 intRC = Send();
 if (intRC) { cout << "Email send error Code =" << intRC << endl << endl;   return false;}
 else       { cout << "Email sent to: " << strEmailAddress << endl << endl; return true;}


}

/****************************************************************************************************
*
* Name:
*  Function: int MessageClass::fEmail(email_type enumEmailType, bool boolCC, bool boolAttach , char* charAttachment )
*
*
* Description:
*  MessageClass function implementation
*
*
* Details:
*   this function emails out MessageClass messages utilizing ipworks library functions of the Class <ExperientEmail><FileMailer>
*
*
* Parameters:
*   boolAttach                          bool    default value false
*   boolCC                              bool    default value false 				
*   charAttachment                      char*   default value ""
*   charMessageText                     char*
*   charSubject                         char*
*
* Variables:
*   boolEMAIL_QUEUE_ACTIVE              Global  <globals.h> set to true if emails are placed into the queue

*   charEMAIL_FROM                      Global  <defines.h>
*   boolEmailON                         Global  <globals.h>

*   charSMTP_PASSWORD                   Global  <defines.h>
*   charSMTP_SERVER_NAME                Global  <defines.h>
*   charSMTP_USER_NAME                  Global  <defines.h>
*   charSubject[]                       Local   char array to pass to <FileMailer> function
*   charTextMessage[]                   Local   char array to pass to <FileMailer> function
*   filemessage                         Local   <ipworks> <FileMailer>
*   i                                   Local   general purpose integer
*   intEMAIL_LOG_MSG_START_POS          Global  <defines.h>
*   intEMAIL_SUBJECT_MAX_LENGTH         Global  <defines.h>
*   intRC                               Local   return code
*   LOG_MESSAGE_503                     Global  <defines.h> error message
*   LOG_MESSAGE_510                     Global  <defines.h> error message
*   objEmailMsg                         Global  <ExperientEmail>
*   objMessage                          Class   <MessageClass>
*   stringErrorMsg                      Local   <cstring>
*   stringSubject                       Local   <cstring>
                                                                      
* Functions:
*   .append()                           Library <cstring>
*   .Connect()                          Library <ipworks> <FileMailer>
*   .Disconnect()                       Library <ipworks> <FileMailer>
*   email_headers_init()                Global  <globalfunctions.h>
*   .fIntializeHeaders()                Global  <ExperientEmail>
*   .GetLastError()                     Library <ipworks> <FileMailer>
*   .Queue()                            Library <ipworks> <FileMailer>
*   .SetAttachmentCount()               Library <ipworks> <FileMailer>
*   .SetAttachments()                   Library <ipworks> <FileMailer>
*   .SetCc()                            Library <ipworks> <FileMailer>
*   .SetFrom()                          Library <ipworks> <FileMailer>
*   .SetImportance()                    Library <ipworks> <FileMailer>
*   .SetMailServer()                    Library <ipworks> <FileMailer>
*   .SetMessageText()                   Library <ipworks> <FileMailer> 
*   .SetPassword()                      Library <ipworks> <FileMailer>
*   .Send()                             Library <ipworks> <FileMailer>
*   .SetSendTo()                        Library <ipworks> <FileMailer>
*   .SetSubject()                       Library <ipworks> <FileMailer>
*   .SetUser()			        Library <ipworks> <FileMailer>
*
*
* Author(s):
*   Bob McCarthy 			Original: 2/19/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 

int MessageClass::fEmail(email_type enumEmailType, bool boolCC, bool boolAttach , string strAttachment  )
{
 
 ExperientEmail                 objEmail;
 int                            intRC;
 MessageClass                   objMessageOut;
 char*                          charTextMessage;
 char*                          charSubject;
 string                         stringErrorMsg = "";
// string                         stringSubject  = "";
 string                         stringMessage  = "";
 string                         stringTemp     = "";


// extern ExperientEmail          objEmailMsg;

 if (!boolEmailON ) {return 0;}



 stringMessage = stringEmailMessageBody;
 objEmail.fIntializeHeaders(); 

 switch (enumEmailType)
  {
   case EMAILINFO:
    objEmail.SetSendTo(charEMAIL_SEND_TO);

    break;

   case EMAILWARNING:
    objEmail.SetSendTo(charEMAIL_SEND_TO);

    break;

   case EMAILALARM:
    objEmail.SetSendTo(charEMAIL_SEND_TO);
    objEmail.SetCc(charEMAIL_SEND_TO_ALARM);

    break;
   }//end switch

  if (!objEmailSlam.CheckEmailSlamFilter(intMessageCode, rtimespecTimeStamp.tv_sec))
  {
   if (objEmailSlam.EmailFiltering[intMessageCode]) {objEmailSlam.fSupressedEmailCount(intMessageCode);return 0;}
   stringSubject = Create_Message(LOG_MESSAGE_527, int2strLZ(intMessageCode));
   objEmailSlam.EmailFiltering[intMessageCode] = true;
  }

  // convert to char* ( filemailer class constraint)
  charSubject = (char*) stringSubject.c_str();

  //check if message text too long...convert to char*
 if (stringEmailMessageBody.length() > intEMAIL_MESSAGE_MAX_LENGTH)
  { 
    stringTemp = charEMAIL_MESSAGE_CROPPED + stringEmailMessageBody;
    stringTemp.erase( intEMAIL_MESSAGE_MAX_LENGTH, string::npos); 
    charTextMessage = (char*) stringTemp.c_str();
  }
 else
  {
   if (enumEmailType == EMAILALARM)
    { 
     // remove extra data for ALARM
 //    charSubject = (char*) stringSERVER_HOSTNAME.c_str();
 //    stringMessage = stringAlarmMessageText;  
    }
   // convert to char*
   charTextMessage = (char*) stringMessage.c_str();
  }

// if (enumEmailType == EMAILALARM) {cout << "CC =" << objPortData.strVendorEmailAddress << endl;}
 if((objPortData.boolVendorEmailAddress)&& (enumEmailType == EMAILALARM))    
   {objEmail.SetCc((char*)objPortData.strVendorEmailAddress.c_str());}
 
 objEmail.SetSubject(charSubject);
 objEmail.SetMessageText(charTextMessage);

 // check if connected  
  if (!objEmail.GetConnected())
   {
   //connect                       
   intRC = objEmail.Connect();
   if (intRC)
    {
     stringErrorMsg = objEmail.GetLastError();
     // error connecting .. queue message
     objMessageOut.fMessage_Create(LOG_CONSOLE_FILE,503, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_503, int2strLZ(intRC), stringErrorMsg ); 
     enQueue_Message(LOG,objMessageOut);
     //re-assign message text due to blank text anomaly
     objEmail.SetMessageText(charTextMessage);
     objEmail.Queue((char*) charEMAIL_QUEUE_DIR);
     boolEMAIL_QUEUE_ACTIVE = true;
     return intRC;
    }
   }//end if (!objEmailMsg.GetConnected())

 //send email
 intRC = objEmail.Send();
 if (intRC)
  {
   // error sending queue message
   stringErrorMsg = objEmail.GetLastError();
   objMessageOut.fMessage_Create(LOG_CONSOLE_FILE,510, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                 objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_510, int2strLZ(intRC), stringErrorMsg ); 
   enQueue_Message(LOG,objMessageOut);
   //re-assign message text due to blank text anomaly
   objEmail.SetMessageText(charTextMessage);
   objEmail.Queue((char*)charEMAIL_QUEUE_DIR);
   boolEMAIL_QUEUE_ACTIVE = true;
   return intRC;
  }

 return 0;

}// end int MessageClass::fEmail()


/****************************************************************************************************
*
* Name:
*  Function: void ExperientEmail::fIntializeHeaders()
*
*
* Description:
*  This is an ExperientEmail Class function implementation
*
*
* Details:
*  The function sets the constant headers for an email message
*    
* Parameters:
*   none
*
* Variables:
*   charEMAIL_FROM                      Global  - <defines.h>
*   charSMTP_PASSWORD                   Global  - <defines.h>
*   charSMTP_SERVER_NAME                Global  - <defines.h>
*   charSMTP_USER_NAME                  Global  - <defines.h>
*   HOST_IP                             Global  - <IP_Address> object
*   intEMAIL_TIMEOUT_SEC                Global  - <defines.h>
*   
* Functions:
*   Config()                            <ipworks.h> <FileMailer>
*   ResetHeaders()                      <ipworks.h> <FileMailer>
*   SetFrom()                           <ipworks.h> <FileMailer>
*   SetLocalHost()                      <ipworks.h> <FileMailer>
*   SetMailServer()                     <ipworks.h> <FileMailer>
*   SetMessageDate()                    <ipworks.h> <FileMailer>
*   SetPassword()                       <ipworks.h> <FileMailer>
*   SetTimeout()                        <ipworks.h> <FileMailer>
*   SetUser()                           <ipworks.h> <FileMailer>
*  
* Author(s):
*   Bob McCarthy 			Original: 3/08/2007
*                                       Updated : N/A
*
****************************************************************************************************/
void ExperientEmail::fIntializeHeaders()
{
 string strKey = IPWORKS_RUNTIME_KEY;

 ResetHeaders();
 SetMessageDate((char*)"*");
#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 SetLocalHost(charEMAIL_LOCAL_HOST); 
// SetMailServer((char*)"localhost");
 SetMailServer(strEMAIL_RELAY_SERVER.c_str());
 SetOtherHeaders((char*)"Content-Type: text/plain");

 if (!strEMAIL_RELAY_USER.empty())     {SetUser(strEMAIL_RELAY_USER.c_str());}
 if (!strEMAIL_RELAY_PASSWORD.empty()) {SetPassword(strEMAIL_RELAY_PASSWORD.c_str());}

 SetTimeout(intEMAIL_TIMEOUT_SEC);
 SetFrom(charEMAIL_FROM);
 Config((char*)"KeepQueue=false");

}//end fIntializeHeaders()

bool EmailSlam::CheckEmailSlamFilter(int MessageCode, time_t time_tTimeofMessage)
{
 time_t 	time_tTimeNow;
 time_t         time_tEarliestTime = time_tTimeofMessage;
 int            index = 0;


 time_tTimeNow = time(NULL);
 sem_wait(&sem_tMutexFilterData);

 for (int i = 0; i < EMAIL_MAX_MESSAGE_CODE_PER_HR; i++)
  {
   if ( (FilterData[MessageCode] [i] == 0)||((time_tTimeNow - FilterData[MessageCode] [i]) > HOUR_EXP_IN_SEC) )
   {FilterData[MessageCode] [i] = time_tTimeofMessage; sem_post(&sem_tMutexFilterData); return true;}
   if (FilterData[MessageCode] [i] < time_tEarliestTime){index = i; time_tEarliestTime = FilterData[MessageCode] [i];}
  }

 FilterData[MessageCode] [index] = time_tTimeofMessage;
 
 sem_post(&sem_tMutexFilterData);
 return false;
}

void EmailSlam::fSupressedEmailCount(int MessageCode)
{
 if (LostEmailCount[MessageCode] == ULLONG_MAX) {return;}
 LostEmailCount[MessageCode]++;

}

void EmailSlam::fRefreshFilterData()
{
 MessageClass	objMessage;
 time_t		time_tTimeNow;

 time_tTimeNow = time(NULL);
 sem_wait(&sem_tMutexFilterData);

 for (int i = 1; i <= LOG_MAX_NUMBER_OF_MESSAGE_CODES; i++)
  {
   for (int j = 0; j < EMAIL_MAX_MESSAGE_CODE_PER_HR; j++)
    {
     if ((time_tTimeNow - FilterData[i] [j]) > HOUR_EXP_IN_SEC)
      {
       FilterData[i] [j] = 0;
       if (EmailFiltering[i])
        {
         objMessage.fMessage_Create(LOG_INFO, 528, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                    objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_528, int2str(i), int2str(LostEmailCount[i]) ); 
         enQueue_Message(LOG,objMessage); 
         EmailFiltering[i] = false;
         LostEmailCount[i] = 0;

        }// end if (EmailFiltering[i])
      }// end if ((time_tTimeNow - FilterData[MessageCode] [i]) > HOUR_EXP_IN_SEC)
    }// end for (int j = 0; j < EMAIL_MAX_MESSAGE_CODE_PER_HR; j++)
  }//end for (int i = 1; i <= LOG_MAX_NUMBER_OF_MESSAGE_CODES; i++)
 
 sem_post(&sem_tMutexFilterData);
}


void Check_Reset_Daily_Counters()
{
 MessageClass                           objMessage;
 vector <string>                        stringSideAorB(3);
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort               CADPort[NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort               ANIPort[NUM_ANI_PORTS_MAX+1];
 extern ExperientCommPort               WRKThreadPort;
// extern ExperientCommPort               ServerMasterSlaveSyncPort;
// extern ExperientCommPort               ServerMasterSlaveGatewayPort;

 stringSideAorB[1] = "-A";
 stringSideAorB[2] = "-B";  

 if (boolCAD_EXISTS){ for (int i = 1; i <= intNUM_CAD_PORTS; i++){ CADPort[i].UDP_Port.fCheckResetDiscardedPacketCounter();} }
 for (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++){ for (int j = 1; j <=2 ; j++){ ALIPort[i][j].UDP_Port.fCheckResetDiscardedPacketCounter();} }
 for (int i = 1; i <= intNUM_ANI_PORTS; i++){ ANIPort[i].UDP_Port.fCheckResetDiscardedPacketCounter();}
 WRKThreadPort.UDP_Port.fCheckResetDiscardedPacketCounter();
// if (!boolSINGLE_SERVER_MODE){ServerMasterSlaveSyncPort.UDP_Port.fCheckResetDiscardedPacketCounter();}
// if (!boolSINGLE_SERVER_MODE){ServerMasterSlaveGatewayPort.UDP_Port.fCheckResetDiscardedPacketCounter();}

 if(intSEMAPHORE_ERRORS)
  {
   objMessage.fMessage_Create(LOG_WARNING,539, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_539, int2str(intSEMAPHORE_ERRORS)); 
   enQueue_Message(MAIN,objMessage);
  }
 intSEMAPHORE_ERRORS = 0;

 if(intLogFileWriteError)
  {
   objMessage.fMessage_Create(LOG_WARNING,540, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_540, int2str(intLogFileWriteError)); 
   enQueue_Message(MAIN,objMessage);
  }
 intLogFileWriteError = 0;

  if(intLogFileOpenError)
  {
   objMessage.fMessage_Create(LOG_WARNING,541, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_541, int2str(intLogFileOpenError)); 
   enQueue_Message(MAIN,objMessage);
  }
 intLogFileOpenError = 0;

 if(intSignalSemaphoreFailureCounter)
  {
   objMessage.fMessage_Create(LOG_WARNING,542, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_542, int2str(intSignalSemaphoreFailureCounter)); 
   enQueue_Message(MAIN,objMessage);
  }
 intSignalSemaphoreFailureCounter = 0;
  
 if(intSyncGatewayPingCounter)
  {
   objMessage.fMessage_Create(LOG_WARNING,543, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_543, int2str(intSyncGatewayPingCounter)); 
   enQueue_Message(MAIN,objMessage);
  }
 intSyncGatewayPingCounter = 0;

 CheckFreeDiskSpace();





}      

int LogFileYear(string stringArg)
{
 string sYear;
 size_t Position;

 if (stringArg.length() < 8) {return 0;}
 Position = stringArg.find_first_of("-");
 if (Position == string::npos){return 0;}
 sYear = stringArg.substr(0,Position);
 if(!Validate_Integer(sYear)){return 0;}

 return char2int(sYear.c_str());
}

int LogFileMonth(string stringArg)
{
 string sMonth;
 size_t Start, End;

 if (stringArg.length() < 8) {return 0;}
 Start = stringArg.find_first_of("-");
 if (Start == string::npos){return 0;}
 End   = stringArg.find_first_of("-", Start+1);
 if (End == string::npos){return 0;}
 sMonth = stringArg.substr(Start+1,(End-Start-1));
 if(!Validate_Integer(sMonth)){return 0;}

 return char2int(sMonth.c_str());
}





/****************************************************************************************************
*
* Name:
*  Function: void RemoveOldLogFiles()
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function traverses the log file directory and deletes old files as determined by
* the configuration of the system.  A Global integer intMONTHS_TO_KEEP_LOG_FILES will cause the function
* to immediately return if set to zero. The the age of each logfile will be calculated from it's name and
* compared against the value of intMONTHS_TO_KEEP_LOG_FILES.  If the age is greater it will be removed.
* 
*   For Debugging Purposes the Library function opendir() may set errno to the following:
* 13 EACCES -Permission denied. 
* 24 EMFILE - Too many file descriptors in use by process. 
* 23 ENFILE - Too many files are currently open in the system. 
* 2  ENOENT - Directory does not exist, or name is an empty string. 
* 12 ENOMEM - Insufficient memory to complete the operation. 
* 20 ENOTDIR - name is not a directory. 
*
*   For Debugging Purposes the Library function stat() may set errno to the following:       
* 13 EACCES: Search permission is denied for one of the directories in the path prefix of path.    
* 9  EBADF - filedes is bad.  
* 14 EFAULT - Bad address.   
* 40 ELOOP - Too many symbolic links encountered while traversing the path.    
* 36 ENAMETOOLONG - File name too long.   
* 2  ENOENT - A component of the path path does not exist, or the path is an empty string.   
* 12 ENOMEM - Out of memory (i.e. kernel memory).   
* 20 ENOTDIR - A component of the path is not a directory.
*
*   For Debugging Purposes the Library function readdir() and closedir() may set errno to the following: 
* 9  EBADF - Invalid directory stream descriptor dir.
*
* Parameters:
* None
*
* Variables:
*  charLOG_FILE_PATH_NAME               Global - <defines.h>                Path to the log files
*  Currentfile                          Local  - <dirent.h><dirent*>        Current file being checked
*  d_name[256]				Local  - <dirent.h><dirent>         Struct member contains file name
*  errno                                Global - <errno.h>
*  iAgeLogFileMonths                    Local  - integer                    calculated age of log file in months
*  iCurrentMonth                        Local  - integer                    Current month 1-12
*  iCurrentYear                         Local  - integer                    Current year 4 digit
*  iLogFileMonth                        Local  - integer                    Log file month 1-12
*  iLogFileYear                         Local  - integer                    4 digit year of log file 
*  intMONTHS_TO_KEEP_LOG_FILES          Global - <globals.h>                How long to keep files
*  intRC				Local  - integer                    return codes
*  iStatErrorCode                       Local  - integer                    Stores the value of errno if fn stat() fails
*  LOG                                  Local  - <header.h><threadorPorttype> enumeration member
*  LogDirectory 			Local  - <dirent.h><DIR *>          The Log file Directory
*  LOG_MESSAGE_532                      Global - <defines.h>                Output Warning Message
*  LOG_MESSAGE_533                      Global - <defines.h>                Output Warning Message
*  LOG_MESSAGE_534                      Global - <defines.h>                Output Warning Message
*  LOG_MESSAGE_535                      Global - <defines.h>                Output Warning Message
*  LOG_MESSAGE_536                      Global - <defines.h>                Output Warning Message
*  LOG_MESSAGE_537                      Global - <defines.h>                Output Warning Message
*  LOG_MESSAGE_538                      Global - <defines.h>                Output Warning Message
*  LOG_WARNING                          Global - <defines.h>                Log Code
*  objBLANK_CALL_RECORD                 Global - <Call_Data>                Blank object of a Call Record
*  objBLANK_LOG_PORT                    Global - <Port_Data>                Blank object of Port Data
*  objMessage                           Local  - <MessageClass>             object for passing messages
*  Statistics                           Local  - <unistd><sys/stat.h><stat> Stucture holding file attributes
*  .st_mode                             Local  - <sys/stat.h><stat>         Structure member
*  strCurrentMonthFileName              Local  - <cstring>                  Log File Name for current Month
*  strLogFilePath                       Local  - <cstring>                  path of log file
*  strPathandFileName		        Local  - <cstring>                  file & path name to be passed to stat function
*                                                                          
* Functions:
*   closedir()                          Library  - <dirent.h>           (int)
*   .c_str()				Library  - <cstring>            (const char*)
*   enQueue_Message()			Global   - <globalfunctions.h>  (void)
*   .erase()                            Library  - <cstring>            (*this)
*   .fMessage_Create()                  Global   - <MessageClass>       (void)
*   GetLogFileName()                    Global   - <globalfunctions.h>  (string)
*   int2strLZ()                         Global   - <globalfunctions.h>  (string)
*   LogFileMonth()                      Local    -                      (int)
*   LogFileYear()                       Local    -                      (int)
*   opendir()                           Library  - <dirent.h>           (DIR*)
*   readdir()                     	Library  - <dirent.h>           (struct dirent)
*   remove()                            Library  - <stdio.h>            (int)
*   S_ISREG()                           Library  - <unistd.h>           (bool)
*   stat()                              Library  - <unistd.h>           (int)
*   time()                     		Library  - <ctime>              (int)
*
* AUTHOR: 10/05/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void RemoveOldLogFiles()
{
 DIR *		LogDirectory;
 struct stat    Statistics;
 struct dirent* Currentfile;
 int            intRC;
 string         strPathandFileName;
 string         strCurrentMonthFileName;
 string         strLogFilePath = charLOG_FILE_PATH_NAME;
 int            iCurrentYear, iLogFileYear;
 int            iCurrentMonth, iLogFileMonth;
 int            iStatErrorCode = 0;
 unsigned int   iAgeLogFileMonths;
 MessageClass   objMessage;

 if (!intMONTHS_TO_KEEP_LOG_FILES){return;}

 LogDirectory =  opendir(charLOG_FILE_PATH_NAME);
 if (!LogDirectory) 
  {
   objMessage.fMessage_Create(LOG_WARNING,536, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_536, int2strLZ(errno));
   enQueue_Message(LOG,objMessage);
   return;
  }

 // generate current logfile name
 strCurrentMonthFileName = GetLogFileName(READABLE_LOG);
 strCurrentMonthFileName.erase(0,strLogFilePath.length()+1);
 iCurrentYear  =  LogFileYear(strCurrentMonthFileName);
 iCurrentMonth =  LogFileMonth(strCurrentMonthFileName);

  if ((iCurrentYear == 0)||(iCurrentMonth == 0)) 
   {
    objMessage.fMessage_Create(LOG_WARNING,534, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_534);
    enQueue_Message(LOG,objMessage);
    return;
   }

 errno=0;
 // parse all the files in the directory
 while ((Currentfile=readdir(LogDirectory)))
 {
  strPathandFileName = charLOG_FILE_PATH_NAME;
  strPathandFileName += "/";
  strPathandFileName += Currentfile->d_name;
  
  intRC = stat(strPathandFileName.c_str(), &Statistics);
  if (intRC < 0) {iStatErrorCode = errno; errno=0; continue;}

  // only process "regular files"
  if(S_ISREG(Statistics.st_mode) )
   {
    iLogFileYear  = LogFileYear(Currentfile->d_name);
    iLogFileMonth = LogFileMonth(Currentfile->d_name);
    if ((iLogFileYear == 0)||(iLogFileMonth == 0)) {continue;}
    iAgeLogFileMonths = (((iCurrentYear - iLogFileYear) * 12) + (iCurrentMonth - iLogFileMonth));
 
    // remove file if over age limit
    if ( iAgeLogFileMonths > intMONTHS_TO_KEEP_LOG_FILES)
     {
      intRC = remove(strPathandFileName.c_str());
      if (intRC)
       {
        objMessage.fMessage_Create(LOG_WARNING,537, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_537, Currentfile->d_name, int2strLZ(errno));
        enQueue_Message(LOG,objMessage); errno = 0;      
       }
      else
       {
        objMessage.fMessage_Create(LOG_INFO,538, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_538, Currentfile->d_name);
        enQueue_Message(LOG,objMessage);
       }
     }// end if (iAgeLogFileMonths > intMONTHS_TO_KEEP_LOG_FILES)
   }// end if(S_ISREG(Statistics.st_mode) )
 
 }// end while ((Currentfile=readdir(LogDirectory)))

 // check if any errors occured during the reading/ traversing. There is a possibility of losing error codes
 // if there are multiple errors ... but we should get the last error here ..
 if (errno)
  {
   objMessage.fMessage_Create(LOG_WARNING,532, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_532, int2strLZ(errno));
   enQueue_Message(LOG,objMessage);
  }
 if (iStatErrorCode)
  {
   objMessage.fMessage_Create(LOG_WARNING,533, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_533, int2strLZ(iStatErrorCode));
   enQueue_Message(LOG,objMessage);
  }

 intRC = closedir(LogDirectory);
 if (intRC < 0)
  {
   objMessage.fMessage_Create(LOG_WARNING,535, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_535, int2strLZ(errno));
   enQueue_Message(LOG,objMessage);
  }

 return;
}

int ConnectToECATS_TCP_Port()
{
 if (boolSTOP_EXECUTION) {return 0;}
 if(!boolSTOP_EXECUTION) {ECATS_PORT.TCP_Port.SetConnected(true);}
 return 0;
}



