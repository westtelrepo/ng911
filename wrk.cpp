/*****************************************************************************
* FILE: wrk.cpp
*
* DESCRIPTION: This file contains all of the functions integral to the WRK Thread
*
* Features:  1. NENA Compliant                  Complies with the NENA Standards for E9-1-1
*            1. Faster Communication:           Utilizes UDP Communications to connect to Workstations
*            2. Denial of Service Protection:   Ignores Packets of wrong length from non-registerd IP Adresses. 
*            3. Alarm Notification:             Instant Text Message for Major Disruptions
*            4. Email Nofification:             Warnings, Information, and Alarms and Reminders
*            5. Thread Implementation           Provides efficient utilization and reduces resource monopolization
*            6. Configurable                    Controller sends configuration signals to workstation 
*            7. Expandable                      Ability to connect with N number of Workstations. 
*         
* AUTHOR: 10/02/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//external class based variables
extern Thread_Trap                              OrderlyShutDown;
extern Thread_Trap                              UpdateVars;
extern queue <MessageClass>                     queue_objMainMessageQ;
extern queue <MessageClass>                     queue_objWRKMessageQ;
extern IP_Address                               HOST_IP;
extern queue <ExperientDataClass>               queue_objWRKData;
extern IP_Address                               MULTICAST_GROUP;
extern Text_Data                                objBLANK_TEXT_DATA;
extern Call_Data                                objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_WRK_PORT;
extern WorkStation                              WorkStationTable[NUM_WRK_STATIONS_MAX+1];
extern int                                      intNUM_WRK_STATIONS;
extern Initialize_Global_Variables              INIT_VARS;

//forward function Declarations
//void WRK_Messaging_Monitor_Event(union sigval union_sigvalArg);
void Workstation_Check_Timed_Event(union sigval union_sigvalArg);
void *WRK_Port_Listen_Thread(void *voidArg);
void SendStrayACKToWorkStations();

extern void *WRK_Messaging_Monitor_Event(void *voidArg);


// WRK Global VARS

struct itimerspec                   itimerspecWRKMessageMonitorDelay;
struct itimerspec                   itimerspecWRKtimedEventDelay;
timer_t                             timer_tWRKMessageMonitorTimerId;
timer_t                             timer_tWRKTimedEventTimerId;
sem_t                               sem_tMutexWRKWorkTable;
sem_t                               sem_tMutexWRKMessageQ;
sem_t                               sem_tMutexWRKPortDataQ;
sem_t                               sem_tMutexWRKInputQ;
sem_t                               sem_tWRKFlag;
ExperientCommPort                   WRKThreadPort;


queue <DataPacketIn>	                        queueWRKPortData;

int                                             intWRK_LOCAL_LISTEN_PORT;
int                                             intWRK_HEARTBEAT_INTERVAL_SEC;
string                                          stringTDD_DEFAULT_LANGUAGE;
int                                             intTDD_SEND_DELAY;
string                                          stringTDD_SEND_TEXT_COLOR;
string                                          stringTDD_RECEIVED_TEXT_COLOR;
int                                             intTDD_CPS_THRESHOLD;
string                                          stringTRANSFER_DEFAULT_GROUP;
string                                          stringMSRP_DEFAULT_LANGUAGE;
string                                          stringMSRP_SEND_TEXT_COLOR;
string                                          stringMSRP_RECEIVED_TEXT_COLOR;
int                                             intXMLength;
bool                                            boolGUI_SIP_PHONE_EXISTS;
string                                          strMAPPING_REGEX;

/****************************************************************************************************
*
* Name:
*   Function void *WRK_Thread(void *voidArg)
*
*
* Description:
*   Handles all of the Workstation related activity for the controller. 
*
*
* Details:
*   This function is implemented as a detached thread from the main program. The program recieves a 
*   pointer to the input queue from main() as its argument.
*
*   The Thread spawns one UDP Port listening thread and creates one timer event thread:
*  
*    a. WRK_Messaging_Monitor_Event:  Utilized to send error communications to main.
*    b. WRK_Port_Listen_Thread     :  UDP Listen Port
*   
*   A global semaphore is used to recieve a signal from three sources :
*
*     a. Data (from Main) has been pushed onto the WRK's input queue.
*     b. Data received from the WRK port pushed onto the Data queue.
*     c. Shutdown signal from main.
*
*   The main loop idles waiting for a signal, it checks for the shutdown signal, if able to continue it processes 2 queues the first is 
*   queueWRKPortData, the second is the queue_objWRKData.
*
*   1. queueWRKPortData:  XML data is popped from the queue and validated, then deciphered for the type of command:
* 
*      a. Heartbeat
*         (1) If this is the first heartbeat from a valid workstation then the workstation is:
*             a. registerd
*             b. init message sent
*             c. active records from the main worktable sent (update grid)
*         (2) subsequent heartbeats keep station active
*
*      b. Rebid
*         (1) Rebid request sent to main
*
*      c. Update CAD
*         (1) Update CAD request sent to main
*
*      d. Transfer
*         (1) Transfer request sent to main
*
*      e. Log 
*         (1) Workstation Log code/message sent to main (750-799)
*
*
*   2. queue_objWRKData: ExperientDataClass object is popped from the queue and processed to be broadcast to the workstation(s)
*
*      a. broadcast XML to all workstations
*
*      b. broadcast XML to a single workstation
*
*      c. The XML to the workstation contains the folowing keys:
* 
*         (1) General:       used to initialize the workstation
*         (2) TDD            used to initialize the workstation
*         (3) Transfer       used to initialize the workstation
*         (4) CallInfo:      Ringing, Connected, Abandoned, Disconnected, ALI Request, Manual ALI Request, ALI Received, ALI Request Failed,
*                            Sent to CAD, ALI No Update
*         (5) Alarm:         Shutdown & PopUp
*         (6) ClearConsole:  used to erase stale data from the workstation
*
*
* Parameters: 				
*   void *voidArg	      		not used
*
*
* Variables:                                          
*.   boolActive                          Global  - <WorkStation> member.. true if workstation active
*.   boolWRKThreadReady                  Global  - <Thread_Trap> member.. volatile bool used to show that CAD thread is ready to shut down
*.   boolDEBUG                           Global  - <globals.h> display debug MSGs if true
*   boolFirstACKRcvd                    Global  - <ExperientCommPort> member.. true when port first ACK recieved
*   boolHeartbeatRequired               Global  - <ExperientCommPort> member.. true if port requires heartbeats
*   boolNAK				Local   - Data is a NAK if true
*.   boolPingStatus                      Global  - <ExperientUDPPort> member
*.   boolPortActive                      Global  - <ExperientCommPort> member.. true if port is awaiting answer to data TX 
*.   boolReadyToShutDown                 Global  - <ExperientCommPort> member
*   boolReducedHeartBeatMode            Global  - <ExperientCommPort> member.. true if port is in Reduced Heartbeat mode
*   boolReducedHeartBeatModeFirstAlarm  Global  - <ExperientCommPort> member.. true if Alarm message has been sent
*.   boolUnexpectedDataShowMessage       Global  - <ExperientCommPort> member
*   boolSTOP_EXECUTION            	Global	- when true, thread lock loop engages for shutdown
*.   CLOCK_REALTIME            		Library - <ctime> real time clock constant
*   CAD_LOCAL_PORT[]                    Global  - configuration array
*   CADPort[]                           Global  - <ExperientCommPort> the CAD Port(s)
*   CadPortDataRecieved			Local   - <DataPacketIn> struct holding Cad Port Data recieved
*.   enumPortType                        Global  - <ExperientCommPort><ipworks.h><IPPort> (threadorPorttype) member
*   enumThreadType                      Global  - <Thread_Statistic_Counters> (threadorPorttype) member
*.   errno                     		Library - <errno.h> - error handling return code
*   ETIMEDOUT                 		Library - <semaphore.h> - indicates a semaphore timeout
*   HOST_IP                             Global  - <IP_Address> object
*   i                         		Local	- general purpose integer variable
*   intCadActiveTrunk                   Global  - trunk number of active record
*   intCAD_CLEANUP_INTERVAL_NSEC 	Global	- <defines.h>
*   intCAD_HEARTBEAT_INTERVAL_SEC       Global  - <defines.h>
*   intCAD_PORT_LISTEN_INTERVAL_NSEC    Global  - <defines.h>
*.   intWRK_MESSAGE_MONITOR_INTERVAL_NSEC Global  - <defines.h>
*   intDataToDisplay                      Local   - amount of data to display for responses greater than 1
*.   intHeartbeatInterval                 Local  - <ExperientCommPort> member.. heartbeat interval of port
*   intLength                             Local  - <DataPacketIn> member
*.   intLocalPort                         Local   -  <ExperientCommPort> member
*.   intMessageNumber                     Local   - Log thread handling code
*.   intNUM_WRK_STATIONS                  Global  - <globals.h>
*   intNUM_TRUNKS_INSTALLED               Global  - <defines.h>
*   intPING_TIMEOUT                       Global  - <defines.h>
*   intPORT_DOWN_REMINDER_SEC             Global  - <defines.h>
*.   intPortNum				  Local   - port number
*.   intPortNum                           Local   - <DataPacketIn> member
*.   intPortNum                           Local   - <ExperientCommPort><ipworks.h><UDPPort> member
*   intPortDownReminderThreshold          Local   - <ExperientCommPort> member.. reminder email interval (sec)for port in reduced HB mode
*.   intPosition                          Local   - used to hold position number
*.   intPosition                          Local   - <WorkStation> member
*.   intRC             			  Local   - return code for various library function calls
*   intTHREAD_SHUTDOWN_DELAY_SEC          Global  - <defines.h>
*.   intUnexpectedDataCharacterCount      Local   - <ExperientCommPort> member
*.   intWORKSTATION_XML_INIT_LENGTH       Local   - length of the init XML message
*.   itimerspecWRKMessageMonitorDelay     Global  - <ctime> struct itimerspec which contains timing parameters
*   itimerspecCadPortListenDelay          Global  - <ctime> struct itimerspec which contains timing parameters
*   itimerspecCadScoreboardDelay          Global  - <ctime> struct itimerspec which contains timing parameters
*   KRN_MESSAGE_142			  Global  - <defines.h> CPU affinity Error message        
*.   LOG_CONSOLE_FILE                     Global  - <defines.h> code to log thread, display to console and log to disk
*.   LOG_WARNING                          Global  - <defines.h> code to log thread, to email warning, display to console and log to disk
*.   objWRKData                   	  Local	  - <ExperientDataClass> object
*   obj_arrayCadWorkTable          	  Local	  - <ExperientDataClass>> array of objects being processed
*.   objMessage				  Local   - <MessageClass> object
*   objPing                               Local   - <ipworks.h><Ping> object
*   OrderlyShutDown                       Global  - <Thread_Trap> object
*.   pthread_attr_tAttr                   Local   - <pthread.h> pthread_attr_t
*.   PTHREAD_CREATE_JOINABLE              Library -  <pthread.h> constant				
*   queue_objCadInputData       	  Local   - <queue> <ExperientDataClass> a pointer to a queue of ExperientDataClass objects from main()
*   sem_tCadFlag                 	  Global  - <semaphore.h> semaphore flag for interprocess communication
*   sem_tMutexCadInputQ        	          Global  - <semaphore.h> semaphore flag set to mutex Cad Input Q
*   sem_tMutexCadPortDataQ		  Global  - <semaphore.h> semaphore flag set to mutex Cad Port Data Q
*   sem_tMutexCadWorkTable                Global  - <semaphore.h> semaphore flag set to mutex Cad Work Table array
*.   sem_tMutexUDPPortBuffer              Global  - <ExperientCommPort><ExperientUDPPort> member
*   sigeventCadACKTimerEvent		  Local   - <csignal> struct sigevent that contains the data that defines the event handler
*.   sigeventWRKMessageMonitorEvent	  Local   - <csignal> struct sigevent that contains the data that defines the event handler
*.   sigev_notify                         Library - <csignal> struct sigevent member
*.   sigev_notify_attributes              Library - <csignal> struct sigevent member
*.   sigev_notify_function                Library - <csignal> struct sigevent member
*.   SIGEV_THREAD                         Library - <csignal> Constant
*.   stringDataIn                         Local   - <DataPacketIn> member
*.   stringError                          Local   - <cstring> Used store return errors from XML functions
*.   stringTemp                           Local   - <cstring> temporary string data 
*   timespecNanoDelay                     Global  - <ctime> struct timespec holds time delay for nanosleep()
*   timespecTimeLastHeartbeat             Global  - <ExperientDataClass> (struct timespec) member
*   timespecTimeNow                       Local   - <ctime> struct timespec holds current time hack in sec & nsec
*   timespecRemainingTime                 Global  - <ctime> struct timespec argument for nanosleep()
*   timespecTimeXmitStamp                 Global  - <ExperientDataClass> (struct timespec) member
*   time_tCurrentTime			  Local   - <ctime> current time in sec since epoch
*   timer_tPortListenTimerId              Global  - <ExperientCommPort><ctime> timer id associated with Cad_Port_Listen_Event()
*.   timer_tWRKMessageMonitorTimerId	  Global  - <ctime> timer id associated with Cad_Messaging_Monitor_Event()
*.   vstringWORKSTATION_MESSAGE[]         Global  - <vector><cstring> array of canned string messages from workstations
*.   WorkStationTable[]                   Global  - <WorkStation> object array
*.   WRK                                  Global  - <header.h><threadorPorttype> enumeration member
*.   WRK_HEARTBEAT                        Global  - <header.h><wrk_functions> enumeration member
*.   WRKListenThread     	          Local   - <pthread.h> pthread_t Listen Thread
*.   WRK_MESSAGE_700                      Global  - <defines.h> Init message
*.   WRK_MESSAGE_702                      Global  - <defines.h> Timer event intialized
*.   WRK_MESSAGE_704                      Global  - <defines.h> Port Initialized
*.   WRK_MESSAGE_706                      Global  - <defines.h> Port Listen Thread Failed to Initialize
*.   WRK_MESSAGE_708                      Global  - <defines.h> Bad XML received
*.   WRK_MESSAGE_716                      Global  - <defines.h> Bad XML received (Debug Mode)
*    WRK_MESSAGE_749                      Global  - <defines.h> Thread Complete
*.   WRK_MESSAGE_750 - 799                Global  - <defines.h> Workstation Error Messages
*    WRK_Messaging_Monitor_Event          Local   - function name
*.   WRKPortDataRecieved                  Local   - <DataPacketIn> Port Data recieved
*                                                                          
* Functions:  

*   Cad_Port_Listen_Event()             Local                           (void)          SIGEV_THREAD function
*   clock_gettime()                     Libray  <ctime>                 (int) 
*   .Config()				Library <ipworks.h> <UDPPort>   (char*)
*   Console_Message()			Global  <globalfunctions.h>     (void)
*   .c_str()                            Library <cstring>               (const char*)
*   .empty()				Library <queue>                 (bool)
*   enQueue_Message()		        Global  <globalfunctions.h>     (void)
*   .fAllThreadsReady()                 Global  <Thread_Trap>             (bool) 
*   .fClear_Record()                    Global  <ExperientDataClass>    (void)
*   .fInt_of_Posn()                     Global  <ExperientDataClass>    (int)
*   .fMessage_Create()			Global  <MessageClass>          (void)
*   .front()                            Library <queue>
*   getpid()				Library <unistd.h>
*   int2strLZ()                         Global  <globalfunctions.h>
*   .length()                           Library <cstring>
*   nanosleep()                         Library <unistd.h>
*   .pop()                              Library <queue>
*   pthread_attr_init()                 Library <pthread.h>
*   pthread_attr_setdetachstate()       Library <pthread.h>
*   pthread_create()                    Library <pthread.h>
*   pthread_self()			Library <pthread.h>
*   pthread_setaffinity_np()		Library <pthread.h>                             (int)
*   Reboot_Controller()                 Global  <globalfunctions.h>
*   Semaphore_Error()                   Global  <globalfunctions.h>
*   sem_wait()             		Library <semaphore.h>
*   sem_post()				Library <semaphore.h>
*   .SetAcceptData()			Library <ipworks.h> <UDPPort>
*   .SetActive()			Library <ipworks.h> <UDPPort>
*   .SetLocalHost()			Library <ipworks.h> <UDPPort>
*   .SetLocalHost()                     Library <ipworks.h> <Ping>
*   .SetLocalPort()			Library <ipworks.h> <UDPPort>
*   .SetRemoteHost()			Library <ipworks.h> <UDPPort>
*   .SetRemotePort()			Library <ipworks.h> <UDPPort>
*   .SetTimeout()                       Library <ipworks.h> <Ping>
*   Set_Timer_Delay()			Global  <globalfunctions.h>
*   sleep()                             Library <unistd.h>  
*   timer_settime()			Library <ctime>
*   timer_create()			Library <ctime>
*   timer_delete()			Library <ctime>
*   UDP_Port.Error_Handler()            Global  <error_routines.cpp>
*   WRK_Messaging_Monitor_Event()	Local   SIGEV_THREAD function
*
* Author(s):
*   Bob McCarthy 			Original: 10/02/2007
*                                       Updated : N/A
*
****************************************************************************************************/
void *WRK_Thread(void *voidArg)
{
 struct sigevent                                 sigeventWRKMessageMonitorEvent;
 struct sigevent                                 sigeventWRKtimedEvent;
 int                                             intRC;
 MessageClass                                    objMessage;
 pthread_attr_t                                  pthread_attr_tAttr;
 pthread_t                                       WRKListenThread;
 pthread_t                                       pthread_tWrkMessagingThread;
 ExperientDataClass                              objWRKData, objRingData;
 DataPacketIn                                    WRKPortDataRecieved;
 string                                          stringError;
 string                                          stringTemp;
 int                                             intMessageNumber;
 int                                             intPosition;
 message_display_type                            enumMessageType;
 Call_Data                                       objCallData;
 struct timespec                                 timespecRemainingTime;
 string                                          strDebug;
 Thread_Data                                     ThreadDataObj;
 bool                                            boolSkip; 
 int                                             index; 
 bool                                            boolA, boolB, boolC; 
 bool                                            boolWrkQdata;
 extern vector <Thread_Data>                     ThreadData;
 extern Workstation_Groups                       WorkstationGroups;
 extern Telephone_Devices                        TelephoneEquipment;
/*
  // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,728, objBLANK_CALL_RECORD, objBLANK_WRK_PORT, KRN_MESSAGE_142); enQueue_Message(WRK,objMessage);}
*/

 ThreadDataObj.strThreadName = "WRK";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in wrk.cpp::WRK_Thread()", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);



 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,700, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_700, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(WRK,objMessage);

 switch (enumWORKSTATION_CONNECTION_TYPE)  {
   case UDP:
        WRKThreadPort.fInitializeWRKUDPport(1); break;
   case TCP:
        WRKThreadPort.fInitializeWRKTCPServerPort(1); break;
        break;
   default: 
        SendCodingError("wrk.cpp *WRK_Thread() -NO_CONNECTION_TYPE_DEFINED ");
        break;
 }

        
   objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,704, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_704); 
   enQueue_Message(WRK,objMessage);



 // intialize WorkStationTable
 for (int i = 1; i <= intNUM_WRK_STATIONS; i++){WorkStationTable[i].intPosition = i; WorkStationTable[i].boolActive = false;}

/*
 // set up WRK Health monitor timer event
 sigeventWRKMessageMonitorEvent.sigev_notify 			= SIGEV_THREAD;
 sigeventWRKMessageMonitorEvent.sigev_notify_function 		= WRK_Messaging_Monitor_Event;
 sigeventWRKMessageMonitorEvent.sigev_notify_attributes         = NULL;



*/

 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,702, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_702); 
 enQueue_Message(WRK,objMessage);


 // start port listen thread
 pthread_attr_init(&pthread_attr_tAttr);
 pthread_attr_setdetachstate(&pthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
 intRC = pthread_create(&WRKListenThread, &pthread_attr_tAttr, *WRK_Port_Listen_Thread, (void*) "1");
 if (intRC) {Abnormal_Exit(WRK, EX_OSERR, WRK_MESSAGE_706);}



 intRC = pthread_create(&pthread_tWrkMessagingThread, &pthread_attr_tAttr, *WRK_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(WRK, EX_OSERR, WRK_MESSAGE_746);}



  //  note cfg file should be completely loaded and string contents set before getting here .. 
//  int    intWORKSTATION_XML_INIT_LENGTH = stringWORKSTATION_XML_INIT.length();


/*
 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventWRKMessageMonitorEvent, &timer_tWRKMessageMonitorTimerId);
 if (intRC){Abnormal_Exit(WRK, EX_OSERR, KRN_MESSAGE_180, "timer_tWRKMessageMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecWRKMessageMonitorDelay, 0,intWRK_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intWRK_MESSAGE_MONITOR_INTERVAL_NSEC);
 Set_Timer_Delay(&itimerspecWRKMessageMonitorDelay, 0,intWRK_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0);
 timer_settime( timer_tWRKMessageMonitorTimerId, 0, &itimerspecWRKMessageMonitorDelay, NULL);		// arm timer
*/


// set up WRK timed Event (set to check connections routinely)
 sigeventWRKtimedEvent.sigev_notify 			= SIGEV_THREAD;
 sigeventWRKtimedEvent.sigev_notify_function 		= Workstation_Check_Timed_Event;
 sigeventWRKtimedEvent.sigev_notify_attributes          = NULL;

 intRC = timer_create(CLOCK_MONOTONIC, &sigeventWRKtimedEvent, &timer_tWRKTimedEventTimerId);
 if (intRC){Abnormal_Exit(WRK, EX_OSERR, KRN_MESSAGE_180, "timer_tWRKTimedEventTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecWRKtimedEventDelay, SECONDS_PER_MINUTE, 0, SECONDS_PER_MINUTE, 0 );
 Set_Timer_Delay(&itimerspecWRKtimedEventDelay, 600, 0, 600, 0 );
 timer_settime( timer_tWRKTimedEventTimerId, 0, &itimerspecWRKtimedEventDelay, NULL);		// arm timer









 
 // WRK do loop
 do 
  {
   stringError.clear();
   intRC = sem_wait(&sem_tWRKFlag);							// wait for data signal

   // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
   if (intRC)
    {
     objMessage.fMessage_Create(LOG_WARNING,703, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_703); 
     enQueue_Message(WRK,objMessage);
    } 


   // Shut Down Trap Area
   if (boolSTOP_EXECUTION )    {
     //timer_delete(timer_tWRKMessageMonitorTimerId);
     timer_delete(timer_tWRKTimedEventTimerId);
     pthread_join( WRKListenThread, NULL); 
     pthread_join( pthread_tWrkMessagingThread, NULL);

 //    while (!WRKThreadPort.boolReadyToShutDown){nanosleep( &timespecNanoDelay, &timespecRemainingTime);} 
     OrderlyShutDown.boolWRKThreadReady = true;
 //    sleep(intTHREAD_SHUTDOWN_DELAY_SEC);
     while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}       
     break;
   }// end if (boolSTOP_EXECUTION  )

  if (boolUPDATE_VARS)    {
    UpdateVars.boolWRKThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    UpdateVars.boolWRKThreadReady = false;       
  }
           
            
   // Process Data received from the Workstations  ..                  
   //  while (!ThreadsafeEmptyQueueCheck(&queueWRKPortData, &sem_tMutexWRKPortDataQ) )   {
 
    boolWrkQdata = false;
    
     //check for data in the Q
    intRC = sem_wait(&sem_tMutexWRKPortDataQ);
    if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKPortDataQ, "WRK    - sem_wait@sem_tMutexWRKPortDataQ in WRK_Thread", 1);}
     
    boolWrkQdata = (!queueWRKPortData.empty());
    if (boolWrkQdata) { 
     
     WRKPortDataRecieved = queueWRKPortData.front();
     queueWRKPortData.pop();
    }
    
    sem_post(&sem_tMutexWRKPortDataQ);
    
    if (boolWrkQdata) {
  
     objWRKData.fClear_Record();
     objWRKData.enumThreadSending = WRK;
     objWRKData.stringWRKStationIPAddr = WRKPortDataRecieved.IPAddress.stringAddress;
     objWRKData.intWorkstation_TCPconnectionID = WRKPortDataRecieved.intConnectionID;
     
     // Load and Verify XML Data into Class
     stringError.clear();

  //   //cout << "DATA Recieved =" << WRKPortDataRecieved.stringDataIn << endl;
     stringError = objWRKData.fLoad_InBound_XML_Message( WRKPortDataRecieved.stringDataIn.c_str(), WRKPortDataRecieved.intWorkstation);
     if (!stringError.empty()) 
      {
       if (stringError != "Bad Data")
        {
         objMessage.fMessage_Create(LOG_WARNING,708, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                    objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_708, stringError, 
                                    ASCII_String(WRKPortDataRecieved.stringDataIn.c_str(), WRKPortDataRecieved.intLength),"","","","",  NORMAL_MSG, NEXT_LINE); 
         enQueue_Message(WRK,objMessage);
        }
       
       continue;
      }
 
     // check that ip address and is registered and matches workstation table  
 //    if ((objWRKData.enumWrkFunction != WRK_HEARTBEAT)&&(objWRKData.enumWrkFunction != WRK_SEND_INIT_DATA)&&(objWRKData.enumWrkFunction != UPDATE_CAD))
 //     {
 //      if(!WorkStationTable[intPosition].boolActive) {continue;}
 //      intPosition = objWRKData.fInt_of_Posn();
//       if (WorkStationTable[intPosition].fCheckForDuplicatePosition(objWRKData)){continue;}
//      }
    
     


     // lock  work table(s)
     intRC = sem_wait(&sem_tMutexMainWorkTable);
     if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in WRK_Thread(a)", 1);}
     intRC = sem_wait(&sem_tMutexWRKWorkTable);
     if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in WRK_Thread(a)", 1);}

     intPosition = WRKPortDataRecieved.intWorkstation;

     // Check for WindowsUser Change
  //   if (intPosition) {WorkStationTable[intPosition].fCheckUserNameChange(objWRKData.strWindowsUser);}

     switch(objWRKData.enumWrkFunction)
      {
       case WRK_HEARTBEAT:
            intPosition = objWRKData.fInt_of_Posn();
            if (!intPosition) {SendCodingError("wrk.cpp - got a position zero HB"); break;}
            // check if posn not active
          //  objWRKData.fDisplay(intPosition);
          if (WorkStationTable[intPosition].fCheckForDuplicatePosition(objWRKData)) {break;}
          if (WorkStationTable[intPosition].fCheckUserNameChange(objWRKData) )      {break;}   


            if(!WorkStationTable[intPosition].boolActive) {WorkStationTable[intPosition].fRegisterWorkStation(objWRKData);}       
            
            clock_gettime(CLOCK_REALTIME, &WorkStationTable[intPosition].timespecTimeLastHeartbeat);
            WorkStationTable[intPosition].strGUIVersion = objWRKData.strGUIVersion;
            WorkStationTable[intPosition].strGUIxmlVersion = objWRKData.strGUIxmlVersion;
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_REBID:
            if (!intPosition) {break;}
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);           
            break;

       case UPDATE_CAD:
            if (!intPosition) {break;}
            objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_TRANSFER:
            if (!intPosition) {break;}
            if (objWRKData.CallData.TransferData.eTransferMethod == mBLIND_TRANSFER) {objWRKData.enumWrkFunction = WRK_BLIND_TRANSFER;}
            objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_CNX_TRANSFER:
            if (!intPosition) {break;}
            objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_BLIND_TRANSFER:
/*
            if (!intPosition) {break;}
            objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;
*/
       case WRK_LOG:
            if (!intPosition) {break;}
            enumMessageType = NORMAL_MESSAGE;
            intMessageNumber = objWRKData.intLogMessageNumber;
            objWRKData.fLoad_Position(SOFTWARE, WRK, int2strLZ(objWRKData.intWorkStation), 0);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            switch (intMessageNumber)
             {
              case 751:
                   objCallData = objWRKData.fCallData();
                   if (enumANI_SYSTEM == ANI_LINK){objCallData.fLoadPosn(WRKPortDataRecieved.intWorkstation);}              
                   objMessage.fMessage_Create(LOG_CONSOLE_FILE, intMessageNumber, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                              objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_361, 
                                              objWRKData.stringWRKStationMSGText,"","","","","", NORMAL_MSG,TDD_CONVERSATION); 
                   break;
              case 754:
                    objMessage.fMessage_Create(LOG_CONSOLE_FILE,intMessageNumber,__LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objWRKData.fCallData(), objBLANK_TEXT_DATA,
                                               objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, objWRKData.stringWRKStationMSGText);
                   break;
              default:
                     objMessage.fMessage_Create(objWRKData.intLogType,intMessageNumber, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objWRKData.fCallData(), objBLANK_TEXT_DATA,
                                                objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, objWRKData.stringWRKStationMSGText);
             }    
            enQueue_Message(WRK,objMessage);
            break;

       case WRK_SEND_INIT_DATA:
            intPosition = objWRKData.fInt_of_Posn();
            if (!intPosition) {SendCodingError("wrk.cpp - got a position zero Init Request"); break;}
            if (WorkStationTable[intPosition].fCheckForDuplicatePosition(objWRKData))        {break;}
            if (WorkStationTable[intPosition].fCheckUserNameChange(objWRKData) )             {break;}   


            // check if posn not active
            if(!WorkStationTable[intPosition].boolActive){WorkStationTable[intPosition].fRegisterWorkStation(objWRKData);}

            clock_gettime(CLOCK_REALTIME, &WorkStationTable[intPosition].timespecTimeLastHeartbeat);
            // generate initXML for worksation with user 
            WorkStationTable[intPosition].fInitXMLSend();



            objMessage.fMessage_Create(LOG_CONSOLE_FILE,739, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objWRKData.fCallData(), objBLANK_TEXT_DATA,
                                       objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,WRK_MESSAGE_739, WorkStationTable[intPosition].RemoteIPAddress.stringAddress);
            enQueue_Message(WRK,objMessage);
            SendStrayACKToWorkStations();
            WorkStationTable[intPosition].fTransmitMainWorktable();
            break;


       case WRK_CONFERENCE_JOIN:
            if (!intPosition) {break;}
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_CONFERENCE_LEAVE:
            if (!intPosition) {break;}
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_TAKE_CONTROL:
             if (!intPosition) {break;} 
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;

       case WRK_TDD_MODE_ON:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
             if (!boolTDD_AUTO_DETECT){enqueue_Main_Input(WRK, objWRKData);}
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;

       case WRK_TDD_MUTE:
            if (!intPosition) {break;}
            objWRKData.enumANIFunction = TDD_MUTE;
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case WRK_TDD_STRING: case WRK_TEXT_STRING:
            //This is a Dispatcher Says sentence that needs to be TX to the caller.  Send to KRN
            // WRK_TDD_STRING soon to be obsolete kept for backwards compability
            if (!intPosition) {break;}
            objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
            objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);

       //     objMessage.fMessage_Create(LOG_CONSOLE_FILE,733,objWRKData.fCallData(), objBLANK_WRK_PORT, WRK_MESSAGE_733, objWRKData.CallData.stringPosn, ASCII_String(objWRKData.stringWRKStationMSGText.c_str(),
       //                                objWRKData.stringWRKStationMSGText.length()) ); 
       //     enQueue_Message(WRK,objMessage);
            break;

       case  WRK_TDD_CHARACTER:
             // no longer used this was an Inbound ANI_LINK TDD character
             break;
 
       case  WRK_DIAL_OUT:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;

       case WRK_BARGE:
            if (!intPosition) {break;}
            objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
            objWRKData.enumANISystem = enumANI_SYSTEM;
            enqueue_Main_Input(WRK, objWRKData);
            WorkStationTable[intPosition].fSendACK(objWRKData);
            break;

       case  WRK_CAD_ERASE:
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;

       case  WRK_RELOAD_CONSOLE:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_FORCE_DISCONNECT:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_RADIO_CONTACT_CLOSURE:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_TOGGLE_HOLD:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_HANGUP:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_PICKUP:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_ANSWER:
             ////cout << "wrk answer -> " << intPosition << endl;
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_MUTE:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;
       case  WRK_VOLUME_UP:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break;   
       case  WRK_VOLUME_DOWN:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break; 
       case  WRK_KILL_CHANNEL:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break; 
       case  WRK_FLASH_TANDEM:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break; 
       case  WRK_COMPLETE_ATT_XFER:
             if (!intPosition) {break;}
             objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
             objWRKData.enumANISystem = enumANI_SYSTEM;
             enqueue_Main_Input(WRK, objWRKData);
             WorkStationTable[intPosition].fSendACK(objWRKData);
             break; 
       case  WRK_DISCONNECT_REQUEST:
              if (!intPosition) {break;}
              objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
              objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
              objWRKData.enumANISystem = enumANI_SYSTEM;
              enqueue_Main_Input(WRK, objWRKData);
              WorkStationTable[intPosition].fSendACK(objWRKData); 
              break;
       case WRK_SEND_DTMF:
              if (!intPosition) {break;}
              objWRKData.intWorkStation = WRKPortDataRecieved.intWorkstation;
              objWRKData.CallData.fLoadPosn(objWRKData.intWorkStation);
              objWRKData.enumANISystem = enumANI_SYSTEM;
              enqueue_Main_Input(WRK, objWRKData);
              WorkStationTable[intPosition].fSendACK(objWRKData);
              break;            
       default:
            SendCodingError("wrk.cpp -CODING ERROR In SWITCH CASE IN WRK THREAD "+int2str(objWRKData.enumWrkFunction));
      }// end switch 
 
    sem_post(&sem_tMutexMainWorkTable);
    sem_post(&sem_tMutexWRKWorkTable);

   } // end if (boolWrkQdata) 

/*******************************************************************************************************************************************************************************************/
/*******************************************************************************************************************************************************************************************/

    boolWrkQdata = false;
   
  // Process data from KRN ..
  // while (!ThreadsafeEmptyQueueCheck(&queue_objWRKData, &sem_tMutexWRKInputQ) )  {
    intRC = sem_wait(&sem_tMutexWRKInputQ);
    if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKInputQ, "WRK    - sem_wait@sem_tMutexWRKInputQ in WRK_Thread", 1);}
    
    boolWrkQdata = (!queue_objWRKData.empty());
    if (boolWrkQdata) {
     objWRKData = queue_objWRKData.front();
     queue_objWRKData.pop();
    }
    sem_post(&sem_tMutexWRKInputQ);
    
    if (boolWrkQdata) {
 
     // lock  work table(s)
    intRC = sem_wait(&sem_tMutexMainWorkTable);
    if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in WRK_Thread(b)", 1);}
    intRC = sem_wait(&sem_tMutexWRKWorkTable);
    if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in WRK_Thread(b)", 1);}

    // check for reload grid command
    if(objWRKData.boolReloadWorkstationGrid)
     {
      intPosition = objWRKData.intWorkStation;
      if(WorkStationTable[intPosition].boolActive){ SendStrayACKToWorkStations();WorkStationTable[intPosition].fTransmitMainWorktable(true);}
     }

    // check if broadcast to (single workstation) else (to all)
    if(objWRKData.boolBroadcastXMLtoSinglePosition)
     {
      intPosition = objWRKData.intWorkStation;
      if (WorkStationTable[intPosition].boolActive) {
       if (objWRKData.stringXMLString.length()) {
        WorkStationTable[intPosition].fTransmit((char*)objWRKData.stringXMLString.c_str(), objWRKData.stringXMLString.length());
       } 
       ////cout << "WRK TX single -> " << endl << objWRKData.stringXMLString << endl;
      }
     }
    else
     {

      // TX to all Active Positions    
      for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
       {
        boolSkip = false;
      //  //cout << "CallData Trunk " << objWRKData.CallData.intTrunk << "History -> " << objWRKData.CallData.ConfData.strHistoryConferencePositions << endl;
      //  //cout << WorkstationGroups.WorkstationGroups.size() << endl;

        if (!WorkstationGroups.WorkstationGroups.empty()) 
         {
          if(IS_ABANDONED_POPUP(objWRKData.stringXMLString))

           {
            index = WorkstationGroups.IndexOfGroupWithWorkstation(i);
            if (index < 0) {break;}
            boolSkip = (!WorkstationGroups.WorkstationGroups[index].CheckAbandonedTrunkList(CreateAbandonedList()) );
           }

          else if (IS_PSAP_STATUS(objWRKData.stringXMLString))

           {
            boolSkip = false;
           }

          else

           {
            // check is internal NG911 Transfer ring .....
            boolA = INIT_VARS.fChannelIsInternalNG911Transfer(objWRKData.CallData.TransferData.strNG911PSAP_TransferNumber);
            boolB = (objWRKData.intCallStateCode == 2 );
            boolC = (objWRKData.CallData.vTransferData.size() > 0);

            if ((boolA)&&(boolB)&&(boolC)) {
             index = WorkstationGroups.IndexOfGroupWithWorkstation(i);
             if (index >= 0) { 
              if (WorkstationGroups.WorkstationGroups[index].HasThisURI(objWRKData.CallData.TransferData.strNG911PSAP_TransferNumber)) {     
               if (!WorkstationGroups.WorkstationGroups[index].AnyPositionInGroupActive(objWRKData.CallData.ConfData)) {
                objRingData = objWRKData;
                objRingData.fLoad_CallState(MAIN,XML_STATUS_MSG_RINGING,"SOFTWARE LOAD");
                objRingData.fCallInfo_XML_Message(XML_STATUS_CODE_RINGING);
                if(WorkStationTable[i].boolActive) {
                 ////cout << "WRK NG911 transfer Ring -> " << endl << objWRKData.stringXMLString << endl;
                 if (objRingData.stringXMLString.length()) {
                  WorkStationTable[i].fTransmit((char*)objRingData.stringXMLString.c_str(), objRingData.stringXMLString.length());
                 }
                 boolSkip = true;
                }
               }
              }
             }
            } // end check is internal NG911 Transfer ring

            //check for trunk based display to Workstations
            if (!boolSkip) {
             switch (objWRKData.CallData.intTrunk) {
              case 0:  break;
              case 98: boolSkip = true; break;
              default:
                      // A position is unique to only one group .... A trunk can appear in more than 1 group
                      index = WorkstationGroups.IndexOfGroupWithWorkstation(i);
                      if (index < 0) {break;}
                      boolSkip = (!WorkstationGroups.WorkstationGroups[index].TrunkInGroup(objWRKData.CallData.intTrunk));

                      
                      
                      //index = WorkstationGroups.IndexOfGroupWithTrunk(objWRKData.CallData.intTrunk);
                      //if (index < 0) {break;}       
                      //boolSkip = (!WorkstationGroups.WorkstationGroups[index].WorkstationInGroup(i));

                      if (boolSkip) {
                       //check each position on the Call then check if (i) is in the same group
                       boolSkip = (!IsPositionPartofAGroupThatIsOnTheCall(i, objWRKData.CallData)); 
                      }
             }

             if(boolSkip) {boolSkip = (!objWRKData.CallData.ConfData.fPositionInHistory(i) );}
            }
           }


         }




        if ((boolSkip)||(!WorkStationTable[i].boolActive)) {continue;}
         
        //  if (!objWRKData.CallData.intTrunk) {//cout << objWRKData.stringXMLString << endl;}
          ////cout << "WRK Multi -> " << endl << objWRKData.stringXMLString << endl;
         if (objWRKData.stringXMLString.length()) {
          WorkStationTable[i].fTransmit((char*)objWRKData.stringXMLString.c_str(), objWRKData.stringXMLString.length());
         }
       }//end for (int i = 1; i <= intNUM_WRK_STATIONS; i++)



     }//enddr if (objWRKData.boolBroadcastXMLtoSinglePosition) else

    sem_post(&sem_tMutexMainWorkTable);    
    sem_post(&sem_tMutexWRKWorkTable);

   }// end if (boolWrkQdata )

  } while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 749, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_749);
 objMessage.fConsole();
 return NULL;

}// end WRK_Thread()

/****************************************************************************************************
*
* Name:
*  Function: void WRK_Messaging_Monitor_Event()
*
*
* Description:
*   An event handler within WRK_Thread() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from WRK Thread.  When activated it
*
*   1. Checks for messages on the message Q and retrieves messages until Q is empty and processes them.
*
*   2. checks the status of the workstations and drops those that have not heartbeated in a specified time
*
*
* Parameters: 				
*   union_sigvalArg			not used
*					
*
* Variables:
*   boolActive                          Global  - <WorkStation> member - workstation is active
*   CLOCK_REALTIME                      Global  - <ctime> constant
*   intMessageCode                      Global  - <MessageClass> member - error/ message code
*   intRC				Local   - return code
*   itimerspecWRKMessageMonitorDelay    Local   - <ctime> struct which contains timing data that for the timer
*   itimerspecDisableTimer              Global  - <ctime> struct which contains data that disables the timer
*   NUM_WRK_STATIONS_MAX                Global  - <defines.h>
*   objMessage                          Local   - <MessageClass> object
*   queue_objWRKMessageQ                Global  - <queue><MessageClass> queue of WRK messages
*   sem_tMutexWRKMessageQ               Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   sem_tMutexMainMessageQ		Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   timer_tWRKMessageMonitorTimerId     Global  - <ctime> timer id for this function
*   timespecTimeNow                     Local   - <ctime> struct timespec - Current time
*   WorkStationTable[]                  Global  - <WorkStation> object - The table containing all registered workstation data
*                                                                       
* Functions:  
*   .empty()				Library <queue>
*   .fRefreshWorkStationList()          Global  <WorkStation>
*   .front()				Library <queue> 
*   .pop()				Library <queue>
*   .push()				Library <queue>
*   Semaphore_Error()                   Global  <globalfunctions.h>
*   sem_wait()             		Library <semaphore.h>
*   sem_post()				Library <semaphore.h>
*   timer_settime()                     Library <ctime>
*
*
* Author(s):
*   Bob McCarthy 			Original: 10/02/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
void *WRK_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 struct timespec                timespecTimeNow;
 bool                           boolskip;
 Thread_Data                    ThreadDataObj;
 extern vector <Thread_Data>    ThreadData;
 extern WorkStation             WorkStationTable[NUM_WRK_STATIONS_MAX+1];



 ThreadDataObj.strThreadName = "WRK Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in wrk.cpp::WRK_Messaging_Monitor_Event()", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

do
{

 experient_nanosleep(intWRK_MESSAGE_MONITOR_INTERVAL_NSEC);

/*
 //Disable Timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tWRKMessageMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}
*/
 
 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in WRK_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexWRKMessageQ);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKMessageQ, "sem_wait@sem_tMutexWRKMessageQ in WRK_Messaging_Monitor_Event", 1);}

 while (!queue_objWRKMessageQ.empty())
  {
    objMessage = queue_objWRKMessageQ.front();
    queue_objWRKMessageQ.pop();
    boolskip = false;
    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 700:						
       // "[DEBUG] WRK Thread Created, PID %%% ThreadSelf %%%" 
       break; 
      case 701:						
       // "[WARNING] WRK Thread Failed to Initialize, System Rebooting"
       break;
      case 702:						
       // "[DEBUG] WRK Health Monitor Timer Event Initialized"
       break;
      case 703:
       // "[WARNING] WRK Thread Signalling Semaphore failed to Lock"						
       break;
      case 704:						
       // "[DEBUG] WRK %%% - Port Initialized"
       break;
      case 705:						
       // "[DEBUG] WRK %%% Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 706:						
       // "[WARNING] WRK 01 - Port Listen Thread Failed to Initialize, System Rebooting"
       break;
      case 707:
       // "[WARNING] WRK 01 - %%% XML Data Field Missing .. Unable to Process "						
       break;
      case 708:						
       // "[WARNING] Workstation %%% .. Unable to Process "
       break;
      case 709:
       // "[INFO] Workstation at Position %%% At IP: %%% Now On Line"						
       break;
      case 710:						
       // "[WARNING] %%% %%% < Invalid Data Received for %%%, Data=\"%%%\""
       break;
      case 711:						
       // "[WARNING] XML to Workstation Length %%% too Long Data=%%%"
       break;
      case 712:						
       // unused
       break;
      case 713:						
       //  "[WARNING] WRK < Duplicate Position %%% Attempted to Register from IP Address %%%"
       break;
      case 714:                                           
       // "Unable to start the application because the position number is currently being used by another workstation."
       break;
      case 715:                                           
       // "WRK %%% < Raw Data Received from IP=%%% (Data Follows) %%%" 
       break;
      case 716:                                           
       // unused
       break;
      case 717:                                           
       //  "[INFO] Workstation at Position %%% At IP: %%% Now Off Line"
       break;
      case 718:
       //  "[WARNING] PSAP Controller not in sync with workstation; refreshing Console window."
       break;
      case 719:
       //  "[WARNING] %%% - UDP Error (Code=%%%, MSG=%%%)"
       break;
      case 720:
       //  "[ALARM] %%% - Port Restored"
       break;
      case 721:
       //  "[ALARM] %%% - Port Down for %%%, (Ping %%%)"
       break;
      case 722:
       //  "[WARNING] %%% - Reminder - Port Down for %%%, (Ping %%%)" 
       break;
      case 723:
       //  "[WARNING] %%% - Port Restored"
       break;
      case 724:
       //  "[WARNING] XML Error MsgText too long, length=%%%"
       break;
      case 725:
       //  "WRK > Cancel Transfer %%% TransferTo=%%%"
       break;
      case 726:
       //  "WRK > Initiate Transfer %%% TransferTo=%%%"
       break;
      case 727:
       //  "WRK %%% < Invalid ANIPort Recieved value=%%%"
       break;
      case 728:
       // "[WARNING] Unable to set CPU affinity for WRK Thread"
       break;
      case 729:
       // Discarded Packet Counter Rollover
       break;
      case 730:
       // "[WARNING] %%% - Incoming Network Packets Discarded over Past 24 Hours = %%%" 
       break;
      case 731:
       // "%%% > Data sent to IP=%%% (Data Follows)" 
       break;
      case 732:
       // ">Invalid %%% received value=%%% (XML Data Follows)"
       break;
      case 733:
       //  ">[TDD] %%%"
       break;
      case 734:
       // "-[WARNING] Port Restart Failed (Code=%%%)"
       break;
      case 735:
       //  "-[WARNING] Port Data Reception Restored"
       break;

      case 736:
       // "-[DEBUG] Workstion Duplicate data follows:%%%"
       break;

      case 737:
       //  "-[INFO] TCP connection id: %%% forcibly disconnected" 
       break;
      case 738:
       //  "-[WARNING] TCP connection id: %%% Send Error: %%%"
       break;
      case 739:
       //  ">[INFO] Init XML Sent to workstation at IP: %%%"
       break;
      case 740:
       //  ">[WARNING] Unable to Rebid ALI, No URI in Table!"
       break;
      case 748:
       //  "[WARNING] WRK Mutex Semaphore Error (Code=%%%, MSG=%%%)" 
      case 749:
       // Thread complete.....
       break;

      case 754: break;
   
      case 750: case 751: case 752: case 753: case 755: case 756: case 757: case 758: case 759:
       break;
      case 760: case 761: case 762: case 763: case 764: case 765: case 766: case 767: case 768: case 769:
       break;
      case 770: case 771: case 772: case 773: case 774: case 775: case 776: case 777: case 778: case 779: 
       break;
      case 780: case 781: case 782: case 783: case 784: case 785: case 786: case 787: case 788: case 789: 
       break;
      case 790: case 791: case 792: case 793: case 794: case 795: case 796: case 797: case 798: case 799: 
       break;
      case 998:
        //cout << "send CLOSE" << endl;
        Transmit_Data(RCC, 1, (char*)"CLOSE", 1); 
        break;
      case 999:
       // test area..................................
       // 
        Transmit_Data(RCC, 1, (const char*)"OPEN", 1); 
          //cout << "send OPEN" << endl;           
       break;
	
      default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in WRK Monitor\n";
       break;
       
   }// end switch


  // Push message onto Main Message Q
  if (!boolskip) {queue_objMainMessageQ.push(objMessage);}
   
  }// end while

 // UnLock WRK Message Q then Main Message Q
 sem_post(&sem_tMutexWRKMessageQ);
 sem_post(&sem_tMutexMainMessageQ);


 // refresh Worktable List if Master
 if(boolIsMaster)
  {
   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

   intRC = sem_wait(&sem_tMutexWRKWorkTable);
   if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in WorkStation::WRK_Messaging_Monitor_Event()", 1);}
   for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
    {
     if (WorkStationTable[i].boolActive) {WorkStationTable[i].fRefreshWorkStationList(timespecTimeNow);}
    }
   sem_post(&sem_tMutexWRKWorkTable);

    // check if ports have been down
    if(enumWORKSTATION_CONNECTION_TYPE == UDP)  {WRKThreadPort.UDP_Port.fCheck_Time_Port_Down();}
  }

 /*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tWRKMessageMonitorTimerId, 0, &itimerspecWRKMessageMonitorDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);
 
 objMessage.fMessage_Create(0, 747, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_747);
 objMessage.fConsole();

return NULL;	


}// end WRK_Messaging_Monitor_Event()

void Workstation_Check_Timed_Event(union sigval union_sigvalArg)
{
 MessageClass objMessage;
 int          intRC;
 string       strConnectionID;
 size_t       iCount = 0;
// bool         boolClearConnection;
 bool         boolAtLeastOneWorkstationLoggedIn = false;
 string       stringMessage;
// size_t       intNumofConnections;

 extern Workstation_Groups                       WorkstationGroups;

 if(boolSTOP_EXECUTION)                     {return;}
 if(enumWORKSTATION_CONNECTION_TYPE != TCP) {return;}

 intRC = sem_wait(&sem_tMutexWRKWorkTable);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in WRK Thread:Workstation_Check_Timed_Event()", 1);}


// sem_wait(&WRKThreadPort.TCP_Server_Port.sem_tMutexConnectionVector);


  switch (WorkstationGroups.WorkstationGroups.empty()) 
   {
    case true:
         // Single Site PSAP
 //        intNumofConnections = WRKThreadPort.TCP_Server_Port.fConnectionsActive();
         for (int i = 0; i <= intNUM_WRK_STATIONS; i++)
          {
           if (WorkStationTable[i].boolActive) {iCount ++;}
          }

         boolAtLeastOneWorkstationLoggedIn = (iCount > 0 );

         if (!boolAtLeastOneWorkstationLoggedIn)
          {
           objMessage.fMessage_Create(LOG_ALARM,726, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                      objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_726); 
           enQueue_Message(WRK,objMessage);
          }
         break;

    case false:
         // Multi Site PSAP    

         // TBC

    
         break;
   }



/*
// check for stale connections ....

 if (intNumofConnections > iCount)
  {
    for (unsigned int i = 1; i < WRKThreadPort.TCP_Server_Port.vConnectionIDactive.size(); i++)  
     {

      if (!WRKThreadPort.TCP_Server_Port.vConnectionIDactive[i]) {continue;}
      boolClearConnection = true;
    
      for (int j = 1; j <= intNUM_WRK_STATIONS; j++)
       {
        if (!WorkStationTable[j].boolActive) {continue;}
        if ( WorkStationTable[j].intTCP_ConnectionID >= (int)WRKThreadPort.TCP_Server_Port.vConnectionIDactive.size())
         {
          SendCodingError( "wrk.cpp - Workstation_Check_Timed_Event -> ConnectionId out of range: id = " +int2strLZ( WorkStationTable[j].intTCP_ConnectionID));
          continue;
         }
 
        if ((int) i == WorkStationTable[j].intTCP_ConnectionID) {boolClearConnection = false;}
       } 
      
      if (boolClearConnection) 
       {
        WRKThreadPort.TCP_Server_Port.Disconnect(i);
        objMessage.fMessage_Create(LOG_INFO, 737, objBLANK_CALL_RECORD, objBLANK_WRK_PORT, WRK_MESSAGE_737, int2str(i));
        enQueue_Message(WRK,objMessage);
       }

     }

  }
 */

 sem_post(&sem_tMutexWRKWorkTable);

}

void Clear_Workstations()
{


 // Must be semaphore protected prior to calling this function


  for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
   {
    WorkStationTable[i].boolActive = false;
    WorkStationTable[i].RemoteIPAddress.fClear();
   }


}

void SendStrayACKToWorkStations()
{
  // Must be semaphore protected prior to calling this function

  ExperientDataClass objData;
  objData.strEventUniqueID = "STRAY_ACK";
  for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
   {
    if (WorkStationTable[i].boolActive)  { WorkStationTable[i].fSendACK(objData);}
   }
 return;
}
/****************************************************************************************************
*
* Name:
*  Function: void *WRK_Port_Listen_Thread()
*
*
* Description:
*   An thread within WRK_Thread() 
*
*
* Details:
*   This function is an implemented thread from WRK_Thread.  When activated it
*   runs the Do_Events() function from the ipworks library until the boolean
*   boolSTOP_EXECUTION is true.
*
*
* Parameters: 				
*   voidArg			        not used
*					
*
* Variables:
*   boolDEBUG                           Global  - <globals.h>
*   boolReadyToShutDown                 Global  - <ExperientCommPort> member
*   boolSTOP_EXECUTION                  Global  - <globals.h>

*   intPortNum				Local   - port number to listen to
*   LOG_CONSOLE_FILE                    Global  - <defines.h>
*   objMessage                          Local   - <MessageClass> object
*   WRK                                 Global  - <header.h><threadorPorttype> enumeration member
*   WRK_MESSAGE_705                     Global  - <defines.h> Listen Thread Created message
*   WRKThreadPort                       Global  - <ExperientCommPort> object
*                                                                          
* Functions:  
*   .DoEvents()                         Library  <ipworks.h>
*   enQueue_Message()                   Global   <globalfunctions.h>
*   .fMessage_Create()                  Global   <MessageClass>
*   getpid()                            Library  <pthread.h>
*   int2strLZ()                         Global   <globalfunctions.h>
*   pthread_self()                      Library  <pthread.h>
*
* Author(s):
*   Bob McCarthy 			Original: 10/02/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
void *WRK_Port_Listen_Thread(void *voidArg)
{
 MessageClass                   objMessage;
 Thread_Data                    ThreadDataObj;
 int				intRC;      
 extern vector <Thread_Data> 	ThreadData;
 

 ThreadDataObj.strThreadName ="WRK-Port "+int2strLZ(WRKThreadPort.intPortNum);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in wrk.cpp::WRK_Port_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,705, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_705, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(WRK,objMessage);


 while (!boolSTOP_EXECUTION)
  {
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
   switch (enumWORKSTATION_CONNECTION_TYPE)    {
     case UDP:
          WRKThreadPort.UDP_Port.DoEvents(); break;
     case TCP:
          WRKThreadPort.TCP_Server_Port.DoEvents(); break;
     default: 
          SendCodingError("wrk.cpp *WRK_Port_Listen_Thread() -NO_CONNECTION_TYPE_DEFINED ");
          break;     
   }
  } 

 WRKThreadPort.TCP_Server_Port.Shutdown();
 WRKThreadPort.UDP_Port.SetActive(false);
 WRKThreadPort.boolReadyToShutDown = true;
 return NULL;
								
}// end WRK_Port_Listen_Thread





