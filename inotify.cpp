/*****************************************************************************
* FILE: inotify.cpp
*  
*
* DESCRIPTION:
* Contains the thread which monitors file descriptors for changes
*
*
* AUTHOR: 1/1/2013 Bob McCarthy
* LAST REVISED:  
*
* COPYRIGHT: 2006-2013 Experient Corporation
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"
#include "/datadisk1/src/inotify/inotify-cxx.cpp"
#include <exception>
 

using namespace std;

void *NFY_Thread(void *voidArg)
{
 MessageClass                   objMessage;
 Inotify 			notify;
 InotifyEvent 			event;
 size_t                         count;
 WorkStation                    NotifyWorkstation;
 ExperientDataClass             objData;
 string                         strError;
 bool                           got_event;
 Thread_Data                    ThreadDataObj; 
 int				intRC;    
 extern vector <Thread_Data> 	ThreadData;
 extern Port_Data               objBLANK_NFY_PORT;
 extern Text_Data		objBLANK_TEXT_DATA;
 extern Call_Data               objBLANK_CALL_RECORD;
 extern Freeswitch_Data         objBLANK_FREESWITCH_DATA;
 extern ALI_Data                objBLANK_ALI_DATA;

 ThreadDataObj.strThreadName ="NFY";
 ThreadDataObj.ThreadPID     = getpid();
 ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, NFY, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in inotify.cpp::NFY_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,116, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_NFY_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,KRN_MESSAGE_116, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(MAIN,objMessage);


 if (!fexists(MESSAGE_CONTROL_CONFIG_FILE_PATH))
  {
   ofstream outfile (MESSAGE_CONTROL_CONFIG_FILE_PATH);
   outfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?></Configuration>" << endl;
   outfile.close();
  }
 if (!fexists(PHONEBOOK_CONFIG_FILE_PATH))
  {
   ofstream outfile (PHONEBOOK_CONFIG_FILE_PATH);
   outfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?></TransferList>" << endl;
   outfile.close();
  }
 if (!fexists(charINI_FILE_PATH_AND_NAME))
  {Abnormal_Exit(NFY, EX_CONFIG , CFG_MESSAGE_003, charINI_FILE_PATH_AND_NAME);} 



 //*********************************************************main loop ******************************************************************
 

    try {
        notify.SetNonBlock(true);
        notify.SetCloseOnExec(true);
        InotifyWatch watch(PHONEBOOK_CONFIG_FILE_PATH, IN_CLOSE_WRITE);
        InotifyWatch watchtwo(MESSAGE_CONTROL_CONFIG_FILE_PATH, IN_CLOSE_WRITE);
        InotifyWatch watchthree(charINI_FILE_PATH_AND_NAME, IN_CLOSE_WRITE);
        notify.Add(watch);
        notify.Add(watchtwo);
        notify.Add(watchthree);        

        while(!boolSTOP_EXECUTION) {
            sleep(1);
            notify.WaitForEvents();

            count = notify.GetEventCount();
            while (count > 0) {

                got_event = notify.GetEvent(&event);

                if (got_event) {
           
                    if (event.GetWatch() == &watch) 
                     {
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,117, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objBLANK_NFY_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,KRN_MESSAGE_117 , PHONEBOOK_CONFIG_FILE_PATH); 
                      enQueue_Message(MAIN,objMessage);
                     }
                    else if (event.GetWatch() == &watchtwo)
                     {
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,126, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objBLANK_NFY_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,KRN_MESSAGE_126 , MESSAGE_CONTROL_CONFIG_FILE_PATH); 
                      enQueue_Message(MAIN,objMessage);
                     }
                    else if (event.GetWatch() == &watchthree)
                     {
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,185, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objBLANK_NFY_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,KRN_MESSAGE_185 , charINI_FILE_PATH_AND_NAME); 
                      enQueue_Message(MAIN,objMessage);
                     }   
                }

                count--;
            }
        }
    } catch (InotifyException &e) {
           strError = "inotify.cpp: Inotify exception occured-> ";
           strError += e.GetMessage();
           SendCodingError( strError ); sleep(2);
    } catch (exception &e) {
           strError = "inotify.cpp: STL exception occured:-> ";
           strError += e.what();
           SendCodingError( strError ); sleep(2); 
    } catch (...) {
           SendCodingError( "inotify.cpp: unknown exception occured" ); sleep(2);
    }

   
  objMessage.fMessage_Create(0, 118, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                             objBLANK_NFY_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_118);
  objMessage.fConsole();


 return NULL;
}








