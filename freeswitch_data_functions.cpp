/*****************************************************************************
* FILE: freeswitch_data_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the Freeswitch_Data Class 
*
*
*
* AUTHOR: 01/29/2012 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"




//Constructor
Memory_Data::Memory_Data() {
  Freeswitch_OS_Memory_Total = 0;
  Freeswitch_OS_Memory_Available = 0;
  Freeswitch_OS_Memory_Usage = 0;
  Freeswitch_OS_Swap_Total = 0;
  Freeswitch_OS_Swap_Free = 0;
  Freeswitch_OS_Swap_Usage = 0;
  Freeswitch_APP_CPU_Usage = 0;
  Freeswitch_APP_MEM_Usage = 0;
  Freeswitch_PID = 0;
  NewFreeswitchDataRX = false;
  OS_Memory_Total = 0;
  OS_Memory_Available = 0;
  OS_Memory_Usage = 0;
  OS_Swap_Total = 0;
  OS_Swap_Free = 0;
  OS_Swap_Usage = 0;
  Controller_APP_CPU_Usage = 0;
  Controller_APP_MEM_Usage = 0;
 }

void Memory_Data::fClear() {
  Freeswitch_OS_Memory_Total = 0;
  Freeswitch_OS_Memory_Available = 0;
  Freeswitch_OS_Memory_Usage = 0;
  Freeswitch_OS_Swap_Total = 0;
  Freeswitch_OS_Swap_Free = 0;
  Freeswitch_OS_Swap_Usage = 0;
  Freeswitch_APP_CPU_Usage = 0;
  Freeswitch_APP_MEM_Usage = 0;
  Freeswitch_PID = 0;
  NewFreeswitchDataRX = false;
  OS_Memory_Total = 0;
  OS_Memory_Available = 0;
  OS_Memory_Usage = 0;
  OS_Swap_Total = 0;
  OS_Swap_Free = 0;
  OS_Swap_Usage = 0;
  Controller_APP_CPU_Usage = 0;
  Controller_APP_MEM_Usage = 0;
 }
 
void Memory_Data::fDisplay() {
  cout << "Freeswitch_OS_Memory_Total:     " << this->Freeswitch_OS_Memory_Total << endl;
  cout << "Freeswitch_OS_Memory_Available: " << this->Freeswitch_OS_Memory_Available << endl;
  cout << "Freeswitch_OS_Memory_Usage:     " << this->Freeswitch_OS_Memory_Usage << endl;
  cout << "Freeswitch_OS_Swap_Total:       " << this->Freeswitch_OS_Swap_Total << endl;
  cout << "Freeswitch_OS_Swap_Free:        " << this->Freeswitch_OS_Swap_Free << endl;
  cout << "Freeswitch_OS_Swap_Usage:       " << this->Freeswitch_OS_Swap_Usage << endl;
  cout << "Freeswitch_APP_CPU_Usage:       " << this->Freeswitch_APP_CPU_Usage << endl;
  cout << "Freeswitch_APP_MEM_Usage:       " << this->Freeswitch_APP_MEM_Usage << endl;
  cout << "Freeswitch_PID:                 " << this->Freeswitch_PID << endl; 
  cout << "NEW FSW DATA RX:                " << this->NewFreeswitchDataRX << endl;
  cout << "OS_Memory_Total:                " << this->OS_Memory_Total << endl;
  cout << "OS_Memory_Available:            " << this->OS_Memory_Available << endl;
  cout << "OS_Memory_Usage:                " << this->OS_Memory_Usage << endl;
  cout << "OS_Swap_Total:                  " << this->OS_Swap_Total << endl;
  cout << "OS_Swap_Free:                   " << this->OS_Swap_Free << endl;
  cout << "OS_Swap_Usage:                  " << this->OS_Swap_Usage << endl;
  cout << "Controller_APP_CPU_Usage:       " << this->Controller_APP_CPU_Usage << endl;
  cout << "Controller_APP_MEM_Usage:       " << this->Controller_APP_MEM_Usage << endl;
}

void Memory_Data::fSendFSCLIrequest() {
    
 ExperientDataClass                             objData;
 this->NewFreeswitchDataRX = false;
  
 objData.enumANIFunction = EXEC_FREESWITCH_MEMORY_USAGE_SCRIPT;
 Queue_AMI_Input(objData);
 sem_post(&sem_tAMIFlag);
 
 return;
}

bool Memory_Data::fLoadOSmemoryUsage(string strTotal, string strAvail) {
    
    long double     longdoubleData;
    float           floatData;
    size_t          found;
    
    found = strTotal.find_first_not_of("0123456789");
    if (found != string::npos)                        {return false;}
    this->OS_Memory_Total = char2int(strTotal.c_str());
    
    found = strAvail.find_first_not_of("0123456789");
    if (found != string::npos)                        {return false;}
    this->OS_Memory_Available = char2int(strAvail.c_str());  
 
    if (this->OS_Memory_Total == 0)                   {return false;}
    longdoubleData = (long double) (this->OS_Memory_Total - this->OS_Memory_Available);
    longdoubleData = (long double) (longdoubleData / this->OS_Memory_Total)*100;
    this->OS_Memory_Usage  = (float) longdoubleData;
 
 return true;   
}

bool Memory_Data::fLoadOSswapUsage(string strSwapTotal, string strSwapFree) {
    
    long double     longdoubleData;
    float           floatData;
    size_t          found;
    
    found = strSwapTotal.find_first_not_of("0123456789");
    if (found != string::npos)                        {return false;}
    this->OS_Swap_Total = char2int(strSwapTotal.c_str());
    
    found = strSwapFree.find_first_not_of("0123456789");
    if (found != string::npos)                        {return false;}
    this->OS_Swap_Free = char2int(strSwapFree.c_str());  
 
    if (this->OS_Swap_Total == 0)                     {return false;}
    longdoubleData = (long double) (this->OS_Swap_Total - this->OS_Swap_Free);
    longdoubleData = (long double) (longdoubleData / this->OS_Swap_Total)*100;
    this->OS_Swap_Usage  = (float) longdoubleData;
  
    
 return true;   
}

bool Memory_Data::fLoadControllerCPUusage(string strCPU) {
 
    size_t found, firstdot, lastdot;
    found = strCPU.find_first_not_of("0123456789.");
    if (found != string::npos) {return false;}
    firstdot = strCPU.find_first_of(".");
    lastdot  = strCPU.find_last_of(".");
    if (firstdot != lastdot)   {return false;}
    
    this->Controller_APP_CPU_Usage = stof(strCPU);
    
    return true; 
}

bool Memory_Data::fLoadControllerMEMusage(string strMEM) {
 
    size_t found, firstdot, lastdot;
    found = strMEM.find_first_not_of("0123456789.");
    if (found != string::npos) {return false;}
    firstdot = strMEM.find_first_of(".");
    lastdot  = strMEM.find_last_of(".");
    if (firstdot != lastdot)   {return false;}
    
    this->Controller_APP_MEM_Usage = stof(strMEM);
    
    return true; 
}





bool Memory_Data::fLoadFreeswitchMemoryData(string strInput) {
    
 string         strData;
 long double    longdoubleData;
 float          floatData;
    
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_MEM_TOTAL_WO_COLON); 
 if (strData.length()) {
  this->Freeswitch_OS_Memory_Total = char2int(strData.c_str());
 }
 else {
  return false;   
 }
 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_MEM_AVAIL_WO_COLON); 
 if (strData.length()) {
  this->Freeswitch_OS_Memory_Available = char2int(strData.c_str());
 }
 else {
  return false;   
 }
 
 if (this->Freeswitch_OS_Memory_Total == 0.0) {return false;}
 longdoubleData = (long double) (this->Freeswitch_OS_Memory_Total - this->Freeswitch_OS_Memory_Available);
 longdoubleData = (long double) (longdoubleData / this->Freeswitch_OS_Memory_Total)*100;
 this->Freeswitch_OS_Memory_Usage  = (float) longdoubleData;

 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_SWAP_TOTAL_WO_COLON); 
 if (strData.length()) {
  this->Freeswitch_OS_Swap_Total = char2int(strData.c_str());
 }
 else {
  // return false;   
 }
 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_SWAP_FREE_WO_COLON); 
 if (strData.length()) {
  this->Freeswitch_OS_Swap_Free = char2int(strData.c_str());
 }
 else {
  // return false;   
 }
 
 if (this->Freeswitch_OS_Swap_Total == 0.0) {return false;}
 longdoubleData = (long double) (this->Freeswitch_OS_Swap_Total - this->Freeswitch_OS_Swap_Free);
 longdoubleData = (long double) (longdoubleData / this->Freeswitch_OS_Swap_Total)*100;
 this->Freeswitch_OS_Swap_Usage  = (float) longdoubleData;
 
 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_FSW_CPU_WO_COLON);
 if (strData.length()) {
  this->Freeswitch_APP_CPU_Usage  = stof(strData);
 }
 else {
  return false;   
 }
 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_FSW_MEM_WO_COLON);
 if (strData.length()) {
  this->Freeswitch_APP_MEM_Usage  = stof(strData);
 }
 else {
  return false;   
 }
 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_FSW_PID_WO_COLON);
 if (strData.length()) {
  this->Freeswitch_PID  = char2int(strData.c_str());
 }
 else {
  //return false;   
 }

 this->NewFreeswitchDataRX = true;
 return true;
}



// constructor
Freeswitch_Data::Freeswitch_Data()  {
  enumFreeswitchEvent		= NOTFOUND;
  boolValidPosition		= false;
  iConfDisplayNumber      	= 1;
  strFreeswitchConferenceUUID	= "";	
 }


//assignment operator
Freeswitch_Data& Freeswitch_Data::operator=(const Freeswitch_Data& b)
 {
  if (&b == this ) {return *this;}
  enumFreeswitchEvent    	= b.enumFreeswitchEvent;
  objChannelData         	= b.objChannelData;
  objRing_Dial_Data           	= b.objRing_Dial_Data;
  objLinkData         		= b.objLinkData;
  boolValidPosition          	= b.boolValidPosition;
  iConfDisplayNumber         	= b.iConfDisplayNumber;
  strFreeswitchConferenceUUID 	= b.strFreeswitchConferenceUUID;
  vectPostionsOnCall            = b.vectPostionsOnCall;
  vectDestChannelsOnCall        = b.vectDestChannelsOnCall;
  objReferData                  = b.objReferData;
  return *this;   
 }


void Freeswitch_Data::fClear()
{
 enumFreeswitchEvent 		= NOTFOUND;
 objChannelData.fClear();
 objReferData.fClear();
 objRing_Dial_Data.fClear();
 objLinkData.fClear();
 boolValidPosition              = false;
 iConfDisplayNumber             = 1;
 strFreeswitchConferenceUUID.clear();
 vectDestChannelsOnCall.clear();
 vectPostionsOnCall.clear();

}

void Freeswitch_Data::fDisplay()
{
  vector<Channel_Data>::size_type             sz = vectDestChannelsOnCall.size();
  vector<Channel_Data>::size_type             sze = vectPostionsOnCall.size();



   //cout << "FreeSwitch Data Display ..........................................." << endl;
   //cout << "Event                    : " << enumFreeswitchEvent << endl;
 
   //cout << "Ring/Dial Data ......>" << endl;
   objRing_Dial_Data.fDisplay();
   //cout << "Link Data .........>" << endl;
   objLinkData.fDisplay();


   //cout << "Vector Destinations on Call Data:" << endl;
   for (unsigned int i = 0; i< sz; i++)
    {
     vectDestChannelsOnCall[i].fDisplay(i,(!i)); 
    }
   //cout << "Vector Positions on Call Data:" << endl;
   for (unsigned int i = 0; i< sze; i++) 
    {
     vectPostionsOnCall[i].fDisplay(i,(!i)); 
    }
 
}


int Freeswitch_Data::fLoad_Bridge_Data_Into_Object( Link_Data objInput, int iChannel,  int iTrunk , bool boolCallerIDsReversed, string strConfDisplay, bool boolPush)
{
 int 				iPosition;
 vector_list                    enumVector;
 extern Telephone_Devices       TelephoneEquipment;
 extern bool 			bool_BCF_ENCODED_IP;
 

 //objChannelData.fClear();

 if (iTrunk < 0) 		{return -1;}
 if (iTrunk >intMAX_NUM_TRUNKS) {return -1;}


 this->objChannelData.fLoadLinkDataIntoChannelData(objInput, iChannel, boolCallerIDsReversed);

 this->objChannelData.fLoadTrunkNumber(iTrunk); 

 if      (this->objChannelData.boolIsPosition)  {this->objChannelData.fLoadConferenceDisplayP(this->objChannelData.iPositionNumber);    enumVector = POSITION_VECTOR;}
 else if (!strConfDisplay.empty())              {this->objChannelData.strConfDisplay = strConfDisplay; enumVector = DESTINATION_VECTOR;}     
 else if (!this->objChannelData.boolIsOutbound) {this->objChannelData.fLoadConferenceDisplayC(this->objChannelData.iTrunkNumber);       enumVector = DESTINATION_VECTOR;}
 else                                           {this->objChannelData.fLoadConferenceDisplayD(this->iConfDisplayNumber);this->iConfDisplayNumber++; enumVector = DESTINATION_VECTOR;
                                                 this->objChannelData.strCustName=CallerNameLookup(this->objChannelData.strCallerID);}
 
 
/*
 switch(iChannel)
  {
   case 1:
        objChannelData.strChannelName  = objInput.strChannelone;
        objChannelData.strChannelID    = objInput.strChanneloneID;
        if (boolCallerIDsReversed)
         {
          objChannelData.strCallerID     = objInput.strCallerIDtwo;       
          objChannelData.strCustName     = objInput.strCustNameTwo;
         }
        else
         {
          objChannelData.strCallerID     = objInput.strCallerIDone;       
          objChannelData.strCustName     = objInput.strCustNameOne;
         }
        objChannelData.IPaddress       = objInput.IPaddressOne;
        objChannelData.boolConnected   = true;
        objChannelData.boolOnHold      = false; 
        objChannelData.iTrunkNumber    = iTrunk; 
 
        if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(objChannelData.strChannelName);}
        else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objChannelData.IPaddress.stringAddress); } 
        objChannelData.fLoadPositionNumber(iPosition);
                
        if      (objChannelData.boolIsPosition)  {objChannelData.fLoadConferenceDisplayP(objChannelData.iPositionNumber); enumVector = POSITION_VECTOR;}
        else if (!strConfDisplay.empty())        {objChannelData.strConfDisplay = strConfDisplay; enumVector = DESTINATION_VECTOR;}     
        else if (!objChannelData.boolIsOutbound) {objChannelData.fLoadConferenceDisplayC(objChannelData.iTrunkNumber);    enumVector = DESTINATION_VECTOR;}
        else                                     {objChannelData.fLoadConferenceDisplayD(iConfDisplayNumber);iConfDisplayNumber++; enumVector = DESTINATION_VECTOR;
                                                  objChannelData.strCustName=CallerNameLookup(objChannelData.strCallerID);}
 
        break;
   case 2:
        objChannelData.strChannelName  = objInput.strChanneltwo;
        objChannelData.strChannelID    = objInput.strChanneltwoID;
        if (boolCallerIDsReversed)
         {
          objChannelData.strCallerID     = objInput.strCallerIDone;       
          objChannelData.strCustName     = objInput.strCustNameOne;
         }
        else
         {
          objChannelData.strCallerID     = objInput.strCallerIDtwo;       
          objChannelData.strCustName     = objInput.strCustNameTwo;
         }
        objChannelData.IPaddress       = objInput.IPaddressTwo;
        objChannelData.boolConnected   = true;
        objChannelData.boolOnHold      = false;
        objChannelData.iTrunkNumber    = iTrunk; 


        if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(objChannelData.strChannelName);}
        else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objChannelData.IPaddress.stringAddress); } 
        objChannelData.fLoadPositionNumber(iPosition);        

        if      (objChannelData.boolIsPosition)   {objChannelData.fLoadConferenceDisplayP(objChannelData.iPositionNumber); enumVector = POSITION_VECTOR;}
        else if (!strConfDisplay.empty())         {objChannelData.strConfDisplay = strConfDisplay; enumVector = DESTINATION_VECTOR;}     
        else if (!objChannelData.boolIsOutbound)  {objChannelData.fLoadConferenceDisplayC(objChannelData.iTrunkNumber);    enumVector = DESTINATION_VECTOR;}
        else                                      {objChannelData.fLoadConferenceDisplayD(iConfDisplayNumber);iConfDisplayNumber++; enumVector = DESTINATION_VECTOR;
                                                   objChannelData.strCustName=CallerNameLookup(objChannelData.strCallerID);}
 
        break;
    default: SendCodingError("freeswitch_data_functions.cpp - Coding error in fn void Freeswitch_Data::fLoad_Channel_Data_Into_Conference_Vector( Link_Data objInput, int iChannel )");
  }
*/


 if(!boolPush) {return 0;}
 switch (enumVector)
  {
   case POSITION_VECTOR: 
        if (!AddtoPositionVector(objChannelData)) {
         return -1;
        }
        else {
         return 0;
        }
   case DESTINATION_VECTOR: vectDestChannelsOnCall.push_back(objChannelData); return 0; 
   default: return 0;  
  }
 
}

bool Freeswitch_Data::fLoadChannelDataFromNonValetParkPickup(string strInput)
{
 size_t                                  SearchPosition;
 string                                  strData;
 int                                     index;
 extern Telephone_Devices                TelephoneEquipment; 


 objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
 if(!objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}

 objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
 if (objChannelData.strChannelName.empty())                                                            {return false;}

 objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON);
 if (objChannelData.strChannelID.empty())                                                              {return false;}

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);             
 objChannelData.boolIsOutbound = (strData == "outbound");

 index = TelephoneEquipment.fIndexWithIPaddress(objChannelData.IPaddress);
 if (index >=0) {objChannelData.fLoadPositionNumber(TelephoneEquipment.Devices[index].intPositionNumber);}


 objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
 objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));
 if      (objChannelData.iPositionNumber) {objChannelData.fLoadConferenceDisplayP(objChannelData.iPositionNumber);}

 objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_NUMBER_WO_COLON);

 //objChannelData.fDisplay(0,true);
 

 return true;
}


bool Freeswitch_Data::fLoadBridgeDataFromFreeswitchSLA(string strInput)
{
  extern Telephone_Devices       TelephoneEquipment;
/*
 //cout << FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON << endl;
 //cout << FREESWITCH_CLI_CALLER_UUID_WO_COLON << endl;
 //cout << FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON << endl;
 //cout << FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON << endl;
 //cout << FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON << endl << endl;;
 //cout << FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON << endl;
 //cout << FREESWITCH_CLI_OTHER_LEG_UUID_WO_COLON << endl;
 //cout << FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON << endl;
 //cout << FREESWITCH_CLI_CALLER_CALLEE_ID_NAME_WO_COLON << endl;
 //cout << FREESWITCH_CLI_CALLER_CALLEE_ID_NUM_WO_COLON << endl;
*/

 string strData;
 string strKey;

 objLinkData.strChannelone = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));
 if (objLinkData.strChannelone.empty())                                                                   {return false;}

 objLinkData.strChanneloneID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
 if (objLinkData.strChanneloneID.empty())                                                                 {return false;}


 objLinkData.IPaddressOne.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
 if(!objLinkData.IPaddressOne.fIsValid_IP_Address())                                                  {return false;}


 objLinkData.strCustNameOne = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NAME_WO_COLON)); 
 objLinkData.strCallerIDone = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NUM_WO_COLON)); 
 

 objLinkData.strCustNameTwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON)); 
 objLinkData.strCallerIDtwo = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
 

 objLinkData.strChanneltwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
 if (objLinkData.strChanneltwo.empty())                                                                   {return false;}

 objLinkData.strChanneltwoID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_UUID_WO_COLON));
 if (objLinkData.strChanneltwoID.empty())                                                                 {return false;}

 objLinkData.IPaddressTwo.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON);
 if(!objLinkData.IPaddressTwo.fIsValid_IP_Address())                                                      {return false;}


 objLinkData.fAreBothPositionChannels();

 this->objReferData.fLoadContactHost(strInput);
 this->objReferData.fLoadContactParams(strInput);
 this->objReferData.fLoadContactURI(strInput);
 this->objReferData.fLoadContactUser(strInput);
 this->objReferData.fLoadContactPort(strInput);

// this->objReferData.fDisplay();
// objLinkData.fDisplay();

 return true;
}

bool Freeswitch_Data::fLoadBridgeDataFromFreeswitchLineRoll(string strInput)
{
 extern Telephone_Devices       TelephoneEquipment;

 objLinkData.strChannelone = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));
 if (objLinkData.strChannelone.empty())                                                                   {return false;}

 objLinkData.strChanneloneID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
 if (objLinkData.strChanneloneID.empty())                                                                 {return false;}

 objLinkData.IPaddressOne.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
 if(!objLinkData.IPaddressOne.fIsValid_IP_Address())                                                      {return false;}


 objLinkData.strCustNameOne = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NAME_WO_COLON)); 
 objLinkData.strCallerIDone = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NUM_WO_COLON)); 
 
 objLinkData.strChanneltwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_CHANNEL_NAME_WO_COLON));
 if (objLinkData.strChanneltwo.empty())                                                                   {return false;}

 objLinkData.strChanneltwoID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_UUID_WO_COLON));
 if (objLinkData.strChanneltwoID.empty())                                                                 {return false;}

 objLinkData.IPaddressTwo.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_RING_NETWORK_ADDRESS_WO_COLON);
 if(!objLinkData.IPaddressTwo.fIsValid_IP_Address())                                                      {return false;}


 objLinkData.strCustNameTwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON)); 

 objLinkData.strCallerIDtwo = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 


 objLinkData.fAreBothPositionChannels();
 //objLinkData.fDisplay();

 this->objReferData.fLoadContactHost(strInput);
 this->objReferData.fLoadContactParams(strInput);
 this->objReferData.fLoadContactURI(strInput);
 this->objReferData.fLoadContactUser(strInput);
 this->objReferData.fLoadContactPort(strInput);
 return true;
}

bool Freeswitch_Data::fLoadLinkDataFromFreeswitchTransferLineRoll(string strInput)
{
 string                                  strData;
 extern Telephone_Devices       TelephoneEquipment;

 objLinkData.strChannelone = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CHANNEL_NAME_WO_COLON));
 if (objLinkData.strChannelone.empty())                                                                   {return false;}

 objLinkData.strChanneloneID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON));
 if (objLinkData.strChanneloneID.empty())                                                                 {return false;}

 objLinkData.IPaddressOne.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
 if(!objLinkData.IPaddressOne.fIsValid_IP_Address())                                                      {return false;}


 objLinkData.strCustNameOne = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NAME_WO_COLON)); 
 objLinkData.strCallerIDone = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NUM_WO_COLON)); 
 
 objLinkData.strChanneltwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
 if (objLinkData.strChanneltwo.empty())                                                                   {return false;}

 objLinkData.strChanneltwoID = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_UUID_WO_COLON));
 if (objLinkData.strChanneltwoID.empty())                                                                 {return false;}

 objLinkData.IPaddressTwo.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON);
 if(!objLinkData.IPaddressTwo.fIsValid_IP_Address())                                                      {return false;}



 objLinkData.strCustNameTwo = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON)); 
 objLinkData.strCallerIDtwo = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 


 objLinkData.fAreBothPositionChannels();
// objChannelData.fDisplay(0,true);
 

 return true;
}

bool Freeswitch_Data::fLoadChannelFromFreeswitchConferenceSLA(string strInput)
{
 size_t                                  SearchPosition;
 string                                  strData;
 string 				 strKey;
 int                                     index;
 bool					 boolLoopBackTransfer = false;
 extern Telephone_Devices                TelephoneEquipment; 
 extern bool                             boolAUDIOCODES_911_ANI_REVERSED;
 extern string                           strCALLER_ID_NAME;
 extern string                           strCALLER_ID_NUMBER;


 SearchPosition =  strInput.find(FREESWITCH_CLI_VARIABLE_CURRENT_APPLICATION_SOFIA_SLA);
 if (SearchPosition != string::npos) 
  {
 //   //cout << "A1" << endl;
    SearchPosition =  strInput.find(FREESWITCH_CHANNEL_STATE_NUMBER_SEVEN );
    if (SearchPosition == string::npos) {return false;}
//    //cout << "B1" << endl;
    SearchPosition =  strInput.find(FREESWITCH_CLI_OTHER_LEG_DIRECTION_INBOUND  );
    if (SearchPosition == string::npos) {return false;}
//    //cout << "C1" << endl;
    boolLoopBackTransfer = true;
  }


 // need GDIt BCF here .... for loopback or  SIP Trunk thru BCF ????..... This will not be executed right now no SLA with BCF ...
 switch (boolLoopBackTransfer)
  {
   case true:
         objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
          if(!objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
          break;
        
   case false:
        objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
        if(!objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}
        break;
  }
// //cout << "D1" << endl;
 objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
 if (objChannelData.strChannelName.empty())                                                            {return false;}
// //cout << "E1" << endl;
 objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON);
 if (objChannelData.strChannelID.empty())                                                              {return false;}
// //cout << "F1" << endl;
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);             
 objChannelData.boolIsOutbound = (strData == "outbound");

 index = TelephoneEquipment.fIndexWithIPaddress(objChannelData.IPaddress);
 if (index >=0) 
  {
   objChannelData.fLoadPositionNumber(TelephoneEquipment.Devices[index].intPositionNumber);
  }

 // this does not seem to matter ......
 switch (boolAUDIOCODES_911_ANI_REVERSED)
  {
   case true:
        objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_RDNIS_WO_COLON)); 
        objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NAME_WO_COLON));
        break;

   case false:
        objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_RDNIS_WO_COLON)); 
        objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLEE_ID_NAME_WO_COLON));
        break;
  }

 if (objChannelData.iPositionNumber) 
  {
   objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(strCALLER_ID_NUMBER); 
   objChannelData.strCustName = strCALLER_ID_NAME;
   objChannelData.fLoadConferenceDisplayP(objChannelData.iPositionNumber);
  }

 objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON);

// objChannelData.fDisplay(0,true);
 

 return true;



}

bool Freeswitch_Data::fLoadChannelFromFreeswitchTransferSLA(string strInput)
{
 size_t                                  SearchPosition;
 string                                  strData;
 int                                     index;
 extern Telephone_Devices                TelephoneEquipment; 


 SearchPosition =  strInput.find(FREESWITCH_CLI_VARIABLE_CURRENT_APPLICATION_THREE_WAY);
 if (SearchPosition == string::npos) {return false;}

 objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_NETWORK_ADDR_WO_COLON);
 if(!objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}

 objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_NAME_WO_COLON));
 if (objChannelData.strChannelName.empty())                                                            {return false;}

 objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_UUID_WO_COLON);
 if (objChannelData.strChannelID.empty())                                                              {return false;}

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_DIRECTION_WO_COLON);             
 objChannelData.boolIsOutbound = (strData == "outbound");

 index = TelephoneEquipment.fIndexWithIPaddress(objChannelData.IPaddress);
 if (index >=0) {objChannelData.fLoadPositionNumber(TelephoneEquipment.Devices[index].intPositionNumber);}


 objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NUM_WO_COLON)); 
 objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_CALLER_ID_NAME_WO_COLON));
 if      (objChannelData.iPositionNumber) {objChannelData.fLoadConferenceDisplayP(objChannelData.iPositionNumber);}

 objChannelData.strTranData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_EAVESDROP_TARGET_UUID_WO_COLON);

 //objChannelData.fDisplay(0,true);
 

 return true;
}

bool Freeswitch_Data::fLoadChannelDataFromOutboundDial(string strInput)
{
 string 			strData;
 int    			iPosition;
 extern Telephone_Devices       TelephoneEquipment;
 extern bool                    bool_BCF_ENCODED_IP;

 objChannelData.strChannelName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CHANNEL_NAME_WO_COLON));
 if (objChannelData.strChannelName.empty())                                                            {return false;}

 objChannelData.strChannelID = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_CALL_UUID_WO_COLON);
 if (objChannelData.strChannelID.empty())                                                              {return false;}

 objChannelData.IPaddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_NETWORK_ADDR_WO_COLON);
 if(!objChannelData.IPaddress.fIsValid_IP_Address())                                                   {return false;}

 objChannelData.boolIsOutbound = true;

 if (bool_BCF_ENCODED_IP) { iPosition = TelephoneEquipment.fPositionNumberFromChannelName(objChannelData.strChannelName);}
 else                     { iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objChannelData.IPaddress.stringAddress); } 
 objChannelData.fLoadPositionNumber(iPosition);

 objChannelData.strCallerID = TelephoneEquipment.fRemoveRoutingPrefix(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CALLER_ID_NUMBER_WO_COLON)); 
 objChannelData.strCustName = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_OTHER_LEG_CALLER_ID_NAME_WO_COLON)); 
 
 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_HEADER_TRUNK_WO_COLON);
 if(!strData.empty()) {strData = strData.erase(0,1);}

 if(!objChannelData.fLoadTrunkNumber(strData, strInput)) {SendCodingError("freeswitch_data_functions.cpp - fLoadChannelDataFromOutboundDial() Unable to load Trunk!");}

 if (objChannelData.iPositionNumber) {objChannelData.fLoadConferenceDisplayP(objChannelData.iPositionNumber);}
 else {return false;} // must be a position!!!!!

// objChannelData.fDisplay(0,true);

 return true;
}



size_t Freeswitch_Data::fNumberofChannelsInCall()
{
 vector<Channel_Data>::size_type         sz =  vectDestChannelsOnCall.size();
 vector<Channel_Data>::size_type         sze = vectPostionsOnCall.size();

 return sz+sze;
}

bool Freeswitch_Data::fLineRegistrationNameIsOnCall(string strName)
{
 vector<Channel_Data>::size_type      sz = vectPostionsOnCall.size();
 size_t                               found;

 if (!strName.length())     {return false;}

 for (unsigned int i = 0; i< sz; i++)
  {
   found = vectPostionsOnCall[i].strChannelName.find(strName);
   if (found != string::npos) {return true;}
  }

 return false;
}

int Freeswitch_Data::fFindLegendInDestVector(string strLegend)
{
 vector<Channel_Data>::size_type      sz = vectDestChannelsOnCall.size();
 if (!strLegend.length())     {return -1;}

 // check list first for match ...
 for (unsigned int i = 0; i< sz; i++)
  {
   if (strLegend == vectDestChannelsOnCall[i].strConfDisplay){ return i;}
  }// end for (unsigned int i = 0; i< sz; i++)
 
 return -1;

}

int Freeswitch_Data::fFindLegendInPosnVector(string strLegend)
{
 vector<Channel_Data>::size_type      sz = vectPostionsOnCall.size();
 if (!strLegend.length())     {return -1;}

 // check list first for match ...
 for (unsigned int i = 0; i< sz; i++)
  {
   if (strLegend == vectPostionsOnCall[i].strConfDisplay){ return i;}
  }// end for (unsigned int i = 0; i< sz; i++)
 
 return -1;
}


int Freeswitch_Data::fFindPositionInPosnVector(int iPos)
{ 
 vector<Channel_Data>::size_type      sz = vectPostionsOnCall.size();
 
 if (!iPos)                   {return -1;}

 // check list first for match ...
 if(!vectPostionsOnCall.empty())
  {
   for (unsigned int i = 0; i< sz; i++)
    {
      if ( iPos == vectPostionsOnCall[i].iPositionNumber) {return i;}
    }// end for (unsigned int i = 0; i< sz; i++)
  }// end if(!vectPostionsOnCall.empty())

 return -1;
}

int Freeswitch_Data::fFindChannelInPosnVector(string strChannelID)
{ 
 vector<Channel_Data>::size_type      sz = vectPostionsOnCall.size();
 
 if (strChannelID.empty())   {return -1;}

 // check list first for match ...
 if(!vectPostionsOnCall.empty())
  {
   for (unsigned int i = 0; i< sz; i++)
    {
      if ( strChannelID == vectPostionsOnCall[i].strChannelID) {return i;}
    }// end for (unsigned int i = 0; i< sz; i++)
  }// end if(!vectPostionsOnCall.empty())

 return -1;
}

int Freeswitch_Data::fFindChannelInDestVector(string strID)
{
 vector<Channel_Data>::size_type      sz = vectDestChannelsOnCall.size();

 if (strID.empty())          {return -1;}

 for (unsigned int i = 0; i< sz; i++)
  {
   if (vectDestChannelsOnCall[i].strChannelID == strID)    {return i;}
  }// end for (unsigned int i = 0; i< sz; i++)


 return -1;
}

int Freeswitch_Data::fFindDestChannel911Caller()
{
 vector<Channel_Data>::size_type      sz = vectDestChannelsOnCall.size();
 string                               C = ANI_CONF_MEMBER_CALLER_PREFIX;

 for (unsigned int i = 0; i< sz; i++)
  {
   if (vectDestChannelsOnCall[i].strConfDisplay.empty()) {continue;}
   if ((vectDestChannelsOnCall[i].strConfDisplay[0] == C[0])&&((vectDestChannelsOnCall[i].intTrunkType == CAMA)||(vectDestChannelsOnCall[i].intTrunkType == LANDLINE)||(vectDestChannelsOnCall[i].intTrunkType == WIRELESS))) {return i;}
  }

 return -1;
}

int Freeswitch_Data::fFindFirstDestChannelCLIDCaller()
{
 vector<Channel_Data>::size_type      sz = vectDestChannelsOnCall.size();

 // find first CLID channel
 for (unsigned int i = 0; i< sz; i++)
  {
   if (vectDestChannelsOnCall[i].intTrunkType == CLID)   {return i;}
  }

 return -1;
}






bool Freeswitch_Data::fRemovePosnChannelFromVector(unsigned int index)
{
 vector<Channel_Data>::size_type      sz = vectPostionsOnCall.size();

 if (index > sz)                 {return false;}
 if (vectPostionsOnCall.empty()) {return false;}

 vectPostionsOnCall.erase(vectPostionsOnCall.begin()+index);
 return true;
}

bool Freeswitch_Data::fRemoveDestChannelFromVector(unsigned int index)
{
 vector<Channel_Data>::size_type      sz = vectDestChannelsOnCall.size();
 if (index > sz)                     {return false;}
 if (vectDestChannelsOnCall.empty()) {return false;}
 vectDestChannelsOnCall.erase(vectDestChannelsOnCall.begin()+index);
 return true;
}

bool Freeswitch_Data::fLoad_Channels_in_LinkData()
{
 // load the linkData object with 2 channels
 // a position and a destination <or>
 // a position and a position
 // a Destination and a Destination
 // if there are more than 2 channels or less than
 // 2 channels return false
 //

 size_t                                  sz,szALLChannels;
 bool                                    boolNoDestChannel       = false;
 bool                                    boolTwoPositions        = false;
 bool                                    boolOneDestChannel      = false;
 bool                                    boolOnePosChannel       = false;
 bool                                    boolTwoDestChannel      = false;
 bool                                    boolNoPositionChannel   = false;



 // clear the link_data object
 this->objLinkData.fClear();


   // AMI will need channel data to build the conference
   sz = szALLChannels = this->vectDestChannelsOnCall.size();  
   if      (sz == 0) {boolNoDestChannel      = true;}
   else if (sz == 1) {boolOneDestChannel     = true;}
   else if (sz == 2) {boolTwoDestChannel     = true;}   
   sz = this->vectPostionsOnCall.size();
   szALLChannels += sz;
   if (sz == 0) {boolNoPositionChannel       = true;} 
   if (sz == 1) {boolOnePosChannel           = true;}    
   if (sz == 2) {boolTwoPositions            = true;}

   this->objLinkData.bBothChannelsPositions  = boolTwoPositions;

   //check that size is equal to 2 !!!!
   if      (szALLChannels > 2) {return false;}
   else if (szALLChannels < 2) {return false;}  
   else if ((boolOnePosChannel)&&( boolOneDestChannel)) 
    {
     this->objLinkData.strChannelone  = this->vectDestChannelsOnCall[0].strChannelName; this->objLinkData.strChanneloneID  = this->vectDestChannelsOnCall[0].strChannelID; this->objLinkData.strCallerIDone = this->vectDestChannelsOnCall[0].strCallerID;
     this->objLinkData.strChanneltwo  = this->vectPostionsOnCall[0].strChannelName;     this->objLinkData.strChanneltwoID  = this->vectPostionsOnCall[0].strChannelID;     this->objLinkData.strCallerIDtwo = this->vectPostionsOnCall[0].strCallerID;
    }
   else if ((boolTwoPositions)&&(boolNoDestChannel))
    {
     this->objLinkData.strChannelone  = this->vectPostionsOnCall[0].strChannelName; this->objLinkData.strChanneloneID  = this->vectPostionsOnCall[0].strChannelID; this->objLinkData.strCallerIDone = this->vectPostionsOnCall[0].strCallerID;
     this->objLinkData.strChanneltwo  = this->vectPostionsOnCall[1].strChannelName; this->objLinkData.strChanneltwoID  = this->vectPostionsOnCall[1].strChannelID; this->objLinkData.strCallerIDtwo = this->vectPostionsOnCall[1].strCallerID;
    }
   else if ((boolTwoDestChannel)&&(boolNoPositionChannel))
    {
     this->objLinkData.strChannelone  = this->vectDestChannelsOnCall[0].strChannelName; this->objLinkData.strChanneloneID  = this->vectDestChannelsOnCall[0].strChannelID; this->objLinkData.strCallerIDone = this->vectDestChannelsOnCall[0].strCallerID;
     this->objLinkData.strChanneltwo  = this->vectDestChannelsOnCall[1].strChannelName; this->objLinkData.strChanneltwoID = this->vectDestChannelsOnCall[1].strChannelID;  this->objLinkData.strCallerIDtwo = this->vectDestChannelsOnCall[1].strCallerID;  
    }
   else {return false;}


 

// //cout << "No  Dest Channel = " << boolNoDestChannel << endl;
// //cout << "One Dest Channel = " << boolOneDestChannel << endl;
// //cout << "Two Dest Channel = " << boolTwoDestChannel << endl;
// //cout << "No  Posn Channel = " << boolNoPositionChannel << endl;
// //cout << "One Posn Channel = " << boolOnePosChannel << endl;
// //cout << "Two Posn Channel = " << boolTwoPositions << endl;
// //cout << "One Xfer Channel = " << boolOneLocalXferChannel << endl;
// //cout << "Meetme Exits     = " << boolReturnMeetmeExists << endl;
 return true;
}


int Freeswitch_Data::fLineNumberofPositiononCall(int iPos)
{
 int 					index;
 int                                    iLineNumber;
 string					strChannelName;
 extern Telephone_Devices          	TelephoneEquipment;

 index = fFindPositionInPosnVector(iPos);
 if (index < 0) {return -1;}

 strChannelName = FreeswitchUsernameFromChannel(vectPostionsOnCall[index].strChannelName);
 if (strChannelName.empty()) {return -1;}

 iLineNumber = TelephoneEquipment.fFindTelephoneLineNumber(strChannelName);

 if (iLineNumber > 0) {return iLineNumber;}
 else                 {return -1;}
}

int Freeswitch_Data::fLineNumberofRingingCall(int iPos)
{
 int 					index;
 int                                    iLineNumber;
 string					strChannelName;
 extern Telephone_Devices          	TelephoneEquipment;

 index = fFindPositionInPosnVector(iPos);
 if (index < 0) {return -1;}

 strChannelName = FreeswitchUsernameFromChannel(vectPostionsOnCall[index].strChannelName);
 if (strChannelName.empty()) {return -1;}



 iLineNumber = TelephoneEquipment.fFindTelephoneLineNumber(strChannelName);

 if (iLineNumber > 0) {return iLineNumber;}
 else                 {return -1;}
}


void Freeswitch_Data::fSetOnHoldinPostionVector(int iPos) {
 int index;

 index = this->fFindPositionInPosnVector(iPos);
 if (index < 0) {return;}

 this->vectPostionsOnCall[index].boolOnHold = true;

 return;
}

void Freeswitch_Data::fSetOffHoldinPostionVector(int iPos) {
 int index;

 index = this->fFindPositionInPosnVector(iPos);
 if (index < 0) {return;}

 this->vectPostionsOnCall[index].boolOnHold = false;

 return;
}


bool Freeswitch_Data::AddtoPositionVector(Channel_Data objChannelData)
{
 size_t                                  sz = this->vectPostionsOnCall.size();;

 //check for duplicate and replace
 for (unsigned int i = 0; i < sz; i++)
  {
   if (this->vectPostionsOnCall[i].strChannelID == objChannelData.strChannelID){
    this->vectPostionsOnCall[i] = objChannelData;
    //cout << "got a duplicate position channel push" << endl;
    return false;
   }
  }
 this->vectPostionsOnCall.push_back(objChannelData);

 return true;
}

bool Freeswitch_Data::fBuild_FreeSWITCH_Conference(bool bBlindXfer)
{

 size_t                                  sz,szALLChannels;
 int                                     index = -1;
 bool                                    bNoDestChannel = false;

 // clear the link_data object
 this->objLinkData.fClear();


   // AMI will need channel data to build the conference
 sz = szALLChannels = this->vectDestChannelsOnCall.size();  
 if      (sz != 1)                                  {bNoDestChannel = true;}


 sz = this->vectPostionsOnCall.size();
 szALLChannels += sz;
 if (sz == 0)                                       {SendCodingError("freeswitch_data_functions.cpp: fBuild_FreeSWITCH_Conference() No Position Channels");return false;} 
 
 switch (szALLChannels)
  {
   case 0: case 1: return false;

   case 2: return fLoad_Channels_in_LinkData();

   default:
           if (bNoDestChannel)                              {SendCodingError("freeswitch_data_functions.cpp: fBuild_FreeSWITCH_Conference() No Dest Channels");return false;}
           // find the "barge" attached channel in the position vector
   	   for (unsigned int i = 0; i < sz; i++)
            {
             if (this->vectPostionsOnCall[i].iSLABarge > 0) { index = i; break;} 
            }
           if (index < 0)                                   {return false;}

           this->objLinkData.strChannelone    = this->vectPostionsOnCall[index].strChannelName;
           this->objLinkData.strChanneloneID  = this->vectPostionsOnCall[index].strChannelID;
           this->objLinkData.strCallerIDone   = this->vectPostionsOnCall[index].strCallerID;
           this->objLinkData.strChanneltwo    = this->vectDestChannelsOnCall[0].strChannelName;
           this->objLinkData.strChanneltwoID  = this->vectDestChannelsOnCall[0].strChannelID; 
           this->objLinkData.strCallerIDtwo   = this->vectDestChannelsOnCall[0].strCallerID;          
  }

 return true;
}







bool Freeswitch_Data::fLoad_EavesDrop_into_Channelobject()
{
 size_t                                  szDestVector,szPosnVector;

// We load the position channels so the conference will load correctly ....

 szDestVector = this->vectDestChannelsOnCall.size();
 szPosnVector = this->vectPostionsOnCall.size();
 

 if      (szPosnVector > 0)  { this->objChannelData = vectPostionsOnCall[0];     return true;}
 else if (szDestVector > 0)  { this->objChannelData = vectDestChannelsOnCall[0]; return true;}
 else                                                                          {return false;}
}



bool Freeswitch_Data::fLoad_LinkChannel_into_Channelobject(int ichannel)
{
 extern Telephone_Devices       TelephoneEquipment;
 extern bool 			bool_BCF_ENCODED_IP;

 switch (ichannel)
  {
   case 1:
          this->objChannelData.fClear();
          this->objChannelData.strChannelName = this->objLinkData.strChannelone;
          this->objChannelData.strChannelID   = this->objLinkData.strChanneloneID;
          this->objChannelData.strCustName    = this->objLinkData.strCustNameOne;
          this->objChannelData.IPaddress      = this->objLinkData.IPaddressOne;

          break;
   case 2:
          this->objChannelData.fClear();
          this->objChannelData.strChannelName = this->objLinkData.strChanneltwo;
          this->objChannelData.strChannelID   = this->objLinkData.strChanneltwoID;
          this->objChannelData.strCustName    = this->objLinkData.strCustNameTwo;
          this->objChannelData.IPaddress      = this->objLinkData.IPaddressTwo;
          break;
   default:
          SendCodingError( "freeswitch_data_functions.cpp - Coding Error in Freeswitch_Data::fLoad_LinkChannel_into_Channelobject()");
          return false;
  }
 switch (bool_BCF_ENCODED_IP) 
  {
   case true:
         this->objChannelData.iPositionNumber = TelephoneEquipment.fPositionNumberFromChannelName(this->objChannelData.strChannelName);
         break;
   case false:
         this->objChannelData.iPositionNumber = TelephoneEquipment.fPostionNumberFromIPaddress(this->objChannelData.IPaddress.stringAddress);
         break;
  }
  this->objChannelData.boolIsPosition = (this->objChannelData.iPositionNumber > 0);


 return true;
}


bool Freeswitch_Data::fLoad_Last_Channel_into_Channelobject()
{
 size_t                                  szDestVector,szPosnVector;

 if (fNumberofChannelsInCall() != 1) {return false;}

 szDestVector = vectDestChannelsOnCall.size();
 szPosnVector = vectPostionsOnCall.size(); 

 objChannelData.fClear();

 if (szDestVector > 0)
  {
   objChannelData = vectDestChannelsOnCall[0]; return true;
  }
 else if (szPosnVector > 0)
  {
   objChannelData = vectPostionsOnCall[0]; return true; 
  }
 return false;
}

string Freeswitch_Data::fFindDestinationChannelIDfromPhoneNumber(string strXferToNum)
{
 vector<Channel_Data>::size_type      sz = vectDestChannelsOnCall.size();

 if (!strXferToNum.length())     {return "";}

 // check list first for match ...
 if(!vectDestChannelsOnCall.empty())
  {
   for (unsigned int i = 0; i< sz; i++)
    {
     if (strXferToNum == vectDestChannelsOnCall[i].strCallerID)
      { return vectDestChannelsOnCall[i].strChannelID;}
    }// end for (unsigned int i = 0; i< sz; i++)
  }// end if(!vectDestChannelsOnCall.empty())


return "";
}



string Freeswitch_Data::fFindPositionChannelIDfromPhoneNumber(string strXferToNum)
{
 vector<Channel_Data>::size_type      sz = vectPostionsOnCall.size();

 if (!strXferToNum.length())     {return "";}

 // check list first for match ...
 if(!vectPostionsOnCall.empty())
  {
   for (unsigned int i = 0; i< sz; i++)
    {
     if (strXferToNum == vectPostionsOnCall[i].strCallerID)
      { return vectPostionsOnCall[i].strChannelID;}
    }// end for (unsigned int i = 0; i< sz; i++)
  }// end if(!vectPostionsOnCall.empty())


return "";
}


void NG911_SIP_Headers::fLoadCallInfoHeaders(string strInput)
{
 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CALL_INFO_WO_COLON);

 //cout << strData << endl;

}












