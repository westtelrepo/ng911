/*****************************************************************************
* FILE: freeswitchcli.cpp
* 
* DESCRIPTION: This file contains all of the functions integral to the freeswitch cli thread.
*
* Features:
*
*    3. Data Reliability Checks             Prevents bad data from causing system crashes
*    6. Heartbeats:                         Sends Heartbeat Message to Freeswitch
*    7. Email/Alarm Notification:           Informational messages on startup;
*                                           Warning messages for unusual activity;
*                                           Alarm messages for connection down/unavailable;
*                                           Reminder messages that alarm condition still exists
*    8. Denial of Service Protection:       Ignores packets from non-registered IP addresses;
*    9. Communications:                     Utilizes TCP communications for connection to Freeswitch;
*                                           If necessary converted to serial communication via Ethernet to serial converter
*   10. Self-Diagnostics:                   Built in diagnostics to help troubleshoot line errors, etc.
*   11. Threaded Implementation:            Provides efficient utilization and reduces resource monopolization
*   12. NENA Compliant:                     Complies with the “NENA Recommended Generic Standards for E9-1-1 PSAP Equipment”
*
* AUTHOR: 2/2/2010 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2022 WestTel Corporation 
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"                          // XML Software

using namespace std;

void  *Freeswitch_CLI_TCP_Listen_Thread(void *voidArg);
void  *Freeswitch_CLI_Login_Thread(void *voidArg);
//void   AMI_Messaging_Monitor_Event(union sigval union_sigvalArg);
int    ConnectToFreeswitch();
int    LogInToFreeswitch();
void   Ping_Freeswitch_Event(union sigval union_sigvalArg);
int    SendFreeswitchMemUsage();
int    SendConferenceInvite(ExperientDataClass objData);
int    BuildMeetMeConf(ExperientDataClass objData);
int    UN_Conference(ExperientDataClass objData);
int    Add_Conference_List(ExperientDataClass objData);
int    Build_Conference_List(ExperientDataClass objData);
int    SendTDDstring(ExperientDataClass objData);
int    TDDMode_AMI(ExperientDataClass objData, tddmodeset modeset = MODE_ON);
int    TDDMode_AGI(string strChannel, Call_Data objCallData, TDD_Data objTDDdata, tddmodeset modeset = MODE_ON);
int    CancelTransfer(ExperientDataClass objData);
int    SendMWI(ExperientDataClass objData);
string DecodeTDDmessage(string strInput);
int    GetVariable(string strChannel, string stVariable );
int    ProcessCLIData(ExperientDataClass objData);
int    Kill_Position_Channel(ExperientDataClass objData);
int    FreeswitchRingBack(ExperientDataClass objData);
void   Delayed_CLI_HANGUP(union sigval union_sigvalArg);
int    EavesDrop(ExperientDataClass objData);
int    Play_Sorry_Message(ExperientDataClass objData);
int    Send_to_Hangup(ExperientDataClass objData);
int    HangupChannel(ExperientDataClass objData);
int    ThreeWay(ExperientDataClass objData);
int    Send_to_Conference(ExperientDataClass objData);
int    SendTextMessage(ExperientDataClass objData);
int    SendFlashHook(ExperientDataClass objData);
int    SendDeflect(ExperientDataClass objData);
int    SendSyncTime();
int    Send_RFAI_Info_Message(ExperientDataClass objData);
int    Add_Conference_Member(ExperientDataClass objData);
int    Move_Valet_To_Conference(ExperientDataClass objData);
string EncodeBase64(string strInput);
bool   StartCLIListenThreads(pthread_t ANIPortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t params[]);
bool   ShutDownCLIports(pthread_t CLIPortListenThread[], int numberofports);
bool   InitializeCLIports();
extern void   *AMI_Messaging_Monitor_Event(void *voidArg);
string FreeswitchReferStringSLA(ExperientDataClass objData);
// Globals

pthread_t                  FreeswitchCLILoginThread;
timer_t                    timer_tFreeswitch_Ping_Event_TimerId;
timer_t                    timer_tCLIMessageMonitorTimerId;
timer_t                    timer_tCLI_Send_Event_TimerId;
struct itimerspec          itimerspecPingInterval;
struct itimerspec          itimerspecCLIMessageMonitorDelay;
struct itimerspec          itimerspecCLISendDelay;
extern sem_t               sem_tMutexCLIMessageQ;
int                        intDELAY_AFTER_FLASH_HOOK_MS       = 500;
int                        intDELAY_BETWEEN_DTMF_MS           = 250;
int                        intDelay_AFTER_STAR_EIGHT_PD_MS    = 900;
int                        intDelay_BETWEEN_DTMF_INDIGITAL_MS = 250;
volatile bool              boolTCP_LISTEN_THREAD_SHUTDOWN     = false;
volatile bool              boolAMI_LOGIN_THREAD_SHUTDOWN      = false;
bool                       boolAMI_TEST_SCRIPT                = false;
bool                       boolAMI_RECORD_SCRIPT              = true;
string                     strTCP_Buffer;
sem_t                      sem_tAMIFlag;
int                        intNUM_CLI_PORTS                   = 1; //for now ...


string strACTIONCOMMAND                                  = "Action: Command";
string strACTIONREDIRECT                                 = "Action: Redirect";
string strACTIONLOGIN                                    = "Action: login";
string strACTIONPING                                     = "Action: ping\r\n";
string strACTIONTDDMODE                                  = "Action: TDDMode";
string strACTIONSENDTDD                                  = "Action: SendTDD";
string strACTIONBRIDGE                                   = "Action: Bridge";
string strPACKET                                         = "packet";
string strXML_ATTRIBUTE_DESTINATION_NUMBER               = "Destination";
string strXML_ATTRIBUTE_DESTINATION_TYPE                 = "DestinationType";
string strXML_ATTRIBUTE_Freeswitch_TRUNK_UNIQUE_ID       = "FreeswitchTrunkChannelID";
string strXML_ATTRIBUTE_Freeswitch_POSITION_UNIQUE_ID    = "FreeswitchPositionChannelID";
string strXML_ATTRIBUTE_Freeswitch_TRUNK_CHANNEL         = "FreeswitchTrunkChannel";
string strXML_ATTRIBUTE_Freeswitch_POSITION_CHANNEL      = "FreeswitchPositionChannel";
string strXML_ATTRIBUTE_Freeswitch_TRANSFER_CHANNEL_LIST = "FreeswitchLocalTransferChannel";
string strXML_ATTRIBUTE_Freeswitch_POSITION              = "Position";
string strXML_ATTRIBUTE_Freeswitch_TRUNK                 = "Trunk";
string strXML_ATTRIBUTE_Freeswitch_CONFERENCE_ID         = "ConferenceID";
string strXML_ATTRIBUTE_CALLER_ID                        = "Callback";
string strXML_ATTRIBUTE_TDD_SEND_TEXT                    = "TDDmessage";
string strXML_ATTRIBUTE_CALL_MUTE                        = "TDDmute";
string strXML_ATTRIBUTE_TDD_ENCODING                     = "EncodingTDD";
string strXML_ATTRIBUTE_Freeswitch_VARIABLE_NAME         = "VariableName";
string strXML_ATTRIBUTE_Freeswitch_CONFERENCE_NUMBER     = "ConferenceNumber";


string strACTION_LOGIN_FREESWITCH                        = "auth experient\r\n";
string strEVENTPLAINALL                                  = "event plain ALL\r\n";
string strADD_EVENT_LIST                                 = "event plain HEARTBEAT MESSAGE TEXT CALL_UPDATE CHANNEL_PROGRESS CHANNEL_PROGRESS_MEDIA MEDIA_BUG_START MEDIA_BUG_STOP CHANNEL_ORIGINATE CHANNEL_BRIDGE CHANNEL_HOLD CHANNEL_UNHOLD PRESENCE_IN CHANNEL_HANGUP_COMPLETE CHANNEL_ANSWER NOTIFY_IN CUSTOM sofia::error myevent::message myevent::memory_usage sofia::register sofia::notify_refer conference::maintenance TDD::RECV_MESSAGE valet_parking::info verto::client_disconnect verto::client_connect \r\n";

string strPING_EVENT                                     = "sendevent ping\n\n";

extern Text_Data                                objBLANK_TEXT_DATA;
extern Call_Data                                objBLANK_CALL_RECORD; 
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_AMI_PORT;
extern queue <MessageClass>                     queue_objMainMessageQ;
extern queue <MessageClass>                     queue_objAMIMessageQ;
extern queue <ExperientDataClass>               queue_objAMIData;
extern Initialize_Global_Variables              INIT_VARS;

       ExperientTCPPort                         FreeswitchCLIPort;

/****************************************************************************************************
*
* Name:
*   Function void *AMI_Thread(void *voidArg)
*
****************************************************************************************************/
void *AMI_Thread(void *voidArg)
{
 int                      intRC;
 MessageClass             objMessage;
 string                   strLogin,strUsername,strSecret;
 struct sigevent          sigeventPingFreeswitchEvent;
 struct sigevent          sigeventAMIMessagingMonitorEvent;
 pthread_attr_t           AMIpthread_attr_tAttr;
 pthread_t                pthread_tAMImessagingThread;
 string                   strTemp;
 ExperientDataClass       objAMIData;
 struct timespec          timespecRemainingTime;
 Thread_Data              ThreadDataObj;
 param_t                  params[intNUM_CLI_PORTS + 1]; 
 pthread_t                FreeswitchCLIListenThread[intNUM_CLI_PORTS + 1];

 extern vector <Thread_Data>    ThreadData;
 extern Thread_Trap             OrderlyShutDown;
 extern Thread_Trap             UpdateVars;

 pthread_attr_init(&AMIpthread_attr_tAttr);

/*
 // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,805, objBLANK_CALL_RECORD, objBLANK_AMI_PORT, KRN_MESSAGE_142); enQueue_Message(AMI,objMessage);}
*/

 ThreadDataObj.strThreadName ="AMI";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in freeswitch_cli.cpp::AMI_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,800, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            AMI_MESSAGE_800, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(AMI,objMessage);


 // configure Freeswitch AMI TCP Port
 string strEOL       = "\r\n";

 InitializeCLIports();
// FreeswitchCLIPort.SetEOL((char*)strEOL.c_str(), 2);
// FreeswitchCLIPort.SetLocalHost(NULL);
// FreeswitchCLIPort.SetLocalPort(Freeswitch_AMI_PORT_NUMBER+1);
// FreeswitchCLIPort.SetRemotePort(Freeswitch_AMI_PORT_NUMBER);
// FreeswitchCLIPort.SetRemoteHost((char*) strFreeswitch_AMI_IP_ADDRESS.c_str());
 ConnectToFreeswitch();
 
/*
  // start AMI TCP listening thread
 pthread_attr_setdetachstate(&AMIpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
 intRC = pthread_create(&FreeswitchCLIListenThread, &AMIpthread_attr_tAttr, *Freeswitch_CLI_TCP_Listen_Thread, (void*) 0);
 if (intRC) {SendCodingError("freeswitch_cli.cpp - Unable to create AMI Listen thread" ); exit(1);}
*/

 if (!StartCLIListenThreads(FreeswitchCLIListenThread, &AMIpthread_attr_tAttr, params)) { SendCodingError("freeswitch_cli.cpp - CLI listen thread failure!");exit(1);}

 //Start AMI Freeswitch Login Thread
 pthread_attr_setdetachstate(&AMIpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
 intRC = pthread_create(&FreeswitchCLILoginThread, &AMIpthread_attr_tAttr, *Freeswitch_CLI_Login_Thread, (void*) 0);
 if (intRC) {SendCodingError("freeswitch_cli.cpp - Unable to create AMI Login thread"); exit(1);}

 // set up Message ping Timed Event
 sigeventPingFreeswitchEvent.sigev_notify                       = SIGEV_THREAD;
 sigeventPingFreeswitchEvent.sigev_notify_function              = Ping_Freeswitch_Event;
 sigeventPingFreeswitchEvent.sigev_notify_attributes            = NULL;
 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventPingFreeswitchEvent, &timer_tFreeswitch_Ping_Event_TimerId);
 if (intRC){Abnormal_Exit(AMI, EX_OSERR, KRN_MESSAGE_180, "timer_tFreeswitch_Ping_Event_TimerId", int2strLZ(errno));}

// intRC = LogInToFreeswitch();

 // load settings into stucture and then stucture into the timer
 itimerspecPingInterval.it_value.tv_sec                       = FREESWITCH_PING_INTERVAL;           // Delay for n sec
 itimerspecPingInterval.it_value.tv_nsec                      = 0;                                  //           n nsec
 itimerspecPingInterval.it_interval.tv_sec                    = FREESWITCH_PING_INTERVAL;           // if both 0 run once
 itimerspecPingInterval.it_interval.tv_nsec                   = 0;                                  //           n nsec
 timer_settime(timer_tFreeswitch_Ping_Event_TimerId, 0, &itimerspecPingInterval, NULL);             // arm timer
/*
  // set up AMI message monitor timer event
 sigeventAMIMessagingMonitorEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventAMIMessagingMonitorEvent.sigev_notify_function 		= AMI_Messaging_Monitor_Event;
 sigeventAMIMessagingMonitorEvent.sigev_notify_attributes 		= NULL;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventAMIMessagingMonitorEvent, &timer_tCLIMessageMonitorTimerId);
 if (intRC){Abnormal_Exit(AMI, EX_OSERR, KRN_MESSAGE_180, "timer_tCLIMessageMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecCLIMessageMonitorDelay, 0,intAMI_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intAMI_MESSAGE_MONITOR_INTERVAL_NSEC );
 Set_Timer_Delay(&itimerspecCLIMessageMonitorDelay, 0,intAMI_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0 );
 timer_settime( timer_tCLIMessageMonitorTimerId, 0, &itimerspecCLIMessageMonitorDelay, NULL);		// arm timer
*/


 intRC = pthread_create(&pthread_tAMImessagingThread, &AMIpthread_attr_tAttr, *AMI_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(AMI, EX_OSERR, AMI_MESSAGE_897);}


 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,803, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, AMI_MESSAGE_803); 
 enQueue_Message(AMI,objMessage);


 // main loop ***************************************************************************
 do
 {
  intRC = sem_wait(&sem_tAMIFlag);							// wait for data signal
//  //cout << "past signal" << endl;
  // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
  if (intRC)   {
   
    objMessage.fMessage_Create(LOG_WARNING,806, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, AMI_MESSAGE_806); 
    enQueue_Message(AMI,objMessage);
   
  } 
 
 // Shut Down Trap Area   
 while ((!boolSTOP_EXECUTION )&&(!queue_objAMIData.empty()))
  {
   objAMIData.fClear_Record();
   intRC = sem_wait(&sem_tMutexAMIInputQ);
   if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexAMIInputQ, "sem_wait@sem_tMutexAMIInputQ in *AMI_Thread()", 1);}
   objAMIData = queue_objAMIData.front();
   queue_objAMIData.pop();
 //  //cout << "popped Queue" << endl;
   sem_post(&sem_tMutexAMIInputQ);
   ProcessCLIData(objAMIData);
  }

 if (boolUPDATE_VARS) 
   {
    UpdateVars.boolAMIThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
     if (!boolSTOP_EXECUTION) {
  
      if (INIT_VARS.ANIinitVariable.boolRestartFreeswitchCLIport){
       ShutDownCLIports(FreeswitchCLIListenThread, 1);
       INIT_VARS.ANIinitVariable.boolRestartFreeswitchCLIport = false;
       InitializeCLIports();
       if (!StartCLIListenThreads(FreeswitchCLIListenThread, &AMIpthread_attr_tAttr, params)) { SendCodingError("freeswitch_cli.cpp - listen thread failure - recommend restart!");;}
      }
     }

    
    UpdateVars.boolAMIThreadReady = false;       
   }
           




 } while (!boolSTOP_EXECUTION);
 // end main loop *********************************************************************** 
// //cout << "before timer delete" << endl;
 timer_delete(timer_tFreeswitch_Ping_Event_TimerId);
// //cout << "after timer delete" << endl;
// timer_delete(timer_tCLIMessageMonitorTimerId);
     for (int i = 1; i <= intNUM_ANI_PORTS; i++)
      {
       pthread_join( FreeswitchCLIListenThread[i], NULL); 
      } 

 pthread_join( FreeswitchCLILoginThread, NULL); 
 pthread_join( pthread_tAMImessagingThread, NULL); 
 OrderlyShutDown.boolAMIThreadReady = true;
// sleep(intTHREAD_SHUTDOWN_DELAY_SEC);
 while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}       
 objMessage.fMessage_Create(0, 899, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, AMI_MESSAGE_899);
 objMessage.fConsole();
 return NULL;
}// void *AMI_Thread(void *voidArg)




int ProcessCLIData(ExperientDataClass objData)
  {

   string                       strTrunkChannel;
   string                       strPositionChannel;
   string                       strTransferChannelList;
   string                       strDestinationExtension;
   string                       strAMIActionRedirect;
   string                       strPosition;
   string                       strTrunk;
   string                       strConfNum;
   string                       strConfID;
   string                       strCallerID;
   string                       strText;
   string                       strDestination;
   string                       strPositionExtension;
   string                       strTransferType;
   string                       strVariable;
   int                          intRC;
   
  // //cout << "sending CLI -> " << objData.enumANIFunction << endl;  
   switch (objData.enumANIFunction)  {
       
     case EXEC_FREESWITCH_MEMORY_USAGE_SCRIPT:
              intRC = SendFreeswitchMemUsage();
              return intRC;
     case TRANSFER:
              // "InitiateTransfer"
 //             //cout << "CLI TRANSFER" << endl;
              intRC = SendConferenceInvite(objData);
              return intRC;
     case BLIND_TRANSFER:
              // "InitiateTransfer"
             ////cout << "CLI BLIND_TRANSFER" << endl;
              intRC = SendDeflect(objData);
              return intRC;

     case CANCEL_TRANSFER:   
               // "CancelTransfer"
 //             //cout << "CLI CANCEL_TRANSFER" << endl;
              intRC = CancelTransfer(objData);
              return intRC;

     case CONNECT:
              // Build Meetme Conference room
 //             //cout << "CLI CONNECT" << endl;
              intRC = BuildMeetMeConf(objData);
              return intRC;
     case RING_BACK:
//              //cout << "CLI RINGBACK" << endl;
              intRC = FreeswitchRingBack(objData);
              return intRC;
     case VALET_JOIN:
           //   //cout << "Valet join" << endl;
              intRC = Move_Valet_To_Conference(objData);
              return intRC;
     case EAVESDROP:
              //cout << "CLI EAVESDROP" << endl;
              intRC = EavesDrop(objData);
              strText = int2str(abs(intRC));
              if (intRC < 0) {
               strText= " -"+strText; 
              }
              if (intRC != 0) {
               SendCodingError( "Freeswitch_cli.cpp - Function Eavesdrop() failed with return code: "+strText);
              }        
              return intRC;

     case UN_CONFERENCE:
              //Generate Kill Conference
              ////cout << "CLI UN_CONFERENCE" << endl;
              intRC = UN_Conference(objData);
              return intRC;

     case CONFERENCE_ADD_MEMBER:
  //            //cout << "CLI ADD MEMBER" << endl;
              intRC = Add_Conference_Member(objData);
              return intRC;

     case CONFERENCE_JOIN_LIST:
  //            //cout << "CLI JOIN LIST" << endl;
              intRC = Add_Conference_List(objData);
              return intRC;

     case CONFERENCE_BUILD_LIST:
  //            //cout << "CLI BUILD LIST" << endl;
              intRC = Build_Conference_List(objData);
              return intRC;

     case CONFERENCE_LEAVE:
   //           //cout << "CLI CONFERENCE LEAVE" << endl;
              intRC = Kill_Position_Channel(objData);
              return intRC;

     case TDD_DISPATCHER_SAYS:
              // Send TDD Text
   //            //cout << "CLI DISPATCHER SAYS" << endl;            
              intRC = SendTDDstring( objData);
              intRC = 0;
              return intRC;

     case TDD_MODE_ON:
              // Mute TDD Mode
  //            //cout << "CLI TDD MODE ON" << endl;
              intRC = TDDMode_AMI(objData, MODE_ON);

              return intRC;

     case TDD_MODE_OFF:
  //            //cout << "CLI TDD MODE OFF" << endl; 
              intRC = TDDMode_AMI(objData, MODE_OFF);
              return intRC;

     case TDD_MUTE:
              // toggle TDD mute
    //          if (objData.TDDdata.boolTDDmute) { intRC = TDDMode_AMI(objData.FreeswitchData.objChannelData.strChannelName, objData.CallData, objData.TDDdata, MUTE_OFF);}
    //          else                             { intRC = TDDMode_AMI(objData.FreeswitchData.objChannelData.strChannelName, objData.CallData, objData.TDDdata, MUTE_ON);}
              intRC = 0;
              return intRC;
     case SMS_SEND_TEXT:

              intRC = SendTextMessage(objData);
              return intRC;

     case BARGE_ON_POSITION: 
  //            //cout << "CLI BARGE ON POSITION" << endl; 
              // hang up the channel for a barge
              intRC = Send_to_Hangup(objData);
              return intRC;

     case BARGE_ON_CONFERENCE:
 //             //cout << "CLI BARGE ON CONFERENCE" << endl;
               if(objData.CallData.CTIsharedLine) { intRC = ThreeWay(objData);}
               return intRC;

     case BARGE_ON_CONFERENCE_LINE_ROLL:
 //             //cout << "CLI BARGE ON CONFERENCE LINE ROLL" << endl;
               intRC =  Send_to_Conference(objData);
               return intRC;

     case PLAY_SORRY_MSG:
 //             //cout << "CLI PLAY SORRY" << endl;
              intRC = Play_Sorry_Message(objData);
              return intRC;

     case KILL_CHANNEL: case MSRP_HANGUP:
  //            //cout << "CLI HANGUP CHANNEL" << endl;
              intRC = HangupChannel(objData);
              return intRC;

     case SEND_FLASH:
 //             //cout << "CLI FLASHOOK" << endl;
              intRC = SendFlashHook(objData);
              return intRC;
     case SYNC_TIME:
              intRC = SendSyncTime();
              return intRC;
     case RFAI_BRIDGE: case RFAI_DROP_LEG:
             intRC = Send_RFAI_Info_Message(objData);
             return intRC;
     case SEND_MWI:
             intRC = SendMWI(objData);
          return intRC;
     default:
     //         //cout << "Invalid XML Node" << endl << AMIData << endl;
              return 1;

   }// end switch

   



         
  // //cout << "Invalid XML Packet" << endl << AMIData << endl;
   return 1;
  }

void Ping_Freeswitch_Event(union sigval union_sigvalArg)
{
 if (boolSTOP_EXECUTION)             {return;}
 if (!FreeswitchCLIPort.GetConnected()){return;}
 FreeswitchCLIPort.SendLine((char*) strPING_EVENT.c_str());
}


int ConnectToFreeswitch()
{
 if (boolSTOP_EXECUTION) {return 0;}
 if(!boolSTOP_EXECUTION) {FreeswitchCLIPort.SetConnected(true);}
 if(!boolSTOP_EXECUTION) {FreeswitchCLIPort.SetAcceptData(true);}
 return 0;
}

int SendMWI(ExperientDataClass objData) {

 int    intRC;
 string strMWImessage = objData.FreeswitchData.objChannelData.strTranData + "\n";

 intRC = FreeswitchCLIPort.SendLine(strMWImessage.c_str());
 return intRC;
}

int Play_Sorry_Message(ExperientDataClass objData)
{
 int    intRC;

 string strTryAgain = "api uuid_transfer " + objData.CallData.TransferData.strTransferFromUniqueid + " Try-Again XML features\n\n";
 intRC = FreeswitchCLIPort.SendLine(strTryAgain.c_str());
 return intRC;
}

int Move_Valet_To_Conference(ExperientDataClass objData)
{
  int intRC;
  string strConfJoinOne = "api uuid_transfer " + objData.FreeswitchData.objChannelData.strChannelID + " -both VALET_JOIN XML features\n\n";
  string strSetVarOne   = "api uuid_setvar "   + objData.FreeswitchData.objChannelData.strChannelID + " conference_name " + objData.CallData.strFreeswitchConfName + "\n\n";
  intRC = FreeswitchCLIPort.SendLine(strSetVarOne.c_str());
  intRC = FreeswitchCLIPort.SendLine(strConfJoinOne.c_str());
 return intRC;
}

int Add_Conference_Member(ExperientDataClass objData)
{
  int intRC;
  string strConfJoinOne = "api uuid_transfer " + objData.FreeswitchData.objChannelData.strChannelID + " -both CONF_JOIN XML features\n\n";
  string strSetVarOne   = "api uuid_setvar "   + objData.FreeswitchData.objChannelData.strChannelID + " conference_name " + objData.CallData.strFreeswitchConfName + "\n\n";
  intRC = FreeswitchCLIPort.SendLine(strSetVarOne.c_str());
  intRC = FreeswitchCLIPort.SendLine(strConfJoinOne.c_str());
 return intRC;
}

int Add_Conference_List(ExperientDataClass objData)
{
 ////cout << "add conference cli"<< endl;
 int    intRC;
 string strConfJoinOne = "api uuid_transfer " + objData.FreeswitchData.objLinkData.strChanneloneID + " -both CONF_JOIN XML features\n\n";
// string strConfJoinTwo = "api uuid_transfer " + objData.FreeswitchData.objLinkData.strChanneltwoID + " -both CONF_JOIN XML features\n\n";


 string strSetVarOne      = "api uuid_setvar " + objData.FreeswitchData.objLinkData.strChanneloneID + " conference_name " + objData.CallData.strFreeswitchConfName + "\n\n";
 string strSetVarTwo      = "api uuid_setvar " + objData.FreeswitchData.objLinkData.strChanneltwoID + " conference_name " + objData.CallData.strFreeswitchConfName + "\n\n";

  sleep(0.1);
 intRC = FreeswitchCLIPort.SendLine(strSetVarOne.c_str());
 intRC = FreeswitchCLIPort.SendLine(strSetVarTwo.c_str());
 intRC = FreeswitchCLIPort.SendLine(strConfJoinOne.c_str());
// //cout << "Add list -> " << strSetVarOne << endl;
// //cout << "Add list -> " << strSetVarTwo << endl;

 return intRC;
}

int Build_Conference_List(ExperientDataClass objData)
{
 ////cout << "build conference cli" << endl;
 int    intRC;
 string strConfBuildOne = "api uuid_transfer " + objData.FreeswitchData.objLinkData.strChanneloneID + " -both CONF_JOIN XML features\n\n";
// string strConfJoinTwo = "api uuid_transfer " + objData.FreeswitchData.objLinkData.strChanneltwoID + " -both CONF_JOIN XML features\n\n";
 string strSetVarOne      = "api uuid_setvar " + objData.FreeswitchData.objLinkData.strChanneloneID + " conference_name " + objData.CallData.strFreeswitchConfName + "\n\n";
 string strSetVarTwo      = "api uuid_setvar " + objData.FreeswitchData.objLinkData.strChanneltwoID + " conference_name " + objData.CallData.strFreeswitchConfName + "\n\n";

 intRC = FreeswitchCLIPort.SendLine(strSetVarOne.c_str());
 intRC = FreeswitchCLIPort.SendLine(strSetVarTwo.c_str());
 intRC = FreeswitchCLIPort.SendLine(strConfBuildOne.c_str());
// //cout << "build list -> " << strSetVarOne << endl;
 ////cout << "build list -> " << strSetVarTwo << endl;
 return intRC;
}

int FreeswitchRingBack(ExperientDataClass objData)
{
 string                   strVarList;
 string                   strConferenceName       = objData.CallData.strFreeswitchConfName;
 string                   strCallidName           = objData.FreeswitchData.objChannelData.strCustName;
 string                   strCallidNumber         = objData.FreeswitchData.objChannelData.strCallerID;
 string                   strUniqueId             = objData.CallData.stringUniqueCallID;
 string                   strPosition             = int2str(objData.FreeswitchData.objChannelData.iPositionNumber);
 string                   strRingBackCause        = objData.FreeswitchData.objChannelData.strHangupCause;
 string                   strUser; 
 string                   strOutput;
 int                      intRC;
 extern Telephone_Devices TelephoneEquipment;

// //cout << "cli.cpp FreeswitchRingBack may need to update this ..." << endl;
 // line view has no idea that a line is shared ..... right now that is....
 // need to get away from CallData.CTIsharedLine maybe ???

 if ((objData.CallData.CTIsharedLine < (int)TelephoneEquipment.NumberofLineViews)&&(objData.CallData.CTIsharedLine > 0)) {
  strUser = TelephoneEquipment.GUILineView[objData.CallData.CTIsharedLine].Name;
 }
 else {
  SendCodingError( "freeswitch_cli.cpp -> fn FreeswitchRingBack() invalid index"); 
  return -1;
 }
 strVarList  = "{api_hangup_hook=\'lua confhanguphook.lua\',session_in_hangup_hook=true,ignore_display_updates=false,fail_on_single_reject=ORIGINATOR_CANCEL,";
 strVarList  += "origination_caller_id_name='" + strCallidName +"'";
 strVarList  += ",origination_caller_id_number='" + strCallidNumber + "',";
 strVarList  += FREESWITCH_CONFERENCE_NAME_WO_COLON;
 strVarList  += "=" + strConferenceName + ",";
 strVarList  += FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON;
 strVarList  += "=" + strPosition + ",";
 strVarList  += FREESWITCH_CONFERENCE_RING_BACK_CAUSE_WO_COLON;
 strVarList  += "=" + strRingBackCause + ",";
 strVarList  += FREESWITCH_CONTROLLER_UNIQUE_ID_WO_COLON;
 strVarList  += "=" + strUniqueId + "}";

 strOutput = "bgapi expand originate " + strVarList;
 strOutput += "user/" + strUser + "@${domain_name}";
 strOutput += " &conference(" + strConferenceName +"@sla)\n\n";

 intRC = FreeswitchCLIPort.SendLine(strOutput.c_str());
 return intRC;
}

string FreeswitchReferStringSLA(ExperientDataClass objData) {



return "bgapi ";
}

string FreeswitchOriginateStringSLA(ExperientDataClass objData)
{
 string                   strVarList;
 string                   strLastGateway          = "none";
 string                   strConferenceName       = objData.CallData.strFreeswitchConfName;
 string                   strTransferNumber       = objData.CallData.TransferData.strTransfertoNumber;
 string                   strTransferFrom         = objData.CallData.TransferData.strTransferFromPosition;
 string                   strUniqueId             = objData.CallData.stringUniqueCallID;
 string                   strOutput;
 int                      index;
 size_t                   sz;
 bool                     boolshowdelimeter       = false;

 extern vector <string>   vGATEWAY_TRANSFER_ORDER;
 extern Telephone_Devices TelephoneEquipment;

 sz = vGATEWAY_TRANSFER_ORDER.size();
 if (!sz)                              {return "";}

 strLastGateway = vGATEWAY_TRANSFER_ORDER[sz-1];
// //cout << "FreeswitchOriginateStringSLA" << endl;
 index = TelephoneEquipment.fIndexWithDeviceName(strLastGateway);
 if (index >=0) {strLastGateway = TelephoneEquipment.Devices[index].IPaddress.stringAddress;}

// strVarList  = "{api_hangup_hook=\'lua confhanguphook.lua\',session_in_hangup_hook=true,continue_on_fail=^^:1:2:3:6:21:25:31:38:41:42:43:44:45:47:50:52:57:58:63:65:66:69:79:81:88:102:127,";
 strVarList  = "{api_hangup_hook=\'lua confhanguphook.lua\',session_in_hangup_hook=true,fail_on_single_reject=ORIGINATOR_CANCEL,";
 strVarList  += "origination_caller_id_name='" + strCALLER_ID_NAME +"'";
 strVarList  += ",origination_caller_id_number='" + strCALLER_ID_NUMBER+ "',";
 strVarList  += FREESWITCH_CLI_VARIABLE_TRANSFER_METHOD_WO_COLON;
 switch(objData.CallData.TransferData.eTransferMethod)
  {
   case mBLIND_TRANSFER: strVarList  += "=BLIND_TRANSFER,"; break;
   case mGUI_TRANSFER:   strVarList  += "=GUI_TRANSFER,"  ; break;
   default:              strVarList  += "=NOT_DEFINED,"   ; break;
  }
 strVarList  += FREESWITCH_CONFERENCE_NAME_WO_COLON;
 strVarList  += "=" + strConferenceName + ",";
 strVarList  += FREESWITCH_SIPX_CONFERENCE_NAME_WO_COLON;
 strVarList  += "=" + strConferenceName + ",";
 strVarList  += FREESWITCH_LAST_GATEWAY_IP_WO_COLON;
 strVarList  += "=" + strLastGateway + ",";
 strVarList  += FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON;
 strVarList  += "=" + strTransferFrom + ",";
 strVarList  += FREESWITCH_CONTROLLER_UNIQUE_ID_WO_COLON;
 strVarList  += "=" + strUniqueId + "}";

 strOutput = "bgapi expand originate " + strVarList;
 
 for (unsigned int i = 0; i < sz; i++)
  {
   if (TelephoneEquipment.fcanDialthisNumber(vGATEWAY_TRANSFER_ORDER[i], strTransferNumber))
    {
     if (boolshowdelimeter) {strOutput += "|";}
     strOutput += "sofia/internal/" + strTransferNumber + "@${" + vGATEWAY_TRANSFER_ORDER[i] +"}";
     boolshowdelimeter = true;
    }
  }
 
 if (!boolshowdelimeter) {SendCodingError("freeswitch_cli.cpp - Coding Error in FreeswitchOriginateStringSLA(): cannot dial number!");}
 
// //cout << "FreeswitchOriginateStringSLA" << endl;
// //cout << objData.FreeswitchData.objChannelData.strChannelID << endl;

 strOutput += " &conference(" + strConferenceName +"@sla)\n\n";
 
 //strOutput += " &sofia_sla(" + objData.FreeswitchData.objChannelData.strChannelID + " inline) \n\n";
  ////cout << "FCLI orig str -> " << strOutput << endl;

 return strOutput;
}

int PullFromParkingLot(ExperientDataClass objData)
{
 int intRC;

 intRC =0;



 return intRC;
}

int SetVariable(ExperientDataClass objData)
{
 int               intRC;
 string            strUUID = objData.FreeswitchData.objRing_Dial_Data.strChannelID;
 string            strSetVar;

////cout << "Set Variable XXX to YYY" << endl;

 strSetVar    = "api uuid_setvar " + strUUID + " " + "FRED" + "\n\n";

 intRC = FreeswitchCLIPort.SendLine(strSetVar.c_str());
 return intRC;
}





int InternalNG911Eavesdrop(ExperientDataClass objData)
{
 string                   strVarList;
 string                   strConferenceName       = objData.CallData.strFreeswitchConfName;
 string                   strCallidName           = objData.FreeswitchData.objChannelData.strCustName;
 string                   strCallidNumber         = objData.FreeswitchData.objChannelData.strCallerID;
 string                   strTransferNumber       = objData.CallData.TransferData.strNG911PSAP_TransferNumber;
 string                   strTransferFrom         = objData.CallData.TransferData.strTransferFromPosition;
 string                   strUniqueId             = objData.CallData.stringUniqueCallID;
 string                   strOutput;
 string                   strData;
 int                      intRC;
 Channel_Data             objDestChannelData;
 bool                     boolInternalCall;
 sip_ng911_transfer_type  eTransferType;
 string                   strXMLtoSend = "";
 bool                     boolXMLisAvailable = true;
 size_t                   found;

 extern Initialize_Global_Variables INIT_VARS;

// //cout << "InternalNG911eavesdrop" << endl;
 eTransferType = INIT_VARS.fNG911_TransferType(strTransferNumber);
 
 if (objData.FreeswitchData.vectDestChannelsOnCall.size())
  {
   objDestChannelData = objData.FreeswitchData.vectDestChannelsOnCall[0];
   strCallidName           = objDestChannelData.strCustName;
   strCallidNumber         = objDestChannelData.strCallerID;
  }


 if (!objData.FreeswitchData.fLoad_EavesDrop_into_Channelobject())         {return -4;}

// //cout << "fn Interrnalng911eavedrop -> " << objData.FreeswitchData.objChannelData.strChannelID << endl;
////cout << "URI ->" << objData.ALIData.I3Data.objLocationURI.strURI << endl;



 strVarList  = "{api_hangup_hook=\'lua eavesdrophanguphook.lua\',session_in_hangup_hook=true,ignore_display_updates=false,fail_on_single_reject=ORIGINATOR_CANCEL,originate_continue_on_timeout=false,hangup_after_bridge=true,ringback=$${us-ring},";
 strVarList  += "origination_caller_id_name='" + strCallidName +"'";
 strVarList  += ",origination_caller_id_number='" + strCallidNumber+ "',";
 strVarList  += ",absolute_codec_string='PCMU,PCMA',";
 strVarList  += FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON;
 strVarList  += "=" + strTransferFrom + ",";
 strVarList  += FREESWITCH_CLI_TRANSFER_TO_WO_COLON;
 strVarList  += "=" + strTransferNumber + ",";
 strVarList  += FREESWITCH_CLI_EAVESDROP_TARGET_UUID_WO_COLON;
 strVarList  += "=" + objData.FreeswitchData.objChannelData.strChannelID + ",";
 strVarList  += "sip_h_X-PANI=";
 strVarList  += objData.CallData.stringPANI + ","; 
 strVarList  += "sip_h_X-TRANSFER-PSAP=";
 strVarList  +=  strTransferNumber + ",";
 strVarList  += "sip_h_X-Trunk=1";
 strVarList  +=  objData.CallData.stringTrunk + ",";
 strVarList  += "sip_h_X-";
 strVarList  += FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON;
 strVarList  += "=" + objData.CallData.TransferData.strTransferTargetUUID + ",";


// Add NG911 Headers ......
if (objData.ALIData.I3Data.objLocationURI.strURI.length()) {
  strVarList += "sip_h_Geolocation='<" + objData.ALIData.I3Data.objLocationURI.strURI +">',";  
}
else{
 cout << "fn InternalNG911Eavesdrop() NO I3 LOCATION by reference" << endl;
}

if ((objData.strNenaCallId.length())&&(objData.strNenaIncidentId.length())) {
 
  strData = "'<urn:nena:uid:callid:" + objData.strNenaCallId;
  strData += ">;purpose=nena-CallId'\\,'<urn:nena:uid:incidentid:";
  strData += objData.strNenaIncidentId;
  strData += ">;purpose=nena-IncidentId'";
  
 // strData ="'<urn:nena:uid:callid:ABCDEFGHIJKLMNOPQRSTUVWXYZ>;purpose=nena-CallId'"; 

  strVarList += "sip_h_Call-Info=" + strData +",";

}
else{
 cout << "fn InternalNG911Eavesdrop() NO I3 CALLINFO HEADER" << endl;
}


////cout << "PIDFLO -> " << endl << objData.ALIData.I3Data.I3XMLData.XMLstring << endl;
////cout << "I3     -> " << endl << objData.ALIData.I3Data.strPidfXMLData << endl;
////cout << "Legacy -> " << endl << ASCII_String(objData.ALIData.stringAliText.c_str(),objData.ALIData.stringAliText.length()) << endl;

if      (objData.ALIData.I3Data.I3XMLData.XMLstring.length()) {strXMLtoSend = objData.ALIData.I3Data.I3XMLData.XMLstring;}
else if (objData.ALIData.I3Data.strPidfXMLData.length())      {strXMLtoSend = objData.ALIData.I3Data.strPidfXMLData;}
else                                                          {boolXMLisAvailable = false;}

if      (objData.ALIData.stringAliText.length())       
 {
  strVarList += "sip_h_X-Legacy-ALI-b64="; 
  strVarList += EncodeBase64(objData.ALIData.stringAliText);
  strVarList += ",";
 }


if (!boolXMLisAvailable) { cout << "NO XML CLI" << endl;}
 switch (eTransferType) 
  {
   case NO_TRANSFER_DEFINED:
         SendCodingError("freeswitchcli.cpp.cpp - CODING ERROR in InternalNG911Eavesdrop() -> NO_TRANSFER_DEFINED" ); return -1;
   case SIP_URI_INTERNAL:
         strVarList  += "sip_h_X-TransferType=internal,";
         boolInternalCall = true;
         break;

   case SIP_URI_EXTERNAL:
         strVarList += "sip_h_X-TransferType=external,";
         if(boolXMLisAvailable)
          {
           // &#44;  XML escape sequence for commas replace , with &#44; FSW uses , as a delimeter
           strVarList +=  "sip_multipart=application/pidf+xml:\'";
           strVarList += FindandReplaceALL(strXMLtoSend, "," , "&#44;");
           strVarList += "\',";
          }
         boolInternalCall = false;
         break;

   case SIP_URI_EXTERNAL_EXPERIENT:
         strVarList  += "sip_h_X-TransferType=external.experient,";
         if(boolXMLisAvailable)
          { 
           // &#44;  XML escape sequence for commas replace , with &#44; FSW uses , as a delimeter          
           strVarList +=  "sip_multipart=application/pidf+xml:\'";         
           strVarList += FindandReplaceALL(strXMLtoSend, "," , "&#44;");
           strVarList += "\',";
          }
         boolInternalCall = false;
         break;
   
  }


 strVarList  += FREESWITCH_CONTROLLER_UNIQUE_ID_WO_COLON;
 strVarList  += "=" + strUniqueId + "}";

 
 strOutput = "bgapi expand originate " + strVarList; 

 //both to internal v2.7.55
 if (boolInternalCall) {  strOutput += "sofia/internal/" + strTransferNumber;}
 else                  {  strOutput += "sofia/internal/" + strTransferNumber;}
 
// need to look at shared versus no shared .....    

 strOutput += ";transport=tcp &conference(" + strConferenceName +"@sla)\n\n";
 
 //strOutput += " &sofia_sla(" + objData.FreeswitchData.objChannelData.strChannelID + " inline) \n\n";
 intRC = FreeswitchCLIPort.SendLine(strOutput.c_str());
// //cout << strOutput << endl;
 return intRC;
}

int EavesDrop(ExperientDataClass objData)
{
 string                   strVarList;
 string                   strLastGateway          = "none";
 string                   strConferenceName       = objData.CallData.strFreeswitchConfName;
 string                   strCallidName           = objData.FreeswitchData.objChannelData.strCustName;
 string                   strCallidNumber         = objData.FreeswitchData.objChannelData.strCallerID;
 string                   strTransferNumber       = objData.CallData.TransferData.strTransfertoNumber;
 string                   strTransferFrom         = objData.CallData.TransferData.strTransferFromPosition;
 string                   strUniqueId             = objData.CallData.stringUniqueCallID;
 string                   strOutput;
 int                      intRC;
 int                      index;
 size_t                   sz;
 bool                     boolshowdelimeter       = false;

// //cout << "EAVESDROP" << endl;
// Normal Conference transfers here .....
//
 extern vector <string>   vGATEWAY_TRANSFER_ORDER;
 extern Telephone_Devices TelephoneEquipment;

 if (IsSIP_URI(objData.CallData.TransferData.strNG911PSAP_TransferNumber)) {return InternalNG911Eavesdrop(objData);}
 if (!objData.FreeswitchData.fLoad_EavesDrop_into_Channelobject())         {return -2;}


 sz = vGATEWAY_TRANSFER_ORDER.size();
 if (!sz)                              {return -3;}

 strLastGateway = vGATEWAY_TRANSFER_ORDER[sz-1];

 index = TelephoneEquipment.fIndexWithDeviceName(strLastGateway);
 if (index >=0) {strLastGateway = TelephoneEquipment.Devices[index].IPaddress.stringAddress;}

 strVarList  = "{api_hangup_hook=\'lua eavesdrophanguphook.lua\',session_in_hangup_hook=true,ignore_display_updates=false,fail_on_single_reject=ORIGINATOR_CANCEL,originate_continue_on_timeout=false,hangup_after_bridge=true,ringback=$${us-ring},";
 strVarList  += "origination_caller_id_name='" + strCALLER_ID_NAME +"'";
 strVarList  += ",origination_caller_id_number='" + strCALLER_ID_NUMBER+ "',";
 strVarList  += FREESWITCH_LAST_GATEWAY_IP_WO_COLON;
 strVarList  += "=" + strLastGateway + ",";
 strVarList  += FREESWITCH_CLI_TRANSFER_FROM_POSITION_WO_COLON;
 strVarList  += "=" + strTransferFrom + ",";
 strVarList  += FREESWITCH_CLI_TRANSFER_TO_WO_COLON;
 strVarList  += "=" + strTransferNumber + ",";
 strVarList  += FREESWITCH_CLI_EAVESDROP_TARGET_UUID_WO_COLON;
 strVarList  += "=" + objData.FreeswitchData.objChannelData.strChannelID + ",";
 strVarList  += "sip_h_X-";
 strVarList  += FREESWITCH_CLI_TRANSFER_TARGET_UUID_WO_COLON;
 strVarList  += "=" + objData.CallData.TransferData.strTransferTargetUUID + ",";
 strVarList  += "sip_h_X-PANI=";
 strVarList  += objData.CallData.stringPANI + ",";
// strVarList  += "sip_h_X-";
// strVarList  += FREESWITCH_VARIABLE_CONTROLLER_UNIQUE_ID_WO_COLON;
// strVarList  += "=" + objData.CallData.TransferData.strTransferTargetUUID + ",";


 strVarList  += FREESWITCH_CONTROLLER_UNIQUE_ID_WO_COLON;
 strVarList  += "=" + strUniqueId + "}";


 strOutput = "bgapi expand originate " + strVarList;

 // this is where to put the can the gateway dial this number check .... also
 for (unsigned int i = 0; i < sz; i++)
  {
   if (TelephoneEquipment.fcanDialthisNumber(vGATEWAY_TRANSFER_ORDER[i], strTransferNumber))
    {
     if (boolshowdelimeter) {strOutput += "|";}
     strOutput += "sofia/internal/" + strTransferNumber + "@${" + vGATEWAY_TRANSFER_ORDER[i] +"}";
     boolshowdelimeter = true;
    }
  }

////cout << objData.FreeswitchData.objChannelData.strChannelID << endl;
// strOutput += " &sofia_sla(" + objData.FreeswitchData.objChannelData.strChannelID + " inline) \n\n";
 strOutput += " &conference(" + strConferenceName +"@sla)\n\n";
 ////cout << strOutput << endl;
 intRC = FreeswitchCLIPort.SendLine(strOutput.c_str());

 return intRC;
}

int ThreeWay(ExperientDataClass objData)
{
 string            strSendMsg   = "SendMsg ";
 string            strCommand   = "call-command: execute";
 string            strAppName   = "execute-app-name: sofia_sla";
 string            strAppArg    = "execute-app-arg: ";
 int               intRC;
 string            strThreeway;
 string            strSetVarOne;
 string            strSetVarTwo;


 ////cout << "threeway" << endl;

 strSetVarOne   = "api uuid_setvar " + objData.CallData.TransferData.strTransferFromUniqueid + " threeway_target_uuid " + objData.FreeswitchData.objChannelData.strChannelID + "\n\n";
 strSetVarTwo   = "api uuid_setvar " + objData.CallData.TransferData.strTransferFromUniqueid + " cust_name " + objData.FreeswitchData.objChannelData.strCustName + "\n\n";
 
 strThreeway = "api uuid_transfer " + objData.CallData.TransferData.strTransferFromUniqueid ;
 strThreeway += " ";
 strThreeway +=  objData.FreeswitchData.objChannelData.strCallerID;
 strThreeway += " XML THREEWAY_CONTEXT\n\n";


 intRC = FreeswitchCLIPort.SendLine(strSetVarOne.c_str());
 intRC = FreeswitchCLIPort.SendLine(strSetVarTwo.c_str());
 intRC = FreeswitchCLIPort.SendLine(strThreeway.c_str());
 return intRC;
}

int Send_to_Conference(ExperientDataClass objData)
{
 int               intRC;
 string            strConferenceName = objData.CallData.strFreeswitchConfName;
 string            strSetVarOne;
 string            strSetVarTwo;
 string            strCommand;

////cout << "send to conference cli" << endl;

 strSetVarOne   = "api uuid_setvar " + objData.CallData.TransferData.strTransferFromUniqueid + " conference_name " + strConferenceName + "\n\n";
 strSetVarTwo   = "api uuid_setvar " + objData.CallData.TransferData.strTransferFromUniqueid + " real_ip_address " + objData.CallData.TransferData.IPaddress.stringAddress +"\n\n"; 

 strCommand = "api uuid_transfer " + objData.CallData.TransferData.strTransferFromUniqueid ;
 strCommand += " ";
 strCommand += " CONF_JOIN XML features\n\n";


 intRC = FreeswitchCLIPort.SendLine(strSetVarOne.c_str());
 intRC = FreeswitchCLIPort.SendLine(strSetVarTwo.c_str());
 intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());
 //cout << "send new line to conference .... " << endl;
 return intRC;
}

int  Send_RFAI_Info_Message(ExperientDataClass objData)
{
 string 	strCommand;
 string 	strData;
 int    	intRC;
 string 	strUUID;
 string		strTransferNumber = objData.CallData.TransferData.strTransfertoNumber;

if      (objData.FreeswitchData.vectDestChannelsOnCall.size()) {strUUID = objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID;}
else if (objData.FreeswitchData.vectPostionsOnCall.size())     {strUUID = objData.FreeswitchData.vectPostionsOnCall[0].strChannelID;}
else                                                           {return -1;}

switch (objData.enumANIFunction) {

 case RFAI_BRIDGE: 
   strData =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><bridge-request xmlns=\"http://rfai.intrado.com/ns/rfai\"><create-bridge uri=\"tel:";
   strData += strTransferNumber;
   strData += "\"/></bridge-request>\n\n";
   break;
 case RFAI_DROP_LEG:
   strData ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><bridge-request xmlns=\"http://rfai.intrado.com/ns/rfai\"><modify-bridge action=\"drop-last-leg\"/></bridge-request>\n\n";
   break;
 default:
   return -1;  
}

strCommand = "bgapi uuid_send_info " + strUUID + " application x-rfai-bridge-request+xml " + strData;
intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());

////cout << "send info to " << strUUID << endl << strData;

 return intRC;
}



int  SendTextMessage(ExperientDataClass objData)
{
 string strCommand;
 int    intRC;
 string strUUID;
 string strToSend;

// strCommand =  "api chat sip|911@" + objData.TDDdata.SMSdata.strToHost + "|internal/" + objData.TDDdata.SMSdata.strFromUser + "@" + objData.TDDdata.SMSdata.strFromHost +"|" + objData.TDDdata.SMSdata.strSMSmessage + "\n\n";
if      (objData.FreeswitchData.vectDestChannelsOnCall.size()) {strUUID = objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID;}
else if (objData.FreeswitchData.vectPostionsOnCall.size())     {strUUID = objData.FreeswitchData.vectPostionsOnCall[0].strChannelID;}
else                                                           {return -1;}

strToSend = FindandReplaceALL(objData.TextData.SMSdata.strSMSmessage, "\\" , "\\\\");
strToSend = FindandReplaceALL(objData.TextData.SMSdata.strSMSmessage, "\'" , "\\\'");

strCommand = "api uuid_msrp_send " + strUUID + " \'" + strToSend + "\'\n";
intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());

// //cout << strCommand << endl;
 return intRC;
}

int SendDeflect(ExperientDataClass objData)
{
 string strUUID;
 string strDestination;
 string strCommand;
 int    intRC;

 strUUID        = objData.FreeswitchData.objChannelData.strChannelID;
 strDestination = objData.FreeswitchData.objChannelData.strTranData; 

// strCommand   = "api uuid_setvar " + strUUID + " Controller_Blind_Xfer true \n\n";
// intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());

 strCommand =  "api uuid_transfer " + strUUID;
 strCommand += " ";
 strCommand += strDestination;
 strCommand += " XML Controller_Blind_Transfer_Context";
 strCommand += "\n\n";

////cout << strCommand << endl;

intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());

return intRC;
}

int SendSyncTime() {
 int     intRC;
 string strCommand = "bgapi fsctl sync_clock_when_idle\n\n";
 
 intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());

 return intRC;
}


// uuid_transfer uuidvar -both park inline will split a bridged call and we can then operate on all of the uuid's .... (FSW bug in 1.6.7 fixed in 1.6.18)


int SendFreeswitchMemUsage() {
 int                intRC;
 string             strCommand;

 strCommand = "bgapi luarun mem.usage.lua\n\n";  
 intRC      = FreeswitchCLIPort.SendLine(strCommand.c_str());
  
 return intRC;   
}



int SendConferenceInvite(ExperientDataClass objData)
{
 string            strDigit;

 string            strSendDTMFprefix; 
 string            strSendDTMF;
 string            strOriginate;
 string            strCommand;

 unsigned long int Delay_btwn_Digit_usec = (intDELAY_BETWEEN_DTMF_MS * 1000);
 unsigned long int Delay_after_Flash_usec = (intDELAY_AFTER_FLASH_HOOK_MS * 1000);
 int               intRC;
 bool              boolFlashSent          = false;
 bool              boolTandem             = false;
 bool              boolFlash              = false;
 bool              boolINdigital          = false;
 size_t            found;
 unsigned int      iStart;
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;

////cout << "send conference invite" << endl;
//strLuaDialOut     = "bgapi lua conference_dial_out.lua " + objData.CallData.TransferData.strTransfertoNumber+ " " + objData.CallData.strFreeswitchConfName + " " + "switchvox" + "\n\n";
//  strluaDialOut   = "bgapi expand originate sofia/internal/" + objData.CallData.TransferData.strTransfertoNumber + "@${switchvox} \"&lua(conference_dial_out.lua " + objData.CallData.strFreeswitchConfName  + ")\"\n\n";

 if(!FreeswitchCLIPort.boolReadyToSend){return 1;}

 switch(objData.CallData.TransferData.eTransferMethod)
  {
   case mTANDEM: case mFLASHOOK:
   //     //cout << "tandem transfer" << endl;
        if (!objData.FreeswitchData.vectDestChannelsOnCall.size()) {return 1;}
        strSendDTMFprefix	= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID + " ";
        boolTandem 		= (objData.CallData.TransferData.eTransferMethod == mTANDEM);
        boolFlash  		= (objData.CallData.TransferData.eTransferMethod == mFLASHOOK);

        for (unsigned int i = 0; i < objData.CallData.TransferData.strTransfertoNumber.length(); i++)
         {
           strDigit = objData.CallData.TransferData.strTransfertoNumber[i]; 
    //       if(strDigit[0] == '*') {continue;}

           //hook Flash
          if (!boolFlashSent)
           {
            for (unsigned int x = 0; x < objData.FreeswitchData.vectDestChannelsOnCall.size(); x++)
             {
              //if(!TelephoneEquipment.fIsAnAudiocodes(objData.FreeswitchData.vectDestChannelsOnCall[x].IPaddress)) {continue;}
              
              if((!TRUNK_TYPE_MAP.fIsTandem(objData.FreeswitchData.vectDestChannelsOnCall[x].iTrunkNumber))&&(boolTandem))     {continue;}
              if((!TRUNK_TYPE_MAP.fIsCLID(objData.FreeswitchData.vectDestChannelsOnCall[x].iTrunkNumber))&&(boolFlash))        {continue;}
              strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[x].strChannelID + " ";
              strSendDTMF = strSendDTMFprefix + DTMF_FLASH_DIGIT +"\n\n";
      //        //cout << "send flash" << endl;
              intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());
             }
            boolFlashSent = true;
              msleep(intDELAY_AFTER_FLASH_HOOK_MS);
       //     usleep(Delay_after_Flash_usec);
           }


          for (unsigned int x = 0; x < objData.FreeswitchData.vectDestChannelsOnCall.size(); x++)
           {
            strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[x].strChannelID + " ";
            strSendDTMF = strSendDTMFprefix + strDigit + "\n\n";
            intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());
           }
          
          for (unsigned int x = 0; x < objData.FreeswitchData.vectPostionsOnCall.size(); x++)
           {
            strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectPostionsOnCall[x].strChannelID + " ";
            strSendDTMF = strSendDTMFprefix + strDigit + "\n\n";
            intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());
           }         

          msleep(intDELAY_BETWEEN_DTMF_MS);
     //     usleep(Delay_btwn_Digit_usec);
         }
        return 0;

   case mINDGITAL:
        if (!objData.FreeswitchData.vectDestChannelsOnCall.size()) {return 1;}
        strSendDTMFprefix	= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID + " ";
        found = objData.CallData.TransferData.strTransfertoNumber.find("*8#");
        if (found == 0) {iStart = 3;}
        else            {iStart = 0;}

        for (unsigned int i = iStart; i < objData.CallData.TransferData.strTransfertoNumber.length(); i++)
         {
           strDigit = objData.CallData.TransferData.strTransfertoNumber[i]; 
    //       if(strDigit[0] == '*') {continue;}

           //*8# 
          if (!boolFlashSent)
           {
            for (unsigned int x = 0; x < objData.FreeswitchData.vectDestChannelsOnCall.size(); x++)
             {
              //if(!TelephoneEquipment.fIsAnAudiocodes(objData.FreeswitchData.vectDestChannelsOnCall[x].IPaddress)) {continue;}
              
              if (!TRUNK_TYPE_MAP.fIsINDIGITAL(objData.FreeswitchData.vectDestChannelsOnCall[x].iTrunkNumber))     {continue;}

              strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[x].strChannelID + " ";
              strSendDTMF = strSendDTMFprefix + "*\n\n";
              intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str()); //send Star
              msleep(intDelay_BETWEEN_DTMF_INDIGITAL_MS);
              strSendDTMF = strSendDTMFprefix + "8\n\n";
              intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str()); //send Eight
              msleep(intDelay_BETWEEN_DTMF_INDIGITAL_MS);
              strSendDTMF = strSendDTMFprefix + "#\n\n";
              intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str()); //send Pound
             }
            boolFlashSent = true;
            msleep(intDelay_AFTER_STAR_EIGHT_PD_MS);           
           }


          for (unsigned int x = 0; x < objData.FreeswitchData.vectDestChannelsOnCall.size(); x++)
           {
            strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[x].strChannelID + " ";
            strSendDTMF = strSendDTMFprefix + strDigit + "\n\n";
            intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());
           }
          
          for (unsigned int x = 0; x < objData.FreeswitchData.vectPostionsOnCall.size(); x++)
           {
            strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectPostionsOnCall[x].strChannelID + " ";
            strSendDTMF = strSendDTMFprefix + strDigit + "\n\n";
            intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());
           }         

          msleep(intDelay_BETWEEN_DTMF_INDIGITAL_MS);
         }

        return 0;

   case mREFER:
          cout << "send RFC 4579 REFER" << endl;

          
 //       strOriginate = FreeswitchReferStringSLA(objData);
          //park
/*
          for (unsigned int x = 0; x < objData.FreeswitchData.vectDestChannelsOnCall.size(); x++)            {
            strOriginate= "bgapi uuid_park " + objData.FreeswitchData.vectDestChannelsOnCall[x].strChannelID;
            strOriginate+="\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());
            
            //cout << strOriginate << endl;
            strOriginate= "bgapi uuid_setvar " + objData.FreeswitchData.vectDestChannelsOnCall[x].strChannelID; 
            strOriginate+= "sip_refer_continue_after_reply True\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());
          }


          for (unsigned int x = 0; x < objData.FreeswitchData.vectPostionsOnCall.size(); x++)            {
            strOriginate= "bgapi uuid_park " + objData.FreeswitchData.vectPostionsOnCall[x].strChannelID;
            strOriginate+="\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());

            //cout << strOriginate << endl;
            strOriginate= "bgapi uuid_setvar " + objData.FreeswitchData.vectPostionsOnCall[x].strChannelID;
            strOriginate+= "sip_refer_continue_after_reply True\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());

          } 
*/
/*
            strOriginate= "bgapi uuid_transfer " + objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID;
            strOriginate= " -both park inline";
            strOriginate+="\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());
*/
            sleep(2);
// then refer 
      //   for (unsigned int x = 0; x < objData.FreeswitchData.vectDestChannelsOnCall.size(); x++)            {
           if (!objData.FreeswitchData.vectDestChannelsOnCall.empty())  {
            strOriginate= "bgapi uuid_deflect " + objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID;
            strOriginate+= " " + objData.CallData.TransferData.strNG911PSAP_TransferNumber;
            strOriginate+="\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());
            strCommand = "bgapi log ";
            strCommand += "INFO " + strOriginate +"\n\n";
            intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());
           }
           else {
            SendCodingError("freeswitch_cli.cpp - Empty Dest Vector in fn SendConferenceInvite()");
           }

          for (unsigned int x = 0; x < objData.FreeswitchData.vectPostionsOnCall.size(); x++)            {
            strOriginate= "bgapi uuid_deflect " + objData.FreeswitchData.vectPostionsOnCall[x].strChannelID;
            strOriginate+= " <sip:" + objData.CallData.ReferData.strContactURI;
            strOriginate+= ";" + objData.CallData.ReferData.strContactParams;
            strOriginate+=">\n\n";
            intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());
            strCommand = "bgapi log ";
            strCommand += "INFO " + strOriginate +"\n\n";
            intRC = FreeswitchCLIPort.SendLine(strCommand.c_str());
          }        




        break;

   default:
      //  //cout << "default FreeswitchOriginateStringSLA" << endl;
        strOriginate = FreeswitchOriginateStringSLA(objData);
        intRC = FreeswitchCLIPort.SendLine(strOriginate.c_str());
        break;
  }

 return 0;
}

int HangupChannel(ExperientDataClass objData)
{
 string            strSendMsg = "SendMsg ";
 string            strHangup  = "call-command: hangup";
 string            strReason  = "hangup-cause: NORMAL_CLEARING\n\n";
 int               intRC;


 strSendMsg = "bgapi uuid_transfer ";
 strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID;
 strSendMsg+= " HANG_UP XML features\n\n";      
 intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());
 return intRC;
}

int Send_to_Hangup(ExperientDataClass objData)
{
 string strSendMsg;
 int    intRC;

 strSendMsg = "bgapi uuid_transfer ";
 strSendMsg+= objData.CallData.TransferData.strTransferFromUniqueid;
 strSendMsg+= " HANG_UP XML features\n\n";      
 intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());

 return intRC;
}


int SendFlashHook(ExperientDataClass objData)
{
 int               index;
 int               intRC;
 bool              boolTandem = false;
 bool              boolCLID   = false;
 string            strSendDTMFprefix;
 string            strSendDTMF;
 string            strTransferUUID;
 string            strDigit  = DTMF_FLASH_DIGIT;
 string            strSendMsg = "SendMsg ";

 index = objData.FreeswitchData.fFindDestChannel911Caller();
 if (index >= 0) {boolTandem = true;}
 else
  {
   index = objData.FreeswitchData.fFindFirstDestChannelCLIDCaller();
   if (index >= 0) {boolCLID = true;}
  }
 
 if ((!boolTandem)&&(!boolCLID)) {return 0;}

 strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[index].strChannelID + " ";
 strSendDTMF = strSendDTMFprefix + strDigit + "\n\n";
 intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());

 return 1;
}


int CancelTransfer(ExperientDataClass objData)
{
 int               intRC;
 string            strSendDTMFprefix;
 string            strSendDTMF;
 string            strTransferUUID;
 string            strDigit  = DTMF_FLASH_DIGIT;
 string            strSendMsg = "SendMsg ";
 string            strHangup  = "call-command: hangup";
 string            strBreak   = "call-command: break\n\n";
 string            strReason  = "hangup-cause: ORIGINATOR_CANCEL\n\n";
 int               index;

 int               HangupSent         = 0;
////cout << "freeswitch_cli.cpp cancel transfer" << endl;
   switch(objData.CallData.TransferData.eTransferMethod)
    {
     case mTANDEM:
          // send flashhook to "stop transfer"
          // don't think this ever get executed .....
          index = objData.FreeswitchData.fFindDestChannel911Caller();
          if (index < 0) {return 0;}

          strSendDTMFprefix= "bgapi uuid_send_dtmf " + objData.FreeswitchData.vectDestChannelsOnCall[index].strChannelID + " ";
          strSendDTMF = strSendDTMFprefix + strDigit + "\n\n";
          intRC = FreeswitchCLIPort.SendLine(strSendDTMF.c_str());
          HangupSent++; 
          break;

     case mATTENDED_TRANSFER:
          // Hangup "transfer to" channel
          strSendMsg = "bgapi uuid_transfer ";
          strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID;
          strSendMsg+= " CANCEL_TRANSFER XML bridged_att_transfer_context\n\n";      
          intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());

          HangupSent++;
          break;
          break;

     default:
          // Hangup "transfer to" channel
 //         strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID;       
 //         intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());
 //         intRC = FreeswitchCLIPort.SendLine(strHangup.c_str());
 //         intRC = FreeswitchCLIPort.SendLine(strReason.c_str());

          strSendMsg ="bgapi log info 911 Controller sending uuid_kill to " + objData.FreeswitchData.objChannelData.strChannelName +"\n\n";
         // //cout << "sending kill to UUID -> " << objData.FreeswitchData.objChannelData.strChannelID << endl;
          intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());         

          strSendMsg ="bgapi uuid_kill " + objData.FreeswitchData.objChannelData.strChannelID + " ORIGINATOR_CANCEL\n\n";
          intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());



          HangupSent++;
          break;





    }// end switch(intTransferType)

 //  TransferNumberPosition = strTransferChannelList.find(strTransferNumber, TransferNumberPosition+1); 
//  }// end while (TransferNumberPosition != string::npos)
 return HangupSent;

}



int Kill_Position_Channel(ExperientDataClass objData)
{
 int               index, intRC;
 string            strSendMsg = "SendMsg ";
 string            strHangup  = "call-command: hangup";
 string            strReason  = "hangup-cause: NORMAL_CLEARING\n\n";
 string            strLogMsg;

 index = objData.FreeswitchData.fFindPositionInPosnVector(objData.CallData.intPosn);

 if (index < 0) {return 0;}

  experient_nanosleep(THREE_TENTHS_SEC_IN_NSEC);
  experient_nanosleep(THREE_TENTHS_SEC_IN_NSEC);

  strLogMsg ="bgapi log info 911 Controller sending call-command: hangup to " + objData.FreeswitchData.vectPostionsOnCall[index].strChannelName +"\n\n";
  intRC = FreeswitchCLIPort.SendLine(strLogMsg.c_str()); 

  strSendMsg+= objData.FreeswitchData.vectPostionsOnCall[index].strChannelID;       
  intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());
  intRC = FreeswitchCLIPort.SendLine(strHangup.c_str());
  intRC = FreeswitchCLIPort.SendLine(strReason.c_str());

  if (intRC) {return 0;}

 return 1;
}




int GetVariable(string strChannel, string stVariable )
{
 int               intRC;
 string            strACTIONGETVAR    = "Action: GetVar";
 string            strCHANNEL         = "Channel: " + strChannel;
 string            strVARIABLE        = "Variable: "+ stVariable;
 string            strActionId        = "ActionId: 600-\r\n";

 intRC = FreeswitchCLIPort.SendLine((char *) strACTIONGETVAR.c_str());

 intRC = FreeswitchCLIPort.SendLine((char *) strCHANNEL.c_str());

 intRC = FreeswitchCLIPort.SendLine((char *) strVARIABLE.c_str());

 intRC = FreeswitchCLIPort.SendLine((char *) strActionId.c_str());

 return 0;
}


void SendDelayedHangup(string strChannel, int iDelay)
{
 struct sigevent	        sigeventAMIsignalEvent;
 int                            intRC;
 param_c*                       p;

 p = new param_c; 

 p->chardata  = strChannel.c_str();
 p->intlength = strChannel.length();

 sigeventAMIsignalEvent.sigev_notify_function                   = Delayed_CLI_HANGUP; 
 sigeventAMIsignalEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventAMIsignalEvent.sigev_notify_attributes                 = NULL;
 sigeventAMIsignalEvent.sigev_value.sival_ptr 		        = p;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventAMIsignalEvent, &timer_tCLI_Send_Event_TimerId);
 if (intRC){SendCodingError("freeswitch_cli.cpp - Unable to Create Timer in fn SendDelayedHangup()"); return;}
 // load settings into stucture and then stucture into the timer (one shot timer)
 itimerspecCLISendDelay.it_value.tv_sec                       = iDelay;
 itimerspecCLISendDelay.it_value.tv_nsec                      = 0;	                				
 itimerspecCLISendDelay.it_interval.tv_sec                    = 0;			               
 itimerspecCLISendDelay.it_interval.tv_nsec                   = 0;
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tCLI_Send_Event_TimerId, 0, &itimerspecCLISendDelay, NULL);}
}

void Delayed_CLI_HANGUP(union sigval union_sigvalArg)
{
 void 			*ptrVal 			= union_sigvalArg.sival_ptr;
 param_c*           	ptrChannel                      = (param_c*) ptrVal;
 ExperientDataClass     objData;

 //objData.CallData.TransferData.intTransferNumberType = SIP;
// objData.FreeswitchData.objChannelData.strChannelName = ptrChannel->chardata;
 CancelTransfer(objData);

 delete ptrChannel;
}



int UN_Conference(ExperientDataClass objData)
{

 int intRC;
// string strChannelOne     = "Channel1: " +  objData.FreeswitchData.objLinkData.strChanneloneID;
// string strChannelTwo     = "Channel2: " +  objData.FreeswitchData.objLinkData.strChanneltwoID;
// //cout << strChannelOne << endl;
// //cout << strChannelTwo << endl;
 string strUUID_BRIDGE           = "api uuid_bridge " + objData.FreeswitchData.objLinkData.strChanneloneID + " " + objData.FreeswitchData.objLinkData.strChanneltwoID + "\n\n";

// FreeswitchCLIPort.SendLine("api freeswitch.consoleLog crit before\n\n");

 intRC = FreeswitchCLIPort.SendLine(strUUID_BRIDGE.c_str());
 if (intRC) {return intRC;}
 
// FreeswitchCLIPort.SendLine("api freeswitch.consoleLog crit after\n\n");

 return 0;
}


int BuildMeetMeConf(ExperientDataClass objData)
{
// string strChannel          = "Channel: " + objData.FreeswitchData.objLinkData.strChannelone;
// string strExtraChannel     = "ExtraChannel: " + objData.FreeswitchData.objLinkData.strChanneltwo;
 string strContext          = "Context: meetme-build";
 string strExtraContext     = "ExtraContext: meetme-build";
 string strExtension        = "Exten: " + objData.CallData.strConferenceNumber;
 string strExtraExtension   = "ExtraExten: " + objData.CallData.strConferenceNumber;
 string strPriority         = "Priority: 1";
 string strExtraPriority    = "ExtraPriority: 1";
 string strActionId         = "ActionId: 400-";
 string strConfVariable     = "Variable: Conf=" +  objData.CallData.strConferenceNumber;
 string strAsync            = "Async: true";
 string strTrunkandPosition = "Conf: "+ objData.CallData.strConferenceNumber + charTAB + "Position: "+ objData.CallData.stringPosn + charTAB+ "ControllerCallID: " + objData.CallData.stringUniqueCallID + charTAB +"\r\n";

 string strChannel, strExtraChannel;

// //cout << "build meetme : " << strChannel << " and " << strExtraChannel << endl;
 strActionId += strTrunkandPosition;

 if(!FreeswitchCLIPort.boolReadyToSend){return 1;}
 int intRC;

 //build conference using redirect
 intRC = FreeswitchCLIPort.SendLine((char *) strACTIONREDIRECT.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strChannel.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strExtraChannel.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strExtension.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strPriority.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strContext.c_str());
 if (intRC) {return intRC;} 
 intRC = FreeswitchCLIPort.SendLine((char *) strExtraExtension.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strExtraPriority.c_str());
 if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strExtraContext.c_str());
 if (intRC) {return intRC;} 
// intRC = FreeswitchCLIPort.SendLine((char *) strConfVariable.c_str());
// if (intRC) {return intRC;}
 //intRC = FreeswitchCLIPort.SendLine((char *) strAsync.c_str());
 //if (intRC) {return intRC;}
 intRC = FreeswitchCLIPort.SendLine((char *) strActionId.c_str());
 if (intRC) {return intRC;}


 //Enable TDD Reception from trunk channel w/mute off
 if (boolTDD_AUTO_DETECT)
  { 
//   intRC = TDDMode_AMI(objData.FreeswitchData.objLinkData.strChannelone, objData.CallData , objData.TDDdata, MODE_ON);
 //  intRC = TDDMode_AMI(objData.FreeswitchData.objLinkData.strChanneltwo, objData.CallData , objData.TDDdata, MODE_ON);
  }



 if (intRC) {return intRC;}
 return 0;
}


int TDDMode_AMI(ExperientDataClass objData, tddmodeset TDD_Set)
{
 int    intRC;
 string strSendMsg;


 switch (TDD_Set)
  {
   case MODE_ON:
         strSendMsg = "bgapi stop_tdd_detect ";
         strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID;
         strSendMsg+= "\n\n";
         strSendMsg = "bgapi start_tdd_detect ";
         strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID;
         strSendMsg+= "\n\n";
        break;
   case MODE_OFF:
         strSendMsg = "bgapi stop_tdd_detect ";
         strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID;
         strSendMsg+= "\n\n";

        break;
   case MUTE_ON:

        //cout << "TDDMode_AMI not coded" << endl;
        break;
   case MUTE_OFF:
        //cout << "TDDMode_AMI not coded" << endl;
        break;
  }

      
 intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());
 return intRC;



}

int SendTDDstring( ExperientDataClass objData)
{
 int    intRC;
 string strSendMsg;

 strSendMsg = "bgapi uuid_send_tdd ";
 strSendMsg+= objData.FreeswitchData.objChannelData.strChannelID + " ";
 strSendMsg+= objData.TextData.TDDdata.strTDDstring;
 strSendMsg+= "\n\n";
 
 if(!FreeswitchCLIPort.boolReadyToSend){return 1;}

 intRC = FreeswitchCLIPort.SendLine(strSendMsg.c_str());
 return intRC;

}


int LogInToFreeswitch()
{
 int intRC;
 ////cout << "log in to freeswitch" << endl;
 if(!FreeswitchCLIPort.boolReadyToSend){return 1;}
 if(boolSTOP_EXECUTION)  {return 1;}
 intRC = FreeswitchCLIPort.SendLine((char *) strACTION_LOGIN_FREESWITCH.c_str());
 if (intRC) {return intRC;}
 sleep(1);

 intRC = FreeswitchCLIPort.SendLine((char *) strADD_EVENT_LIST.c_str());
 if (intRC) {return intRC;}
 
 return 0;
}



string DecodeTDDmessage(string strInput)
{
 XMLParserBase64Tool	b64;
 unsigned char*         ptrUcharDecodeInput;
 XMLError               xe;
 string                 strOutput="";
 int                    intLength;

 if (strInput.empty()) {return "";}

 ptrUcharDecodeInput = b64.decode(strInput.c_str(),&intLength, &xe);
      
 if (xe != eXMLErrorNone) {SendCodingError( "freeswitch_cli.cpp - Error decoding TDD XML : error = " + int2str( xe )); return "";}

 for(int i=0; i< intLength; i++){strOutput+= ptrUcharDecodeInput[i];}

 return strOutput;
}

bool InitializeCLIports()
{
 extern int						intNUM_CLI_PORTS;
 extern ExperientTCPPort              			FreeswitchCLIPort;;

 //only one for now ....
 for (int i = 1; i <= intNUM_CLI_PORTS; i++) { 
   FreeswitchCLIPort.fInitializeAMIport(i);
 }
 return true;
}


bool ShutDownCLIports(pthread_t CLIPortListenThread[], int portnumber)
{
 extern ExperientTCPPort              			FreeswitchCLIPort;;
 int                                                    numberofports = 1; //for now

 // not using portnumber for now ....

 for (int i = 1; i<= numberofports; i++) {
  FreeswitchCLIPort.Disconnect();
 }
 for (int i = 1; i<= numberofports; i++) {FreeswitchCLIPort.boolRestartListenThread = true;}

 for (int i = 1; i<= numberofports; i++) { pthread_join( CLIPortListenThread[i], NULL); }  

 return true;
}

bool StartCLIListenThreads(pthread_t FreeswitchCLIListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t params[])
{
 // internal function to CTI thread
 extern int						intNUM_CLI_PORTS;
 int							intRC;

  for (int i = 1; i<= intNUM_CLI_PORTS; i++) 
   {
    params[i].intPassedIn = i;
    intRC = pthread_create(&FreeswitchCLIListenThread[i], pthread_attr_tAttr, *Freeswitch_CLI_TCP_Listen_Thread, (void*) &params[i]);
    if (intRC) {SendCodingError("freeswitch_cli.cpp ->StartCLIListenThreads() -> Unable to start CLI Listen thread!"); return false;}
   }
return true;
}

void *Freeswitch_CLI_TCP_Listen_Thread(void *voidArg)
{
 Thread_Data                    ThreadDataObj; 
 int 				intRC;  
 param_t* p          = (param_t*) voidArg;
 int      intPortNum = p->intPassedIn;
 MessageClass                   objMessage;
 Port_Data                      objPortData; 
 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="CLI Listen"+ int2strLZ(intPortNum);;
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in freeswitch_cli::Freeswitch_CLI_TCP_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);
 objPortData.fLoadPortData(AMI, intPortNum);
 objMessage.fMessage_Create(0, 603, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                            CTI_MESSAGE_603, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 
 boolTCP_LISTEN_THREAD_SHUTDOWN = false;

 while ((!boolSTOP_EXECUTION)&&(!INIT_VARS.ANIinitVariable.boolRestartFreeswitchCLIport)) {FreeswitchCLIPort.DoEvents(); experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);}
 FreeswitchCLIPort.Disconnect();
 sleep(1); 
 FreeswitchCLIPort.DoEvents();
 objMessage.fMessage_Create(0, 603, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                            CTI_MESSAGE_603b, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 FreeswitchCLIPort.Disconnect();
 boolTCP_LISTEN_THREAD_SHUTDOWN = true;
 return NULL;								
}


void *Freeswitch_CLI_Login_Thread(void *voidArg)
{
 int intRC;
 Thread_Data                    ThreadDataObj; 
     
 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="CLI Login";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in freeswitch_cli::Freeswitch_CLI_Login_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 do
 {
  // just keep on trying to log into the Freeswitch AMI indefinately ... 
  if (!boolSTOP_EXECUTION)
   {
    experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
    if(!FreeswitchCLIPort.GetConnected()) 
     {
      FreeswitchCLIPort.boolFreeswitchLoggedin = false;
      ConnectToFreeswitch();
      for (int i = 1; i <= 3; i++) {if (!boolSTOP_EXECUTION) {sleep(1);}}
     }
    if((FreeswitchCLIPort.GetConnected())&&(!boolSTOP_EXECUTION)&&(!FreeswitchCLIPort.boolFreeswitchLoggedin)) 
     {
      intRC = LogInToFreeswitch(); 
      if (intRC) {FreeswitchCLIPort.boolFreeswitchLoggedin = false;}
      else       {FreeswitchCLIPort.boolFreeswitchLoggedin = true;}
     }
    for (int i = 1; i <= 10; i++) { if (!boolSTOP_EXECUTION) {sleep(1);}}
   }
 } while (!boolSTOP_EXECUTION);

 FreeswitchCLIPort.Disconnect();
 boolAMI_LOGIN_THREAD_SHUTDOWN = true;
 return NULL;
}



 
void *AMI_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 Thread_Data            	ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="CLI Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in freeswitch_cli.cpp::AMI_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 //if (boolSTOP_EXECUTION) {return;}
// else                    {timer_settime(timer_tCLIMessageMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}

do
{

  experient_nanosleep(intAMI_MESSAGE_MONITOR_INTERVAL_NSEC);
  if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in AMI_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexCLIMessageQ);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexCLIMessageQ, "sem_wait@sem_tMutexCLIMessageQ in AMI_Messaging_Monitor_Event", 1);}

 while (!queue_objAMIMessageQ.empty())
  {
    objMessage = queue_objAMIMessageQ.front();
    queue_objAMIMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 800:						
       // AMI Thread Created
       break; 
      case 801:						
       // "[WARNING] AMI Thread Failed to Initialize, System Rebooting" ... should not pass thru here handled in krn
       break;
      case 802:						
       // "-[WARNING] AMI TCP Status: Disconnect, Code=%%% Desc=%%%"
       break;
      case 803:
       // "-[DEBUG] Message Monitor Timer Event Initialized"						
       break;
      case 804:
       // "-[INFO] AMI TCP Connection Status Code=%%% Desc=%%%"						
       break; 
      case 805:
       // "[WARNING] Unable to set CPU affinity for AMI Thread"					
       break; 
      case 806:
       //  "-[WARNING] Thread Signalling Semaphore failed to Lock"					
       break; 
      case 807:
       //  ">[RAW] TCP Data Received (Data Follows) %%%"					
       break; 
      case 808:
       // ">[INFO] AMI TCP Port Ready to Send"
       break;
      case 809:
       // Used to pass AMI Script Data to be written
       break;
      case 810:
       // ">[ALARM] All Devices Unregistered, Freeswitch Restarted"
       break;
      case 811:
       // ">[ALARM] All Devices Unregistered, Unable to Restart Freeswitch"
       break;
      case 812:
       //  ">[WARNING] Invalid Trunk Field, Data=\"%%%\", (Data Follows)"
       break;
      case 813:
       // ">[WARNING] Invalid Trunk Field (Out of Range), Data=\"%%%\", (Data Follows)"
       break;
      case 899:
       //  "-Thread Complete"
       break;     	
      default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in AMI Monitor\n";
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock AMI Message Q then Main Message Q
 sem_post(&sem_tMutexCLIMessageQ);
 sem_post(&sem_tMutexMainMessageQ);

/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tCLIMessageMonitorTimerId, 0, &itimerspecCLIMessageMonitorDelay, NULL);}
*/

}while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 898, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, AMI_MESSAGE_898);
 objMessage.fConsole();


return NULL;

}// end AMI_Messaging_Monitor_Event()



