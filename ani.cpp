/*****************************************************************************
* FILE: ani.cpp
* 
* DESCRIPTION: This file contains all of the functions integral to the ANI Thread
*
* Features:
*
*    1. ANI Devices  :                      Designed to Process Proctor ANI-Link serial communications
*                                           Open ended design will allow the incorporation of other
*                                           telephone systems with minimal programming.
*    2. Data Error Correction               Data reconstruction provided for out of order data reception (cable pulled scenarios)
*                                           preventing stale or new data from causing confusing displays on workstation call grid
*    3. Data Reliability Checks             Prevents bad data from causing system crashes
*    4. Telephone Formats:                  Supports both 8, 10 and 10/20 digit telephone formats
*    5. Out of Area NPD support             System will not stall from out of area NPD codes in older 8 digit systems
*    6. Heartbeats:                         Listens for activity or Heartbeats from the Proctor ANI-Link
*    7. Email/Alarm Notification:           Informational messages on startup;
*                                           Warning messages for unusual activity;
*                                           Alarm messages for connection down/unavailable;
*                                           Reminder messages that alarm condition still exists
*    8. Denial of Service Protection:       Ignores packets from non-registered IP addresses;
*                                           Timed shutdown when approaching full buffer;
*    9. Communications:                     Utilizes UDP communications for connections;
*                                           If necessary converted to serial communication via Ethernet to serial converter
*   10. Self-Diagnostics:                   Built in diagnostics to help troubleshoot line errors, etc.
*   11. Threaded Implementation:            Provides efficient utilization and reduces resource monopolization
*   12. NENA Compliant:                     Complies with the “NENA Recommended Generic Standards for E9-1-1 PSAP Equipment”
*
* AUTHOR: 4/6/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation 
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//test
//external class based variables
extern Thread_Trap                              OrderlyShutDown;
extern Thread_Trap                              UpdateVars;
extern queue <MessageClass>	                queue_objMainMessageQ;
extern queue <MessageClass>	                queue_objANIMessageQ;
extern IP_Address                               HOST_IP;
extern IP_Address                               MULTICAST_GROUP;
extern queue <ExperientDataClass> 	        queue_objANIData;
extern queue <ExperientDataClass> 	        queue_objAMIData;
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data				objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_ANI_PORT;
extern Port_Data                                objBLANK_AGI_PORT;
extern ani_system                               enumANI_SYSTEM;
extern Initialize_Global_Variables              INIT_VARS;

// ANI Global VARS
Trunk_Type_Mapping			        TRUNK_TYPE_MAP;
Trunk_Sequencing                                Trunk_Map;

struct itimerspec			        itimerspecANIHealthMonitorDelay;
struct itimerspec			        itimerspecDaily;
timer_t				                timer_tANIHealthMonitorTimerId;
timer_t				                timer_tDailyTimerId;
sem_t					        sem_tMutexANIMessageQ;
sem_t					        sem_tMutexANIPortDataQ;
sem_t					        sem_tANIFlag;
sem_t                                           sem_tMutexANIPort;
sem_t                                           sem_tMutexANIWorkTable;
sem_t                                           sem_tMutexANIInputQ;
sem_t                                           sem_tMutexConfAssignmentQ;
sem_t                                           sem_tConfAssignmentFlag;
sem_t                                           sem_tMutexConferenceNumberAssignmentObject;
string                                          strANI_14_DIGIT_REQ_TEST_KEY;
string 						strANI_16_DIGIT_REQ_TEST_KEY;
string                                          strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY;
string                                          strASTERISK_AMI_POSITION_HEADER_911;
string                                          strASTERISK_AMI_POSITION_HEADER_FIRE;
ExperientCommPort                               ANIPort[NUM_ANI_PORTS_MAX+1];
ExperientCommPort                               ServerConfAssignmentPort;
queue <DataPacketIn>	                        queueANIPortData[NUM_ANI_PORTS_MAX+1];
queue <DataPacketIn>                            queue_ConfAssignmentRequestData;
int                                             intNUM_ANI_PORTS;
int                                             intANI_PORT_DOWN_REMINDER_SEC;
int                                             intANI_PORT_DOWN_THRESHOLD_SEC;
unsigned int                                    intANI_ROTATE_CHECK_MULTIPLIER;
int                                             intTRUNK_ASSIGNMENT_LISTEN_PORT_NUMBER                          = 55000;
int                                             intTRUNK_ASSIGNMENT_REPLY_PORT_NUMBER                           = 55001;
volatile bool                                   boolCONF_ASSIGNMENT_RESPONSE_THREAD_READY_TO_SHUTDOWN          = false;
enum RebuildCallState                           {ToRing,ToConnect};
vector <string>                                 vstringASTERISK_ALARM_MESSAGE(50);
ANI_Data_Format                                 ANIdataFormat;
Last_On_Hold                                    objPosition;

//forward function declarations
//void                    ANI_Messaging_Monitor_Event(union sigval union_sigvalArg);
void                    Daily_Timed_Event(union sigval union_sigvalArg);
void                    *ANI_Port_Listen_Thread(void *voidArg);
void                    *Server_CONF_REQ_Listen_Thread(void *voidArg);
void                    *Server_CONF_REQ_Response_Thread(void *voidArg);
void                    ANI_Check_Hearbeat();
unsigned long long int  No_Ring_And_Connect(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg);
unsigned long long int  No_Ring(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg,bool boolConnect = false);
unsigned long long int  Different_Phone_Numer(ExperientDataClass objData, int intTrunk, int intPortNum, string stringArg, bool boolConnect = false);
bool                    Check_Connect(ExperientDataClass objData, int intTrunk, int intPortNum, string stringArg);
unsigned long long int  Check_Previous_Connect(ExperientDataClass objData, int intTrunk, int intPortNum, string stringArg, bool bool60secNansw );
void                    Check_Position_Number_Same(ExperientDataClass objData, int intTrunk,  int intPortNum, string stringArg);
bool                    Is_Same_Call(ExperientDataClass* objData, int intTrunk, int intPortNum, RebuildCallState enumArg, string stringArg, bool* boolWarningMsg
                                     , bool bool60secNansw = false);

void                    SendANIToMain(ani_functions ANI_Function, ExperientDataClass objData, int intPortNum, bool boolWarningMsg = false);
bool                    Match_Freeswitch_Object_To_Table(ExperientDataClass* objData, string strRawData, int intPort);
void                    Check_TDD_Caller_Sentences();
void                    Check_Conference_Size_On_Hangup(int i, ExperientDataClass objData, int intPortNum, string Hangup="");
unsigned int            RotatePortNumber(int intPortNum);
unsigned int            FindRegisteredANIport(int intPortNum);
unsigned int            FindActiveAMIport(int intPortNum);
int                     IndexWithConfNumber(unsigned long long int iConfNumber);
int                     IndexWithCallUniqueID(string strUniqueID);
int                     IndexWithFreeSwitchConference(string strUniqueID);
int                     IndexWithFreeSwitchConferenceName(string strName);
int                     IndexOfNewANITableEntry(ExperientDataClass objData);
int                     IndexofActiveChannelinTable( string strUniqueID);
int                     IndexofActiveChannelinTable( string strUniqueID, int iButNotThisIndex);
int                     IndexofActiveSharedLineInTable(int iLineNumber);
int                     IndexofTablePositionVectorMatch(string strUniqueID);
int                     IndexofTableTransferVectorMatch(string strUniqueID);
int                     IndexofTableConferenceVectorMatch(string strUniqueID);
int                     IndexWithTransferTarget(string strUUID);
int                     LocateRingChannelIdinTable(string strUniqueID);
int                     LocatePositionChannelIdinTableConferenceVector(string strUniqueID);
void                    Check_Stale_Vector_Data();
bool                    SendTransferToAMI(ExperientDataClass objData, int index, transfer_method eTransferMethod, Port_Data objPortData);
bool                    BothChannelsAlreadyPresentandConnected(Freeswitch_Data objData);
int                     LocatePositionChannelUsernameInTable(string strUsername);
void                    SendTryAgainToCaller(ExperientDataClass objData);
bool                    PositionIsActiveOnCallandNotonHold(int iPosition);
int                     IndexofActiveSMSConversation(string strFrom);
bool                    CheckForUnAttendedTransfer(Channel_Data objChannelData);
void                    CheckForCancelAttendedTransferPositiontoPosition(Channel_Data objChannelData, int iTableindex, int intTranVectorIndex);
bool                    TransferTargetIsDifferentPostion(Transfer_Data objTransferData); 
bool                    PreBuild_Conference(int intTableIndex);
bool                    Add_Conference_Member(int intTableIndex, ExperientDataClass objData); 
bool                    InitializeANIports(bool SKIPACTIVE=false);
bool                    ShutDownANIports(pthread_t ALiServicePortListenThread[], int inumber);
bool                    StartANIListenThreads(pthread_t ANIPortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t params[]);
string                  FindUUIDofCallinLot(int &i, int &j, string strLot);
void                    RemoveAllPostionsFromMSRPCall(int index, int aniport);
void                    RemoveAllOnHoldPostionsFromMSRPCall(int index, int intPortNum);


extern void *ANI_Messaging_Monitor_Event(void *voidArg);


//Globals
vector <ExperientDataClass>             vANIWorktable;
Telephone_Devices                       TelephoneEquipment;

bool                                    boolANI_20_DIGIT_VALID_CALLBACK;
Conference_Number                       objConferenceList;

/****************************************************************************************************
*
* Name:
*   Function void *ANI_Thread(void *voidArg)
*
*
* Description:
*   Handles all of the ANI-Link related activity for the controller. 
*
*
* Details:
*   This function is implemented as a detached thread from the main program. The pointer *voidArg is not
*   used at this time.
*   
*   The thread spawns a timer ( ANI_Messaging_Monitor_Event() ) which pushes messages to main , checks for ANI-Link
*   heartbeat messages, checks for stale data in the port buffer(s), checks for port restoration if a port has been
*   undergone emergency shut down and checks time elapsed for alarm notification of port down status. 
*
*   note : the interval of the timer should be less than the port buffer force transmit threshold.  Presently the timer is set
*          to just less than every 1/10th of a sec (intANI_Messaging_Monitor_INTERVAL_NSEC) and the buffer is set to 1/2 a sec.  
*          (doubleBUFFER_FORCE_TRANSMIT).  Worst case the buffer force transmit would execute at about 3/5 sec.
*
*   The thread also spawns X number of port listening threads ( ANI_Port_Listen_Thread() ) which act to listen on the defined ANI ports
*
*   A global semaphore is used to recieve a signal from interprocess communications.  The signals to the thread originate from the
*   following locations : 
*                         1. Main:                      to signal shutdown
*                         2. ANI_Port_Listen_Thread()   to signal reception of port data
*
*   The main loop idles waiting for the signal, it checks for the shutdown flag, if not then it pops data off a queue, validates 
*   the data and places the data into an ExperientDataClass object, queues the data to be logged and queues the object to Main for further 
*   processing.
*
*
* Parameters: 				
*   void *voidArg	      		not used
*
*
* Variables:
*   ABANDONED                                   Global  - <header.h> (ani_functions) enumeration member
*   ALARM                                       Global  - <header.h> (ani_functions) enumeration member
*   ALI_REPEAT_SCROLL_BACK                      Global  - <header.h> (ani_functions) enumeration member
*   ALI_REQUEST                                 Global  - <header.h> (ani_functions) enumeration member
*   ANI                                         Global  - <header.h> threadorPorttype enumeration
*   ANI_MESSAGE_300                             Global  - <defines.h> ANI Thread Created output message
*   ANI_MESSAGE_302                             Global  - <defines.h> ANI Health Monitor Timer Event Initialized output message
*   ANI_MESSAGE_303                             Global  - <defines.h> ANI Thread Signalling Semaphore failed to Lock output message
*   ANI_MESSAGE_304                             Global  - <defines.h> Port Initialized output message
*   ANI_MESSAGE_306                             Global  - <defines.h> Connect output message
*   ANI_MESSAGE_307                             Global  - <defines.h> Disconnect output message
*   ANI_MESSAGE_308                             Global  - <defines.h> Transfer output message
*   ANI_MESSAGE_309                             Global  - <defines.h> Conference output message
*   ANI_MESSAGE_310                             Global  - <defines.h> On Hold output message
*   ANI_MESSAGE_311                             Global  - <defines.h> Off Hold output message
*   ANI_MESSAGE_312                             Global  - <defines.h> Abandoned output message
*   ANI_MESSAGE_313                             Global  - <defines.h> No Correlation output message
*   ANI_MESSAGE_315                             Global  - <defines.h> ANI ALARM output message
*   ANI_MESSAGE_316                             Global  - <defines.h> Ringing output message
*   ANI_MESSAGE_317                             Global  - <defines.h> 60 Seconds No Answer output message
*   ANI_MESSAGE_318                             Global  - <defines.h> Heartbeat Received output message
*   ANI_MESSAGE_325                             Global  - <defines.h> ANI WARNING output message
*   ANI_MESSAGE_326                             Global  - <defines.h> WARNING Port Restored output message
*   ANI_MESSAGE_327                             Global  - <defines.h> Unexpected Character Count output message
*   ANI_MESSAGE_332                             Global  - <defines.h> ALARM Port Restored output message
*   ANI_MESSAGE_333                             Global  - <defines.h> First Heartbeat Received output message
*   ANI_MESSAGE_399                             Global  - <defines.h> ANI Thread complete output message
*   ANIPort[]                                   Global  - <ExperientCommPort> the ANI Port(s)
*   ANIPortDataRecieved			        Local   - <DataPacketIn> struct holding Cad Port Data recieved
*   ANIPortListenThread[]                       Local   - <pthread.h> pthread_t array
*   .boolFirstACKRcvd                           Global  - <ExperientCommPort> member.. true if first heartbeat recieved
*   .boolANIThreadReady                         Global  - <Thread_Trap> member.. volatile bool used to show that ANI thread is ready to shut down
*   boolDEBUG                                   Global  - <globals.h> display debug MSGs if true
*   boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC           Global  - <globals.h> config
*   .boolEmergencyShutdown                      Global  - <ExperientCommPort> member.. true if port shut down due to too much data
*   .boolPingStatus                             Global  - <ExperientUDPPort> member.. Ping Successful or Ping Fail
*   .boolPortActive                             Global  - <ExperientCommPort> member.. true if port is awaiting answer to data TX 
*   .boolReadyToShutDown                        Global  - <ExperientCommPort> member.. true if port is closed and ready to shut down 
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member.. true if port is in Reduced Heartbeat mode
*   .boolReducedHeartBeatModeFirstAlarm         Global  - <ExperientCommPort> member.. true if Alarm message has been sent
*   boolSTOP_EXECUTION                          Global	- when true, thread lock loop engages for shutdown
*   boolWarningMsg                              Local   - flag to display a warning message
*   .boolUnexpectedDataShowMessage              Global  - <ExperientCommPort> member.. flag to allow messages about unexpected data
*   CLOCK_REALTIME            	                Library - <ctime> real time clock constant
*   CONFERENCE                                  Global  - <header.h> (ani_functions) enumeration member
*   CONFIG_CHANGE                               Global  - <header.h> (ani_functions) enumeration member
*   CONNECT                                     Global  - <header.h> (ani_functions) enumeration member
*   CPU_AFFINITY_ALL_OTHER_THREADS_SET          Global  - <globals.h><cpu_set_t>
*   DISCONNECT                                  Global  - <header.h> (ani_functions) enumeration member
*   .enumANIFunction                            Global  - <ExperientDataClass> <header.h> (ani_functions) enumeration member
*   .enumPortType                               Global  - <ExperientCommPort> (threadorPorttype) member
*   .enumPortType                               Global  - <ExperientCommPort><UDPPort> (threadorPorttype) member
*   .enumThreadSending                          Global  - <ExperientDataClass> member
*   errno                     	                Library - <errno.h> - error handling return code
*   HEARTBEAT                                   Global  - <header.h> (ani_functions) enumeration member
*   HOLD_OFF                                    Global  - <header.h> (ani_functions) enumeration member
*   HOLD_ON                                     Global  - <header.h> (ani_functions) enumeration member
*   HOST_IP                                     Global  - <IP_Address> object
*   i                         	                Local	- general purpose integer variable
*   intANI_PORT_DOWN_THRESHOLD_SEC              Global  - <globals.h> loaded by config file 
*   intANI_PORT_DOWN_REMINDER_SEC               Global  - <globals.h> loaded by config file 
*   intANI_Messaging_Monitor_INTERVAL_NSEC      Global  - <defines.h>
*   intANI_HEARTBEAT_INTERVAL_SEC               Global  - <defines.h>
*   intFirstLook                                Local   - used for starting point for ani port lookup
*   .intHeartbeatInterval                       Global  - <ExperientCommPort> member.. heartbeat interval of port
*   .intLength                                  Global  - <DataPacketIn> member
*   intNUM_ANI_PORTS                            Global  - <globals.h>
*   intPING_TIMEOUT                             Global  - <defines.h>
*   .intPortDownReminderThreshold               Global  - <ExperientCommPort> member.. threshold in sec for reminder email
*   .intPortDownThresholdSec                    Global  - <ExperientCommPort> member.. threshold in sec to define port is down
*   intPortNum				        Local   - port number
*   .intPortNum                                 Global  - <DataPacketIn> member
*   .intPortNum                                 Global  - <ExperientCommPort> member
*   .intPortNum                                 Global  - <ExperientCommPort><ipworks.h><UDPPort> member
*   intRC             		                Local   - integer - return code for various library function calls
*   intTHREAD_SHUTDOWN_DELAY_SEC                Global  - <defines.h> integer
*   intTrunk                                    Local   - integer  trunk number
*   .intUnexpectedDataCharacterCount            Global  - <ExperientCommPort> member
*   .intUniqueCallID                            Global  - <ExperientDataClass> member
*   .itimerspecANIHealthMonitorDelay            Global  - <ctime> struct itimerspec which contains timing parameters
*   KRN_MESSAGE_142				Global  - <defines.h> CPU affinity Error message 
*   LOG_ALARM                                   Global  - <defines.h> code to log thread, display to console and log to disk ,and email alarm    
*   LOG_CONSOLE_FILE                            Global  - <defines.h> code to log thread, display to console and log to disk
*   LOG_WARNING                                 Global  - <defines.h> code to log thread, to email warning, display to console and log to disk
*   NO_CORRELATION                              Global  - <header.h> (ani_functions) enumeration member
*   objANIData                   	        Local	- <ExperientDataClass> object
*   vANIWorktable                               Global  - vector <ExperientDataClass> open ended object array
*   objMessage				        Local   - <MessageClass> object
*   OrderlyShutDown                             Global  - <Thread_Trap> object
*   .PingPort                                   Global  - <ExperientCommPort> <ExperientPing> object
*   pthread_attr_tAttr                          Local   - <pthread.h> pthread_attr_t
*   PTHREAD_CREATE_JOINABLE                     Library - <pthread.h> constant
*   queueANIPortData[]                          Global  - queue <DataPacketIn> array
*   RINGING                                     Global  - <header.h> (ani_functions) enumeration member				
*   sem_tANIFlag                 	        Global  - <semaphore.h> semaphore flag for interprocess communication
*   sem_tMutexCadInputQ   		        Global  - <semaphore.h> semaphore flag set to mutex Cad Input Q
*   sem_tMutexANIPortDataQ		        Global  - <semaphore.h> semaphore flag set to mutex ANI Port Data Q
*   sem_tMutexUDPPortBuffer                     Global  - <semaphore.h> <ExperientCommPort><UDPPort> member semaphore
*   sigeventANIMessagingMonitorEvent	        Local	- <csignal> struct sigevent that contains the data that defines the event handler
*   .sigev_notify                               Library - <csignal> struct sigevent member
*   .sigev_notify_attributes                    Library - <csignal> struct sigevent member
*   .sigev_notify_function                      Library - <csignal> struct sigevent member
*   SIGEV_THREAD                                Library - <csignal> constant
*   SILENT_ENTRY_LOG_OFF                        Global  - <header.h> (ani_functions) enumeration member
*   stringABANDONED                             Local   - <cstring> constant string
*   .stringANIAlarmCode                         Global  - <ExperientDataClass> member
*   .stringANIAlarmMessage                      Global  - <ExperientDataClass> member
*   .stringBuffer                               Global  - <ExperientDataClass> member
*   stringCONFERENCE                            Local   - <cstring> constant string
*   stringCONNECT                               Local   - <cstring> constant string
*   .stringDataIn                               Global  - <DataPacketIn> member
*   stringDISCONNECT                            Local   - <cstring> constant string
*   stringHOLDON                                Local   - <cstring> constant string
*   stringHOLDOFF                               Local   - <cstring> constant string
*   stringNOCORRELATION                         Local   - <cstring> constant string
*   .CallData.stringPANI                                 Global  - <ExperientDataClass> member    
*   .stringPosn                                 Global  - <ExperientDataClass> member
*   stringRINGING                               Local   - <cstring> constant string
*   stringSILENTENTRYLOGOFF                     Local   - <cstring> constant string
*   stringSIXTYSECNOANSWER                      Local   - <cstring> constant string
*   stringTemp                                  Local   - <cstring> temporary string data 
*   stringTemp2                                 Local   - <cstring> temporary string data
*   stringTenDigitPhoneNumber                   Global  - <ExperientDataClass> member
*   stringTRANSFER                              Local   - <cstring> constant string
*   stringTrunk                                 Global  - <ExperientDataClass> member 
*   TIME_DATE_CHANGE                            Global  - <header.h> (ani_functions) enumeration member   
*   timespecNanoDelay                           Global  - <ctime> struct timespec holds time delay for nanosleep()
*   timespecTimeLastHeartbeat                   Global  - <ExperientDataClass> (struct timespec) member
*   timespecTimeRecordReceivedStamp             Global  - <ExperientDataClass> (struct timespec) member
*   timespecRemainingTime                       Global  - <ctime> struct timespec argument for nanosleep()
*   timer_tANIHealthMonitorTimerId	        Global	- <ctime> timer id associated with Cad_Messaging_Monitor_Event()
*   ToConnect                                   Global  - <RebuildCallState> enumeration member
*   ToRing                                      Global  - <RebuildCallState> enumeration member
*   TRANSFER                                    Global  - <header.h> (ani_functions) enumeration member
*   TRANSFER_NUMBER_PROGRAM                     Global  - <header.h> (ani_functions) enumeration member
*   .UDP_Port                                   Global  - <ExperientCommPort> <ExperientUDPPort> object
*                                                                          
* Functions:
*   Abnormal_Exit()			Global  <globalfunctions.h>                     (void)
*   clock_gettime()                     Library <ctime>
*   .Config()				Library <ipworks.h> <UDPPort>                   (char*)
*   .Config()				Library <ipworks.h> <Ping>                      (char*)
*   Console_Message()			Global  <globalfunctions.h>                     (void)
*   enqueue_Main_Input()                Global  <globalfunctions.h>                     (void)
*   enQueue_Message()		       	Global  <globalfunctions.h>                     (void)
*   .fAllThreadsReady()                 Global  <Thread_Trap>                             (bool)
*   .fEmergencyPortShutDown()           Global  <ExperientCommPort>                     (void)
*   .fClearHeartbeatMissingMode()       Global  <ExperientCommPort>                     (void)
*   .fClear_Record()                    Global  <ExperientDataClass>                    (void)
*   .fMessage_Create()			Global  <MessageClass>                          (void)
*   .front()                            Library <queue>                                 (*this)
*   getpid()				Library <unistd.h>                              (pid_t)                             
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   Is_Same_Call()                      Local                                           (bool)
*   nanosleep()                         Library <unistd.h>                              (int)
*   .pop()                              Library <queue>                                 (void)
*   Ports_Ready_To_Shut_Down()          Global  <globalfunctions.h>                     (bool)
*   PortQueueRoundRobin()               Global  <globalfunctions.h>                     (int)
*   pthread_attr_init()                 Library <pthread.h>                             (int)
*   pthread_attr_setdetachstate()       Library <pthread.h>                             (int)
*   pthread_create()                    Library <pthread.h>                             (int)
*   pthread_self()			Library <pthread.h>                             (pthread_t)
*   pthread_setaffinity_np()		Library <pthread.h>                             (int)
*   SendANIToMain()                     Local                                           (void)
*   Semaphore_Error()                   Global  <globalfunctions.h>                     (void)
*   sem_init()                          Library <semaphore.h>                           (int)
*   sem_wait()             		Library <semaphore.h>                           (int)
*   sem_post()				Library <semaphore.h>                           (int)
*   .SetAcceptData()			Library <ipworks.h> <UDPPort>                   (int)
*   .SetActive()			Library <ipworks.h> <UDPPort>                   (int)
*   .SetDontRoute()                     Library <ipworks.h> <UDPPort>                   (int)
*   .SetLocalHost()			Library <ipworks.h> <UDPPort>                   (int)
*   .SetLocalHost()                     Library <ipworks.h> <Ping>                      (int)
*   .SetLocalPort()			Library <ipworks.h> <UDPPort>                   (int)
*   .SetPacketSize()                    Library <ipworks.h> <Ping>                      (int)
*   .fSetPortActive()                   Global  <ExperientCommPort>                     (void)
*   .SetRemoteHost()			Library <ipworks.h> <UDPPort>                   (int)
*   .SetRemotePort()			Library <ipworks.h> <UDPPort>                   (int)
*   .SetTimeout()                       Library <ipworks.h> <Ping>                      (int)
*   Set_Timer_Delay()			Global  <globalfunctions.h>                     (void)
*   sleep()                             Library <unistd.h>                              (unsigned int)
*   timer_settime()			Library <ctime>                                 (int)
*   timer_create()			Library <ctime>                                 (int)
*   timer_delete()			Library <ctime>                                 (int)
*   .Error_Handler()                    Global  <ExperientUDPPort>                      (void)
*
* AUTHOR: 4/6/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation 
****************************************************************************************************/
void *ANI_Thread(void *voidArg)
{
 struct sigevent         sigeventDailyTimedEvent;
 struct sigevent	 sigeventANIMessagingMonitorEvent;
 int                     intRC;
 MessageClass            objMessage;
 string                  stringTemp, stringTemp2;
 pthread_t               ANIPortListenThread[intNUM_ANI_PORTS + 1];
 pthread_t		 pthread_tAniMessagingThread;
 param_t                 params[intNUM_ANI_PORTS + 1]; 
 pthread_attr_t          pthread_attr_tAttr;
 ExperientDataClass      objANIData;
 Call_Data               objCallData;
 DataPacketIn		 ANIPortDataRecieved;
 unsigned int		 intPortNum;
 unsigned int            intFirstLook = 0;
 unsigned int            intTrunk;
 unsigned int            intPosn;
 unsigned int            intTransferToPosition;
 struct timespec         timespecRemainingTime;
 int                     index;
 int                     iTranIndex;
 int                     iConfIndex;
 int                     iPosIndex;
 int                     iCount;
 string                  strLegend;
 string                  strRoute;
 string                  strChannel;
 string                  strAMIVariable;
 string                  stringCONNECT                = "CONNECT";
 string                  stringTRANSFER               = "TRANSFER";
 string                  stringDISCONNECT             = "DISCONNECT";
 string                  stringCONFERENCE             = "CONFERENCE";
 string                  stringHOLDON                 = "HOLD ON";
 string                  stringHOLDOFF                = "HOLD OFF";
 string                  stringABANDONED              = "ABANDONED";
 string                  stringRINGING                = "RINGING";
 string                  stringNOCORRELATION          = "NO CORRELATION";
 string                  stringSIXTYSECNOANSWER       = "SIXTY SEC NO ANSWER";
 string                  stringSILENTENTRYLOGOFF      = "SILENT ENTRY LOGOFF";
 bool                    boolWarningMsg;
 bool                    bIsInConference;
 Port_Data               objPortData;
 size_t                  sz;
 bool                    boolA, boolB, boolC;
 bool                    boolRunningCheck;
 Channel_Data            objChannelData;
 PhoneBookEntry          objCallID;
 Thread_Data                     		 ThreadDataObj;      
 extern vector <Thread_Data> 			 ThreadData;
/*
  // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,355, objBLANK_CALL_RECORD, objBLANK_ANI_PORT, KRN_MESSAGE_142); enQueue_Message(ANI,objMessage);}
*/

 ThreadDataObj.strThreadName = "ANI";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ani.cpp::ANI_Thread()", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);


 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,300, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                            objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_300, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(ANI,objMessage);

 // Initialize ANI PORT I/O
 InitializeANIports();
 pthread_attr_init(&pthread_attr_tAttr);
 pthread_attr_setdetachstate(&pthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);

 //we do not need listen threads for ANI
 //if (!StartANIListenThreads(ANIPortListenThread, &pthread_attr_tAttr, params)) {Abnormal_Exit(ALI, EX_OSERR, ANI_MESSAGE_390);}

/*
 for (int i = 1; i <= intNUM_ANI_PORTS; i++)
  {
   ANIPort[i].UDP_Port.fInitializeANIport(i);
   objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,304, objBLANK_CALL_RECORD, objBLANK_ANI_PORT, ANI_MESSAGE_304); 
   enQueue_Message(ANI,objMessage);

  }// end for (int i = 1; i <= intNUM_ANI_PORTS; i++)
*/
/*
 // start port listen thread(s)
 pthread_attr_init(&pthread_attr_tAttr);
 pthread_attr_setdetachstate(&pthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
 for (int i = 1; i<= intNUM_ANI_PORTS; i++)
  {
   params[i].intPassedIn = i;
   intRC = pthread_create(&ANIPortListenThread[i], &pthread_attr_tAttr, *ANI_Port_Listen_Thread, (void*) &params[i]);
   if (intRC) {Abnormal_Exit(ANI, EX_OSERR, ANI_MESSAGE_390, int2str(i));}
  }
*/

 // start ANI messaging thread
 intRC = pthread_create(&pthread_tAniMessagingThread, &pthread_attr_tAttr, *ANI_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(ANI, EX_OSERR, ANI_MESSAGE_390);}


/*
 // set up ANI message monitor timer event
 sigeventANIMessagingMonitorEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventANIMessagingMonitorEvent.sigev_notify_function 		= ANI_Messaging_Monitor_Event;
 sigeventANIMessagingMonitorEvent.sigev_notify_attributes 		= NULL;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventANIMessagingMonitorEvent, &timer_tANIHealthMonitorTimerId);
 if (intRC){Abnormal_Exit(ANI, EX_OSERR, KRN_MESSAGE_180, "timer_tANIHealthMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecANIHealthMonitorDelay, 0,intANI_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intANI_MESSAGE_MONITOR_INTERVAL_NSEC );
 Set_Timer_Delay(&itimerspecANIHealthMonitorDelay, 0,intANI_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0);       // One Shot Timer ARM
 timer_settime( timer_tANIHealthMonitorTimerId, 0, &itimerspecANIHealthMonitorDelay, NULL);		// arm timer

 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,302, objBLANK_CALL_RECORD, objBLANK_ANI_PORT, ANI_MESSAGE_302); 
 enQueue_Message(ANI,objMessage);
*/

 //create Daily Timer
 
 sigeventDailyTimedEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventDailyTimedEvent.sigev_notify_function 		        = Daily_Timed_Event;
 sigeventDailyTimedEvent.sigev_notify_attributes 		= NULL;

 intRC = timer_create(CLOCK_MONOTONIC, &sigeventDailyTimedEvent, &timer_tDailyTimerId);
 if (intRC){Abnormal_Exit(ANI, EX_OSERR, KRN_MESSAGE_180, "timer_tDailyTimerId", int2strLZ(errno));}
// Set_Timer_Delay(&itimerspecDaily, SECONDS_PER_DAY, 0, SECONDS_PER_DAY, 0 );
 Set_Timer_Delay(&itimerspecDaily, SECONDS_PER_DAY, 0, 0, 0 );                  // One Shot Timer
 timer_settime( timer_tDailyTimerId, 0, &itimerspecDaily, NULL);		// arm timer


 objPosition.IintializeVector();

 //************************************************************************************************************************************************
/*

							MAIN LOOP

*/
//*************************************************************************************************************************************************
 do 
  {

   intRC = sem_wait(&sem_tANIFlag);							// wait for data signal

   // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
   if (intRC)
    {
     objMessage.fMessage_Create(LOG_WARNING,303, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_303); 
     enQueue_Message(ANI,objMessage);
    } 

//************************************************************************************************************************************************
/*

							SHUTDOWN TRAP

*/
//************************************************************************************************************************************************
   // Shut Down Trap Area
   if (boolSTOP_EXECUTION )
    {
     sem_post(&sem_tConfAssignmentFlag);                             // send signal to                                                    
     //timer_delete(timer_tANIHealthMonitorTimerId);
     timer_delete(timer_tDailyTimerId);     
     for (int i = 1; i <= intNUM_ANI_PORTS; i++)
      {
       pthread_join( ANIPortListenThread[i], NULL); 
      } 
     pthread_join( pthread_tAniMessagingThread, NULL); 
     OrderlyShutDown.boolANIThreadReady = true;
     while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}        
     break;										
    }// end if (boolSTOP_EXECUTION  )

  if (boolUPDATE_VARS)  {
    UpdateVars.boolANIThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    if (!boolSTOP_EXECUTION) {
     //need torewrite this section with a class var used it initialize the ports .......
     if (INIT_VARS.ANIinitVariable.boolRestartANIports) {
      //freeswitch CLI is used for ANI Port (different thread!!!)......................................
      //ShutDownANIports(ANIPortListenThread, INIT_VARS.ANIinitVariable.intOldNumberOfANIports);
      INIT_VARS.ANIinitVariable.boolRestartANIports = false;
      InitializeANIports();
      //   if (!StartANIListenThreads(ANIPortListenThread, &pthread_attr_tAttr, params)) { SendCodingError("ani.cpp - listen thread failure - recommend restart!");;}
     }
    else {
     InitializeANIports();

    }
    }
    UpdateVars.boolANIThreadReady = false;       
  }

//*************************************************************************************************************************************************
/*
							PROCESS initiate/cancel transfers, sendTDD ,Update Worktable
							GUI --> PBX   GUI --> GUI
*/
//************************************************************************************************************************************************
   // process Freeswitch initiate/cancel transfers, sendTDD ,Update Worktable if present
   while (!ThreadsafeEmptyQueueCheck(&queue_objANIData, &sem_tMutexANIInputQ))
    {
     intRC = sem_wait(&sem_tMutexANIInputQ);
     if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIInputQ, "0 sem_wait@sem_tMutexANIInputQ in *ANI_Thread()", 1);}
     objANIData.fClear_Record();
     objANIData = queue_objANIData.front();
     queue_objANIData.pop();
     sem_post(&sem_tMutexANIInputQ);
   
     if (objANIData.enumANISystem != ASTERISK)       {continue; /* jump to end  while (!queue_objANIData.empty())*/}
     intPortNum = objANIData.intANIPortCallReceived;
     strAMIVariable.clear();
     switch (objANIData.enumANIFunction)
      {
       case UPDATE_CALLBACK:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-a", 1);}
            
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            vANIWorktable[index].CallData.fLoadCallbackNumber(objANIData.CallData.stringTenDigitPhoneNumber);
            vANIWorktable[index].CallData.fSetTrunkTypeString();
            sem_post(&sem_tMutexANIWorkTable);

            break;

       case SEND_FLASH:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-b", 1);}

            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}

           // //cout << "got send flash" << endl;  // put in some kind of message and pop up ....

            objANIData.FreeswitchData=vANIWorktable[index].FreeswitchData;


    //        objANIData.CallData.TransferData.eTransferMethod = mTANDEM;
            objANIData.enumANIFunction                       = SEND_FLASH;
            Queue_AMI_Input(objANIData);
            sem_post(&sem_tAMIFlag);   

            sem_post(&sem_tMutexANIWorkTable);
            break;
 
       case SILENT_ENTRY_LOG_OFF:
           // //cout << "silent entry logoff 2" << endl;
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-b", 1);} 

            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}

            strLegend = ANI_CONF_MEMBER_POSITION_PREFIX + int2str(objANIData.intWorkStation);
            iPosIndex = vANIWorktable[index].FreeswitchData.fFindLegendInPosnVector(strLegend);
            if (iPosIndex < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            
            objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosIndex];

            objANIData.CallData.TransferData.eTransferMethod = mBLANK;
            objANIData.enumANIFunction                       = KILL_CHANNEL;
            Queue_AMI_Input(objANIData);
            sem_post(&sem_tAMIFlag);   
    //        //cout << "got HANGUP" << endl;
            sem_post(&sem_tMutexANIWorkTable);
            break;
               
       case KILL_CHANNEL:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-b", 1);}

            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}

            iTranIndex = vANIWorktable[index].FreeswitchData.fFindLegendInPosnVector(objANIData.CallData.ConfData.strParticipant);
            if (iTranIndex < 0) { iTranIndex = vANIWorktable[index].FreeswitchData.fFindLegendInDestVector(objANIData.CallData.ConfData.strParticipant);}
            else                {objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iTranIndex];}

            if (iTranIndex < 0) { sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            else                {objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall[iTranIndex];}

           // //cout << "got Kill Channel" << endl;  // put in some kind of message and pop up ....

            objANIData.CallData.TransferData.eTransferMethod = mBLANK;
            objANIData.enumANIFunction                       = CANCEL_TRANSFER;
            Queue_AMI_Input(objANIData);
            sem_post(&sem_tAMIFlag);   

            sem_post(&sem_tMutexANIWorkTable);
            break;

       case BLIND_TRANSFER:
            ////cout << " new blind transfer !!!!!!!!!!!!!!" << endl;
            objChannelData.fClear();
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-BLIND_TRANSFER", 1);}
            index = IndexWithCallUniqueID(objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            // check number of people on call otherwise send error
             intPosn = objANIData.intWorkStation;
            if (vANIWorktable[index].FreeswitchData.fNumberofChannelsInCall() != 2){
             objANIData = vANIWorktable[index];
             objANIData.CallData.fLoadPosn(intPosn);
             objMessage.fMessage_Create(LOG_INFO, 381, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.CallData, objBLANK_TEXT_DATA,
                                        objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_381);
             enQueue_Message(ANI,objMessage);
             sem_post(&sem_tMutexANIWorkTable);
             continue; /* jump to end  while (!queue_objANIData.empty())*/
            }
            if (vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall.size()) {
             objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall[0];
             objANIData.FreeswitchData.objChannelData.strTranData = objANIData.CallData.TransferData.strTransfertoNumber;
             objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[0];
            }
            else{
             // Position to postion call
             if (vANIWorktable[index].FreeswitchData.vectPostionsOnCall[0].iPositionNumber == (int)intPosn){
              objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[1];
              objANIData.FreeswitchData.objChannelData.strTranData = objANIData.CallData.TransferData.strTransfertoNumber;
              objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[0];
             }
             else{
              objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[0];
              objANIData.FreeswitchData.objChannelData.strTranData = objANIData.CallData.TransferData.strTransfertoNumber;
              objChannelData = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[1];
             }
            }
            ////cout << "sending blind transfer to AMI" << endl;
            Queue_AMI_Input(objANIData);
            sem_post(&sem_tAMIFlag);

           // //cout << "sending kill to other channel" << endl;
            objANIData.enumANIFunction = KILL_CHANNEL;           
            objANIData.FreeswitchData.objChannelData = objChannelData;
            Queue_AMI_Input(objANIData);
            sem_post(&sem_tAMIFlag);           

            sem_post(&sem_tMutexANIWorkTable);
            break;
     
       case TRANSFER:
            //cout << " ani TRANSFER!!!!!!!!!!!!!!" << endl;
            bIsInConference = false;
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-b", 1);}

            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}

            // check if phone number is in dest list .... Or is currently Conferenced in on Call ... this needs to change ..... Position number from ipaddress ..... or from list ????

            // This section is N/A for SLA  TBC ........     
            intTransferToPosition = objANIData.CallData.TransferData.fReturnTransferNumberAsPositionNumber();
            if (intTransferToPosition) {bIsInConference = vANIWorktable[index].CallData.fIsInConference(POSITION_CONF, intTransferToPosition);}

/*
            strChannel = vANIWorktable[index].FreeswitchData.fFindDestinationChannelIDfromPhoneNumber(objANIData.CallData.fLoadCallBackDisplay(objANIData.CallData.TransferData.strTransfertoNumber));
            if (((strChannel.length()) || (bIsInConference))&& (objANIData.CallData.TransferData.strTransfertoNumber != "911"))
             {
              // TBC may need to switch these 2 lines
              objCallData = objANIData.fCallData();
              objCallData.fLoadPosn(objANIData.intWorkStation);
              objMessage.fMessage_Create(LOG_WARNING, 368, objCallData, objPortData, ANI_MESSAGE_368, objANIData.CallData.TransferData.strTransfertoNumber);
              enQueue_Message(ANI,objMessage);
              sem_post(&sem_tMutexANIWorkTable);
//            jump to end  while (!queue_objANIData.empty()) 
              continue;                                              
             }
*/

            // generate a UUID for the transfer ...
            
            objCallID.fClear();
            objCallID = vANIWorktable[index].CallerIDandNamefromConference();
            vANIWorktable[index].FreeswitchData.objChannelData.strCallerID = objCallID.strCallerID;
            vANIWorktable[index].FreeswitchData.objChannelData.strCustName = objCallID.strCustName;

            objANIData.FreeswitchData = vANIWorktable[index].FreeswitchData;

      //      if (objANIData.enumANIFunction == TRANSFER) { SendTransferToAMI(objANIData, index, mGUI_TRANSFER);}
      //      else                                        { SendTransferToAMI(objANIData, index, mBLIND_TRANSFER);}


            // check if transfer is an INDIGITAL transfer
            //cout << "Transfer methos before -> " << objANIData.CallData.TransferData.fTransferMethod() << endl;

            boolRunningCheck = objANIData.CallData.TransferData.fCheckINDIGITAL_Transfer_Number();
            //cout << "bool after Indigital transfer check -> " << boolRunningCheck << endl;
            boolRunningCheck = boolRunningCheck || objANIData.CallData.TransferData.fCheckINTRADO_TRANSFER(vANIWorktable[index].CallData.intTrunk);
            //cout << "bool after INTRADO RFAI transfer check -> " << boolRunningCheck << endl;
            boolRunningCheck = boolRunningCheck || objANIData.CallData.TransferData.fCheckREFER_TRANSFER(vANIWorktable[index].CallData.intTrunk); 
            //cout << "bool after REFER transfer check -> " << boolRunningCheck << endl;

            //cout << "TRANS TYPE " << objANIData.CallData.TransferData.strTransferType << endl;
            //cout << "Transfer methos after -> " << objANIData.CallData.TransferData.fTransferMethod() << endl;            
           // //cout << "send this transfer to AMI" << endl;
            SendTransferToAMI(objANIData, index, objANIData.CallData.TransferData.eTransferMethod, objPortData);
            
            sem_post(&sem_tMutexANIWorkTable);
            break;

       case CANCEL_TRANSFER:
            ////cout << " ANI Cancel Transfer" << endl;
            
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-d", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
           // //cout << "index -> " << index << endl;
            boolA = false;         

            // find channel data from Transfer vector using TransferTargetUUID (PRIMARY)
            iTranIndex = vANIWorktable[index].CallData.fFindTransferTarget(objANIData.CallData.TransferData.strTransferTargetUUID);
            if (iTranIndex >= 0) {
              strChannel = vANIWorktable[index].CallData.vTransferData[iTranIndex].strTransferFromUniqueid;
            } else {

             // find transfer from TransferData Vector
             strChannel = vANIWorktable[index].FreeswitchData.fFindDestinationChannelIDfromPhoneNumber(objANIData.CallData.TransferData.strTransfertoNumber);
             if (!strChannel.length()) { 
              strChannel = vANIWorktable[index].CallData.fFindChannelIDfromPhoneNumber(objANIData.CallData.TransferData.strTransfertoNumber);
             }            
             if (!strChannel.length()) { 
              strChannel = vANIWorktable[index].FreeswitchData.fFindPositionChannelIDfromPhoneNumber(objANIData.CallData.TransferData.strTransfertoNumber);
             }
             
             iTranIndex = vANIWorktable[index].CallData.fFindTransferData(objANIData.CallData.TransferData.strTransfertoNumber, objANIData.intWorkStation);
            }

            //This could be a Drop Last Leg Scenario 
            if ((strChannel.empty())&&(iTranIndex < 0)){
             //cout << "drop Last Leg ???" << endl;
             //cout << TransferMethod(objANIData.CallData.TransferData.eTransferMethod) << endl;
             //cout << "T-Number" << objANIData.CallData.TransferData.strTransfertoNumber << endl;
             //cout << "S-Number" << objANIData.CallData.TransferData.strNG911PSAP_TransferNumber << endl;
            // objANIData.CallData.TransferData.fDisplay();

             sem_post(&sem_tMutexANIWorkTable); continue;/* jump to end  while (!queue_objANIData.empty())*/
            }

            if ((strChannel.empty())&&(iTranIndex >=0)){strChannel = vANIWorktable[index].CallData.vTransferData[iTranIndex].strTransferFromUniqueid;}

            vANIWorktable[index].CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, objANIData.CallData.TransferData.strTransfertoNumber, intPortNum);
            vANIWorktable[index].CallData.TransferData.fLoadFromPosition(objANIData.intWorkStation);
            objANIData.FreeswitchData = vANIWorktable[index].FreeswitchData;
            objANIData.CallData     = vANIWorktable[index].CallData;
            objANIData.CallData.fLoadPosn(objANIData.intWorkStation);
            // Just loading the UUID ... all other info in Channel will be stale ...
            objANIData.FreeswitchData.objChannelData.fLoadChannelID(strChannel);
 


            if (iTranIndex >= 0) { 

              vANIWorktable[index].CallData.vTransferData[iTranIndex].boolGUIcancel = true;
              if (vANIWorktable[index].CallData.TransferData.eTransferMethod == mTANDEM) {
               vANIWorktable[index].CallData.vTransferData[iTranIndex].fCheckINTRADO_TRANSFER(vANIWorktable[index].CallData.intTrunk);
              }
              objANIData.CallData.TransferData = vANIWorktable[index].CallData.vTransferData[iTranIndex];

              switch ( objANIData.CallData.TransferData.eTransferMethod )
               {
                case mTANDEM:
                     
                     vANIWorktable[index].CallData.fConferenceStringRemove(vANIWorktable[index].CallData.vTransferData[iTranIndex].strConfDisplay);
                     vANIWorktable[index].enumANIFunction   = CANCEL_TRANSFER;
                     vANIWorktable[index].stringTimeOfEvent = objMessage.stringTimeStamp;
                     objANIData.fSetANIfunction(CANCEL_TRANSFER);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_367t, objANIData.CallData.TransferData.strTransfertoNumber,
                                                vANIWorktable[index].CallData.TransferData.strTransferTargetUUID); 
                     enQueue_Message(ANI,objMessage);
                     enqueue_Main_Input(ANI, vANIWorktable[index]);
                     break;

                case mINTRADO:
                     
                     vANIWorktable[index].CallData.fConferenceStringRemove(vANIWorktable[index].CallData.vTransferData[iTranIndex].strConfDisplay);
                     vANIWorktable[index].enumANIFunction   = CANCEL_TRANSFER;
                     vANIWorktable[index].stringTimeOfEvent = objMessage.stringTimeStamp;
                     enqueue_Main_Input(ANI, vANIWorktable[index]);
                     objANIData.fSetANIfunction(RFAI_DROP_LEG);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_367s, objANIData.CallData.TransferData.strTransfertoNumber,
                                                vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                 
                     enQueue_Message(ANI,objMessage);
                     break;

                case mGUI_TRANSFER:
                     //conference transfer .... check if multiple CHANNEL_ORIGINATE updates happened.
                     vANIWorktable[index].CallData.vTransferData[iTranIndex].boolGUIcancel = true;
                     sz = vANIWorktable[index].CallData.vTransferData[iTranIndex].vUUID.size();
                     if (sz > 1) { 
                      boolA = true;
                     } 
                     break;

                case mREFER:
                     //cout << "cancel refer transfer" << endl;
                     break;

                default:
                     
                     // note we will send the message upon hangup .....
                     vANIWorktable[index].CallData.vTransferData[iTranIndex].boolGUIcancel = true;
                     break; 
               }
            }
            else {
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.CallData, objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                         ANI_MESSAGE_367, objANIData.CallData.TransferData.strTransfertoNumber,
                                         objANIData.CallData.TransferData.strTransferTargetUUID);
              //  mBLANK;
            }

            // send to AMI
            if (boolA) {
             //Conference Transfer need to send multiple kills to FSW
             for (unsigned int i = 0; i < sz; i++) {
              objANIData.FreeswitchData.objChannelData.fLoadChannelID(vANIWorktable[index].CallData.vTransferData[iTranIndex].vUUID[i]);
              Queue_AMI_Input(objANIData);
              sem_post(&sem_tAMIFlag);
             }
            }
            else if((boolUSE_POLYCOM_CTI)&&(objANIData.CallData.TransferData.eTransferMethod  == mATTENDED_TRANSFER)) {
             Queue_CTI_Event(objANIData);
            }
            else {
             Queue_AMI_Input(objANIData);
             sem_post(&sem_tAMIFlag);
            }     

            sem_post(&sem_tMutexANIWorkTable);
            break;

       case DIAL_DEST:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-d", 1);}

            if (PositionIsActiveOnCallandNotonHold(objANIData.intWorkStation)) 
             {
              // temp dialout fix .....
          //    objANIData.enumWrkFunction      = WRK_TOGGLE_HOLD; 
          //    Queue_CTI_Event(objANIData); 
          //    objANIData.enumWrkFunction      = WRK_DIAL_OUT;                      
             }
            Queue_CTI_Event(objANIData);
            sem_post(&sem_tMutexANIWorkTable);
            break;



       case CONFERENCE_JOIN_SLA:
         //  //cout << "CONFERENCE_JOIN_SLA" << endl;
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-d", 1);}

            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; }
            if (!objANIData.fLoad_Position(SOFTWARE, ANI, int2str(objANIData.intWorkStation), intPortNum,"IN CONFERENCE_JOIN_SLA")) {sem_post(&sem_tMutexANIWorkTable);continue;}

            //build a conference if only 2 on call
            PreBuild_Conference(index);
            vANIWorktable[index].enumWrkFunction = WRK_BARGE;
            vANIWorktable[index].intWorkStation = objANIData.intWorkStation;
            Queue_CTI_Event(vANIWorktable[index]);


            sem_post(&sem_tMutexANIWorkTable);
            break;

       case TDD_DISPATCHER_SAYS: 
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            objANIData.FreeswitchData =  vANIWorktable[index].FreeswitchData;
            intPosn = objANIData.intWorkStation;
            objANIData.CallData     =  vANIWorktable[index].CallData;
            objANIData.CallData.fLoadPosn(objANIData.intWorkStation);
 
            objANIData.CallData.eJSONEventCode = TDD_DISPATCHER_SAYS;           
            objMessage.fMessage_Create(LOG_CONSOLE_FILE, 382, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.CallData, objANIData.TextData,
                                       objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                       ANI_MESSAGE_382, int2strLZ(objANIData.intWorkStation), 
                                       ASCII_String(objANIData.TextData.TDDdata.strTDDstring.c_str(),
                                       objANIData.TextData.TDDdata.strTDDstring.length()) );
            enQueue_Message(ANI,objMessage);
            
            sz =  vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall.size();
            for (unsigned int i = 0; i < sz; i++) 
             {
              objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall[i];  
              Queue_AMI_Input(objANIData);
              sem_post(&sem_tAMIFlag);
             }
          
            sem_post(&sem_tMutexANIWorkTable);
            continue; 

       case SMS_SEND_TEXT:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            intPosn = objANIData.intWorkStation;
            stringTemp = Create_Message ("%%% To %%%", int2str(intPosn),  vANIWorktable[index].TextData.SMSdata.strFromUser);
         //   stringTemp = Create_Message ("%%% To %%%", int2str(intPosn),  vANIWorktable[index].TDDdata.SMSdata.strFromUser);
            objANIData.CallData     =  vANIWorktable[index].CallData;
            objANIData.CallData.fLoadPosn(objANIData.intWorkStation);
            objANIData.fSetANIfunction(SMS_SEND_TEXT);
            objMessage.fMessage_Create(LOG_CONSOLE_FILE, 382, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.fCallData(), objANIData.TextData,
                                       objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_382s, stringTemp,
                                       ASCII_String(objANIData.TextData.SMSdata.strSMSmessage.c_str(), 
                                       objANIData.TextData.SMSdata.strSMSmessage.length()),"","","","",NORMAL_MSG, NEXT_LINE );
            enQueue_Message(ANI,objMessage); 
            vANIWorktable[index].TextData.SMSdata.strSMSmessage = objANIData.TextData.SMSdata.strSMSmessage;
            vANIWorktable[index].enumANIFunction = SMS_SEND_TEXT;
            Queue_AMI_Input(vANIWorktable[index]);
            sem_post(&sem_tAMIFlag);


            sem_post(&sem_tMutexANIWorkTable);
            continue;
       case MSRP_CONFERENCE_LEAVE:
           // //cout << "MSRP CONFERENCE LEAVE" << endl;
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; }

            vANIWorktable[index].CallData.fConferenceStringRemove(POSITION_CONF, objANIData.intWorkStation);
            vANIWorktable[index].enumANIFunction = SILENT_ENTRY_LOG_OFF; 
            vANIWorktable[index].CallData.fLoadPosn(objANIData.intWorkStation);
            SendANIToMain(SILENT_ENTRY_LOG_OFF, vANIWorktable[index], intPortNum);
 
            sem_post(&sem_tMutexANIWorkTable);
            continue;

       case MSRP_HANGUP:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; }
         //   //cout << "ANI MSRP Hangup -> " << objANIData.CallData.intPosn << endl;
            //check if position is last on call ....
            switch (vANIWorktable[index].CallData.fNumPositionsInConference())
             {
              case 0: case 1:
               stringTemp = Create_Message ("%%% To %%%", int2str(intPosn),  vANIWorktable[index].TextData.SMSdata.strFromUser);   
               
               Queue_AMI_Input(objANIData);
               sem_post(&sem_tAMIFlag);
               break;
              default:
               // same as MSRP_CONFERENCE_LEAVE
               vANIWorktable[index].CallData.fConferenceStringRemove(POSITION_CONF, objANIData.intWorkStation);
               vANIWorktable[index].enumANIFunction = SILENT_ENTRY_LOG_OFF; 
               vANIWorktable[index].CallData.fLoadPosn(objANIData.intWorkStation);
               
               SendANIToMain(SILENT_ENTRY_LOG_OFF, vANIWorktable[index], intPortNum);
               break;
             }

            sem_post(&sem_tMutexANIWorkTable);
            continue;
       case MSRP_ANSWER:

            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}

            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; }
            intPosn = objANIData.intWorkStation;

            //objANIData.CallData     =  vANIWorktable[index].CallData;
            vANIWorktable[index].CallData.fLoadPosn(intPosn);
            objANIData.FreeswitchData.objChannelData.fLoadMSRPPositionChannel(intPosn);
            objANIData.FreeswitchData.objChannelData.iTrunkNumber = vANIWorktable[index].FreeswitchData.objRing_Dial_Data.iTrunkNumber;
            objANIData.FreeswitchData.objChannelData.intTrunkType = vANIWorktable[index].FreeswitchData.objRing_Dial_Data.intTrunkType;

            // put posn into call if not already ... 
            if (vANIWorktable[index].CallData.fLegendInConferenceChannelDataVector(objANIData.FreeswitchData.objChannelData.strConfDisplay) < 0){
             
             vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(objANIData.FreeswitchData.objChannelData);
             vANIWorktable[index].CallData.fConferenceStringAdd(POSITION_CONF, intPosn);           
            }
            else { 
             vANIWorktable[index].CallData.fConferenceStringAdd(POSITION_CONF, intPosn);             
            }
            if (vANIWorktable[index].boolConnect) {
             vANIWorktable[index].enumANIFunction = MSRP_CONFERENCE_JOIN; 
             SendANIToMain(MSRP_CONFERENCE_JOIN, vANIWorktable[index], intPortNum);              
            }
            else {
             // put the position into the call .. show it connected ...
             vANIWorktable[index].boolConnect = true;
             vANIWorktable[index].enumANIFunction = MSRP_CONNECT; 
             SendANIToMain(MSRP_CONNECT, vANIWorktable[index], intPortNum);
            }
            
            // knock all held positions off Call
            RemoveAllOnHoldPostionsFromMSRPCall(index, intPortNum);
            
            sem_post(&sem_tMutexANIWorkTable);

            continue;

            break;
       case TDD_MODE_ON:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            objANIData.FreeswitchData =  vANIWorktable[index].FreeswitchData;
            intPosn = objANIData.intWorkStation;

            objANIData.CallData     =  vANIWorktable[index].CallData;
            objANIData.CallData.fLoadPosn(intPosn);
            

            sz =  vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall.size();
            for (unsigned int i = 0; i < sz; i++) 
             {

              objANIData.FreeswitchData.objChannelData = vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall[i];
              vANIWorktable[index].CallData.fSetTDDModeonConferenceVector(objANIData.FreeswitchData.objChannelData.strChannelID);
              objANIData.CallData.fSetTDDModeonConferenceVector(objANIData.FreeswitchData.objChannelData.strChannelID);
              if(!objANIData.FreeswitchData.objChannelData.boolTDDMode)
               {
                objANIData.fSetANIfunction(TDD_MODE_ON);
                objMessage.fMessage_Create(LOG_CONSOLE_FILE, 391, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.CallData, 
                                           objBLANK_TEXT_DATA,objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                           ANI_MESSAGE_391, objANIData.FreeswitchData.objChannelData.strChannelID);
                enQueue_Message(ANI,objMessage);  
                Queue_AMI_Input(objANIData);
                sem_post(&sem_tAMIFlag);
                vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall[i].boolTDDMode = true;
               }
             }

            sem_post(&sem_tMutexANIWorkTable);
            continue; 
      
       case TDD_MUTE:
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-c", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}
            objANIData.FreeswitchData =  vANIWorktable[index].FreeswitchData;
            intPosn = objANIData.intWorkStation;
            objANIData.CallData     =  vANIWorktable[index].CallData;
            objANIData.TDDdata      =  vANIWorktable[index].TDDdata;
            objANIData.CallData.fLoadPosn(intPosn);
        //    objANIData.FreeswitchData.objChannelData.fLoadChannelName(vANIWorktable[index].FreeswitchData.strAsteriskTrunkChannel);
            Queue_AMI_Input(objANIData);
            sem_post(&sem_tAMIFlag);
            sem_post(&sem_tMutexANIWorkTable);
            break;

       case FORCE_DISCONNECT:
           // //cout << "FORCE DISCONNECT IN ANI" << endl;
            iCount = 0;
            intRC = sem_wait(&sem_tMutexANIWorkTable);
            if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "0 sem_wait@sem_tMutexANIWorkTable in ANI_Thread-d", 1);}
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID); 
            if (index < 0) {sem_post(&sem_tMutexANIWorkTable); continue; /* jump to end  while (!queue_objANIData.empty())*/}  
           
            boolA = (vANIWorktable[index].CallData.fNumPositionsInConference() > 0);
            boolB = (vANIWorktable[index].CallData.fNumDestinationsInConference() > 0);
            boolC = vANIWorktable[index].CallData.fIsInConference(CALLER_CONF);
   
            //send hangup message to all Freeswitch channels
            vANIWorktable[index].fSendHangup();

            // remove all destination members from conference
            while (vANIWorktable[index].CallData.fIsInConference(DEST_CONF))
             {
              if (vANIWorktable[index].CallData.fIsInConference(DEST_CONF, iCount ))
               {
                vANIWorktable[index].CallData.fConferenceStringRemove(DEST_CONF, iCount);
                vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
                objANIData.CallData   = vANIWorktable[index].CallData;
                objANIData.FreeswitchData   = vANIWorktable[index].FreeswitchData; 
                objANIData.fLoad_Position(SOFTWARE, ANI, "0", intPortNum, "00");
                if ((!vANIWorktable[index].CallData.fNumDestinationsInConference())&&(!boolA)&&(!boolC))
                 {objANIData.enumANIFunction = DISCONNECT; SendANIToMain(DISCONNECT, objANIData, intPortNum);}
                else 
                 {objANIData.enumANIFunction = SILENT_ENTRY_LOG_OFF; SendANIToMain(SILENT_ENTRY_LOG_OFF, objANIData, intPortNum);}
               } 
              iCount ++;
             }
           // remove all Positions from Conference
            for (int j = 1; j <= intNUM_WRK_STATIONS; j++)
             {
              if (vANIWorktable[index].CallData.fIsInConference(POSITION_CONF, j ))
               {
                intPosn = j; 
                vANIWorktable[index].CallData.fConferenceStringRemove(j);
                vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
                objANIData.CallData   = vANIWorktable[index].CallData;
                objANIData.FreeswitchData   = vANIWorktable[index].FreeswitchData; 
                objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(j), intPortNum, int2strLZ(j)); 
                if ((!vANIWorktable[index].CallData.fNumPositionsInConference())&&(!boolC))
                 {objANIData.enumANIFunction = DISCONNECT; SendANIToMain(DISCONNECT, objANIData, intPortNum);}
                else 
                 {objANIData.enumANIFunction = SILENT_ENTRY_LOG_OFF; SendANIToMain(SILENT_ENTRY_LOG_OFF, objANIData, intPortNum);}
               }
             }
            // remove caller from Conference
            if (boolC)
             {
              for (int j = 0; j <= intNUM_TRUNKS_INSTALLED; j++)
               {
                if (vANIWorktable[index].CallData.fIsInConference(CALLER_CONF, j ))
                 {
                  vANIWorktable[index].CallData.fConferenceStringRemove(CALLER_CONF, iCount);
                  vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
                  objANIData.CallData   = vANIWorktable[index].CallData;
                  objANIData.FreeswitchData   = vANIWorktable[index].FreeswitchData; 
                  objANIData.fLoad_Position(SOFTWARE, ANI, "0", intPortNum, "00");
                  objANIData.enumANIFunction = DISCONNECT; 
                  SendANIToMain(DISCONNECT, objANIData, intPortNum);

                 }
               }
             }
            if ((!boolA)&&(!boolB)&&(!boolC))
             {
              vANIWorktable[index].enumANIFunction = DISCONNECT;
              vANIWorktable[index].fLoad_Position(SOFTWARE, ANI, "0", intPortNum, "00"); 
              SendANIToMain(DISCONNECT, vANIWorktable[index], intPortNum);

             }

            vANIWorktable[index].CallData.fReleaseConferenceNumber();
            vANIWorktable.erase(vANIWorktable.begin()+index);
            sem_post(&sem_tMutexANIWorkTable);            
            break;

       case MSRP_ON_HOLD:
           // //cout << "ANI MSRP ON HOLD" << endl;
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}         
            intPosn = objANIData.intWorkStation;
            // do not put on hold if 2 or more on call
            if (vANIWorktable[index].CallData.fNumPositionsInConference() > 1 ) {
             vANIWorktable[index].CallData.fConferenceStringRemove(POSITION_CONF, intPosn);
             objANIData.CallData        = vANIWorktable[index].CallData;
             objANIData.enumANIFunction = SILENT_ENTRY_LOG_OFF; 
             objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));
             SendANIToMain(SILENT_ENTRY_LOG_OFF, vANIWorktable[index], intPortNum);             
             break;
             }
            vANIWorktable[index].FreeswitchData.fSetOnHoldinPostionVector(intPosn);
            vANIWorktable[index].CallData.fOnHoldPositionAdd(intPosn);
            objANIData.CallData   = vANIWorktable[index].CallData;
            objANIData.enumANIFunction = HOLD_ON;
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));
            SendANIToMain(HOLD_ON, objANIData, intPortNum);
            break;

       case MSRP_OFF_HOLD:
         //   //cout << "ANI MSRP OFF HOLD" << endl;
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}         
            intPosn = objANIData.intWorkStation;
            //check race condition that workstation is no longer on the call
            if (!vANIWorktable[index].CallData.fIsInConference(POSITION_CONF, intPosn) ) {
            // //cout << "MSRP OFF HOLD RACE ENCOUNTERED" << endl;
             vANIWorktable[index].CallData.fOnHoldPositionRemove(intPosn); // just in case          
             vANIWorktable[index].CallData.fConferenceStringAdd(POSITION_CONF, intPosn);
             objANIData.CallData        = vANIWorktable[index].CallData;
             objANIData.enumANIFunction = MSRP_CONFERENCE_JOIN;
             objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn)); 
             SendANIToMain(MSRP_CONFERENCE_JOIN, objANIData, intPortNum);
             break;           
            }
            vANIWorktable[index].FreeswitchData.fSetOffHoldinPostionVector(intPosn);
            vANIWorktable[index].CallData.fOnHoldPositionRemove(intPosn);
            objANIData.CallData   = vANIWorktable[index].CallData;
            objANIData.enumANIFunction = HOLD_OFF;
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));
            SendANIToMain(HOLD_OFF, objANIData, intPortNum);
            break;
/*
       case TDD_CHAR_RECEIVED:
            //Test Code 
            //cout << "got TDD char" << endl;
            index = IndexofActiveChannelinTable( objANIData.FreeswitchData.objChannelData.strChannelID);
            if (index < 0) {break;}
            for (unsigned int i =0; i < objANIData.FreeswitchData.objChannelData.strTranData.length(); i++)
             {
              objANIData.CallData = vANIWorktable[index].CallData;
              objANIData.TDDdata.strTDDcharacter = objANIData.FreeswitchData.objChannelData.strTranData[i];
              SendANIToMain(TDD_CHAR_RECEIVED, objANIData, intPortNum);
              vANIWorktable[index].TDDdata.fLoadTDDReceived(objANIData.TDDdata.strTDDcharacter);
             }    

            break;
*/
       default: continue; /* jump to end  while (!queue_objANIData.empty())*/
      }// end switch (objANIData.enumANIFunction)
  
 //    if ((objANIData.fCreateAsterisk_XML_Message(strAMIVariable))&&(intPortNum > 0))
 //     {
 //      intRC = Transmit_Data(ANI, intPortNum,(char*) objANIData.stringXMLString.c_str(), objANIData.stringXMLString.length(),0);
 //     }
//     if (intRC) {//cout << "ERROR Transmitting to Asterisk AMI from ANI thread" << endl;} 
                     
    
   
    }// end  while (!queue_objANIData.empty())

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
/*


									Process INBOUND PBX CALL DATA




*/
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

   intFirstLook = RotatePortNumber(intFirstLook);
                       
   while (PortQueueRoundRobin(queueANIPortData, intNUM_ANI_PORTS, intFirstLook, &sem_tMutexANIPortDataQ) != 0 )
    {
     // sequentially select port queue to pop() data (this prevents a DOS attack from slowing down all ports)
     intPortNum = PortQueueRoundRobin(queueANIPortData, intNUM_ANI_PORTS, intFirstLook, &sem_tMutexANIPortDataQ);
     intFirstLook = RotatePortNumber(intFirstLook);
 
     // pop data off the Q
     intRC = sem_wait(&sem_tMutexANIPortDataQ);
     if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIPortDataQ, "ANI    - sem_wait@sem_tMutexANIPortDataQ in ANI_Thread", 1);}
     ANIPortDataRecieved.fClear();
     ANIPortDataRecieved = queueANIPortData[intPortNum].front();
     queueANIPortData[intPortNum].pop();
     sem_post(&sem_tMutexANIPortDataQ);

     intPortNum = ANIPortDataRecieved.intPortNum;   //necessary ?

     // shut down port if string buffer size exceeded .. 
     if(ANIPortDataRecieved.boolStringBufferExceeded)
      {
       ANIPort[intPortNum].fEmergencyPortShutDown(objBLANK_CALL_RECORD, "String Buffer Size > ", ANIPortDataRecieved.intLength);
       continue;
      }

     objANIData.fClear_Record();
     objANIData.enumThreadSending = ANI;
     objANIData.enumANISystem     = ANIPort[intPortNum].UDP_Port.enumANISystem;
     objANIData.intANIPortCallReceived = intPortNum;
     objPortData.fClear();
     objPortData = ANIPort[intPortNum].UDP_Port.fPortData();

     if (!objANIData.fLoad_FREESWITCH_Data(ANIPortDataRecieved.stringDataIn.c_str(), intPortNum))      { continue; /* jump to end  while (PortQueueRoundRobin(queueANIPortData, intNUM_ANI_PORTS, intFirstLook) != 0 )*/}

     if (!Match_Freeswitch_Object_To_Table(&objANIData, ANIPortDataRecieved.stringDataIn, intPortNum)) { continue; /* jump to end  while (PortQueueRoundRobin(queueANIPortData, intNUM_ANI_PORTS, intFirstLook) != 0 )*/}


     // we have a good record .. clear bad char count
     if(ANIPort[intPortNum].intUnexpectedDataCharacterCount)
      {
       objMessage.fMessage_Create(LOG_WARNING, 327, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_327, 
                                  int2strLZ(ANIPort[intPortNum].intUnexpectedDataCharacterCount));
       enQueue_Message(ANI,objMessage);
       ANIPort[intPortNum].intUnexpectedDataCharacterCount = 0;
       ANIPort[intPortNum].boolUnexpectedDataShowMessage   = true;
      }

     // get Timestamp on record
     clock_gettime(CLOCK_REALTIME, &objANIData.timespecTimeRecordReceivedStamp);

     //reset port heartbeat time
     ANIPort[intPortNum].timespecTimeLastHeartbeat = objANIData.timespecTimeRecordReceivedStamp;
     if (ANIPort[intPortNum].boolReducedHeartBeatMode)
      {

       if(!ANIPort[intPortNum].boolReducedHeartBeatModeFirstAlarm)
        {objMessage.fMessage_Create(LOG_WARNING, 326, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_326);}
       else                                                       
        {objMessage.fMessage_Create(LOG_ALARM, 332, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_332);}

       enQueue_Message(ANI,objMessage);
        
       ANIPort[intPortNum].fClearHeartbeatMissingMode();
      }


     // lock table
     intRC = sem_wait(&sem_tMutexANIWorkTable);
     if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in ANI_Thread", 1);}

   
     boolWarningMsg = false;

     // check for proper order and send data to Main
     switch (objANIData.enumANIFunction )
      {
       case CONNECT:           
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}         // may need to push blank record and do the bad call routine for ANI link version TBC ....
      
            vANIWorktable[index].CallData.fConferenceStringAdd(POSITION_CONF, objANIData.CallData.intPosn);
            vANIWorktable[index].boolConnect = true;
            if (vANIWorktable[index].CallData.boolIsOutbound) {
            ////cout << "connect in ANI" << endl;
            }
            vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
            objANIData.CallData   = vANIWorktable[index].CallData;

           
            vANIWorktable[index].boolConnect = true;
            vANIWorktable[index].boolRingBack = false;
            vANIWorktable[index].enumANIFunction = CONNECT;

            if      (vANIWorktable[index].CallData.intCallbackNumber > 0)      {SendANIToMain(CONNECT, vANIWorktable[index], intPortNum);}
            else if (!vANIWorktable[index].CallData.strSIPphoneNumber.empty()) {SendANIToMain(NON_TEN_DIGIT_NUMBER_CONNECT, vANIWorktable[index], intPortNum);}
            else if (!vANIWorktable[index].CallData.stringPANI.empty())        {SendANIToMain(PANI_CONNECT, vANIWorktable[index], intPortNum);}
            else                                                               {SendANIToMain(CONNECT, vANIWorktable[index], intPortNum);}
            
            break;

       case DISCONNECT:
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}

            intPosn = objANIData.CallData.intPosn; 
            objANIData.CallData = vANIWorktable[index].CallData;
            vANIWorktable[index].CallData.fReleaseConferenceNumber();
            vANIWorktable[index].CallData.ConfData.strActiveConferencePositions.clear();         
            objANIData.fLoad_ConferenceData(SOFTWARE, ANI, "0", intPortNum);
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));
            SendANIToMain(DISCONNECT, objANIData,intPortNum);
            vANIWorktable.erase(vANIWorktable.begin()+index);
            break;

       case ABANDONED: case ABANDONED_BUSY:
            // abandoned is the same as a connect .. position number will be 00 in Ani-link we may get a position number ...
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}
            intPosn = objANIData.CallData.intPosn; 
            if(!intPosn)  {
              // inbound
   //           vANIWorktable[index].FreeswitchData.objChannelData.fLoadConferenceDisplayC(vANIWorktable[index].CallData.intTrunk);
    //          vANIWorktable[index].CallData.fLoadCallBackDisplay();
   //           vANIWorktable[index].FreeswitchData.objChannelData.strCallerID = vANIWorktable[index].CallData.strCallBackDisplay;
   //           vANIWorktable[index].FreeswitchData.objChannelData.strCustName = vANIWorktable[index].FreeswitchData.objRing_Dial_Data.strCustName;
              vANIWorktable[index].FreeswitchData.objRing_Dial_Data.fSetChannelHangup(objANIData.FreeswitchData.objChannelData);
              if (!vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.size()) {
                vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objRing_Dial_Data);
              }
             }
            vANIWorktable[index].CallData.ConfData.strActiveConferencePositions.clear();         
            vANIWorktable[index].boolConnect = true;
            vANIWorktable[index].CallData.fReleaseConferenceNumber();

            objANIData.fLoad_ConferenceData(SOFTWARE, ANI, "0", intPortNum);          
            objANIData.CallData   = vANIWorktable[index].CallData;
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn)); 

            SendANIToMain(objANIData.enumANIFunction, objANIData, intPortNum);
            vANIWorktable.erase(vANIWorktable.begin()+index); 
           break;
       case TRANSFER:
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}

            if(!Is_Same_Call(&objANIData, index, intPortNum, ToConnect, stringTRANSFER, &boolWarningMsg))
             {
              SendANIToMain(TRANSFER, objANIData, intPortNum, boolWarningMsg);
              vANIWorktable[index] 			= objANIData;
              vANIWorktable[index].boolRinging          = true;
              vANIWorktable[index].boolConnect          = true;
              break;
             }
                        
            // Previous Transfer is OK ..
            vANIWorktable[index].CallData.TransferData.fLoad_Transfer_Number(SOFTWARE, ANI, objANIData.CallData.TransferData.strTransfertoNumber, intPortNum);

            // normal Transfer ..
            objANIData.CallData.intUniqueCallID   = vANIWorktable[index].CallData.intUniqueCallID;
            objANIData.CallData.intCallbackNumber =  vANIWorktable[index].CallData.intCallbackNumber; 
            objANIData.FreeswitchData =                 vANIWorktable[index].FreeswitchData; 
            SendANIToMain(TRANSFER, objANIData, intPortNum);
   
            break;

       case CONFERENCE_JOIN:
            
            // position number will be afu if no ring or connect .............. TBC ??????????
          //  //cout << "Conference_JOIN a" << endl;
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}

            intPosn = objANIData.CallData.intPosn; 

            if (intPosn) {vANIWorktable[index].CallData.fConferenceStringAdd(POSITION_CONF, intPosn);}
            vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
            objANIData.CallData   = vANIWorktable[index].CallData;
            objANIData.FreeswitchData = vANIWorktable[index].FreeswitchData;
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));

            SendANIToMain(CONFERENCE_JOIN, objANIData, intPortNum);
            //FRED CONFERENCE ???
            break;

       case HOLD_ON:
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}         

            intPosn = objANIData.CallData.intPosn;
            vANIWorktable[index].CallData.fOnHoldPositionAdd(intPosn);
            vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
            objANIData.CallData   = vANIWorktable[index].CallData;
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));
            SendANIToMain(HOLD_ON, objANIData, intPortNum);
           // //cout << "SEND HOLD TO MAIN" << endl;
            break;

       case HOLD_OFF:
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;} 
 
            intPosn = objANIData.CallData.intPosn; 
            vANIWorktable[index].CallData.fOnHoldPositionRemove(intPosn);
            vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
            objANIData.CallData       = vANIWorktable[index].CallData;
            objANIData.FreeswitchData   = vANIWorktable[index].FreeswitchData;
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn));      
            SendANIToMain(HOLD_OFF, objANIData, intPortNum);
            break;

 
          
       case ALARM:
  
              objMessage.fMessage_Create(LOG_WARNING,325, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objANIData.fCallData(), objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_325,
                                         objANIData.stringANIAlarmMessage, objANIData.stringANIAlarmCode );
           
            enQueue_Message(ANI,objMessage);
            enqueue_Main_Input(ANI, objANIData);
            break;
 
       case MSRP_RINGING:
             index = IndexOfNewANITableEntry(objANIData);
             vANIWorktable[index].CallData.fLoad_Conference_Number(int2str(objConferenceList.AssignConferenceNumber()));
             vANIWorktable[index].fSetUniqueCallID(vANIWorktable[index].CallData.intConferenceNumber);
             clock_gettime(CLOCK_REALTIME, &vANIWorktable[index].timespecTimeRecordReceivedStamp);
             vANIWorktable[index].stringRingingTimeStamp = get_time_stamp(vANIWorktable[index].timespecTimeRecordReceivedStamp);  
             vANIWorktable[index].enumANIFunction = RINGING;
             vANIWorktable[index].stringTimeOfEvent      = objMessage.stringTimeStamp;
             vANIWorktable[index].TextData.SMSdata.strSMSmessage += "\r\n"; 
             intTrunk = objANIData.CallData.intTrunk; 
             vANIWorktable[index].CallData.fSetTrunkTypeString();
             vANIWorktable[index].CallData.fConferenceStringAdd(CALLER_CONF, intTrunk);
 //            vANIWorktable[index].FreeswitchData.objRing_Dial_Data.boolConnected = false;
             vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objRing_Dial_Data);
             vANIWorktable[index].FreeswitchData.vectDestChannelsOnCall.push_back(vANIWorktable[index].FreeswitchData.objRing_Dial_Data);
             vANIWorktable[index].timespecRinging = vANIWorktable[index].timespecTimeRecordReceivedStamp;  
             SendANIToMain(MSRP_RINGING, vANIWorktable[index], intPortNum); 
             vANIWorktable[index].boolRinging = true;
             vANIWorktable[index].fSetNenaIds();
             vANIWorktable[index].fSetANIfunction(SMS_MSG_RECEIVED);
             objMessage.fMessage_Create(LOG_CONSOLE_FILE, 352, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[index].fCallData(), objANIData.TextData,
                                        objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_352s, objANIData.TextData.SMSdata.strFromUser,
                                        ASCII_String(objANIData.TextData.SMSdata.strSMSmessage.c_str(), objANIData.TextData.SMSdata.strSMSmessage.length()),"","","","",NORMAL_MSG, NEXT_LINE );
             enQueue_Message(ANI,objMessage); 

             enqueue_Main_Input(ANI, vANIWorktable[index]);
             break;
       case RINGING: 
             ////cout << "ani got ringing" << endl;
             index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);            
             intTrunk = objANIData.CallData.intTrunk;
             // check 911 trunk rotate status
             Trunk_Map.fAddCall(intTrunk);
 
            // check for out of order condition ie a call with same conference number is active
            if (index >=0)
             {
              // the record should not be active ..
              objCallData.fLoadTrunk(intTrunk);
              
              objMessage.fMessage_Create(LOG_WARNING, 334, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_334, stringRINGING);
              enQueue_Message(ANI,objMessage);
              // create and send disconnect to KRN ...
              vANIWorktable[index].enumANIFunction = DISCONNECT;
              SendANIToMain(DISCONNECT, vANIWorktable[index], intPortNum, true);
              vANIWorktable.erase(vANIWorktable.begin()+index);               
             }

            index = IndexOfNewANITableEntry(objANIData);
            objANIData.fLoadCallOriginator(CALLER_CONF, intTrunk);
            objANIData.FreeswitchData.objRing_Dial_Data = objANIData.FreeswitchData.objChannelData;
            objANIData.CallData.fSetTrunkTypeString();           
            objANIData.CallData.fConferenceStringAdd(CALLER_CONF, intTrunk);
            objANIData.fSetNenaIds();
            SendANIToMain(RINGING, objANIData, intPortNum);
            vANIWorktable[index] = objANIData;
            vANIWorktable[index].boolRinging = true;
            clock_gettime(CLOCK_REALTIME, &vANIWorktable[index].timespecRinging);
                   //     vANIWorktable[index].CallData.fDisplay();
            break;

       case ALI_REQUEST:
            // Ignore
            break;

       case TIME_DATE_CHANGE:
            //Ignore
            break;

       case TRANSFER_NUMBER_PROGRAM:
            //Ignore
            break;

       case HEARTBEAT:
            // first heartbeat recieved msg .. ANI does not recieve ACKs so we are using this field
            if( !ANIPort[intPortNum].boolFirstACKRcvd)
             {
              objMessage.fMessage_Create(LOG_INFO,333, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_333);
              enQueue_Message(ANI,objMessage);
              ANIPort[intPortNum].boolFirstACKRcvd = true;
             }
            if(LOG_CONSOLE_FILE*boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC)
             {
              objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC,318, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, 
                                         objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_318, "","","","","","",DEBUG_MSG);
              enQueue_Message(ANI,objMessage);
             }
            break;

       case ALI_REPEAT_SCROLL_BACK:
            //Ignore
            break;

       case SILENT_ENTRY_LOG_OFF:
             // position number will be afu if no ring or connect .............. TBC ??????????
            index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
            if (index < 0) {break;}       

            intPosn = objANIData.CallData.intPosn; 
            vANIWorktable[index].CallData.fConferenceStringRemove(intPosn);
            vANIWorktable[index].UPdate_CallData_Conference_Data_Vector();
            objANIData.CallData   = vANIWorktable[index].CallData;
            objANIData.FreeswitchData   = vANIWorktable[index].FreeswitchData; 
            objANIData.fLoad_Position(SOFTWARE, ANI, int2strLZ(intPosn), intPortNum, int2strLZ(intPosn)); 
            SendANIToMain(SILENT_ENTRY_LOG_OFF, objANIData, intPortNum);
            break;

       case TDD_CHAR_RECEIVED:
            index = IndexofActiveChannelinTable( objANIData.FreeswitchData.objChannelData.strChannelID);
            if (index < 0) {break;}
            for (unsigned int i =0; i < objANIData.FreeswitchData.objChannelData.strTranData.length(); i++)
             {
              objANIData.CallData = vANIWorktable[index].CallData;
              objANIData.TextData.TDDdata.strTDDcharacter = objANIData.FreeswitchData.objChannelData.strTranData[i];
              objANIData.TextData.TextType =  vANIWorktable[index].TextData.TextType = TDD_MESSAGE;
              SendANIToMain(TDD_CHAR_RECEIVED, objANIData, intPortNum);
              vANIWorktable[index].TextData.TDDdata.fLoadTDDReceived(objANIData.TextData.TDDdata.strTDDcharacter);
             }    

            break;


       case TDD_MUTE:
             index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
             if (index < 0) {break;}
          
             vANIWorktable[index].TDDdata.boolTDDmute = objANIData.TDDdata.boolTDDmute;
             objANIData.CallData.intUniqueCallID                          = vANIWorktable[index].CallData.intUniqueCallID;
             SendANIToMain(TDD_MUTE, objANIData, intPortNum);
            break;

       case TDD_MODE_ON:
             index = IndexWithCallUniqueID( objANIData.CallData.stringUniqueCallID);
             if (index < 0) {break;}
             objANIData.CallData.intUniqueCallID                          = vANIWorktable[index].CallData.intUniqueCallID;
             SendANIToMain(TDD_MODE_ON, objANIData, intPortNum);
             break;
       
       case DIAL_DEST:
            index = IndexOfNewANITableEntry(objANIData);
            if (index < 0) {break;}
            objANIData.FreeswitchData.objRing_Dial_Data = objANIData.FreeswitchData.objChannelData;
//objANIData.FreeswitchData.objChannelData.fDisplay();
            objANIData.CallData.fConferenceStringAdd(POSITION_CONF, objANIData.CallData.intPosn);
            objANIData.fLoadCallOriginator(POSITION_CONF, objANIData.CallData.intPosn);
            objANIData.CallData.fConferenceStringAdd(DEST_CONF, objANIData.FreeswitchData.iConfDisplayNumber);
            objANIData.FreeswitchData.objChannelData.fClear();
            objANIData.FreeswitchData.objChannelData.fLoadConferenceDisplayD(objANIData.FreeswitchData.iConfDisplayNumber);
            objANIData.FreeswitchData.objChannelData.strCallerID       = objANIData.FreeswitchData.objRing_Dial_Data.strTranData;
            objANIData.FreeswitchData.objChannelData.strCustName       = CallerNameLookup(objANIData.FreeswitchData.objRing_Dial_Data.strTranData);
            objANIData.FreeswitchData.iConfDisplayNumber++;
            objANIData.FreeswitchData.objChannelData.boolIsOutbound    = true;
            objANIData.FreeswitchData.objChannelData.iGUIlineView = objANIData.FreeswitchData.objRing_Dial_Data.iGUIlineView;

            objANIData.FreeswitchData.objChannelData.iLineNumber       = objANIData.FreeswitchData.objRing_Dial_Data.iLineNumber;
            objANIData.FreeswitchData.objChannelData.intTrunkType      = objANIData.FreeswitchData.objRing_Dial_Data.intTrunkType;
            objANIData.FreeswitchData.objChannelData.iTrunkNumber      = objANIData.FreeswitchData.objRing_Dial_Data.iTrunkNumber;
////cout << "Trunk in ANI = " << objANIData.FreeswitchData.objChannelData.iTrunkNumber << endl;
            objANIData.FreeswitchData.objChannelData.boolConnected     = false;
            intRC = TelephoneEquipment.fIndexofTelephonewithRegistrationName(objANIData.FreeswitchData.objRing_Dial_Data.strTranData);
            if (intRC >=0 ) { 
              // do something ???
            }

            objANIData.CallData.ConfData.vectConferenceChannelData.push_back(objANIData.FreeswitchData.objRing_Dial_Data);
            objANIData.CallData.ConfData.vectConferenceChannelData.push_back(objANIData.FreeswitchData.objChannelData);

            objANIData.UPdate_CallData_Conference_Data_Vector();
            objANIData.CallData.fLoadCallBackDisplay(objANIData.FreeswitchData.objRing_Dial_Data.strTranData,true);

            objANIData.CallData.boolCallBackVerified = true;
            objANIData.boolRinging = true;
            objANIData.CallData.boolIsOutbound = true;           
            objANIData.CallData.fSetTrunkTypeString(true,objANIData.CallData.TransferData.strTransfertoNumber );
            objANIData.CallData.fSetSharedLineFromLineNumberPlusPosition(objANIData.FreeswitchData.objChannelData);
            vANIWorktable[index] = objANIData;
            vANIWorktable[index].CallData.fLoadTrunk(objANIData.FreeswitchData.objChannelData.iTrunkNumber);


           
            SendANIToMain(DIAL_DEST, vANIWorktable[index], intPortNum);
            break;
       default:
            // Error TBC
            SendCodingError("CODING ERROR IN SWITCH in ANI_THREAD");
            
      }// End switch 

     // unlock work table
     sem_post(&sem_tMutexANIWorkTable);

    }// end while (PortQueueRoundRobin(queueANIPortData, intNUM_ANI_PORTS, intFirstLook) != 0 )


  } while (!boolSTOP_EXECUTION);  
// *******************************************************************************************************************************************************
/*
							END MAIN ANI LOOP

*/
//********************************************************************************************************************************************************
 objMessage.fMessage_Create(0, 399, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                            objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_399);
 objMessage.fConsole();
 return NULL;

}// end ANI_Thread()
/*********************************************************************************************************************************************************

							END ANI THREAD


*********************************************************************************************************************************************************/
void Daily_Timed_Event(union sigval union_sigvalArg)
{
 extern Trunk_Sequencing           	Trunk_Map;
 extern timer_t 			timer_tDailyTimerId;
 extern itimerspec			itimerspecDaily;
	
 if(boolSTOP_EXECUTION) {return;}
 Trunk_Map.fCheckTrunkUsagebyDays();
// rearm timer 
 timer_settime( timer_tDailyTimerId, 0, &itimerspecDaily, NULL);		// re-arm timer
}
/****************************************************************************************************
*
* Name:
*  Function: void ANI_Messaging_Monitor_Event(union sigval union_sigvalArg)
*
*
* Description:
*   An event timer within ANI_Thread() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from ANI.  When activated it
*
*   1. Disable timer
*
*   2. Checks for messages on the message Q, retrieve messages until Q is empty and forward them to main.
*
*   3. Checks for time elapsed since last communication on the port .. ANI_Check_Hearbeat()
*
*   4. Checks if a Port Down Reminder should be sent.. Check_Port_Down_Notification()
*
*   5. Check if a Port Restart should be performed .. .Check_Port_Restart()
*
*   6. Check for stale data in input buffer .. .fCheck_Buffer()
*
*   7. Re-arm timer
*
*
* Parameters: 				
*   union_sigvalArg			not used
*
* Variables:
*   ANI                                 Global  - <header.h> threadorPorttype enumeration
*   ANIPort                             Global  - <ExperientCommPort> object array
*   CLOCK_REALTIME                      Global  - <ctime> constant
*   i                                   Local   - general purpose integer
*   intRC				Local   - return code
*   .intMessageCode                     Global  - <MessageClass> member
*   intNUM_ANI_PORTS                    Global  - <globals.h>
*   itimerspecANIHealthMonitorDelay     Local   - <ctime> struct which contains timing data that for the timer
*   itimerspecDisableTimer              Global  - <ctime> struct which contains data that disables the timer
*   objMessage                          Local   - <MessageClass> object
*   queue_objANIMessageQ                Global  - <queue><MessageClass> queue of ANI messages
*   sem_tMutexANIMessageQ               Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   sem_tMutexMainMessageQ		Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   timer_tANIHealthMonitorTimerId      Global  - <ctime> timer id for this function
*   .UDP_Port                           Global  - <ExperientCommPort> <ExperientUDPPort> object
*                                                                       
* Functions:
*   ANI_Check_Hearbeat()                Local                                   (void)
*   Check_Port_Down_Notification()      Global  <globalfunctions.h>             (void)
*   .fCheck_Buffer()                    Global  <ExperientCommPort>             (void)  
*   .Check_Port_Restart()               Global  <ExperientCommPort>             (void)   
*   .empty()				Library <queue>                         (bool)
*   .front()				Library <queue>                         (*this)                        
*   .pop()				Library <queue>                         (void)
*   .push()				Library <queue>                         (void)
*   Semaphore_Error()                   Global  <globalfunctions.h>             (void)           
*   sem_wait()             		Library <semaphore.h>                   (int)
*   sem_post()				Library <semaphore.h>                   (int)
*   timer_settime()                     Library <ctime>                         (int)                         
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *ANI_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 Thread_Data            	ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="ANI Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ani.cpp::ANI_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

/*
 //Disable Timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tANIHealthMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}
*/

do
{

 experient_nanosleep(intANI_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in ANI_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexANIMessageQ);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIMessageQ, "sem_wait@sem_tMutexANIMessageQ in ANI_Messaging_Monitor_Event", 1);}

 while (!queue_objANIMessageQ.empty())
  {
    objMessage.fClearMsg();
    objMessage = queue_objANIMessageQ.front();
    queue_objANIMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 300:						
       // ANI Thread Created
       break; 
      case 301:						
       // "[WARNING] ANI Thread Failed to Initialize, System Rebooting" ... should not pass thru here handled in krn
       //cout << "coding error in ANI monitor\n";
       break;
      case 302:						
       // "[DEBUG] ANI Health Monitor Timer Event Initialized"
       break;
      case 303:
       // "[WARNING] ANI Thread Signalling Semaphore failed to Lock"						
       break;
      case 304:						
       // "[DEBUG] ANI %%% - Port Initialized"
       break;
      case 305:						
       // "[DEBUG] ANI %%% Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 306:						
       //  "ANI %%% < Connect    %%%"
       break;
      case 307:
       // "ANI %%% < Disconnect %%%"						
       break;
      case 308:						
       // "ANI %%% > Transfer   %%% Transfer=%%%"
       break;
      case 309:
       // "ANI %%% < Conference %%%"						
       break;
      case 310:						
       // "ANI %%% < On Hold    %%%"
       break;
      case 311:						
       // "ANI %%% < Off Hold   %%%"
       break;
      case 312:						
       // "ANI %%% < Abandoned  %%%"
       break;
      case 313:						
       // "[ALARM] ANI %%% < No Correlation %%%"
       break;
      case 314:                                           

       break;
      case 315:                                           
       // "[ALARM] ANI %%% > %%% (Code=%%%)"
       break;
      case 316:                                           
       // "ANI %%% < Ringing    %%%"
       break;
      case 317:						
       // "[WARNING] ANI %%% < 60 Seconds No Answer %%%"
       break;
      case 318:						
       // "ANI %%% < Heartbeat Received"
       break;
      case 319:						
       //not used
       break;
      case 320:						
       // "[WARNING] ANI %%% No Valid ANI Transmissions Received in Last %%% Seconds, Ping (%%%)"
       break;
      case 321:                                           
       //port down alarm
       break;
      case 322:                                           
       //port down reminder
       break;
      case 323:                                           
       // "[WARNING] ANI %%% < Invalid Data Received for %%%, Data=\"%%%\""
       break;
      case 324:                                           
       // "[WARNING] ANI %%% < Invalid Data Received, /START OF SAMPLE/%%%/END OF SAMPLE/"
       break;
      case 325:                                           
       // "[WARNING] ANI %%% < %%% (Code=%%%)"
       break; 
      case 326:                                           
       // "[WARNING] ANI %%% - Port Restored"
       break;
      case 327:                                           
       // "[WARNING] ANI %%% - Unexpected Character Count =%%%"
       break;  
      case 328:                                           
       // "[ALARM] %%% - Port Data Reception Inhibited for %%% String Buffer Size > %%%"
       break;
      case 329:                                           
       // "[WARNING] %%% - UDP Error (Code=%%%, MSG=%%%)"
       break;
      case 330:                                           
       // "[WARNING] %%% - Port Restart Failed (Code=%%%)"
       break;
      case 331:                                           
       // "[WARNING] %%% - Port Data Reception Restored"
       break;
      case 332:                                           
       // "[ALARM] ANI %%% - Port Restored"
       break;
      case 333:                                           
       // "[INFO] ANI %%% - First Heartbeat Received"
       break;
      case 334:                                           
       // "[WARNING] ANI %%% * %%% - DATA Out of Order, No Disconnect Message Received"
       break;
      case 335:                                           
       // "[WARNING] ANI %%% * %%% - DATA Out of Order, No Ringing Message Received"
       break;
      case 336:                                           
       // "[WARNING] ANI %%% * %%% - pANI Change, Old pANI=%%%; New pANI=%%%)"
       break;
      case 337:                                           
       // "[WARNING] ANI %%% * %%% - DATA Out of Order, No Connect Message Received"
       break;
      case 338:                                           
       // "[WARNING] ANI %%% * %%% - Position Number Change, Old Pos=%%%, New Pos=%%% (Trunk=%%%, Callback=%%%)"
       break;
      case 339:                                           
       // "[WARNING] ANI %%% * %%% - Phone Number Change, Old Callback=%%% (Trunk=%%%, New Callback=%%%)"
       break;
      case 340:                                           
       // "[WARNING] ANI %%% * %%% - Previous Connect Recieved"
       break;
      case 341:                                           
       // unused
       break; 
      case 342:                                           
       // "ANI %%% < Silent Entry LogOff %%%"
       break;
      case 343:                                           
       // "[WARNING] ANI %%% < Ringing Event Created %%%"
       break; 
      case 344:                                           
       // "[WARNING] ANI %%% < Connect Event Created %%%"
       break; 
      case 345:                                           
       // "[WARNING] ANI %%% < Disconnect Event Created %%%"
       break;
      case 346:
       // "[WARNING] ANI %%% * %%% - Previous On Hold Recieved"
       break; 
      case 347:
       // "[WARNING] %%% %%% %%% - Buffer Cleared Length = %%%"
       break;         
      case 348:
       // "[WARNING] ANI %%% - Invalid Trunk Map, Index=%%% Data=%%%" UNUSED FOR NOW ....
       break; 
      case 349:
       // "[WARNING] ANI %%% - Unable to Parse XML Data, Error=%%%"
       break;         
      case 350:
       // "[WARNING] ANI %%% - Unable to Parse XML Data, Missing %%% Attribute"
       break; 
      case 351:
       // ">TDD Character \"%%%\" Received"
       break;
       case 352:
       // ">TDD Caller Says: \"%%%\" Received"
       break;  
       
      case 353:
       //  "[INFO] ANI %%% - NPD 8 encountered; NPD set to zero"
       break; 
      case 354:
       //  unused ...........
       break;
      case 355:
       // "[WARNING] Unable to set CPU affinity for %%% Thread"
       break;
      case 356:
       // Discarded Packet Counter Rollover
       break;
      case 357:
       // "[WARNING] %%% - Incoming Network Packets Discarded over Past 24 Hours = %%%" 
       break;
      case 358:
       // "ANI %%% < Raw Data Received from IP=%%% (Data Follows) %%%" 
       break;
      case 359:
       // "%%% > Data sent to IP=%%% (Data Follows)"" 
       break;
      case 360:
       // ">[WARNING] Unable to Match Asterisk Data UniqueId=%%% to Current Call (Data Follows) %%%" 
       break;
      case 361:
       // ">TDD Conversation Record (Data Follows)"
       break;
      case 362:
       // ">TDD MUTE ON"
       break; 
      case 363:
       // ">TDD MUTE OFF"
       break; 
      case 364:
       // ">Asterisk AMI Login: %%%"
       break;
      case 365:
       // "<Transfer from GUI; (Destination=%%%)"
       break; 
      case 366:
       //  ">Dial Out/Transfer Terminated; Extension=%%%"
       break; 
      case 367:
       // "<Transfer from GUI Cancelled; (Destination=%%%)"
       break;
      case 368:
       // ">[WARNING] Transfer Cancelled, ext: %%% is currently connected "   
       break;
      case 369:
       // ">External Transfer Connected; Number=%%%" 
       break; 
      case 370:
       // ">External Call Connected; Number=%%%" 
       break; 
      case 371:
       // ">External Call Disconnected; Number=%%%"  
       break; 
      case 372:
       //">External Call Dialed; Number=%%%"   
       break; 
      case 373:
       //  ">External Call On Hold; Number=%%%"
       break;    
      case 374:
       // ">External Call Off Hold; Number=%%%"   
       break;   
      case 375:
       // "> %%% ; Status=%%%"  
       break;   
      case 376:
       // ">[WARNING] Dial failed to %%%, No Outbound %%% Route Available"
       break; 
      case 377:
       // "-[WARNING] 911 Trunk %%% Rotate check failed thru %%% calls"
       break;     
      case 378:
       //  "-[DEBUG] Trunk Assignment Listen Thread Created, PID %%% ThreadSelf %%%"
       break;     
      case 379:
       //  "-[INFO] %%%; Communication Recieved on Unmonitored Device"
       //  "-[INFO] %%%; Communication Ended on Unmonitored Device"
       break; 
      case 380:
       //   "-[DEBUG] Trunk Assignment Response Thread Created, PID %%% ThreadSelf %%%"
       break; 
      case 381:
       //   "-[WARNING] Trunk Assignment Response Thread Failed to Initialize, System Rebooting"
       break; 
      case 382:
       //   ">[TDD] Dispatcher %%% says: %%%"
       break; 
      case 383:
       //  "-[WARNING] AGI Error Server: %%%; Message: %%%"
       break; 
      case 384:
       //   "-[WARNING] AGI Fail Server: %%%; Message: %%%; Code: %%%"
       break; 
      case 385:
       //    "-[WARNING] AGI Timeout Server: %%%; MessageID: %%%; Channel: %%%; CallerId: %%%; CustName: %%%"
       break; 
      case 386:
       //    "-[ALARM] %%% has Failed to Register for %%%; (Ping %%%)"
       break; 
      case 387:
       //  "-[WARNING] Reminder; %%% at IP:%%% has Failed to Register for %%%; (Ping %%%)"
       break; 
      case 388:
       //  "-[ALARM] %%%; Communication Restored"
       break;
      case 389:
       //  "-[WARNING] Unable to assign Conference Number"
       break;
      case 390:
       // "[WARNING] ANI - Port Listen Thread Failed to Initialize, System Rebooting"
       break;
      case 391:
       // ">TDD MODE ON"
       break;
      case 392:
       //">Blind Transfer from Phone Dialed; (Destination=%%%)"
       break;
      case 393:
       //">">Ring Back From Failed Blind Transfer; (Destination=%%%)"
       break;
      case 394:
       //"-[WARNING] 911 Trunk %%% Rotate Cleared with Call OK"
       break;
      case 395:
       break;
      case 396:
       break;
      case 397:
       // TBC
       break;
      case 398:
       // "-[WARNING] Mutex Semaphore Error (Code=%%%, MSG=%%%)"
       break;
      case 399:                                           
       // thread complete
      
       break;
      
      case 999:
       // test area..................................
             
       break;	
      default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in ANI Monitor\n";
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock ANI Message Q then Main Message Q
 sem_post(&sem_tMutexANIMessageQ);
 sem_post(&sem_tMutexMainMessageQ);

 if(boolIsMaster)
  {
   ANI_Check_Hearbeat();
   Check_Port_Down_Notification(ANI, intNUM_ANI_PORTS, ANIPort);
  }
 else            
  {
   for (int i = 1; i <= intNUM_ANI_PORTS; i++) 
    {
     clock_gettime(CLOCK_REALTIME, &ANIPort[i].timespecTimeLastHeartbeat);
     ANIPort[i].fClearHeartbeatMissingMode();
    }
  }

 //check for TDD Sentence Completion
 Check_TDD_Caller_Sentences();

 //check for port restart condition
 for (int i = 1; i <= intNUM_ANI_PORTS; i++) {ANIPort[i].Check_Port_Restart();}

 //check for stale Data in Buffer(s)
 for (int i = 1; i <= intNUM_ANI_PORTS; i++) {ANIPort[i].UDP_Port.fCheck_Buffer();}

 //check Phone Monitoring
 TelephoneEquipment.fCheckTimeouts();

 // check for stale table data
// Check_Stale_Vector_Data();      

 // check for conferences assigned Limit
// Check_Conferences_Assigned_Limit();

/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tANIHealthMonitorTimerId, 0, &itimerspecANIHealthMonitorDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);
 
 objMessage.fMessage_Create(0, 397, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_397);
 objMessage.fConsole();

return NULL;	

}// end ANI_Messaging_Monitor_Event()

/****************************************************************************************************
*
* Name:
*  Function: void *ANI_Port_Listen_Thread(void *voidArg)
*
*
* Description:
*   An thread created within ANI_Thread() 
*
*
* Details:
*   This function is an implemented thread from within Ali_Thread.  When activated it
*   runs the Do_Events() function from the ipworks library until the boolean
*   boolSTOP_EXECUTION is true.  This function establishes  UDP port listening.
*
*
* Parameters: 				
*   voidArg			        an recast integer representing the port number
*					
*
* Variables:
*   ANI                                 Global  - <header.h> threadorPorttype enumeration member
*   ANIPort                             Global  - <ExperientCommPort> object
*   ANI_MESSAGE_305                     Global  - <defines.h> Listen Thread Created output message
*   boolDEBUG                           Global  - <globals.h> show debug messages
*   boolReadyToShutDown                 Global  - <ExperientCommPort> member
*   boolSTOP_EXECUTION                  Global  - <globals.h>
*   intPortNum				Local   - port number to listen to
*   LOG_CONSOLE_FILE                    Global  - <defines.h> Log thread constant
*   objMessage                          Local   - <MessageClass> object
*   .UDP_Port                           Global  - <ExperientCommPort> <ExperientUDPPort> object
*                                                                          
* Functions:  
*   .DoEvents()                         Library <ipworks><UDPPort>                      (int)
*   enQueue_Message()                   Global  <globalfunctions.h>                     (void)
*   .fMessage_Create()                  Global  <MessageClass>                          (void)
*   getpid()				Library <unistd.h>                              (pid_t)            
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   pthread_self()			Library <pthread.h>                             (pthread_t)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *ANI_Port_Listen_Thread(void *voidArg)
{
 param_t* p          = (param_t*) voidArg;
 int      intPortNum = p->intPassedIn;
 int                            intRC;
 MessageClass                   objMessage;
 Port_Data                      objPortData;
 Thread_Data                    ThreadDataObj; 
     
 extern vector <Thread_Data> 			ThreadData;
 extern Initialize_Global_Variables             INIT_VARS;

 ThreadDataObj.strThreadName ="ANI Port "+ int2strLZ(intPortNum);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ani.cpp::ANI_Port_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 objPortData.fLoadPortData(ANI, intPortNum);
 objMessage.fMessage_Create(0, 305, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                            ANI_MESSAGE_305,
                            int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 while ((!boolSTOP_EXECUTION)&&(!INIT_VARS.ANIinitVariable.boolRestartANIports)){
  experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
  ANIPort[intPortNum].UDP_Port.DoEvents();
 }

 objMessage.fMessage_Create(0, 305, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            ANI_MESSAGE_305b,
                            int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 ANIPort[intPortNum].UDP_Port.SetActive(false);
 ANIPort[intPortNum].boolReadyToShutDown = true;
 return NULL;
								
}// end ANI_Port_Listen_Thread


/****************************************************************************************************
*
* Name:
*  Function: void ANI_Check_Hearbeat()
*
*
* Description:
*   A function called within ANI_Messaging_Monitor_Event() 
*
*
* Details:
*   This function checks that there is no period of inactivity of greater
*   intHeartbeatInterval seconds.  If there is, the function activates the
*   heartbeat missing mode (prevents multiple messages until the port is
*   back up) and generates a warning message with a ping check of the port.
*
*
* Parameters: 				
*   none			        
*					
*
* Variables:
*   ANI                                 Global  - <header.h> threadorPorttype enumeration member
*   ANI_MESSAGE_320                     Global  - <defines.h> No Valid ANI Transmissions Received in Last %%% Seconds message
*   ANIPort[]                           Global  - <ExperientCommPort> object array
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member
*   CLOCK_REALTIME                      Library - <ctime> real time clock constant
*   doubleTimeDiff                      Local   - time difference
*   i                                   Local   - int counter
*   .intHeartbeatInterval               Global  - <ExperientCommPort> member
*   intNUM_ANI_PORTS                    Global  - <globals.h> int Number of ANI Ports
*   LOG_WARNING                         Global  - <defines.h>
*   objMessage                          Local   - <MessageClass> object
*   .timespecTimeLastHeartbeat          Global  - <ExperientCommPort> member
*   timespecTimeNow                     Local   - <ctime> <timespec> structure
*   UDP_Port                            Local   - <ExperientUDPPort> object
*                                                                          
* Functions:  
*   clock_gettime()			Library <ctime>                         (int)
*   enQueue_Message()                   Global  <globalfunctions.h>             (void)
*   .fMessage_Create()                  Global  <MessageClass>                  (void)
*   .fPingStatusString()                Global  <ExperientUDPPort>             (string)
*   .fSetHeartbeatMissingMode()         Global  <ExperientCommPort>             (void)
*   int2str()                           Global  <globalfunctions.h>             (string)
*   int2strLZ()                         Global  <globalfunctions.h>             (string)
*   time_difference()                   Global  <globalfunctions.h>             (long double)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void ANI_Check_Hearbeat()
{
 struct timespec        timespecTimeNow;
 long double            doubleTimeDiff;
 MessageClass           objMessage;
 Port_Data              objPortData;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

  for (int i = 1; i <= intNUM_ANI_PORTS; i++)
   {
    doubleTimeDiff = time_difference(timespecTimeNow, ANIPort[i].timespecTimeLastHeartbeat);
    if (doubleTimeDiff > ANIPort[i].intHeartbeatInterval+ 5)
     {
      objPortData.fClear(); objPortData.fLoadPortData(ANI, i);
 
      if (!ANIPort[i].boolReducedHeartBeatMode)
       {      
        ANIPort[i].fSetHeartbeatMissingMode();
        objMessage.fMessage_Create(LOG_WARNING,320, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                   ANI_MESSAGE_320, int2str(int(doubleTimeDiff)),
                                   ANIPort[i].UDP_Port.fPingStatusString()   ); 
        enQueue_Message(ANI,objMessage);

       }// end if (!boolReducedHeartBeatMode)

     }// end if (doubleTimeDiff > ANIPort[i].intHeartbeatInterval+ 1)

   }// end for (int i = 1; i <= intNUM_ANI_PORTS; i++)

// sem_post(&sem_tMutexANIPort);

}// end void ANI_Check_Hearbeat()

/****************************************************************************************************
*
* Name:
*  Function: unsigned long long int No_Ring(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg, bool boolConnect)
*
*
* Description:
*  Utility function called by function Is_Same_Call()
*
*
* Details:
*   This function checks if the Ringing event has been logged in the MAIN worktable , if it hasn't
*   then an error is generated and the ringing event is recreated into the worktable and a new callid is 
*   generated. 
.   If the flag boolConnect was set (ie a connect had occured previously) then the out of order flag is set (boolConnectOutofOrder)
*   and the worktable is recreated to the connect event.
*
* Parameters:
*   boolConnect                                 bool
*   intIndex                                    integer 
*   intPortNum                                  integer 				
*   objData                                     <ExperientDataClass> object
*   stringArg                                   <cstring>                             
*
* Variables:
*   ANI                                 Global  <header.h> threadorPorttype enumeration member
*   ANI_MESSAGE_335                     Global  <defines.h> DATA Out of Order, No Ringing Message Received error message
*   .boolConnectOutofOrder              Global  <ExperientDataClass> member
*   .boolRecordRebuiltFlag              Global  <ExperientDataClass> member 
*   .boolRinging                        Global  <ExperientDataClass> member 
*   CONNECT                             Global  <header.h> ani_functions enumeration member
*   .enumANIFunction                    Global  <ExperientDataClass> member
*   .intCallbackNumber                  Global  <ExperientDataClass> member
*   .intPosn                            Global  <ExperientDataClass> member
*   .intUniqueCallID                    Global  <ExperientDataClass> member
*   LOG_WARNING                         Global  <defines.h> log code
*   MAIN                                Global  <header.h> threadorPorttype enumeration member
*   objData                             Local   <ExperientDataClass> object
*   objMessage                          Local   <MessageClass> object
*   RINGING                             Global  <header.h> ani_functions enumeration member
*   stringPosn                          Global  <ExperientDataClass> member .. position number
*   stringTrunk                         Global  <ExperientDataClass> member .. trunk number
*   vANIWorktable                       Global  <ExperientDataClass> object array .. the main work table    
*                                                              
* Functions:
*   enQueue_Message()                   Global  <globalfunctions.h>                     (void)
*   .fMessage_Create()                  Global  <MessageClass>                          (void)
*   .fSetUniqueCallID()                 Global  <ExperientDataClass>                    (unsigned long long int)
*   int2strLZ()                         Global  <globalfuntions.h>                      (string) 
*   SendANIToMain()                   Local                                           (void)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 

unsigned long long int  No_Ring(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg, bool boolConnect)
{
 MessageClass                       objMessage;
 Port_Data                          objPortData;
 extern vector <ExperientDataClass> vANIWorktable;

 
 objPortData.fLoadPortData(ANI, intPortNum); 

 if (!vANIWorktable[intIndex].boolRinging)
  {
   if(boolConnect) {vANIWorktable[intIndex].boolConnectOutofOrder = true;}
   objMessage.fMessage_Create(LOG_WARNING, 335, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intIndex].fCallData(), 
                              objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_335, stringArg);
   enQueue_Message(ANI,objMessage);
//   objData.fSetUniqueCallID(vANIWorktable[intIndex].CallData.intConferenceNumber);
   objData.enumANIFunction = RINGING;
   objData.boolRecordRebuiltFlag = true;
   SendANIToMain(RINGING, objData, intPortNum, true);
   vANIWorktable[intIndex].boolRinging = true;

   if(!boolConnect){return objData.CallData.intUniqueCallID;}
   objData.enumANIFunction = CONNECT;
   SendANIToMain(CONNECT, objData, intPortNum, true);
   vANIWorktable[intIndex].boolConnect = true;
   return objData.CallData.intUniqueCallID;
  }
 return 0;
}// end No_Ring(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg)

/****************************************************************************************************
*
* Name:
*  Function: void SendANIToMain(ani_functions ANI_Function, ExperientDataClass objData,  int intPortNum, bool boolWarningMsg)
*
*
* Description:
*  Utility function 
*
*
* Details:
*   This function places an ExperientDataClass object onto the Main input queue for processing and generates a log message in one of three
*   formats depending on the type of ANI_Function and if the boolWarningMsg was set.
*
* Parameters:
*   ANI_Function                                <ani_functions> enumeraton 
*   boolWarningMsg                              bool
*   intIndex                                    integer 
*   intPortNum                                  integer 				
*   objData                                     <ExperientDataClass> object
*   stringArg                                   <cstring>                             
*
* Variables:
*   ABANDONED                           Global  <header.h> ani_functions enumeration member                         
*   ALLFIELDS                           Local   <MessageFormat> enumeration member
*   ANI                                 Global  <header.h> threadorPorttype enumeration member
*   ANI_MESSAGE_306                     Global  <defines.h> Connect message
*   ANI_MESSAGE_307                     Global  <defines.h> Disconnect message
*   ANI_MESSAGE_308                     Global  <defines.h> Transfer message
*   ANI_MESSAGE_309                     Global  <defines.h> Conference message
*   ANI_MESSAGE_310                     Global  <defines.h> On Hold message
*   ANI_MESSAGE_311                     Global  <defines.h> Off Hold message
*   ANI_MESSAGE_312                     Global  <defines.h> Abandoned message
*   ANI_MESSAGE_313                     Global  <defines.h> No Correlation message
*   ANI_MESSAGE_316                     Global  <defines.h> Ringing message
*   ANI_MESSAGE_317                     Global  <defines.h> 60 Seconds No Answer message
*   ANI_MESSAGE_342                     Global  <defines.h> Silent Entry LogOff message
*   ANI_MESSAGE_343                     Global  <defines.h> DATA Out of Order, No Ringing Message Received error message
*   ANI_MESSAGE_344                     Global  <defines.h> Connect Event Created error message
*   ANI_MESSAGE_345                     Global  <defines.h> Disconnect Event Created error message
*   CONNECT                             Global  <header.h> ani_functions enumeration member
*   enumMessageFormat                   Local   <MessageFormat>
*   .intCallbackNumber                  Global  <ExperientDataClass> member
*   intLogCode                          Local   integer for log codes
*   intMsgCode                          Local   integer for message code
*   .intPosn                            Global  <ExperientDataClass> member
*   .intTrunk                           Global  <ExperientDataClass> member
*   .intUniqueCallID                    Global  <ExperientDataClass> member
*   LOG_ALARM                           Global  <defines.h>
*   LOG_CONSOLE_FILE                    Global  <defines.h>
*   LOG_WARNING                         Global  <defines.h>
*   NOPOSITION                          Local   <MessageFormat> enumeration member
*   objMessage                          Local   <MessageClass> object
*   PLUSTRANSFER                        Local   <MessageFormat> enumeration member
*   RINGING                             Global  <header.h> ani_functions enumeration member
*   stringMsg                      Local   <cstring>
*   stringPANI                          Local   <cstring>
*   .stringPANI                         Global  <ExperientDataClass> member
*   .stringTimeOfEvent                  Global  <ExperientDataClass> member
*   .stringTimeStamp                    Global  <MessageClass> member
*    
*                                                              
* Functions:
*   enqueue_Main_Input()                Global  <globalfunctions.h>                     (void)
*   enQueue_Message()                   Global  <globalfunctions.h>                     (void)
*   .fMessage_Create()                  Global  <MessageClass>                          (void)
*   int2strLZ()                         Global  <globalfuntions.h>                      (string) 
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void SendANIToMain(ani_functions ANI_Function, ExperientDataClass objData, int intPortNum, bool boolWarningMsg)
{
 MessageClass                   objMessage;
 string                         stringMsg;
 unsigned char                  chTemp;
 int                            intLogCode = LOG_CONSOLE_FILE;
 int                            intMsgCode;
 Port_Data			objPortData;
 Call_Data                      objCallData;
 struct timespec                timespecTimeNow;
 enum                           MessageFormat{NOMESSAGE,NOPOSITION, ALLFIELDS, PLUSTRANSFER};
 MessageFormat                  enumMessageFormat;
 
 objPortData.fLoadPortData(ANI, intPortNum);
 objData.fSetANIfunction( ANI_Function);            
 switch (ANI_Function)
  {
   case RINGING: case MSRP_RINGING:
        if(boolWarningMsg)
         {
          intLogCode            = LOG_WARNING;
          intMsgCode            = 343;
          stringMsg             = ANI_MESSAGE_343;
         }
        else
         {
          intMsgCode = 316; 
          if (objData.CallData.strSIPphoneNumber.empty()) {stringMsg = ANI_MESSAGE_316;}
          else                                            {stringMsg = Create_Message(ANI_MESSAGE_316v,objData.CallData.strSIPphoneNumber);}
         }
        enumMessageFormat = NOPOSITION;
        break;
 
   case CONNECT: case MSRP_CONNECT:
        if(boolWarningMsg)
         {
          intLogCode            = LOG_WARNING;
          intMsgCode            = 344;
          stringMsg             = ANI_MESSAGE_344;
         }
        else {intMsgCode = 306; stringMsg = ANI_MESSAGE_306;}
        
        enumMessageFormat = ALLFIELDS;
        break;

   case NON_TEN_DIGIT_NUMBER_CONNECT:
        intMsgCode              = 370;
        if (objData.CallData.strSIPphoneNumber.empty()) {stringMsg = ANI_MESSAGE_370;}
        else                                            {stringMsg = Create_Message(ANI_MESSAGE_306a,objData.CallData.strSIPphoneNumber);intMsgCode = 306;}
        enumMessageFormat       = PLUSTRANSFER;
        break;

   case PANI_CONNECT:
        intMsgCode              = 306;
        stringMsg               = ANI_MESSAGE_306;
        enumMessageFormat       = ALLFIELDS;
        break;

   case ABANDONED:
        intMsgCode              = 312; 
        stringMsg               = ANI_MESSAGE_312;
        enumMessageFormat       = ALLFIELDS;
        break;

   case ABANDONED_BUSY:
        intMsgCode              = 312; 
        stringMsg               = ANI_MESSAGE_312b;
        enumMessageFormat       = ALLFIELDS;
        break;
       
   case DISCONNECT:
        if(boolWarningMsg)
         {
          intLogCode            = LOG_WARNING;
          intMsgCode            = 345;
          stringMsg             = ANI_MESSAGE_345;
         }
        else {intMsgCode = 307; stringMsg = ANI_MESSAGE_307;}

        enumMessageFormat = ALLFIELDS;
        break;

//   case NO_CORRELATION:
//        intMsgCode              = 313;
//        intLogCode              = LOG_ALARM; 
 //       stringMsg               = ANI_MESSAGE_313;
 //       enumMessageFormat       = NOPOSITION;
 //       break;

   case TRANSFER:
        intMsgCode              = 308;
        stringMsg               = ANI_MESSAGE_308;
        enumMessageFormat       = PLUSTRANSFER;
        objCallData             = objData.fCallData();
        if (objData.CallData.TransferData.strTransfertoNumber.length() > 4) 
         {objData.fLoad_Position(SOFTWARE, ANI, "00",  intPortNum, "00");}
        
        break;

   case CONFERENCE: case CONFERENCE_JOIN: case MSRP_CONFERENCE_JOIN:
        intMsgCode              = 309;
        stringMsg               = ANI_MESSAGE_309;
        enumMessageFormat       = ALLFIELDS;
        break;

   case HOLD_ON:
        intMsgCode              = 310;
        stringMsg               = ANI_MESSAGE_310;
        enumMessageFormat       = ALLFIELDS;
        break;
        
   case HOLD_OFF:
        intMsgCode              = 311;
        stringMsg               = ANI_MESSAGE_311;
        enumMessageFormat       = ALLFIELDS;
        break;

//   case SIXTY_SEC_NO_ANSWER:
//        intMsgCode              = 317;
//        stringMsg               = ANI_MESSAGE_317;
 //       intLogCode              = LOG_WARNING;
 //       enumMessageFormat       = NOPOSITION;
 //       break;

   case SILENT_ENTRY_LOG_OFF:
        intMsgCode              = 342;
        if (!objData.CallData.intPosn) 
         {stringMsg = ANI_MESSAGE_342; stringMsg+= "exten: " + objData.CallData.TransferData.strTransfertoNumber;}
        else                       {stringMsg = ANI_MESSAGE_342;}
        enumMessageFormat       = ALLFIELDS;
        break;
   case TDD_CHAR_RECEIVED:
        intMsgCode              = 351;
        chTemp = objData.TextData.TDDdata.strTDDcharacter[0];
        stringMsg = Create_Message(ANI_MESSAGE_351, ASCII_Convert(chTemp));
 //       if(boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC){enumMessageFormat       = ALLFIELDS;}
 //       else                                   {enumMessageFormat       = NOMESSAGE;}
        enumMessageFormat       = ALLFIELDS;
        break;
   case TDD_CALLER_SAYS:
        intMsgCode              = 352;
        stringMsg = Create_Message(ANI_MESSAGE_352, objData.TextData.TDDdata.strTDDstring);
 //       if(boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC){enumMessageFormat       = ALLFIELDS;}
 //       else                                   {enumMessageFormat       = NOMESSAGE;}
        enumMessageFormat       = ALLFIELDS;
        objData.CallData.eJSONEventCode = TDD_CALLER_SAYS;
        objData.enumANIFunction = TDD_CALLER_SAYS;
        break;
 
   case TDD_MUTE:
        if (objData.TextData.TDDdata.boolTDDmute) {stringMsg = ANI_MESSAGE_362; intMsgCode = 362;}
        else                                      {stringMsg = ANI_MESSAGE_363; intMsgCode = 363;} 
        enumMessageFormat       = ALLFIELDS;
        break;
   case TDD_MODE_ON:
        intMsgCode              = 391;
        stringMsg = Create_Message(ANI_MESSAGE_391, objData.FreeswitchData.objChannelData.strChannelName);
        enumMessageFormat       = ALLFIELDS;
        break; 
   case DIAL_DEST:
        intMsgCode              = 372;
        stringMsg               = ANI_MESSAGE_372;
        enumMessageFormat       = PLUSTRANSFER;      
        break;
   case DELETE_RECORD:
        intMsgCode              = 374;
        stringMsg = Create_Message(ANI_MESSAGE_374L, objData.CallData.strConferenceNumber);
        enumMessageFormat       = ALLFIELDS;
        break;
   case RINGING_POSITION_TO_POSITION:
        enumMessageFormat       = NOMESSAGE;
        break;
   case VALET_PARK: case VALET_JOIN:
        enumMessageFormat       = NOMESSAGE;
        break;     
   default: 

       Abnormal_Exit(ANI, EX_SOFTWARE, "*Error in 1st switch case in function SendANIToMain" );

   }//end switch

//  if (objData.stringPANI != "**********"){stringPANI = " : pANI="+ objData.stringPANI;}

  switch (enumMessageFormat)
   {
    case NOMESSAGE:
         clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
         objData.stringTimeOfEvent = get_time_stamp(timespecTimeNow);         
         enqueue_Main_Input(ANI, objData);
         return;
    case NOPOSITION:
         objMessage.fMessage_Create(intLogCode, intMsgCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objData.TextData,
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringMsg);               
         break;
    case ALLFIELDS:
         objMessage.fMessage_Create(intLogCode, intMsgCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objData.TextData,
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringMsg);         
         break;
    case PLUSTRANSFER:
         objMessage.fMessage_Create(intLogCode, intMsgCode , __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objData.TextData,
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                    stringMsg, objData.CallData.TransferData.strTransfertoNumber );
         break;
    default:
       Abnormal_Exit(ANI, EX_SOFTWARE, "*Error in 2nd switch case in function SendANIToMain" ); 
   }// end switch

 objData.stringTimeOfEvent = objMessage.stringTimeStamp;
 enQueue_Message(ANI,objMessage);
 enqueue_Main_Input(ANI, objData);

}// end SendANIToMain(ani_functions ANI_Function, ExperientDataClass objData, int intPortNum, bool boolWarningMsg)


/****************************************************************************************************
*
* Name:
*  Function: unsigned long long int  Different_Phone_Numer(ExperientDataClass objData, int intIndex, 
*                                                          int intPortNum, string stringArg, bool boolConnect)
*
*
* Description:
*  A function called by function Is_Same_Call()
*
*
* Details:
*   This function checks the phone number in the worktable vs the phone number contained within the
*   object passed in for a match.  
*   if there is no match then an error is generated and the ringing event is recreated into the worktable and a new callid is 
*   generated.  If the flag boolConnect was set (ie a connect had occured previously) then the out of order flag is set (boolConnectOutofOrder)
*   and the worktable is recreated to the connect event.
*
* Parameters:
*   boolConnect                                 bool                          passed in true if state of worktable is connect
*   intPortNum                                  integer                       ANI port number data was recieved on
*   intIndex                                    integer                       Trunk number 				
*   objData                                     <ExperientDataClass> object   
*   stringArg                                   <cstring>                     used to pass message data                         
*
* Variables:
*   ANI                                 Global  <header.h> threadorPorttype enumeration member
*   ANI_MESSAGE_339                     Global  <defines.h> Phone Number Change error message
*   .boolConnectOutofOrder              Global  <ExperientDataClass> member
*   .boolRecordRebuiltFlag              Global  <ExperientDataClass> member
*   .boolRinging                        Global  <ExperientDataClass> member .. true if ringing function has occured
*   CONNECT                             Global  <header.h> <ani_functions> enumeration member
*   DISCONNECT                          Global  <header.h> <ani_functions> enumeration member
*   .enumANIFunction                    Global  <ExperientDataClass> member
*   intMAX_NUM_TRUNKS                   Global  <defines.h> maximum number of trunks that can be programmed
*   .intCallbackNumber                  Global  <ExperientDataClass> member
*   .intPosn                            Global  <ExperientDataClass> member
*   .intTrunk                           Global  <ExperientDataClass> member
*   .intUniqueCallID                    Global  <ExperientDataClass> member
*   LOG_WARNING                         Global  <defines.h> log code
*   MAIN                                Global  <header.h> threadorPorttype enumeration member
*   objMessage                          Local   <MessageClass> object
*   RINGING                             Global  <header.h> <ani_functions> enumeration member
*   .stringPosn                         Global  <ExperientDataClass> member .. position number
*   .stringTenDigitPhoneNumber          Global  <ExperientDataClass> member .. callback number
*   .stringTrunk                        Global  <ExperientDataClass> member .. trunk number
*   vANIWorktable                       Global  <ExperientDataClass> object array .. the main work table    
*                                                              
* Functions:
*   .fClearRecord()                     Global  <ExperientDataClass>                    (void)
*   .fMessage_Create()                  Local   <globalfunctions.h>                     (void)
*   .fSetUniqueCallID()                 Global  <ExperientDataClass>                    (void)
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   SendANIToMain()                   Local                                           (void)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 

unsigned long long int  Different_Phone_Numer(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg, bool boolConnect)
{
 MessageClass                            objMessage;
 extern vector <ExperientDataClass>      vANIWorktable;


 bool                                    boolIsDifferentTelephoneNumber = false;
 bool                                    boolIsDifferentPANI            = false;
 bool                                    boolIsDifferentSIPNumber       = false;
 Port_Data                               objPortData;
 string                                  strNumberData                  = objData.CallData.stringTenDigitPhoneNumber;

 objPortData.fLoadPortData(ANI, intPortNum);

 // there is no reload of sip number just the prefix ......

 if (vANIWorktable[intIndex].CallData.stringTenDigitPhoneNumber != objData.CallData.stringTenDigitPhoneNumber)   {boolIsDifferentTelephoneNumber = true;}

 if (vANIWorktable[intIndex].CallData.stringPANI != objData.CallData.stringPANI)		                 {boolIsDifferentPANI            = true;}
 
 if(boolIsDifferentTelephoneNumber||boolIsDifferentPANI||boolIsDifferentSIPNumber)
  {
   
   if (boolIsDifferentPANI)           { 
    objMessage.fMessage_Create(LOG_WARNING, 336, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,vANIWorktable[intIndex].fCallData(), 
                               objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_336, 
                               stringArg, objData.CallData.stringPANI);
                               enQueue_Message(ANI,objMessage);
   }
   if (boolIsDifferentTelephoneNumber||boolIsDifferentSIPNumber){ 
    objMessage.fMessage_Create(LOG_WARNING, 339, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intIndex].fCallData(), objBLANK_TEXT_DATA, 
                               objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_339, stringArg, strNumberData); 
    enQueue_Message(ANI,objMessage);
   }


   //send disconnect to KRN .. 
   vANIWorktable[intIndex].enumANIFunction = DISCONNECT;
   if (vANIWorktable[intIndex].CallData.stringPosn == ""){vANIWorktable[intIndex].CallData.stringPosn == "00";}
   SendANIToMain(DISCONNECT, vANIWorktable[intIndex], intPortNum, true);
   vANIWorktable[intIndex].fClear_Record();

   //create and send ringing to KRN ...
   if(boolConnect) {objData.boolConnectOutofOrder = true;}
   objData.enumANIFunction = RINGING;
//   objData.fSetUniqueCallID(objData.CallData.intConferenceNumber);
   objData.boolRecordRebuiltFlag = true;
   SendANIToMain(RINGING, objData, intPortNum, true);
   vANIWorktable[intIndex].boolRinging = true;
   if(!boolConnect){return objData.CallData.intUniqueCallID;}
   objData.enumANIFunction = CONNECT;
   SendANIToMain(CONNECT, objData, intPortNum, true);
   vANIWorktable[intIndex].boolConnect = true;
   return objData.CallData.intUniqueCallID;
  }
 return 0;
}

/****************************************************************************************************
*
* Name:
*  Function: bool Check_Connect(ExperientDataClass objData, int intTrunk, int intPortNum, string stringArg)
*
*
* Description:
*  A function called by function Is_Same_Call()
*
*
* Details:
*   This function checks if the Connect event has been logged for the worktable record, if it hasn't
*   then an error is generated and the Connect event is performed.
*
* Parameters:
*   intPortNum                                  integer
*   intTrunk                                    integer  				
*   objData                                     <ExperientDataClass> object
*   stringArg                                   <cstring>                             
*
* Variables:
*   ANI                                 Global  <header.h> threadorPorttype enumeration member
*   ANI_MESSAGE_337                     Global  <defines.h> No Connect Message Received error message
*   .boolConnect                        Global  <ExperientDataClass> member 
*   CONNECT                             Global  <header.h> ani_functions enumeration member
*   .enumANIFunction                    Global  <ExperientDataClass> member
*   .intCallbackNumber                  Global  <ExperientDataClass> member
*   .intPosn                            Global  <ExperientDataClass> member
*   .intTrunk                           Global  <ExperientDataClass> member
*   .intUniqueCallID                    Global  <ExperientDataClass> member
*   LOG_WARNING                         Global  <defines.h> log code
*   objMessage                          Local   <MessageClass> object
*   vANIWorktable                       Global  <ExperientDataClass> object array .. the main work table   
*                                                              
* Functions:
*   enQueue_Message()                   Global  <globalfunctions.h>                     (void)
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   SendANIToMain()                   Local                                           (void)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
bool Check_Connect(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg)
{
 MessageClass                            objMessage;
 Port_Data                               objPortData;
 extern vector <ExperientDataClass>      vANIWorktable;

 objPortData.fLoadPortData(ANI, intPortNum);

 if (!vANIWorktable[intIndex].boolConnect) 
  {
   objData.CallData.intUniqueCallID = vANIWorktable[intIndex].CallData.intUniqueCallID;
   objMessage.fMessage_Create(LOG_WARNING, 337, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_337, stringArg);
   enQueue_Message(ANI,objMessage);
   objData.enumANIFunction = CONNECT;
   SendANIToMain(CONNECT, objData, intPortNum, true);
   vANIWorktable[intIndex].boolConnect = true;
   return false;
  }
 return true;

}// end void Check_Connect(ExperientDataClass objData, int intTrunk)

/****************************************************************************************************
*
* Name:
*  Function: unsigned long long int Check_Previous_Connect(ExperientDataClass objData, int intIndex, int intPortNum, 
*                                                          string stringArg, bool bool60secNansw)
*
*
* Description:
*  A function called by function Is_Same_Call()
*
*
* Details:
*   This function checks if the Connect event has been logged for the worktable record, 
*   if it has:
*   An error is generated and the DISCONNECT event is performed followed by a recreated RINGING event. If the 
*   flag for 60 sec no answer is set then the flag boolConnectOutofOrder is set to true. The function will then return
*   the Unique Call ID generated by the new ring event.
*   if it has not:
*   return 0
*   
*
* Parameters:
*   intPortNum                                  integer
*   intTrunk                                    integer  				
*   objData                                     <ExperientDataClass> object
*   stringArg                                   <cstring>                             
*
* Variables:
*   ANI                                 Global  <header.h> threadorPorttype enumeration member
*   ANI_MESSAGE_340                     Global  <defines.h> Previous Connect Recieved error message
*   .boolRecordRebuiltFlag              Global  <ExperientDataClass> member 
*   .boolConnect                        Global  <ExperientDataClass> member 
*   .boolConnectOutofOrder              Global  <ExperientDataClass> member 
*   DISCONNECT                          Global  <header.h> ani_functions enumeration member
*   .enumANIFunction                    Global  <ExperientDataClass> member
*   .intCallbackNumber                  Global  <ExperientDataClass> member
*   .intPosn                            Global  <ExperientDataClass> member
*   .intTrunk                           Global  <ExperientDataClass> member
*   .intUniqueCallID                    Global  <ExperientDataClass> member
*   LOG_WARNING                         Global  <defines.h> log code
*   vANIWorktable                       Global  <ExperientDataClass> object array .. the main work table   
*   objMessage                          Local   <MessageClass> object
*   RINGING                             Global  <header.h> ani_functions enumeration member
*    
*                                                              
* Functions:
*   enQueue_Message()                   Global  <globalfunctions.h>                     (void)
*   .fSetUniqueCallID()                 Global  <ExperientDataClass>                    (void)
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   SendANIToMain()                   Local                                           (void)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
unsigned long long int Check_Previous_Connect(ExperientDataClass objData, int intIndex, int intPortNum, string stringArg, bool bool60secNansw)
{
 MessageClass                        objMessage;
 Port_Data			     objPortData;
 extern vector <ExperientDataClass>  vANIWorktable;

 objPortData.fLoadPortData(ANI, intPortNum);

 if (vANIWorktable[intIndex].boolConnect)
  {
   if(!bool60secNansw) {vANIWorktable[intIndex].boolConnectOutofOrder = true;}
   objMessage.fMessage_Create(LOG_WARNING, 340, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objBLANK_TEXT_DATA, 
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_340, stringArg, int2strLZ(intIndex) );
   enQueue_Message(ANI,objMessage);
   //send disconnect to KRN .. 
   vANIWorktable[intIndex].enumANIFunction = DISCONNECT;
   vANIWorktable[intIndex].boolConnect     = false;
   SendANIToMain(DISCONNECT, vANIWorktable[intIndex], intPortNum, true);
   vANIWorktable[intIndex].fClear_Record();

   //create and send ringing to KRN ...
   if(!bool60secNansw) {objData.boolConnectOutofOrder = true;}
   objData.enumANIFunction = RINGING;
 //  objData.fSetUniqueCallID(objData.CallData.intConferenceNumber);
   objData.boolRecordRebuiltFlag = true;
   SendANIToMain(RINGING, objData, intPortNum, true);
   vANIWorktable[intIndex].boolRinging = true;
   return objData.CallData.intUniqueCallID;  

  }// end if (vANIWorktable[intIndex].boolConnect)

return 0;
}// end Check_Previous_Connect(ExperientDataClass objData, int intTrunk, int intPortNum, string stringArg)


/****************************************************************************************************
*
* Name:
*  Function: bool Is_Same_Call(ExperientDataClass* objData, int index, int intPortNum, RebuildCallState enumArg, string stringArg, 
*                              bool boolWarningMsg, bool bool60secNansw)
*
*
* Description:
*  A function called in ANI_Thread()
*
*
* Details:
*   This function checks if the object passed in corresponds to the same call contained within the worktable. If it is the same call,
*   the function returns true, otherwise it returns false.
*   furthermore the state of the worktable is modified (rebuilt either to ring or to connect) within the sub-function that detects the error.
*   
*
* Parameters:
*   bool60secNansw                              bool                  used to flag special case for 60 sec no answer
*   boolWarningMsg                              bool                  used to designate in what section the function failed
*   enumArg                                     <RebuildCallState>    used to designate how far to rebuild the call if not the same
*   intPortNum                                  integer
*   index                                       integer  				
*   objData                                     <ExperientDataClass> object               
*
* Variables:
*   .boolConnectOutofOrder              Global  <ExperientDataClass> member 
*   .intUniqueCallID                    Global  <ExperientDataClass> member
*   vANIWorktable                       Global  <ExperientDataClass> object array .. the main work table
*   objDataCopy                         Global  <ExperientDataClass> object .. a copy of the argument objData
*    
*                                                              
* Functions:
*   Check_Connect()                     Local                                           (bool)
*   Check_Previous_Connect()            Local                                           (unsigned long long int)
*   Different_Phone_Numer()             Local                                           (unsigned long long int)
*   No_Ring()                           Local                                           (unsigned long long int)
*   .fSetUniqueCallID()                 Global  <ExperientDataClass>                    (void)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
bool Is_Same_Call(ExperientDataClass* objData, int index, int intPortNum, RebuildCallState enumArg, string stringArg, bool* boolWarningMsg, 
                  bool bool60secNansw)
{
 ExperientDataClass             objDataCopy(objData);                           //copy of objData
 extern vector <ExperientDataClass> vANIWorktable;


 switch (enumArg)
  {
   case ToRing:
        if ((objData->CallData.intUniqueCallID = No_Ring( objDataCopy, index, intPortNum, stringArg)))                     {return false;}
        if ((objData->CallData.intUniqueCallID = Different_Phone_Numer(objDataCopy, index, intPortNum, stringArg)))        {return false;}
        if ((objData->CallData.intUniqueCallID = Check_Previous_Connect(objDataCopy, index, intPortNum, stringArg, bool60secNansw)))
         {*boolWarningMsg = true;  return false;}  
        break;
   case ToConnect:
        if ((objData->CallData.intUniqueCallID = No_Ring( objDataCopy, index, intPortNum, stringArg,true)))                {return false;}
        if ((objData->CallData.intUniqueCallID = Different_Phone_Numer(objDataCopy, index, intPortNum, stringArg,true)))   {return false;}
        if (!Check_Connect(objDataCopy, index, intPortNum, stringArg))
          {
           vANIWorktable[index].boolConnectOutofOrder = true;
           objData->CallData.intUniqueCallID = vANIWorktable[index].CallData.intUniqueCallID;
           return false;
          } 
        break;
  }
*boolWarningMsg = false;
return true;
}




bool Match_Freeswitch_Object_To_Table(ExperientDataClass* objData, string strRawData, int intPortNum)
{
 int                               intRC;
 MessageClass                      objMessage;
 Port_Data                         objPortData;
 Call_Data                         objCallData;
 bool                              boolBlindTransferHangup              = false;
 bool                              boolIsAPositionChannel               = false;
 bool                              boolIDmatch                          = false;
 bool                              boolTrunkChannelMatch                = false;
 bool                              boolPositionChannelMatch             = false;
 bool                              boolDestChannelMatch                 = false;
 bool                              boolTranChannelMatch                 = false;
 bool                              boolTranChannelOneMatch              = false; 
 bool                              boolTranChannelTwoMatch              = false; 
 bool                              boolRingingChannelMatch              = false;
 bool                              boolPositiontoPositionCall           = false;
 bool                              boolTransferTargetisPosition         = false;
 bool                              boolConfChannelMatch                 = false;
 bool          			   boolSharedLineAvailable 		= false;
 bool                              boolSendHoldOff                      = false;
 bool                              boolSendDisconnect                   = false;
// bool                              boolConfChannelOneMatch              = false; 
// bool                              boolConfChannelTwoMatch              = false; 
 bool                              boolA,boolB,boolC;
 bool                              boolCallerIDsAreReversed;
 Link_Data                         tempObjLinkData;
 int                               intDestVectorIndex			= -1;
 int                               intTranVectorIndex			= -1;
 int                               intConfVectorIndex 			= -1;
 int                               intChannelOneTranIndex		= -1;
 int                               intChannelTwoTranIndex		= -1;
 //int                               intChannelOneConfIndex;
 //int                               intChannelTwoConfIndex;
 int                               intPosnVectorIndex			= -1;
 int                               intTableIndex			= -1;
 int				   intSecondTableIndex			= -1;
 int                               iLineNumber 				= 0;
 int                               intPosition 				= 0;
 int                               intTransferFromPosition 		= 0;
 int                               j 					= 0;
 int                               ilinkdatachannelnumber		= 0;
 int                               iTransferConnect 			= 370;
 string                            strTemp;
 string                            strDialedNumber;
 string                            strLegend;
 size_t                            sz					= 0;
 PhoneBookEntry                    objCallID;
 ani_functions                     eANIfunction				= UNCLASSIFIED;
 ExperientDataClass                TempobjData;

 vector <ExperientDataClass>::size_type   szTable;

 extern bool 				bool_BCF_ENCODED_IP;
 extern Initialize_Global_Variables 	INIT_VARS;

 extern void    Queue_LIS_Event(ExperientDataClass objData);

 objPortData.fLoadPortData(ANI, intPortNum);
 
 switch(objData->FreeswitchData.enumFreeswitchEvent)
  {
   case FREESWITCH_CHANNEL_ORIGINATE:
         // Here we add data to a GUI Transfer ......
       // //cout << "ADD Data to GUI TRANSFER" << endl;
        //   objData->CallData.TransferData.fDisplay();
         intRC = sem_wait(&sem_tMutexANIWorkTable);
         if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}

 //        intTableIndex = IndexWithCallUniqueID(objData->CallData.stringUniqueCallID); /*no longer need to track all uuids */

         intTableIndex = IndexWithTransferTarget(objData->CallData.TransferData.strTransferTargetUUID);
         if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 

         intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferDataWithTransferTarget(objData->CallData.TransferData.strTransferTargetUUID);       
         if (intTranVectorIndex >= 0) {         
           vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromChannel = objData->CallData.TransferData.strTransferFromChannel;
           vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromUniqueid = objData->CallData.TransferData.strTransferFromUniqueid;
           //push back all associated uuids
           vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].vUUID.push_back(objData->CallData.TransferData.strTransferFromUniqueid);

           intConfVectorIndex = -1;
           strTemp = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
           if (!strTemp.empty()) {intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fLegendInConferenceChannelDataVector(strTemp);}
           if (intConfVectorIndex >=0) {
             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelName = objData->CallData.TransferData.strTransferFromChannel;
             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelID = objData->CallData.TransferData.strTransferFromUniqueid;           
           }



         }

        sem_post(&sem_tMutexANIWorkTable);return false;  

   case FREESWITCH_MEDIA_BUG_START: 
        // Marks a channel that is being barged in on 
       // //cout <<"media bug start" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID); 
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 
        ////cout << "adding media bug ->";;
        intDestVectorIndex  =  vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objChannelData.strChannelID );  
        if (intDestVectorIndex >= 0)  {vANIWorktable[intTableIndex].FreeswitchData.vectDestChannelsOnCall[intDestVectorIndex].iSLABarge++;}

        intPosnVectorIndex  =  vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objChannelData.strChannelID ); 
        if (intPosnVectorIndex >= 0)  {vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iSLABarge++;} 

        sem_post(&sem_tMutexANIWorkTable);return false; 
 
   case FREESWITCH_MEDIA_BUG_STOP:
       // unMarks a channel that had been barged in on
      // //cout << "media bug stop" << endl; 
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID); 
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 

        intDestVectorIndex  =  vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objChannelData.strChannelID );  
        if (intDestVectorIndex >= 0)  {vANIWorktable[intTableIndex].FreeswitchData.vectDestChannelsOnCall[intDestVectorIndex].iSLABarge--;}

        intPosnVectorIndex  =  vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objChannelData.strChannelID ); 
        if (intPosnVectorIndex >= 0)  {vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iSLABarge--;} 

        sem_post(&sem_tMutexANIWorkTable);return false; 
 

   case FREESWITCH_ANSWER_CONFERENCE_TRANSFER:
    //    //cout << "FREESWITCH_ANSWER_CONFERENCE_TRANSFER" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strTranData);

        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 
////cout << "in answer conference transfer " << endl;
        switch (objData->FreeswitchData.objChannelData.iPositionNumber)                                                  
         {
          case 0:

                  // This means that an "outside" the Server Connected from a Conference Transfer
                  intPosition = char2int(objData->CallData.TransferData.strTransferFromPosition.c_str());
                  intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strTransfertoNumber, intPosition);
                  if (intTranVectorIndex < 0) { 
                   intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strRDNIS, intPosition);
                  }
                  if (intTranVectorIndex < 0) {
                   SendCodingError("ani.cpp - CODING ERROR in case FREESWITCH_ANSWER_CONFERENCE_TRANSFER: case 0: intTranVectorIndex < 0" );
                   sem_post(&sem_tMutexANIWorkTable);return false;       
                  }
                  //get line info from transfering position .....                        
                  intPosnVectorIndex = vANIWorktable[intTableIndex].FreeswitchData.fFindPositionInPosnVector
                                                                    (char2int(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.c_str()));
                  if (intPosnVectorIndex >= 0) {
                   objData->FreeswitchData.objChannelData.iGUIlineView = vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iGUIlineView;
                   objData->FreeswitchData.objChannelData.iLineNumber  = vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iLineNumber;
                   objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iTrunkNumber);
                  }
                  objData->FreeswitchData.objChannelData.strConfDisplay    = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
                  objData->FreeswitchData.objChannelData.strCallerID       = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber;
                  objData->FreeswitchData.objChannelData.strTranData.clear();
 
                  vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);
                  vANIWorktable[intTableIndex].CallData.fLoadPosn(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition);
                  vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
                  // //cout << "378.f" << endl;
                  vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 378, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                             objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378,  
                                             vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber,
                                             vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID );
                  enQueue_Message(ANI,objMessage); 


                  objData->stringTimeOfEvent = objMessage.stringTimeStamp;
 //               if (boolTDD_AUTO_DETECT)
 //                {
 //                 objData->enumANIFunction = TDD_MODE_ON;
 //                 vANIWorktable[intTableIndex].AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objLinkData.strChanneltwo;
 //                 Queue_AMI_Input(objData);
 //                 sem_post(&sem_tAMIFlag);
//                 }

                  objData->enumANIFunction = TRANSFER_CONNECT;
                  objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
                  objData->CallData = vANIWorktable[intTableIndex].CallData; 

                  enqueue_Main_Input(ANI, objData);

                  sem_post(&sem_tMutexANIWorkTable);return false;       
                  break;
          default:


////cout << "transfer number " << objData->CallData.TransferData.strTransfertoNumber << endl;
////cout << "from position -> " << char2int(objData->CallData.TransferData.strTransferFromPosition.c_str()) << endl;
////cout << "RDNIS -> " << objData->CallData.TransferData.strRDNIS << endl;
                  intTransferFromPosition = char2int(objData->CallData.TransferData.strTransferFromPosition.c_str());
                  intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strTransfertoNumber, intTransferFromPosition);

                  if (intTranVectorIndex < 0){
                   intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strRDNIS, intTransferFromPosition);
                  }
                  if (intTranVectorIndex < 0) {
                   SendCodingError("ani.cpp - CODING ERROR in case FREESWITCH_ANSWER_CONFERENCE_TRANSFER: case default: intTranVectorIndex < 0" );
                   sem_post(&sem_tMutexANIWorkTable);return false;       
                  }

                //  //cout << "intTranVectorIndex"  << " = " << intTranVectorIndex << endl;
               //   //cout << "loop back transfer" << endl;
                  // Here we had a "Loop Back" Conference Transfer to a Position On the System.  
                  // The Transfer appeared to be external but was really Internal
            //      //cout << "conference join position " << objData->FreeswitchData.objChannelData.iPositionNumber << endl; 
                  objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
                  objData->FreeswitchData.objChannelData.fLoadLineNumberFromChannel();
 //                 vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData);   
                  j = objData->FreeswitchData.objChannelData.iPositionNumber;
////cout << "J is " << j << endl;
                    objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
                    objData->FreeswitchData.objChannelData.boolConnected = true;
                    vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);

     
                    vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData, intTranVectorIndex);
                    vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, int2strLZ(intTransferFromPosition), intPortNum, "IN FREESWITCH_ANSWER_CONFERENCE_TRANSFER");
                    objData->CallData = vANIWorktable[intTableIndex].CallData;
                    objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
                    objData->enumANIFunction = TRANSFER_CONNECT;
                    enqueue_Main_Input(ANI, objData);
                 // //cout << "FREESWITCH_ANSWER_CONFERENCE_TRANSFER 2" << endl;

                  vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, int2strLZ(j), intPortNum, "IN FREESWITCH_ANSWER_CONFERENCE_TRANSFER");
                  if (j < 1 ) {vANIWorktable[intTableIndex].CallData.boolConfXferUpdate = true;}
                  else 
                   {
                    switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
                     {
                      case mGUI_TRANSFER:
                       //  //cout << "378.y" << endl;
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, 395, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,  ANI_MESSAGE_378,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber,
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID );
                         enQueue_Message(ANI,objMessage); 
                         break;
                      case mNG911_TRANSFER:
                        // //cout << "378.d.1" << endl;
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);                       
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, 395, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378d,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strNG911PSAP_TransferNumber,
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID );
                         enQueue_Message(ANI,objMessage);
                         break;
                      default:
                         SendCodingError("ani.cpp - Coding Error in fn Match_Freeswitch_Object_To_Table() at case FREESWITCH_ANSWER_CONFERENCE_TRANSFER\n");
                     }

                   vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex); 
                   }
                  sem_post(&sem_tMutexANIWorkTable);return false;
         }


        sem_post(&sem_tMutexANIWorkTable);return true;
       
   case FREESWITCH_CALL_UPDATE: 

        // We process this after a SLA Barge In .. a FREESWITCH_MEDIA_BUG_START will precede this section during a Barge In.
        // if we do not see the counter above zero ignore (indicates a connect which is handled elsewhere)
        // Replaces FREESWITCH_SLA_JOIN_CALL for the new FreeSWITCH Barge 5/30/2012
        //
        // added check on GUI Blind Transfer for position to position calls in Transfer Data ... 4/18/2016
        // 
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}
     //   //cout << "CALL UPDATE IN ANI" << endl;
    //    objData->FreeswitchData.objChannelData.fDisplay();
         TempobjData = objData;
  //      //cout << objData->FreeswitchData.objChannelData.strTranData << endl;

        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strTranData); 
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 

        intDestVectorIndex  =  vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objChannelData.strTranData );  
        boolDestChannelMatch =  (intDestVectorIndex >= 0);
        intPosnVectorIndex  =  vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objChannelData.strTranData ); 
        boolPositionChannelMatch =  (intPosnVectorIndex >= 0);

 //       if (boolDestChannelMatch)     {//cout << "Dest Channel Match" << endl;}
 //       if (boolPositionChannelMatch) {//cout << "Posn Channel Match" << endl;}

       // vANIWorktable[intTableIndex].FreeswitchData.fDisplay();



        //check if channel had a MEDIA_BUG_START recorded
        if (boolDestChannelMatch) 
         { if (vANIWorktable[intTableIndex].FreeswitchData.vectDestChannelsOnCall[intDestVectorIndex].iSLABarge <= 0) {sem_post(&sem_tMutexANIWorkTable);return false; }} 
        else if (boolPositionChannelMatch){
          if (vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iSLABarge <= 0)     {sem_post(&sem_tMutexANIWorkTable);return false; }
          //check if position is already on call
          if (vANIWorktable[intTableIndex].FreeswitchData.fFindPositionInPosnVector(objData->FreeswitchData.objChannelData.iPositionNumber) >= 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 
         }
        
    //    //cout << "got past media Bug ->" << objData->FreeswitchData.objChannelData.iPositionNumber << endl;
        switch (objData->FreeswitchData.objChannelData.iPositionNumber)                                                  
         {
          case 0:
                  
                  intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strTransfertoNumber, 
                                                                                               char2int(objData->CallData.TransferData.strTransferFromPosition.c_str()));
                  if (intTranVectorIndex < 0) { sem_post(&sem_tMutexANIWorkTable);return false;}
                  iTransferConnect = 378;
                 // //cout << "FREESWITCH_CALL_UPDATE: found in Table ..." << endl;

                  if (vANIWorktable[intTableIndex].CallData.boolConfXferUpdate) {iTransferConnect = 395; vANIWorktable[intTableIndex].CallData.boolConfXferUpdate= false;}

                  // Determine if a position to position CALL
 //objData->FreeswitchData.fDisplay();                   
                  switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
                   {
                    case mGUI_TRANSFER:
                       // //cout << "this 378.a" << endl;
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData,
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber);
                         enQueue_Message(ANI,objMessage); 
                         break;
                    case mNG911_TRANSFER:
                         // //cout << "this 378d.2" << endl; 
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);                       
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData,
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378d,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strNG911PSAP_TransferNumber);
                         enQueue_Message(ANI,objMessage);
                         break;
                    default:
                         SendCodingError("ani.cpp - Coding Error in fn Match_Freeswitch_Object_To_Table() at case FREESWITCH_CALL_UPDATE\n");
                   }
       
                  objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
                  objData->FreeswitchData.objChannelData.strConfDisplay    = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
                  objData->FreeswitchData.objChannelData.strCallerID       = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber;
                  objData->FreeswitchData.objChannelData.strTranData.clear();

                  objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
                  objData->FreeswitchData.objChannelData.boolConnected = true;

                  vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);
                  vANIWorktable[intTableIndex].CallData.fLoadPosn(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition);

                  vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);

                  objData->stringTimeOfEvent = objMessage.stringTimeStamp;
 //               if (boolTDD_AUTO_DETECT)
 //                {
 //                 objData->enumANIFunction = TDD_MODE_ON;
 //                 vANIWorktable[intTableIndex].AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objLinkData.strChanneltwo;
 //                 Queue_AMI_Input(objData);
 //                 sem_post(&sem_tAMIFlag);
//                 }

                  objData->enumANIFunction = TRANSFER_CONNECT;
                  objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
                  objData->CallData = vANIWorktable[intTableIndex].CallData; 
                  objData->boolDoNotSendToCAD = true;
               
               //   if (boolPositiontoPositionCall) { objData->FreeswitchData.objLinkData = tempObjLinkData ;} this may affect RCC's not needed for SLA

                 
                  enqueue_Main_Input(ANI, objData);

                  
                 
                  sem_post(&sem_tMutexANIWorkTable);return false;       
                  break;
          default:
                  PreBuild_Conference(intTableIndex);                  
       //           //cout << "position Barge " << endl;   
       //           //cout << "conference join position " << objData->FreeswitchData.objChannelData.iPositionNumber << endl;
                  objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].CallData.intTrunk);                   
                  objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
                  objData->FreeswitchData.objChannelData.strTranData.clear();
              //    //cout << "A" << endl;   
                  vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData);

                  j = objData->FreeswitchData.objChannelData.iPositionNumber;
                  vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, j);
                  vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, int2strLZ(j), intPortNum, "IN FREESWITCH_CALL_UPDATE");
                  objData->CallData = vANIWorktable[intTableIndex].CallData;
                  objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
                  objData->enumANIFunction = CONFERENCE_JOIN;
                 // //cout << " conf join from FREESWITCH_CALL_UPDATE" << endl;

                  Add_Conference_Member(intTableIndex, TempobjData); 
         }


        sem_post(&sem_tMutexANIWorkTable);return true;
     
   case FREESWITCH_BARGE_WITH_BCF:
        // this was added as a workaround to the ORACLE BCF for conference and position Barges due to the ip address being hidden by the BCF
       // //cout << "Barge from BCF" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}
        intTableIndex = -1;

        //If already on call return .....
        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID);
        if (intTableIndex >= 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}


        // find conference name in table
        szTable = vANIWorktable.size();      
        for (unsigned int i = 0; i < szTable; i++)
         {
          if (vANIWorktable[i].CallData.strFreeswitchConfName == objData->CallData.strFreeswitchConfName) {intTableIndex = i;break;}
         }
        //if not a position or if conference not found return ...
        if (intTableIndex < 0)                                     { sem_post(&sem_tMutexANIWorkTable);return false; }
        if(!objData->FreeswitchData.objChannelData.iPositionNumber){ sem_post(&sem_tMutexANIWorkTable);return false; }
          

        objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
        objData->FreeswitchData.objChannelData.iTrunkNumber      = vANIWorktable[intTableIndex].CallData.intTrunk;           
    //    //cout << "push into table" << endl;
        //Push Data into the table
    //    //cout << "B" << endl;  
        vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData); 

        j = objData->FreeswitchData.objChannelData.iPositionNumber;
        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, j);
        vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, int2strLZ(j), intPortNum, "FREESWITCH_BARGE_WITH_BCF");
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
        objData->enumANIFunction =CONFERENCE_JOIN;

        sem_post(&sem_tMutexANIWorkTable);return true;  

      
   case FREESWITCH_SLA_JOIN_CALL:
        // In this case the Pertinent/current Freeswitch Data Passed in is :
        // FreeswitchData.objChannelData
        cout << "FREESWITCH_SLA_JOIN_CALL" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
         if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}

        szTable = vANIWorktable.size(); 

        // Return false if either channel is Part of a Blind or Attended Transfer (will be handled by FREESWITCH_CHANNEL_ANSWER)      
        for (unsigned int i = 0; i < szTable; i++) {
          intChannelOneTranIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChannelone, objData->FreeswitchData.objLinkData.strChanneloneID);
          if (intChannelOneTranIndex >= 0) {sem_post(&sem_tMutexANIWorkTable);return false;}
          intChannelTwoTranIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChanneltwo, objData->FreeswitchData.objLinkData.strChanneltwoID);
          if (intChannelTwoTranIndex >= 0) {sem_post(&sem_tMutexANIWorkTable);return false;}        
        }


        cout << "SLA JOIN CALL GOT DATA TO MATCH ...." << endl;

        // find match in table ....
        intChannelOneTranIndex  = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneloneID);
        boolTranChannelOneMatch = (intChannelOneTranIndex >= 0);
        intChannelTwoTranIndex  = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneltwoID);
        boolTranChannelTwoMatch = (intChannelTwoTranIndex >= 0); 
 
        cout << "1 match -> " << boolTranChannelOneMatch << endl;
        cout << "2 match -> " << boolTranChannelTwoMatch << endl;
        objData->FreeswitchData.objReferData.fDisplay(); 
        
 
        //return if both match (already in Table) or neither in Table (will be handled by FREESWITCH_CHANNEL_ANSWER)  
        if (boolTranChannelOneMatch&&boolTranChannelTwoMatch)   {
         //update refer object before exiting ....
         intTableIndex = intChannelOneTranIndex;
         vANIWorktable[intTableIndex].CallData.ReferData          = objData->FreeswitchData.objReferData;
         vANIWorktable[intTableIndex].FreeswitchData.objReferData = objData->FreeswitchData.objReferData;
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        } 
        if (!(boolTranChannelOneMatch||boolTranChannelTwoMatch)){sem_post(&sem_tMutexANIWorkTable);return false;}



        if (boolTranChannelOneMatch) {ilinkdatachannelnumber = 2; intTableIndex = intChannelOneTranIndex;}
        else                         {ilinkdatachannelnumber = 1; intTableIndex = intChannelTwoTranIndex;}

        // Load unmatched channel from Link Object in ChannelData object
        objData->FreeswitchData.fLoad_Bridge_Data_Into_Object(objData->FreeswitchData.objLinkData, ilinkdatachannelnumber, 0, true, "", false);
    
        //the unmatched channel must be a from a position or we return false ......
        if (!objData->FreeswitchData.objChannelData.iPositionNumber) {sem_post(&sem_tMutexANIWorkTable);return false;}

        //check if position is already on the call or return false ...
        if(vANIWorktable[intTableIndex].CallData.fIsInConference(POSITION_CONF,objData->FreeswitchData.objChannelData.iPositionNumber )){sem_post(&sem_tMutexANIWorkTable);return false;}

        //Load Shared Line Number and trunk
        objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
        objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].CallData.intTrunk);           

        if (vANIWorktable[intTableIndex].CallData.strFreeswitchConfName.empty()) {
         vANIWorktable[intTableIndex].CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strChannelID; //fred UUID_Generate_String();
         ////cout << "generate conf ID" << endl;
        }
         boolA = PreBuild_Conference(intTableIndex);
         boolB = (vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() == 1);
         boolC = (!boolA)&&(boolB); // On Hold Pickoff
 //        if (boolC) {//cout << "pickoff" << endl;}

        TempobjData = objData;
        TempobjData.CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].CallData.strFreeswitchConfName;
        //Push Data into the table

    //    //cout << "C" << endl;  
        vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData); 

        j = objData->FreeswitchData.objChannelData.iPositionNumber;
        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, j);
        vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, int2strLZ(j), intPortNum, "IN FREESWITCH_SLA_JOIN_CALL");
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
        objData->enumANIFunction =CONFERENCE_JOIN;

        if (!boolC) {Add_Conference_Member(intTableIndex, TempobjData);}
        sem_post(&sem_tMutexANIWorkTable);return true;
  
   case FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER:
   //     //cout << "FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER" << endl;

        intPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objData->CallData.TransferData.IPaddress.stringAddress);
//        //cout << "position -> " << intPosition << endl;
//        //cout << objData->CallData.TransferData.IPaddress.stringAddress << endl;
//        //cout << "Channel Name -> " << objData->FreeswitchData.objChannelData.strChannelName << endl;
        if (intPosition <= 0)             
         {
          intPosition = TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));
         }
        if (intPosition <= 0) {return false;}
                   
        intTransferFromPosition = objData->CallData.TransferData.fLoadFromPosition(objData->CallData.TransferData.strTransferFromPosition);
        if (intTransferFromPosition <= 0) {return false;}
//        //cout << "transfer from -> " << intTransferFromPosition << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d.a", 1);}

        // find conference# in table

     //  vANIWorktable[0].CallData.fDisplay(); 
      

        szTable       = vANIWorktable.size();
        intTableIndex = -1;
        for (unsigned int i = 0; i < szTable; i++) 
         {
          strLegend = ANI_CONF_MEMBER_POSITION_PREFIX + objData->CallData.TransferData.strTransferFromPosition;
          j = vANIWorktable[i].CallData.fLegendInConferenceChannelDataVector(strLegend);
          if (j < 0) {continue;}
          if (vANIWorktable[i].CallData.ConfData.vectConferenceChannelData[j].strChannelID == objData->CallData.strFreeswitchConfName) {intTableIndex = i; break;}
         }
        if (intTableIndex < 0)      {sem_post(&sem_tMutexANIWorkTable); return false;}

        // find Transfer Index
        intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strTransfertoNumber, intTransferFromPosition);
        if (intTranVectorIndex < 0) {sem_post(&sem_tMutexANIWorkTable); return false;}

  
       // find conference entry with transfer number ...... Internal will always be a position(reverse search)
        intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceDataFromCallIDNumber(objData->CallData.TransferData.strTransfertoNumber);
        if (intConfVectorIndex < 0) {return false;}

        vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strConfDisplay);
        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, intPosition);
        vANIWorktable[intTableIndex].CallData.ConfData.fRemoveTransferFromHistory(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strConfDisplay);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].iPositionNumber = intPosition;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolIsPosition  = true;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadConferenceDisplayP(intPosition);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].IPaddress       = objData->CallData.TransferData.IPaddress;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolConnected   = true;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelName = objData->FreeswitchData.objChannelData.strChannelName; 
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelID   = objData->FreeswitchData.objChannelData.strChannelID; 
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadLineNumberFromChannel();

        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadCustomerName(intPosition);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadPositionCallID(intPosition);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);


        //Put Position to into Position From in Transfer Data reference to CONFERENCE_JOIN code 378b
        vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition = int2str(intPosition);
      
        //put updated conference data into position vector
     //   //cout << "d" << endl;  
        vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex]);    

        //Check for duplicates on position # joining call .. intPosition

        vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(intPosition);  

 //       vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fDisplay(1,true); 
        iTransferConnect = 395;
        vANIWorktable[intTableIndex].CallData.fLoadPosn(intPosition);

        switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
         {
          case mGUI_TRANSFER:
             //  //cout << "378.x" << endl;
               vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
               objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                          objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378,  
                                          vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber,
                                          vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID);
               enQueue_Message(ANI,objMessage); 
               break;
          case mNG911_TRANSFER:
               vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
              // //cout << "378d.3" << endl;
               objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                          objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378d,  
                                          vANIWorktable[intTableIndex].CallData.TransferData.strNG911PSAP_TransferNumber, 
                                          vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID);
               enQueue_Message(ANI,objMessage);
               break;
          default:
               SendCodingError("ani.cpp - Coding Error in fn Match_Freeswitch_Object_To_Table() at case FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER\n");
         }
       
        objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
        objData->FreeswitchData.objChannelData.strConfDisplay    = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
        objData->FreeswitchData.objChannelData.strCallerID       = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber;
        objData->FreeswitchData.objChannelData.strTranData.clear();

        objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
        objData->FreeswitchData.objChannelData.boolConnected = true; 
        vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);


        vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);

        objData->stringTimeOfEvent = objMessage.stringTimeStamp;
        objData->enumANIFunction = TRANSFER_CONNECT_INTERNAL_NG911;
        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
        objData->CallData = vANIWorktable[intTableIndex].CallData; 
        //set to false so as not to send to a list .....
        objData->boolDoNotSendToCAD = true;
               
        //   if (boolPositiontoPositionCall) { objData->FreeswitchData.objLinkData = tempObjLinkData ;} this may affect RCC's not needed for SLA

        // //cout << "internal NG911 Transfer Connect" << endl;

        enqueue_Main_Input(ANI, objData);

        // Send Channel to conference .....
        objData->CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].CallData.strFreeswitchConfName;
        objData->enumANIFunction = CONFERENCE_JOIN_LIST;
        objData->enumANIFunction = BARGE_ON_CONFERENCE_LINE_ROLL;
        Queue_AMI_Input(objData);
        sem_post(&sem_tAMIFlag);
  
        sem_post(&sem_tMutexANIWorkTable); return false;
        break;

   case FREESWITCH_CHANNEL_ANSWER_UPDATE_GUI_BLIND_TRANSFER:
        // Check IP address from CallData.TransferData.IPaddress, if it is a position IP then update USE this before using ........... CONFERENCE_JOIN
       // //cout << "FREESWITCH_CHANNEL_ANSWER_UPDATE_GUI_BLIND_TRANSFER" << endl;

        intTransferFromPosition = objData->CallData.TransferData.fLoadFromPosition(objData->CallData.TransferData.strTransferFromPosition);
        if (intTransferFromPosition <= 0) {return false;}

        intPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objData->CallData.TransferData.IPaddress.stringAddress);
        if (intPosition < 0)             {return false;}

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}
   
        // find conference# in table
        szTable       = vANIWorktable.size();
        intTableIndex = -1;
        for (unsigned int i = 0; i < szTable; i++){
             if(objData->CallData.strFreeswitchConfName == vANIWorktable[i].CallData.strFreeswitchConfName) {intTableIndex = i; break;}
        }
        if (intTableIndex < 0)      {sem_post(&sem_tMutexANIWorkTable); return false;}

        // find Transfer Index
        intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strTransfertoNumber, intTransferFromPosition);
        if (intTranVectorIndex < 0) {sem_post(&sem_tMutexANIWorkTable); return false;}

        //find conference entry by unique ID (race could cause it to fail ..)
        intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceData(objData->FreeswitchData.objChannelData.strChannelID);
        if (intConfVectorIndex < 0) {
          // find conference entry with transfer number ...... (reverse search)
          intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceDataFromCallIDNumber(objData->CallData.TransferData.strTransfertoNumber);
          if (intConfVectorIndex < 0) {return false;}
        }

        switch (intPosition)
         {
          case 0:
           //Destination Channel Connect.
           strDialedNumber = objData->CallData.TransferData.strTransfertoNumber;
           vANIWorktable[intTableIndex].CallData.TransferData.fClear();
           vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
           //set trunk to objringdialtrunk
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolConnected = true;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].IPaddress     = objData->FreeswitchData.objChannelData.IPaddress;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelID  = objData->FreeswitchData.objChannelData.strChannelID;
           vANIWorktable[intTableIndex].FreeswitchData.vectDestChannelsOnCall.push_back(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex]);

           objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
           objData->CallData       = vANIWorktable[intTableIndex].CallData;
           objData->CallData.fLoadPosn(0);
    //       //cout << "378.c" << endl;
           objMessage.fMessage_Create(LOG_CONSOLE_FILE, 378, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA, objPortData,  
                                      objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378b, strDialedNumber);
           eANIfunction = TRANSFER_CONNECT_BLIND;
           objData->enumANIFunction = eANIfunction;
           objData->stringTimeOfEvent = objMessage.stringTimeStamp;
           enQueue_Message(ANI,objMessage);

           enqueue_Main_Input(ANI, objData);
           break;

          default:
          //Position Channel Update for conf join .....
      //  vANIWorktable[intTableIndex].CallData.fDisplay();

           vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strConfDisplay);
           vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, intPosition);
           vANIWorktable[intTableIndex].CallData.ConfData.fRemoveTransferFromHistory(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strConfDisplay);
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].iPositionNumber = intPosition;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolIsPosition  = true;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadConferenceDisplayP(intPosition);
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].IPaddress       = objData->CallData.TransferData.IPaddress;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolConnected   = true;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelName = objData->FreeswitchData.objChannelData.strChannelName; 
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strChannelID   = objData->FreeswitchData.objChannelData.strChannelID; 
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadLineNumberFromChannel();
  //         vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fSetSharedLineFromLineNumber();
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);;
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fLoadCustomerName(intPosition);

           //Put Position to into Position From in Transfer Data reference to CONFERENCE_JOIN code 378b
           vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition = int2str(intPosition);
           vANIWorktable[intTableIndex].CallData.boolBlindXferUpdate = true;
           //  //cout << "this position is" << intPosition << endl;
      
           //put updated conference data into position vector
    //    //cout << "e" << endl;  
           vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex]);

           //Position Channel Connect.
           strDialedNumber = objData->CallData.TransferData.strTransfertoNumber;
           vANIWorktable[intTableIndex].CallData.TransferData.fClear();
           vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);

           objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
           objData->CallData       = vANIWorktable[intTableIndex].CallData;
           objData->CallData.fLoadPosn(intPosition);
        //   //cout << "378.d" << endl;
           objMessage.fMessage_Create(LOG_CONSOLE_FILE, 378, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA, 
                                      objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378b, strDialedNumber);
           eANIfunction = CONFERENCE_JOIN;
           objData->enumANIFunction = eANIfunction;
           objData->stringTimeOfEvent = objMessage.stringTimeStamp;
           enQueue_Message(ANI,objMessage);

           enqueue_Main_Input(ANI, objData);
           break;          


           break;
         }

 //       vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fDisplay(1,true);        
        sem_post(&sem_tMutexANIWorkTable); return false;
        break;

   case FREESWITCH_MSRP_TEXT_MSG_CONNECT:
       // here we have a connect from a MSRP ring 
        // The Data we will compare in the Table is objlinkdata. Channel One should have the Position Data, Channel Two should have
        // the Caller Channel that Answered. (this is reversed from normal calls i.e. FREESWITCH_CHANNEL_ANSWER)
        // The Position is derived from Presence Data and stored in objChannelData
 //  //cout << "MSRP CHANNEL ANSWER in ANI .. ANI" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d1a", 1);}
 
         intTableIndex = -1;
         szTable = vANIWorktable.size();
         tempObjLinkData            =  objData->FreeswitchData.objLinkData;

         // check if both channels are already in Table 
         if (BothChannelsAlreadyPresentandConnected(objData->FreeswitchData)) {sem_post(&sem_tMutexANIWorkTable); return false;}  

        for (unsigned int i = 0; i < szTable; i++)
         {
          intPosnVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objLinkData.strChanneltwoID );
          boolPositionChannelMatch     =  (intPosnVectorIndex >= 0);
          intDestVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objLinkData.strChanneloneID );
          boolDestChannelMatch         =  (intDestVectorIndex >= 0);         
          intChannelOneTranIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChannelone, objData->FreeswitchData.objLinkData.strChanneloneID);
          boolTranChannelOneMatch      =  (intChannelOneTranIndex >= 0);
          intChannelTwoTranIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChanneltwo, objData->FreeswitchData.objLinkData.strChanneltwoID);
          boolTranChannelTwoMatch      =  (intChannelTwoTranIndex >= 0); 

                        
          if (boolDestChannelMatch||boolPositionChannelMatch||boolTranChannelOneMatch||boolTranChannelTwoMatch) { boolIDmatch=true; intTableIndex= i; break;}
         }

       if (!boolIDmatch)
         {
          intTableIndex = LocateRingChannelIdinTable(objData->FreeswitchData.objLinkData.strChanneltwoID); 
          if (intTableIndex >= 0) {boolRingingChannelMatch = true;}
         }
         if (intTableIndex < 0){sem_post(&sem_tMutexANIWorkTable); return false;}

 //       if(boolDestChannelMatch)       {//cout << "dest  channel connect" << endl;}
 //        if(boolPositionChannelMatch)   {//cout << "posn  channel connect" << endl;}
  //       if(boolTranChannelOneMatch)    {//cout << "Tran  channel One connect" << endl;}
 //        if(boolRingingChannelMatch)    {//cout << "ring  channel connect" << endl;}
 //        if(boolTranChannelTwoMatch)    {//cout << "Tran  channel Two connect" << endl;}
 //        if(boolConfChannelOneMatch)    {//cout << "Conf  channel One connect" << endl;}
 //        if(boolConfChannelTwoMatch)    {//cout << "Conf  channel Two connect" << endl;}

////cout << "CH1 ->" << objData->FreeswitchData.objLinkData.strChannelone << endl;
////cout << "CH2 ->" << objData->FreeswitchData.objLinkData.strChanneltwo << endl;
         // new msrp text call
         if ((boolRingingChannelMatch)&&(!boolPositionChannelMatch)&&(!boolDestChannelMatch))
          {
           switch (vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.boolIsOutbound)
            {
             case true: break;
             case false:
             // Inbound Ring .......
             objData->enumANIFunction = CONNECT;
             //load Link1 (Position Data)
             vANIWorktable[intTableIndex].FreeswitchData.fLoad_Bridge_Data_Into_Object
              (objData->FreeswitchData.objLinkData, 1, vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber, false, "", false);
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strPresenceID = objData->FreeswitchData.objChannelData.strPresenceID;
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.boolMSRP = true;
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadPositionNumber(objData->FreeswitchData.objChannelData.iPositionNumber);
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadConferenceDisplayP(objData->FreeswitchData.objChannelData.iPositionNumber);
     //   //cout << "F" << endl;  
             vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);

             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);

             vANIWorktable[intTableIndex].FreeswitchData.fLoad_Bridge_Data_Into_Object
              ( objData->FreeswitchData.objLinkData, 2, vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber, false, "", false );
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strPresenceID = "";
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.boolMSRP = true;
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadConferenceDisplayC(objData->FreeswitchData.objChannelData.iTrunkNumber);
             vANIWorktable[intTableIndex].FreeswitchData.vectDestChannelsOnCall.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);
             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);

           // Load position into Table (note objChannelData will have the data from link channel 2 from previous function call do not add code between !!!!) 
           //Position will be wrong if MSRP         
        //     vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadPositionNumber(objData->FreeswitchData.objChannelData.iPositionNumber);
        //     vANIWorktable[intTableIndex].FreeswitchData.objChannelData.boolMSRP = true;
        //     vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strPresenceID = objData->FreeswitchData.objChannelData.strPresenceID;                                 

         
             vANIWorktable[intTableIndex].CallData.fLoadPosn( objData->FreeswitchData.objChannelData.iPositionNumber);                    
             vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, vANIWorktable[intTableIndex].CallData.stringPosn, intPortNum,"FREESWITCH_CHANNEL_ANSWER");
             objData->CallData = vANIWorktable[intTableIndex].CallData;  
   
           //Put Caller into Conference string: 
  //         vANIWorktable[intTableIndex].CallData.fLoadCallBackDisplay(); 
   //        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(CALLER_CONF,objData->CallData.intTrunk);
              
             break;
            }
          }


        sem_post(&sem_tMutexANIWorkTable);
        return true;

   case FREESWITCH_VALET_PICKUP:
        // This is from a Parked call picked up preceded by a Blind transfer.
        // Data passed in is objData->FreeswitchData.objLinkData
        // objData->CallData.TransferData.  The UUID of the call should match objLinkData.strChanneloneID. (caller in the lot) 
        // objLinkData.strChanneltwoID should be the channel that picked up the call.				
       //  //cout << "ani - valet pickup" << endl;
        intSecondTableIndex = -1;

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable case FREESWITCH_VALET_PICKUP", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneloneID);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable); return false;} 

        intChannelOneTranIndex       =  vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChannelone, objData->FreeswitchData.objLinkData.strChanneloneID);
        boolTranChannelOneMatch      =  (intChannelOneTranIndex >= 0);
        intChannelTwoTranIndex       =  vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChanneltwo, objData->FreeswitchData.objLinkData.strChanneltwoID);
        boolTranChannelTwoMatch      =  (intChannelTwoTranIndex >= 0);
        if (!(boolTranChannelOneMatch||boolTranChannelTwoMatch)) {sem_post(&sem_tMutexANIWorkTable); return false;} 
        if (boolTranChannelOneMatch)                             {intTranVectorIndex = intChannelOneTranIndex; j = 2;}
        else                                                     {intTranVectorIndex = intChannelTwoTranIndex; j = 1;}
        // j points to the "new" channel.
       // //cout << "New channel is link num -> " << j << endl;
 
        switch (j) {
         case 1:       
          intSecondTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneltwoID, intTableIndex);
          break;
         case 2:
          intSecondTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneloneID, intTableIndex);
          break;
        }                   
     
 //       //cout << "Call is in index -> " << intTableIndex << " other is in index -> " << intSecondTableIndex << endl;
              
        // we may have two calls that hit the lot in a blind xfer or a pick off
        objData->FreeswitchData.objChannelData.fLoadLinkDataIntoChannelData(objData->FreeswitchData.objLinkData, j);

        strDialedNumber = objData->CallData.TransferData.strTransfertoNumber;

        vANIWorktable[intTableIndex].FreeswitchData.objLinkData    =  objData->FreeswitchData.objLinkData;
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData =  objData->FreeswitchData.objChannelData; 

        if (objData->FreeswitchData.objChannelData.iPositionNumber){
         //Position has grabbed the call from the parking lot We need to remove transferData and Temp Conf channel Data (will have dup UUID)
         // and add the position also Line numbers may have changed from phone ....
 //         //cout <<" it is a position .... -> " << objData->FreeswitchData.objChannelData.iPositionNumber << endl;
          vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, objData->FreeswitchData.objChannelData.iPositionNumber);
          vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(vANIWorktable[intTableIndex].FreeswitchData, j, intTranVectorIndex, false);
          //Line numbers may be wrong ... They can shift from Park
          vANIWorktable[intTableIndex].fUpdateLineNumbersInTable(objData->FreeswitchData.objChannelData);
          vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(objData->FreeswitchData.objChannelData.iPositionNumber);
          vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay);
          vANIWorktable[intTableIndex].CallData.fRemoveCallPark();
// objData->FreeswitchData.objChannelData.fDisplay();         
          eANIfunction = TRANSFER_CONNECT; // ensure rcc switching
        }
        else {
         vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay);       
         vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(XFER_CONF, vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber-1);
         vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(vANIWorktable[intTableIndex].FreeswitchData, j, intTranVectorIndex, true);
         vANIWorktable[intTableIndex].CallData.fRemoveCallPark();
         eANIfunction = TRANSFER_CONNECT_BLIND; // No rcc switching
        }

        vANIWorktable[intTableIndex].CallData.TransferData.fClear();
        vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
        objData->CallData.TransferData.fClear();
        objData->CallData.fRemoveTransferData(intTranVectorIndex);


        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
        objData->CallData       = vANIWorktable[intTableIndex].CallData;

        objData->CallData.fLoadPosn(objData->FreeswitchData.objChannelData.iPositionNumber);
       // //cout << "370.f second" << endl;
        objData->fSetANIfunction(VALET_JOIN);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                   objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_370f, strDialedNumber);
        enQueue_Message(ANI,objMessage);
        objData->enumANIFunction = eANIfunction; //rcc switching issues
        objData->stringTimeOfEvent = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, objData);

//vANIWorktable[intTableIndex].FreeswitchData.fDisplay();
//vANIWorktable[intTableIndex].CallData.fDisplay();

       
        // if caller is in another table entry then send a disconnect .....
        if (intSecondTableIndex >=0) {
          objData->CallData = vANIWorktable[intSecondTableIndex].CallData;
          objData->CallData.fLoadPosn(0);
          objData->fSetANIfunction(MERGE_CALL, vANIWorktable[intTableIndex].CallData.stringUniqueCallID);         
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 396, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                     objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_396, vANIWorktable[intTableIndex].CallData.stringUniqueCallID);
          enQueue_Message(ANI,objMessage);
 
          objData->enumANIFunction = DISCONNECT; 
          sem_post(&sem_tMutexANIWorkTable);
          return true;
        }
        else {
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }

   case FREESWITCH_NON_VALET_PICKUP:
        // This is from a Parked call not using the Park Softkey (blind transfer). 
        // Data passed in is objData->FreeswitchData.objChannelData
        // objData->CallData.TransferData.  The UUID of the call will not match the table, we will match by the parking lot number.
        //This case statement may be no sequiter ................
       // //cout << "ani - non valet pickup" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable case FREESWITCH_NON_VALET_PICKUP", 1);}
        szTable = vANIWorktable.size();
        intTableIndex = -1;
        intSecondTableIndex = -1;

       // objData->FreeswitchData.objChannelData.fDisplay();
        for (unsigned int i = 0; i < szTable; i++) {
        intTranVectorIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objChannelData.strTranData);
        boolTranChannelMatch      =  (intTranVectorIndex >= 0);
        if (boolTranChannelMatch) {intTableIndex= i; break;}
        }
        if (intTableIndex < 0){
        sem_post(&sem_tMutexANIWorkTable);
        return false;
        }
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData =  objData->FreeswitchData.objChannelData; 
        strDialedNumber = objData->FreeswitchData.objChannelData.strTranData;
        objData->FreeswitchData.objChannelData.boolConnected = true;        
        objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);

        if (objData->FreeswitchData.objChannelData.iPositionNumber){
         //Position has grabbed the call from the parking lot
          vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay);
          vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, objData->FreeswitchData.objChannelData.iPositionNumber);

          vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);
          eANIfunction = TRANSFER_CONNECT; // ensure rcc switching
        }
        else {
        // //cout << "non valet else" << endl;
        // another caller has grabbed the call from the parking lot
        // check if the caller is in another table entry (i.e. he was transfered here ...)
         intSecondTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneltwoID); 
             
         vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay);       
         vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(XFER_CONF, vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber-1);
         vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);
         eANIfunction = TRANSFER_CONNECT_BLIND; // No rcc switching
        }

       vANIWorktable[intTableIndex].CallData.TransferData.fClear();
       vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
       objData->CallData.TransferData.fClear();
       objData->CallData.fRemoveTransferData(intTranVectorIndex);
       objData->enumANIFunction = eANIfunction;

       objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
       objData->CallData       = vANIWorktable[intTableIndex].CallData;

       objData->CallData.fLoadPosn(objData->FreeswitchData.objChannelData.iPositionNumber);
  //     //cout << "370.f first" << endl;
       objData->fSetANIfunction(VALET_JOIN);
       objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                  objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_370f, strDialedNumber);
       enQueue_Message(ANI,objMessage);
       objData->stringTimeOfEvent = objMessage.stringTimeStamp;
       enqueue_Main_Input(ANI, objData);

        sem_post(&sem_tMutexANIWorkTable);
        return false;

   case FREESWITCH_VALET_PARK_MSG:
        // Channel has entered the parking lot ..................
        // We match the objData->FreeswitchData.objChannelData to a transfer data vector channel and add the valet-extension 
        // Upgraded to look for transfer Target UUID as well.
        // We keep the transfer data so that the channel killer will not kill the channel
        // In the conference vector a clone of the channel already exist due to the xfer.  Update the data to Parkinglot with callback of Valet Extension 
        // TranData will have the Valet transfer to number
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable case FREESWITCH_VALET_PARK_MSG", 1);}
        intTableIndex = -1;
        intTranVectorIndex = -1;
       // //cout << "FREESWITCH_VALET_PARK_MSG" << endl;
       // //cout << objData->FreeswitchData.objChannelData.strChannelName << endl;
       // //cout << objData->FreeswitchData.objChannelData.strChannelID << endl;
       // //cout << objData->FreeswitchData.objChannelData.strCallerID << endl;
       // //cout << objData->FreeswitchData.objChannelData.strCustName << endl;
       // //cout << objData->FreeswitchData.objChannelData.strTranData << endl;

        szTable = vANIWorktable.size();
        for (unsigned int i = 0; i < szTable; i++){
         intTranVectorIndex = vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objChannelData.strChannelName, objData->FreeswitchData.objChannelData.strChannelID);
         if (intTranVectorIndex >= 0) {intTableIndex = i; break;}
         intTranVectorIndex = vANIWorktable[i].CallData.fFindTransferDataWithTransferTarget(objData->CallData.TransferData.strTransferTargetUUID);
         if (intTranVectorIndex >= 0) {intTableIndex = i; break;}         
        }
        if ((intTranVectorIndex < 0)||(intTableIndex < 0)){
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }

        vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strValetExtension = objData->FreeswitchData.objChannelData.strValetExtension;

        //find Temp Channel in Conf Vector leave xfer data In (note the UUIDs are identical ...)
        intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fFindCallbackwithChannelIDinConferenceVector(objData->FreeswitchData.objChannelData.strTranData, 
                                                                                                               objData->FreeswitchData.objChannelData.strChannelID);
        if ((intConfVectorIndex < 0)||(intTableIndex < 0)){
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }
        // Make Temp channel mimick parking Lot to GUI ... set lot to onhold true 

        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolParked = true;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolConnected = true;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strCustName = "Call Park";
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strCallerID = objData->FreeswitchData.objChannelData.strValetExtension;;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strTranData = objData->FreeswitchData.objChannelData.strTranData;;

        // find parked channel 
        intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fFindChannelIDinConferenceVector(objData->FreeswitchData.objChannelData.strChannelID);         
        if ((intConfVectorIndex < 0)||(intTableIndex < 0)){
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }

        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].boolParked = true;
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strValetExtension = objData->FreeswitchData.objChannelData.strValetExtension;
        vANIWorktable[intTableIndex].CallData.fSetCallPark(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strConfDisplay,
                                                           objData->FreeswitchData.objChannelData.strValetExtension);

        objData->CallData = vANIWorktable[intTableIndex].CallData;
        // need to send position of call in lot
        objData->CallData.fLoadPosn(vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].iPositionNumber);
        
        objData->fSetANIfunction(VALET_PARK); 
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                  objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_370g, objData->FreeswitchData.objChannelData.strValetExtension,
                                  vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID );
        enQueue_Message(ANI,objMessage);
        
 //       //cout << "updated valet extension ->" << objData->FreeswitchData.objChannelData.strValetExtension << endl;
        SendANIToMain(VALET_PARK, vANIWorktable[intTableIndex], vANIWorktable[intTableIndex].intANIPortCallReceived);
        sem_post(&sem_tMutexANIWorkTable);
        return false;

   case FREESWITCH_VALET_EXIT_MSG:
       // //cout << "VALET EXIT" << endl;
        // Call has timed out in the lot and will ring back
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable case FREESWITCH_VALET_EXIT_MSG", 1);}

//find by uuid ?????
        szTable = vANIWorktable.size();
        for (unsigned int i = 0; i < szTable; i++){
         intTranVectorIndex = vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objChannelData.strChannelName, objData->FreeswitchData.objChannelData.strChannelID);
         if (intTranVectorIndex >= 0) {intTableIndex = i; break;}
        }
        if ((intTranVectorIndex < 0)||(intTableIndex < 0)){
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }
////cout << "FOUND IT " << endl;
      //  objData->FreeswitchData.objChannelData.fDisplay();
        vANIWorktable[intTableIndex].CallData.fRemoveCallPark();
        vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod = mRING_BACK;
        vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].boolIsRingBack = true;

        sem_post(&sem_tMutexANIWorkTable);
        return false; 
    

   case FREESWITCH_TRANSFER_RINGBACK_CONNECT:
        // This is from a Blind transfer that failed and rang back and was anwered.
        // Data passed in is objData->FreeswitchData.objLinkData
        // objData->CallData.TransferData.  The UUID of the call should match objLinkData.strChanneltwoID. 
        // objLinkData.strChanneloneID should be the channel that picked up the ringback.				

        intSecondTableIndex = -1;

       // objData->FreeswitchData.objLinkData.fDisplay();

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable case FREESWITCH_TRANSFER_RINGBACK_CONNECT", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objLinkData.strChanneltwoID);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable); return false;} 

        intChannelOneTranIndex       =  vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChannelone, objData->FreeswitchData.objLinkData.strChanneloneID);
        boolTranChannelOneMatch      =  (intChannelOneTranIndex >= 0);
        intChannelTwoTranIndex       =  vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChanneltwo, objData->FreeswitchData.objLinkData.strChanneltwoID);
        boolTranChannelTwoMatch      =  (intChannelTwoTranIndex >= 0);
        if (!(boolTranChannelOneMatch||boolTranChannelTwoMatch)) {sem_post(&sem_tMutexANIWorkTable); return false;} 
        if (boolTranChannelOneMatch)                             {intTranVectorIndex = intChannelOneTranIndex; j = 2;}
        else                                                     {intTranVectorIndex = intChannelTwoTranIndex; j = 1;}
        // j points to the "new" channel.
        ////cout << "New channel is link num -> " << j << endl;
        objData->FreeswitchData.objChannelData.fLoadLinkDataIntoChannelData(objData->FreeswitchData.objLinkData, j);
        vANIWorktable[intTableIndex].FreeswitchData.objLinkData    =  objData->FreeswitchData.objLinkData;
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData =  objData->FreeswitchData.objChannelData; 

        if (!objData->FreeswitchData.objChannelData.iPositionNumber) {
         SendCodingError("ani.cpp - coding error case FREESWITCH_TRANSFER_RINGBACK_CONNECT: Position did not answer call !!!");
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }
        //We need to remove transferData and Temp Conf channel Data (will have dup UUID)
        // and add the position also Line numbers may have changed from phone ....

         vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, objData->FreeswitchData.objChannelData.iPositionNumber);
         vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(vANIWorktable[intTableIndex].FreeswitchData, j, intTranVectorIndex, false);
         //Line numbers may be wrong ... They can shift from RingBack
         vANIWorktable[intTableIndex].fUpdateLineNumbersInTable(objData->FreeswitchData.objChannelData);
         vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(objData->FreeswitchData.objChannelData.iPositionNumber);
         vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay);

     // objData->FreeswitchData.objChannelData.fDisplay();         
        eANIfunction = TRANSFER_CONNECT; // ensure rcc switching
        vANIWorktable[intTableIndex].CallData.TransferData.fClear();
        vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
        vANIWorktable[intTableIndex].boolRingBack = false;
        objData->boolRingBack = false;
        objData->CallData.TransferData.fClear();
        objData->CallData.fRemoveTransferData(intTranVectorIndex);

        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
        objData->CallData       = vANIWorktable[intTableIndex].CallData;

        objData->CallData.fLoadPosn(objData->FreeswitchData.objChannelData.iPositionNumber);

       // //cout << "370.f second" << endl;
        objData->fSetANIfunction(RING_BACK_CONNECT);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, 
                                   objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_370e);
        enQueue_Message(ANI,objMessage);

        objData->enumANIFunction = eANIfunction; //rcc switching
        objData->stringTimeOfEvent = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, objData);

       sem_post(&sem_tMutexANIWorkTable);
       return false;

   case FREESWITCH_CHANNEL_ANSWER_UPDATE_UNATTENDED_BLIND_TRANSFER:
        // This is from a Polycom attended transfer turning into a blind transfer to an audiocodes gateway.
        // Data passed in is objData->FreeswitchData.objLinkData

        //  //cout << "ANI -> Update unattended blind transfer" << endl;
        //objData->FreeswitchData.objLinkData.fDisplay();

       intRC = sem_wait(&sem_tMutexANIWorkTable);
       if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable case FREESWITCH_CHANNEL_ANSWER_UPDATE_UNATTENDED_BLIND_TRANSFER", 1);}
       intTableIndex = -1;
       boolIDmatch = false;
       szTable = vANIWorktable.size();

       if (BothChannelsAlreadyPresentandConnected(objData->FreeswitchData)) {sem_post(&sem_tMutexANIWorkTable); return false;}  
       for (unsigned int i = 0; i < szTable; i++)  { 
         intDestVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objLinkData.strChanneltwoID );
         boolDestChannelMatch         =  (intDestVectorIndex >= 0);
         intTranVectorIndex           =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strCallerIDone);
         boolTranChannelOneMatch      =  (intTranVectorIndex >= 0);
         if (boolDestChannelMatch||boolTranChannelOneMatch) { boolIDmatch=true; intTableIndex= i; break;}        
       }

 //       if(boolDestChannelMatch)       {//cout << "dest  channel connect" << endl;}
 //       if(boolTranChannelOneMatch)    {//cout << "Tran  channel One connect" << endl;}

        //should get both channel match
        if (!boolTranChannelOneMatch)                                                                                           {sem_post(&sem_tMutexANIWorkTable); return false;}
        if (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod != mPOORMAN_BLIND_TRANSFER) {sem_post(&sem_tMutexANIWorkTable); return false;}     
        
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->CallData.fLoadPosn(0); 
        strDialedNumber = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber;
        intPosition     = char2int(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.c_str());
        vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 395, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                   objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_370c, strDialedNumber, 
                                   vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
        objData->CallData.fLoadPosn(intPosition); 
        eANIfunction = TRANSFER_CONNECT_BLIND;

        enQueue_Message(ANI,objMessage);

        objData->stringTimeOfEvent = objMessage.stringTimeStamp;

        vANIWorktable[intTableIndex].CallData.TransferData.fClear();
        vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
        objData->CallData.TransferData.fClear();
        objData->CallData.fRemoveTransferData(intTranVectorIndex);
        objData->enumANIFunction = eANIfunction;
        vANIWorktable[intTableIndex].FreeswitchData.objLinkData =  objData->FreeswitchData.objLinkData;  
        vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(vANIWorktable[intTableIndex].FreeswitchData, 1, intTranVectorIndex, false);
        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;

        enqueue_Main_Input(ANI, objData);

        sem_post(&sem_tMutexANIWorkTable); return false;

        break;

   case FREESWITCH_CHANNEL_ANSWER:
        // here we have a connect from a ring 
        // The Data we will compare in the Table is objlinkdata. Channel One should have the inbound Caller Data, Channel Two should have
        // the Position Channel that Answered.

 //objData->FreeswitchData.objLinkData.fDisplay();
 ////cout << "FREESWITCH CHANNEL ANSWER..in ani............................................................" << endl;
         intRC = sem_wait(&sem_tMutexANIWorkTable);
         if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d1", 1);}
 
         intTableIndex = -1;
         szTable = vANIWorktable.size();
         tempObjLinkData            =  objData->FreeswitchData.objLinkData;

         // check if both channels are already in Table 
         if (BothChannelsAlreadyPresentandConnected(objData->FreeswitchData)) {sem_post(&sem_tMutexANIWorkTable); return false;}  

        for (unsigned int i = 0; i < szTable; i++)
         {
          intPosnVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objLinkData.strChanneltwoID );
          boolPositionChannelMatch     =  (intPosnVectorIndex >= 0);
          intDestVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objLinkData.strChanneltwoID );
          boolDestChannelMatch         =  (intDestVectorIndex >= 0);
          
          intChannelOneTranIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChannelone, objData->FreeswitchData.objLinkData.strChanneloneID);
          boolTranChannelOneMatch      =  (intChannelOneTranIndex >= 0);

          intChannelTwoTranIndex       =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objLinkData.strChanneltwo, objData->FreeswitchData.objLinkData.strChanneltwoID);
          boolTranChannelTwoMatch      =  (intChannelTwoTranIndex >= 0); 

                        
          if (boolDestChannelMatch||boolPositionChannelMatch||boolTranChannelOneMatch||boolTranChannelTwoMatch) { boolIDmatch=true; intTableIndex= i; break;}
         }

        if (!boolIDmatch) {
          intTableIndex = LocateRingChannelIdinTable(objData->FreeswitchData.objLinkData.strChanneltwoID); 
          if (intTableIndex >= 0) {boolRingingChannelMatch = true;}
        }

         if (intTableIndex < 0){sem_post(&sem_tMutexANIWorkTable); return false;}


  //       if(boolDestChannelMatch)       {//cout << "dest  channel connect" << endl;}
  //       if(boolPositionChannelMatch)   {//cout << "posn  channel connect" << endl;}
  //       if(boolTranChannelOneMatch)    {//cout << "Tran  channel One connect" << endl;}
  //       if(boolRingingChannelMatch)    {//cout << "ring  channel connect" << endl;}
  //       if(boolTranChannelTwoMatch)    {//cout << "Tran  channel Two connect" << endl;}
 //        if(boolConfChannelOneMatch)    {//cout << "Conf  channel One connect" << endl;}
 //        if(boolConfChannelTwoMatch)    {//cout << "Conf  channel Two connect" << endl;}

////cout << "CH1 ->" << objData->FreeswitchData.objLinkData.strChannelone << endl;
////cout << "CH2 ->" << objData->FreeswitchData.objLinkData.strChanneltwo << endl;

         // Transfer Connect..................
         if (boolTranChannelOneMatch||boolTranChannelTwoMatch)
          { 
           //Determine Transfer Type .......           
           if(boolTranChannelOneMatch) {intTranVectorIndex = intChannelOneTranIndex; ilinkdatachannelnumber= 2;}
           else                        {intTranVectorIndex = intChannelTwoTranIndex; ilinkdatachannelnumber= 1;}

           vANIWorktable[intTableIndex].CallData.TransferData.eTransferMethod = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod;

           switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
            {
             case mATTENDED_TRANSFER:

                  boolCallerIDsAreReversed = false;
                  objData->FreeswitchData.objChannelData.fLoadGUILineView(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromChannel);
                  objData->FreeswitchData.objChannelData.fLoadLineNumberFromChannel(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromChannel, 
                                                                           char2int(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.c_str()));
                  break;

             case mBLIND_TRANSFER:

                  boolCallerIDsAreReversed = false;

                  break;

             case mPOORMAN_BLIND_TRANSFER:
                  break;

             case mRING_BACK:
                  boolCallerIDsAreReversed = false;
                  // load linkdata into freeswitch object ..... 
                   objData->FreeswitchData.objChannelData.fLoadLinkDataIntoChannelData(objData->FreeswitchData.objLinkData, ilinkdatachannelnumber, boolCallerIDsAreReversed);
                  break;
            
             default:
                   SendCodingError( "ANI .. Error in Case Switch(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod) in FREESWITCH_CHANNEL_ANSWER");
            }

           strDialedNumber = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber;

           vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData, ilinkdatachannelnumber, intTranVectorIndex, boolCallerIDsAreReversed);
           vANIWorktable[intTableIndex].fUpdateLineNumbersInTable(objData->FreeswitchData.objChannelData);
           vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(objData->FreeswitchData.objChannelData.iPositionNumber);

           // if no positions on attended xfer call then is poor man blind transfer
           boolA = vANIWorktable[intTableIndex].CallData.ConfData.fAnyPositionsActiveOnCall();
           boolB = vANIWorktable[intTableIndex].CallData.ConfData.fAnyPositionsOnHold();
           boolC = (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod == mATTENDED_TRANSFER);
 
           if (boolC && (!( (boolA)||(boolB) )  ) )          
             {vANIWorktable[intTableIndex].CallData.TransferData.eTransferMethod = mPOORMAN_BLIND_TRANSFER;}

           objData->CallData =  vANIWorktable[intTableIndex].CallData;

           boolTransferTargetisPosition =  (TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressTwo.stringAddress)&&
           TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressOne.stringAddress));


          
       
           //load 00 if outside number dialed or postion number answered (used for logging in switch)
           if      (bool_BCF_ENCODED_IP)          { objData->CallData.fLoadPosn(TelephoneEquipment.fPositionNumberFromChannelName(objData->FreeswitchData.objLinkData.strChannelone));}
           else if (boolTransferTargetisPosition) { objData->CallData.fLoadPosn(TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressOne.stringAddress));
                                                    iTransferConnect = 395;} 
           else                                   { objData->CallData.fLoadPosn(TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressOne.stringAddress));}
           eANIfunction = TRANSFER_CONNECT;
           intPosition = objData->CallData.intPosn;
        //   //cout << "position Channel -> " << boolIsAPositionChannel << "intPosition -> " << intPosition << endl;
        //   //cout << "Transfer Target is Position" << boolTransferTargetisPosition << endl;
        //   //cout << TelephoneEquipment.fPositionNumberFromChannelName(objData->FreeswitchData.objLinkData.strChannelone) << endl;
        //   //cout << TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressTwo.stringAddress) << endl;
        //   //cout << TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressOne.stringAddress) << endl;

           switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
            {
             case mATTENDED_TRANSFER:
                  objData->fSetANIfunction(TRANSFER_CONNECT);
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                             objPortData,  objBLANK_FREESWITCH_DATA,
                                             objBLANK_ALI_DATA,ANI_MESSAGE_370d, strDialedNumber,
                                             objData->CallData.TransferData.strTransferTargetUUID);
                  if (!boolTransferTargetisPosition) {
                   // put transfer from position into Var for transfer to a non position.
                   intPosition = char2int(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.c_str());
                  // //cout << "transfer from is -> " <<  intPosition << endl;
                  }
                  else{
                   //for position to position transfer : 
                   // intPosition is still objData->CallData.intPosn which is link 1 the transfered to position
                   // the position transferiing will be in link 2 
                   ////cout << "position transfering the call -> " << TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressTwo.stringAddress) << endl;
                  }
                  eANIfunction = TRANSFER_CONNECT;
                  break;
             case mBLIND_TRANSFER:
                  if (intPosition) {iTransferConnect = 395; boolTransferTargetisPosition = true;}
                  objData->fSetANIfunction(TRANSFER_CONNECT_BLIND);
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                             objPortData,  objBLANK_FREESWITCH_DATA,
                                             objBLANK_ALI_DATA,ANI_MESSAGE_370b, strDialedNumber, 
                                             objData->CallData.TransferData.strTransferTargetUUID);
                  eANIfunction = TRANSFER_CONNECT_BLIND;
                  break;
             case mRING_BACK:
                  // remove the x2 from the fConferenceStringRemove
                  vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay); 
                  objData->fSetANIfunction(RING_BACK_CONNECT);
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                             objPortData,  objBLANK_FREESWITCH_DATA,
                                             objBLANK_ALI_DATA,ANI_MESSAGE_370e, strDialedNumber, 
                                             objData->CallData.TransferData.strTransferTargetUUID );
                  intPosition = objData->CallData.intPosn;
                  vANIWorktable[intTableIndex].CallData.fLoadPosn(intPosition);
                  vANIWorktable[intTableIndex].boolRingBack = false;
                  eANIfunction = RING_BACK_CONNECT;         
                  break;
             case mPOORMAN_BLIND_TRANSFER:
                  objData->fSetANIfunction(TRANSFER_CONNECT_BLIND); 
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA, 
                                             objBLANK_ALI_DATA, ANI_MESSAGE_370c, strDialedNumber,
                                             objData->CallData.TransferData.strTransferTargetUUID);
                  eANIfunction = TRANSFER_CONNECT_BLIND;
                  break;
             default:

                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA,
                                             objPortData,  objBLANK_FREESWITCH_DATA,
                                             objBLANK_ALI_DATA,ANI_MESSAGE_370, strDialedNumber);
            }
           enQueue_Message(ANI,objMessage);

           objData->stringTimeOfEvent = objMessage.stringTimeStamp;
 //          if (boolTDD_AUTO_DETECT)
 //           {
 //            objData->enumANIFunction = TDD_MODE_ON;
 //            vANIWorktable[intTableIndex].AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objLinkData.strChanneltwo;
 //            Queue_AMI_Input(objData);
 //            sem_post(&sem_tAMIFlag);
//            }

           vANIWorktable[intTableIndex].CallData.TransferData.fClear();
           vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
           objData->CallData.TransferData.fClear();
           objData->CallData.fRemoveTransferData(intTranVectorIndex);
           objData->CallData.fLoadPosn(intPosition); 
           objData->enumANIFunction = eANIfunction;
           
           objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
           if ((boolPositiontoPositionCall)||(boolTransferTargetisPosition)) { objData->FreeswitchData.objLinkData = tempObjLinkData ;}
           enqueue_Main_Input(ANI, objData);
         
           sem_post(&sem_tMutexANIWorkTable);
           return false;
          }// endif if (boolTranChannelOneMatch||boolTranChannelTwoMatch)
////cout << "got here " << endl;
 
         if(boolDestChannelMatch)
          {
          // //cout << "boolDestChannelMatch" << endl;
           sem_post(&sem_tMutexANIWorkTable);
           return false;
          }

         //FSW 20.20.3 bug, preferred fix. race condition  SLA vs built conference. Use media bug & CALL UPDATE for polycom phone barge not CHANNEL_ANSWER     
         if (vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() >= 2) {
           // //cout << "need to be conferenceing" << endl;
           sem_post(&sem_tMutexANIWorkTable);
           return false;
         }
         /*--------------------------------------------------------------------------- */ 
                      

////cout << "got here 1" << endl;
         // check if it was an inbound call (RING Connect)
         if (!vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.boolIsOutbound)
          {

           objData->enumANIFunction = CONNECT; 

           



           //FSW 20.20.3 bug This fix repairs multiple duplicate position channels in conf vector during repeated SLA phone barges "duplicate position channel push" warning precedes.
           // although not needed, it does not hurt to have.  Fix is in in previous 20.20.3 if statement.   
           if (vANIWorktable[intTableIndex].FreeswitchData.fLoad_Bridge_Data_Into_Object( objData->FreeswitchData.objLinkData, 2,  
                                                                                          vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber ) >= 0) {
            vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);
           }
           // Remove DUP Conf Vector Position channels //FSW 20.20.3 bug 
           if (vANIWorktable[intTableIndex].FreeswitchData.objChannelData.iPositionNumber) {
            vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.iPositionNumber);
           }
           if (vANIWorktable[intTableIndex].FreeswitchData.fLoad_Bridge_Data_Into_Object( objData->FreeswitchData.objLinkData, 1,  
                                                                                          vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber ) >= 0) {
            vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);
           }
           // Remove DUP Conf Vector Position channels //FSW 20.20.3 bug 
           if (vANIWorktable[intTableIndex].FreeswitchData.objChannelData.iPositionNumber) {
            vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.iPositionNumber);
           }

           // Load position into Table (note objChannelData will have the data from link channel 1 from previous function call do not add code between !!!!) 
           //Position will be wrong if MSRP
           if ( objData->FreeswitchData.objChannelData.boolMSRP) {
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadPositionNumber(objData->FreeswitchData.objChannelData.iPositionNumber);
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.boolMSRP = objData->FreeswitchData.objChannelData.boolMSRP;
             vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strPresenceID = objData->FreeswitchData.objChannelData.strPresenceID;                                 
           }

           //If NOT A SHARED LINE GUI LINE VIEW NEEDS TO BE UPDATED ON THE RING DIAL AND THROUGHOUT THE CONF DATA....
           // THE GUI LINE VIEW WILL BE THAT OF THE FIRST RINGING NON SHARED LINE.
           if (!TelephoneEquipment.fLineNumberAtPositionIsShared(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data)) {
            vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadGUILineView(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strChannelName);
            vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.fLoadGUILineView(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strChannelName);
            vANIWorktable[intTableIndex].fUpdateGUILineViewOnly(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.iGUIlineView); 
           } 

           vANIWorktable[intTableIndex].CallData.fLoadPosn( vANIWorktable[intTableIndex].FreeswitchData.objChannelData.iPositionNumber);                    
           vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, vANIWorktable[intTableIndex].CallData.stringPosn, intPortNum,"FREESWITCH_CHANNEL_ANSWER");
           objData->CallData = vANIWorktable[intTableIndex].CallData;  
   
           //Put Caller into Conference string: 
           vANIWorktable[intTableIndex].CallData.fLoadCallBackDisplay(); 
           vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(CALLER_CONF,objData->CallData.intTrunk);

    //     vANIWorktable[intTableIndex].CallData.fDisplay();
          }
         else {
       
           ////cout << "Dial out connect" << endl;
           //Dial Out Connect
           objData->enumANIFunction = CONNECT;
           vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fClear();
           if (bool_BCF_ENCODED_IP) { j = TelephoneEquipment.fPositionNumberFromChannelName(objData->FreeswitchData.objLinkData.strChannelone); }
           else                     { j = TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressOne.stringAddress); }         
           objData->FreeswitchData.objChannelData.fLoadPositionNumber(j);
 
           if (!objData->FreeswitchData.objChannelData.iPositionNumber) { boolIsAPositionChannel = false; objData->FreeswitchData.objChannelData.boolIsOutbound = true;}
           else                                                         { boolIsAPositionChannel = true;  objData->FreeswitchData.objChannelData.boolIsOutbound = false;}
           objData->FreeswitchData.fLoad_Bridge_Data_Into_Object( objData->FreeswitchData.objLinkData, 1,  0, false, "", false );
           //Make corrections to CallID and Name and Conference Display

           objData->FreeswitchData.objChannelData.strCustName=CallerNameLookup(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strTranData);
           objData->FreeswitchData.objChannelData.strCallerID=vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strTranData;

 
           objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
           objData->FreeswitchData.objChannelData.intTrunkType      = vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.intTrunkType;
           objData->FreeswitchData.objChannelData.iGUIlineView      = vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iGUIlineView;
           objData->FreeswitchData.objChannelData.iLineNumber       = vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iLineNumber; 



           if (boolIsAPositionChannel) {
            
             // position to position call
         //     //cout << "G" << endl;  
             strTemp = ANI_CONF_MEMBER_DIAL_OUT_PREFIX;
             strTemp+="1";
             vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, j);
             vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(strTemp); 
             vANIWorktable[intTableIndex].CallData.ConfData.fRemoveTransferFromHistory(strTemp);

             objData->FreeswitchData.objChannelData.strCustName=TelephoneEquipment.fCallerIDnameofTelephonewithPosition(j);
             objData->FreeswitchData.objChannelData.strCallerID=TelephoneEquipment.fCallerIDnumberofTelephonewithPosition(j);
             vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strCustName=TelephoneEquipment.fCallerIDnameofTelephonewithPosition
                                                                                                          (vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iPositionNumber);
             vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strCallerID=TelephoneEquipment.fCallerIDnumberofTelephonewithPosition
                                                                                                          (vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iPositionNumber);

             //Update ringdial gui line view and line number 

 
             
             //add positions to vector ....
        // //cout << "g.h" << endl;              
             vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data);
             vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData);

             // update conference vector
             intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceDataFromConfDisplay(strTemp);
             if (intConfVectorIndex >= 0) {
              vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex] = objData->FreeswitchData.objChannelData;
             }
             else {
             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(objData->FreeswitchData.objChannelData);
             vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(j);
             }
             
             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data);
             vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iPositionNumber);

             vANIWorktable[intTableIndex].CallData.fLoadPosn(TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objLinkData.IPaddressOne.stringAddress));
             vANIWorktable[intTableIndex].fSetANIfunction(CONNECT);
             objMessage.fMessage_Create(LOG_CONSOLE_FILE, 395, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, objBLANK_TEXT_DATA,
                                        objPortData,  objBLANK_FREESWITCH_DATA,
                                        objBLANK_ALI_DATA,ANI_MESSAGE_395,  
                                        vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strTranData);            
             vANIWorktable[intTableIndex].enumANIFunction        = CONNECT;
             vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
             vANIWorktable[intTableIndex].boolConnect            = true;
             enQueue_Message(ANI,objMessage);
             enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);
             sem_post(&sem_tMutexANIWorkTable);return false; 
           }
           else {                                                                             
            // position to outside call
            vANIWorktable[intTableIndex].FreeswitchData.vectDestChannelsOnCall.push_back(objData->FreeswitchData.objChannelData);
      //  //cout << "i" << endl;  
            vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data);

            intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceDataFromCallIDNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strTranData);
            if (intConfVectorIndex < 0) {
             intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceDataFromConfDisplay(objData->FreeswitchData.objChannelData.strConfDisplay);
            }
            if (intConfVectorIndex >=0) {
             vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex] = objData->FreeswitchData.objChannelData;
            }
            // Load position into Table (note objChannelData will have the data from link channel 1 from previous function call do not add code between !!!!)                 
            vANIWorktable[intTableIndex].CallData.fLoadPosn( vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iPositionNumber);                    
            vANIWorktable[intTableIndex].fLoad_Position(SOFTWARE, ANI, vANIWorktable[intTableIndex].CallData.stringPosn, intPortNum,"FREESWITCH_CHANNEL_ANSWER");
            vANIWorktable[intTableIndex].CallData.fLoadCallBackDisplay(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strTranData);
            objData->CallData = vANIWorktable[intTableIndex].CallData;  
           }             


         }

       //  //cout << "H" << endl;  
       // Check size of call !!!! might be a conference join
       if ((vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() > 2)&&(objData->enumANIFunction == CONNECT)) {
        //FSW 20.20.3 Issue Do we build the conference or Let FSW keep as an SLA ???
        objData->enumANIFunction = CONFERENCE_JOIN;        
       }



         ////cout << "got here 2" << endl;
         sem_post(&sem_tMutexANIWorkTable);return true;

  case HANGUP:
        // In this case the Pertinent/current Freeswitch Data Passed in is :
        // FreeswitchData.objChannelData.strChannelName
        // FreeswitchData.objChannelData.strChannelID
        
      // //cout << "got a hangup-" << objData->FreeswitchData.objChannelData.strChannelID << endl;
      // objData->FreeswitchData.objChannelData.fDisplay();

        boolTrunkChannelMatch = boolDestChannelMatch = boolIDmatch = false;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}
        szTable = vANIWorktable.size();
////cout << "size = " << szTable << endl; 

        for (unsigned int i = 0; i < szTable; i++)
         {
          boolRingingChannelMatch      = (vANIWorktable[i].FreeswitchData.objRing_Dial_Data.strChannelID == objData->FreeswitchData.objChannelData.strChannelID);
          intPosnVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objChannelData.strChannelID );
          boolPositionChannelMatch     =  (intPosnVectorIndex >= 0);
          intDestVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objChannelData.strChannelID );
          boolDestChannelMatch         =  (intDestVectorIndex >= 0);
          intTranVectorIndex           =  vANIWorktable[i].CallData.fFindTransferData(objData->FreeswitchData.objChannelData.strChannelName, objData->FreeswitchData.objChannelData.strChannelID);
          boolTranChannelMatch         =  (intTranVectorIndex >= 0);
          intConfVectorIndex           =  vANIWorktable[i].CallData.ConfData.fFindConferenceData(objData->FreeswitchData.objChannelData.strChannelID);
          boolConfChannelMatch         =  (intConfVectorIndex >= 0);
          if (boolDestChannelMatch||boolPositionChannelMatch||boolTranChannelMatch||boolRingingChannelMatch||boolConfChannelMatch) { boolIDmatch=true; j= i; break;}
         }

        if (!boolIDmatch) {CheckForUnAttendedTransfer(objData->FreeswitchData.objChannelData);}

        if (!boolIDmatch){sem_post(&sem_tMutexANIWorkTable); return false; } 

        // Set boolean for position to position transfers in Conf Vector ......
//        if (boolConfChannelMatch) {boolDupPosConfChannelMatch   =  vANIWorktable[j].CallData.ConfData.fFindSamePositionInConfvector(objData->FreeswitchData.objChannelData.strChannelID);} 

//        //cout << "Ringing Channel Match  = " << boolRingingChannelMatch << endl;
//        //cout << "Transfer Match         = " << boolTranChannelMatch << endl;
//        //cout << "Dest Channel Match     = " << boolDestChannelMatch << endl;
//        //cout << "Conf Channel Match     = " << boolConfChannelMatch << endl;
//        //cout << "Position Channel Match = " << boolPositionChannelMatch << endl;



        if ((boolDestChannelMatch) && (vANIWorktable[j].boolSMSmessage)) { RemoveAllPostionsFromMSRPCall(j, intPortNum); }
        //Some type of transfer .......
        if((boolTranChannelMatch)&&(!boolDestChannelMatch))
         {
          
         // //cout << "B1" << boolPositionChannelMatch << " B2 " << TransferTargetIsDifferentPostion(vANIWorktable[j].CallData.vTransferData[intTranVectorIndex]) << endl;

          boolA = CheckForUnAttendedTransfer(objData->FreeswitchData.objChannelData);
  //        //cout << "transfer and not dest channel" << endl;
  //        //cout << "Pos match -> " << boolPositionChannelMatch << "Transfer method -> " << vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].eTransferMethod 
   //            << " GUI XFER -> " << mGUI_TRANSFER << " Attended -> " << mATTENDED_TRANSFER << endl;
   //       //cout << "HANGUP CAUSE -> " << objData->FreeswitchData.objChannelData.strHangupCause << endl;

          // check if this was a cancelled transfer and set boolean
          if (!vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].boolGUIcancel){
           // IS THE POSITION NUMBER THE SAME AS THE TRANSFER FROM BUT THE UNIQUE ID DIFFERENT and unattended not set?
           boolB = (((unsigned int)objData->FreeswitchData.objChannelData.iPositionNumber) == char2int(vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.c_str()));
           if (((!boolPositionChannelMatch)&&(boolB))&&(!boolA)){
            vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].boolGUIcancel = (objData->FreeswitchData.objChannelData.strHangupCause == FREESWITCH_CLI_VALUE_ORIGINATOR_CANCEL);
           }

          }
          // if channel in both vectors -> indicative of a polycom cancel transfer ..... a Polycom Conference .... or check if GUI Transfer
          if ((boolPositionChannelMatch)||(vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].eTransferMethod == mGUI_TRANSFER)||
              (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].eTransferMethod == mATTENDED_TRANSFER)||
              (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].eTransferMethod == mBLIND_TRANSFER))
             
           {

            switch (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
             {
              case mBLIND_TRANSFER:
                     if (objData->FreeswitchData.objChannelData.strHangupCause == "NORMAL_CLEARING"){
                     // //cout << "bind xfer normal clear ... should get ringback is this a race ?" << endl;
                      sem_post(&sem_tMutexANIWorkTable);return false;
                     }
                     // would only happen upon a failure .....
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, objBLANK_TEXT_DATA,
                                                objPortData,  objBLANK_FREESWITCH_DATA,
                                                objBLANK_ALI_DATA,ANI_MESSAGE_367d,  
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber,
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);

                     if(vANIWorktable[j].FreeswitchData.fNumberofChannelsInCall() == 1)
                      {
                       objData->CallData = vANIWorktable[j].CallData;
                       objData->CallData.fLoadPosn(vANIWorktable[j].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iPositionNumber);
                       objData->enumANIFunction = DISCONNECT;
                       boolSendDisconnect = true;
                      }

                     break;

              //     if (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].boolGUIcancel)
              //      {objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, vANIWorktable[j].CallData, objPortData,  ANI_MESSAGE_367c,  vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber);break;}
              //     else
              //      {objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, vANIWorktable[j].CallData, objPortData,  ANI_MESSAGE_366b,  vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber);break;}
                      
              //        sem_post(&sem_tMutexANIWorkTable);return false;     

              case mATTENDED_TRANSFER: 
                   ////cout << " attended transfer 366" << endl;
                   if (boolConfChannelMatch) { 
                    vANIWorktable[j].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fSetChannelHangup(objData->FreeswitchData.objChannelData);
                   }
                   if (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].boolGUIcancel){
                     vANIWorktable[j].fSetANIfunction(CANCEL_TRANSFER);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                                objBLANK_TEXT_DATA, objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_366a, 
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber , 
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);}
                   else {
                   vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].eTransferMethod = mPOORMAN_BLIND_TRANSFER; //??????
                   boolSendDisconnect = false;
                  // //cout << "fail here ?" << endl;
                   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 369, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                              objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_369, 
                                              vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber,
                                              objData->FreeswitchData.objChannelData.strHangupCause, 
                                              vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
                   
                   }
                   vANIWorktable[j].enumANIFunction = TRANSFER_FAIL;
                   vANIWorktable[j].stringTimeOfEvent      = objMessage.stringTimeStamp;
                   enqueue_Main_Input(ANI, vANIWorktable[j]);                    

                   break;

              case mPOORMAN_BLIND_TRANSFER:
                     ////cout << "poorman 366" << endl;

                   if (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].boolGUIcancel)  {
                     vANIWorktable[j].fSetANIfunction(CANCEL_TRANSFER);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                                objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                                objBLANK_ALI_DATA,ANI_MESSAGE_366a,  
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber);
                     break;
                   }
                   else  {
                     vANIWorktable[j].fSetANIfunction(TRANSFER_FAIL);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                                objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                                objBLANK_ALI_DATA,ANI_MESSAGE_366a,  
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber);
                     if(vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.length()) {
                      
                       vANIWorktable[j].CallData.fOnHoldPositionRemove(char2int(vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition.c_str()));
                       vANIWorktable[j].UPdate_CallData_Conference_Data_Vector();
                       boolSendHoldOff = true;
                     }     
                   }
                   break;
              case mGUI_TRANSFER: 
                  // //cout << "GUI XFER HANGUP" << endl;
                  // check for hangup cause .... 
                   if (vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].boolGUIcancel){
                     vANIWorktable[j].fSetANIfunction(CANCEL_TRANSFER);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                                objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                                                objBLANK_ALI_DATA, ANI_MESSAGE_367c,  
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber,
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
                   }
                   else {
                     vANIWorktable[j].fSetANIfunction(TRANSFER_FAIL);
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 383, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                                objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                                                objBLANK_ALI_DATA, ANI_MESSAGE_383a,  
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber, 
                                                objData->FreeswitchData.objChannelData.strHangupCause,
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
                     vANIWorktable[j].enumANIFunction = TRANSFER_FAIL;
                     vANIWorktable[j].stringTimeOfEvent      = objMessage.stringTimeStamp;
                     enqueue_Main_Input(ANI, vANIWorktable[j]); 
                  }
/*

changed elseif to else -> becausewe are getting normal clearing on transfer failures .........
                   else if (!(objData->FreeswitchData.objChannelData.strHangupCause == "NORMAL_CLEARING")){
                     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 367, vANIWorktable[j].CallData, objPortData,  ANI_MESSAGE_367r,  
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber, 
                                                objData->FreeswitchData.objChannelData.strHangupCause,
                                                vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
                     vANIWorktable[j].enumANIFunction = TRANSFER_FAIL;
                     vANIWorktable[j].stringTimeOfEvent      = objMessage.stringTimeStamp;
                     enqueue_Main_Input(ANI, vANIWorktable[j]); 
                   }
                   else { 
                     //cout << "race? -> " << objData->FreeswitchData.objChannelData.strHangupCause << endl;
                     sem_post(&sem_tMutexANIWorkTable);return false; // this is indicative of a known race condition involving a Transfer Failure (Hangup comes in prior to FREESWITCH_CHANNEL_ORIGINATE)
                   }
*/
                   break;
              default:
                  // //cout << "default 366" << endl;
                   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                              objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                              objBLANK_ALI_DATA, ANI_MESSAGE_366,  
                                              vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber);
                   break;
             } 
            enQueue_Message(ANI,objMessage);

            vANIWorktable[j].CallData.fConferenceStringRemove(vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strConfDisplay);
            vANIWorktable[j].CallData.fRemoveTransferData(intTranVectorIndex);
            vANIWorktable[j].CallData.TransferData.fClear();
            vANIWorktable[j].FreeswitchData.fRemovePosnChannelFromVector(intPosnVectorIndex);
           // //cout << "E" << endl;
            Check_Conference_Size_On_Hangup(j, vANIWorktable[j], intPortNum);

            vANIWorktable[j].stringTimeOfEvent      = objMessage.stringTimeStamp;
            if(boolSendHoldOff) 
             {
              ////cout << "send hold off !" << endl;
              vANIWorktable[j].enumANIFunction        = HOLD_OFF;
              enqueue_Main_Input(ANI, vANIWorktable[j]); 
             }


            vANIWorktable[j].enumANIFunction        = CANCEL_TRANSFER;
            enqueue_Main_Input(ANI, vANIWorktable[j]); 

            if (boolSendDisconnect)
             {
              sem_post(&sem_tMutexANIWorkTable);return true;       
             }
            else
             {
              sem_post(&sem_tMutexANIWorkTable);return false;
             } 
           } 
          
          //SendCodingError( "ANI in special hangup can we ever get here?");
         // //cout << "ANI in special hangup can we ever get here?" << endl;       

          sem_post(&sem_tMutexANIWorkTable);return false;
         }     

        // if the hangup is only in the conference vector then we have a polycom cancel transfer that had been connected then canceled .... Or a failure of a GUI Transfer
        // or a call park scenario where the call hangs up.
        if ((boolConfChannelMatch)&&(!boolDestChannelMatch)&&(!boolPositionChannelMatch)&&(!boolRingingChannelMatch)) 
         {
          ////cout << "only in conf vector 366" << endl;
          if (vANIWorktable[j].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strHangupCause.empty())
           { 
//vANIWorktable[j].FreeswitchData.fDisplay();
//vANIWorktable[j].CallData.fDisplay();
           // vANIWorktable[j].fSetANIfunction(CANCEL_TRANSFER);
            objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, objBLANK_TEXT_DATA, objPortData,  objBLANK_FREESWITCH_DATA,
                                       objBLANK_ALI_DATA,ANI_MESSAGE_366a,  vANIWorktable[j].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strCallerID);
            enQueue_Message(ANI,objMessage);
   //         //cout << "this one" << endl;
           }
          else
           {
            objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                       objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,"DUMMY MESSAGE to get timestamp");
           }

          vANIWorktable[j].CallData.fConferenceStringRemove(vANIWorktable[j].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strConfDisplay);
          vANIWorktable[j].enumANIFunction        = CANCEL_TRANSFER;
          vANIWorktable[j].stringTimeOfEvent      = objMessage.stringTimeStamp;
          enqueue_Main_Input(ANI, vANIWorktable[j]); 
          sem_post(&sem_tMutexANIWorkTable);return false;
         }


   
        // Dest or Posn Hangup ....................................................            
        objData->CallData = vANIWorktable[j].CallData; 
        if(boolPositionChannelMatch)
         {
         // //cout << "position Hangup" << endl;
          CheckForUnAttendedTransfer(objData->FreeswitchData.objChannelData);
          objData->CallData.fLoadPosn(vANIWorktable[j].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iPositionNumber);        
          vANIWorktable[j].CallData.fConferenceStringRemove(vANIWorktable[j].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].strConfDisplay);
          vANIWorktable[j].FreeswitchData.fRemovePosnChannelFromVector(intPosnVectorIndex);
          boolBlindTransferHangup = vANIWorktable[j].CallData.fPerformedABlindTransfer(objData->CallData.intPosn); 
        //  if ( boolBlindTransferHangup) {//cout << "Blind Transfer Hangup !" << endl;}            
         }
        else if(boolDestChannelMatch)
         { 
          ////cout << "dest Hangup" << endl;
          objData->CallData.fLoadPosn(0);
          vANIWorktable[j].CallData.TransferData.strTransfertoNumber = vANIWorktable[j].FreeswitchData.vectDestChannelsOnCall[intDestVectorIndex].strCallerID;
          vANIWorktable[j].CallData.fConferenceStringRemove(vANIWorktable[j].FreeswitchData.vectDestChannelsOnCall[intDestVectorIndex].strConfDisplay);
          vANIWorktable[j].FreeswitchData.fRemoveDestChannelFromVector(intDestVectorIndex);              
         }

        if (vANIWorktable[j].boolConnect)     {objData->enumANIFunction = DISCONNECT;}
        else                                  {objData->enumANIFunction = ABANDONED;}
                
        if (vANIWorktable[j].FreeswitchData.fNumberofChannelsInCall() > 0)                                                  
         {
          objData->enumANIFunction = SILENT_ENTRY_LOG_OFF; // //cout << "This silent entry log-off" << endl;
         }

        if (!boolBlindTransferHangup) {
        //  //cout << "D" << endl;
          Check_Conference_Size_On_Hangup(j, vANIWorktable[j], intPortNum);
        }

        // Check_Conference_Size_On_Hangup(j, vANIWorktable[j], intPortNum);
         
        if (intConfVectorIndex >= 0) {
         //update conference channel Data to reflect hangup 
      //   //cout << "HANGUP -> " << objData->FreeswitchData.objChannelData.strHangupCause << endl;
         vANIWorktable[j].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fSetChannelHangup(objData->FreeswitchData.objChannelData);
         objData->CallData.ConfData = vANIWorktable[j].CallData.ConfData;        
        }
        sem_post(&sem_tMutexANIWorkTable);return true; 
      
   case FREESWITCH_MWI_SUMMARY:
        // Send a MWI message to the phone do not need to process further
        objData->enumANIFunction = SEND_MWI;
        Queue_AMI_Input(objData);
        sem_post(&sem_tAMIFlag);
        return false;

   case FREESWITCH_SMS_MESSAGE:
// this may go away .........................
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-d", 1);}
////cout << "FREESWITCH_SMS_MESSAGE" << endl;
        intTableIndex = IndexofActiveSMSConversation( objData->TextData.SMSdata.strFromUser);
        if (intTableIndex < 0)
         {
          // new Text Conversation
          intTableIndex = IndexOfNewANITableEntry(objData);
          if(!vANIWorktable[intTableIndex].CallData.fLoad_Conference_Number(int2str(objConferenceList.AssignConferenceNumber()))) {return false;}
          vANIWorktable[intTableIndex].fSetUniqueCallID(vANIWorktable[intTableIndex].CallData.intConferenceNumber);
          clock_gettime(CLOCK_REALTIME, &vANIWorktable[intTableIndex].timespecTimeRecordReceivedStamp);
          vANIWorktable[intTableIndex].stringRingingTimeStamp = get_time_stamp(vANIWorktable[intTableIndex].timespecTimeRecordReceivedStamp);  
         }

        vANIWorktable[intTableIndex].TDDdata.SMSdata               = objData->TDDdata.SMSdata;
        vANIWorktable[intTableIndex].TextData.SMSdata              = objData->TDDdata.SMSdata;
        vANIWorktable[intTableIndex].ALIData.I3Data.objLocationURI = objData->TDDdata.SMSdata.objLocationURI;
        vANIWorktable[intTableIndex].ALIData.I3Data.strBidId       = objData->TDDdata.SMSdata.strBidId;
        vANIWorktable[intTableIndex].CallData.fSet_PhoneNumberFromANI(TEN_DIGIT_PRECHECKED, PSUEDO_ANI, objData->TDDdata.SMSdata.strFromUser); 
 //       vANIWorktable[intTableIndex].CallData.fLoadCallbackNumber(objData->TDDdata.SMSdata.strFromUser);
        vANIWorktable[intTableIndex].fSetANIfunction(SMS_MSG_RECEIVED);  
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 352, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].fCallData(), vANIWorktable[intTableIndex].TextData, 
                                   objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_352s, objData->TDDdata.SMSdata.strFromUser,
                                   ASCII_String(objData->TDDdata.SMSdata.strSMSmessage.c_str(), objData->TDDdata.SMSdata.strSMSmessage.length()),"","","","",NORMAL_MSG, NEXT_LINE );
        enQueue_Message(ANI,objMessage); 
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp; 
         
        vANIWorktable[intTableIndex].enumANIFunction        = SMS_MSG_RECEIVED;
        vANIWorktable[intTableIndex].TDDdata.SMSdata.strSMSmessage += "\r\n";
        vANIWorktable[intTableIndex].CallData.fAddAllPositionsToConference();
        vANIWorktable[intTableIndex].boolSMSmessage        = true;        

        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]); 

        sem_post(&sem_tMutexANIWorkTable);return false; 
        break;

   case FREESWITCH_MSRP_TEXT_MESSAGE:
////cout << "FREESWITCH_MSRP_TEXT_MESSAGE" << endl;  

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-e", 1);}
        szTable = vANIWorktable.size();

        for (unsigned int i = 0; i < szTable; i++)
         {
          intPosnVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objChannelData.strChannelID );
          boolPositionChannelMatch     =  (intPosnVectorIndex >= 0);
          intDestVectorIndex           =  vANIWorktable[i].FreeswitchData.fFindChannelInDestVector(objData->FreeswitchData.objChannelData.strChannelID );
          boolDestChannelMatch         =  (intDestVectorIndex >= 0);
          if (boolPositionChannelMatch||boolDestChannelMatch) {boolIDmatch=true; intTableIndex = i; break;}
         } 
        if (boolPositionChannelMatch) { return false;}       
//        if (boolPositionChannelMatch) { //cout << "position match" << endl;}
//        if (boolDestChannelMatch)     { //cout << "Dest     match" << endl;}

        if (!boolIDmatch) {
          // New MSRP Message
          objData->CallData.strCustName = objData->FreeswitchData.objChannelData.strCustName;
          objData->CallData.fLoadCallbackNumber(objData->FreeswitchData.objChannelData.strCallerID);
          objData->CallData.fLoadTrunk(objData->FreeswitchData.objChannelData.iTrunkNumber);
          objData->CallData.boolIsOutbound = objData->FreeswitchData.objChannelData.boolIsOutbound;
          objData->CallData.fLoadPosn(0);
          objData->FreeswitchData.objChannelData.fLoadConferenceDisplayC(objData->CallData.intTrunk);
          objData->CallData.fLoadCallBackDisplay(); 
          objData->CallData.fConferenceStringAdd(CALLER_CONF, objData->CallData.intTrunk);         
          objData->FreeswitchData.objRing_Dial_Data = objData->FreeswitchData.objChannelData;
          objData->enumANIFunction = MSRP_RINGING; 
          objData->boolSMSmessage = true;      
          sem_post(&sem_tMutexANIWorkTable);return true;
        }
        vANIWorktable[intTableIndex].boolSMSmessage        = true;        
        vANIWorktable[intTableIndex].TextData              = objData->TextData;

// Location to I3 ?????? Most likely not ........ 
  //      vANIWorktable[intTableIndex].ALIData.I3Data.objLocationURI = objData->TDDdata.SMSdata.objLocationURI;
  //      vANIWorktable[intTableIndex].ALIData.I3Data.strBidId       = objData->TDDdata.SMSdata.strBidId;
  //      vANIWorktable[intTableIndex].CallData.fSet_PhoneNumberFromANI(TEN_DIGIT_PRECHECKED, PSUEDO_ANI, objData->TDDdata.SMSdata.strFromUser); 
 //       vANIWorktable[intTableIndex].CallData.fLoadCallbackNumber(objData->TDDdata.SMSdata.strFromUser);


        //determine if message from position or from caller ........
        intPosition = objData->FreeswitchData.objChannelData.iPositionNumber;
        if (objData->FreeswitchData.objChannelData.iPositionNumber)
         {
          objData->fSetANIfunction(SMS_MSG_SENT);  
          strTemp = "Position " + int2str(objData->FreeswitchData.objChannelData.iPositionNumber);
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 352, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].fCallData(), objData->TextData, 
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_352s, strTemp,
                                     ASCII_String(objData->TDDdata.SMSdata.strSMSmessage.c_str(), 
                                     objData->TDDdata.SMSdata.strSMSmessage.length()),"","","","",NORMAL_MSG, NEXT_LINE );
          enQueue_Message(ANI,objMessage);
          vANIWorktable[intTableIndex].enumANIFunction        = SMS_MSG_SENT;
          vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
          vANIWorktable[intTableIndex].TextData.SMSdata.strSMSmessage += "\r\n"; 

          vANIWorktable[intTableIndex].CallData.fLoadPosn(intPosition);
          // do we add all users ?
   //        vANIWorktable[intTableIndex].CallData.fAddAllPositionsToConference();                
          objData->FreeswitchData.objChannelData.fLoadMSRPPositionChannel(intPosition);

            // put posn into call if not already ...
         if (vANIWorktable[intTableIndex].CallData.fLegendInConferenceChannelDataVector(objData->FreeswitchData.objChannelData.strConfDisplay) < 0){
          vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(objData->FreeswitchData.objChannelData);
          vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, intPosition);           
         }

          enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]); 
         }
         else
         {
         // //cout << "this one ????" << endl;
          objData->fSetANIfunction(SMS_MSG_RECEIVED);  
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 352, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].fCallData(), 
                                     objData->TextData, objPortData, objBLANK_FREESWITCH_DATA,
                                     objBLANK_ALI_DATA,ANI_MESSAGE_352s, objData->TextData.SMSdata.strFromUser,
                                     ASCII_String(objData->TextData.SMSdata.strSMSmessage.c_str(), 
                                     objData->TextData.SMSdata.strSMSmessage.length()),"","","","",NORMAL_MSG, NEXT_LINE );
          enQueue_Message(ANI,objMessage); 
          vANIWorktable[intTableIndex].enumANIFunction = SMS_MSG_RECEIVED; 
          vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
          vANIWorktable[intTableIndex].TextData.SMSdata.strSMSmessage += "\r\n"; 
 
         // do we add all users ?
   //        vANIWorktable[intTableIndex].CallData.fAddAllPositionsToConference();                
 
          enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);        
         }


        sem_post(&sem_tMutexANIWorkTable);return false; 
        break;   



   default: break;
  }



                    

 switch (objData->enumANIFunction)
  {

   case LOCATION_URI:
        // we need to stop this one and add a section to ringing ....... in main 
        //cout << "ANI RET FALSE" << endl;
        return false;

        //cout << "ani   --> Uri Data by reference only" << endl;
        //cout << "bid id -> " << objData->ALIData.I3Data.strBidId << endl; 
        //cout << "NENA CALLID -> " << objData->ALIData.I3Data.strNenaCallId << endl;
        objData->ALIData.I3Data.enumLISfunction = URL_RECIEVED;
        if(IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID) >= 0) {
         cout << "LOCATION_URI is in table" << endl;
        }
        objData->CallData.fLoadTrunk(objData->FreeswitchData.objChannelData.iTrunkNumber);
        enqueue_Main_Input(LIS, objData);              
        return false;

   case IRR_FILE_DATA:

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-g", 1);}
        szTable = vANIWorktable.size();
        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID);
        if (intTableIndex < 0 ) {sem_post(&sem_tMutexANIWorkTable);return false;}
        vANIWorktable[intTableIndex].CallData.fLoadCallRecording(objData->FreeswitchData.objChannelData);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 366, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                   objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA,
                                   objBLANK_ALI_DATA,"DUMMY MESSAGE to get timestamp");
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
        vANIWorktable[intTableIndex].enumANIFunction = UPDATE_ANI_DATA;
        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);

        sem_post(&sem_tMutexANIWorkTable);
        return false;

   case LOCATION_DATA:
         //cout << "ani --> Data by value.............................................................................................." << endl;
        // We send this Direct to the LIS Thread to obtain URN EMS FIRE POLICE ........
        objData->ALIData.I3Data.enumLISfunction =  I3_BY_VALUE;
        if(IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID) >= 0) {
          cout << "LOCATION_DATA is in table" << endl;
         }
        //Queue_LIS_Event(objData); moved back .....
        enqueue_Main_Input(LIS, objData);              
        return false;

   case LEGACY_ALI_DATA:
       //  //cout << "ani --> Legacy ALI Data" << endl;
        objData->ALIData.I3Data.enumLISfunction =  LEGACY_ALI_DATA_RECIEVED;
        if(IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID) >= 0) {
         cout << "LEGACY_ALI_DATA is in table" << endl;
        }
        enqueue_Main_Input(LIS, objData);

        return false;

   case RINGING: 
        ////cout << "ringing dispatch" << endl;
        // We will recieve a message for each dispatch telephone ...Determine if we have a duplicate and discard.
        // We will also see the attempt to ring a line that already has a call on it,
        intPosition = 0;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-g", 1);}
        szTable = vANIWorktable.size();

        //Check for RINGBACK !!!!!!

        if (objData->FreeswitchData.objChannelData.iRingingPosition > 0) {
          ////cout << "ringing position -> " << objData->FreeswitchData.objChannelData.iRingingPosition << endl;
          ////cout << "iGUIlineView     -> " << objData->FreeswitchData.objChannelData.iGUIlineView << endl;
          ////cout << "line number      -> " << objData->FreeswitchData.objChannelData.iLineNumber << endl;
          objData->FreeswitchData.objChannelData.iRingingLineNumber[objData->FreeswitchData.objChannelData.iRingingPosition] = objData->FreeswitchData.objChannelData.iLineNumber;
          objData->CallData.fLoadCallRecording(objData->FreeswitchData.objChannelData);          
        }

        for (unsigned int i = 0; i < szTable; i++)  {
          // for inbound calls we check the rings of the ringdialobject.
          // check if coming from a Parking Lot Ring back
          if(((objData->FreeswitchData.objChannelData.strChannelID == vANIWorktable[i].FreeswitchData.objRing_Dial_Data.strChannelID))||
             (objData->CallData.TransferData.boolIsRingBack)) {

            // add ring line number of position to vANIWorktable[i].FreeswitchData.objRing_Dial_Data
            if (objData->FreeswitchData.objChannelData.iRingingPosition > 0) { 
              vANIWorktable[i].FreeswitchData.objRing_Dial_Data.iRingingLineNumber[objData->FreeswitchData.objChannelData.iRingingPosition] = objData->FreeswitchData.objChannelData.iLineNumber;
            }
            objData->CallData.fReleaseConferenceNumber();
            vANIWorktable[i].CallData.fLoadCallRecording(objData->FreeswitchData.objChannelData);
             ////cout << "ringing siphx stream -> " << objData->FreeswitchData.objChannelData.strCallRecording << endl;
            // set callback if call came in with Legacy ALI
            if (vANIWorktable[i].ALIData.boolALIRecordReceived) {
             vANIWorktable[i].fLoadCallbackfromALI(true);
            } 
                           
            if (vANIWorktable[i].CallData.fTransferDataIsRingback(objData->FreeswitchData.objChannelData.strChannelID)) { 
             vANIWorktable[i].FreeswitchData.objRing_Dial_Data.iGUIlineView = objData->FreeswitchData.objChannelData.iGUIlineView;
             vANIWorktable[i].FreeswitchData.objRing_Dial_Data.iLineNumber = objData->FreeswitchData.objChannelData.iLineNumber;

             if (!vANIWorktable[i].boolRingBack) {
              vANIWorktable[i].fSetANIfunction(RING_BACK);
              vANIWorktable[i].CallData.fLoadPosn(0);
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 316, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[i].CallData, objBLANK_TEXT_DATA,
                                         objPortData,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_316r); 
              enQueue_Message(ANI,objMessage);   
             }
             vANIWorktable[i].boolRingBack = true;
             vANIWorktable[i].enumANIFunction = RING_BACK;
             enqueue_Main_Input(ANI, vANIWorktable[i]);
            }
            else {
             vANIWorktable[i].enumANIFunction = UPDATE_ANI_DATA;
             enqueue_Main_Input(ANI, vANIWorktable[i]);
            }

            sem_post(&sem_tMutexANIWorkTable);return false; 
          }
        } //end for
        if (IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID) >= 0) {
         // objData->FreeswitchData.objChannelData.fDisplay();
 
          objData->CallData.fReleaseConferenceNumber();
          sem_post(&sem_tMutexANIWorkTable);return false; 
        }

        sem_post(&sem_tMutexANIWorkTable);return true; 

   case DIAL_DEST: 
        ////cout << "ani dial dest" << endl; 
        //check for duplicate and update record if so.  otherwise is new call  (do we check if channel is already in table ?
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-g", 1);}
        szTable = vANIWorktable.size();

        for (unsigned int i = 0; i < szTable; i++)
         {
          if((objData->FreeswitchData.objChannelData.strChannelID == vANIWorktable[i].FreeswitchData.objRing_Dial_Data.strChannelID))
           {
            objData->CallData.fReleaseConferenceNumber();
            vANIWorktable[i].CallData.fLoadCallRecording(objData->FreeswitchData.objChannelData);
            vANIWorktable[i].FreeswitchData.objRing_Dial_Data = objData->FreeswitchData.objChannelData;
            if ((objData->FreeswitchData.objChannelData.iRingingPosition > 0) && 
               (objData->FreeswitchData.objChannelData.iRingingPosition != objData->FreeswitchData.objChannelData.iPositionNumber)) {
             // Send RINGING_POSITION_TO_POSITION to Main

             vANIWorktable[i].FreeswitchData.objChannelData = objData->FreeswitchData.objChannelData;
             vANIWorktable[i].enumANIFunction = RINGING_POSITION_TO_POSITION;
             SendANIToMain(RINGING_POSITION_TO_POSITION, vANIWorktable[i], vANIWorktable[i].intANIPortCallReceived);            
            }
            sem_post(&sem_tMutexANIWorkTable);return false; 
           }
         }  
        if (IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID) >= 0)
         {
          objData->CallData.fReleaseConferenceNumber();
          sem_post(&sem_tMutexANIWorkTable);return false; 
         }

        //this is the dial out from a position
        objData->FreeswitchData.objChannelData.iLineNumber = 
                                       TelephoneEquipment.fFindTelephoneLineNumber(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));

        objData->FreeswitchData.objChannelData.iGUIlineView = TelephoneEquipment.fIndexofLineView(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));
        objData->CallData.fSetSharedLineFromLineNumberPlusPosition(objData->FreeswitchData.objChannelData);

        objData->FreeswitchData.objChannelData.strCustName = TelephoneEquipment.fCallerIDnameofTelephonewithPosition
                                                                                                          (objData->FreeswitchData.objChannelData.iPositionNumber);
        objData->FreeswitchData.objChannelData.strCallerID=TelephoneEquipment.fCallerIDnumberofTelephonewithPosition
                                                                                                          (objData->FreeswitchData.objChannelData.iPositionNumber);
        
       sem_post(&sem_tMutexANIWorkTable);return true; 

   case HEARTBEAT:  return true;


 
   case TRANSFER_FAIL:
       // //cout << "ANI transfer Fail" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-g", 1);}

        intTableIndex = IndexWithCallUniqueID(objData->CallData.stringUniqueCallID);
        if (intTableIndex < 0) {
         intTableIndex = IndexofActiveChannelinTable(objData->CallData.TransferData.strTransferMatchingChannelId);
        } 
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 

        intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferTarget(objData->CallData.TransferData.strTransferTargetUUID);
        if (intTranVectorIndex < 0) { 
         intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->CallData.TransferData.strTransfertoNumber, 
                                                                                      char2int(objData->CallData.TransferData.strTransferFromPosition.c_str()));
         } 
        if (intTranVectorIndex < 0) {sem_post(&sem_tMutexANIWorkTable); return false; } 

        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
        switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
         {
          case mGUI_TRANSFER: 
                    objMessage.fMessage_Create(LOG_CONSOLE_FILE, 383, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                               objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_383a,  
                                               vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber, objData->CallData.TransferData.strHangupCause, 
                                               vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
                    break;
          case mNG911_TRANSFER:
                    objMessage.fMessage_Create(LOG_CONSOLE_FILE, 383, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                               objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_383c,  
                                               vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber, objData->CallData.TransferData.strHangupCause, 
                                               vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID);
                    break;
          case mBLIND_TRANSFER:
                    //should be obsolete ....... 
                       SendCodingError( "ani.cpp - coding error: case TRANSFER_FAIL -> case mBLIND_TRANSFER");
                    break;
                    objMessage.fMessage_Create(LOG_CONSOLE_FILE, 383, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                               objBLANK_TEXT_DATA,objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_383b,  
                                               vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransfertoNumber, objData->CallData.TransferData.strHangupCause, 
                                               vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferTargetUUID );
                    // If the number of Channels on the call is less than X ring back the transfer party .....
                    if (vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() == 1)
                     {
                      vANIWorktable[intTableIndex].FreeswitchData.fLoad_Last_Channel_into_Channelobject();
                      
                      vANIWorktable[intTableIndex].enumANIFunction = RING_BACK;
                      Queue_AMI_Input(vANIWorktable[intTableIndex]);
                      sem_post(&sem_tAMIFlag);        
                     }




                    break;
          default:
                    SendCodingError( "ani.cpp - coding error: case TRANSFER_FAIL -> default");
                    objMessage.fMessage_Create(LOG_CONSOLE_FILE, 383, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, objBLANK_TEXT_DATA, objPortData, 
                                               objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_383,  objData->CallData.TransferData.strTransfertoNumber, 
                                               objData->CallData.TransferData.strHangupCause);
         }
        enQueue_Message(ANI,objMessage);

        intConfVectorIndex = -1;
        strTemp = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
        if (!strTemp.empty()) {intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fLegendInConferenceChannelDataVector(strTemp);}
        if (intConfVectorIndex >=0)
         {
          vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strHangupCause = objData->CallData.TransferData.strHangupCause;
         }
    
        vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
      //  //cout << "C" << endl;
        Check_Conference_Size_On_Hangup(intTableIndex, vANIWorktable[intTableIndex], intPortNum, objData->CallData.TransferData.strHangupCause);  

        vANIWorktable[intTableIndex].enumANIFunction = TRANSFER_FAIL;
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);              

        sem_post(&sem_tMutexANIWorkTable);return false;

  case ATT_TRANSFER_REJ:
       //Call was rejected by us due to a transfer attempt from within a conference.
       // ... close attended transfer window of gui & log the failure.
       // we will have a conference name to find in the table ...

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-h1", 1);}

        intTableIndex = IndexWithFreeSwitchConferenceName(objData->CallData.strFreeswitchConfName);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}

        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->CallData.intPosn);
  
        // This is a special circumstance in that we are rejecting the call before it Bridges in freeswitch ... We log the attempt
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 373, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, objBLANK_TEXT_DATA, objPortData, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_373,  objData->CallData.TransferData.strTransfertoNumber,
                                   objData->CallData.TransferData.strTransferTargetUUID   );
        enQueue_Message(ANI,objMessage);
        // and then log the failure ....
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 369, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, objBLANK_TEXT_DATA, objPortData, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_369,  
                                   objData->CallData.TransferData.strTransfertoNumber, "CALL_REJECTED", objData->CallData.TransferData.strTransferTargetUUID );         
        enQueue_Message(ANI,objMessage);       
 
       // send message to controller to close the transfer window ...

        vANIWorktable[intTableIndex].enumANIFunction =  TRANSFER_FAIL;
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);              

       sem_post(&sem_tMutexANIWorkTable);return false;

  case ATT_TRANSFER_FAIL:
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-g", 1);}

        szTable = vANIWorktable.size();
        for (unsigned int i = 0; i < szTable; i++)
         {
          
          intTranVectorIndex       =  vANIWorktable[i].CallData.fFindTransferData("NULL", objData->CallData.TransferData.strTransferFromUniqueid );
          boolTranChannelMatch     =  (intTranVectorIndex >= 0);
          if (boolTranChannelMatch)   {  j= i; break;}
         }       

        if (!boolTranChannelMatch){sem_post(&sem_tMutexANIWorkTable);return false; } 

        vANIWorktable[j].CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 369, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[j].CallData, 
                                   objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_369,  
                                   objData->CallData.TransferData.strTransfertoNumber, objData->CallData.TransferData.strHangupCause,
                                   objData->CallData.TransferData.strTransferTargetUUID  );         
        enQueue_Message(ANI,objMessage);
        intConfVectorIndex = -1;
        strTemp = vANIWorktable[j].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
        if (!strTemp.empty()) {intConfVectorIndex = vANIWorktable[j].CallData.fLegendInConferenceChannelDataVector(strTemp);}
        if (intConfVectorIndex >=0)
         {
          vANIWorktable[j].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strHangupCause = objData->CallData.TransferData.strHangupCause;
         }
        vANIWorktable[j].CallData.fRemoveTransferData(intTranVectorIndex);
        vANIWorktable[j].enumANIFunction = TRANSFER_FAIL;
        vANIWorktable[j].stringTimeOfEvent      = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, vANIWorktable[j]);

        sem_post(&sem_tMutexANIWorkTable);return false;

      
   case ATTENDED_TRANSFER:
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-g", 1);}
////cout << "attended xfer" << endl;
        intTableIndex = IndexofActiveChannelinTable(objData->CallData.TransferData.strTransferMatchingChannelId);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}

 // vANIWorktable[intTableIndex].CallData.TransferData.fDisplay();

        vANIWorktable[intTableIndex].CallData.TransferData =  objData->CallData.TransferData;
        vANIWorktable[intTableIndex].CallData.TransferData.fLoadConferenceDisplayX(vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);
        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(XFER_CONF,vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);        
        vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber++;
        vANIWorktable[intTableIndex].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber);
        vANIWorktable[intTableIndex].CallData.vTransferData.push_back(vANIWorktable[intTableIndex].CallData.TransferData);
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[intTableIndex].CallData.TransferData);
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadGUILineView(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strChannelName);
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadLineNumberFromChannel(vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strChannelName, 
                                                                                              char2int(objData->CallData.TransferData.strTransferFromPosition.c_str()));
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].CallData.intTrunk);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);
      
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);      
        
        vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber = objData->CallData.TransferData.strTransfertoNumber;
        vANIWorktable[intTableIndex].enumANIFunction = DIAL_DEST;
        objData->fSetANIfunction(ATTENDED_TRANSFER);  
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 373, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA, 
                                   objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_373, 
                                   objData->CallData.TransferData.strTransfertoNumber, 
                                   objData->CallData.TransferData.strTransferTargetUUID);
        enQueue_Message(ANI,objMessage);
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
  //      vANIWorktable[intTableIndex].enumANIFunction = ATTENDED_TRANSFER;
        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);   
        sem_post(&sem_tMutexANIWorkTable);return false;
 

   case CONFERENCE_ATT_TRANSFER_FAIL:
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-h", 1);}

        intTableIndex = IndexWithFreeSwitchConferenceName(objData->CallData.TransferData.strTransferMatchingChannelId);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 

        intTranVectorIndex       =  vANIWorktable[intTableIndex].CallData.fFindTransferData("NULL", objData->CallData.TransferData.strTransferFromUniqueid );
        if (intTranVectorIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; }             

        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 369, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, objBLANK_TEXT_DATA, objPortData,  
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_369,  objData->CallData.TransferData.strTransfertoNumber,
                                   objData->CallData.TransferData.strHangupCause);         
        enQueue_Message(ANI,objMessage);
        intConfVectorIndex = -1;
        strTemp = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
        if (!strTemp.empty()) {intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fLegendInConferenceChannelDataVector(strTemp);}
        if (intConfVectorIndex >=0)
         {
          vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].strHangupCause = objData->CallData.TransferData.strHangupCause;
         }
        vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);


        sem_post(&sem_tMutexANIWorkTable);return false;

   case CONFERENCE_ATT_XFER:
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-h", 1);}

        intTableIndex = IndexWithFreeSwitchConferenceName(objData->CallData.TransferData.strTransferMatchingChannelId);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false; } 

        vANIWorktable[intTableIndex].CallData.TransferData =  objData->CallData.TransferData;
        vANIWorktable[intTableIndex].CallData.TransferData.fLoadConferenceDisplayX(vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);
        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(XFER_CONF,vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);        
        vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber++;
        vANIWorktable[intTableIndex].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber);
        vANIWorktable[intTableIndex].CallData.vTransferData.push_back(vANIWorktable[intTableIndex].CallData.TransferData);
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[intTableIndex].CallData.TransferData);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
  
        vANIWorktable[intTableIndex].enumANIFunction = DIAL_DEST;         
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 373, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA, objPortData, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_373,  objData->CallData.TransferData.strTransfertoNumber);
        enQueue_Message(ANI,objMessage);      
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]); 
        sem_post(&sem_tMutexANIWorkTable);return false;
 
        
   case BLIND_TRANSFER:
       // //cout << "blind transfer from phone " << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-h", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->CallData.TransferData.strTransferFromUniqueid);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}


        //find position from matching channel id ....
        intConfVectorIndex = vANIWorktable[intTableIndex].CallData.ConfData.fFindConferenceData(objData->CallData.TransferData.strTransferMatchingChannelId);
        if (intConfVectorIndex < 0) {
        // //cout << "matching UUID not found !" << endl;
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }       
        intPosition = vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].iPositionNumber;
        if (intPosition <= 0) {
        // //cout << "Position not found !" << endl;
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }
       // objData->CallData.TransferData.IPaddress.stringAddress = TelephoneEquipment.fIPaddressFromPositionNumber(intPosition);
        objData->CallData.TransferData.fLoadFromPosition(intPosition);
        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->CallData.TransferData.strTransferFromPosition);
        vANIWorktable[intTableIndex].CallData.TransferData = objData->CallData.TransferData;
        vANIWorktable[intTableIndex].CallData.TransferData.fLoadConferenceDisplayX(vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);
        vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(XFER_CONF,vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);        
        vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber++;
        vANIWorktable[intTableIndex].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber);
        vANIWorktable[intTableIndex].CallData.vTransferData.push_back(vANIWorktable[intTableIndex].CallData.TransferData);
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[intTableIndex].CallData.TransferData);
        vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intTableIndex].FreeswitchData.objChannelData);

        vANIWorktable[intTableIndex].fSetANIfunction(BLIND_TRANSFER); 
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 392, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                   objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                   ANI_MESSAGE_392,  vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber,
                                   objData->CallData.TransferData.strTransferTargetUUID );
        enQueue_Message(ANI,objMessage);

        vANIWorktable[intTableIndex].enumANIFunction        = BLIND_TRANSFER;          
        vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
        enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);
        sem_post(&sem_tMutexANIWorkTable);return false; 

   case RETRY_CONF_BLIND_TRANSFER:

        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-h", 1);}

        intTableIndex = IndexofActiveChannelinTable(objData->CallData.TransferData.strTransferFromUniqueid);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}
        ////cout << "RETRY_CONF_BLIND_TRANSFER" << endl;      
        objCallID.fClear();
        objCallID = vANIWorktable[intTableIndex].CallerIDandNamefromConference();
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strCallerID = objCallID.strCallerID;
        vANIWorktable[intTableIndex].FreeswitchData.objChannelData.strCustName = objCallID.strCustName;

        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
        objData->intWorkStation = objData->CallData.intPosn;
        SendTransferToAMI(objData, intTableIndex, mBLIND_TRANSFER, objPortData);
            

    //    //cout << "retry Blind Transfer" << endl;

        sem_post(&sem_tMutexANIWorkTable);return false; 
      
   case RING_BACK:
      //  //cout << "ringback in ani" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-h", 1);}
        
        intTableIndex = IndexofActiveChannelinTable(objData->CallData.TransferData.strTransferFromUniqueid);
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}

        intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData("NULL" , objData->CallData.TransferData.strTransferFromUniqueid);
        if(intTranVectorIndex >=0){

          vANIWorktable[intTableIndex].CallData.fConferenceStringRemove(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay);
          vANIWorktable[intTableIndex].enumANIFunction        = CANCEL_TRANSFER;          
          vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
          enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);
        }   
        else { 
         SendCodingError("ani.cpp - coding error: case RING_BACK -> intTranVectorIndex < 0"); 
         sem_post(&sem_tMutexANIWorkTable);
         return false;
        }
        //transfer fail message ..... 
        vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_FAIL);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 369, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, objBLANK_TEXT_DATA, objPortData, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_369b,  objData->CallData.TransferData.strTransfertoNumber, 
                                   objData->CallData.TransferData.strHangupCause, objData->CallData.TransferData.strTransferTargetUUID);         
        enQueue_Message(ANI,objMessage);
        vANIWorktable[intTableIndex].boolRingBack = false;  //need to set to trigger ringing in :  case RINGING:   ??? Race condition possible ???  
        vANIWorktable[intTableIndex].CallData.TransferData.boolIsRingBack = true;      
        vANIWorktable[intTableIndex].CallData.TransferData.eTransferMethod = mRING_BACK;
        vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].boolIsRingBack = true;      
        vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod = mRING_BACK; 

        sem_post(&sem_tMutexANIWorkTable);return false; 


   case HOLD_ON: case HOLD_OFF:
         // In this case the Pertinent/current Freeswitch Data Passed in is :
        // FreeswitchData.objChannelData.strChannelName
        // FreeswitchData.objChannelData.strChannelID
        // we can get here from two methods PRESENCE_IN and reqular hold and unhold.  (they do not always fire when in conferences with sla)
        // hence the check if the channel is already on hold or off hold.
////cout << "HOLD ON .. HOLD OFF" << endl;
////cout << " " << objData->FreeswitchData.objChannelData.strChannelID << endl;
////cout << " " << objData->FreeswitchData.objChannelData.strChannelName << endl;
//        objData->FreeswitchData.objChannelData.fDisplay();             
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-i", 1);}
  
        intTableIndex = LocatePositionChannelIdinTableConferenceVector(objData->FreeswitchData.objChannelData.strChannelID); 
        if (intTableIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}

        intPosnVectorIndex = vANIWorktable[intTableIndex].FreeswitchData.fFindChannelInPosnVector(objData->FreeswitchData.objChannelData.strChannelID);
        if (intPosnVectorIndex < 0) {sem_post(&sem_tMutexANIWorkTable);return false;}


        j = vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].iPositionNumber;
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->fLoad_Position(SOFTWARE, ANI, int2strLZ(j), intPortNum, "IN HOLD ON/OFF");
////cout << "position is -> " << j << endl;
        switch(objData->enumANIFunction)
         {
          case HOLD_ON:
               objPosition.Set_Last_OnHold(vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].strChannelName,
                                           vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[intPosnVectorIndex].strChannelID,j);
               //check if already on hold
               if (vANIWorktable[intTableIndex].CallData.fIsOnHold(j)) {sem_post(&sem_tMutexANIWorkTable);return false;}
               break;
          case HOLD_OFF:
                //check if already on unhold 
              // //cout << "Hold off ! in case HOLD_ON: case HOLD_OFF:" << endl;

               if (!vANIWorktable[intTableIndex].CallData.fIsOnHold(j)) {sem_post(&sem_tMutexANIWorkTable);return false;}
               break;
          default:
               //should not get here
               SendCodingError("ani.cpp - coding error Match_Freeswitch_Object_To_Table() switch (case HOLD_ON: case HOLD_OFF:)");
               sem_post(&sem_tMutexANIWorkTable);return false;
               break;
         }
      //  //cout << "return true" << endl;
        sem_post(&sem_tMutexANIWorkTable);return true;
  
   case VALET_JOIN:
        // A channel in a Parking Lot has been merged into a conference.  We will need to merge the calls in the table.
        // In this case the Pertinent/current Freeswitch Data Passed in is :
        // FreeswitchData.objChannelData
        // CallData.strFreeswitchConfName
        // CallData.strFreeswitchConfID
 //       //cout << "VAlet Join in ANI" << endl;
 //       //cout << "conf name is -> " << objData->CallData.strFreeswitchConfName << endl;
 //       //cout << "conf id   is -> " << objData->CallData.strFreeswitchConfID << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-VALET_JOIN", 1);}
        intTableIndex = -1;
        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID);
        if (intTableIndex < 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}

        // we now need to find where in the table this call is to be merged. 
        intSecondTableIndex = IndexWithFreeSwitchConferenceName(objData->CallData.strFreeswitchConfName);
        if (intSecondTableIndex < 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}       

        //Transfer will be attached to the Parked Call
        intTranVectorIndex       =  vANIWorktable[intTableIndex].CallData.fFindTransferData(objData->FreeswitchData.objChannelData.strChannelName,
                                                                                            objData->FreeswitchData.objChannelData.strChannelID );
        if (intTranVectorIndex < 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}

        switch (objData->FreeswitchData.objChannelData.iPositionNumber)
         {
          case 0:
           eANIfunction = TRANSFER_CONNECT_BLIND; // No rcc switching
           break;
          default:
         //Position was in the parking lot
          eANIfunction = TRANSFER_CONNECT; // ensure rcc switching
         }

         //add the channels from index 2 
         vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber--;
         sz = vANIWorktable[intSecondTableIndex].FreeswitchData.vectDestChannelsOnCall.size();
         for (unsigned int i = 0; i < sz; i++){
           vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(XFER_CONF, vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);
           vANIWorktable[intSecondTableIndex].FreeswitchData.vectDestChannelsOnCall[i].fLoadConferenceDisplayX(vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber);
           vANIWorktable[intTableIndex].FreeswitchData.iConfDisplayNumber++; 
           intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fLegendInConferenceChannelDataVector(vANIWorktable[intSecondTableIndex].FreeswitchData.vectDestChannelsOnCall[i].strConfDisplay);
           if (intConfVectorIndex >=0){
            vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex] = vANIWorktable[intSecondTableIndex].FreeswitchData.vectDestChannelsOnCall[i];
           }
           else{
            vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intSecondTableIndex].FreeswitchData.vectDestChannelsOnCall[i]);        
           }        
           vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(vANIWorktable[intSecondTableIndex].FreeswitchData.vectDestChannelsOnCall[i]);
         }
         sz = vANIWorktable[intSecondTableIndex].FreeswitchData.vectPostionsOnCall.size();
         for (unsigned int i = 0; i < sz; i++) {
          intConfVectorIndex = vANIWorktable[intTableIndex].CallData.fLegendInConferenceChannelDataVector(vANIWorktable[intSecondTableIndex].FreeswitchData.vectPostionsOnCall[i].strConfDisplay);
          if (intConfVectorIndex >=0){
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex] = vANIWorktable[intSecondTableIndex].FreeswitchData.vectPostionsOnCall[i];
          }
          else{
           vANIWorktable[intTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[intSecondTableIndex].FreeswitchData.vectPostionsOnCall[i]);
           
          }
          vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, vANIWorktable[intSecondTableIndex].FreeswitchData.vectPostionsOnCall[i].iPositionNumber);
          vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(vANIWorktable[intSecondTableIndex].FreeswitchData.vectPostionsOnCall[i]);
         }

       strDialedNumber = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strValetExtension;
       vANIWorktable[intTableIndex].CallData.strFreeswitchConfName     = objData->CallData.strFreeswitchConfName;
       vANIWorktable[intTableIndex].CallData.TransferData.fClear();
       vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
       objData->CallData.TransferData.fClear();
       objData->CallData.fRemoveTransferData(intTranVectorIndex);
       objData->enumANIFunction = eANIfunction;

       objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
       objData->CallData       = vANIWorktable[intTableIndex].CallData;

       objData->CallData.fLoadPosn(objData->FreeswitchData.objChannelData.iPositionNumber);
       ////cout << "370.f third" << endl;
       objData->fSetANIfunction(VALET_JOIN);
       objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA, 
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_370f, strDialedNumber);
       enQueue_Message(ANI,objMessage);
       objData->stringTimeOfEvent = objMessage.stringTimeStamp;
       enqueue_Main_Input(ANI, objData);

       objData->CallData = vANIWorktable[intSecondTableIndex].CallData;
       objData->CallData.fLoadPosn(0);
       objData->fSetANIfunction(MERGE_CALL, vANIWorktable[intTableIndex].CallData.stringUniqueCallID);         
       objMessage.fMessage_Create(LOG_CONSOLE_FILE, 396, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData->CallData, objBLANK_TEXT_DATA, objPortData, 
                                  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_396, vANIWorktable[intTableIndex].CallData.stringUniqueCallID);
       enQueue_Message(ANI,objMessage);
 
       objData->enumANIFunction = DISCONNECT; 
  
        sem_post(&sem_tMutexANIWorkTable);
        return true; 
   case CONFERENCE_JOIN:
        // In this case the Pertinent/current Freeswitch Data Passed in is :
        // FreeswitchData.objChannelData
        // CallData.strFreeswitchConfName
        // CallData.strFreeswitchConfID

        ////cout << "conf join in ANI" << endl;
        intRC = sem_wait(&sem_tMutexANIWorkTable);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-CONFERENCE_JOIN", 1);}
        intTableIndex = -1;
 
        //return  if channel UUID is already on an active call ........
        intTableIndex = IndexofActiveChannelinTable(objData->FreeswitchData.objChannelData.strChannelID);
        if (intTableIndex >= 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}
 ////cout << "A" << endl;
        //find if transfering to a position
        if (bool_BCF_ENCODED_IP){
         intPosition = TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));
        }
        else{
         ////cout << objData->FreeswitchData.objChannelData.IPaddress.stringAddress << endl;
         intPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objData->FreeswitchData.objChannelData.IPaddress.stringAddress);
        } 
////cout << "A1" << endl;
        //check to see if this was part of a GUI blind or Conference transfer (utilizes conference ) 
        intTableIndex = IndexofTableTransferVectorMatch(objData->FreeswitchData.objChannelData.strChannelID);
                if (intTableIndex >= 0)
                 {
                 // //cout << "GUI Xfer" << endl;
                 // if (TelephoneEquipment.Devices[index].fUpdateLinesInUse() ) {//cout << "IN USE" << endl;}
                //  //cout << TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName)) << endl;
                //  objData->FreeswitchData.objChannelData.fDisplay();
                  intTranVectorIndex = vANIWorktable[intTableIndex].CallData.fFindTransferData("NULL", objData->FreeswitchData.objChannelData.strChannelID);
                  if (intTranVectorIndex < 0) 
                   { SendCodingError( "ani.cpp - Coding Error in Match_Freeswitch_Object_To_Table() case CONFERENCE_JOIN: intTranVectorIndex < 0"); sem_post(&sem_tMutexANIWorkTable); return false;}

                  // Check if transfer is to another position ..... then show that position connected ..... Check for Blind Xfer Update
                  iTransferConnect = 378; 
                  vANIWorktable[intTableIndex].CallData.fLoadPosn(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition);
                  eANIfunction = TRANSFER_CONNECT;
                  if (vANIWorktable[intTableIndex].CallData.boolBlindXferUpdate) {iTransferConnect = 395; vANIWorktable[intTableIndex].CallData.boolBlindXferUpdate= false;}

                  switch (vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].eTransferMethod)
                   {
                    case mGUI_TRANSFER:
              //           //cout << "In this one for conf join do we wait for channel answer ? " << endl;
                         if (intPosition == 0){
                          sem_post(&sem_tMutexANIWorkTable);return false; 
                         }
                         ////cout << "378.e" << endl;
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber,
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID);
                         break;
                    case mNG911_TRANSFER:
                        // //cout << "378d.4" << endl;
                         //  Internal ng911 transfers will be handled by FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER
                         if (INIT_VARS.fChannelIsInternalNG911Transfer(objData->FreeswitchData.objChannelData.strChannelName)) {sem_post(&sem_tMutexANIWorkTable);return false;}
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378d,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strNG911PSAP_TransferNumber,
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID);
                         break;
                    case mBLIND_TRANSFER:
               //          //cout << "pos -> " << intPosition << endl;
               //          //cout << "in call -> " << vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() << endl;
                         // for an esablished conference use this connect .... for single channel ops: FREESWITCH_CHANNEL_ANSWER_UPDATE_GUI_BLIND_TRANSFER
                         if((vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() < 2)||(intPosition == 0)){
                          sem_post(&sem_tMutexANIWorkTable);return false;
                         }
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378b,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber,
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransferTargetUUID);
                         eANIfunction = TRANSFER_CONNECT_BLIND;

                    //     //cout << "blind xfer connect" << endl;
                    //     objData->FreeswitchData.objChannelData.fDisplay();
                    //     //cout << objData->CallData.strFreeswitchConfName << endl;
                    //     //cout << objData->CallData.strFreeswitchConfID << endl;
                         break;
                    default:
                        // //cout << "default" << endl;
                       //  //cout << "378.w" << endl;
                         vANIWorktable[intTableIndex].fSetANIfunction(TRANSFER_CONNECT);
                         objMessage.fMessage_Create(LOG_CONSOLE_FILE, iTransferConnect, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData,
                                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_378,  
                                                    vANIWorktable[intTableIndex].CallData.TransferData.strTransfertoNumber);
                   }



                  enQueue_Message(ANI,objMessage); 
                 
                  objData->FreeswitchData.objChannelData.strConfDisplay = vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strConfDisplay;
                  objData->FreeswitchData.objChannelData.boolConnected = true;
                  objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
                 // //cout << objData->FreeswitchData.objChannelData.strChannelName << endl; 
 
                  // check if transfer to is actually a position modify with correct Channel Data
                  j = TelephoneEquipment.fPositionNumberFromChannelName(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));                 
                  if (j > 0) { 
                             // //cout << "J is -> " << j << endl;
                              objData->FreeswitchData.objChannelData.iPositionNumber = j;
                              objData->FreeswitchData.objChannelData.boolIsPosition = true;
                              objData->FreeswitchData.objChannelData.strConfDisplay = ConferenceMemberString(POSITION_CONF,j);
                              objData->FreeswitchData.objChannelData.IPaddress.stringAddress = TelephoneEquipment.fIPaddressFromPositionNumber(j);
                              objData->FreeswitchData.objChannelData.iLineNumber = 
                                       TelephoneEquipment.fFindTelephoneLineNumber(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));
                              objData->FreeswitchData.objChannelData.strCustName = 
                                       TelephoneEquipment.fCallerIDnameofTelephonewithRegistrationName(FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName));
                              vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData, intTranVectorIndex);
                             }
    //              else if (INIT_VARS.fChannelIsInternalNG911Transfer(objData->FreeswitchData.objChannelData.strChannelName)) 
    //                         {
    //                         // This will be handled by FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER
     //                         //cout << "HERE" << endl;
     //                         sem_post(&sem_tMutexANIWorkTable);return false;
     //                        }
                  else       {vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);}

                  vANIWorktable[intTableIndex].CallData.fLoadPosn(vANIWorktable[intTableIndex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition);
                  vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);

                  objData->stringTimeOfEvent = objMessage.stringTimeStamp;
 //               if (boolTDD_AUTO_DETECT)
 //                {
 //                 objData->enumANIFunction = TDD_MODE_ON;
 //                 vANIWorktable[intTableIndex].AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objChannelData.strChannelName = objData->AsteriskData.objLinkData.strChanneltwo;
 //                 Queue_AMI_Input(objData);
 //                 sem_post(&sem_tAMIFlag);
//                 }

                  objData->enumANIFunction = eANIfunction;
                  objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
                  objData->CallData = vANIWorktable[intTableIndex].CallData; 

               
               //   if (boolPositiontoPositionCall) { objData->FreeswitchData.objLinkData = tempObjLinkData ;} this may affect RCC's not needed for SLA
                  enqueue_Main_Input(ANI, objData);

                  vANIWorktable[intTableIndex].CallData.TransferData.fClear();
                  vANIWorktable[intTableIndex].CallData.fRemoveTransferData(intTranVectorIndex);
                 // //cout << "b. checking conference size" << endl;
                  Check_Conference_Size_On_Hangup(j, vANIWorktable[intTableIndex], intPortNum);  
                  sem_post(&sem_tMutexANIWorkTable);
                  return false;
                 }// end if (intTableIndex >= 0)
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
/*                                                        CONF TRANSFERS ABOVE                                                                                  */
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
// Determine size and composition of conference here vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

////cout << "before check" << endl;
                intTableIndex = IndexofTableConferenceVectorMatch(objData->FreeswitchData.objChannelData.strChannelID);
                if (intTableIndex >= 0) 
                 {
                  sem_post(&sem_tMutexANIWorkTable);
                  return false;
                 }

//  //cout << "checking conference" << endl;    
        //match previously established Freeswitch Conference
        intTableIndex = IndexWithFreeSwitchConferenceName(objData->CallData.strFreeswitchConfName);
        if (intTableIndex < 0)         
         {
////cout << "Condition One" << endl;
          // The conference is just being built use shared username field to match to table ...
      //    //cout << objData->FreeswitchData.objChannelData.strChannelName << endl;
          strTemp = FreeswitchUsernameFromChannel(objData->FreeswitchData.objChannelData.strChannelName);
          strTemp += "@";
          intTableIndex = LocatePositionChannelUsernameInTable(strTemp);
          if (intTableIndex < 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}
         }
        else
         {
          switch (vANIWorktable[intTableIndex].FreeswitchData.fNumberofChannelsInCall() )
           {
            case 0:
                   // impossible ???
                   SendCodingError("ani.cpp - Coding Error in Match_Freeswitch_Object_To_Table() case CONFERENCE_JOIN fNumberofChannelsInCall = 0");
                   sem_post(&sem_tMutexANIWorkTable);return false;

            case 1:
                   // this is from a ringback ... send an un conference to AMI
                   objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
                   vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);
                   vANIWorktable[intTableIndex].fUpdateLineNumbersInTable(objData->FreeswitchData.objChannelData);
                 //  //cout << "remove dup pos number ----> " << objData->FreeswitchData.objChannelData.iPositionNumber << endl; 
                   vANIWorktable[intTableIndex].CallData.ConfData.fRemoveDuplicatePositionChannels(objData->FreeswitchData.objChannelData.iPositionNumber);
                  // //cout << "A" << endl;
                   Check_Conference_Size_On_Hangup(intTableIndex, vANIWorktable[intTableIndex], intPortNum);




                   vANIWorktable[intTableIndex].fSetANIfunction(RING_BACK);  // do we do transfer connect blind ??????                          
                   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 370, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[intTableIndex].CallData, 
                                              objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_370e);
                   enQueue_Message(ANI,objMessage);
                   vANIWorktable[intTableIndex].enumANIFunction        = TRANSFER_CONNECT_BLIND; 
                   vANIWorktable[intTableIndex].stringTimeOfEvent      = objMessage.stringTimeStamp;
                   enqueue_Main_Input(ANI, vANIWorktable[intTableIndex]);
                   sem_post(&sem_tMutexANIWorkTable);return false;

            case 2:
                   if (PreBuild_Conference(intTableIndex) ){}
                   break;

            default:
                   // check for Internal NG911 Transfer ...... Already taken care of in FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER
                   if (INIT_VARS.fChannelIsInternalNG911Transfer(objData->FreeswitchData.objChannelData.strChannelName)) {
                    // //cout << "return false ...." << endl;
                    sem_post(&sem_tMutexANIWorkTable);return false;
                   }
                  // //cout << "got through ...." << endl;
                   // Conference Barge gets here .....
                   break;
           }
          }
 // //cout << "got here f" << endl; 

     
        if(!objData->CallData.strFreeswitchConfName.empty())
         {
          vANIWorktable[intTableIndex].CallData.strFreeswitchConfName = objData->CallData.strFreeswitchConfName;
          vANIWorktable[intTableIndex].CallData.strFreeswitchConfID   = objData->CallData.strFreeswitchConfID;
         }
        // add Line number to RingDial object
      //  if (objData->FreeswitchData.objChannelData.iRingingPosition)
      //   {
     //     vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iRingingLineNumber[objData->FreeswitchData.objChannelData.iRingingPosition] = objData->FreeswitchData.objChannelData.iLineNumber;
      //   }
////cout << "push in posn vector" << endl;
// objData->FreeswitchData.objChannelData.fDisplay();
        // push conference into Position vector
        objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
        objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.iTrunkNumber);
        vANIWorktable[intTableIndex].fUpdate_FREESWITCH_ConferenceData(objData->FreeswitchData.objChannelData);
        vANIWorktable[intTableIndex].CallData.fLoadPosn(objData->FreeswitchData.objChannelData.iPositionNumber);
        objData->CallData = vANIWorktable[intTableIndex].CallData;
        objData->FreeswitchData = vANIWorktable[intTableIndex].FreeswitchData;
      
// //cout << "got here g" << endl;      
        sem_post(&sem_tMutexANIWorkTable);return true;

   case BARGE_ON_POSITION:
           ////cout << "Barge on Position in ANI" << endl;
           if(!boolUSE_POLYCOM_CTI) { SendTryAgainToCaller(objData); return false;}         

           intRC = sem_wait(&sem_tMutexANIWorkTable);
           if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-k", 1);}
           intTableIndex = -1;

           sz =  vANIWorktable.size();
           
           j = char2int(objData->CallData.TransferData.strTransfertoNumber.c_str());     
       //    //cout << "barge to " << j << endl;
           for (unsigned int i = 0; i < sz; i++)
            {
             if (vANIWorktable[i].CallData.fIsInConference(POSITION_CONF, j)&&(vANIWorktable[i].boolConnect)&&(!vANIWorktable[i].CallData.fIsOnHold(objData->CallData.intPosn)))
              { intTableIndex = i; break;}

            }  
           if (intTableIndex < 0) { SendTryAgainToCaller(objData);  sem_post(&sem_tMutexANIWorkTable);return false;}
           

           //Determine line number phone is on.
           iLineNumber = vANIWorktable[intTableIndex].FreeswitchData.fLineNumberofPositiononCall(j);
       //    //cout << "line number -> " << iLineNumber << endl;
           if (iLineNumber <= 0 ) { SendCodingError("ani.cpp - Coding Error in Match_Freeswitch_Object_To_Table() case BARGE_ON_POSITION: iLineNumber LTE zero");
                                    sem_post(&sem_tMutexANIWorkTable);return false;;
                                  }
       
           //if using shared lines use polycom cti otherwise use freeswitch cli
           if(TelephoneEquipment.fLineNumberAtPositionIsShared(iLineNumber, j))
            {
          //   //cout << "line shared -> Hangup Barge Channel " << iLineNumber << endl;
             // Hangup channel from Barge 
              objData->CallData.CTIsharedLine = iLineNumber;           
              Queue_AMI_Input(objData);
              sem_post(&sem_tAMIFlag);

             vANIWorktable[intTableIndex].enumWrkFunction = WRK_BARGE;
             vANIWorktable[intTableIndex].intWorkStation = objData->CallData.intPosn;
             vANIWorktable[intTableIndex].CallData.CTIsharedLine = iLineNumber;
             Queue_CTI_Event(vANIWorktable[intTableIndex]);
            }
           else
            {
             vANIWorktable[intTableIndex].FreeswitchData.fLoad_EavesDrop_into_Channelobject();
             objData->FreeswitchData.objChannelData = vANIWorktable[intTableIndex].FreeswitchData.objChannelData;
             objData->enumANIFunction = BARGE_ON_CONFERENCE;
             Queue_AMI_Input(objData);
             sem_post(&sem_tAMIFlag);

            }

           sem_post(&sem_tMutexANIWorkTable);return false;
           return false;

   case BARGE_ON_CONFERENCE:
           // dialed barge from the dial plan ........
           //cout << "BARGE ON CONFERENCE in ANI" << endl;
           intRC = sem_wait(&sem_tMutexANIWorkTable);
           if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-k", 1);}


           intTableIndex = IndexWithConfNumber(objData->CallData.intConferenceNumber);
           if (intTableIndex < 0) { SendTryAgainToCaller(objData);  sem_post(&sem_tMutexANIWorkTable);return false;}
       
/* This is done in main .........
           // check if shared line is in use and there is at least one position number on the call : Do CTI Phone Barge ....

           // find a shared line if avaiable ...
           boolSharedLineAvailable = false;
           iLineNumber = -1;
           for (unsigned int i = 0; i < vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall.size(); i++)
            {
             intPosition = vANIWorktable[intTableIndex].FreeswitchData.vectPostionsOnCall[i].iPositionNumber;
             iLineNumber = vANIWorktable[intTableIndex].FreeswitchData.fLineNumberofPositiononCall(intPosition); //cout << "line number is -> " << iLineNumber << endl;
             if(TelephoneEquipment.fLineNumberAtPositionIsShared(iLineNumber, intPosition)) { 
              boolSharedLineAvailable = true; 
              vANIWorktable[intTableIndex].CallData.CTIsharedLine = iLineNumber;
              break;
             }
            }
           
           if (boolSharedLineAvailable){
             //Barge from the CTI
             // hangup channel ..
             ////cout << "Barge From The CTI ->" << endl;
             objData->enumANIFunction                              = KILL_CHANNEL;
             objData->FreeswitchData.objChannelData.strChannelName = objData->CallData.TransferData.strTransferFromChannel;                           
             objData->FreeswitchData.objChannelData.strChannelID   = objData->CallData.TransferData.strTransferFromUniqueid; 
             Queue_AMI_Input(objData);
             sem_post(&sem_tAMIFlag); 

             sleep (intPOLYCOM_CTI_BARGE_DELAY_MS/1000.0);
             objData->intWorkStation = objData->CallData.intPosn;           
             objData->enumWrkFunction = WRK_BARGE;
             objData->CallData.CTIsharedLine = vANIWorktable[intTableIndex].CallData.CTIsharedLine; 
             Queue_CTI_Event(objData);
             sem_post(&sem_tMutexANIWorkTable);return false;
             return false;
           }
*/      
            //build a conference if only 2 on call
            PreBuild_Conference(intTableIndex);
           // //cout << "sending to AMI " << endl;
          // objData->FreeswitchData.objChannelData.fDisplay();
           //add the new guy (no positions or shared lines available ...
           objData->CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].CallData.strFreeswitchConfName;
           objData->FreeswitchData.objChannelData = vANIWorktable[intTableIndex].FreeswitchData.objChannelData;
           objData->enumANIFunction = BARGE_ON_CONFERENCE_LINE_ROLL;
           Queue_AMI_Input(objData);
           sem_post(&sem_tAMIFlag);

   
           sem_post(&sem_tMutexANIWorkTable);return false;
           return false;



        return false;

     case BARGE_ON_POSITION_LINE_ROLL:
          // //cout << "BARGE ON POS LINE ROLL" << endl;
           intRC = sem_wait(&sem_tMutexANIWorkTable);
           if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-k", 1);}
           intTableIndex = -1;
           intPosition = objData->CallData.intPosn; // this is the position to Barge from
           sz =  vANIWorktable.size();
           
           j = char2int(objData->CallData.TransferData.strTransfertoNumber.c_str());     

           for (unsigned int i = 0; i < sz; i++)
            {
             if (vANIWorktable[i].CallData.fIsInConference(POSITION_CONF, j)&&(vANIWorktable[i].boolConnect)&&(!vANIWorktable[i].CallData.fIsOnHold(objData->CallData.intPosn)))
              { intTableIndex = i; break;}

            }  
           if (intTableIndex < 0) { SendTryAgainToCaller(objData);  sem_post(&sem_tMutexANIWorkTable);return false;}
           
           
 
           //build a conference if only 2 on call
           PreBuild_Conference(intTableIndex);
                      
           objData->FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(objData->CallData.TransferData, objData->CallData.intPosn);
    //    //cout << "J" << endl;  
           vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData);

           vANIWorktable[intTableIndex].UPdate_CallData_Conference_Data_Vector();
           vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, objData->CallData.intPosn);           



           objData->CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].CallData.strFreeswitchConfName;
           objData->enumANIFunction = CONFERENCE_JOIN_LIST;
       //    //cout << objData->CallData.strFreeswitchConfName << endl;
           objData->enumANIFunction = BARGE_ON_CONFERENCE_LINE_ROLL;
           Queue_AMI_Input(objData);
           sem_post(&sem_tAMIFlag);

          // //cout << "BOP CONF JOIN" << endl;
           objData->enumANIFunction = CONFERENCE_JOIN;
           objData->CallData        = vANIWorktable[intTableIndex].CallData;
           objData->CallData.fLoadPosn(intPosition);   //put barge from position back in object
           objData->FreeswitchData  = vANIWorktable[intTableIndex].FreeswitchData;
           sem_post(&sem_tMutexANIWorkTable);return true;
           return false;



   case BARGE_ON_CONFERENCE_LINE_ROLL:
          // //cout << "BARGE ON CONF LINE ROLL " << endl;
           intRC = sem_wait(&sem_tMutexANIWorkTable);
           if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-k", 1);}


           intTableIndex = IndexWithConfNumber(objData->CallData.intConferenceNumber);
           if (intTableIndex < 0) { SendTryAgainToCaller(objData);  sem_post(&sem_tMutexANIWorkTable);return false;}          

           intPosition = objData->CallData.intPosn; // this is the position Barge from
            //build a conference if only 2 on call
            PreBuild_Conference(intTableIndex);
            
           // transfer object will have data for new participant

           objData->FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(objData->CallData.TransferData, objData->CallData.intPosn);

           objData->FreeswitchData.objChannelData.fLoadGUILineView(objData->FreeswitchData.objChannelData.strChannelName);
           objData->FreeswitchData.objChannelData.fLoadLineNumberFromChannel();
           objData->FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[intTableIndex].CallData.intTrunk);
     //   //cout << "K" << endl;  
           vANIWorktable[intTableIndex].FreeswitchData.AddtoPositionVector(objData->FreeswitchData.objChannelData);

           vANIWorktable[intTableIndex].UPdate_CallData_Conference_Data_Vector();
           vANIWorktable[intTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, objData->CallData.intPosn);           
 // objData->FreeswitchData.objChannelData.fDisplay();
           objData->CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].CallData.strFreeswitchConfName;
           // send Join to CLI
           objData->enumANIFunction = BARGE_ON_CONFERENCE_LINE_ROLL;
           Queue_AMI_Input(objData);
           sem_post(&sem_tAMIFlag);
          // //cout << "BOC LINE ROLL " << endl;
           objData->enumANIFunction = CONFERENCE_JOIN;
 
           objData->CallData        = vANIWorktable[intTableIndex].CallData;
           objData->CallData.fLoadPosn(intPosition);   //put barge from position back in object
           objData->FreeswitchData  = vANIWorktable[intTableIndex].FreeswitchData;
           sem_post(&sem_tMutexANIWorkTable);return true;
           return false;



        return false;

   case CONFERENCE_LEAVE:
       // //cout << "conference leave" << endl;
        return false;

   case TDD_MODE_ON:
           intRC = sem_wait(&sem_tMutexANIWorkTable);
           if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-k", 1);}
           intTableIndex = IndexWithCallUniqueID( objData->CallData.stringUniqueCallID);
           if (intTableIndex < 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}
           j = objData->CallData.intPosn;
           vANIWorktable[intTableIndex].fSetANIfunction(TDD_MODE_ON);
           objData->CallData = vANIWorktable[intTableIndex].CallData;
           objData->CallData.fLoadPosn(j);
           sem_post(&sem_tMutexANIWorkTable);return true;        

   case TDD_MUTE:                     
           intRC = sem_wait(&sem_tMutexANIWorkTable);
           if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Match_Freeswitch_Object_To_Table()-L", 1);}
           intTableIndex = IndexWithCallUniqueID( objData->CallData.stringUniqueCallID);
   //       intTableIndex = IndexWithConfNumber( objData->CallData.intConferenceNumber);
           if (intTableIndex < 0)  {sem_post(&sem_tMutexANIWorkTable);return false;}
           j = objData->CallData.intPosn;
           vANIWorktable[intTableIndex].fSetANIfunction(TDD_MUTE);
           objData->CallData = vANIWorktable[intTableIndex].CallData;
           objData->CallData.fLoadPosn(j);
           sem_post(&sem_tMutexANIWorkTable);return true;
     
   case TDD_CHAR_RECEIVED:
           
           return true;

   default: break;
          

  }

// objMessage.fMessage_Create(LOG_WARNING, 360, objData->fCallData(), objPortData, ANI_MESSAGE_360, objData->strAsteriskUniqueCallId, 
//                            ASCII_String(strRawData.c_str(), strRawData.length()),"","","","",false, true );
// enQueue_Message(ANI,objMessage);
 
 return false;
}

bool PreBuild_Conference(int intTableIndex)
{
  extern vector <ExperientDataClass>             vANIWorktable; 
 // table must be semaphore protected before calling
 // check for 2 channels only in call table, if so build conference.

 //build a conference if only 2 in table
 if(vANIWorktable[intTableIndex].FreeswitchData.fLoad_Channels_in_LinkData())
  {
   if(vANIWorktable[intTableIndex].CallData.strFreeswitchConfName.empty()) {
    vANIWorktable[intTableIndex].enumANIFunction = CONFERENCE_BUILD_LIST;                                                                            
    vANIWorktable[intTableIndex].CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].FreeswitchData.objRing_Dial_Data.strChannelID; //fred UUID_Generate_String();
   }
   else  {
    vANIWorktable[intTableIndex].enumANIFunction = CONFERENCE_JOIN_LIST;
   }
 //  //cout << "prebuilding conference ->" << vANIWorktable[intTableIndex].CallData.strFreeswitchConfName << endl;
   Queue_AMI_Input(vANIWorktable[intTableIndex]);
   sem_post(&sem_tAMIFlag);
   return true;
  }
return false;
}

bool Add_Conference_Member(int intTableIndex, ExperientDataClass objData) 
{
  extern vector <ExperientDataClass>             vANIWorktable; 
 // table must be semaphore protected before calling
 ////cout << "conference add member" << endl;
 if (vANIWorktable[intTableIndex].CallData.strFreeswitchConfName.empty()) {SendCodingError("ani.cpp Add_Conference_Member() No conference name"); return false;}

 objData.CallData.strFreeswitchConfName = vANIWorktable[intTableIndex].CallData.strFreeswitchConfName;
 objData.enumANIFunction = CONFERENCE_ADD_MEMBER;
 Queue_AMI_Input(objData);
 sem_post(&sem_tAMIFlag);
 return true;
}

void Check_TDD_Caller_Sentences()
{
 int                                            intRC;
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         tablesize;
// //cout << "IN TDD SENTENCE" << endl;
 intRC = sem_wait(&sem_tMutexANIWorkTable);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in Check_TDD_Caller_Sentences", 1);}
// //cout << "LOCK TDD SENTENCE" << endl;
 tablesize = vANIWorktable.size();
 for (unsigned int i = 0; i < tablesize; i++)
  {
   vANIWorktable[i].enumThreadSending = ANI;
   if(vANIWorktable[i].fCheckTDDbuffer())
    {
     vANIWorktable[i].enumANIFunction = TDD_CALLER_SAYS;
     SendANIToMain(TDD_CALLER_SAYS, vANIWorktable[i], vANIWorktable[i].intANIPortCallReceived);
    }
  }
  sem_post(&sem_tMutexANIWorkTable);
//  //cout << "UNLOCK TDD SENTENCE" << endl;
}

void Check_Conference_Size_On_Hangup(int i, ExperientDataClass objData, int intPortNum, string strHangup)
{
 extern vector <ExperientDataClass>      		vANIWorktable;
 extern bool 						bool_BCF_ENCODED_IP;
 size_t							sz;


// //cout << "in conference size check" << endl;
 // note MUTEX on vANIWorktable must be in place before calling this function !
 switch (vANIWorktable[i].FreeswitchData.fNumberofChannelsInCall()) 
  {
   case 2:
    // //cout << "Size is 2" << endl;
     if (vANIWorktable[i].CallData.strFreeswitchConfName.empty()) {return;}
 //    if (!bool_BCF_ENCODED_IP) {return;}  //needs to be line roll vs shared line .....
     // this undo's the conference with only 2 players ......
     objData = vANIWorktable[i];
     objData.FreeswitchData.fLoad_Channels_in_LinkData();
     objData.enumANIFunction = UN_CONFERENCE;
     objData.FreeswitchData.enumFreeswitchEvent = HANGUP;
     vANIWorktable[i].CallData.strFreeswitchConfName.clear();
     vANIWorktable[i].CallData.strFreeswitchConfID.clear();

//Update the line views to that of the remaining position .... 
    sz = vANIWorktable[i].FreeswitchData.vectPostionsOnCall.size();
    switch (sz) {
     case 0: break;
     case 1:
      //Channel has not been flagged disconnected 
      vANIWorktable[i].fUpdateDestChannelLineNumbersInTable(vANIWorktable[i].FreeswitchData.vectPostionsOnCall[0]);
      break;
     case 2:
      // Leave them be ??? 2 position channels could be on diff
      break;
     default:
      // impossible ...
      break;
    }


     
  //   if(objData.CallData.strFreeswitchConfName.empty()) {//cout << "conference name is empty in Check_Conference_Size_On_Hangup()" << endl;;}
 //    if (!strHangup.empty()) {objData.CallData.
    // //cout << "SEND UNCONFERENCE FROM ANI " << endl;
     Queue_AMI_Input(objData);
     sem_post(&sem_tAMIFlag); 
     break; 
   case 1:
    //  //cout << "size is 1" << endl;
     break;
   default:
 //    //cout << "size is more than 2 or 0" << endl;
     break;
  }

 return;
}



unsigned int RotatePortNumber(int intPortNum)
{
 intPortNum++;
 if (intPortNum > intNUM_ANI_PORTS) {intPortNum = 1;}
 return intPortNum;
}

unsigned int FindRegisteredANIport(int intPortNum)
{
 extern ExperientCommPort  ANIPort[NUM_ANI_PORTS_MAX+1];
 int                       intRC;
 int                       intCheckPort;

 intRC = sem_wait(&sem_tMutexANIPort);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIPort, "sem_wait@sem_tMutexANIPort in fn FindRegisteredANIport()", 1);}
 intCheckPort =  RotatePortNumber(intPortNum);
 for (int i =1; i <= intNUM_ANI_PORTS; i++)
  {
   if (ANIPort[intCheckPort].boolAudiocodesRegistered) {sem_post(&sem_tMutexANIPort); return intCheckPort;}
   intCheckPort = RotatePortNumber(intCheckPort);
  }

 sem_post(&sem_tMutexANIPort);
 return 0;
}


// May not need .......

unsigned int FindActiveAMIport(int intPortNum)
{
 extern ExperientCommPort  ANIPort[NUM_ANI_PORTS_MAX+1];
 int                       intRC;
 int                       intCheckPort;

 intRC = sem_wait(&sem_tMutexANIPort);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIPort, "sem_wait@sem_tMutexANIPort in fn FindRegisteredANIport()", 1);}
 intCheckPort =  RotatePortNumber(intPortNum);
 for (int i =1; i <= intNUM_ANI_PORTS; i++)
  {
   if (ANIPort[intCheckPort].boolPortActive) {sem_post(&sem_tMutexANIPort); return intCheckPort;}
   intCheckPort = RotatePortNumber(intCheckPort);
  }

 sem_post(&sem_tMutexANIPort);
 return 0;
}

int IndexWithConfNumber(unsigned long long int iConfNumber)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();

 if (iConfNumber <= 0) {return -1;}

  for (unsigned int i = 0; i< sz; i++)
   {
    if (vANIWorktable[i].CallData.intConferenceNumber == iConfNumber) {return i;}
   }
  return -1;
}

int IndexWithCallUniqueID(string strUniqueID)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();

 if (strUniqueID.empty()) {return -1;}

  for (unsigned int i = 0; i< sz; i++)
   {
    if (vANIWorktable[i].CallData.stringUniqueCallID == strUniqueID) {return i;}
   }
  return -1;
}

int IndexOfNewANITableEntry(ExperientDataClass objData)
{
  // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;

 vANIWorktable.push_back(objData);

 return vANIWorktable.size() - 1;
}


bool PositionIsActiveOnCallandNotonHold(int iPosition)
{
 // note table should be semaphore locked before calling this function

 size_t sz = vANIWorktable.size();


 for (unsigned int i = 0; i < sz; i++) 
  { 
   if ((vANIWorktable[i].CallData.fIsInConference(POSITION_CONF, iPosition))&&
       (!vANIWorktable[i].CallData.fIsOnHold(iPosition)))                {return true;}
  }

 return false;
}

int  IndexofActiveSMSConversation(string strFrom)
{
// note table should be semaphore locked before calling this function

 size_t sz = vANIWorktable.size();

 for (unsigned int i = 0; i < sz; i++) 
  { 
   if (vANIWorktable[i].TDDdata.SMSdata.strFromUser == strFrom)                {return i;}   //Legacy code to be removed ....
   if (vANIWorktable[i].TextData.SMSdata.strFromUser == strFrom)               {return i;}   
  }

 return -1;
}

/****************************************************************************************************
*
* Name:
*  Function:bool LocateRingChannelIdinTable(string strUniqueID)
*
*
* Description:
*  Returns a boolean of whether there is a match to the channel-ID already existing in the 
*  vector vANIWorktable[index].FreeswitchData.objRing_Dial_Data
*
* Details:
*	FreeswitchData.objRing_Dial_Data contains the channel Data associated with a ringing
*  or Dialed out Call.  This function is called after the Channel Bridge event to match
*  the Bridge with a previous ring or Dial out. 
*	The semaphore for the ANI table must be set prior to calling this function.
*
*
* Parameters:
*   strUniqueID                         <cstring> channelID to look for
*  
* Variables:
*   vANIWorktable                       Extern  - <vector><ExperientDataClass>      The ANI table
*   sz                                  Local   - <vector><Channel_Data>::size_type size of the vectPostionsOnCall container
*                                                           
* Functions:
*   .size()                             Library - <vector>              (size_t)                 
*
* AUTHOR: 03/151/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation 
****************************************************************************************************/ 
int LocateRingChannelIdinTable(string strUniqueID)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();

 if(strUniqueID.empty()){return -1;}

 for (unsigned int i = 0; i< sz; i++)
   {
    if (vANIWorktable[i].FreeswitchData.objRing_Dial_Data.strChannelID == strUniqueID)
     {return i;}
   }
  return -1;
}

int IndexWithFreeSwitchConferenceID(string strUniqueID)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();

 if(strUniqueID.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   if (vANIWorktable[i].CallData.strFreeswitchConfID == strUniqueID) {return i;}  
  }
 return -1;
}

int IndexWithFreeSwitchConferenceName(string strName)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();

 if(strName.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   if (vANIWorktable[i].CallData.strFreeswitchConfName == strName) {return i;}  
  }
 return -1;
}

int IndexofTableTransferVectorMatch(string strUniqueID)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();
 int                                            index;

 if(strUniqueID.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   index = vANIWorktable[i].CallData.fFindTransferData("NULL", strUniqueID);
   if (index >=0)                                       {return i;}
  }
  
 return -1;
}

int IndexWithTransferTarget(string strUniqueID)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();
 int                                            index;

 if(strUniqueID.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   index = vANIWorktable[i].CallData.fFindTransferDataWithTransferTarget(strUniqueID);
   if (index >=0)                                       {return i;}
  }
  
 return -1;
}




int IndexofTableConferenceVectorMatch(string strUniqueID)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();
 int                                            index;

 if(strUniqueID.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   index = vANIWorktable[i].CallData.ConfData.fFindConferenceData(strUniqueID);
   if (index >=0)                                       {return i;}
  }
  
 return -1;
}


int IndexofTablePositionVectorMatch(string strUniqueID)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();
 vector <Channel_Data>::size_type               szChannel;

 if(strUniqueID.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   szChannel = vANIWorktable[i].FreeswitchData.vectPostionsOnCall.size();
   for (unsigned int j = 0; j< szChannel; j++)
    {
     if (vANIWorktable[i].FreeswitchData.vectPostionsOnCall[j].strChannelID == strUniqueID)
      {return i;}
    }
  }
 return -1;
}

int IndexofTableDestinationVectorMatch( string strUniqueID)
{
 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();
 vector <Channel_Data>::size_type               szChannel;

 if(strUniqueID.empty()){return -1;}
 for (unsigned int i = 0; i< sz; i++)
  {
   szChannel = vANIWorktable[i].FreeswitchData.vectDestChannelsOnCall.size();
   for (unsigned int j = 0; j< szChannel; j++)
    {
     if (vANIWorktable[i].FreeswitchData.vectDestChannelsOnCall[j].strChannelID == strUniqueID)
      {return i;}
    }
  }
 return -1;
}

int IndexofActiveSharedLineInTable(int iLineNumber)
{
  // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         sz = vANIWorktable.size();

 if(!iLineNumber) {return -1;}

 for (unsigned int i = 0; i< sz; i++)
  {
   if (vANIWorktable[i].CallData.CTIsharedLine == iLineNumber) {return i;}
  }

 return -1;
}


int IndexofActiveChannelinTable( string strUniqueID)
{
  // table must be semaphore protected before calling this function !
 int                                            index;
 if(strUniqueID.empty()){return -1;}
 index = -1;

 index = IndexofTablePositionVectorMatch( strUniqueID);
 if (index >=0) {return index;}
 index = IndexofTableDestinationVectorMatch(strUniqueID); 
 if (index >=0) {return index;}

 return index;
}

int IndexofActiveChannelinTable( string strUniqueID, int iButNotThisIndex)
{
  // table must be semaphore protected before calling this function !
 int                                            index;
 size_t                                         sz;
 extern vector <ExperientDataClass>             vANIWorktable;

 if(strUniqueID.empty())     {return -1;}
 index = -1;
 sz    = vANIWorktable.size();

 for (unsigned int i=0; i < sz; i++) {
  if ((int) i == iButNotThisIndex)                                           {continue;}
  if (vANIWorktable[i].FreeswitchData.fFindChannelInPosnVector(strUniqueID)) {return i;}
  if (vANIWorktable[i].FreeswitchData.fFindChannelInDestVector(strUniqueID)) {return i;}
 }

 return index;
}




bool BothChannelsAlreadyPresentandConnected(Freeswitch_Data objFreeswitchData)
{
  // table must be semaphore protected before calling this function !
 int indexOne, indexTwo;
 extern vector <ExperientDataClass>             vANIWorktable;

 indexOne = IndexofActiveChannelinTable(objFreeswitchData.objLinkData.strChanneloneID);
 if (indexOne < 0) {return false;}
 indexTwo = IndexofActiveChannelinTable(objFreeswitchData.objLinkData.strChanneltwoID); 
 if (indexTwo < 0) {return false;}
 if (indexOne != indexTwo) {return false;}

 return (vANIWorktable[indexOne].boolConnect);
}


int LocatePositionChannelIdinTableConferenceVector(string strUniqueID)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         tablesize = vANIWorktable.size();
 vector <Channel_Data>::size_type               ConfSize;

 if(strUniqueID.empty()){return -1;}


 for (unsigned int i = 0; i< tablesize; i++)
   {
    ConfSize = vANIWorktable[i].FreeswitchData.vectPostionsOnCall.size();
    for (unsigned int j = 0; j< ConfSize; j++)
     {
      if (vANIWorktable[i].FreeswitchData.vectPostionsOnCall[j].strChannelID == strUniqueID)
       {return i;}
     }
   }
  return -1;
}


int LocatePositionChannelUsernameInTable(string strUsername)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vANIWorktable;
 vector <ExperientDataClass>::size_type         tablesize = vANIWorktable.size();
 vector <Channel_Data>::size_type               ConfSize;
 size_t                                         found;
 if(strUsername.empty()){return -1;}

 for (unsigned int i = 0; i< tablesize; i++)
   {
    ConfSize = vANIWorktable[i].FreeswitchData.vectPostionsOnCall.size();
    for (unsigned int j = 0; j< ConfSize; j++)
     {
      found = vANIWorktable[i].FreeswitchData.vectPostionsOnCall[j].strChannelName.find(strUsername);
      if (found != string::npos)                                                         {return i;}
     }
   }

  return -1;
}






void Check_Stale_Vector_Data()
{
 extern vector <ExperientDataClass>     vANIWorktable; 
 extern Conference_Number               objConferenceList;
 int                                    intRC;
 int                                    iTableIndex;
 MessageClass                           objMessage;
 long long int                          uliTimeInUse;

 // lock main work table
 intRC = sem_wait(&sem_tMutexANIWorkTable);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in fn Check_Stale_Vector_Data()", 1);}

 // lock conference vector
 intRC = sem_wait(&sem_tMutexConferenceNumberAssignmentObject);					
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexConferenceNumberAssignmentObject, "sem_wait@sem_tMutexConferenceNumberAssignmentObject in fn Check_Stale_Vector_Data()", 1);}

 for (int i = MIN_CONFERENCE_NUMBER; i <= MAX_CONFERENCE_NUMBER; i++)
  {
   uliTimeInUse = (long long int) (objConferenceList.TimeInUse(i));
   if (uliTimeInUse > CONFERENCE_NUMBER_IN_USE_MAX_TIME_PERIOD_SEC) 
    {
     iTableIndex = IndexWithConfNumber(i);

     // No Table Data associated with the conference number ....
     if (iTableIndex < 0) {objConferenceList.ReleaseConferenceNumber(i); continue;}
     
     objMessage.fMessage_Create(LOG_WARNING,374, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vANIWorktable[iTableIndex].CallData, objBLANK_TEXT_DATA, objBLANK_ANI_PORT, 
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_374, int2strLZ(i), seconds2time(uliTimeInUse) ); 
     enQueue_Message(ANI,objMessage);
     objConferenceList.ReleaseConferenceNumber(i);

     // Push a Detelte thru the main message Q     
     vANIWorktable[iTableIndex].enumThreadSending = ANI;
     vANIWorktable[iTableIndex].enumANIFunction   = DELETE_RECORD;
     SendANIToMain(DELETE_RECORD, vANIWorktable[iTableIndex], 0);
     vANIWorktable.erase(vANIWorktable.begin()+iTableIndex); 

    }// end if (uliTimeInUse > CONFERENCE_NUMBER_IN_USE_MAX_TIME_PERIOD_SEC)

  }// end for (int i = 100; i <= MAX_CONFERENCE_NUMBER; i++)

 sem_post(&sem_tMutexConferenceNumberAssignmentObject);	
 sem_post(&sem_tMutexANIWorkTable);
 return;
}




bool SendTransferToAMI(ExperientDataClass objData, int index, transfer_method eTrnsferType, Port_Data objPortData)
{
 // function must be semaphore protected on vANIWorktable before calling.
 // the following variables must be set for the function to work correctly:
 //
 // objData.CallData.TransferData
 // objData.intWorkStation
 //
 extern vector <ExperientDataClass>     vANIWorktable;  
 MessageClass           		objMessage;
 bool                                   boolValet;
 int                                    iNumChannels;
 bool                                   boolPark;
 string                                 strParkUUID;
 int                                    iTableIndex, iVectorIndex, iPosVectorIndex;


 vANIWorktable[index].CallData.TransferData = objData.CallData.TransferData;
 vANIWorktable[index].CallData.TransferData.fLoadFromPosition(objData.intWorkStation);

 ////cout << "send Transfer to AMI " << endl;

 vANIWorktable[index].intWorkStation = objData.intWorkStation;
 vANIWorktable[index].CallData.fLoadPosn(objData.intWorkStation);

 objData.FreeswitchData = vANIWorktable[index].FreeswitchData;
 objData.CallData     = vANIWorktable[index].CallData;
 iNumChannels         = vANIWorktable[index].FreeswitchData.fNumberofChannelsInCall();
 boolPark             = vANIWorktable[index].CallData.TransferData.fIsPark(boolValet);


 switch (eTrnsferType)
  {
   case mGUI_TRANSFER:
                     // //cout << "GUI transfer" << endl;
                      if (boolPark&&boolValet) {
                       objMessage.fMessage_Create(LOG_CONSOLE_FILE,314, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, 
                                                  objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_314 );                      
                       enQueue_Message(ANI,objMessage);
                       vANIWorktable[index].enumANIFunction = TRANSFER_FAIL;
                       enqueue_Main_Input(ANI, vANIWorktable[index]);
                       return false;
                      }
                      if (boolPark){
                         strParkUUID = FindUUIDofCallinLot(iTableIndex, iVectorIndex, vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                         if (strParkUUID.empty()){
                          objMessage.fMessage_Create(LOG_CONSOLE_FILE,315, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, 
                                                     objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_315 );                      
                          enQueue_Message(ANI,objMessage);
                          vANIWorktable[index].enumANIFunction = TRANSFER_FAIL;
                          enqueue_Main_Input(ANI, vANIWorktable[index]);
                          return false;
                         }
                      }
                      
                   //   //cout << "CONFERENCE Transfer In mGUI_TRANSFER" << endl;
                      vANIWorktable[index].CallData.TransferData.strTransferTargetUUID = UUID_Generate_String();

                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayX(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(XFER_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);        
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[index].CallData.intTrunk);

                      // get line numbers from position channel that conferencing the call
                      iPosVectorIndex = vANIWorktable[index].FreeswitchData.fFindPositionInPosnVector(char2int(vANIWorktable[index].CallData.TransferData.strTransferFromPosition.c_str()));
                      if (iPosVectorIndex >= 0) {
                       vANIWorktable[index].FreeswitchData.objChannelData.iGUIlineView = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosVectorIndex].iGUIlineView;
                       vANIWorktable[index].FreeswitchData.objChannelData.iLineNumber  = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosVectorIndex].iLineNumber;
                       vANIWorktable[index].FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosVectorIndex].iTrunkNumber);
                      }
                      //add transferTarget UUID to tran data ..... 
                      vANIWorktable[index].FreeswitchData.objChannelData.strTranData = vANIWorktable[index].CallData.TransferData.strTransferTargetUUID;
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objData.CallData = vANIWorktable[index].CallData;
                      objData.fSetANIfunction(CONFERENCE); // CONFERENCE ???? FRED
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData,
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;

   case mNG911_TRANSFER: case mREFER:
                      ////cout << "365.a" << endl;
                      vANIWorktable[index].CallData.TransferData.strTransferTargetUUID = UUID_Generate_String();
                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayX(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(XFER_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);        
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[index].CallData.intTrunk);
                      // get line numbers from position channel that conferencing the call
                      iPosVectorIndex = vANIWorktable[index].FreeswitchData.fFindPositionInPosnVector(char2int(vANIWorktable[index].CallData.TransferData.strTransferFromPosition.c_str()));
                      if (iPosVectorIndex >= 0) {
                       vANIWorktable[index].FreeswitchData.objChannelData.iGUIlineView = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosVectorIndex].iGUIlineView;
                       vANIWorktable[index].FreeswitchData.objChannelData.iLineNumber  = vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosVectorIndex].iLineNumber;
                       vANIWorktable[index].FreeswitchData.objChannelData.fLoadTrunkNumber(vANIWorktable[index].FreeswitchData.vectPostionsOnCall[iPosVectorIndex].iTrunkNumber);
                      }
                      
                      vANIWorktable[index].FreeswitchData.objChannelData.strTranData = vANIWorktable[index].CallData.TransferData.strTransferTargetUUID;
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objData.CallData = vANIWorktable[index].CallData;

                      switch (eTrnsferType) {
                       case mNG911_TRANSFER: 
                            objData.fSetANIfunction(TRANSFER, mNG911_TRANSFER);                      
                            objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, 
                                                       objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365d, 
                                                       objData.CallData.TransferData.strNG911PSAP_TransferNumber,
                                                       vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);
                            break;
                       case mREFER:
                            objData.fSetANIfunction(TRANSFER, mREFER);    
                            objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                       objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365h, objData.CallData.TransferData.strTransfertoNumber,
                                                       vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);

                            break;
                       default: break;
                      }

                      break;

/*
   case mREFER:
                      //cout << "REFER IN ANI" << endl;
                      //cout << "TRANS NUMBER a ->" << objData.CallData.TransferData.strTransfertoNumber << endl;
                      //cout << "TRANS NUMBER b ->" << objData.CallData.TransferData.strNG911PSAP_TransferNumber << endl;
                      //cout << "TRANS TYPE       " << objData.CallData.TransferData.strTransferType << endl;
                     
   //vTransferData 
                      vANIWorktable[index].CallData.TransferData.strTransferTargetUUID = UUID_Generate_String();  
                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayT(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      //this could change depending on how the notify subsciption works .......
                      vANIWorktable[index].CallData.fConferenceStringAdd(TANDEM_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;      
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);




                      objData.CallData = vANIWorktable[index].CallData;
                      objData.fSetANIfunction(TRANSFER, mREFER);    
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365h, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;
*/
   case mBLIND_TRANSFER:
                    // //cout << "BLIND TRANSFER using this one???" << endl;  // are we using this ???? where is targetUUID ?
                      objData.fSetANIfunction(TRANSFER, mBLIND_TRANSFER); 

                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayX(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(XFER_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);        
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,ANI_MESSAGE_365b, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;
   case mTANDEM:
                      vANIWorktable[index].CallData.TransferData.strTransferTargetUUID = UUID_Generate_String(); 
                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayT(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(TANDEM_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;      
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objData.CallData = vANIWorktable[index].CallData;
                      objData.fSetANIfunction(TRANSFER, mTANDEM);
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365c, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;
   case mINTRADO: 

                      vANIWorktable[index].CallData.TransferData.strTransferTargetUUID = UUID_Generate_String();  
                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayT(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(TANDEM_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;      
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objData.CallData = vANIWorktable[index].CallData;
                      objData.fSetANIfunction(TRANSFER, mINTRADO);    
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365g, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;

   case mINDGITAL:
                      vANIWorktable[index].CallData.TransferData.strTransferTargetUUID = UUID_Generate_String();  
                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayT(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(TANDEM_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;      
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objData.CallData = vANIWorktable[index].CallData;
                      objData.fSetANIfunction(TRANSFER, mINDGITAL);    
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData,
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365f, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;
   case mFLASHOOK:
                      objData.fSetANIfunction(TRANSFER, mFLASHOOK);    
  
                      vANIWorktable[index].CallData.TransferData.fLoadConferenceDisplayT(vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].CallData.fConferenceStringAdd(FLASH_CONF,vANIWorktable[index].FreeswitchData.iConfDisplayNumber);
                      vANIWorktable[index].FreeswitchData.iConfDisplayNumber++;      
                      vANIWorktable[index].CallData.TransferData.strTransferName = CallerNameLookup(vANIWorktable[index].CallData.TransferData.strTransfertoNumber);
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].FreeswitchData.objChannelData.fLoadChannelDatafromTransferData(vANIWorktable[index].CallData.TransferData);
                      vANIWorktable[index].CallData.ConfData.vectConferenceChannelData.push_back(vANIWorktable[index].FreeswitchData.objChannelData);
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365e, objData.CallData.TransferData.strTransfertoNumber,
                                                 vANIWorktable[index].CallData.TransferData.strTransferTargetUUID);                      
                      break;
   case mATTENDED_TRANSFER:

                      // Right now only we can do attended transfer from the Polycoms so we will do this through CTI.  if there is no CTI we will change the transfer to
                      // a Conference Transfer.
                      if(boolUSE_POLYCOM_CTI)
                       {
                        objData.enumWrkFunction = WRK_TRANSFER;
                        Queue_CTI_Event(objData);
                        return true;
                       }                    
                      vANIWorktable[index].CallData.TransferData.eTransferMethod = objData.CallData.TransferData.eTransferMethod = mGUI_TRANSFER;
                      objData.fSetANIfunction(TRANSFER, mATTENDED_TRANSFER);    
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,365, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.CallData, objBLANK_TEXT_DATA, objPortData, 
                                                 objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_365, objData.CallData.TransferData.strTransfertoNumber );
                      vANIWorktable[index].CallData.vTransferData.push_back(vANIWorktable[index].CallData.TransferData);
                      break;
   default:
                      SendCodingError("Coding error in fn SendTransferToAMI() wrong eTrnsferType "); return false;
  }           
 enQueue_Message(ANI,objMessage);
 vANIWorktable[index].enumANIFunction        = DIAL_DEST;
 vANIWorktable[index].stringTimeOfEvent      = objMessage.stringTimeStamp;
 vANIWorktable[index].CallData.TransferData.eTransferMethod = eTrnsferType;

 
// need to change to a comphrehensive list add ... no if statements .................................
 //check to see if Conference has been built previously if not build it

 switch (eTrnsferType)  {

   case mGUI_TRANSFER: case mNG911_TRANSFER:  case mREFER:

                   //     //cout << "GUI/ng911" << endl;

                   //     //cout << "channels in call -> " << vANIWorktable[index].FreeswitchData.fNumberofChannelsInCall() << endl;
                   //     //cout << "transfer to      -> " << vANIWorktable[index].CallData.TransferData.strTransfertoNumber << endl;
                   //     //cout << " parked UUID     -> " << strParkUUID << endl;
                        if (boolPark){
                         PreBuild_Conference(index);
                         vANIWorktable[index].enumANIFunction = VALET_JOIN;
                         vANIWorktable[index].FreeswitchData.objChannelData.strChannelID = strParkUUID;                        
                         Queue_AMI_Input(vANIWorktable[index]);
                         sem_post(&sem_tAMIFlag);
                         // may need to send through main ....
                        }
                        else if (eTrnsferType==mREFER) {
                         PreBuild_Conference(index);
                         vANIWorktable[index].enumANIFunction = TRANSFER;
                         Queue_AMI_Input(vANIWorktable[index]);
                         sem_post(&sem_tAMIFlag);

                        } else {
                         PreBuild_Conference(index);
                         vANIWorktable[index].enumANIFunction = EAVESDROP;
                         // send transfer to AMI thru Main .... (needs ALI Data)
                         //      //cout << "send transfer to main ->" << endl;
                         vANIWorktable[index].FreeswitchData.fLoad_EavesDrop_into_Channelobject();
                         enqueue_Main_Input(ANI, vANIWorktable[index]);
                        } 
                        break;
   case mBLIND_TRANSFER:
                        enqueue_Main_Input(ANI, vANIWorktable[index]); 
                        
                         // //cout << "BLIND GUI XFER" << endl;
                //          vANIWorktable[index].CallData.strFreeswitchConfName = vANIWorktable[index].FreeswitchData.objRing_Dial_Data.strChannelID;
                          PreBuild_Conference(index);
                        //  if(!vANIWorktable[index].FreeswitchData.fBuild_FreeSWITCH_Conference()) {SendCodingError("ani.cpp SendTransferToAMI() 2. cannot build conference!");}
                        //  if (vANIWorktable[index].FreeswitchData.fBuild_FreeSWITCH_Conference()) 
                        //     {vANIWorktable[index].CallData.strFreeswitchConfName = vANIWorktable[index].FreeswitchData.objLinkData.strChanneloneID;}
                      //    vANIWorktable[index].CallData.strFreeswitchConfName = vANIWorktable[index].FreeswitchData.objRing_Dial_Data.strChannelID;
                      //    vANIWorktable[index].enumANIFunction = CONFERENCE_JOIN_LIST;
                      //    Queue_AMI_Input(vANIWorktable[index]);
                      //    sem_post(&sem_tAMIFlag);   
                         
                       //  //cout << vANIWorktable[index].CallData.strFreeswitchConfName << endl;
                         vANIWorktable[index].enumANIFunction = TRANSFER;
                         // send transfer to AMI

                         Queue_AMI_Input(vANIWorktable[index]);
                         sem_post(&sem_tAMIFlag);

                        //if (boolUSE_POLYCOM_CTI)
                        if (false)
                         {
                          objData.enumWrkFunction = WRK_HANGUP;
                          Queue_CTI_Event(objData);
                         }
                        else
                         {

                          ////cout << "send conference leave" << endl;
                          vANIWorktable[index].enumANIFunction = CONFERENCE_LEAVE ;
                          Queue_AMI_Input(vANIWorktable[index]);
                          sem_post(&sem_tAMIFlag); 
                         }
                        break;
   case mTANDEM: case mFLASHOOK:  case mINDGITAL:
                        enqueue_Main_Input(ANI, vANIWorktable[index]); 
                        vANIWorktable[index].enumANIFunction = TRANSFER;
                        Queue_AMI_Input(vANIWorktable[index]);
                        sem_post(&sem_tAMIFlag);
                        break;
   case mINTRADO:
                        enqueue_Main_Input(ANI, vANIWorktable[index]); 
                        vANIWorktable[index].enumANIFunction = RFAI_BRIDGE;
                        Queue_AMI_Input(vANIWorktable[index]);
                        sem_post(&sem_tAMIFlag);
                        break;

  default:
                        enqueue_Main_Input(ANI, vANIWorktable[index]); 
                        break;
  }

 return true;
}

string FindUUIDofCallinLot(int &iTableindex, int &intTranVectorIndex, string strLot)
{
   // vANIWorktable must be locked prior to call of this function.
  extern vector <ExperientDataClass>    vANIWorktable; 
  size_t 				szTable;

  intTranVectorIndex = -1;
  iTableindex        = -1;

  if (strLot.empty()) {return "";}
  szTable = vANIWorktable.size();
  for (unsigned int i = 0; i < szTable; i++){
   intTranVectorIndex = vANIWorktable[i].CallData.fFindParkData(strLot);
   if (intTranVectorIndex >= 0) {iTableindex = i; break;}
  }
 if ((intTranVectorIndex >=0)&&(iTableindex >=0)){
  return vANIWorktable[iTableindex].CallData.vTransferData[intTranVectorIndex].strTransferFromUniqueid;
 }

 return "";
}
bool CheckForUnAttendedTransfer(Channel_Data objChannelData)
{
  // vANIWorktable must be locked prior to call of this function.

  size_t 				szTable;
  extern vector <ExperientDataClass>    vANIWorktable; 
  extern Telephone_Devices		TelephoneEquipment; 
  extern bool                           bool_BCF_ENCODED_IP;   
  int    				intTranVectorIndex = -1;
  int                                   iTableindex = -1;
  bool                                  BoolA, BoolB, BoolC, BoolD, BoolE, BoolF, BoolG, BoolH, BoolI;
  bool                                  boolExternalUnattendedTransfer, boolInternalUnattendedTransfer;
  bool                                  boolInternalPositionTransfer;

  //objChannelData.fDisplay();
  szTable = vANIWorktable.size();
 // //cout << "endpoint disposition -> " << objChannelData.strEndpointDisposition << endl;
 // //cout << "Hangup   disposition -> " << objChannelData.strHangupDisposition << endl;
 // //cout << "Transfer disposition -> " << objChannelData.strTransferDisposition << endl;

  BoolA = (objChannelData.strEndpointDisposition == FREESWITCH_CLI_VALUE_ATTENDED_TRANSFER);
  BoolB = (objChannelData.strHangupDisposition   == FREESWITCH_CLI_VALUE_RECV_CANCEL); 
  BoolC = (objChannelData.strTransferDisposition == FREESWITCH_CLI_VALUE_REPLACED);

  BoolD = (objChannelData.strEndpointDisposition == FREESWITCH_CLI_VALUE_ANSWER);
  BoolE = (objChannelData.strHangupDisposition   == FREESWITCH_CLI_VALUE_SEND_BYE); 
  BoolF = (objChannelData.strTransferDisposition == FREESWITCH_CLI_VALUE_RECV_REPLACE); 

  BoolG = BoolD;
  BoolH = (objChannelData.strHangupDisposition   == FREESWITCH_CLI_VALUE_RECV_BYE); 
  BoolI = BoolF; 

  if (bool_BCF_ENCODED_IP) { 
   objChannelData.iPositionNumber = TelephoneEquipment.fPositionNumberFromChannelName(objChannelData.strChannelName); 
  }
  else { 
   objChannelData.iPositionNumber = TelephoneEquipment.fPostionNumberFromIPaddress(objChannelData.IPaddress.stringAddress);
  }

  boolExternalUnattendedTransfer = BoolA && BoolB && BoolC;
  boolInternalUnattendedTransfer = BoolD && BoolE && BoolF;
  boolInternalPositionTransfer   = BoolG && BoolH && BoolI;

 // //cout << "ext unattended ->" << boolExternalUnattendedTransfer << endl;
//  //cout << "int unattended ->" << boolInternalUnattendedTransfer << endl;
//  //cout << "int pos xfer ->" << boolInternalPositionTransfer << endl;
 // //cout << BoolD << " " << BoolE << " " << BoolF << endl;

  if ((!boolExternalUnattendedTransfer)&&(!boolInternalUnattendedTransfer)&&(!boolInternalPositionTransfer)) {return false;}

  if ((boolExternalUnattendedTransfer)&&(objChannelData.iPositionNumber <= 0))
   {
    for (unsigned int i = 0; i < szTable; i++)
     { 
      intTranVectorIndex = vANIWorktable[i].CallData.fFindTransferData(objChannelData.strTranData);
      if (intTranVectorIndex >= 0) {iTableindex = i; break;}
     }
    if ((intTranVectorIndex < 0)||(iTableindex < 0)) {return true;}
   }
  else 
   {
//    //cout << "position ->" << objChannelData.iPositionNumber << endl;
    if (objChannelData.iPositionNumber <= 0)          {return true;}

    // this added for race condition answer recv before hangup ...... 
    for (unsigned int i = 0; i < szTable; i++)
     { 
      intTranVectorIndex = vANIWorktable[i].CallData.fFindTransferData(objChannelData.iPositionNumber);
      if (intTranVectorIndex >= 0) {iTableindex = i; break;}
     }
    if ((intTranVectorIndex < 0)||(iTableindex < 0)) {return true;}   

   }
// //cout << "setting Transfer to unattended" << endl;
 vANIWorktable[iTableindex].CallData.vTransferData[intTranVectorIndex].eTransferMethod = mPOORMAN_BLIND_TRANSFER;
 vANIWorktable[iTableindex].CallData.TransferData.eTransferMethod                      = mPOORMAN_BLIND_TRANSFER;
 return true;
}

bool TransferTargetIsDifferentPostion(Transfer_Data objTransferData)
{
 // vANIWorktable must be locked prior to call of this function.
  string 				strUserName;
  size_t 				szTable;
  extern vector <ExperientDataClass>    vANIWorktable;
  extern Telephone_Devices		TelephoneEquipment;   
 
  int					iPosition;
  string 				strPosition;
  szTable = vANIWorktable.size();

  iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objTransferData.IPaddress.stringAddress);
  if (iPosition <= 0) {return false;} 
  strPosition = int2str(iPosition);

  if (strPosition != objTransferData.strTransferFromPosition) {return true;}


 return false;
}



void CheckForCancelAttendedTransferPositiontoPosition(Channel_Data objChannelData, int iTableindex, int intTranVectorIndex)
{
  // vANIWorktable must be locked prior to call of this function.
  string 				strUserName;
  size_t 				szTable;
  extern vector <ExperientDataClass>    vANIWorktable;
  extern Telephone_Devices		TelephoneEquipment;   
 
  int					iPosition;
  string 				strPosition;
  szTable = vANIWorktable.size();


  if (objChannelData.strHangupCause != FREESWITCH_CLI_VALUE_ORIGINATOR_CANCEL) {return;}
  if ((intTranVectorIndex < 0)||(iTableindex < 0))                             {return;}

  strUserName = FreeswitchUsernameFromChannel(objChannelData.strChannelName);

  if (strUserName.empty()) {return;}

  iPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objChannelData.IPaddress.stringAddress);
  if (iPosition <= 0) {return;} 
  strPosition = int2str(iPosition);

  if (strPosition != vANIWorktable[iTableindex].CallData.vTransferData[intTranVectorIndex].strTransferFromPosition) {return;}

  vANIWorktable[iTableindex].CallData.vTransferData[intTranVectorIndex].boolGUIcancel = true;

 return;
}

bool InitializeANIports(bool boolSKIPACTIVE) {
 extern int                             intNUM_ANI_PORTS;
 extern ExperientCommPort               ANIPort[NUM_ANI_PORTS_MAX+1];
 string                                 strKey = IPWORKS_RUNTIME_KEY;
 extern Initialize_Global_Variables     INIT_VARS;



 for (int i = 1; i <= intNUM_ANI_PORTS; i++) { 
  //cout << "initialize ANI Port " << i << endl;
 
  ANIPort[i].intPortNum                         = i;   
  ANIPort[i].RemoteIPAddress.stringAddress    	= INIT_VARS.ANIinitVariable.ANI_PORT_VARS[i].REMOTE_IP_ADDRESS.stringAddress;
  ANIPort[i].intRemotePortNumber 	   	= FREESWITCH_CLI_PORT_NUMBER;
  ANIPort[i].intPortDownReminderThreshold 	= INIT_VARS.ANIinitVariable.ANI_PORT_VARS[i].intPORT_DOWN_REMINDER_SEC;
  ANIPort[i].intPortDownThresholdSec 		= INIT_VARS.ANIinitVariable.ANI_PORT_VARS[i].intPORT_DOWN_THRESHOLD_SEC;
  ANIPort[i].intHeartbeatInterval               = INIT_VARS.ANIinitVariable.ANI_PORT_VARS[i].intHEARTBEAT_INTERVAL_SEC;

  ANIPort[i].UDP_Port.fInitializeANIport(i);
 }

 INIT_VARS.ANIinitVariable.boolRestartANIports = false;
 return true;
}
/*
void InitializeDSBport() {

 string                                 		strKey = IPWORKS_RUNTIME_KEY;
 extern ExperientTCPPort              			DashboardPort;
 extern Initialize_Global_Variables             	INIT_VARS;

  DashboardPort.SetRuntimeLicense((char*)strKey.c_str());
  DashboardPort.Remote_IP.stringAddress 	= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.REMOTE_IP_ADDRESS.stringAddress;
  DashboardPort.intRemotePortNumber 		= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.intREMOTE_PORT_NUMBER;
  DashboardPort.intPortDownReminderThreshold 	= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC;
  DashboardPort.intPortDownThresholdSec 	= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.intPORT_DOWN_THRESHOLD_SEC;

  INIT_VARS.DSBinitVariable.boolRestartDashboardPort = false;
  if (!boolUSE_DASHBOARD) {return;}
  ////cout << "init dashboard port" << endl;
  DashboardPort.fInitializeDSBport();

 return;
}
*/





bool ShutDownANIports(pthread_t ANIPortListenThread[], int numberofports)
{
 extern ExperientCommPort  				ANIPort[NUM_ANI_PORTS_MAX+1];

 for (int i = 1; i<= numberofports; i++) {
  ANIPort[i].TCP_Server_Port.Shutdown();
  ANIPort[i].UDP_Port.SetActive(false);
  ANIPort[i].TCP_Port.Disconnect();
 }
 for (int i = 1; i<= numberofports; i++) {ANIPort[i].boolRestartListenThread = true;}

 for (int i = 1; i<= numberofports; i++) { pthread_join( ANIPortListenThread[i], NULL); }  

 return true;
}

bool StartANIListenThreads(pthread_t ANIPortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t params[])
{
 // internal function to ALI thread
 extern int						intNUM_ANI_PORTS;
 int							intRC;

  if (intNUM_ANI_PORTS > NUM_ANI_PORTS_MAX) {SendCodingError("ani.cpp ->StartANIListenThreads() -> Number of ANI ports > MAX!"); return false;}
  for (int i = 1; i<= intNUM_ANI_PORTS; i++) 
   {
    params[i].intPassedIn = i;
    intRC = pthread_create(&ANIPortListenThread[i], pthread_attr_tAttr, *ANI_Port_Listen_Thread, (void*) &params[i]);
    if (intRC) {SendCodingError("ani.cpp ->StartANIListenThreads() -> Unable to start ANI Listen thread!"); return false;}
   }
return true;
}


void  SendTryAgainToCaller(ExperientDataClass objData)
{


objData.enumANIFunction = PLAY_SORRY_MSG;

Queue_AMI_Input(objData);
sem_post(&sem_tAMIFlag);
                       

}

void RemoveAllPostionsFromMSRPCall(int index, int intPortNum)
{
 //used if caller hangs up first (should not happen) 
extern int                                     	intNUM_WRK_STATIONS;
extern vector <ExperientDataClass>             	vANIWorktable;
int    						intConfVectorIndex;

// Table should be locked prior to call to function
////cout << "RemoveAllPostionsFromMSRPCall" << endl;
  for (int i = 1; i<= intNUM_WRK_STATIONS; i++) { 
   if (vANIWorktable[index].CallData.fIsInConference(POSITION_CONF, i)) {
    intConfVectorIndex = vANIWorktable[index].CallData.fLegendInConferenceChannelDataVector(ConferenceMemberString(POSITION_CONF, i));
    if (intConfVectorIndex >= 0) {
     vANIWorktable[index].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fSetChannelHangup();
    }  
    vANIWorktable[index].CallData.fConferenceStringRemove(POSITION_CONF, i);
    vANIWorktable[index].enumANIFunction = SILENT_ENTRY_LOG_OFF; 
    vANIWorktable[index].CallData.fLoadPosn(i);
    SendANIToMain(SILENT_ENTRY_LOG_OFF, vANIWorktable[index], intPortNum);
   }
  }

 return;
}

void RemoveAllOnHoldPostionsFromMSRPCall(int index, int intPortNum)
{
int    						intConfVectorIndex;
extern int                                     	intNUM_WRK_STATIONS;
extern vector <ExperientDataClass>             	vANIWorktable;

// Table should be locked prior to call to function
////cout << "RemoveAllOnHoldPostionsFromMSRPCall" << endl;
  for (int i = 1; i<= intNUM_WRK_STATIONS; i++) { 
   if (vANIWorktable[index].CallData.fIsOnHold(i)) {
    intConfVectorIndex = vANIWorktable[index].CallData.fLegendInConferenceChannelDataVector(ConferenceMemberString(POSITION_CONF, i));
    if (intConfVectorIndex >= 0) {
     vANIWorktable[index].CallData.ConfData.vectConferenceChannelData[intConfVectorIndex].fSetChannelHangup();
    }  
    vANIWorktable[index].CallData.fOnHoldPositionRemove(i);    
    vANIWorktable[index].CallData.fConferenceStringRemove(POSITION_CONF, i);
    vANIWorktable[index].enumANIFunction = SILENT_ENTRY_LOG_OFF;
  //  vANIWorktable[index].enumANIFunction = SILENT_ENTRY_LOG_OFF; //fred need to find pos in conf vector and show hung up  
    vANIWorktable[index].CallData.fLoadPosn(i);
    SendANIToMain(SILENT_ENTRY_LOG_OFF, vANIWorktable[index], intPortNum);
   }
  }

 return;
}









