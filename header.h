#ifndef HEADER_H_
#define HEADER_H_


#define IPWORKS_V16

#include <unistd.h>	                                // Posix Unix Environment
#include <sys/syscall.h>
#include <stdlib.h>
#include <sys/reboot.h>                             // linux reboot
#include <sys/stat.h>                               // mkdir
#include <sys/types.h>                              // mkdir
#include <pthread.h>                                // enable threads
#include <net/if.h>                                 // sockets local interfaces
#include <sys/ioctl.h>                              // 
#include <iostream>                                 // basic I/O
#include <iomanip>
#include <fstream>
#include <semaphore.h>                              // POSIX semaphores
#include <cctype>                                   // character functions
#include <csignal>                                  // posix signals
#include <cerrno>                                   // error numbers
#include <queue>                                    // C++ queue's
#include <cstring>                                  // C++ string class
#include <sstream>                                  // C++ string streams
#include <ctime>                                    // C++ time functions
#include <time.h>
#include <vector>                                   // vector class
#include <deque>
#include <list>
#include <cmath>                                    // C++ Math Library
#include <termios.h>                                // definitions used by the terminal I/O interfaces
//#include <boost/archive/text_iarchive.hpp>        // serialization library
//#include <boost/archive/text_oarchive.hpp>        // serialization library
//#include <boost/serialization/vector.hpp>
#include <sysexits.h>                                   // C++ extra Exit Codes
#include <locale>                                       // Used for isdigit
#include <climits>
#include <cfloat>
#include <list>
#include <map>
#include <regex>
#include <sys/statvfs.h>
#include <dirent.h> 
#include <arpa/inet.h>
#include <curses.h>
#include <dirent.h>
#include <sys/resource.h>
#include <uuid/uuid.h>
#include <thread>
#include <regex>
#include "/datadisk1/src/inotify/inotify-cxx.h"

#ifdef IPWORKS_V16
#include "/datadisk1/src/ipworks-16.0/include/ipworks.h" // Email, TCIP, UDP software
#else
#include "/datadisk1/src/ipworks_v9/include/ipworks.h"	 // Email, TCIP, UDP software
#endif

enum          vector_list                               {ALL_VECTORS, POSITION_VECTOR, DESTINATION_VECTOR, LOCAL_VECTOR, ASYNCGOTO_VECTPR};
#pragma once
enum          message_type                              {NORMAL_MSG, DEBUG_MSG};
#pragma once
enum          tddmodeset                                {MODE_ON, MODE_OFF, MUTE_ON, MUTE_OFF};
#pragma once
enum          rcc_state                                 {RADIO, TELEPHONE};
#pragma once
enum          rcc_function                              {NONE_DEFINED, SINGLE, DUAL, WITH_DELAY};
#pragma once
enum          log_type                                  {READABLE_LOG, XML_LOG, DEBUG_LOG, AMI_SCRIPT, JSON_LOG};
#pragma once
enum          conference_member                         {POSITION_CONF, CALLER_CONF, DEST_CONF, TANDEM_CONF, XFER_CONF, FLASH_CONF};
#pragma once 
enum          threadorPorttype                          {NO_PORT_OR_THREAD,MAIN,CAD,ALI,ANI,WRK,LOG,SYC,AGI,AMI,RCC,CTI,LIS,NFY,ECATS,DSB};
#pragma once
enum          Port_Connection_Type                      {NOT_DEF, UDP,TCP};
#pragma once

enum          Port_ALI_Protocol_Type                    {NO_ALI_PROTO, ALI_MODEM, ALI_E2, ALI_SERVICE};
#pragma once

enum          email_type                                {EMAILINFO, EMAILWARNING, EMAILALARM};
#pragma once 
enum          ali_format                                {NO_FORMAT, FOURTEEN_DIGIT, SIXTEEN_DIGIT, E2_PROTOCOL};
#pragma once
enum          data_format                               {FORMAT_ERROR, SEVEN_DIGIT,TEN_TWENTY_DIGIT, XML_DATA_FIELD, ASTERSIK_AMI_LIST, SOFTWARE};
#pragma once
enum          ali_functions                             {NO_ALI_FUNCTION_DEFINED, ALI_BID_REQUEST, ALI_RECEIVED, ALI_ERROR, ALI_TIMEOUT, ALI_UNABLE_TO_BID, SEND_TO_CAD, CAD_SEND_FAIL}; 

#pragma once
enum          ani_functions                             {UNCLASSIFIED, CONNECT, DISCONNECT, TRANSFER, CONFERENCE, HOLD_ON, HOLD_OFF, ABANDONED, ABANDONED_BUSY, ALARM, RINGING, ALI_REQUEST,        
                                                         PLAY_SORRY_MSG, TIME_DATE_CHANGE, TRANSFER_NUMBER_PROGRAM, HEARTBEAT, ALI_REPEAT_SCROLL_BACK, SILENT_ENTRY_LOG_OFF, CANCEL_TRANSFER, TRANSFER_CONNECT, TRANSFER_CONNECT_BLIND, TRANSFER_CONNECT_INTERNAL_NG911, TDD_DISPATCHER_SAYS, TDD_CHAR_RECEIVED, TDD_CALLER_SAYS, TDD_MUTE, TDD_MODE_ON, TDD_MODE_OFF, DIAL_DEST, BARGE_ON_POSITION, BARGE_ON_CONFERENCE, BARGE_ON_POSITION_LINE_ROLL, BARGE_ON_CONFERENCE_LINE_ROLL, NON_TEN_DIGIT_NUMBER_CONNECT, PANI_CONNECT, AGI_ERROR, AGI_TIMEOUT, AGI_FAIL, LOCATION_URI, LOCATION_DATA, LEGACY_ALI_DATA, SMS_MSG_RECEIVED, SMS_MSG_SENT, SMS_SEND_TEXT, TAKE_CONTROL, TAKE_CONTROL_SOFTWARE, UN_CONFERENCE, DELETE_RECORD, UPDATE_CALLBACK, FORCE_DISCONNECT, BLIND_TRANSFER, ATTENDED_TRANSFER, RING_BACK, RING_BACK_CONNECT, TRANSFER_FAIL, ATT_TRANSFER_FAIL, ATT_TRANSFER_REJ, CONFERENCE_JOIN, CONFERENCE_LEAVE, CONFERENCE_ATT_XFER, CONFERENCE_JOIN_LIST, CONFERENCE_BUILD_LIST, CONFERENCE_JOIN_SLA, CONFERENCE_ADD_MEMBER, SEND_FLASH, KILL_CHANNEL, CONFERENCE_ATT_TRANSFER_FAIL, EAVESDROP, UPDATE_ANI_DATA, RETRY_CONF_BLIND_TRANSFER, CLI_TRANSFER, VALET_JOIN, VALET_PARK, MSRP_RINGING, MSRP_ANSWER, MSRP_CONNECT, MSRP_HANGUP, IRR_FILE_DATA, RINGING_POSITION_TO_POSITION, MSRP_ON_HOLD, MSRP_OFF_HOLD, MSRP_CONFERENCE_LEAVE, MSRP_CONFERENCE_JOIN, SYNC_TIME, MERGE_CALL, TEXT_LOG_CONVERSATION, RFAI_BRIDGE, RFAI_DROP_LEG, SEND_MWI, EXEC_FREESWITCH_MEMORY_USAGE_SCRIPT};
#pragma once
enum          location_type                             { NO_TYPE, CIRCLE, POINT, CIVIC_ADDRESS};
#pragma once
enum          lis_functions                             {NO_LIS_FUNCTION, URL_RECIEVED, DEREFERENCE_URL, I3_BY_VALUE, I3_DATA_RECEIVED, ECRF_HTTP_TIMEOUT,
                                                         LEGACY_ALI_DATA_RECIEVED, POST_LIS_LOCATION_REQUEST, LIS_HTTP_TIMEOUT, LIS_HTTP_DATA_RECEIVED};
#pragma once
enum          I3_convert                                {ALI30W, ALI30X, NEXTGEN};
#pragma once
enum          freeswitch_event                           {  NOTFOUND, HANGUP,  HOLD, UNHOLD, FREESWITCH_TDD_CHAR, FREESWITCH_CHANNEL_BRIDGE, FREESWITCH_CHANNEL_ORIGINATE,
                                                            FREESWITCH_CALL_UPDATE, FREESWITCH_CHANNEL_PROGRESS, FREESWITCH_CHANNEL_ANSWER_UPDATE_UNATTENDED_BLIND_TRANSFER,
                                                            REPLY_OK, FREESWITCH_HEARTBEAT, FREESWITCH_CUSTOM, FREESWITCH_REGISTER, FREESWITCH_PRESENCE_IN, FREESWITCH_SLA_JOIN_CALL,
                                                            FREESWITCH_MEDIA_BUG_START, FREESWITCH_MEDIA_BUG_STOP, FREESWITCH_CONFERENCE, FREESWITCH_MYEVENT_MESSAGE, FREESWITCH_CHANNEL_ANSWER,
                                                            FREESWITCH_PRESENCE_PROGRESSING, FREESWITCH_PRESENCE_IDLE, FREESWITCH_SMS_MESSAGE, FREESWITCH_ANSWER_CONFERENCE_TRANSFER, 
                                                            FREESWITCH_SOFIA_ERROR, FREESWITCH_BARGE_WITH_BCF, FREESWITCH_CHANNEL_ANSWER_UPDATE_GUI_BLIND_TRANSFER,
                                                            FREESWITCH_CHANNEL_ANSWER_UPDATE_NG911_INTERNAL_TRANSFER, FREESWITCH_MSRP_TEXT_MESSAGE, FREESWITCH_MSRP_TEXT_MSG_RING,
                                                            FREESWITCH_MSRP_TEXT_MSG_CONNECT,FREESWITCH_VALET_PICKUP, FREESWITCH_NON_VALET_PICKUP, FREESWITCH_VALET_PARK_MSG, 
                                                            FREESWITCH_VALET_EXIT_MSG, FREESWITCH_TRANSFER_RINGBACK_CONNECT, FREESWITCH_MWI_SUMMARY, FREESWITCH_MEM_USAGE  };


#pragma once
enum          nena_geopriv_method                       {ngOther, ngManual, ngCell, ngWireless};

#pragma once
enum          nena_adr_service_environment              {seOther, seResidence, seBusiness};

#pragma once
enum          nena_adr_service_type                     {stOther, stPOTS, stPOTSremote, stCoin, stOneWay, stCOCoin, stVOIP, stVOIPwireless, stVOIPcoin, stWireless, stMLTShosted, stMLTSlocal};

#pragma once
enum          nena_adr_service_mobility			{smOther, smFixed, smMobile, smNomadic};

#pragma once
enum          asterisk_Data_type                        {UNDETERMINED_DATA, TRUNK_DATA, POSITION_DATA, TRANSFER_DATA,  LINK_DATA, DIAL_DATA};
 
#pragma once
enum          ani_format                                {EIGHT_DIGIT, TEN_DIGIT, TWENTY_DIGIT, TEN_DIGIT_NON_STANDARD, TEN_DIGIT_PRECHECKED};

#pragma once
enum          ani_type                                  {CALLBACK, PSUEDO_ANI};

#pragma once
enum          Telephone_Data_type                       {NO_PHONE_DEFINED, GUI_PHONE, NON_GUI_PHONE, BOTH_PHONES, AUDIOCODES, IPGATEWAY, NG911SIPTRUNK, POLYCOM_670, POLYCOM_VVX, YEALINK};
 

#pragma once
enum          transfer_type                             {SIP = 1, SIP_LOCAL = 2, TANDEM = 4, PBX_TELCO = 8};

#pragma once
enum          sip_ng911_transfer_type                   {NO_TRANSFER_DEFINED, SIP_URI_INTERNAL, SIP_URI_EXTERNAL, SIP_URI_EXTERNAL_EXPERIENT};


#pragma once
enum          transfer_method                           {mBLANK, mGUI_TRANSFER, mBLIND_TRANSFER, mATTENDED_TRANSFER, mPOORMAN_BLIND_TRANSFER, mRING_BACK, mTANDEM, 
                                                         mINDGITAL, mNG911_TRANSFER, mFLASHOOK, mINTRADO, mREFER};

#pragma once
enum          ani_system                                {NO_SYSTEM, ANI_LINK, ASTERISK, FREESWITCH}; 

#pragma once
enum          display_mode                              {NO_DISPLAY_DEFINED, MINIMAL_DISPLAY, NORMAL_DISPLAY, DEBUG_KRN, DEBUG_ALI, DEBUG_ANI, DEBUG_CAD, DEBUG_WRK, DEBUG_AMI, 
                                                         DEBUG_RCC, DEBUG_DSB, DEBUG_ALL};                          

#pragma once
enum          bid_type                                  {NO_BID, INITIAL, REBID, MANUAL, AUTO, LATE_RECORD, H_BEAT};

#pragma once
enum          wrk_functions    	                        {BAD_KEY, WRK_HEARTBEAT, WRK_REBID, UPDATE_CAD, WRK_TRANSFER, WRK_CNX_TRANSFER, WRK_LOG, WRK_ALARM, WRK_SEND_INIT_DATA, 
                                                         WRK_CONFERENCE_JOIN, WRK_CONFERENCE_LEAVE, WRK_TAKE_CONTROL, WRK_TDD_MODE_ON, WRK_TDD_MUTE, WRK_TDD_CHARACTER,
                                                         WRK_TDD_STRING, WRK_DIAL_OUT, WRK_CONF_BARGE, WRK_CAD_ERASE, WRK_RELOAD_CONSOLE, WRK_FORCE_DISCONNECT, 
                                                         WRK_RADIO_CONTACT_CLOSURE, WRK_TOGGLE_HOLD, WRK_OFF_HOLD, WRK_BLIND_TRANSFER, WRK_HANGUP, WRK_ANSWER, WRK_BARGE, WRK_MUTE,
                                                         WRK_PICKUP, WRK_VOLUME_UP, WRK_VOLUME_DOWN, WRK_KILL_CHANNEL, WRK_FLASH_TANDEM, WRK_COMPLETE_ATT_XFER, WRK_TEXT_STRING, WRK_DISCONNECT_REQUEST,
                                                         WORKSTATION_LOG_IN, WORKSTATION_LOG_OUT, WRK_TAKE_CONTROL_SOFTWARE, WRK_SEND_DTMF, WRK_PRESS_LINE 
                                                        };

#pragma once
enum          xml_status                                {XML_UNUSED, XML_RINGING, XML_CONNECTED, XML_ABANDONED, XML_DISCONNECTED };

#pragma once
enum          trunk_type                                {CAMA, CLID, LANDLINE, WIRELESS, SIP_TRUNK, NG911_SIP, INDIGITAL_WIRELESS, MSRP_TRUNK, INTRADO, REFER};

#pragma once
enum          trunk_rotate_rule                         {NO_RULE, MULTIPLIER, GROUPCOUNT, DAYS};

#pragma once
enum          message_display_type                      {NORMAL_MESSAGE, ALI_RECORD, NEXT_LINE, TDD_CONVERSATION};

#pragma once
enum          text_type                                 {NO_TEXT_TYPE, TDD_MESSAGE, MSRP_MESSAGE};

#endif /*HEADER_H_*/
