/*****************************************************************************
* FILE: Experient_Controller.cpp
*
* DESCRIPTION:
*   
*
* Important Program Notes:
* 1. all of the time functions will roll back and fail on Jan 19 2038 GMT if the 
*    controller is implemented on a 32 Bit OS.
*
*
* AUTHOR: 8/1/2009 Bob McCarthy
* LAST REVISED:  
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "adr.h"
#include "globalfunctions.h"
#include "/datadisk1/src/simpleini/SimpleIni.h"          // ini software
#include "/datadisk1/src/xmlParser/xmlParser.h"


//forward function declaration


void    Server_Beacon_Broadcast_Event(union sigval union_sigvalArg);
void    Abandoned_Call_Pop_Up_Timed_Event(union sigval union_sigvalArg);
void    Telephone_Monitor_Timer_Event(union sigval union_sigvalArg);
int     Start_Thread(threadorPorttype enumThread);
int     Start_Timed_Threads();
int     Process_CFG_File();
//bool    Find_NTP_Start_Script();
int     Process_Previous_Shutdown_File();
int     Delete_Orderly_Shutdown_File();
int     Delete_Debug_Files();
string  get_ip_address(string stringDevice);
void    SendALItoCAD_SinglePosition(ExperientDataClass objData, bool boolSendAllPositions = false, int intTrunk = 0);
void    SendALItoCAD_Conference_List(int intTrunk);
void    SendANItoALI(ExperientDataClass objData);
bool    Check_Same_Call_WRK_Functions(ExperientDataClass* objData, int intTrunk);
void    Process_Connect(ExperientDataClass objData, int intTrunk);
bool    Check_Connect(ExperientDataClass objData, int intTrunk);
void    Check_Position_Number_Same(ExperientDataClass objData, int intTrunk);
void    SendALItoWRK(ExperientDataClass objData, bool boolShowALIStatus = false);
void    Manual_Bid( ExperientDataClass objData, int intTrunk, bool boolRebidManualBid = false); 
void    UpdateCadfromManualBid(ExperientDataClass objData);
void    UpdateCadfromWorkstationData(ExperientDataClass objData);
bool    CheckIfControllerRunning();
int     FindOldestRingingCall();
void    SendCAD_Erase_Msg(string strPosn, unsigned int iIndex);
void    SendCAD_Erase_Msg_To_Conference_List(unsigned int iIndex);
void    Send_ALI_Fail_PopUp_To_Conference_List(unsigned int iIndex);
void    Queue_WRK_Event(ExperientDataClass objData);
void    Queue_ANI_Event(ExperientDataClass objData);
void    Queue_CTI_Event(ExperientDataClass objData);
void    Queue_LIS_Event(ExperientDataClass objData);
void    Queue_RCC_Event(rcc_state enumRCCstate , ExperientDataClass objData, int intWorkstation, rcc_function enumRCCfunction);
void    cbreak(struct termios tty);
void 	StartUpPingChecks();
bool    CheckForDirectory(string strFullPath);
void    CheckForPhoneALIdirectory();
int 	HighestTrunkNumber(int intArg);
int     findServerNumber(string ipaddress, int CaseSwitch = 0);
void    Display_Controller_Init_Data();
void    Display_Controller_Workstation_Data();
void    Set_Display_Mode(display_mode enumMode);
void    Show_Server_Status_Table();
void    Display_Controller_Up_Time(struct timespec timespecStartTime);
void    Display_Menu();
void    Display_Init_Table_Menu();
void    Display_Thread_Table_Data();
void    Display_ANItableZERO(int i = 0);
void    Display_KRNtableZERO(int i = 0);
void    DisplayEmailsSupressed();
void    Display_Phonebook_List();
void    DisplayDSBinfo();
void    DisplayADVinfo();
void    InitializeClassofServiceVector();
int     IndexOfNewMainTableEntry(ExperientDataClass objData);
int     IndexWithConfNumberMainTable(unsigned long long int iConfNumber);
int     IndexWithCallUniqueIDMainTable(string strUniqueID);
int     IndexWithFreeswitchChannelUUID(string strUUID);
int     IndexWithNENAcallID(string strUUID);
void    Check_Abandoned_Calls(int count);
void    Check_Disconnected_Calls(int count);
void    ShutdownConfirm();
//void    Determine_Dial_Prefix_Rule();
//void    Display_Prefix_Assignments();
void    ForceGUIupdate();
void    Check_Stale_Calls(int count);
void    Force_Disconnect(string strUniqueCallID, bool warning=true);
void    Kernel_Option_Force_Disconnect();
void    Kernel_Option_Reload_Workstation();
void    ToggleAMIScript();
void    DeleteAMIScript();
void    ShutdownSignalCatcher(int iparam);
void    AbortSignalCatcher(int iparam);
void    FPESignalCatcher(int iparam);
void    ILLSignalCatcher(int iparam);
bool    Load_Gateway_Order();
bool    Load_Phone_Ping_Monitor_List();
void    CheckforNGALIinUnmatchedTable(int index, int iTrunk);
void    ScanMainTableforNgALI();
void    Check_Time_in_Debug_Mode();
void    Display_Current_Calls();
void    Check_Abandoned_List(ExperientDataClass objData);
void    Take_Control(int iTableIndex, ExperientDataClass objData);
bool    DisconnectRequest(int iTableIndex);
Port_Connection_Type ConnectionType(string strInput);
int     Delete_PID_File();
void    DisplayUnmatchedI3data();
void    DisplayCPUstatisticsPerThread();
bool    PositionIsOnAnotherCall(int i, unsigned int index);
void    Cleanup();

void    EmailDailyAlarmsSummary();
void    DailyFreeswitchTimeSync();
bool    WorkstationIsOnaCall(int iWorkstation); 
int     FindOpenLineIndexforDialing(int iWorkstation);
int     FindOpenNonDialLineIndexforBarge(int iWorkstation);
void    Check_Single_Channel_Calls(int count);
void    Check_MSRP_NO_ANSWER_Calls(int count);
void    Reload_InitFile();
int     SharedLineThatsAvaiableForBarge(int index);
int     LineAvailableForOnHoldPickoff(int index);
int     LineNumberForOffHold(int index, int iPosition);
int     NumberOfCallsOnHold(int iPosition);
volatile int iNumberRCCdevicesInstalled;
volatile int iNumberRCCdevicesConnected;




//test
void    SendShutdownSignal();
void    SendSegFaultSignal();
void    SendAbortSignal();
void    SendFPESignal();
void    SendILLSignal();
void testTDDmodeOnIndexZero();
void SendTestTDDMessage();
void testWorkstationDoubleAnswer();
void SendHTTPSdereferenceRequestZERO();
void SendInfoMessage();
void SendCancelTandemTransfer();
void SendHTTPSdereferenceData(string strFile, string strUUID, int iPort);
void LoadADR();

Port_Connection_Type    enumWORKSTATION_CONNECTION_TYPE = UDP;

vector <Thread_Data> ThreadData;

ADR_REQUIREMENTS ADR_REQUIRED;

extern void     BecomeMaster();
extern string   DashboardUpdate();

// external thread functions
extern void *Cad_Thread(void *voidArg);
extern void *ALI_Thread(void *voidArg);
extern void *ANI_Thread(void *voidArg);
extern void *WRK_Thread(void *voidArg);
extern void *Log_Console_Email_Thread(void *voidArg);
extern void *AMI_Thread(void *voidArg);
extern void *RCC_Thread(void *voidArg);
extern void *CTI_Thread(void *voidArg);
extern void *LIS_Thread(void *voidArg);
extern void *NFY_Thread(void *voidArg);
extern void *DSB_Thread(void *voidArg);
extern void *Main_ScoreBoard_Event(void *voidArg);
extern void *Main_Messaging_Monitor_Event(void *voidArg);

//extern class variables
extern ExperientCommPort                CADPort                                 [NUM_CAD_PORTS_MAX+1];
extern RCC_Device                       RCCdevice                               [NUM_WRK_STATIONS_MAX+1];
extern ExperientEmail                   objEmailMsg;
extern ServerData                       ServerStatusTable                       [MAX_NUM_OF_SERVERS+1];
extern Trunk_Type_Mapping               TRUNK_TYPE_MAP;
extern ANI_Data_Format                  ANIdataFormat;
extern Telephone_Devices                TelephoneEquipment;
extern Trunk_Sequencing                 Trunk_Map;
extern ExperientCommPort                WRKThreadPort;
extern Conference_Number                objConferenceList;

//iterate display mode
display_mode& operator++(display_mode& s) {
 //Prefix ++ overload
 if (s != NORMAL_DISPLAY){ s = display_mode ( s+1); }
 else                    { s = MINIMAL_DISPLAY;}
 return s;
}




// Global Variables to Experient_Controller
queue <MessageClass>	                queue_objMainMessageQ;
queue <MessageClass>	                queue_objLogMessageQ;
queue <MessageClass>	                queue_objCadMessageQ;
queue <MessageClass>	                queue_objALiMessageQ;
queue <MessageClass>	                queue_objANIMessageQ;
queue <MessageClass>	                queue_objWRKMessageQ;
queue <MessageClass>	                queue_objSYCMessageQ;
queue <MessageClass>                    queue_objOutputMessageQ;
queue <MessageClass>                    queue_objAMIMessageQ;
queue <MessageClass>                    queue_objRCCMessageQ;
queue <MessageClass>                    queue_objCTIMessageQ;
queue <MessageClass>                    queue_objLISMessageQ;
queue <MessageClass>                    queue_objDSBMessageQ;

map <string,string>                     objPreviousMap;
string                                  strCONTROLLER_UUID;
Workstation_Groups                      WorkstationGroups;
Local_Call_Rules                        LocalCallRules;
Initialize_Global_Variables             INIT_VARS;
Memory_Data                             MemoryData;
int                                     Phonebook_fd;
int                                     Phonebook_wd;
WorkStation                             WorkStationTable[NUM_WRK_STATIONS_MAX+1];
// old SYC thread vars
ServerData                              ServerStatusTable[MAX_NUM_OF_SERVERS+1];
volatile bool                           boolNTP_Service_Running                           = true;
sem_t                                   sem_tMutex_ServerStatusTable;
int                                     intMASTER_SLAVE_SYNC_PORT_NUMBER;                                            
int                                     intMASTER_SLAVE_BROADCAST_PORT_NUMBER;
int                                     intMASTER_SLAVE_GATEWAY_INTERROGATE_PORT_NUMBER;
int                                     intMASTER_SLAVE_GATEWAY_REPLY_PORT_NUMBER;
volatile int                            intMenu;
I3_convert                              enumI3CONVERSION_TYPE                             = ALI30W;
bool                                    boolJSON_LOGGING                                  = false;
string                                  strCONFIG_FILE_VERSION                            = "No Value Entered";
string                                  strAGI_VERSION_NUMBER                             = "No Value Detected";
Thread_Trap                             OrderlyShutDown;
Thread_Trap                             UpdateVars;
ExperientCommPort                       BeaconPort;
bool                                    boolSTRATUS_SERVER_MONITOR;
cpu_set_t                               CPU_AFFINITY_ALL_OTHER_THREADS_SET;
cpu_set_t                               CPU_AFFINITY_SYNC_THREAD_SET;

int                                     intNUM_OF_SERVERS;
int                                     intNUM_TRUNKS_INSTALLED;
int                                     intNUM_DSB_PORTS;
ani_system                              enumANI_SYSTEM;
int                                     intCAD_PORT_DOWN_REMINDER_SEC;
int                                     intCAD_PORT_DOWN_THRESHOLD_SEC;
int                                     intALI_PORT_DOWN_REMINDER_SEC;
int                                     intALI_PORT_DOWN_THRESHOLD_SEC;
int                                     intWRK_PORT_DOWN_THRESHOLD_SEC;
int                                     intWRK_PORT_DOWN_REMINDER_SEC;
int                                     intWRK_GUI_TAB_BEHAVIOR_CODE;
int                                     intSYC_PORT_DOWN_THRESHOLD_SEC;
int                                     intSYC_PORT_DOWN_REMINDER_SEC;
int                                     intDEVICE_DOWN_THRESHOLD_SEC;
int                                     intDEVICE_DOWN_REMINDER_SEC;
int                                     intNUM_OF_PROCESSORS;
int                                     intNUM_AUDIOCODES_DEVICES;
int                                     intALI_WIRELESS_AUTO_REBID_SEC;
int                                     intALI_WIRELESS_AUTO_REBID_COUNT;
unsigned int                            intGUI_MAX_ABANDONED_CALLS;
long long int                           intKBused = 0;
bool                                    boolREBID_NG911 = true;
bool                                    boolNG_ALI = true;
bool                                    boolLEGACY_ALI = true;
bool                                    boolLEGACY_ALI_ONLY = false;
bool                                    boolUSE_ALI_SERVICE_SERVER = false;
unsigned long int                       intDEBUG_MODE_REMINDER_SEC  = 3600;
long double                             ldDebugReminder             = 0.0;
long double                             ldMSRP_RING_TIMEOUT         = 60.0;
bool                                    bool_POLYCOM_404_FIRMWARE   = true;       //true fleetwide as of 6/2020
bool                                    bool_FREESWITCH_VERSION_212_PLUS = true;  //true fleetwide as of 6/2020
int                                     intCLID_DIAL_PREFIX_CODE;
//bool                                    boolTREAT_LF_AS_CR = false;
int                                     int911_DIAL_PREFIX_CODE;
int                                     intSIP_DIAL_PREFIX_CODE;
//string                                  strDIAL_PREFIX_STAR;
//string                                  strDIAL_PREFIX_NINE;
//string                                  strDIAL_PREFIX_NONE;
string                                  strIPWORKS_RUN_TIME_KEY = IPWORKS_RUNTIME_KEY;
int                                     intLastErrorCode;
string                                  stringLastErrorMessage;
string                                  stringPSAP_Name;
string                                  stringSERVER_HOSTNAME;
string                                  strASTERISK_AMI_IP_ADDRESS;
string                                  stringPRODUCT_NAME;
string                                  stringCOMPANY_NAME;
int                                     intSERVER_NUMBER;
string                                  strCUSTOMER_SUPPORT_NUMBER;
string                                  strABOUT_SPLASH_FILE;
string                                  strABOUT_SPLASH_COLOR;
string                                  strABOUT_SPLASH_FONT;
string                                  strABOUT_SPLASH_FONT_PT;
string                                  stringPSAP_ID;
string                                  stringOS_NAME;
string                                  strVIEW_POSITION_DEFAULT;
bool                                    boolSINGLE_SERVER_MODE; 
bool                                    boolALLOW_MANUAL_ALI_BIDS;
bool                                    boolMULTICAST;
bool                                    boolBEACON_ACTIVE;
bool                                    boolSEND_TO_CAD_ON_CONFERENCE;
bool                                    boolTDD_AUTO_DETECT;
bool                                    boolSEND_LEGACY_ALI_ON_SIP_TRANSFER = false;
bool                                    bool_BCF_ENCODED_IP                 = false;
bool                                    boolAUDIOCODES_911_ANI_REVERSED     = false;
bool                                    boolShowCallRecorder                = false;
//bool                                    boolSHOW_RING_DIALOG                = false;

int                                     intSHOW_RING_DIALOG                 = 0;
string                                  strRINGTONE911                      = "";
string                                  strRINGTONETEXT                     = "";
string                                  strRINGTONEADMIN                    = "";
string                                  strRINGIMAGE911                     = "";
string                                  strRINGIMAGETEXT                    = "";
string                                  strRINGIMAGEADMIN                   = "";
bool                                    boolENABLE_PING_PHONE_MONITOR       = false;
bool                                    boolCLEAR_DIAL_PAD_AFTER_USE        = false;
int                                     intTELEPHONE_PING_INTERVAL_SEC      = EXPERIENT_PHONE_PING_INTERVAL_SEC;
unsigned long int                       intTelephoneConsecutivePingFailures = 0;
unsigned long int                       intTelephoneConsecutivePingSuccesses = 0;
unsigned long int                       intTelephonePingFailureThreshold    = 3;
unsigned long int                       intTelephonePingSuccessThreshold    = 3;
bool                                    boolPBX_SHUT_DOWN                   = false;   
string                                  strNG911_PHONEBOOK_SIP_TRANSFER_HEADER = "NG911";
string                                  strNG911_PHONEBOOK_SIP_TRANSFER_ICON_FILE = "none";
string                                  strMSRP_LIS_SERVER;
string                                  strNG911_LIS_SERVER;
string                                  strNG911_ECRF_SERVER;
string                                  strNG911_SOS_SERVICE_BID;
string                                  strLIS_CERTIFICATE;
string                                  strECRF_CERTIFICATE;
int                                     intLIS_HTTP_TIMEOUT_SEC;
int                                     intECRF_HTTP_TIMEOUT_SEC;
bool                                    boolMSRP_LIS_USE_POST_ONLY  = false;
bool                                    boolNG911_LIS_USE_POST_ONLY = false;
bool                                    boolSHOW_RAPID_LITE_BUTTON  = false;
bool                                    boolUSE_URI_LEGACY_ALI      = false;
IP_Address                              HOST_IP;
IP_Address                              SYNC_IP;
IP_Address                              REMOTE_IP;
IP_Address                              MULTICAST_GROUP;
IP_Address                              BEACON_IP;
IP_Address                              ECATS_IP;
unsigned int                            ECATS_MSG_BUFFER_SIZE                                   = 1000;
int                                     ECATS_PORT_NUMBER                                       = 50010;
int                                     ECATS_ALARM_DELAY_SEC                                   = 180;
int                                     ECATS_ALARM_REMINDER_SEC                                = 1800;
Port_Connection_Type                    ECATS_PORT_CONNECTION_TYPE;
unsigned int                            intDEFAULT_ALI_NOISE_THRESHOLD                          = 5;
int                                     intBEACON_PORT_NUMBER;
//display and logging
bool                                    BOOL_SEND_ECATS                                         = false;
volatile bool                           boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_LIS_RAW_TRAFFIC                     = false;
volatile bool                           boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC                     = false;
volatile bool                           boolAbandonedCallPopUpActive                            = false;

volatile bool                           boolSHOW_CLI_REGISTRATION_PACKETS                       = false;

volatile bool                           boolDISPLAY_CONSOLE_OUTPUT                              = true;
volatile bool                           boolLOG_DISPLAY_KRN_HEARTBEAT_TRAFFIC                   = false;
volatile bool                           boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC                   = false;
volatile bool                           boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC                   = false;
volatile bool                           boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC                   = false;
volatile bool                           boolLOG_DISPLAY_WRK_HEARTBEAT_TRAFFIC                   = false;
volatile bool                           boolLOG_DISPLAY_RCC_HEARTBEAT_TRAFFIC                   = false;
volatile bool                           CaughtShutdownSignal                                    = false;
volatile bool                           boolDEBUG                                               = false;
bool                                    boolEmailON                                             = true;
string                                  strEMAIL_RELAY_SERVER                                   = "localhost";
string                                  strEMAIL_RELAY_USER                                     = "None";
string                                  strEMAIL_RELAY_PASSWORD                                 = "None";                       
volatile int                            intBecomeMaster                                         = 0;
bool                                    boolUSE_POLYCOM_CTI                                     = true;
int                                     intPOLYCOM_CTI_END_CALL_SOFTKEY                         = 4;
int                                     intPOLYCOM_CTI_HTTP_PORT                                = 80;
int                                     intPOLYCOM_CTI_TCP_PORT                                 = 50030;
int                                     intPOLYCOM_CTI_BARGE_DELAY_MS                           = 200;
bool                                    boolSHOW_LINE_VIEW_TAB                                  = false;
bool                                    boolSHOW_RINGING_TAB                                    = false;
bool                                    bool911_TRANSFER_TANDEM_ONLY                            = false;
bool                                    boolSHOW_CALLBACK_BUTTON                                = false;
bool                                    boolSHOW_SPEED_DIAL_BUTTON                              = false;
bool                                    boolSHOW_DIAL_PAD_BUTTON                                = false;
bool                                    boolSHOW_ANSWER_BUTTON                                  = false;
bool                                    boolSHOW_BARGE_BUTTON                                   = false;
bool                                    boolSHOW_HOLD_BUTTON                                    = false;
bool                                    boolSHOW_HANGUP_BUTTON                                  = false;
bool                                    boolSHOW_MUTE_BUTTON                                    = false;
bool                                    boolSHOW_VOLUME_BUTTON                                  = false;
bool                                    boolSHOW_ATTENDED_XFER_BUTTON                           = false;
bool                                    boolSHOW_FLASH_XFER_BUTTON                              = false;
bool                                    boolSHOW_FLASH_HOOK_BUTTON                              = false;
bool                                    boolSHOW_BLIND_XFER_BUTTON                              = false;
bool                                    boolSHOW_CONF_XFER_BUTTON                               = false;
bool                                    boolSHOW_TANDEM_XFER_BUTTON                             = false;
bool                                    boolSHOW_ALERT_BUTTON                                   = true;
bool                                    boolSHOW_TDD_BUTTON                                     = true;
bool                                    boolSHOW_TEXT_BUTTON                                    = false;
bool                                    boolSHOW_DROP_LAST_LEG_BUTTON                           = false;
bool                                    boolSHOW_IM_BUTTON                                      = false;
bool                                    boolPLAY_DTMF_TONES                                     = false;
bool                                    boolVERIFY_ALI_RECORDS                                  = true;
bool                                    boolREBID_PHASE_ONE_ALI_RECORDS_ONLY                    = true;
bool                                    boolEMAIL_MONTHLY_LOGS                                  = true;
bool                                    boolWRK_DISPLAY_RINGING_CALL_ALWAYS	                    = true;
bool                                    boolSEND_TO_CAD_ON_TAKE_CONTROL                         = true;
string                                  strCTI_USERNAME;
string                                  strCTI_PASSWORD;
unsigned long int                       intMAX_CONFERENCE_NUMBER                                = MAX_CONFERENCE_NUMBER;
unsigned long int                       intMIN_CONFERENCE_NUMBER                                = MIN_CONFERENCE_NUMBER;
string                                  strCONFERENCE_NUMBER_DIAL_PREFIX                        = "";
char                                    charHostName[intHOST_NAME_LENGTH];
char*                                   charEMAIL_SEND_TO_ALARM;
char*                                   charEMAIL_SEND_TO;
char*                                   charEMAIL_FROM;
char*                                   charEMAIL_CC_TO;
char*                                   charEMAIL_LOCAL_HOST;
string                                  stringEMAIL_SEND_TO_ALARM;
string                                  stringEMAIL_SEND_TO;
string                                  stringEMAIL_FROM;
string                                  stringEMAIL_CC_TO;
display_mode                            enumDISPLAY_MODE;
string                                  stringVERSION_NUMBER;
unsigned int                            intAVAIL_DISK_SPACE_WARN_GIG;

int                                     intALARM_POPUP_DURATION_MS;
int                                     intTCP_RECONNECT_RECURSION_LEVEL                        = 7;
int                                     intTCP_RECONNECT_INTERVAL_SEC                           = 5;
int                                     intSIP_PHONE_MIN_JITTER                                 = 20;
int                                     intSIP_PHONE_MAX_JITTER                                 = 50;
int                                     intSIP_PHONE_JITTER_DEPTH                               = 5;
bool                                    boolSIP_PHONE_ECHO_CANCELLATION                         = false;
bool                                    boolSIP_PHONE_SILENCE_DETECTION                         = true;
bool                                    boolSIP_PHONE_AUTO_GAIN_CONTROL                         = false;
unsigned int                            intSIP_PHONE_UDP_PORT                                   = 5060;
unsigned int                            intRCC_REMOTE_TCP_PORT_NUMBER                           = 4998;
string                                  strDISPATCHER_SAYS_PREFIX_EXPORT;
string                                  strCALLER_SAYS_EXPORT;
string                                  strESME_ID_STRING;
bool                                    boolRCC_DEFAULT_CONTACT_POSITIONS                       = true;

string                                  strGATEWAY_TRANSFER_ORDER;
string                                  strTELEPHONE_PING_LIST;

bool                                    boolROUTING_PREFIXES_USED                               = false;
bool                                    boolADD_NINE_FOR_OUTBOUND_CALLS                         = false;
bool                                    boolShowPSAPstatusApplet                                = false;
bool                                    boolShowSMSapplet                                       = false;
bool                                    boolShowPSAPappletAdmin                                 = false;
unsigned int                            intDISPLAY_SPACES;
char                                    charCADHEARTBEATMSG[5]                                  = {(char) 2,'H',(char) 3, 'K','\0'};
char                                    charALIHEARTBEATMSG[3]                                  = {'H',(char) 13,'\0'};
string                                  strTWO_CR_LF;
volatile bool                           boolUPDATE_VARS                                         = false;
volatile bool                           boolSTOP_EXECUTION                                      = false;
volatile bool                           boolSTOP_LOG_THREAD                                     = false;      
volatile bool                           boolEMAIL_QUEUE_ACTIVE                                  = false;
volatile bool                           boolIsMaster                                            = false;
volatile bool                           boolSendBeaconOnce                                      = false;
bool                                    boolUSE_DASHBOARD                                       = true;
string                                  strDASHBOARD_API_KEY;
int                                     intTDD_CPS_THRESHOLD_SEC                                = 4;
const char                              charACK                                                 = (char) 6;
const char                              charNAK                                                 = (char) 21;
const char                              charCR                                                  = (char) 13;
const char                              charSTX                                                 = (char) 2;
const char                              charETX                                                 = (char) 3;
const char                              charLF                                                  = (char) 10;
const char                              charTAB                                                 = (char) 9;
const char                              charBKSP                                                = (char) 8;
const char                              charSPACE                                               = (char) 32;
const float                             floatNANOSEC_PER_SEC   	                                = 1000000000.0;
const long double                       longdoubleNANOSEC_PER_SEC                               = 1000000000.0;
const long int                          intNANOSEC_PER_SEC                                      = 1000000000;


int                                     intNUM_WRK_STATIONS;

const int                               XML_STATUS_CODE_CONNECT                                 = 1; 
const int                               XML_STATUS_CODE_DISCONNECT                              = 2;
const int                               XML_STATUS_CODE_ON_HOLD                                 = 5;
const int                               XML_STATUS_CODE_OFF_HOLD_CONNECTED                      = 6;
const int                               XML_STATUS_CODE_ABANDONED                               = 7;
const int                               XML_STATUS_CODE_ABANDONED_CLEARED                       = 8;

const int                               XML_STATUS_CODE_RINGING                                 = 11;
const int                               XML_STATUS_CODE_DIALING                                 = 12;
const int                               XML_STATUS_CODE_TRANSFER_CANCEL                         = 13;
const int                               XML_STATUS_CODE_ANI_DONT_UPDATE_STATUS                  = 60;
const int                               XML_STATUS_CODE_ANI_RINGBACK                            = 76;
const int                               XML_STATUS_CODE_SMS_TEXT_SENT                           = 77;
const int                               XML_STATUS_CODE_SMS_TEXT_RECEIVED                       = 78;
const int                               XML_STATUS_CODE_ALI_BID_NOT_ALLOWED                     = 79; 
const int                               XML_STATUS_CODE_ALI_BID_RETRY                           = 80;
const int                               XML_STATUS_CODE_ALI_NO_GEOLOCATION_URI                  = 81;
const int                               XML_STATUS_CODE_TDD_CHANGE_MUTE                         = 83;
const int                               XML_STATUS_CODE_TDD_DISPATCHER_SAYS                     = 84;
const int                               XML_STATUS_CODE_TDD_CALLER_SAYS                         = 85;
const int                               XML_STATUS_CODE_TDD_CHAR_RECEIVED                       = 86;
const int                               XML_STATUS_CODE_ALI_MANUAL_BID                          = 87;
const int                               XML_STATUS_CODE_MAN_ALI_TIMEOUT                         = 88;
const int                               XML_STATUS_CODE_AUTO_ALI_REBID                          = 89;
const int                               XML_STATUS_CODE_AUTO_ALI_TIMEOUT                        = 90;
const int                               XML_STATUS_CODE_ALI_REBID                               = 91;
const int                               XML_STATUS_CODE_ALI_TIMEOUT                             = 92;
const int                               XML_STATUS_CODE_ALI_RECEIVED                            = 93;
const int                               XML_STATUS_CODE_MAN_ALI_RECEIVED                        = 94;
const int                               XML_STATUS_CODE_AUTO_ALI_RECEIVED                       = 95;
const int                               XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS                  = 96;
const int                               XML_STATUS_CODE_SEND_TO_CAD                             = 97; 
const int                               XML_STATUS_CODE_CONFERENCEJOIN                          = 98;
const int                               XML_STATUS_CODE_CONFERENCELEAVE                         = 99;

int                             	intSecondsRebidsActivePostDisconnect                    = 300;
int                                     intSecondsToKeepDisconnectInTable                       = 400;


string                                  stringDEFAULT_GATEWAY_DEVICE;
string                                  stringSYNC_ETHERNET_DEVICE;


// files
string                                  stringCurrentJSONlogFileName                            = "";
string                                  stringCurrentLogFileName                                = "";
string                                  strCurrentLogFileMonth                                  = "";
string                                  stringCurrentXMLlogFileName                             = "";
string                                  stringCurrentDebugFileName                              = "";
string                                  stringCurrentAMIlogFileName                             = "";
ofstream                                JSONlogfile;
ofstream                                ReadableLogFile;
ofstream                                XMLlogfile;
ofstream                                DebugLogFile;
ofstream                                AMIscriptLogfile;
ofstream                                CTI_TCP_PORT_FILE;

// counters
unsigned long long int                  intLogFileWriteError                                    = 0;
unsigned long long int                  intLogFileOpenError                                     = 0;
unsigned long long int                  intSEMAPHORE_ERRORS                                     = 0;
unsigned long long int                  intSignalSemaphoreFailureCounter                        = 0;
unsigned long long int                  intSyncGatewayPingCounter                               = 0;
volatile int                            intDayofMonth;
volatile int                            intDailyAlarmEmail                                      = 0;
volatile int                            intDayofMonthFreeswitchTimeSyncSent                     = 0;
// semaphores
sem_t                                   sem_tMutexMainMessageQ;
sem_t                                   sem_tMutexLogMessageQ;
sem_t                                   sem_tMutexOutputMessageQ;
sem_t                                   sem_tMutexDashboardMessageQ;
sem_t                                   sem_tLogConsoleEmailFlag;
sem_t                                   sem_tMutexMainInputQ;
sem_t                                   sem_tMutexMainWorkTable;
sem_t                                   sem_tMutexCLIMessageQ;
sem_t                                   sem_tMutexRCCMessageQ;
sem_t                                   sem_tMutexRCCInputQ;
sem_t                                   sem_tMutexRCCtable;
sem_t                                   sem_tMutexCTIMessageQ;
sem_t                                   sem_tMutexLISMessageQ;
sem_t                                   sem_tMutexDSBMessageQ;
sem_t                                   sem_tRCCflag;
sem_t                                   sem_tCTIflag;
sem_t                                   sem_tLISflag;
sem_t                                   sem_tDSBflag;
sem_t                                   sem_tMutexAMIInputQ;
sem_t                                   sem_tMutexCTIInputQ;
sem_t                                   sem_tMutexLISInputQ;
sem_t                                   sem_tMutexDSBInputQ;
sem_t                                   sem_tMutexCall_LIST;
sem_t                                   sem_tMutexLISWorkTable;
sem_t                                   sem_tMutexLISPortDataQ;
sem_t                                   sem_tMutexDSBPortDataQ;
sem_t                                   sem_tMutexUnmatchedALIData;
sem_t                                   sem_tMutexDebugLogName;
sem_t                                   sem_tMutexEmailSupress;
sem_t                                   sem_tMutexWorkstationHistory;
sem_t                                   sem_tMutexThreadData;

// threads
pthread_attr_t                          pthread_attr_tAttr;
pthread_attr_t                          pthread_attr_tAttrJoinable;								
pthread_t                               pthread_tCadThread; 
pthread_t                               pthread_tLogConsoleEmailThread;
pthread_t                               pthread_tALiThread;
pthread_t                               pthread_tANIThread;
pthread_t                               pthread_tWRKThread;
pthread_t                               pthread_tRCCThread;
pthread_t                               pthread_tAMIThread;
pthread_t                               pthread_tCTIThread;
pthread_t                               pthread_tNFYThread;
pthread_t                               pthread_tLISThread;
pthread_t                               pthread_tDSBThread;
pthread_t                               pthread_tMainScoreboardThread;
pthread_t                               pthread_tMainMessagingThread;

struct itimerspec                       itimerspecDisableTimer;
//struct itimerspec                         itimerspecMainHealthMonitorDelay;			
//struct itimerspec                         itimerspecMainScoreboardDelay;
struct itimerspec                       itimerspecServer_Beacon_Broadcast_Interval;
struct itimerspec                       itimerspecServer_Telephone_Ping_Monitor_Interval;
struct itimerspec                       itimerspecAbandonedPopUpInterval;

//timer_t                                   timer_tHealthMonitorTimerId;
//timer_t                                   timer_tMainScoreboardTimerId;
timer_t                                 timer_tServer_Beacon_Broadcast_Event_TimerId;
timer_t	                                timer_tAbandoned_Pop_Up_Event_TimerId;
timer_t                                 timer_tServer_Phone_Ping_Monitor_Event_TimerId;

struct timespec                         timespecNanoDelay;
struct timespec                         timespecTempDelay;

struct sigevent                         sigeventAbandonedPopUpEvent;

struct timespec                         timespecDebugMode;
struct timespec                         timespecControllerStartTime;

queue <ExperientDataClass>              queue_objCadData;
queue <ExperientDataClass>              queue_objAliData;
queue <ExperientDataClass>              queue_objANIData;
queue <ExperientDataClass>              queue_objMainData;
queue <ExperientDataClass>              queue_objWRKData;
queue <ExperientDataClass>              queue_objAMIData;
queue <ExperientDataClass>              queue_objCTIData;
queue <ExperientDataClass>              queue_objLISData;
queue <ExperientDataClass>              queue_objDSBData;

vector <ExperientDataClass>             vMainWorkTable;
deque  <ExperientDataClass>             vWorkstationHistory;
vector <ExperientDataClass>             vUnmatchedALIData;

Call_Data                               objBLANK_CALL_RECORD;
Text_Data                               objBLANK_TEXT_DATA;
Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
ALI_Data                                objBLANK_ALI_DATA;
Port_Data                               objBLANK_KRN_PORT;
Port_Data                               objBLANK_CAD_PORT;
Port_Data                               objBLANK_ANI_PORT;
Port_Data                               objBLANK_ALI_PORT;
Port_Data                               objBLANK_LOG_PORT;
Port_Data                               objBLANK_WRK_PORT;
//Port_Data                               objBLANK_SYC_PORT;
//Port_Data                               objBLANK_AGI_PORT;
Port_Data                               objBLANK_AMI_PORT;
Port_Data                               objBLANK_RCC_PORT;
Port_Data                               objBLANK_CTI_PORT;
Port_Data                               objBLANK_LIS_PORT;
Port_Data                               objBLANK_NFY_PORT;
Port_Data                               objBLANK_DSB_PORT;

vector <string>                         VstringNumbers(intMAX_ALI_PORT_PAIRS+1);
vector <string>                         NPD(8);
vector <string>                         vstringWRK_XML_KEYS(NUM_WRK_XML_KEYS);
vector <string>                         vstrCLASS_OF_SERVICE_ABBV(NUM_CLASS_OF_SERVICE_ABBV);
vector <string>                         vGATEWAY_TRANSFER_ORDER;
vector <PhoneBookEntry>                 vCALL_LIST;
vector <int>                            vTELEPHONE_PING_LIST;

string                                  stringALI_LOG_SPACES;

string                                  strCALLER_ID_NAME;
string                                  strCALLER_ID_NUMBER;
string                                  stringNTP_SERVICE_START_SCRIPT;
string                                  stringNTP_SERVICE_STOP_SCRIPT;
string                                  stringUBUNTU_VERSION;

string                                  strTEST_TDD_CONVERSATION;

ofstream                                WorkstationInitfile;

//*****************************MAIN PROGRAM *************************************************
int main(int argc, char *argv[]){

int                     intRC; 			
ExperientDataClass      objCadDataMain, objALiDataMain;
MessageClass            objMessage;
struct sigevent         sigeventMainHealthMonitorEvent;
struct sigevent         sigeventMainScoreboardEvent;
struct sigevent         sigeventServerBeaconBroadcastEvent;
struct sigevent         sigeventServerPhonePingMonitorEvent;
string                  stringTest;
string                  strTestUUID;
bool                    boolDebugLoop = false;
string                  stringMessage;                 
ExperientEmail          objTestEmail;
//struct timespec         timespecMenuSleepTime;
struct termios          oldt;
Thread_Data             ThreadDataObj;      



/*
if (argc == 1) {
 cout << argv << argv[0] << endl;
 cout << argv << argv[1] << endl;

return 0;
}
*/

ThreadData.clear();
ThreadDataObj.strThreadName ="MAIN";
//ThreadDataObj.ThreadPID     = getpid();
//ThreadDataObj.ThreadID      = syscall(SYS_gettid);
//ThreadDataObj.pth_SelfID    = pthread_self();

ThreadData.push_back(ThreadDataObj);


//timespecMenuSleepTime.tv_sec = 0;
//timespecMenuSleepTime.tv_nsec = 1000000; //    1/1000 sec

strCONTROLLER_UUID = ThreadDataObj.strThread_UUID;

strTEST_TDD_CONVERSATION = "Caller:HERE IS A TEST CONVESATION to test if the tdd conversation will wrap correctly I am the Caller: wrap wrap wrap wrap wrap";
strTEST_TDD_CONVERSATION += charETX;
strTEST_TDD_CONVERSATION += "Dispatcher 1:WHAT IS YOUR EMERGENCY does mine wrap as well as yours does, I am not sure? I am Dispatcher 1: wrap wrap wrap wrap";
strTEST_TDD_CONVERSATION += charLF;
strTEST_TDD_CONVERSATION += "Line feed";
strTEST_TDD_CONVERSATION += charCR;
strTEST_TDD_CONVERSATION += "Carriage Return";
strTEST_TDD_CONVERSATION += charCR;
strTEST_TDD_CONVERSATION += charLF;
strTEST_TDD_CONVERSATION += "Carriage Return and Line Feed";
strTEST_TDD_CONVERSATION += charETX;

// Time Hack for start
clock_gettime(CLOCK_REALTIME, &timespecControllerStartTime);

// initialize vector holding string representation of the integers 1 thru XX
for (int i = 1; i <= intMAX_ALI_PORT_PAIRS; i++){VstringNumbers[i] = int2str(i);}

// initialize vectors holding XML Keys
vstringWRK_XML_KEYS[0]  = WRK_XML_NODE_KEY_0;
vstringWRK_XML_KEYS[1]  = WRK_XML_NODE_KEY_1;
vstringWRK_XML_KEYS[2]  = WRK_XML_NODE_KEY_2;
vstringWRK_XML_KEYS[3]  = WRK_XML_NODE_KEY_3;
vstringWRK_XML_KEYS[4]  = WRK_XML_NODE_KEY_4;
vstringWRK_XML_KEYS[5]  = WRK_XML_NODE_KEY_5;
vstringWRK_XML_KEYS[6]  = WRK_XML_NODE_KEY_6;
vstringWRK_XML_KEYS[7]  = WRK_XML_NODE_KEY_7;
vstringWRK_XML_KEYS[8]  = WRK_XML_NODE_KEY_8;
vstringWRK_XML_KEYS[9]  = WRK_XML_NODE_KEY_9;
vstringWRK_XML_KEYS[10] = WRK_XML_NODE_KEY_10;
vstringWRK_XML_KEYS[11] = WRK_XML_NODE_KEY_11;
vstringWRK_XML_KEYS[12] = WRK_XML_NODE_KEY_12;
vstringWRK_XML_KEYS[13] = WRK_XML_NODE_KEY_13;
vstringWRK_XML_KEYS[14] = WRK_XML_NODE_KEY_14;
vstringWRK_XML_KEYS[15] = WRK_XML_NODE_KEY_15;
vstringWRK_XML_KEYS[16] = WRK_XML_NODE_KEY_16;
vstringWRK_XML_KEYS[17] = WRK_XML_NODE_KEY_17;
vstringWRK_XML_KEYS[18] = WRK_XML_NODE_KEY_18;
vstringWRK_XML_KEYS[19] = WRK_XML_NODE_KEY_19;
vstringWRK_XML_KEYS[20] = WRK_XML_NODE_KEY_20;
vstringWRK_XML_KEYS[21] = WRK_XML_NODE_KEY_21;
vstringWRK_XML_KEYS[22] = WRK_XML_NODE_KEY_22;
vstringWRK_XML_KEYS[23] = WRK_XML_NODE_KEY_23;
vstringWRK_XML_KEYS[24] = WRK_XML_NODE_KEY_24;
vstringWRK_XML_KEYS[25] = WRK_XML_NODE_KEY_25;
vstringWRK_XML_KEYS[26] = WRK_XML_NODE_KEY_26;
vstringWRK_XML_KEYS[27] = WRK_XML_NODE_KEY_27;
vstringWRK_XML_KEYS[28] = WRK_XML_NODE_KEY_28;
vstringWRK_XML_KEYS[29] = WRK_XML_NODE_KEY_29;
vstringWRK_XML_KEYS[30] = WRK_XML_NODE_KEY_30;
vstringWRK_XML_KEYS[31] = WRK_XML_NODE_KEY_31;
vstringWRK_XML_KEYS[32] = WRK_XML_NODE_KEY_32;
vstringWRK_XML_KEYS[33] = WRK_XML_NODE_KEY_33;
vstringWRK_XML_KEYS[34] = WRK_XML_NODE_KEY_34;

// Initialize Blank Port/Thread Variables
objBLANK_KRN_PORT.fLoadThreadData(MAIN);
objBLANK_CAD_PORT.fLoadThreadData(CAD);
objBLANK_ANI_PORT.fLoadThreadData(ANI);
objBLANK_ALI_PORT.fLoadThreadData(ALI);
objBLANK_LOG_PORT.fLoadThreadData(LOG);
objBLANK_WRK_PORT.fLoadThreadData(WRK);
//objBLANK_SYC_PORT.fLoadThreadData(SYC);
//objBLANK_AGI_PORT.fLoadThreadData(AGI);
objBLANK_AMI_PORT.fLoadThreadData(AMI);
objBLANK_RCC_PORT.fLoadThreadData(RCC);
objBLANK_CTI_PORT.fLoadThreadData(CTI);
objBLANK_LIS_PORT.fLoadThreadData(LIS);
objBLANK_NFY_PORT.fLoadThreadData(NFY);
objBLANK_DSB_PORT.fLoadThreadData(DSB);

// load 2 carriage returns & Linefeeds to variable
strTWO_CR_LF = charCR; strTWO_CR_LF += charLF;strTWO_CR_LF += charCR; strTWO_CR_LF += charLF;

// get OS version of Ubuntu
//system( GET_OPERTATING_SYSTEM_INFO_COMMAND); 


// check if Program is running already
if (CheckIfControllerRunning()){exit(0);}

//thread shutdown trap wait delay
timespecNanoDelay.tv_sec 	= 0;
timespecNanoDelay.tv_nsec 	= intNANO_DELAY_THREAD_WAIT_ON_SHUTDOWN;

// version number
stringVERSION_NUMBER  = charVERSION_NUMBER;

// find hostname
size_t temp = intHOST_NAME_LENGTH+20; 
gethostname(charHostName, temp);
stringSERVER_HOSTNAME = charHostName;
charEMAIL_LOCAL_HOST  = charHostName;

// calculate display spaces available before wrapping
//this is calculated the same as stringPrefix in fn MessageClass::fMessage_Create()
stringTest = stringSERVER_HOSTNAME + " | " + get_time_stamp(timespecControllerStartTime) + " " + "000" + " | " + LOG_BLANK_CALL_DATA_STRING;
intDISPLAY_SPACES = LOG_DISPLAY_LINE_LENGTH - stringTest.length();


//set to constant length
if (stringSERVER_HOSTNAME.length() > intHOST_NAME_LENGTH)
{
 Abnormal_Exit(MAIN, EX_CONFIG, CFG_MESSAGE_023, int2str(stringSERVER_HOSTNAME.length()), int2str(intHOST_NAME_LENGTH));
}
else if (stringSERVER_HOSTNAME.length() < intHOST_NAME_LENGTH)
{
 stringSERVER_HOSTNAME.append( (intHOST_NAME_LENGTH -stringSERVER_HOSTNAME.length()), ' ');
}

// Init Class of Service AbbV's
InitializeClassofServiceVector();

// initialize semaphores
sem_init(&sem_tMutexThreadData,0,1); 
sem_init(&sem_tMutexUnmatchedALIData,0,1);     
sem_init(&sem_tMutexCadWorkTable,0,1);
sem_init(&sem_tMutexAliWorkTable,0,1);
sem_init(&sem_tMutexAliDisconnectList,0,1);
sem_init(&sem_tMutexMainWorkTable,0,1);
sem_init(&sem_tMutexWRKWorkTable,0,1);
sem_init(&sem_tMutexRCCtable,0,1);
sem_init(&sem_tMutexANIWorkTable,0,1);
sem_init(&sem_tMutexLISWorkTable,0,1);
sem_init(&sem_tMutex_ServerStatusTable,0,1);
sem_init(&sem_tMutexCadInputQ,0,1);
sem_init(&sem_tMutexAliInputQ,0,1);
sem_init(&sem_tMutexMainInputQ,0,1);
sem_init(&sem_tMutexWRKInputQ,0,1);
sem_init(&sem_tMutexANIInputQ,0,1);
sem_init(&sem_tMutexAMIInputQ,0,1);
sem_init(&sem_tMutexRCCInputQ,0,1);
sem_init(&sem_tMutexCTIInputQ,0,1);
sem_init(&sem_tMutexLISInputQ,0,1);
sem_init(&sem_tMutexDSBInputQ,0,1);
sem_init(&sem_tMutexMainMessageQ,0,1);
sem_init(&sem_tMutexLogMessageQ,0,1);
sem_init(&sem_tMutexCadMessageQ,0,1);
sem_init(&sem_tMutexAliMessageQ,0,1);
sem_init(&sem_tMutexANIMessageQ,0,1);
sem_init(&sem_tMutexWRKMessageQ,0,1);
sem_init(&sem_tMutexRCCMessageQ,0,1);
sem_init(&sem_tMutexCLIMessageQ,0,1);
sem_init(&sem_tMutexCTIMessageQ,0,1);
sem_init(&sem_tMutexLISMessageQ,0,1);
sem_init(&sem_tMutexDSBMessageQ,0,1);
sem_init(&sem_tMutexOutputMessageQ,0,1);
sem_init(&sem_tMutexDebugLogName,0,1);
sem_init(&sem_tMutexEmailSupress,0,1);                                           
sem_init(&sem_tMutexCadPortDataQ,0,1);
sem_init(&sem_tMutexAliPortDataQ,0,1);
sem_init(&sem_tMutexANIPortDataQ,0,1);
sem_init(&sem_tMutexWRKPortDataQ,0,1);
sem_init(&sem_tMutexLISPortDataQ,0,1);
sem_init(&sem_tMutexDSBPortDataQ,0,1);
sem_init(&sem_tMutexCall_LIST,0,1);
sem_init(&sem_tMutexANIPort,0,1);
sem_init(&sem_tMutexWorkstationHistory,0,1);

sem_init(&sem_tMutexE2TransactionID,0,1);
sem_init(&sem_tCadFlag,0,0);
sem_init(&sem_tLogConsoleEmailFlag,0,0);
sem_init(&sem_tAliFlag,0,0);
sem_init(&sem_tANIFlag,0,0);
sem_init(&sem_tWRKFlag,0,0);
sem_init(&sem_tRCCflag,0,0);
sem_init(&sem_tCTIflag,0,0);
sem_init(&sem_tLISflag,0,0);
sem_init(&sem_tDSBflag,0,0);

//sem_init(&sem_tServerHeartbeatRecievedFlag,0,0);
//sem_init(&sem_tGatewayTimedResponseFlag, 0, 0);
///sem_init(&sem_tGatewayReplyFlag, 0, 0);
//sem_init(&sem_tGatewayInterrogateReplyFlag, 0, 0);
sem_init(&sem_tConfAssignmentFlag, 0, 0);
sem_init(&sem_tAMIFlag, 0, 0);
sem_init(&sem_tMutexConfAssignmentQ,0,1);
sem_init(&sem_tMutexConferenceNumberAssignmentObject,0,1);


// process INI file
Process_CFG_File();





// Find Network Time Protocol file
//Find_NTP_Start_Script();


/*
// create thread CPU affinity sets..
intNUM_OF_PROCESSORS = sysconf(_SC_NPROCESSORS_CONF);
if(intNUM_OF_PROCESSORS < 1){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_052 ); }

// create empty sets
CPU_ZERO (&CPU_AFFINITY_ALL_OTHER_THREADS_SET);
CPU_ZERO (&CPU_AFFINITY_SYNC_THREAD_SET); 

for (int i = 0; i < intNUM_OF_PROCESSORS; i++){CPU_SET (i, &CPU_AFFINITY_ALL_OTHER_THREADS_SET);}
CPU_SET (intNUM_OF_PROCESSORS - 1, &CPU_AFFINITY_SYNC_THREAD_SET);

if ((!boolSINGLE_SERVER_MODE)&&(intNUM_OF_PROCESSORS > 1))
{
 CPU_CLR (intNUM_OF_PROCESSORS - 1, &CPU_AFFINITY_ALL_OTHER_THREADS_SET); 
}
*/

//check for emailqueue directory
CheckForDirectory(charEMAIL_QUEUE_DIR);
CheckForPhoneALIdirectory(); 

// main thread created message
if (boolDEBUG){Console_Message( "110 |", KRN_MESSAGE_110, int2strLZ(getpid()) ,int2strLZ(pthread_self()) );}

// intialize email
objEmailMsg.fIntializeHeaders();
objEmailMsg.Connect();



// init beacon port
if(boolBEACON_ACTIVE)
 { 

  BeaconPort.enumPortType                                = MAIN;
  BeaconPort.intPortNum                                  = 1;
  BeaconPort.ePortConnectionType                         = UDP;
  BeaconPort.UDP_Port.enumPortType                       = MAIN;
  BeaconPort.UDP_Port.intPortNum                         = 1; 
  BeaconPort.UDP_Port.intPortDownThresholdSec            = intBEACON_PORT_DOWN_THRESHOLD_SEC;
  BeaconPort.UDP_Port.intPortDownReminderThreshold       = intBEACON_PORT_DOWN_REMINDER_SEC ; 
  BeaconPort.UDP_Port.boolPortWasDown                    = false;
  BeaconPort.UDP_Port.boolPortAlarm                      = false;
#ifdef IPWORKS_V16
  BeaconPort.UDP_Port.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
  BeaconPort.UDP_Port.Config((char*)"UseConnection=true");
  BeaconPort.UDP_Port.Config((char*)"QOSFlags=16||160");						// critical..minimize delay
  BeaconPort.UDP_Port.Config((char*)"MaxPacketSize=512");
  BeaconPort.UDP_Port.SetTimeToLive(254);
  BeaconPort.UDP_Port.SetLocalHost(NULL);
  BeaconPort.UDP_Port.SetLocalPort(intBEACON_PORT_NUMBER);
  BeaconPort.UDP_Port.SetRemoteHost((char*)BEACON_IP.stringAddress.c_str());
  BeaconPort.UDP_Port.SetRemotePort(intBEACON_PORT_NUMBER);
  BeaconPort.UDP_Port.fPortData();
//  sem_init(&BeaconPort.UDP_Port.sem_tMutexUDPPortBuffer,0,1);
//  sem_init(&BeaconPort.UDP_Port.sem_tMutexDiscardedPackets,0,1); 
  BeaconPort.UDP_Port.intDiscardedPackets                = 0;
  BeaconPort.fSetPortActive();
  intRC = BeaconPort.UDP_Port.SetActive(true);
  if (intRC){BeaconPort.UDP_Port.Error_Handler();} 
 }

//check for consecutive unexpected shutdowns 
Process_Previous_Shutdown_File();
//Delete_Orderly_Shutdown_File();

if (boolIsMaster){stringMessage = "Master";}
else             {stringMessage = "Slave";}
// start up message
objMessage.fMessage_Create(LOG_INFO,101, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_101, stringVERSION_NUMBER, stringMessage, strCONTROLLER_UUID);
enQueue_Message(MAIN,objMessage);

// turn off NTP if in dual Server Mode , On if Single Server Mode .... (NTP now done by VMSphere)
//NTP_Time_Server_Control(MAIN, boolSINGLE_SERVER_MODE);

objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,102, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                           objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_102); 
enQueue_Message(MAIN,objMessage);

//Initialize Timer Disable Structure
Set_Timer_Delay(&itimerspecDisableTimer, 0, 0, 0, 0);

// Initialize and set thread detached attribute 
pthread_attr_init(&pthread_attr_tAttr);
pthread_attr_setdetachstate(&pthread_attr_tAttr, PTHREAD_CREATE_DETACHED);

pthread_attr_init(&pthread_attr_tAttrJoinable);
pthread_attr_setdetachstate(&pthread_attr_tAttrJoinable, PTHREAD_CREATE_JOINABLE);

//start LOG thread
intRC = Start_Thread(LOG);

StartUpPingChecks();
/*
// set up Main Health monitor timer
sigeventMainHealthMonitorEvent.sigev_notify 			= SIGEV_THREAD;
sigeventMainHealthMonitorEvent.sigev_value.sival_ptr 		= (void *) &queue_objOutputMessageQ;		
sigeventMainHealthMonitorEvent.sigev_notify_function 		= Main_Messaging_Monitor_Event;
sigeventMainHealthMonitorEvent.sigev_notify_attributes 		= NULL;
intRC = timer_create(CLOCK_MONOTONIC, &sigeventMainHealthMonitorEvent, &timer_tHealthMonitorTimerId);
if (intRC){Abnormal_Exit(MAIN, EX_OSERR, KRN_MESSAGE_180, "timer_tHealthMonitorTimerId", int2strLZ(errno));}

//Set_Timer_Delay(&itimerspecMainHealthMonitorDelay, 0,intMAIN_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intMAIN_MESSAGE_MONITOR_INTERVAL_NSEC );
Set_Timer_Delay(&itimerspecMainHealthMonitorDelay, 0,intMAIN_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0 );             // One shot timer
timer_settime( timer_tHealthMonitorTimerId, 0, &itimerspecMainHealthMonitorDelay, NULL);			// arm cleanup timer
*/

// Health Monitor Timer Event Initialized message
objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,104, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_104); 
enQueue_Message(MAIN,objMessage);


// start Cad thread if CAD Exists
if(boolCAD_EXISTS) {Start_Thread(CAD);}
else               {OrderlyShutDown.boolCadThreadReady = true;}

// start ALI thread
if(true) 			        {Start_Thread(ALI);}
else                                    {OrderlyShutDown.boolALiThreadReady = true;}
if(true)                        	{Start_Thread(LIS);}
else                                    {OrderlyShutDown.boolLISThreadReady = true;}
// start ANI thread
Start_Thread(ANI);

//start WRK thread
Start_Thread(WRK);

//start AMI thread
Start_Thread(AMI);

if (boolCONTACT_RELAY_INSTALLED) {Start_Thread(RCC);}
else                             {OrderlyShutDown.boolRCCThreadReady = true;}

if (boolUSE_POLYCOM_CTI)         {Start_Thread(CTI);}
else                             {OrderlyShutDown.boolCTIThreadReady = true;}

if (true)                        {Start_Thread(DSB);}
else                             {OrderlyShutDown.boolDSBThreadReady = true;}

Start_Thread(NFY);

// Sleep 2 seconds to allow thread(s) creation
objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,105,__LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT,
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_105); 
enQueue_Message(MAIN,objMessage);
sleep(2);

Start_Timed_Threads();

/*
// setup sigeventMainScoreboardEvent timer event
sigeventMainScoreboardEvent.sigev_notify 			= SIGEV_THREAD;
sigeventMainScoreboardEvent.sigev_notify_function 		= Main_ScoreBoard_Event;
sigeventMainScoreboardEvent.sigev_notify_attributes 	        = NULL;

// create timer
intRC = timer_create(CLOCK_MONOTONIC, &sigeventMainScoreboardEvent, &timer_tMainScoreboardTimerId);
if (intRC){Abnormal_Exit(MAIN, EX_OSERR, KRN_MESSAGE_180, "timer_tMainScoreboardTimerId", int2strLZ(errno));}

// load settings into stucture and then stucture into the timer
//Set_Timer_Delay(&itimerspecMainScoreboardDelay, 0,intMAIN_SCOREBOARD_INTERVAL_NSEC, 0, intMAIN_SCOREBOARD_INTERVAL_NSEC);
Set_Timer_Delay(&itimerspecMainScoreboardDelay, 0,intMAIN_SCOREBOARD_INTERVAL_NSEC, 0, 0);              // One shot timer
timer_settime( timer_tMainScoreboardTimerId, 0, &itimerspecMainScoreboardDelay, NULL);			// arm timer
*/



// set up  Abandoned Pop up Timed event
sigeventAbandonedPopUpEvent.sigev_notify                          = SIGEV_THREAD;
sigeventAbandonedPopUpEvent.sigev_notify_function                 = Abandoned_Call_Pop_Up_Timed_Event;
sigeventAbandonedPopUpEvent.sigev_notify_attributes               = NULL;
intRC = timer_create(CLOCK_MONOTONIC, &sigeventAbandonedPopUpEvent, &timer_tAbandoned_Pop_Up_Event_TimerId);
if (intRC){Abnormal_Exit(MAIN, EX_OSERR, KRN_MESSAGE_180, "timer_tAbandoned_Pop_Up_Event_TimerId", int2strLZ(errno));}
Set_Timer_Delay(&itimerspecAbandonedPopUpInterval, 1, 0, ABANDONED_CALL_NOTIFICATION_TIME_SEC, 0);
// we do not arm this timer here ...



if(boolBEACON_ACTIVE)
 {
  // setup sigeventServerBeaconBroadcastEvent timer event
  sigeventServerBeaconBroadcastEvent.sigev_notify               = SIGEV_THREAD;		
  sigeventServerBeaconBroadcastEvent.sigev_notify_function      = Server_Beacon_Broadcast_Event;
  sigeventServerBeaconBroadcastEvent.sigev_notify_attributes    = NULL;

  // create timer
  intRC = timer_create(CLOCK_MONOTONIC, &sigeventServerBeaconBroadcastEvent, &timer_tServer_Beacon_Broadcast_Event_TimerId);
  if (intRC){Abnormal_Exit(MAIN, EX_OSERR, KRN_MESSAGE_180, "timer_tServer_Beacon_Broadcast_Event_TimerId", int2strLZ(errno));}

  // load settings into stucture and then stucture into the timer
  Set_Timer_Delay(&itimerspecServer_Beacon_Broadcast_Interval, EXPERIENT_HOME_BEACON_INTERVAL_SEC, 0, EXPERIENT_HOME_BEACON_INTERVAL_SEC, 0);
  timer_settime( timer_tServer_Beacon_Broadcast_Event_TimerId, 0, &itimerspecServer_Beacon_Broadcast_Interval, NULL);			// arm timer
 }

if(boolENABLE_PING_PHONE_MONITOR)
 {
 // setup sigeventServerPhonePingMonitorEvent timer event
  sigeventServerPhonePingMonitorEvent.sigev_notify                  = SIGEV_THREAD;		
  sigeventServerPhonePingMonitorEvent.sigev_notify_function         = Telephone_Monitor_Timer_Event;
  sigeventServerPhonePingMonitorEvent.sigev_notify_attributes       = NULL;

  // create timer
  intRC = timer_create(CLOCK_MONOTONIC, &sigeventServerPhonePingMonitorEvent, &timer_tServer_Phone_Ping_Monitor_Event_TimerId);
  if (intRC){Abnormal_Exit(MAIN, EX_OSERR, KRN_MESSAGE_180, "timer_tServer_Phone_Ping_Monitor_Event_TimerId", int2strLZ(errno));}

  // load settings into stucture and then stucture into the timer
  Set_Timer_Delay(&itimerspecServer_Telephone_Ping_Monitor_Interval, intTELEPHONE_PING_INTERVAL_SEC, 0, intTELEPHONE_PING_INTERVAL_SEC, 0);
  timer_settime( timer_tServer_Phone_Ping_Monitor_Event_TimerId, 0, &itimerspecServer_Telephone_Ping_Monitor_Interval, NULL);			// arm timer

 }
//********************************************BEGIN Main Loop*****************************************


if (signal(SIGTERM, ShutdownSignalCatcher) == SIG_ERR){cout <<"Cannot handle SIGTERM!" << endl;}                                   
if (signal(SIGABRT, AbortSignalCatcher) == SIG_ERR)   {cout <<"Cannot handle SIGABRT!" << endl;}                                   
if (signal(SIGFPE,  FPESignalCatcher) == SIG_ERR)     {cout <<"Cannot handle SIGFPE!" << endl;}                                   
if (signal(SIGILL,  ILLSignalCatcher) == SIG_ERR)     {cout <<"Cannot handle SIGILL!" << endl;}                                   


tcgetattr( STDIN_FILENO, &oldt);



do
{

 experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC);


 // Make Keyboard Input NoEcho
 intMenu = NonBlockingNoEchoInputChar();
 
 
 // tbc this will be a function .......
 if (intMenu == 's'){Show_Server_Status_Table();}
 if (intMenu == 'd'){
                      Display_Menu();
                      boolDebugLoop = true; 
                      do
                       {
                        intMenu = NonBlockingNoEchoInputChar();

                        switch (intMenu)
                         {
                          case -1: boolDebugLoop = true; 
                          case 49:
                               Set_Display_Mode(NORMAL_DISPLAY);cout << "Display Mode Set to Normal\n"   << endl;boolDebugLoop = false;break;
                          case 50:
                               Set_Display_Mode(MINIMAL_DISPLAY);cout << "Display Mode Set to Minimal\n" << endl;boolDebugLoop = false;break;
                          case 51:
                               Set_Display_Mode(DEBUG_ALL);cout << "Display Mode Set to Debug ALL\n"     << endl;boolDebugLoop = false;break;
                          case 52:
                               Set_Display_Mode(DEBUG_KRN);cout << "Display Mode Set to Debug KRN\n"     << endl;boolDebugLoop = false;break;
                          case 53:
                                cout << endl << "Display Registration Packets (y/n)\n";  
                               do
                                {
                                 intMenu = NonBlockingNoEchoInputChar();                                
                                 switch (intMenu)
                                  {
                                   case 121: case 89:
                                        boolSHOW_CLI_REGISTRATION_PACKETS = true;
                                        Set_Display_Mode(DEBUG_ANI);cout << "Display Mode Set to Debug ANI with Registration Packets\n"        << endl;boolDebugLoop = false;break;
                                   default:
                                        boolSHOW_CLI_REGISTRATION_PACKETS = false;                 
                                        Set_Display_Mode(DEBUG_ANI);cout << "Display Mode Set to Debug ANI without Registration Packets\n"     << endl;boolDebugLoop = false;break;
                                  }
                                }while (boolDebugLoop);  
                               break;


                          case 54:
                               Set_Display_Mode(DEBUG_ALI);cout << "Display Mode Set to Debug ALI\n"     << endl;boolDebugLoop = false;break;
                          case 55:
                               Set_Display_Mode(DEBUG_CAD);cout << "Display Mode Set to Debug CAD\n"     << endl;boolDebugLoop = false;break;
                          case 56:
                               Set_Display_Mode(DEBUG_WRK);cout << "Display Mode Set to Debug WRK\n"     << endl;boolDebugLoop = false;break;
                          case 57:
                               Set_Display_Mode(DEBUG_RCC);cout << "Display Mode Set to Debug RCC\n"     << endl;boolDebugLoop = false;break;
                          case 97:
                               Set_Display_Mode(DEBUG_DSB);cout << "Display Mode Set to Debug DSB\n"     << endl;boolDebugLoop = false;break;
                          default:
                               cout << endl << "Invalid Selection ..." << endl;
                               Display_Menu();
                         }

                        }while (boolDebugLoop);

                     }
 else if (intMenu == 'h')
  {
   cout << endl << "Help Menu :\n";
   cout << "a = ANI format Detected\n";
   cout << "c = Show WRK TCP connections\n";
   cout << "d = Display Mode \n";
   cout << "D = Available Disk Space\n";
   cout << "f = Force Call Data to Disconnect\n";
   cout << "i = Show Workstation init xml\n";
   cout << "I = Show init Vars Menu\n";
   cout << "k = Current Calls\n";
   cout << "K = Trunk Last Call List\n";
   cout << "m = Test Email\n";
   cout << "M = Email Messages Supressed\n";
   cout << "p = Phonebook Transfer List\n";
   cout << "r = Reload Workstation\n";
   cout << "R = Reload ini file\n";
   cout << "s = Server Status Table\n"; 
   cout << "S = Reset WRK TCP Socket\n";
   cout << "& = Delete Debug files\n";
   cout << "t = Telephone Status Table \n";
   cout << "T = Thread Table Data\n";    
   cout << "u = Server Up Time \n";
   cout << "U = Force GUI Update Check \n";
   cout << "v = Version\n"; 
   cout << "w = Workstation Status Table \n";
                     
   cout << "x = Exit \n\n"; 
  }
 else if (intMenu == 'T'){Display_Thread_Table_Data();}
 else if (intMenu == 'v'){Display_Controller_Version();}
 else if (intMenu == 'I'){Display_Controller_Init_Data();}
 else if (intMenu == 'i'){
                          cout << INIT_VARS.fInitXMLString(0) << endl;
                          WorkstationInitfile.open ("/datadisk1/ng911/workstation.init",ios::trunc|ios::in);
                          WorkstationInitfile << INIT_VARS.fInitXMLString(0);
                          WorkstationInitfile.close();             
 }
 
 else if (intMenu == 'w'){Display_Controller_Workstation_Data();}
 else if (intMenu == 'u'){Display_Controller_Up_Time(timespecControllerStartTime);}
 else if (intMenu == 'D'){CheckFreeDiskSpace(true);}
 else if (intMenu == 'm'){objTestEmail.fSendTestMail();}
 else if (intMenu == 'M'){DisplayEmailsSupressed();}
 else if (intMenu == 'K'){Trunk_Map.fDisplayLastCallTime();}
 else if (intMenu == 'k'){Display_Current_Calls();}
 else if (intMenu == 'p'){Display_Phonebook_List();}
// else if (intMenu == 'r'){RemoveOldLogFiles();}
 else if (intMenu == 'z'){Display_ANItableZERO();}                          // debug TBC
 else if (intMenu == 'a'){ANIdataFormat.fDisplayANIformatDetected();} 
 else if (intMenu == 't'){TelephoneEquipment.fDisplay();}
 else if (intMenu == 'U'){ForceGUIupdate();}
 else if (intMenu == 'f'){Kernel_Option_Force_Disconnect();}
 else if (intMenu == 'r'){Kernel_Option_Reload_Workstation();}
 else if (intMenu == 'R'){Reload_InitFile();}  
// else if (intMenu == 'c'){SendCancelTandemTransfer();}                         
// else if (intMenu == 'S'){ToggleAMIScript();}
 else if (intMenu == '&'){Delete_Debug_Files();}
 else if (intMenu =='l'){DisplayUnmatchedI3data();}

 // debug for RCC ....
/*
 else if (intMenu == 'o') 
  { 
   objMessage.fMessage_Create(0, 999, objBLANK_CALL_RECORD , objBLANK_KRN_PORT, KRN_MESSAGE_181);
   enQueue_Message(WRK,objMessage);
  }
 else if (intMenu == 'O') 
  { 
   objMessage.fMessage_Create(0, 998, objBLANK_CALL_RECORD , objBLANK_KRN_PORT, KRN_MESSAGE_181);
   enQueue_Message(WRK,objMessage);
  }
*/

// else if (intMenu == 'l'){int itest = 100; cout << vstringWRK_XML_KEYS[itest] << endl;}
// else if (intMenu == 'F'){FillConferenceObject(); cout << "filled 450 conferences" << endl;}

// else if (intMenu == '9'){testTDDmodeOnIndexZero();}


 else if (intMenu == 'n'){cout << "Conferences in use = " << objConferenceList.ConferencesInUse() << endl;} 


 else if ((intMenu == 'x')||(intMenu == 'X')) {ShutdownConfirm();}

 else if (intMenu == 'S'){WRKThreadPort.fResetWRKTCPSocket(1);}
 else if (intMenu == 'c'){WRKThreadPort.TCP_Server_Port.fCheckConnections();}
// else if (intMenu == 'L'){testaddconnection();}
// else if (intMenu == 'b') {SendTestTDDMessage();}
// else if (intMenu == 'q') {SendShutdownSignal();} // test shutdown signal
// else if (intMenu == '1') {SendSegFaultSignal();} // test segfault signal
// else if (intMenu == '2') {SendAbortSignal();}    // test abort signal
// else if (intMenu == '3') {SendFPESignal();}    // test floating point exception signal
// else if (intMenu == '4') {SendILLSignal();}    // test illegal instruction signal
// else if (intMenu == '5') {free(&intRC);}        //test segfault
// else if (intMenu == '0'){cout << 10/0 << endl;} //Test floating point exception signal
// else if (intMenu == '1') {SendHTTPSdereferenceRequestZERO();}
// else if (intMenu == '1'){testWorkstationDoubleAnswer();}
// else if (intMenu == '1'){UpdateVars.fShowThreadsState();}
// else if (intMenu == '2'){INIT_VARS.WRKinitVariable.fShowColorPaletteUsers();}
 else if (intMenu == '3') {
  strTestUUID = "161424458969490554377981566545:esrp.west.com";
  SendHTTPSdereferenceData("/datadisk1/ng911/testdata/pidflo.location.response.xml", strTestUUID,1);
  SendHTTPSdereferenceData("/datadisk1/ng911/testdata/service.info.response.xml", strTestUUID,3);
  SendHTTPSdereferenceData("/datadisk1/ng911/testdata/provider.info.response.xml", strTestUUID,2); 
  SendHTTPSdereferenceData("/datadisk1/ng911/testdata/subscriber.info.response.xml", strTestUUID,4);
  sem_post(&sem_tLISflag);
 }
 else if (intMenu == '0') {LoadADR();}

} while (!boolSTOP_EXECUTION);

//********************************************END Main Loop*****************************************


// Don't put any output from here on !!!!!!!!!!
if (!CaughtShutdownSignal) {Cleanup();}


 return 0;
}

void Cleanup()
{
 MessageClass            objMessage;
 struct timespec         timespecRemainingTime;


objMessage.fMessage_Create(0, 181, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_181);
objMessage.fConsole();


// order shut down
boolSTOP_EXECUTION = true;
pthread_join( pthread_tMainScoreboardThread, NULL);
pthread_join( pthread_tMainMessagingThread, NULL);

sem_post(&sem_tAMIFlag);                                  // signal boolSTOP_EXECUTION to AMI thread
sem_post(&sem_tANIFlag);                                  // signal boolSTOP_EXECUTION to ANI thread
if(boolCAD_EXISTS)             {sem_post(&sem_tCadFlag);} // signal boolSTOP_EXECUTION to CAD thread 
if(boolCONTACT_RELAY_INSTALLED){sem_post(&sem_tRCCflag);} // signal boolSTOP_EXECUTION to RCC thread
if(boolUSE_POLYCOM_CTI)        {sem_post(&sem_tCTIflag);} // signal boolSTOP_EXECUTION to CTI thread
if(boolNG_ALI)                 {sem_post(&sem_tLISflag);} // signal boolSTOP_EXECUTION to LIS thread
if(boolLEGACY_ALI)             {sem_post(&sem_tAliFlag);} // signal boolSTOP_EXECUTION to ALI thread
sem_post(&sem_tWRKFlag);                                  // signal boolSTOP_EXECUTION to WRK thread
sem_post(&sem_tDSBflag);                                  // signal boolSTOP_EXECUTION to DSB thread
sem_post(&sem_tLogConsoleEmailFlag);			  // signal boolSTOP_EXECUTION to LOG thread

//cout << "B" << endl;
pthread_join(pthread_tNFYThread, NULL);
//cout << "C" << endl;
//cout << "D" << endl;
//pthread_join( pthread_tMainScoreboardThread, NULL);
//cout << "E" << endl;
//objMessage.fMessage_Create(0, 182, objBLANK_CALL_RECORD , objBLANK_KRN_PORT, KRN_MESSAGE_182, int2strLZ(intTHREAD_SHUTDOWN_DELAY_SEC+1));
//objMessage.fConsole();
//timer_delete(timer_tMainScoreboardTimerId );
//timer_delete(timer_tHealthMonitorTimerId );
//pthread_join( pthread_tMainMessagingThread, NULL);


if(boolBEACON_ACTIVE)             {timer_delete(timer_tServer_Beacon_Broadcast_Event_TimerId );}
//cout << "F" << endl;
if(boolENABLE_PING_PHONE_MONITOR) {timer_delete(timer_tServer_Phone_Ping_Monitor_Event_TimerId );}
//cout << "G" << endl;
timer_delete(timer_tAbandoned_Pop_Up_Event_TimerId);
//cout << "H" << endl;
/*
if(boolBEACON_ACTIVE)
 {
  string sNamePlusShutDown = stringSERVER_HOSTNAME + EXPERIENT_SERVER_BEACON_SHUTDOWN_SUFFIX;
  intRC = BeaconPort.UDP_Port.Send((char*)sNamePlusShutDown.c_str(),sNamePlusShutDown.length());
  if (intRC){BeaconPort.UDP_Port.Error_Handler();}
  if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC))
   {
    objMessage.fMessage_Create(0, 163, objBLANK_CALL_RECORD , objBLANK_KRN_PORT, LOG_MESSAGE_531, BEACON_IP.stringAddress + 
                               ":"+int2str(intBEACON_PORT_NUMBER), ASCII_String((char*)sNamePlusShutDown.c_str(), sNamePlusShutDown.length()),"","","","", NEXT_LINE);
    objMessage.fConsole();
   } 
 }
*/

while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
//pthread_join( pthread_tMainScoreboardThread, NULL);

experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC);
objMessage.fMessage_Create(0, 199, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objBLANK_KRN_PORT,
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_199);
objMessage.fConsole();

Delete_PID_File();
objEmailMsg.Disconnect();
objEmailMsg.DoEvents();

experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);


if (boolDEBUG){Console_Message( "170 |", "-MainInputQ      size = "+ int2strLZ(queue_objMainData.size()));}
if (boolDEBUG){Console_Message( "171 |", "-MainMessageQ    size = "+ int2strLZ(queue_objMainMessageQ.size()));}
if (boolDEBUG){Console_Message( "172 |", "-CadMessageQ     size = "+ int2strLZ(queue_objCadMessageQ.size()));}
if (boolDEBUG){Console_Message( "173 |", "-LogMessageQ     size = "+ int2strLZ(queue_objLogMessageQ.size()));}
if (boolDEBUG){Console_Message( "174 |", "-AliMessageQ     size = "+ int2strLZ(queue_objALiMessageQ.size()));}
if (boolDEBUG){Console_Message( "175 |", "-ANIMessageQ     size = "+ int2strLZ(queue_objANIMessageQ.size()));}
if (boolDEBUG){Console_Message( "176 |", "-WRKMessageQ     size = "+ int2strLZ(queue_objWRKMessageQ.size()));}
if (boolDEBUG){Console_Message( "177 |", "-OutputMessageQ  size = "+ int2strLZ(queue_objOutputMessageQ.size()));}
if (boolDEBUG){Console_Message( "178 |", "-CAD ACTIVE TRUNK     = "+ int2strLZ(intCadActivePosition));}
if (boolDEBUG){Console_Message( "179 |", "-SEMAPHORE_ERRORS     = "+ int2strLZ(intSEMAPHORE_ERRORS));}


// cleanup operations

sem_destroy(&sem_tCadFlag);
sem_destroy(&sem_tAliFlag);
sem_destroy(&sem_tLogConsoleEmailFlag);
sem_destroy(&sem_tANIFlag);
sem_destroy(&sem_tWRKFlag);
//sem_destroy(&sem_tServerHeartbeatRecievedFlag);
//sem_destroy(&sem_tGatewayTimedResponseFlag);
//sem_destroy(&sem_tGatewayReplyFlag);
sem_destroy(&sem_tConfAssignmentFlag);
sem_destroy(&sem_tAMIFlag);
sem_destroy(&sem_tRCCflag);
sem_destroy(&sem_tCTIflag);
sem_destroy(&sem_tLISflag);
sem_destroy(&sem_tDSBflag);
sem_destroy(&sem_tMutexCadInputQ);
sem_destroy(&sem_tMutexAliInputQ);
sem_destroy(&sem_tMutexMainInputQ);
sem_destroy(&sem_tMutexWRKInputQ);
sem_destroy(&sem_tMutexANIInputQ);
sem_destroy(&sem_tMutexAMIInputQ);
sem_destroy(&sem_tMutexRCCInputQ);
sem_destroy(&sem_tMutexCTIInputQ);
sem_destroy(&sem_tMutexLISInputQ);
sem_destroy(&sem_tMutexUnmatchedALIData);
sem_destroy(&sem_tMutexCadWorkTable);
sem_destroy(&sem_tMutexAliWorkTable);
sem_destroy(&sem_tMutexWRKWorkTable);
sem_destroy(&sem_tMutexRCCtable);
sem_destroy(&sem_tMutexMainWorkTable);
sem_destroy(&sem_tMutexANIWorkTable);
sem_destroy(&sem_tMutexLISWorkTable);
sem_destroy(&sem_tMutex_ServerStatusTable);
sem_destroy(&sem_tMutexAliDisconnectList);
sem_destroy(&sem_tMutexConferenceNumberAssignmentObject);
sem_destroy(&sem_tMutexE2TransactionID);
sem_destroy(&sem_tMutexANIPort);
sem_destroy(&sem_tMutexCall_LIST);
sem_destroy(&sem_tMutexLogMessageQ);
sem_destroy(&sem_tMutexCadMessageQ);
sem_destroy(&sem_tMutexAliMessageQ);
sem_destroy(&sem_tMutexOutputMessageQ);
sem_destroy(&sem_tMutexANIMessageQ);
sem_destroy(&sem_tMutexWRKMessageQ);
sem_destroy(&sem_tMutexRCCMessageQ);
sem_destroy(&sem_tMutexMainMessageQ);
sem_destroy(&sem_tMutexCLIMessageQ);
sem_destroy(&sem_tMutexCTIMessageQ);
sem_destroy(&sem_tMutexLISMessageQ);
sem_destroy(&sem_tMutexDSBMessageQ);
sem_destroy(&sem_tMutexDebugLogName);
//sem_destroy(&sem_tMutex_GatewayReplyQ);
sem_destroy(&sem_tMutexCadPortDataQ);
sem_destroy(&sem_tMutexAliPortDataQ);
sem_destroy(&sem_tMutexANIPortDataQ);
sem_destroy(&sem_tMutexWRKPortDataQ);
sem_destroy(&sem_tMutexLISPortDataQ);
sem_destroy(&sem_tMutexDSBPortDataQ);
sem_destroy(&sem_tMutexWorkstationHistory);
sem_destroy(&sem_tMutexThreadData);
sem_destroy(&sem_tMutexConfAssignmentQ);



pthread_attr_destroy(&pthread_attr_tAttr);

//pthread_detach(pthread_tLogConsoleEmailThread);




sem_destroy(&sem_tMutexEmailSupress);
//cout << "fi" << endl;
//experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);

return;
}




//void Main_ScoreBoard_Event(union sigval union_sigvalArg)
void *Main_ScoreBoard_Event(void *voidarg)
{
 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz;
 ExperientDataClass                             objData, objDataCopy;
 int                                            intRC;
 int						index;
 int                                            intTrunk;
 int                                            iTableIndex;
 int                                            intNewPosition;
 int                                            intOldPosition;
 int						intParticipants;
 int						intPositions;
 int						intLineNumber;
 MessageClass                                   objMessage;
 string                                         stringMessage;
 string                                         strData;
 WorkStation                                    NotifyWorkstation;
 bool                                           bCallbackVerified;
 string                                         strTemp;
 string                                         strTempA;
 Call_Data                                      objCallData;
 bool                                           boolSendtoALI;
 bool                                           bCallbackWasZero;
 unsigned long long int                         iPreviousCallBack;
 bool                                           bPreviousALI;
 bool       					boolBidPidflo = false;
 bool						boolA, boolB, boolC, boolD;
 bool						boolTransferisToaPosition;
 bool                                           boolCallConnectedandNotRingback = false;
// if(!boolIsMaster) {return;}
/*								
 // disable timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tMainScoreboardTimerId, 0, &itimerspecDisableTimer, NULL);}
*/
do
{
 experient_nanosleep(intMAIN_SCOREBOARD_INTERVAL_NSEC);

  if (boolUPDATE_VARS) {continue;}


 while (!ThreadsafeEmptyQueueCheck(&queue_objMainData, &sem_tMutexMainInputQ))
  {
   // Pop Data off Inbound Q
   intRC = sem_wait(&sem_tMutexMainInputQ);
   if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainInputQ, 
              "sem_wait@sem_tMutexMainInputQ in function Main_ScoreBoard_Event()", 1);}
   objData.fClear_Record();
   objData = queue_objMainData.front();
   queue_objMainData.pop();
   intRC = sem_post(&sem_tMutexMainInputQ);
   objData.enumANISystem = enumANI_SYSTEM;

   // Lock Work Table
   intRC = sem_wait(&sem_tMutexMainWorkTable);
   if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in Main_ScoreBoard_Event", 1);}
 
   iTableIndex = IndexWithCallUniqueIDMainTable(objData.CallData.stringUniqueCallID);
   intTrunk    = objData.CallData.intTrunk;

 //  cout << "THREAD CALLING -> " << Thread_Calling(objData.enumThreadSending) << endl;
 //  cout << "ANI FUNCTION   -> " << ANIfunction(objData.enumANIFunction) << endl;
   switch (objData.enumThreadSending)
    {
     case MAIN:
           if (iTableIndex < 0) {break;}
           switch(objData.intLogMessageNumber)
            {
             case 255:
                  // INITIAL ALI BID FAIL REBID NEW DATABASE
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  vMainWorkTable[iTableIndex].ALIData.intNumberOfRetryBids++;
                  vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_BID_RETRY ;
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT, bPreviousALI);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  break;
             case 256:
                  // ALI REBID
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  vMainWorkTable[iTableIndex].ALIData.intNumberOfRetryBids++;
                  vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_BID_RETRY ;
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT, bPreviousALI);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  break;                 
             default:
                  SendCodingError( "ExperientController.cpp - Coding Error in Main_ScoreBoard_Event() in switch(objData.intLogMessageNumber)");
            }// end switch(objData.intLogMessageNumber) 

          break;
     case ANI:
          // cout << "MAIN ANI" << endl;
           switch (objData.enumANIFunction)
            {
             case RINGING_POSITION_TO_POSITION:
                  if (iTableIndex < 0) {break; } 
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_RINGING,"SOFTWARE LOAD");
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_RINGING);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = true;
                  vMainWorkTable[iTableIndex].intWorkStation = objData.FreeswitchData.objChannelData.iRingingPosition;
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                 break;

             case RING_BACK:
                 if (iTableIndex < 0) {break; } 
                 //cout << "RING_BACK IN MAIN" << endl;
                 vMainWorkTable[iTableIndex].boolRingBack = true;
                 vMainWorkTable[iTableIndex].CallData = objData.CallData;
                 vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;
               //  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_RINGING,"SOFTWARE LOAD");
                 vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ANI_RINGBACK);
                 vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                 vMainWorkTable[iTableIndex].boolSMSmessage = false;
                 vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                 Queue_WRK_Event(vMainWorkTable[iTableIndex]);


                 break;
             case RINGING: case MSRP_RINGING:
                  if (iTableIndex >= 0) {vMainWorkTable[iTableIndex] = objData; }
                  else                  {iTableIndex = IndexOfNewMainTableEntry(objData);}

                  vMainWorkTable[iTableIndex].fSetNENA_ID_Vars();
                  if (vMainWorkTable[iTableIndex].ALIData.I3Data.objGeoLocationHeaderURI.boolDataRecieved) {
                   vMainWorkTable[iTableIndex].objGeoLocation = objData.ALIData.I3Data.objGeoLocationHeaderURI;
                   vMainWorkTable[iTableIndex].objGeoLocation.boolDataRecieved = true;
                  }
                  vMainWorkTable[iTableIndex].boolRingBack = false;
                  clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].timespecTimeRecordReceivedStamp);
                  objData.stringRingingTimeStamp = vMainWorkTable[iTableIndex].stringRingingTimeStamp = objData.stringTimeOfEvent;
                  objData.boolRinging            = vMainWorkTable[iTableIndex].boolRinging            = true;             
        //          objData.boolALIRebidButton     = vMainWorkTable[iTableIndex].boolALIRebidButton     = false;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_RINGING,"SOFTWARE LOAD");
                  
                  objData.intCallStateCode = vMainWorkTable[iTableIndex].intCallStateCode;


                  switch (TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType) {
                    case CLID: 
                         vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = true;
                         boolSendtoALI =  boolALLOW_CLID_ALI_BIDS ;
                         break;
                    case SIP_TRUNK:
                         if (!ValidTenDigitNumber(objData.CallData.stringTenDigitPhoneNumber)) 
                          {
                           vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = true;
                           boolSendtoALI = false;
                          }
                         else {boolSendtoALI =  boolALLOW_CLID_ALI_BIDS ;}
                         break;

                    case NG911_SIP: case MSRP_TRUNK: case REFER: case CAMA:
                          if (boolLEGACY_ALI) {boolSendtoALI = true; break;}
                          boolSendtoALI = false;

                          //turn on rebid button if we have dereference URI.....

                          cout << "URI in MAIN -> objloc " << vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI.strURI << endl;
                          cout << "URI in MAIN -> geoloc " << vMainWorkTable[iTableIndex].objGeoLocation.strURI << endl;
                          cout << "NENACallID  ->        " << vMainWorkTable[iTableIndex].strNenaCallId << endl;
                          if ((vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI.boolDataRecieved)||
                              (vMainWorkTable[iTableIndex].objGeoLocation.boolDataRecieved)) {
                           
                           vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = URL_RECIEVED;

                           if (TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType == MSRP_TRUNK) {                          
                            vMainWorkTable[iTableIndex].ALIData.I3Data.fLoadBidID(vMainWorkTable[iTableIndex].strNenaCallId);                              
                           }
                           else {
                            vMainWorkTable[iTableIndex].ALIData.I3Data.fLoadBidID(vMainWorkTable[iTableIndex].strNenaCallId);                           
                           }
                           cout << "experientController.cpp fLoadBidID() with pani after " << vMainWorkTable[iTableIndex].ALIData.I3Data.strBidId << endl;

                           enqueue_Main_Input(LIS, vMainWorkTable[iTableIndex]); 
 /*            
                           vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_REBID; 
                           bPreviousALI = ((!vMainWorkTable[iTableIndex].ALIData.I3Data.strALI30WRecord.empty())||
                                           (!vMainWorkTable[iTableIndex].ALIData.I3Data.strNGALIRecord.empty()));
                           vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();
*/
                          }
                          else {
                           vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_NO_GEOLOCATION_URI;   /// XML_STATUS_CODE_ALI_BID_NOT_ALLOWED;
                          }
                          break;
                    
                    default:
                        boolSendtoALI = true;
                  }
  
                if((boolLEGACY_ALI)&&(boolSendtoALI)) {
                 // cout << "do we send to Legacy ALI Thread ? " << boolSendtoALI << endl;
                    //turn on ALI rebid button ...
                    //cout << "set Legacy ALI rebid" << endl;
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_REBID;
                    objData.ALIData.enumALIBidType = vMainWorkTable[iTableIndex].ALIData.enumALIBidType = INITIAL;
                    SendANItoALI(objData);
                }
                else if ((TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType != NG911_SIP )&&
                        (TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType != MSRP_TRUNK )&&
                        (TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType != REFER )) {
                   //turn off ALI rebid button ...
                   //cout << "turn off ALI rebid" << endl;
                   // vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_BID_NOT_ALLOWED; get rid of this ....... becouse of cama has ng911
                 
                }
                  //cout << "update PSAP status" << endl;
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();               
                  // workstation code ..
                  if(!vMainWorkTable[iTableIndex].boolRecordRebuiltFlag)
                   {
                    
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_RINGING);
                    vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                    //If No Workstations online and MSRP then send Nobody home message and hangup with error code .... fred
                   // cout << "send to workstation" << endl;
                    
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                   }
                
                if(boolNG_ALI){ CheckforNGALIinUnmatchedTable(iTableIndex, intTrunk);}


               
                  break;
            
             case CONNECT: case TRANSFER_CONNECT: case TRANSFER_CONNECT_BLIND: case TRANSFER_CONNECT_INTERNAL_NG911: case PANI_CONNECT: case NON_TEN_DIGIT_NUMBER_CONNECT: 
             case RING_BACK_CONNECT: 
                  if (iTableIndex < 0) {break;}
                  //cout << "MAIN TRANSFER CONNECT" << endl;
                  vMainWorkTable[iTableIndex].boolRingBack = false;
                  intOldPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objData.FreeswitchData.objLinkData.IPaddressTwo.stringAddress);
                  intNewPosition = TelephoneEquipment.fPostionNumberFromIPaddress(objData.FreeswitchData.objLinkData.IPaddressOne.stringAddress);
                  boolTransferisToaPosition = (intNewPosition > 0);
                  if ((boolTransferisToaPosition)||(objData.enumANIFunction != TRANSFER_CONNECT_BLIND)) { 
                     Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, DUAL);
                     objDataCopy = objData;
                     objDataCopy.intWorkStation = intNewPosition;
                     if (objDataCopy.fUpdateMuteButton(true)) {
                      Queue_WRK_Event(objDataCopy);
                     }  
                  }
                  //cout << "Main Connect " << objData.CallData.intPosn << endl;
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified;
                  strTempA = vMainWorkTable[iTableIndex].CallData.stringTenDigitPhoneNumber;
                  strTemp  = vMainWorkTable[iTableIndex].CallData.stringPANI;
                  vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                  if (bCallbackVerified)
                   {
                    vMainWorkTable[iTableIndex].CallData.fLoadCallbackNumber(strTempA);
                    vMainWorkTable[iTableIndex].CallData.stringPANI                = strTemp;
                   }
           
                  vMainWorkTable[iTableIndex].fUpdateTrunkTypeStringwithTDD();
                  vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                  vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;
                  vMainWorkTable[iTableIndex].boolConnect  = true;
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  if(boolNG_ALI){ CheckforNGALIinUnmatchedTable(iTableIndex, intTrunk);}
                  if(boolTDD_AUTO_DETECT) {vMainWorkTable[iTableIndex].fTurnOnTDDmode();}
                  // send to CAD ....
                  if(vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived) { 
                   
                    vMainWorkTable[iTableIndex].boolConnectOutofOrder = false;

                    if      ((objData.enumANIFunction == CONNECT)&&(objData.CallData.intPosn > 0)) {
                            // Only 1 there anyhow ...... and will ba a position
                             SendALItoCAD_Conference_List(iTableIndex);
                    } 
                    else if (objData.enumANIFunction == TRANSFER_CONNECT_INTERNAL_NG911) {

                            if (boolSEND_TO_CAD_ON_CONFERENCE) {

                             SendALItoCAD_Conference_List(iTableIndex);
                             
                            }
                            else {
                             SendALItoCAD_SinglePosition(vMainWorkTable[iTableIndex]);
                             // cout << "send to CAD position ->" << vMainWorkTable[iTableIndex].CallData.intPosn << endl;
                            }
                    }
                    else if (((!objData.boolDoNotSendToCAD  )&&(vMainWorkTable[iTableIndex].CallData.intPosn > 0))&&(boolSEND_TO_CAD_ON_CONFERENCE)) {
                             SendALItoCAD_Conference_List(iTableIndex);
                    }

                  }
                  if (vMainWorkTable[iTableIndex].stringConnectTimeStamp.empty()) {
                   vMainWorkTable[iTableIndex].stringConnectTimeStamp = objData.stringTimeOfEvent;
                  }
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].WindowButtonData.boolTransferWindow = true;
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(!vMainWorkTable[iTableIndex].FreeswitchData.vectDestChannelsOnCall.empty());
                  // workstation code ..
                  if (objData.enumANIFunction == CONNECT) { vMainWorkTable[iTableIndex].fResetTransferWindow(false,true);}
                  if (objData.enumANIFunction == TRANSFER_CONNECT) {
                    if (boolTransferisToaPosition){
                     vMainWorkTable[iTableIndex].CallData.fLoadPosn(intOldPosition);
                    }
                    vMainWorkTable[iTableIndex].fResetTransferWindow(true,true);
                  }
                  if (objData.enumANIFunction == TRANSFER_CONNECT_BLIND) {vMainWorkTable[iTableIndex].fResetTransferWindow(false,true);}
                  // update mute button
                  objData.intWorkStation = vMainWorkTable[iTableIndex].CallData.intPosn;
                  if (objData.fUpdateMuteButton(true)) {
                   Queue_WRK_Event(objData);
                  }  
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}                      
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                  
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable[iTableIndex].CallData.fLoadPosn(objData.CallData.intPosn);
     
                  //ALI to workstationList
                  vMainWorkTable[iTableIndex].fUpdatePhoneALI();
                  break;

             case DISCONNECT:
                  if (iTableIndex < 0) {break;}

                  if (!PositionIsOnAnotherCall(objData.CallData.intPosn, iTableIndex)) {
                   Queue_RCC_Event(RADIO, objData, objData.CallData.intPosn, SINGLE);
                   objDataCopy = objData;
                   objDataCopy.intWorkStation = objData.CallData.intPosn;
                   if (objDataCopy.fUpdateMuteButton(false)) {
                    Queue_WRK_Event(objDataCopy);
                   }  
                  }
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  SendCAD_Erase_Msg_To_Conference_List(iTableIndex);
                  vMainWorkTable[iTableIndex].CallData = objData.CallData; // 2/7/2020 added
                  vMainWorkTable[iTableIndex].fClear_Active_Flags();
                  vMainWorkTable[iTableIndex].fLoad_ConferenceData(SOFTWARE, MAIN, "0", 0);    
                  vMainWorkTable[iTableIndex].stringDisConnectTimeStamp         = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition 	= false;
                  vMainWorkTable[iTableIndex].enumANIFunction 			= DISCONNECT;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_DISCONNECT,"SOFTWARE LOAD");                  
                  // send disconnect to ALI
                  //SendANItoALI(vMainWorkTable[iTableIndex]); // no longer needed .... timed erase

                  // workstation code ..
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_DISCONNECT, bPreviousALI);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable[iTableIndex].fResetTransferWindow(false,false);
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}
                  vMainWorkTable[iTableIndex].fLogTextConversation();
                  if (vMainWorkTable[iTableIndex].boolConnect) {
                   vMainWorkTable[iTableIndex].boolAbandoned = false;
                  }
   
                  // vMainWorkTable.erase(vMainWorkTable.begin()+iTableIndex); No longer erase 

                                  
                  break;

             case ATTENDED_TRANSFER:
                //  cout << "attended transfer" << endl;             
                  if (iTableIndex < 0) {break;}
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);
                  Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, SINGLE);
                      
                    //send connect message to update position.in workstation ... may change to transfer later ..
                  if (objData.CallData.intPosn)
                   {
                    vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, objData.CallData.stringPosn,0 );
                    objDataCopy = objData;
                    objDataCopy.intWorkStation = objData.CallData.intPosn;
                    if (objDataCopy.fUpdateMuteButton(false)) {
                     Queue_WRK_Event(objDataCopy);
                    }      
                   }
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                  if(boolTDD_AUTO_DETECT) {vMainWorkTable[iTableIndex].fTurnOnTDDmode();}
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]); 
                  break;

             case TRANSFER:
                 // cout << "main transfer ...." << endl;             
                  if (iTableIndex < 0) {break;}
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                    //send connect message to update position.in workstation ... may change to transfer later ..
                  if (objData.CallData.intPosn)
                   {
                    vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, objData.CallData.stringPosn,0 );
                   }
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                  if(boolTDD_AUTO_DETECT) {vMainWorkTable[iTableIndex].fTurnOnTDDmode();}
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]); 
                  break;
             
             case CONFERENCE_JOIN:  
                  if (iTableIndex < 0) {break;}
                  Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, SINGLE);

                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);
                  vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;                      
                  // send conference message to workstation conferencing in 
                  intOldPosition = vMainWorkTable[iTableIndex].CallData.intPosn;
                  intNewPosition = objData.CallData.intPosn;
                  objDataCopy = objData;
                  objDataCopy.intWorkStation = objData.CallData.intPosn;
                  if (objDataCopy.fUpdateMuteButton(true)) {
                   Queue_WRK_Event(objDataCopy);
                  }      
                  vMainWorkTable[iTableIndex].CallData.ConfData = objData.CallData.ConfData;
                  vMainWorkTable[iTableIndex].CallData.CTIsharedLine =  objData.CallData.CTIsharedLine;
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].WindowButtonData.boolTransferWindow = true;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONFERENCEJOIN, bPreviousALI);

                  vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, int2strLZ(intOldPosition), 0, int2strLZ(intOldPosition));
                  if(boolTDD_AUTO_DETECT) {vMainWorkTable[iTableIndex].fTurnOnTDDmode();}
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}                      
                  if ((boolSEND_TO_CAD_ON_CONFERENCE)&&(vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived))
                   {
                    objData = vMainWorkTable[iTableIndex];
                    objData.CallData.intPosn = intNewPosition;
                    SendALItoCAD_SinglePosition(objData);
                   }
                  break;
             case MSRP_CONFERENCE_JOIN:
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;                      
                  // send conference message to workstation conferencing in 
                  intOldPosition = vMainWorkTable[iTableIndex].CallData.intPosn;
                  intNewPosition = objData.CallData.intPosn;
                  vMainWorkTable[iTableIndex].CallData.ConfData = objData.CallData.ConfData;
                  vMainWorkTable[iTableIndex].CallData.CTIsharedLine =  objData.CallData.CTIsharedLine;
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].WindowButtonData.boolTransferWindow = true;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONFERENCEJOIN, bPreviousALI);

                  vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, int2strLZ(intOldPosition), 0, int2strLZ(intOldPosition));
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}                      
                  if ((boolSEND_TO_CAD_ON_CONFERENCE)&&(vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived))
                   {
                    objData = vMainWorkTable[iTableIndex];
                    objData.CallData.intPosn = intNewPosition;
                    SendALItoCAD_SinglePosition(objData);
                   }
                  break;
 
             case VALET_PARK: case VALET_JOIN:
                  if (iTableIndex < 0) {break; }
                  vMainWorkTable[iTableIndex].CallData          = objData.CallData;
                  vMainWorkTable[iTableIndex].FreeswitchData    = objData.FreeswitchData; 
                  vMainWorkTable[iTableIndex].CallData.ConfData = objData.CallData.ConfData;
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_OFF_HOLD_CONNECTED, bPreviousALI);

                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();


                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  break;
                                        
             case HOLD_ON:
                  if (iTableIndex < 0) {break;}
                 //  if(!vMainWorkTable[iTableIndex].CallData.fHasAttendedTransfer(objData.CallData.intPosn))
                     Queue_RCC_Event(RADIO, objData, objData.CallData.intPosn, SINGLE);
                 //  else
                  //   {Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, SINGLE);}
                  // send Hold on message to all workstations if no conference or all on hold 
                  intOldPosition = vMainWorkTable[iTableIndex].CallData.intPosn;
                  vMainWorkTable[iTableIndex].CallData    = objData.CallData;
                  vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, int2strLZ(intOldPosition), 0, int2strLZ(intOldPosition));
                  SendCAD_Erase_Msg(objData.CallData.stringPosn, iTableIndex);
                  //Update Mute
                  objDataCopy = objData;
                  objDataCopy.intWorkStation = objData.CallData.intPosn;
                  if (objDataCopy.fUpdateMuteButton(true)) {
                   Queue_WRK_Event(objDataCopy);
                  }   
                  // check if on hold status changes ...
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                 // if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);} 
                  vMainWorkTable[iTableIndex].fCheckOnHoldMessage(objData.stringTimeOfEvent);                     
                  break;

             case HOLD_OFF:
                  if (iTableIndex < 0) {break;}
                  Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, SINGLE);
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  intOldPosition = vMainWorkTable[iTableIndex].CallData.intPosn;
                  intNewPosition = objData.CallData.intPosn;
                  vMainWorkTable[iTableIndex].CallData.ConfData     = objData.CallData.ConfData;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, int2strLZ(intOldPosition), 0, int2strLZ(intOldPosition));
                  vMainWorkTable[iTableIndex].stringTimeOfEvent  = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].WindowButtonData.boolTransferWindow = true;
                  vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(!vMainWorkTable[iTableIndex].FreeswitchData.vectDestChannelsOnCall.empty());

                  vMainWorkTable[iTableIndex].fCheckOnHoldMessage(objData.stringTimeOfEvent);
                  objData = vMainWorkTable[iTableIndex];
                  objData.CallData.intPosn = intNewPosition;
                  objData.fCallInfo_XML_Message(XML_STATUS_CODE_OFF_HOLD_CONNECTED, bPreviousALI);
                  if(boolCAD_ERASE){SendALItoCAD_SinglePosition(objData);}                   
                  Queue_WRK_Event(objData);
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}                      
                  break;

             case ABANDONED: case ABANDONED_BUSY:
                  if (iTableIndex < 0) {break;}
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  vMainWorkTable[iTableIndex].fSetAbandoned();
                  if (vMainWorkTable[iTableIndex].CallData.fIsOutboundCall()) 
                   {
                    switch (objData.enumANIFunction)
                     {
                      case ABANDONED:
                           vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CANCELED,"SOFTWARE LOAD");
                           Queue_RCC_Event(RADIO, objData, objData.CallData.intPosn, SINGLE);
                           objDataCopy = objData;
                           objDataCopy.intWorkStation = objData.CallData.intPosn;
                           if (objDataCopy.fUpdateMuteButton(false)) {
                            Queue_WRK_Event(objDataCopy);
                           }      
                           break;
                      case ABANDONED_BUSY:
                           vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CANCELED_BUSY,"SOFTWARE LOAD");
                           Queue_RCC_Event(RADIO, objData, objData.CallData.intPosn, WITH_DELAY );
                           objDataCopy = objData;
                           objDataCopy.intWorkStation = objData.CallData.intPosn;
                           if (objDataCopy.fUpdateMuteButton(false)) {
                            Queue_WRK_Event(objDataCopy);
                           }      
                           break;
                      default: break;
                     }
                   }
                  else                                                        
                   {
                    vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_ABANDONED,"SOFTWARE LOAD");
                   }
                  bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified;
                  strTempA = vMainWorkTable[iTableIndex].CallData.stringTenDigitPhoneNumber;
                  strTemp  = vMainWorkTable[iTableIndex].CallData.stringPANI;
                  vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                  if (bCallbackVerified)
                   {
                    vMainWorkTable[iTableIndex].CallData.fLoadCallbackNumber(strTempA);
                    vMainWorkTable[iTableIndex].CallData.stringPANI                = strTemp;
                   }
                  vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                  vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForAbandoned();
                  vMainWorkTable[iTableIndex].stringConnectTimeStamp = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].enumANIFunction = ABANDONED;
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();


                  // workstation code ..
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ABANDONED, bPreviousALI);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable[iTableIndex].fResetTransferWindow(false,false);
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                 

                   // send abandoned to ALI
                  SendANItoALI(vMainWorkTable[iTableIndex]);

                  // remove from Worktable if Outbound or Position to Position Call
                  if ((vMainWorkTable[iTableIndex].CallData.fIsOutboundCall())||(vMainWorkTable[iTableIndex].CallData.ConfData.fPositionInHistory())) {
                    vMainWorkTable.erase(vMainWorkTable.begin()+iTableIndex);
                  }
                  break;

             case ALARM:
                  // workstation code ... Handled in Health Monitor Timer ..
                  break;
                         
             case SILENT_ENTRY_LOG_OFF:
                  if (iTableIndex < 0) {break;}
                  //check if position is on another call due to race condition .........
                  if (!PositionIsOnAnotherCall(objData.CallData.intPosn, iTableIndex)) {
                   Queue_RCC_Event(RADIO, objData, objData.CallData.intPosn, SINGLE);
                   objDataCopy = objData;
                   objDataCopy.intWorkStation = objData.CallData.intPosn;
                   if (objDataCopy.fUpdateMuteButton(false)) {
                    Queue_WRK_Event(objDataCopy);
                   }                     
                  }
                  
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);  
                  intNewPosition = objData.CallData.intPosn;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  vMainWorkTable[iTableIndex].CallData.ConfData               = objData.CallData.ConfData;
                  vMainWorkTable[iTableIndex].stringTimeOfEvent               = objData.stringTimeOfEvent;                 
                  
                  vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;
                  objData = vMainWorkTable[iTableIndex];
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(!vMainWorkTable[iTableIndex].FreeswitchData.vectDestChannelsOnCall.empty());
                  objData.fCallInfo_XML_Message(XML_STATUS_CODE_CONFERENCELEAVE, bPreviousALI);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(objData);
                  if(intNewPosition)
                   {
                    objData.fLoad_Position(SOFTWARE, MAIN, int2strLZ(intNewPosition), 0 );
                    SendCAD_Erase_Msg(objData.CallData.stringPosn, iTableIndex);
                   }         
                  
                  // check if on hold status changes ...
                  vMainWorkTable[iTableIndex].fCheckOnHoldMessage(objData.stringTimeOfEvent);
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}                      
                  break;

             case TDD_CHAR_RECEIVED:
                  if (iTableIndex < 0) {break;} 
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);   
                  //Freeswitch TDD character recieved 
                  vMainWorkTable[iTableIndex].TextData.TextType = TDD_MESSAGE;
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fLoadTDDReceived(objData.TextData.TDDdata.strTDDcharacter);
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(true);
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fSetTDDSendButton();
                  vMainWorkTable[iTableIndex].TextData.fSetWindow(true);
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_TDD_CHAR_RECEIVED, bPreviousALI);
                  vMainWorkTable[iTableIndex].fUpdateTrunkTypeStringwithTDD();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]); 
                  break;

             case TDD_CALLER_SAYS:
                  // TDD string received
                  if (iTableIndex < 0) {break;}
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived); 
                  vMainWorkTable[iTableIndex].TextData.TextType = TDD_MESSAGE;  
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fLoadTDDReceived(objData.TextData.TDDdata.strTDDstring,true);
                  vMainWorkTable[iTableIndex].TextData.TDDdata.CreateCallerSays();
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(true);
                  vMainWorkTable[iTableIndex].TextData.fSetWindow(true);
                  vMainWorkTable[iTableIndex].enumThreadSending = MAIN; 
                  vMainWorkTable[iTableIndex].fAddSentenceToTextConversation(vMainWorkTable[iTableIndex].TextData.TDDdata.strTDDstring);
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_TDD_CALLER_SAYS, bPreviousALI);
                  vMainWorkTable[iTableIndex].fUpdateTrunkTypeStringwithTDD();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  break;

             case SMS_MSG_SENT:
                   cout << "SMS_MSG_SENT .. needed ? this has been deactivated" << endl;
/*
                  // May not need this code .......
                  // This is a "Dispatcher says: " sentence -> send to all workstations,
                  // This is different than SMS_SEND_TEXT in that this message was detected (i.e. we will not send to ANI for TX)
                   if (iTableIndex < 0) {break;} 
                   strTemp = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, get_Local_Time_Stamp(), "P"+int2str(objData.CallData.intPosn));
                   strTemp += objData.TDDdata.SMSdata.strSMSmessage;
                   strTemp += charETX;
                   vMainWorkTable[iTableIndex].CallData = objData.CallData;                   
                   bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                   vMainWorkTable[iTableIndex].enumANIFunction = TDD_DISPATCHER_SAYS;
                   vMainWorkTable[iTableIndex].TDDdata.fLoadTDDReceived(strTemp,true,false);
                   vMainWorkTable[iTableIndex].TDDdata.fSetWindow(true);
                   vMainWorkTable[iTableIndex].enumThreadSending = MAIN;
                   vMainWorkTable[iTableIndex].fAddSentenceToTDDConversation(strTemp);
                   vMainWorkTable[iTableIndex].TDDdata.boolTDDthresholdMet = true;
                   vMainWorkTable[iTableIndex].boolSMSmessage = true;                  
                   vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_SENT, bPreviousALI);

                   // send to WRK
                   vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                   Queue_WRK_Event(vMainWorkTable[iTableIndex]);
*/
                  break;
             case MSRP_CONNECT:
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                  bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified;
                  strTempA = vMainWorkTable[iTableIndex].CallData.stringTenDigitPhoneNumber;
                  strTemp  = vMainWorkTable[iTableIndex].CallData.stringPANI;
                  vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                  if (bCallbackVerified)
                   {
                    vMainWorkTable[iTableIndex].CallData.fLoadCallbackNumber(strTempA);
                    vMainWorkTable[iTableIndex].CallData.stringPANI                = strTemp;
                   }
                  vMainWorkTable[iTableIndex].fUpdateTrunkTypeStringwithTDD();
                  vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                  vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;
                  vMainWorkTable[iTableIndex].boolConnect  = true;
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_CONNECT,"SOFTWARE LOAD");
                  if(boolNG_ALI){ CheckforNGALIinUnmatchedTable(iTableIndex, intTrunk);}

                  if(vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived)    
                   {
                    vMainWorkTable[iTableIndex].boolConnectOutofOrder = false;
                    if (!objData.boolDoNotSendToCAD  ) {SendALItoCAD_Conference_List(iTableIndex);}
                   }
                  vMainWorkTable[iTableIndex].stringConnectTimeStamp = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].boolSMSmessage = true;
                  vMainWorkTable[iTableIndex].TextData.fSetWindow(true);
                  vMainWorkTable[iTableIndex].TextData.boolTextSendButton = true;
                  vMainWorkTable[iTableIndex].TextData.SMSdata.strSMSmessage.clear(); // do not want to add another message !!! Already there on ring
                  // workstation code ..
                  vMainWorkTable[iTableIndex].enumThreadSending = MAIN;
                  if (objData.enumANIFunction == CONNECT) { vMainWorkTable[iTableIndex].fResetTransferWindow(false,true);}
                  vMainWorkTable[iTableIndex].UpdatePSAPstatus();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  if (boolShowPSAPstatusApplet) {Queue_WRK_Event(vMainWorkTable[iTableIndex]);}                      
                   vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_RECEIVED, bPreviousALI);

                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);

                  vMainWorkTable[iTableIndex].CallData.fLoadPosn(objData.CallData.intPosn);  
                  break;

             case SMS_MSG_RECEIVED:
                  if (iTableIndex < 0) { iTableIndex = IndexOfNewMainTableEntry(objData);}
                  else                 { vMainWorkTable[iTableIndex].CallData = objData.CallData;}
                  
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);   

                    
                  //Get URL if available
                  

                  //Legacy TDD Code ... To be removed later:
                  objData.TextData.SMSdata.CreateCallerSays();
                  vMainWorkTable[iTableIndex].TextData.SMSdata = objData.TextData.SMSdata; 
                  if (objData.TDDdata.SMSdata.CIVICA_TEST()) {objData.ALIData.I3Data.fLoadSimulationData(objData.TDDdata.SMSdata.strBidId); } //remove
                  vMainWorkTable[iTableIndex].ALIData.I3Data = objData.ALIData.I3Data; // not needed
                  vMainWorkTable[iTableIndex].TextData.TextType = MSRP_MESSAGE;
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].TextData.fSetWindow(true);
          //      vMainWorkTable[iTableIndex].TextData.boolTextSendButton = true;
                  vMainWorkTable[iTableIndex].enumThreadSending = MAIN;
                  vMainWorkTable[iTableIndex].fAddSentenceToTextConversation(objData.TextData.SMSdata.strSMSmessage);
                  vMainWorkTable[iTableIndex].boolSMSmessage = true;
                  vMainWorkTable[iTableIndex].boolRinging  = true;
                  if (vMainWorkTable[iTableIndex].boolConnect){
                   vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_SMS_TEXT,"SOFTWARE LOAD");
                   vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_RECEIVED, bPreviousALI);
                  }
                  else{
                   // state is ringing until we answer .... by sending a text ...
                   vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_RINGING,"SOFTWARE LOAD");
                   vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_RECEIVED, bPreviousALI);
                   
                  }
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
 
                  if (!bPreviousALI) {                 
                   if      (boolMSRP_LIS_USE_POST_ONLY           )                     { vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = POST_LIS_LOCATION_REQUEST;} 
                   else if (objData.TextData.SMSdata.objLocationURI.boolDataRecieved)  { vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; } 
                   else                                                                { boolBidPidflo = false; }
                  } 
                 if (boolBidPidflo) {Queue_LIS_Event(vMainWorkTable[iTableIndex]);}
                 boolBidPidflo = true;

/*
                  if (!bPreviousALI) {
                   vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_REBID;
                   vMainWorkTable[iTableIndex].ALIData.enumALIBidType = INITIAL;
                   vMainWorkTable[iTableIndex].enumANIFunction = ALI_REQUEST;
                   SendANItoALI(vMainWorkTable[iTableIndex]);
                  }                
                 vMainWorkTable[iTableIndex].enumANIFunction        = SMS_MSG_RECEIVED;
*/
                  break;

             case TDD_MUTE:
                  //Asterisk TDD Mute change
                  //if state does not change do nothing ...
                  if (iTableIndex < 0) {break;}
                  bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);   
                  if (vMainWorkTable[iTableIndex].TextData.TDDdata.boolTDDmute == objData.TextData.TDDdata.boolTDDmute){break;}
                  vMainWorkTable[iTableIndex].stringTimeOfEvent = objData.stringTimeOfEvent;
                  vMainWorkTable[iTableIndex].TextData.TDDdata.boolTDDmute = objData.TextData.TDDdata.boolTDDmute;
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_TDD_CHANGE_MUTE, bPreviousALI);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  break;

             case TDD_MODE_ON:
                  if (iTableIndex < 0) {break;}
                  cout << "in this tdd mode this code is blank" << endl;
                  break;

             case CANCEL_TRANSFER:
                    if (iTableIndex < 0) {break;}            
                    bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified; 
                    vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                    vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                    bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);
                    vMainWorkTable[iTableIndex].WindowButtonData.boolTransferWindow = true;                      
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                    vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);                 
                    break;

             case TRANSFER_FAIL:
                   if (iTableIndex < 0) {break;}            
                    bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified; 
                    vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                    vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                    bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);
                    vMainWorkTable[iTableIndex].fResetTransferWindow(false);                      
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]); 
                    break;



             case EAVESDROP: 
                  //  cout << "EAVESDROP from main" << endl;
                    if (iTableIndex < 0) {break;}            
                    bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified; 
                    vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                    vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                    bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                    vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    vMainWorkTable[iTableIndex].enumANIFunction = EAVESDROP;
                    vMainWorkTable[iTableIndex].FreeswitchData = objData.FreeswitchData;
                    if ((boolLEGACY_ALI)&&(vMainWorkTable[iTableIndex].ALIData.stringAliText.length()))
                     { /* here we would convert to I3*/     }
          //  cout << vMainWorkTable[iTableIndex].ALIData.I3Data.I3XMLData.XMLstring << endl;
          //  cout << vMainWorkTable[iTableIndex].ALIData.stringAliText << endl;
                    Queue_AMI_Input(vMainWorkTable[iTableIndex]);
                    sem_post(&sem_tAMIFlag);

                    break;

             case DIAL_DEST:
                  if (iTableIndex >= 0) 
                   { 
                    // this is a polycom attended transfer
                    Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, SINGLE);
                    objDataCopy = objData;
                    objDataCopy.intWorkStation = objData.CallData.intPosn;
                    if (objDataCopy.fUpdateMuteButton(true)) {
                     Queue_WRK_Event(objDataCopy);
                    }      
                    bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified; 
                    vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                    vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                    bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                    vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                   }
                  else                 
                   { 
                    // this is a DIAL out
                    Queue_RCC_Event(TELEPHONE, objData, objData.CallData.intPosn, SINGLE);
                    objDataCopy = objData;
                    objDataCopy.intWorkStation = objData.CallData.intPosn;
                    if (objDataCopy.fUpdateMuteButton(true)) {
                     Queue_WRK_Event(objDataCopy);
                    }      
                    iTableIndex = IndexOfNewMainTableEntry(objData);
                    vMainWorkTable[iTableIndex].stringRingingTimeStamp = objData.stringTimeOfEvent;
                    vMainWorkTable[iTableIndex].fLoad_CallState(MAIN, XML_STATUS_MSG_DIALING,"SOFTWARE LOAD");
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_BID_NOT_ALLOWED; 
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_DIALING);
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                   }
                  break;

             case BLIND_TRANSFER:
                    if (iTableIndex < 0) {break;}
                    Queue_RCC_Event(RADIO, objData, objData.CallData.intPosn, SINGLE);
                    //objDataCopy = objData;
                    //objDataCopy.intWorkStation = objData.CallData.intPosn;
                    //if (objDataCopy.fUpdateMuteButton(false)) {
                     //Queue_WRK_Event(objDataCopy);
                    //}      
                    bCallbackVerified = vMainWorkTable[iTableIndex].CallData.boolCallBackVerified; 
                    vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                    vMainWorkTable[iTableIndex].CallData.boolCallBackVerified = bCallbackVerified;
                    bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_CONNECT, bPreviousALI);
                    vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    break;

            
             case DELETE_RECORD:
                  if (iTableIndex < 0) {break;}
                  vMainWorkTable[iTableIndex].fXML_MessageDeleteRecord();
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                  vMainWorkTable.erase(vMainWorkTable.begin()+iTableIndex);

                  break;

             case UPDATE_ANI_DATA:
                  if (iTableIndex < 0) {break;} 

                  vMainWorkTable[iTableIndex].CallData                      = objData.CallData;
                  vMainWorkTable[iTableIndex].FreeswitchData                = objData.FreeswitchData;
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ANI_DONT_UPDATE_STATUS, vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);

                  break;

             case TAKE_CONTROL:

                  break;

             
 
             default:
              //    cout << "WTFO" << objData.enumANIFunction << EAVESDROP << endl;
                  SendCodingError("ExperientController.cpp - coding error Main_ScoreBoard_Event switch (objData.enumANIFunction)");
              cout << "coding error -> " << ANIfunction(objData.enumANIFunction) << endl;
   

            }//end switch (objData.enumANIFunction)

           break;
      case LIS:
           if (!boolNG_ALI) {break;}

           switch(objData.ALIData.I3Data.enumLISfunction)  {

             case LIS_HTTP_DATA_RECEIVED: case LIS_HTTP_TIMEOUT: case ECRF_HTTP_TIMEOUT:
                  if (iTableIndex < 0) {break;} 

                   boolA = (objData.ALIData.I3Data.enumLISfunction == LIS_HTTP_TIMEOUT);
                   boolB = (objData.ALIData.I3Data.enumLISfunction == ECRF_HTTP_TIMEOUT);
                   objData.CallData = vMainWorkTable[iTableIndex].CallData;
                   objData.TextData = vMainWorkTable[iTableIndex].TextData;
                   objData.ALIData  = vMainWorkTable[iTableIndex].ALIData;  
                   objData.intCallStateCode  = vMainWorkTable[iTableIndex].intCallStateCode;
                   objData.intCallStatusCode = vMainWorkTable[iTableIndex].intCallStatusCode;
                   objData.boolConnect       = vMainWorkTable[iTableIndex].boolConnect;
                  
                   if (boolA||boolB) {
                    if (boolB) {strTemp = LIS_MESSAGE_277b;}
                    else       {strTemp = LIS_MESSAGE_277;}
                    objMessage.fMessage_Create(LOG_CONSOLE_FILE,277, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objData.TextData,
                                               objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objData.ALIData, strTemp, objData.strErrorMessage ); 
                    enQueue_Message(LIS,objMessage);
                    objData.ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
                    objData.WindowButtonData.fSetButtonsForALItimeout(true);
                    objData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT, true);
                    Queue_WRK_Event(objData);
                   }
                   else {
                ////cout << "ALI Button on" << endl;
                    objData.ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
                    objData.WindowButtonData.fSetButtonsForALIReceived();
                    objData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
                    Queue_WRK_Event(objData);
                   }

                  break;

             

             case URL_RECIEVED:
                   cout << "LIS:URLRECIEVED" << endl;
                  // cout << "I3   -> " << objData.ALIData.I3Data.strNenaCallId << endl; 
                  // cout << "Main -> " << objData.strNenaCallId << endl;                  
                   //This is where we start with a URL ALI not by value attached ....
                   //cout << "Dereference LIS in Main" << endl;
                   //need to check if MSRP trunk and check POST ONLY in this ....... FRED
                   
                   objData.ALIData.I3Data.objGeoLocationHeaderURI = objData.ALIData.I3Data.objLocationURI;
                   if (iTableIndex >= 0) {
                    vMainWorkTable[iTableIndex].ALIData.I3Data.objGeoLocationHeaderURI = objData.ALIData.I3Data.objLocationURI;
                   } 

                   clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);
                   BidNextGenURI(iTableIndex, objData); 
                   break;
             case LEGACY_ALI_DATA_RECIEVED:
                  //cout << "legacy ALI in Main" << endl;
                  if (iTableIndex < 0)
                   {
                    iTableIndex = IndexWithFreeswitchChannelUUID(objData.FreeswitchData.objChannelData.strChannelID);
                    if (iTableIndex < 0)
                     {
                      sem_wait(&sem_tMutexUnmatchedALIData);

                      cout << "Legacy ALI is unmatched push into table, size was = " << vMainWorkTable.size() << " Channel-ID = "<< objData.FreeswitchData.objChannelData.strChannelID << endl;
                      vUnmatchedALIData.push_back(objData);

                      objData.fClear_Record(); 
                      sem_post(&sem_tMutexUnmatchedALIData);
                      break;
                     }
                   }
                  vMainWorkTable[iTableIndex].ALIData                             = objData.ALIData;
                  vMainWorkTable[iTableIndex].ALIData.intALIState                 = XML_STATUS_CODE_ALI_RECEIVED;
                  vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived       = true; 
                  vMainWorkTable[iTableIndex].fLoadCallbackfromALI();
                  vMainWorkTable[iTableIndex].ALIData.stringType                  = 2;
                  vMainWorkTable[iTableIndex].WindowButtonData.fSetNoALICadBid();                     
                  vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, true);


                 //update Callback In ANI
                 if (vMainWorkTable[iTableIndex].CallData.intCallbackNumber)
                  {
                    vMainWorkTable[iTableIndex].enumANIFunction = UPDATE_CALLBACK;
                    vMainWorkTable[iTableIndex].CallData.fSetTrunkTypeString();
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);
                  } 
                  vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);

                  break;



             case I3_BY_VALUE:
                  //  BY_VALUE came from ANI ... I3_DATA_RECEIVED comes from LIS
                  if (boolLEGACY_ALI_ONLY) {cout << "LEGACY ALI ONLY" << endl; break;}
                  cout << "I3 PIDFLO By Value in main" << endl;
                  cout << "BREAK ..." << endl;
                  break;
                  iTableIndex = IndexWithFreeswitchChannelUUID(objData.FreeswitchData.objChannelData.strChannelID);

                  cout << "by value UUID -> " <<  objData.FreeswitchData.objChannelData.strChannelID << endl;                 
                  Queue_LIS_Event(objData); 


                  break;

             case I3_DATA_RECEIVED:
                  if (boolLEGACY_ALI_ONLY) {cout << "LEGACY ALI ONLY" << endl; break;}
                  cout << "I3 PIDFLO in Main" << endl;                 
                  
                  cout << "GOT ALI in KRN index =" << iTableIndex << endl;
             //     objData.ALIData.I3Data.fLoadSimulationData(objData.ALIData.I3Data.strBidId);
                  cout << objData.ALIData.I3Data.strNenaCallId << endl;
                  cout << "table size -> " <<vMainWorkTable.size() << endl;
                 // cout << vMainWorkTable[0].strNenaCallId << endl;
                  objData.fSetPANItoI3();
                  objData.ALIData.I3Data.fCreateLegacyALIrecord();
                  objData.ALIData.I3Data.fCreateLegacyALI30Xrecord();
                  objData.ALIData.I3Data.fCreateNextGenALIrecord();
                  if (iTableIndex < 0) {
                    iTableIndex = IndexWithNENAcallID(objData.ALIData.I3Data.strNenaCallId);
                    cout << "Nena index here is -> " << iTableIndex << endl;
                    cout << objData.ALIData.I3Data.strNenaCallId << endl;
                  }
                  if (iTableIndex < 0) {
                    iTableIndex = IndexWithFreeswitchChannelUUID(objData.FreeswitchData.objChannelData.strChannelID);
                    cout << "FSW index here is -> " << iTableIndex << endl;
                    cout << objData.FreeswitchData.objChannelData.strChannelID << endl;      
                    if (iTableIndex < 0) {
                      //should always match now ...... Race condition if not
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE,280, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LIS_MESSAGE_280, objData.ALIData.I3Data.strNenaCallId,
                                                 objData.FreeswitchData.objChannelData.strChannelID); 
                      enQueue_Message(LIS,objMessage); 
                      break;

                      /*
                      sem_wait(&sem_tMutexUnmatchedALIData);
                      
                      cout << "i3 ALI is unmatched push into table, size was = " << vMainWorkTable.size() << " Channel-ID = "<< objData.FreeswitchData.objChannelData.strChannelID << endl;
                      vUnmatchedALIData.push_back(objData);
                      objData.fClear_Record(); 
                      sem_post(&sem_tMutexUnmatchedALIData);
                      break;
                     */
                    
                    }
                  }

                  if (objData.ALIData.I3Data.I3XMLData.CallInfo.strCallbackNum.length()) {

                   vMainWorkTable[iTableIndex].fLoad_CallbackNumber(objData.ALIData.I3Data.I3XMLData.CallInfo.strCallbackNum); //Load I3 callback
                  }
                  //will need to differentiate from I3 and Pidflo in here ..... I3 takes priority
                  strData.clear();
                  if      (objData.ALIData.I3Data.strPidfXMLData.length()) { 
                   strData = objData.ALIData.I3Data.strPidfXMLData; 
                 //  cout << "PIDFLO ? " << endl;
                //  cout << strData << endl; 
                  }






                  if (objData.ALIData.I3Data.I3XMLData.XMLstring.length()) { 
                   strData = objData.ALIData.I3Data.I3XMLData.XMLstring;
                  // cout << "ALIBODY ?" << endl;
                  // cout << strData << endl;
                   vMainWorkTable[iTableIndex].fLoad_CallbackNumber(objData.ALIData.I3Data.I3XMLData.CallInfo.strCallbackNum); //Load I3 callback
                  }
                  





                  if (!strData.empty()){

                 //  objMessage.fMessage_Create(LOG_CONSOLE_FILE,278, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA,
                 //                             objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LIS_MESSAGE_278, strData); 
                //   enQueue_Message(LIS,objMessage); 
                  }
                  else {
                   SendCodingError( "Experient_Controller.cpp CODING ERROR in case I3_DATA_RECEIVED: XML string empty!");
                   //break;
                  }
                  vMainWorkTable[iTableIndex].ALIData                             = objData.ALIData;
                  vMainWorkTable[iTableIndex].ALIData.intALIState                 = XML_STATUS_CODE_ALI_RECEIVED;
                  vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived       = true;
                  vMainWorkTable[iTableIndex].ALIData.stringType                  = 2; 

                  switch(enumI3CONVERSION_TYPE) {
                   case ALI30W:  vMainWorkTable[iTableIndex].ALIData.stringAliText = objData.ALIData.I3Data.strALI30WRecord; break;
                   case ALI30X:  vMainWorkTable[iTableIndex].ALIData.stringAliText = objData.ALIData.I3Data.strALI30XRecord; break;
                   case NEXTGEN: vMainWorkTable[iTableIndex].ALIData.stringAliText = objData.ALIData.I3Data.strNGALIRecord; break;
                  }
                  vMainWorkTable[iTableIndex].fLoadCallbackfromALI();
                  vMainWorkTable[iTableIndex].ALIData.stringType                  = 2;
                  vMainWorkTable[iTableIndex].WindowButtonData.fSetNoALICadBid(); // this might be it  .......                    
                  vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
     //             vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, true); 
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
 
                  //cout << "callbacknumber -> " << vMainWorkTable[iTableIndex].CallData.intCallbackNumber << endl;
                  //cout << "ALI - Text -> " << vMainWorkTable[iTableIndex].ALIData.stringAliText << endl;
                                  
//cout << ASCII_String(objData.ALIData.I3Data.strALI30WRecord.c_str(),objData.ALIData.I3Data.strALI30WRecord.length()) << endl;
                 //update Callback In ANI
                 if (vMainWorkTable[iTableIndex].CallData.intCallbackNumber) {
                    vMainWorkTable[iTableIndex].enumANIFunction = UPDATE_CALLBACK;
                    vMainWorkTable[iTableIndex].CallData.fSetTrunkTypeString();
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);
                 } 
                 strData = charSTX + vMainWorkTable[iTableIndex].ALIData.stringAliText; 
                 strData += charETX;
                 
                 objMessage.fMessage_Create(LOG_CONSOLE_FILE, 279, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].CallData, objBLANK_TEXT_DATA, objBLANK_LIS_PORT,
                                            objBLANK_FREESWITCH_DATA, vMainWorkTable[iTableIndex].ALIData, LIS_MESSAGE_279, strData ,"","","","","", NORMAL_MSG, ALI_RECORD);

                 enQueue_Message(LIS,objMessage);

                 vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                 Queue_WRK_Event(vMainWorkTable[iTableIndex]);  //do we need this one ?

                  cout << "ALI main Button on ................................................................................................" << endl;
                  
                  vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
                  vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();
                  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);

                  // set clock for auto wireless rebid ???? ..
                  //cout << "AUTO WIRELESS REBID >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ?????????????????????????????????????????" << endl;
                  //cout << intALI_WIRELESS_AUTO_REBID_COUNT << endl;
                  clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);

           
                  // Send to CAD if connected or out of order situation and not a manual ALI and posn != 0
                 if((((vMainWorkTable[iTableIndex].boolConnect)||(vMainWorkTable[iTableIndex].boolConnectOutofOrder))&&
                  (!(CheckforManualBidTrunk(intTrunk)))&&(true))) {  

                  vMainWorkTable[iTableIndex].boolConnectOutofOrder = false;
                  SendALItoCAD_Conference_List(iTableIndex);
                 }
                 break;

             default:
                   break;

            }

           break;

      case ALI:       
           //cout << "ALI in MAin" << endl;
           // check if ALI is from a Disconnected Call ....
           if (objData.ALIData.enumALIBidType == LATE_RECORD)  { 
              cout << "late record" << endl;
              objData.fLoadCallbackfromALI();
              objData.ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
              if (objData.boolAbandoned&& (iTableIndex < 0)) 
               {
                //late abandoned clear
                objData.fLoad_CallState(MAIN,XML_STATUS_MSG_ABANDONED_CLEARED,"SOFTWARE LOAD");
               }


              objData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, true);
              objData.WindowButtonData.fSetButtonsForALIReceived();

              Queue_WRK_Event(objData);

              if (iTableIndex >= 0) {
               vMainWorkTable[iTableIndex].ALIData                             = objData.ALIData;
               vMainWorkTable[iTableIndex].ALIData.intALIState                 = XML_STATUS_CODE_ALI_RECEIVED;
               vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived       = true; 
               vMainWorkTable[iTableIndex].ALIData.stringAliText               = objData.ALIData.stringAliText;
               vMainWorkTable[iTableIndex].ALIData.stringType                  = objData.ALIData.stringType;
               vMainWorkTable[iTableIndex].ALIData.I3Data                      = objData.ALIData.I3Data;
               vMainWorkTable[iTableIndex].fLoadCallbackfromALI();             
              // vMainWorkTable[iTableIndex].WindowButtonData.fSetNoALICadBid();
               cout << "set for ALI RX" << endl;
               vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();

              }
                  
              break;
           }//end if (objData.ALIData.enumALIBidType == LATE_RECORD)

           if (iTableIndex < 0) {break;}
           //cout << "found in table" << endl;
           bPreviousALI      = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);
           iPreviousCallBack = vMainWorkTable[iTableIndex].CallData.intCallbackNumber;
           bCallbackWasZero  = (vMainWorkTable[iTableIndex].CallData.intCallbackNumber == 0);
           vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
           // Initial , Rebid or Manual or Auto Bid --- Timeouts
           if (objData.ALIData.boolALIRecordFail)
            {
             //cout << "record fail" << endl;
             vMainWorkTable[iTableIndex].ALIData.stringALITimeoutTimeStamp  = objData.stringTimeOfEvent;
             if (!vMainWorkTable[iTableIndex].boolAbandoned)
              {
               vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALItimeout(bPreviousALI||objData.ALIData.boolALIRecordReceived);
               vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK = objData.ALIData.timespecTimeAliAutoRebidOK;
              }
  
             vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
             switch (vMainWorkTable[iTableIndex].ALIData.enumALIBidType)
              {
               case INITIAL:
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT,bPreviousALI);
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    break;
               case AUTO:
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_AUTO_ALI_TIMEOUT;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_AUTO_ALI_TIMEOUT,bPreviousALI);
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    break;     
               case REBID:
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT,bPreviousALI);
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    break;
               case MANUAL:
                    vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForManualALITimeout(); 
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_MAN_ALI_TIMEOUT;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_MAN_ALI_TIMEOUT);
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    break;
               default:
                    SendCodingError("ExperientController.cpp - coding error Main_ScoreBoard_Event in switch (vMainWorkTable[iTableIndex].ALIData.enumALIBidType): ALI Fail");

              }// end switch (vMainWorkTable[iTableIndex].ALIData.enumALIBidType)
            
             if(CheckforManualBidTrunk(intTrunk)) {vMainWorkTable.erase(vMainWorkTable.begin()+iTableIndex);} 
             break;
            }// end if (objData.ALIData.boolALIRecordFail)

           
           // Don't update if had record but got record not found
           if ((objData.ALIData.stringType == "9")&&(bPreviousALI))  
            {
             //cout << "record not found" << endl;
             vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
             vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, true);
             vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();
             Queue_WRK_Event(vMainWorkTable[iTableIndex]);
             break;
            }
          // cout << "Main Record process" << endl;                  
           vMainWorkTable[iTableIndex].ALIData.intALIBidIndex              = objData.ALIData.intALIBidIndex;
           vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived       = true;
           vMainWorkTable[iTableIndex].ALIData.stringALIReceivedTimeStamp  = objData.stringTimeOfEvent;
           vMainWorkTable[iTableIndex].ALIData.stringAliText               = objData.ALIData.stringAliText;
           vMainWorkTable[iTableIndex].ALIData.intLastDatabaseBid          = objData.ALIData.intLastDatabaseBid;
           if (INIT_VARS.ALIinitVariable.boolUSE_ALI_SERVICE_SERVER) {
            vMainWorkTable[iTableIndex].CallData.fLoadCallbackNumber(objData.CallData.stringTenDigitPhoneNumber);
           }
           else{
            vMainWorkTable[iTableIndex].fLoadCallbackfromALI();
           }
           vMainWorkTable[iTableIndex].ALIData.stringType                  = objData.ALIData.stringType;                   
           vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();
           vMainWorkTable[iTableIndex].ALIData.I3Data                      = objData.ALIData.I3Data;


          // cout << "Callbackwas Zero ->" << bCallbackWasZero << endl;
          // cout << "New Callback     ->" << vMainWorkTable[iTableIndex].CallData.intCallbackNumber << endl;
          // cout << "previous callback->" << iPreviousCallBack << endl;

           // update ANI callback if number is updated ...
           if (((bCallbackWasZero)&&(vMainWorkTable[iTableIndex].CallData.intCallbackNumber > 0 ))||
              ((!bCallbackWasZero)&&(vMainWorkTable[iTableIndex].CallData.intCallbackNumber != iPreviousCallBack )) ) {
          //   cout << "update callback" << endl;
             vMainWorkTable[iTableIndex].enumANIFunction = UPDATE_CALLBACK;
             vMainWorkTable[iTableIndex].CallData.fSetTrunkTypeString();
             Queue_ANI_Event(vMainWorkTable[iTableIndex]);
           }

           switch (vMainWorkTable[iTableIndex].ALIData.enumALIBidType)
              {
               case INITIAL:
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, true); 
                    break;
               case AUTO:
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_AUTO_ALI_RECEIVED;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_AUTO_ALI_RECEIVED, true);
                    break;     
               case REBID:
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
                    break;
               case MANUAL:
                    vMainWorkTable[iTableIndex].WindowButtonData.boolALIRebidButton = false;
                    vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_MAN_ALI_RECEIVED;
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_MAN_ALI_RECEIVED, true);
                    break;
               default:
                    SendCodingError("ExperientController.cpp - coding error Main_ScoreBoard_Event in switch (vMainWorkTable[iTableIndex].ALIData.enumALIBidType) :ALI Received)"); 

              }// end switch (vMainWorkTable[iTableIndex].ALIData.enumALIBidType)
           vMainWorkTable[iTableIndex].fUpdatePhoneALI();
           Queue_WRK_Event(vMainWorkTable[iTableIndex]);
           // set clock for auto wireless rebid ..
           clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);
           
           // Send to CAD if connected or out of order situation and not a manual ALI and posn != 0
           if((((vMainWorkTable[iTableIndex].boolConnect)||(vMainWorkTable[iTableIndex].boolConnectOutofOrder))&&
             (!(CheckforManualBidTrunk(intTrunk)))&&(true)))    
             {
              vMainWorkTable[iTableIndex].boolConnectOutofOrder = false;
              SendALItoCAD_Conference_List(iTableIndex);
             }
           // clear if manual rebid
           if(CheckforManualBidTrunk(intTrunk)) { vMainWorkTable.erase(vMainWorkTable.begin()+iTableIndex); }
           break;

      case WRK:
          

           switch(objData.enumWrkFunction)
            {
             case WRK_REBID:

                  switch (objData.ALIData.enumALIBidType) {
                    case REBID:

                         if (iTableIndex < 0) {cout << "WRK REBID TI<0 " << endl; break;}
                         intTrunk = vMainWorkTable[iTableIndex].CallData.intTrunk;
                         vMainWorkTable[iTableIndex].ALIData.strALIRequestTrunk = int2strLZ(intTrunk);
                         if (objData.ALIData.I3Data.objLocationURI.strURI.empty()) {
                          // rebid geolocation

                          if (!vMainWorkTable[iTableIndex].objGeoLocation.strURI.empty()) {

                           vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI = vMainWorkTable[iTableIndex].objGeoLocation;
                           vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI.boolDataRecieved = true;
                           objData.ALIData.I3Data.objLocationURI = vMainWorkTable[iTableIndex].objGeoLocation;
                           objData.ALIData.I3Data.objLocationURI.boolDataRecieved = true;

                           if (vMainWorkTable[iTableIndex].ALIData.I3Data.strBidId.empty()) {
                            //cout << "wrk rebid fLoadBidID() -> " << vMainWorkTable[iTableIndex].strNenaCallId << endl;
                            vMainWorkTable[iTableIndex].ALIData.I3Data.fLoadBidID(vMainWorkTable[iTableIndex].strNenaCallId); 
                           }
                           //cout << "BID ID is -> " << vMainWorkTable[iTableIndex].ALIData.I3Data.strBidId << endl;
                          }
                          else if ((TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType == NG911_SIP )||
                                   (TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType == MSRP_TRUNK)||
                                   (TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType == REFER     ) ) {
                            //send error
                            objMessage.fMessage_Create(LOG_CONSOLE_FILE, 740, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].CallData, 
                            vMainWorkTable[iTableIndex].TextData, objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, vMainWorkTable[iTableIndex].ALIData,
                            WRK_MESSAGE_740 ); 
                            enQueue_Message(WRK,objMessage);
                            vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALItimeout(true);
                            Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                           break;
                          }
                         }
                         else {
                          vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI = objData.ALIData.I3Data.objLocationURI;
                         }



                         switch ( TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType)
                          {
                           case CLID: break;
                           case MSRP_TRUNK:
/*
                                 if (boolMSRP_LIS_USE_POST_ONLY)                                   { vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = POST_LIS_LOCATION_REQUEST; }
                                 else if (objData.ALIData.I3Data.objLocationURI.boolDataRecieved)  { vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; } 
                                 else                                                              { break;}                                                          
                                 
                                Queue_LIS_Event(vMainWorkTable[iTableIndex]); 
*/
                                clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);
                                BidNextGenURI(iTableIndex, vMainWorkTable[iTableIndex]);
                                break;
                           case NG911_SIP: case REFER:
/*
                                  cout << "rebid NG911" << endl;
                                 if (boolNG911_LIS_USE_POST_ONLY)                                    { 
                                  cout << "USE POST" << endl;
                                  vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = POST_LIS_LOCATION_REQUEST;
                                  if (vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI.strURI.empty()) {
                                   vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI.strURI = strNG911_LIS_SERVER;
                                  } 
                                 }
                                 else if (objData.ALIData.I3Data.objLocationURI.boolDataRecieved)    { 
                                  vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = DEREFERENCE_URL;
                                  vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI = objData.ALIData.I3Data.objLocationURI; 
                                 }
                                 else if (!vMainWorkTable[iTableIndex].ALIData.I3Data.objGeoLocationHeaderURI.strURI.empty()) {
                                  vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; 
                                  vMainWorkTable[iTableIndex].ALIData.I3Data.objLocationURI = vMainWorkTable[iTableIndex].ALIData.I3Data.objGeoLocationHeaderURI; 
                                 }
                                 else                                                                { 
                                  cout << "URI data not RX?" <<endl; 
                                  break;
                                 }                                                          

                                Queue_LIS_Event(vMainWorkTable[iTableIndex]); 
*/

                                //note for rebids all data is already in the table we check locationuri in I3Data then gelocation

                                clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);
                                BidNextGenURI(iTableIndex, vMainWorkTable[iTableIndex]);
                                break;

                           default:
                            // check
                            //cout << "in default check ................" << endl;
                            if ((!boolLEGACY_ALI)&&(!boolUSE_ALI_SERVICE_SERVER)) {

                             if (boolNG911_LIS_USE_POST_ONLY)                                    { vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = POST_LIS_LOCATION_REQUEST; }
                             else if (objData.ALIData.I3Data.objLocationURI.boolDataRecieved)    { vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; } 
                             else                                                                { break;}
                             cout << "Que Lis Event ................" << endl;
                             Queue_LIS_Event(vMainWorkTable[iTableIndex]); 
                             break;
                            }

                            if(CheckforManualBidTrunk(intTrunk)){Manual_Bid(objData, iTableIndex, true);break;}
                            // code to rebuild table if not in table.................basically load what we recieved.......
                            if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                        
                            vMainWorkTable[iTableIndex].ALIData.enumALIBidType = REBID;
                            vMainWorkTable[iTableIndex].enumANIFunction = ALI_REQUEST;

                            if(! vMainWorkTable[iTableIndex].WindowButtonData.boolALIRebidButton) {
                              // somehow we got a rebid with the button off ... this would be a Programming error ..
                              objMessage.fMessage_Create(LOG_WARNING, 166, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].fCallData(), 
                                                         objBLANK_TEXT_DATA,objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,KRN_MESSAGE_166);
                              enQueue_Message(MAIN,objMessage);
                              break;
                            }
                            //cout << "send ALI rebid to ALI thread ..." << endl;


                            vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIbid();
                            vMainWorkTable[iTableIndex].ALIData.stringALIRebidTimeStamp = objData.stringTimeOfEvent;
                            bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                            objData            =  vMainWorkTable[iTableIndex];          
                            // send bid to ali
                            SendANItoALI(objData);
                           // Update Workstation
                           vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_REBID;
                           vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_REBID, bPreviousALI);
                           //cout << "Q WRK from main" << endl;
                           Queue_WRK_Event( vMainWorkTable[iTableIndex]);
                           break;
                          }
                         break;
                    case MANUAL:
                         cout << "case MANUAL:" << endl;
                         if(!boolALLOW_MANUAL_ALI_BIDS){break;}
                         if (iTableIndex >= 0) {vMainWorkTable[iTableIndex] = objData; }
                         Manual_Bid(objData, iTableIndex); 
                         break;

                    default:
                        //error
                        SendCodingError("ExperientController.cpp - coding error Main_ScoreBoard_Event switch (objData.ALIData.enumALIBidType)");

                  }//end switch (objData.ALIData.enumALIBidType) 
   
                  break;

             case UPDATE_CAD:
                  if(!boolCAD_EXISTS)                   {break;}
                  // check if Manual Bid
                  if (iTableIndex < 0)  {UpdateCadfromManualBid(objData);              break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData, iTableIndex))          {break;}
                  if (!vMainWorkTable[iTableIndex].WindowButtonData.boolCADSendButton){break;}
                  if (!vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived)     {break;}                 
                 
                              
                  if (vMainWorkTable[iTableIndex].boolDisconnect)                     {break;}
               
                  objData.CallData                  = vMainWorkTable[iTableIndex].CallData;
                  objData.ALIData                   = vMainWorkTable[iTableIndex].ALIData;
                  objData.WindowButtonData          = vMainWorkTable[iTableIndex].WindowButtonData;
                  objData.enumANISystem             = vMainWorkTable[iTableIndex].enumANISystem;

                  // substitute Workstation into Position For CAD
                  objData.CallData.intPosn                   = objData.intWorkStation;
                  objData.CallData.stringPosn                = int2strLZ(objData.CallData.intPosn); 
                  SendALItoCAD_SinglePosition(objData);
                  objMessage.fMessage_Create(0,0,__LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                                             objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "");
                  objData.stringTimeOfEvent = objMessage.stringTimeStamp;
                  objData.boolBroadcastXMLtoSinglePosition = true;
                  objData.stringXMLString = NotifyWorkstation.fCreateAlert(WORKSTATION_ALI_SENT_TO_CAD_MESSAGE, objBLANK_CALL_RECORD);
                  Queue_WRK_Event(objData);

                  break;


             case WRK_TRANSFER:
                  if (iTableIndex < 0) {break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex))                        {break;}  
                  // cout << "WRK TRANSFER........................................................................................................................." << endl;
                  if(!vMainWorkTable[iTableIndex].CallData.fIsInConference(POSITION_CONF,objData.intWorkStation))    // this needs to be changed (objData.intWorkStation) TBC .....
                   {
                    objCallData = objData.fCallData();
                    objCallData.fLoadPosn(objData.intWorkStation);
                    objMessage.fMessage_Create(LOG_WARNING, 143, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA, objBLANK_KRN_PORT,
                                               objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_143, int2strLZ(objData.intWorkStation));                    
                    enQueue_Message(MAIN,objMessage);                  
                    break;
                   }
                  objData.FreeswitchData = vMainWorkTable[iTableIndex].FreeswitchData;
                  objData.enumANIFunction = TRANSFER; 
                  Queue_ANI_Event(objData); 
                  break;

             case WRK_CNX_TRANSFER:
                  if (iTableIndex < 0) {break;}
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  vMainWorkTable[iTableIndex].CallData.TransferData.eTransferMethod = objData.CallData.TransferData.eTransferMethod;
                  vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                  vMainWorkTable[iTableIndex].enumWrkFunction = objData.enumWrkFunction;
                  if ((boolUSE_POLYCOM_CTI)&&(objData.CallData.TransferData.eTransferMethod  == mATTENDED_TRANSFER))
                   {
                    Queue_CTI_Event(vMainWorkTable[iTableIndex]); break; 
                   }
                  objData.enumANISystem = vMainWorkTable[iTableIndex].enumANISystem;

                  objData.enumANIFunction = CANCEL_TRANSFER;
                  Queue_ANI_Event(objData);  
                  break;

             case WRK_BLIND_TRANSFER:
                  //cout << "wrk Blind transfer" << endl;
                  if (iTableIndex < 0) {break;}
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  objData.enumANISystem = vMainWorkTable[iTableIndex].enumANISystem;
                  objData.enumANIFunction = BLIND_TRANSFER;
                  Queue_ANI_Event(objData);
                  break;
/*
             case WRK_CONFERENCE_JOIN:
                  if (iTableIndex < 0) {break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  objData.FreeswitchData = vMainWorkTable[iTableIndex].FreeswitchData;
                  objData.enumANIFunction = CONFERENCE;
                  Queue_ANI_Event(objData);
                  break;
*/
/*
             case WRK_CONFERENCE_LEAVE:
                  if (iTableIndex < 0) {break;}        
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  objData.enumANISystem =  vMainWorkTable[iTableIndex].enumANISystem;
                  objData.enumANIFunction = SILENT_ENTRY_LOG_OFF;
                  Queue_ANI_Event(objData);
                  break;
*/
             case WRK_TAKE_CONTROL:
                  if (iTableIndex < 0) {break;}        
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  Take_Control( iTableIndex, objData); 
                  break;

             case WRK_LOG:
                  // should not get here ...
                  break; 
             
             case  WRK_TDD_CHARACTER:
                   //not used at this time ...
                   //this was used to relay ANI_LINK TDD characters
                   break;


             case WRK_TDD_STRING: case WRK_TEXT_STRING:
                   
                   // This is a "Dispatcher says: " sentence -> send to all workstations, -> send to ANI for TX
                   if (iTableIndex < 0) {break;} 
                   if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}

                   switch (objData.TextData.TextType)
                    {
                     case NO_TEXT_TYPE:
                          //This would be a Legacy TDD String/ SMS Message we need to move the data into a different var ....
                   //       cout << "NO TEXT TYPE" << endl;
                          if (vMainWorkTable[iTableIndex].boolSMSmessage) 
                           { 
                            //SMS Message 
                            objData.enumANIFunction = SMS_SEND_TEXT; 
                            strTempA = objData.TextData.TDDdata.strTDDstring;
                            objData.TextData.TextType = MSRP_MESSAGE;
                            objData.TextData.SMSdata.strSMSmessage = objData.TextData.TDDdata.strTDDstring;
                           }
                          else                                            
                           { 
                            // TDD 
                            objData.enumANIFunction = TDD_DISPATCHER_SAYS; 
                            strTempA = objData.TextData.TDDdata.strTDDstring;
                            objData.TextData.TextType = TDD_MESSAGE;
  //                          objData.TextData.TDDdata = objData.TDDdata;
//cout << "a. this TDD in main" << endl;
                           }
   

                          break;
                     case TDD_MESSAGE:
                   //       cout << "TDD MESSAGE" << endl;
                          objData.enumANIFunction = TDD_DISPATCHER_SAYS; 
                          strTempA = objData.TextData.TDDdata.strTDDstring;
                          objData.TDDdata = objData.TextData.TDDdata;        //Legacy TDD ???
//cout << "b. this TDD in main" << endl;
                          break;

                     case MSRP_MESSAGE:
                    //      cout << "MSRP_MESSAGE" << endl;
                          objData.enumANIFunction = SMS_SEND_TEXT;
                          strTempA = objData.TextData.SMSdata.strSMSmessage;
                    //      cout << "stringTempA ->" << strTempA << endl;
                          break;
                    }

                   // Send string to ANI to be transmitted as TDD or MSRP.
                   Queue_ANI_Event(objData);

                  
                  // if(objData.intWorkStation == vMainWorkTable[iTableIndex].CallData.intPosn){ strTemp = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, "", get_Local_Time_Stamp());}
                 //  else                                                                      { strTemp = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, " ["+int2str(objData.intWorkStation)+"]", get_Local_Time_Stamp());}

                   strTemp = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, get_Local_Time_Stamp(), "P"+int2str(objData.intWorkStation));
                   strTemp += "\n";
                   strTemp += strTempA;                   
                   strTemp += "\n";
                   bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                   vMainWorkTable[iTableIndex].enumANIFunction = TDD_DISPATCHER_SAYS;
            //       vMainWorkTable[iTableIndex].TDDdata.fLoadTDDReceived(strTemp,true,false); //remove
                   vMainWorkTable[iTableIndex].TextData.TDDdata.fLoadTDDReceived(strTemp,true,false);
             //      vMainWorkTable[iTableIndex].TDDdata.fSetWindow(true); //remove
                   vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(true);
                   vMainWorkTable[iTableIndex].enumThreadSending = MAIN;
             //      vMainWorkTable[iTableIndex].fAddSentenceToTDDConversation(strTemp); //remove
                   vMainWorkTable[iTableIndex].fAddSentenceToTextConversation(strTemp);
            //       vMainWorkTable[iTableIndex].TDDdata.boolTDDthresholdMet = true; //remove
                   vMainWorkTable[iTableIndex].TextData.TDDdata.boolTDDthresholdMet = true;

                   if (vMainWorkTable[iTableIndex].boolSMSmessage)
                    {
                     vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_SENT, bPreviousALI);
                    }
                   else
                    {
                     vMainWorkTable[iTableIndex].fUpdateTrunkTypeStringwithTDD();                    
                     vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_TDD_DISPATCHER_SAYS, bPreviousALI);
                    }

                   // send to WRK
                   vMainWorkTable[iTableIndex].CallData.fLoadPosn(objData.intWorkStation);
                   vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                   Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    
                   
                   break;

             case WRK_TDD_MUTE:
                  if (iTableIndex < 0) {break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  objData.FreeswitchData =  vMainWorkTable[iTableIndex].FreeswitchData;
                  objData.enumANIFunction = TDD_MUTE;
                  Queue_ANI_Event(objData);
                  break;

             case WRK_TDD_MODE_ON:
                  if (iTableIndex < 0)                                     {break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  if(boolTDD_AUTO_DETECT)                                  {break;}
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}

                  vMainWorkTable[iTableIndex].fTurnOnTDDmode();       
                  break;

             case WRK_CAD_ERASE:
                  // send CAD Erase Message w/o protections ... Use Workstation for Position , Load Arbitrary Trunk
                  objData.fLoad_Position(SOFTWARE, MAIN, int2strLZ(objData.intWorkStation), 0);
                  objData.fLoad_Trunk(SOFTWARE, MAIN, "", 99);
                  objData.fSetCadEraseMsg();
                  sem_wait(&sem_tMutexCadInputQ);
                  queue_objCadData.push(objData);
                  sem_post(&sem_tMutexCadInputQ);
                  sem_post(&sem_tCadFlag);
                  break;

             case WRK_RELOAD_CONSOLE:
                  objData.fLoad_Position(SOFTWARE, MAIN, int2strLZ(objData.intWorkStation), 0); 
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE,725, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objBLANK_TEXT_DATA, 
                                             objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,WRK_MESSAGE_725 , int2strLZ(objData.intWorkStation)); 
                  enQueue_Message(WRK,objMessage);
                  objData.stringXMLString = NotifyWorkstation.fCreateAlert(WRK_MESSAGE_725w, objData.fCallData());
                  objData.boolBroadcastXMLtoSinglePosition = true;
                  objData.boolReloadWorkstationGrid        = true;
                  Queue_WRK_Event(objData);
                  break;

             case WRK_FORCE_DISCONNECT:
                  Force_Disconnect(objData.CallData.stringUniqueCallID);
                  break;
             case WRK_RADIO_CONTACT_CLOSURE:
                  Queue_RCC_Event(objData.enumRCCstate, objData, objData.intWorkStation, SINGLE);
                  objDataCopy = objData;
                  objDataCopy.intWorkStation = objData.CallData.intPosn;
                  if (objDataCopy.fUpdateMuteButton(true)) {
                    Queue_WRK_Event(objDataCopy);
                  }       
                  break;
    
             case WRK_DIAL_OUT:
                  Check_Abandoned_List(objData);
                  objData.IndexOpenForDialing = FindOpenLineIndexforDialing(objData.intWorkStation);
                //  cout << "workstation ->" << objData.intWorkStation << endl;
                 // cout << "index for dialing ->" << objData.IndexOpenForDialing << endl;
                  objData.enumANIFunction = DIAL_DEST;
                  Queue_ANI_Event(objData); 
                  break;
             
             case WRK_HANGUP: 
                  if (iTableIndex < 0) {break;}
                   //Check if MSRP Hangup
                   if (vMainWorkTable[iTableIndex].boolSMSmessage){

                    //  Send End of Session Text
                    vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                    vMainWorkTable[iTableIndex].enumANIFunction = SMS_SEND_TEXT;
                    vMainWorkTable[iTableIndex].TextData.SMSdata.strSMSmessage = MSRP_END_OF_SESSION;                   
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);
  
                    // Send to Workstation and add to Text Conversation
                    strTemp = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, get_Local_Time_Stamp(), "P"+int2str(objData.intWorkStation));
                    strTemp += "\n";
                    strTemp += MSRP_END_OF_SESSION;                   
                    strTemp += "\n";
                    bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      
                    vMainWorkTable[iTableIndex].enumANIFunction = TDD_DISPATCHER_SAYS;
                    vMainWorkTable[iTableIndex].TextData.TDDdata.fLoadTDDReceived(strTemp,true,false);
                    vMainWorkTable[iTableIndex].TextData.TDDdata.fSetWindow(true);
                    vMainWorkTable[iTableIndex].enumThreadSending = MAIN;
                    vMainWorkTable[iTableIndex].fAddSentenceToTextConversation(strTemp);
                    vMainWorkTable[iTableIndex].TextData.TDDdata.boolTDDthresholdMet = true;                
                    vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_SENT, bPreviousALI);
                    vMainWorkTable[iTableIndex].CallData.fLoadPosn(objData.intWorkStation);
                    vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
                    Queue_WRK_Event(vMainWorkTable[iTableIndex]);
                    
                    // Delay
                    experient_nanosleep(THREE_QUARTER_SEC_IN_NSEC);

                    //Send Hangup to CLI
                    vMainWorkTable[iTableIndex].enumANIFunction = MSRP_HANGUP;
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);  
                    break;
                   }

                  vMainWorkTable[iTableIndex].enumWrkFunction = objData.enumWrkFunction;
                  vMainWorkTable[iTableIndex].intWorkStation  = objData.intWorkStation; 
                  Queue_CTI_Event(vMainWorkTable[iTableIndex]);
                  break;

             case WRK_COMPLETE_ATT_XFER:
                  if (iTableIndex < 0) {break;}
                  vMainWorkTable[iTableIndex].enumWrkFunction = objData.enumWrkFunction;
                  vMainWorkTable[iTableIndex].intWorkStation  = objData.intWorkStation; 
                  Queue_CTI_Event(vMainWorkTable[iTableIndex]);
                  break;

             case WRK_VOLUME_UP: case WRK_VOLUME_DOWN: case WRK_SEND_DTMF:
                  //if (iTableIndex < 0) {break;}
                  Queue_CTI_Event(objData);
                  break;

             case WRK_MUTE:
                  //Toggle Mute Button ....
                  Queue_CTI_Event(objData);
                  // Send Polling Request to Phone ... Returns with Mute status
                  if (objData.fUpdateMuteButton(true)) {
                   Queue_WRK_Event(objData);
                  }
                  else {
                   SendCodingError("Experient_Controller.cpp - ...error updating Mute Button in WRK_MUTE " ); break;
                  }
                  break;
                  
             case WRK_DISCONNECT_REQUEST:
                  if (iTableIndex < 0) {break;}
                 // cout << "disconnect request .. main from " << objData.intWorkStation << endl;
                  vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                  vMainWorkTable[iTableIndex].CallData.fLoadPosn(objData.intWorkStation);
                  if(DisconnectRequest(iTableIndex)) {
                   // conference leave for position ......
                   vMainWorkTable[iTableIndex].enumANIFunction = MSRP_CONFERENCE_LEAVE;
                   Queue_ANI_Event(vMainWorkTable[iTableIndex]); 
                  }
                  vMainWorkTable[iTableIndex].fCreateDisconnectResponceXML(DisconnectRequest(iTableIndex));
                  Queue_WRK_Event(vMainWorkTable[iTableIndex]);   
                  break;

             case WRK_TOGGLE_HOLD:
                 // cout << "toggle Hold" << endl;
                  if (iTableIndex < 0) {break;}
                   // Check if we are Holding a MSRP Text Message .....
                   if (vMainWorkTable[iTableIndex].boolSMSmessage){
                    vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                  //  cout << "workstation" << objData.intWorkStation << endl;
                  //  cout << " is on hold -> " << vMainWorkTable[iTableIndex].CallData.fIsOnHold(objData.intWorkStation) << endl;
                    switch (vMainWorkTable[iTableIndex].CallData.fIsOnHold(objData.intWorkStation))
                     {
                      case false:
                        vMainWorkTable[iTableIndex].enumANIFunction = MSRP_ON_HOLD; break;
                      case true:
                        vMainWorkTable[iTableIndex].enumANIFunction = MSRP_OFF_HOLD; break;
                     }
                    
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);  
                    break;
                   }
                 
                  // check if workstation has more than 1 call on hold and that selected call is on hold
                  if ((NumberOfCallsOnHold(objData.intWorkStation) > 1)&&(vMainWorkTable[iTableIndex].CallData.fIsOnHold(objData.intWorkStation))) {
             
                   // cout << "call is on Hold " << endl;
                    vMainWorkTable[iTableIndex].CallData.CTIsharedLine = LineNumberForOffHold(iTableIndex, objData.intWorkStation);
                   // cout << "Shared Line index" << vMainWorkTable[iTableIndex].CallData.CTIsharedLine << endl;
                    vMainWorkTable[iTableIndex].enumWrkFunction      = WRK_OFF_HOLD;
                    vMainWorkTable[iTableIndex].intWorkStation       = objData.intWorkStation; 
                    Queue_CTI_Event(vMainWorkTable[iTableIndex]);

                  }
                  else {Queue_CTI_Event(objData); /* just press the hold button */ }
                  break;

             case WRK_PICKUP:
                  objData.fLoad_Position(SOFTWARE, MAIN, int2strLZ(objData.intWorkStation), 0);
                  iTableIndex = FindOldestRingingCall();
                  if (iTableIndex < 0) {break;}
                   // Check if we are answering a MSRP Text Message .....
                  if (vMainWorkTable[iTableIndex].boolSMSmessage){
                    vMainWorkTable[iTableIndex].enumANIFunction = MSRP_ANSWER;
                    vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);  
                    break;
                   }
                  vMainWorkTable[iTableIndex].enumWrkFunction          = WRK_PICKUP;
                  vMainWorkTable[iTableIndex].intWorkStation           = objData.intWorkStation;
                  vMainWorkTable[iTableIndex].boolWorkstationIsOnACall = WorkstationIsOnaCall(objData.intWorkStation);
                  Queue_CTI_Event(vMainWorkTable[iTableIndex]);
                  break;

             case WRK_ANSWER:
                   if (iTableIndex < 0) {break;}
                   //cout << "WRK_ANSWER" << endl;
                   // Check if we are answering a MSRP Text Message .....
                   if (vMainWorkTable[iTableIndex].boolSMSmessage){
                    vMainWorkTable[iTableIndex].enumANIFunction = MSRP_ANSWER;
                    vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);  
                    break;
                   }
                   // Answer will do one of two things if call is already answered it will perform a Conference Join through the cli ...
                   // if it is ringing it will answer the call ...................... thru the cti
                   vMainWorkTable[iTableIndex].boolWorkstationIsOnACall = WorkstationIsOnaCall(objData.intWorkStation);
                   
                   boolCallConnectedandNotRingback = ((vMainWorkTable[iTableIndex].boolConnect) && (!vMainWorkTable[iTableIndex].boolRingBack) );
                   switch(boolCallConnectedandNotRingback)
                    {
                     case true:
                          // Need to update the calculation of the line number of the telephone of the user that the call 
                          // resides on (shared or if there is no line presentation)
                          // is shared ......
                          //cout << "HERE 1" << endl;
                          switch (TelephoneEquipment.NumberofLineViews)  //this needs to be recalculated with a new function FRED FRED FRED
                           {
                            case 0:
                                  //TBC need to send dial barge thru cti and dial plan ........ TBC 
                                  SendCodingError( "ExperientController.cpp - not coded in Main_ScoreBoard_Event()");
                                 break;
                            default:
                                 objData.enumWrkFunction = WRK_BARGE;
                                 objData.CallData.CTIsharedLine = vMainWorkTable[iTableIndex].CallData.CTIsharedLine;
                                 Queue_CTI_Event(objData);
                                 break;
                           }            
                          break;

                     case false:
                          //cout << "CTIsharedLine ->" << vMainWorkTable[iTableIndex].CallData.CTIsharedLine << endl;
                          //cout << "RingingLineNumber -> " << vMainWorkTable[iTableIndex].FreeswitchData.objRing_Dial_Data.iRingingLineNumber[objData.intWorkStation] << endl;
                          vMainWorkTable[iTableIndex].enumWrkFunction = WRK_ANSWER;
                          vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                          Queue_CTI_Event(vMainWorkTable[iTableIndex]);
                          break;
                    }
 
                  break;

             case WRK_BARGE:
                  if (iTableIndex < 0) {break;}
                 // cout << "WRK_BARGE" << endl;
                  // Check if we are joining a MSRP Text Message .....
                   if (vMainWorkTable[iTableIndex].boolSMSmessage){
                    vMainWorkTable[iTableIndex].enumANIFunction = MSRP_ANSWER;
                    vMainWorkTable[iTableIndex].intWorkStation = objData.intWorkStation;
                  //  cout << "MSRP BARGE ................." << endl;
                    Queue_ANI_Event(vMainWorkTable[iTableIndex]);  
                    break;
                   }

                  if (!boolUSE_POLYCOM_CTI) {SendCodingError( "ExperientController.cpp - Cannot Barge a call without CTI" ); break;}
                   
                   // Determine line number of Call not connected or a 2 party call on hold... Press Line vs Barge
                   intLineNumber = -1;
                   boolA = vMainWorkTable[iTableIndex].boolConnect;
                   if (!boolA) {intLineNumber = vMainWorkTable[iTableIndex].CallData.CTIsharedLine;}
                   else        {intLineNumber = LineAvailableForOnHoldPickoff(iTableIndex);}
                   boolA = (intLineNumber < 0 );
                   //cout << "experient controller.cpp WRK_BARGE line number -> " << intLineNumber << endl;

                   switch(boolA)
                    {
                     case true:
                         // cout << "Ring/Dial Line Index -> " << TelephoneEquipment.fDetermine_Shared_Line_Phone_LineIndex(vMainWorkTable[iTableIndex].FreeswitchData.objRing_Dial_Data.iGUIlineView, objData.intWorkStation) << endl;
                         // cout << "Number of Line Views -> " << TelephoneEquipment.NumberofLineViews << endl;
                          switch (TelephoneEquipment.NumberofLineViews)// needs to be recalculated by a function call ..... FRED FRED FRED
                           {
                            case 0:
                                // cout << "case zero" << endl;
                                  //TBC need to send dial barge thru cti and dial plan 
                                 objData.enumWrkFunction = WRK_CONF_BARGE;
                                 objData.IndexOpenForDialing = FindOpenLineIndexforDialing(objData.intWorkStation);
                                 objData.CallData = vMainWorkTable[iTableIndex].CallData;
                                 Queue_CTI_Event(objData);
                                 break;

                            default:
                                 /*
                                  1. Determine if Call is on shared Line
                                  2. Determine Number of Positions on Call
                                  3. Determine Conference Size (dest + Pos)
                                   a. Shared Line size 2 with position on hold --- perform pick off (handled by fn LineAvailableForOnHoldPickoff() above ....)
                                   b. Shared Line size 2+ with at least 1 position -- perform SLA Barge (handled here ...)
                                   c. No Shared Line with at least 1 position -- Perform Conference Barge 
                                   d. No Shared Line No Positions Shared line available -- Conference Barge on Shared Line with 911 check

                                 */
                                 //intPositions    = vMainWorkTable[iTableIndex].CallData.fNumPositionsInConference();
                                 //intParticipants = intPositions + vMainWorkTable[iTableIndex].CallData.fNumDestinationsInConference();
                            
                                 intRC = SharedLineThatsAvaiableForBarge(iTableIndex);
                                // vMainWorkTable[iTableIndex].CallData.fSetSharedLineFromLineNumberPlusPosition
                                 //cout <<  "Shared Line Number -> " << vMainWorkTable[iTableIndex].CallData.CTIsharedLine << endl;                              
                                // if (vMainWorkTable[iTableIndex].CallData.fIsOnHold(objData.intWorkStation)) {
                                //  cout << "Workstation is On Hold and is trying to Barge !" << endl;

                                // } 
                                // cout << "WRK BARGE exp controller.cpp shared line avail ->" << intRC << endl;
                                 // need to know number of positions on the call ......
                                 //cout << "# of POS  IN CONF -> " << vMainWorkTable[iTableIndex].CallData.fNumPositionsInConference() << endl;
                                 //cout << "# of DEST IN CONF -> " << vMainWorkTable[iTableIndex].CallData.fNumDestinationsInConference() << endl;

                                 if ( intRC > 0 ) {
                                   // cout << "SLA Barge here" << endl;
                                   // need to find active position participant ......
                                   vMainWorkTable[iTableIndex].CallData.CTIsharedLine = intRC;
                                   objData.CallData = vMainWorkTable[iTableIndex].CallData;
                                   objData.enumWrkFunction = WRK_BARGE;
                                 }
                                 else  {
                                   //cout << "No SLA yet" << endl;
                                   objData.enumWrkFunction = WRK_CONF_BARGE;
                                   
                                   if (TRUNK_TYPE_MAP.fIsTandem(vMainWorkTable[iTableIndex].CallData.intTrunk)) { 
                                  //  cout << "is a 911 call !" << endl;
                                    objData.CallData = vMainWorkTable[iTableIndex].CallData;
                                    objData.IndexOpenForDialing = FindOpenNonDialLineIndexforBarge(objData.intWorkStation);
                                  //  cout << "index for dialing is " << objData.IndexOpenForDialing << endl;
                                    if (!objData.IndexOpenForDialing){
                                   //  cout << "index not found !" << endl;
                                     objData.IndexOpenForDialing = FindOpenLineIndexforDialing(objData.intWorkStation);
                                   //  cout << "index now is " << objData.IndexOpenForDialing << endl;
                                    }
                                   }
                                   else {
                                    objData.CallData = vMainWorkTable[iTableIndex].CallData;
                                    objData.IndexOpenForDialing = FindOpenLineIndexforDialing(objData.intWorkStation);

                                   }
                                 }
                                 Queue_CTI_Event(objData);
                                 break;
                           }            
                          break;

                     case false:

                          if (vMainWorkTable[iTableIndex].CallData.ConfData.fNumofPositionsInHistory() == 2) {
                         //  cout << "here we go !!!!" << endl;
                           objData.enumWrkFunction = WRK_BARGE;
                           objData.CallData.CTIsharedLine = intLineNumber;
                           Queue_CTI_Event(objData);
                          }
                          else {
                          //push the ringing or held line ....
                           objData.enumWrkFunction = WRK_PRESS_LINE;
                           objData.CallData.CTIsharedLine = intLineNumber;
                           Queue_CTI_Event(objData);
                          }
                          break;
                    }

                  break;

   

             case WRK_FLASH_TANDEM:
                  if (iTableIndex < 0)                                     {break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  objData.enumANIFunction = SEND_FLASH;
                  Queue_ANI_Event(objData);     
                  break;

             case WRK_KILL_CHANNEL:
                  if (iTableIndex < 0)                                     {break;} 
                  if(! Check_Same_Call_WRK_Functions(&objData,iTableIndex)){break;}
                  objData.enumANIFunction = KILL_CHANNEL;
                  Queue_ANI_Event(objData); 
                  break;
             default:
                // error
                SendCodingError("ExperientController.cpp - 1. Coding error Main_ScoreBoard_Event switch (objData.enumWrkFunction)");

            }//end switch(objData.enumWrkFunction)

           break;

      default:
            // error
            SendCodingError("ExperientController.cpp - 2. Coding error Main_ScoreBoard_Event switch (objData.enumThreadSending)");

     }// end switch (objData.enumThreadSending)
  

   // UnLock Work Table  
   sem_post(&sem_tMutexMainWorkTable);
 
  }// end (!queue_objMainData.empty())


  // send auto wireless rebids if enabled ..
   if (intALI_WIRELESS_AUTO_REBID_COUNT > 0)
    {
     intRC = sem_wait(&sem_tMutexMainWorkTable);
     if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "1st sem_wait@sem_tMutexMainWorkTable in Main_ScoreBoard_Event", 1);}
     sz = vMainWorkTable.size();
     // this will be moded for super center ....
     for (unsigned int i = 0; i< sz; i++)
      {
       vMainWorkTable[i].fCheckWirelessRebid();
      }// end for for (unsigned int i = 0; i< sz; i++)
     sem_post(&sem_tMutexMainWorkTable);
    }//  if (intALI_WIRELESS_AUTO_REBID_COUNT > 0)
 
/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tMainScoreboardTimerId, 0, &itimerspecMainScoreboardDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 148, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objBLANK_KRN_PORT,
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_148);
 objMessage.fConsole();

return NULL;								
}// end Main_ScoreBoard_Event



/****************************************************************************************************
*
* Name:
*  Function: bool Check_Same_Call(ExperientDataClass objData, int intTrunk)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function checks the phone number in the worktable vs the phone number contained within the
*   object passed in for a match.  if the number has changed the funtion returns false and an error
*   message is generated.
*
* Parameters:
*   intTrunk                                    integer  				
*   objData                                     <ExperientDataClass> object                             
*
* Variables:
*   boolRinging                         Global  <ExperientDataClass> member .. true if ringing function has occured
*   intMAX_NUM_TRUNKS                   Global  <defines.h> maximum number of trunks that can be programmed
*   LOG_WARNING                         Global  <defines.h> log code
*   MAIN                                Global  <header.h> threadorPorttype enumeration member
*   obj_arrayMainWorkTable[]            Global  <ExperientDataClass> object array .. the main work table
*   objMessage                          Local   <MessageClass> object
*   stringPosn                          Global  <ExperientDataClass> member .. position number
*   stringTenDigitPhoneNumber           Global  <ExperientDataClass> member .. callback number
*   stringTrunk                         Global  <ExperientDataClass> member .. trunk number
*    
*                                                              
* Functions:
*   .fClearRecord()                     Global  <ExperientDataClass>
*   .fMessage_Create()                  Local   <globalfunctions.h>
*
* Author(s):
*   Bob McCarthy 			Original: 7/25/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
/*
bool Check_Same_Call(ExperientDataClass objData, int intTrunk, string stringArg, bool boolCheckPhone)
{
 MessageClass                   objMessage;
 extern ExperientDataClass      obj_arrayMainWorkTable[intMAX_NUM_TRUNKS+1];

 if ((obj_arrayMainWorkTable[intTrunk].stringTenDigitPhoneNumber != objData.stringTenDigitPhoneNumber)&& boolCheckPhone)
  {
   // test
   objMessage.fMessage_Create(LOG_WARNING, 128, obj_arrayMainWorkTable[intTrunk].fCallData(), objBLANK_KRN_PORT, 
                              KRN_MESSAGE_128, stringArg, objData.stringTenDigitPhoneNumber);
   enQueue_Message(MAIN,objMessage);

   return false; 
  }

 if (obj_arrayMainWorkTable[intTrunk].intUniqueCallID != objData.intUniqueCallID)
  {
   //test
   objMessage.fMessage_Create(LOG_WARNING, 127, obj_arrayMainWorkTable[intTrunk].fCallData(), objBLANK_KRN_PORT, 
                              KRN_MESSAGE_127, stringArg, int2str(objData.intUniqueCallID));
   enQueue_Message(MAIN,objMessage);

   return false; 
  }

 return true;

}// end bool Check_Same_Call(ExperientDataClass objData, int intTrunk)
*/
/****************************************************************************************************
*
* Name:
*  Function: void SendALItoCAD_SinglePosition(ExperientDataClass objData)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function places a single record onto the CAD Input Q and sends a signal to the CAD thread
*   that the data has been placed onto it's Queue. it is called when the following condition
*   exists : Off Hold, Conference Join, Manual ALI received 
*   either order.
*
* Parameters: 				
*   objData                                     <ExperientDataClass> object                             
*
* Variables:
*   boolCAD_EXISTS                      Global  boolean initialized from config file
*   .intPosn				Global  <ExperientDataClass> member 
*   queue_objCadData                    Global  queue to CAD thread
*   sem_tCadFlag                        Global  signaling semaphore  
*   sem_tMutexCadInputQ                 Global  mutex semaphore to prevent simultanous Q operations
*   .stringPosn				Global  <ExperientDataClass> member 
*                                                              
* Functions:
*   .fSetCadRecord()                    Global  <ExperientDataClass>
*   int2strLZ()				Global  <globalfunctions.h>
*   .push()                             Library <queue>
*   sem_post()                          Library <semaphore.h>
*   sem_wait()                          Library <semaphore.h>
*
* Author(s):
*   Bob McCarthy 			Original: 4/6/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
void SendALItoCAD_SinglePosition(ExperientDataClass objData, bool boolSendAllPositions, int intTrunk)
{
 
 if((boolCAD_EXISTS)&&(objData.ALIData.boolALIRecordReceived))
  {
   // send actual position that fielded call
   objData.CallData.stringPosn = int2strLZ(objData.CallData.intPosn); // just to make sure it is in correct format ....
   objData.fSetCadRecord();
   sem_wait(&sem_tMutexCadInputQ);
   queue_objCadData.push(objData);
   sem_post(&sem_tMutexCadInputQ);
   sem_post(&sem_tCadFlag);

  }// end if (boolCAD_EXISTS)

}// end void SendALItoCAD(ExperientDataClass objData)
/****************************************************************************************************
*
* Name:
*  Function: void SendALItoCAD_Conference_List(int iIndex)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function places multiple ALI records onto the CAD Input Q and sends a signal to the CAD thread
*   that the data has been placed onto it's Queue. The list sent to is the active conference list minus the on Hold
*   list.  The function is called when ALI Data has been received.  (ANI Connect as well)
*
* Parameters: 
*   iIndex                                    integer   Table index				
*
* Variables:
*   boolALIRecordReceived               Local   <ALI_Data> member boolean
*   boolCAD_EXISTS                      Global  boolean initialized from config file
*   intNUM_WRK_STATIONS                 Global  number of workstations
*   MAIN                                Global  <header.h> <threadorPorttype> enumeration member 
*   POSITION_CONF                       Global  <header.h> <conference_member> enumeration member
*   vMainWorkTable[]                    Global  array of <ExperientDataClass> objects
*   objData                             Local   <ExperientDataClass> object
*   SOFTWARE                            Global  <header.h> <data_format> enumeration member
*                                                              
* Functions:
*   .fIsInConference()                  Global  <Call_Data>                       (bool)
*   .fIsOnHold()                        Global  <Call_Data>                       (bool)
*   .fLoad_Position()                   Global  <ExperientDataClass>              (bool)
*   SendALItoCAD_SinglePosition()       Local                                     (void)
*
* Author(s):
*   Bob McCarthy 			Original: 01/21/2010
*                                       Updated : N/A
*
****************************************************************************************************/ 
void SendALItoCAD_Conference_List(int iIndex)
{
 // This function must be called with a semaphore lock on vMainWorkTable[]
 ExperientDataClass                             objData;
 
 if(!boolCAD_EXISTS)                                                {return;}
 if(iIndex < 0)                                                     {return;}
 if(!vMainWorkTable[iIndex].ALIData.boolALIRecordReceived)          {return;}

 
 objData           = vMainWorkTable[iIndex];

 for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
  {  
   if (!objData.CallData.fIsInConference(POSITION_CONF, i))    {continue;}
   if (objData.CallData.fIsOnHold(i))                          {continue;}
   if (!objData.fLoad_Position(SOFTWARE, MAIN, int2str(i), 0)) {continue;}
 //  cout << "sending ALI to CAD for position " << i << endl; 
   SendALItoCAD_SinglePosition(objData);   

  }// end for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
}
/****************************************************************************************************
*
* Name:
*  Function: void SendCAD_Erase_Msg(string strPosn, int iIndex)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function places an Erase record onto the CAD Input Q and sends a signal to the CAD thread
*   that the data has been placed onto it's Queue. It is called when the following conditions
*   exists : ALI Data exists and CAD exists and an ANI Hold On or 
*   an ANI Conference Leave has occurred
*
* Parameters: 
*   intTrunk                                    integer   Table index				
*   strPosn                                     <cstring> Position to send in Erase Message                             
*
* Variables:
*   ALIData                             Local   <ALI_Data> object
*   boolALIRecordReceived               Local   <ALI_Data> member boolean
*   boolCAD_EXISTS                      Global  boolean initialized from config file
*   vMainWorkTable[]                    Global  array of <ExperientDataClass> objects
*   objData                             Local   <ExperientDataClass> object
*   queue_objCadData                    Global  queue to CAD thread
*   sem_tCadFlag                        Global  signaling semaphore  
*   sem_tMutexCadInputQ                 Global  mutex semaphore to prevent simultanous Q operations
*   SOFTWARE                            Global  <header.h> <data_format> enumeration member
*    
*                                                              
* Functions:
*   .fLoad_Position()                   Global  <ExperientDataClass>              (bool)
*   .fSetCadEraseMsg()                  Global  <ExperientDataClass>              (void)
*   .push()                             Library <queue>                           (void)
*   sem_post()                          Library <semaphore.h>                     (int)
*   sem_wait()                          Library <semaphore.h>                     (int)
*
* Author(s):
*   Bob McCarthy 			Original: 05/12/2009
*                                       Updated : N/A
*
****************************************************************************************************/ 
void SendCAD_Erase_Msg(string strPosn, unsigned int iIndex)
{
 ExperientDataClass                             objData;
 int                                            intPosition;

 if(!Validate_Integer(strPosn))    {return;}
 if (iIndex > vMainWorkTable.size()-1) {return;}

 objData           = vMainWorkTable[iIndex];
 intPosition       = char2int(strPosn.c_str());

 if((boolCAD_EXISTS)&&(objData.ALIData.boolALIRecordReceived)&&(intPosition > 0))
  {
   objData.fLoad_Position(SOFTWARE, MAIN, strPosn, 0);
   objData.fSetCadEraseMsg();
   sem_wait(&sem_tMutexCadInputQ);
   queue_objCadData.push(objData);
   sem_post(&sem_tMutexCadInputQ);
   sem_post(&sem_tCadFlag);
  }
}
/****************************************************************************************************
*
* Name:
*  Function: void SendCAD_Erase_Msg_To_Conference_List(int iIndex)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function places multiple Erase records onto the CAD Input Q and sends a signal to the CAD thread
*   that the data has been placed onto it's Queue. The list sent is the active conference list minus the on Hold
*   list.  It is called when the following conditions exists : ALI Data exists and CAD exists and 
*   an ANI disconnect has occured
*
* Parameters: 
*   iIndex                                    integer   Table index				
*
* Variables:
*   ALIData                             Local   <ALI_Data> object
*   boolALIRecordReceived               Local   <ALI_Data> member boolean
*   boolCAD_EXISTS                      Global  boolean initialized from config file
*   end                                 Local   <size_t> string search index
*   Found                               Local   <size_t> string search index on on hold match
*   MAIN                                Global  <header.h> <threadorPorttype> enumeration member 
*   obj_arrayMainWorkTable[]            Global  array of <ExperientDataClass> objects
*   objData                             Local   <ExperientDataClass> object
*   queue_objCadData                    Global  queue to CAD thread
*   sem_tCadFlag                        Global  signaling semaphore  
*   sem_tMutexCadInputQ                 Global  mutex semaphore to prevent simultanous Q operations
*   SOFTWARE                            Global  <header.h> <data_format> enumeration member
*   start                               Local   <size_t> string search index
*   strActiveConferencePositions        Local   <ExperientDataClass> member string 
*   strConferenceList                   Local   <cstring> the conference list to be manipulated
*   strConferencePosition               Local   <cstring> the conference Position to be sent
*   string::npos                        Library <cstring> size_t indicates beyond end of string
*   strOnHoldPositions                  Local   <ExperientDataClass> member string 
*                                                              
* Functions:
*   .empty()                            Library <cstring>                         (bool)
*   .erase()                            Library <cstring>                         (string)
*   .find                               Library <cstring>                         (size_t)
*   .find_first_of()                    Library <cstring>                         (size_t)
*   .find_first_not_of()                Library <cstring>                         (size_t)
*   .fLoad_Position()                   Global  <ExperientDataClass>              (bool)
*   .fSetCadEraseMsg()                  Global  <ExperientDataClass>              (void)
*   .push()                             Library <queue>                           (void)
*   sem_post()                          Library <semaphore.h>                     (int)
*   sem_wait()                          Library <semaphore.h>                     (int)
*   .substr()                           Library <cstring>                         (string)
*
* Author(s):
*   Bob McCarthy 			Original: 05/12/2009
*                                       Updated : N/A
*
****************************************************************************************************/ 
void SendCAD_Erase_Msg_To_Conference_List(unsigned int iIndex)
{
 // This function must be called with a semaphore lock on vMainWorkTable[]

 ExperientDataClass                             objData;
 string                                         strConferenceList;
 string                                         strConferencePosition;
 size_t                                         start;
 size_t                                         end;
 size_t                                         Found;


 if(!boolCAD_EXISTS)                                                {return;}
 if(iIndex > vMainWorkTable.size()-1)                               {return;}

 if(!vMainWorkTable[iIndex].ALIData.boolALIRecordReceived)          {return;}
 
 objData           = vMainWorkTable[iIndex];
 strConferenceList = vMainWorkTable[iIndex].CallData.ConfData.strActiveConferencePositions;

 start = strConferenceList.find(charTAB);

 while (!strConferenceList.empty())
  {
   end = strConferenceList.find (charTAB, start+1);
   if (end == string::npos) {break;}
   if ((start+1) >= (strConferenceList.length()-1))  {SendCodingError( "ExperientController.cpp - substr SendCAD_Erase_Msg_To_Conference_List"); return;}
   strConferencePosition = strConferenceList.substr(start+1, (end-start-1));
   strConferenceList.erase(0,end);
   start = 1;
 
   Found = strConferencePosition.find(ANI_CONF_MEMBER_POSITION_PREFIX);
   if (Found == string::npos) {continue;}

   strConferencePosition.erase(0,Found+1);

   Found = strConferencePosition.find_first_not_of("0123456789");
   if (Found != string::npos) {continue;}
  
   if (char2int(strConferencePosition.c_str()) > (unsigned long long int) intNUM_WRK_STATIONS) {continue;}

   if (!objData.fLoad_Position(SOFTWARE, MAIN, strConferencePosition, 0)) {continue;}

   if (objData.CallData.fIsOnHold(objData.CallData.intPosn)) {continue;}


   SendCAD_Erase_Msg(strConferencePosition, iIndex);
  
  }// end while (!strConferenceList.empty())
}

void Send_ALI_Fail_PopUp_To_Conference_List(unsigned int iIndex)
{
 // This function must be called with a semaphore lock on vMainWorkTable[]

 ExperientDataClass                             objData;
 string                                         strConferenceList;
 string                                         strConferencePosition;
 size_t                                         start;
 size_t                                         end;
 size_t                                         Found;
 MessageClass                                   objMessage;
 string                                         strPopUp;
 WorkStation                                    NotifyWorkstation;

 if(iIndex > vMainWorkTable.size()-1)                               {return;}

 objMessage.objCallData = vMainWorkTable[iIndex].CallData;
 strPopUp = objMessage.fALI_Failed_PopUp_Message();
 objData           = vMainWorkTable[iIndex];
 strConferenceList = vMainWorkTable[iIndex].CallData.ConfData.strActiveConferencePositions;

 start = strConferenceList.find(charTAB);

 while (!strConferenceList.empty())
  {
   end = strConferenceList.find (charTAB, start+1);
   if (end == string::npos) {break;}

   if ((start+1)>= (strConferenceList.length()-1)) {SendCodingError("ExperientController.cpp - substr Send_ALI_Fail_PopUp_To_Conference_List"); return;}
   strConferencePosition = strConferenceList.substr(start+1, (end-start-1));
   strConferenceList.erase(0,end);
   start = 1;
 
   Found = strConferencePosition.find(ANI_CONF_MEMBER_POSITION_PREFIX);
   if (Found == string::npos) {continue;}

   strConferencePosition.erase(0,Found+1);

   Found = strConferencePosition.find_first_not_of("0123456789");
   if (Found != string::npos) {continue;}
  
   if (char2int(strConferencePosition.c_str()) > (unsigned long long int) intNUM_WRK_STATIONS) {continue;}

   if (!objData.fLoad_Position(SOFTWARE, MAIN, strConferencePosition, 0)) {continue;}

   if (objData.CallData.fIsOnHold(objData.CallData.intPosn)) {continue;}


   objData.stringXMLString = NotifyWorkstation.fCreateAlert(strPopUp, objBLANK_CALL_RECORD);
   Queue_WRK_Event(objData);
  
  }// end while (!strConferenceList.empty())
}






/****************************************************************************************************
*
* Name:
*  Function: void SendALItoWRK(ExperientDataClass objData)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function places a record onto the WRK Input Q and sends a signal to the WRK thread
*   that the data has been placed onto it's Queue. it is called when the following condition
*   exists : ANI Connect has occured and ALI record recieved.  These two events can occur in 
*   either order.
*
* Parameters: 				
*   objData                                     <ExperientDataClass> object                             
*
* Variables:
*   queue_objWRKData                    Global  queue to WRK thread
*   sem_tWRKFlag                        Global  signaling semaphore  
*   sem_tMutexWRKInputQ                 Global  mutex semaphore to prevent simultanous Q operations
*    
*                                                              
* Functions:
*   fEncodeAliRecordXML()               Global  <ExperientDataClass>
*   sem_post()                          Library <semaphore.h>
*   sem_wait()                          Library <semaphore.h>
*
* Author(s):
*   Bob McCarthy 			Original: 9/20/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
/*
void SendALItoWRK(ExperientDataClass objData, bool boolShowALIStatus)
{
 int   intStatus;
 
 if (boolShowALIStatus) {intStatus = XML_STATUS_CODE_ALI_RECEIVED;}
 else                   {intStatus = XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS;}

 objData.fCallInfo_XML_Message(intStatus, true);
 sem_wait(&sem_tMutexWRKInputQ);
 objData.stringXMLString = objData.stringAliXMLMessage; 
 queue_objWRKData.push(objData);
 sem_post(&sem_tMutexWRKInputQ);
 sem_post(&sem_tWRKFlag);

}// end void SendALItoWRK(ExperientDataClass objData)

*/
/****************************************************************************************************
*
* Name:
*  Function: void SendANItoALI(ExperientDataClass objData)
*
*
* Description:
*  Utility function called by function Main_ScoreBoard_Event(union sigval union_sigvalArg)
*
*
* Details:
*   This function places a record onto the ALI Input Q and sends a signal to the ALI thread
*   that the data has been placed onto it's Queue. 
*
* Parameters: 				
*   objData                                     <ExperientDataClass> object                             
*
* Variables:
*   queue_objAliData                    Global  queue to ALI thread
*   sem_tAliFlag                        Global  signaling semaphore  
*   sem_tMutexAliInputQ                 Global  mutex semaphore to prevent simultanous Q operations
*    
*                                                              
* Functions:
*   .push()                             Library <queue.h>
*   sem_post()                          Library <semaphore.h>
*   sem_wait()                          Library <semaphore.h>
*
* Author(s):
*   Bob McCarthy 			Original: 7/20/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
void SendANItoALI(ExperientDataClass objData)
{
 sem_wait(&sem_tMutexAliInputQ);
 queue_objAliData.push(objData);
 sem_post(&sem_tMutexAliInputQ);
 sem_post(&sem_tAliFlag);

}// end void SendANItoALI(ExperientDataClass objData)

/****************************************************************************************************
*
* Name:
*  Function: void Main_Messaging_Monitor_Event()
*
*
* Description:
*   An event handler within main() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from main.  When activated it
*
*   1. Checks for messages on the message Q, if none then exit.
*
*   2. retrieves messages until Q is empty and processes them.
*
*
* Parameters: 				
*   union_sigvalArg			a library <csignal> structure that contains a pointer to pass  
*					(mqd_t) union_sigvalArg.sival_ptr
*
* Variables:
*   char_arrayBuffer			Local   - buffer to pass the message Q data to
*   CLOCK_REALTIME                      Global  - <ctime> constant
*   intRC				Local   - return code
*   mq_attrMsgQAttr			Local   - <mqueue.h> structure holding the message Q attributes
*   mqd_tMessageQ			Local   - <mqueue.h> MessageQ id passed in from union_sigvalArg.sival_ptr
*   sem_tMutexMainMessageQ			Global  - <semaphore.h> mutex to prevent multiple copies of handler operating at same time
*   ssize_tNbytes			Local   - <mqueue.h> return value of library function mq_receive
*   timespecTimeNow                     Local   - <ctime> struct hodling current time                                                                       
* Functions:  
*   mq_getattr()			Library <mqueue.h>
*   mq_receive()			Library <mqueue.h>
*   Semaphore_Error()                   Global  semaphore error handler
*   sem_wait()             		Library <semaphore.h>
*   sem_post()				Library <semaphore.h>
*
*
* Author(s):
*   Bob McCarthy 			Original: 11/25/2006
*                                       Updated : N/A
*
****************************************************************************************************/ 
//void Main_Messaging_Monitor_Event(union sigval union_sigvalArg)
void *Main_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 queue <MessageClass>*  	queue_objOutputMessageQ;
// void 				*ptrVal 				= union_sigvalArg.sival_ptr;
 string                         stringPopUp;
 string                         stringXML;
 string                         stringTemp;
 WorkStation                    NotifyWorkstation;
 ExperientDataClass             objData;
 int                            intTrunk;
 int                            iTableIndex;
 struct timespec                timespecRemainingTime;
 int                            iLoopCount = 0;
 queue_objOutputMessageQ 	= (queue <MessageClass>*) voidArg;				//recast voidArg
 
/*
//Disable Timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tHealthMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}
*/
do
{
 iLoopCount++;
 if (iLoopCount > 1000) {iLoopCount = 1;}

 experient_nanosleep(intMAIN_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in Main_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexLogMessageQ);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexLogMessageQ, "sem_wait@sem_tMutexLogMessageQ in Main_Messaging_Monitor_Event", 1);}

 while (!queue_objMainMessageQ.empty())
  {
    objMessage = queue_objMainMessageQ.front();
    queue_objMainMessageQ.pop();
 
    objData.enumThreadSending = MAIN;
 

      //Unlock till just before the next iteration 
    sem_post(&sem_tMutexLogMessageQ); 
    sem_post(&sem_tMutexMainMessageQ);




   // Check Callback number versus main Table Callback number
   if ((!objMessage.objCallData.stringUniqueCallID.empty())&&(objMessage.objCallData.intTrunk != 0))
    {
     intRC = sem_wait(&sem_tMutexMainWorkTable);
     if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "a.sem_wait@sem_tMutexMainWorkTable in Main_Messaging_Monitor_Event()", 1);}
    
     iTableIndex = IndexWithCallUniqueIDMainTable(objMessage.objCallData.stringUniqueCallID);
     if (iTableIndex >= 0)
      {
       if (vMainWorkTable[iTableIndex].CallData.intCallbackNumber != 0)
        {
         objMessage.objCallData = vMainWorkTable[iTableIndex].CallData;
         objMessage.fUpdateCallInfoInMessageText();
        }
      }  
         
     sem_post(&sem_tMutexMainWorkTable);
      

    }// end if (objData.intCallbackNumber)
   
    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 100:						
       //non critical coding error
       // send out message email etc......
       //cout << "CODING ERROR CHECK IT !!!!!!!!!!!!!!!!!!!!!!!!\n";                              //test
       break; 
      case 101:						
       // Initializing message
       break;
      case 102:						
       // Semaphores Initialized
       break;
      case 103:
       // LOG Thread Initializing						
       break;
      case 104:						
       // Health Monitor Timer Event Initialized
       break;
      case 105:						
       // Sleep 2 seconds to allow thread(s) creation
       break;
      case 106:						
       // CAD Thread Thread Initializing
       break;
      case 107:
       // ALI Thread Initializing						
       break;
      case 108:						
       // ANI Thread Initializing
       break;
      case 109:
       //						
       break;
      case 110:						
       //
       break;
      case 111:						
       //
       break;
      case 112:						
       //
       break;
      case 113:						
       //
       break;
      case 114:                                           
       //
       break;
      case 115:                                           
       //
       break;
      case 116:                                           
       //
       break;
      case 117:
       if  (!INIT_VARS.fLoadPhonebook() ){SendCodingError("Experient_Conroller.cpp - Unable to update Phonebook!");break;}						
       objData.stringXMLString = NotifyWorkstation.fCreateAlert(NEW_PHONE_BOOK_MESSAGE, objBLANK_CALL_RECORD);
       INIT_VARS.fInitXMLString(0); 
       Queue_WRK_Event(objData);
       break;
      case 118:						
       //
       break;
      case 119:						
       //
       break;
      case 120:						
       //
       break;
      case 121:                                           
       //
       break;
      case 122:                                           
       //
       break;
      case 123:                                           
       //
       break;
      case 124:						
       //
       break;
      case 125:						
       //
       break;
      case 126:	
       INIT_VARS.fLoadMessageControl();					
       break;
      case 127:						
       //
       break;
      case 128:                                           
       //
       break;
      case 129:                                           
       //
       break;
      case 130:                                           
       //
       break;
      case 131:                                           
       //
       break;
      case 132:                                           
       //
       break;
      case 133:                                           
       //
       break;
      case 134:						
       //
       break;
      case 135:						
       //
       break;
      case 136:						
       //
       break;
      case 137:						
       //
       break;
      case 138:                                           
       //
       break;
      case 139:                                           
       //
       break;
      case 140:                                           
       //
       break;
      case 141:                                           
       //
       break;
      case 142:                                           
       //
       break;
//      case 143:   Used lower in code for readability ....                                        
       //
//       break;
      case 144:						
       //
       break;
      case 145:						
       //
       break;
      case 146:						
       //
       break;
      case 147:						
       //
       break;
      case 148:                                           
       //
       break;
      case 149:                                           
       //
       break;
     case 150:                                           
       //
       break;
      case 151:                                           
       //
       break;
      case 152:                                           
       //
       break;
      case 153:                                           
       //
       break;
      case 154:						
       //
       break;
      case 155:						
       //
       break;
      case 156:						
       //
       break;
      case 157:						
       //
       break;
      case 158:                                           
       //
       break;
      case 159:                                           
       //
       break;
      case 160:
       // UDP Error
       break;
      case 170:
       //shutdown Console message MainInputQ size
       break;
      case 171:
       //shutdown Console message MainMessageQ size
       break;
      case 172:
       //shutdown Console message CadMessageQ size
       break;
      case 173:
       //shutdown Console message LogMessageQ size
       break;
      case 174:
       //shutdown Console message AliMessageQ size
       break;
      case 175:
       //shutdown Console message ANIMessageQ size
       break;
      case 176:
       //shutdown Console message OutputMessageQ size
       break;
      case 177:
       //shutdown Console message CAD ACTIVE TRUNK
       break;
      case 178:
       //shutdown Console message SEMAPHORE_ERRORS
       break;
      case 179:
       //shutdown Console message ShutDown Complete
       break;
      case 180:
       // Timer Create Error
       break;
      case 185:
       UpdateVars.boolMAINThreadWait = true;
       boolUPDATE_VARS = true;
       SendSignaltoThreads();
       while ((!UpdateVars.fAllThreadsReady())&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);} 
       if (!boolSTOP_EXECUTION) 
        {      
         if (INIT_VARS.WRKinitVariable.fLoadColorPaletteUsers()) {cout << "Color Editor Users Updated" << endl;}
        }
       boolUPDATE_VARS = false;
       UpdateVars.boolMAINThreadWait = false;
       break;
      case 186:
       break;
      case 187:
       break;
      case 197:
       // Mutex semaphore error
       break;
      case 198:
       // Uncorrectable semaphore error ...Reboot
       break;
      case 199:                                           
       // A thread failed to initialize ...Reboot
      // exit(1);                                         //TBC 
       break;






// ALI SECTION.....................Used to Track and update ALI Messages.........................................................
//...............................................................................................................................
      case 220: case 222: case 225: case 226: case 231:
        // Update Position to that of Main Worktable

        intTrunk = objMessage.objCallData.intTrunk;
        if (CheckforManualBidTrunk(intTrunk)){break;}
        // Lock Work Table
        intRC = sem_wait(&sem_tMutexMainWorkTable);
        if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "b.sem_wait@sem_tMutexMainWorkTable in Main_Messaging_Monitor_Event()", 1);} 
        iTableIndex = IndexWithCallUniqueIDMainTable(objMessage.objCallData.stringUniqueCallID);
        if (iTableIndex >= 0)
         { 
          objMessage.objCallData = vMainWorkTable[iTableIndex].fCallData();
          objMessage.fUpdatePositionInMessageText();
         }
        sem_post(&sem_tMutexMainWorkTable);
        
        break;     
   
         
      case 256:
       //  special message to track ALI rebids (LOG_TYPE = 0 .. the log thread will ignore it)
       //  place the an object with trunk and message number into the MAIN message Q
       objData.CallData = objMessage.objCallData;      
       objData.intLogMessageNumber = 256;
       intRC = sem_wait(&sem_tMutexMainInputQ);
       if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainInputQ, "a.sem_wait@sem_tMutexMainInputQ in Main_Messaging_Monitor_Event()", 1);}
       queue_objMainData.push(objData);
       sem_post(&sem_tMutexMainInputQ);
       break;

      case 255: 
       //  ">[WARNING] ALI Record Fail on Database=%%%, Retry on Database=%%%" 
       //  place the an object with trunk and message number into the MAIN message Q
       intTrunk = objMessage.objCallData.intTrunk;
       objData.CallData.intTrunk = intTrunk;
       objData.intLogMessageNumber = 255;
       intRC = sem_wait(&sem_tMutexMainWorkTable);
       if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "b.sem_wait@sem_tMutexMainWorkTable in Main_Messaging_Monitor_Event()", 1);} 
       iTableIndex = IndexWithCallUniqueIDMainTable(objMessage.objCallData.stringUniqueCallID);
       if (iTableIndex >= 0) 
        {
         objMessage.objCallData = vMainWorkTable[iTableIndex].fCallData();
         objMessage.fUpdatePositionInMessageText();       
        }     
      
       sem_post(&sem_tMutexMainWorkTable);
       

       intRC = sem_wait(&sem_tMutexMainInputQ);
       if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainInputQ, "c.sem_wait@sem_tMutexMainInputQ in Main_Messaging_Monitor_Event()", 1);}
       queue_objMainData.push(objData);
       sem_post(&sem_tMutexMainInputQ);

       break;

     case 262:
       // "-[WARNING] ALI Bid Failed for %%%"
       if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "d.sem_wait@sem_tMutexMainWorkTable in Main_Messaging_Monitor_Event()", 1);} 
       iTableIndex = IndexWithCallUniqueIDMainTable(objMessage.objCallData.stringUniqueCallID);
       if (iTableIndex >= 0) 
        {
         Send_ALI_Fail_PopUp_To_Conference_List(iTableIndex);      
        }     
      
       sem_post(&sem_tMutexMainWorkTable);
       
       break;

// ANI SECTION............................................................................................................
//.........................................................................................................................
    
     case 366: case 367:
            //  "[INFO] Transfer from Phone Cancelled;\n %%%"
            objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fTransfer_Cancelled_PopUp_Message(), objMessage.objCallData);
            objData.intWorkStation = objMessage.objCallData.intPosn;
            objData.boolBroadcastXMLtoSinglePosition = true;
            Queue_WRK_Event(objData);
            break;
     case 368:
            //  "[WARNING] Transfer Cancelled, ext: %%% is currently connected " 
            objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fTransfer_Cancelled_PopUp_Message_Warning(), objMessage.objCallData);
            objData.intWorkStation = objMessage.objCallData.intPosn;
            objData.boolBroadcastXMLtoSinglePosition = true;
            Queue_WRK_Event(objData);
            break;
    
     case 365: case 373:
     //       // "[INFO]\nTransfer/Conference Dialed;\n (Destination=%%%)"
     //       objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fTransfer_Dialed_PopUp_Message());
     //       objData.intWorkStation = objMessage.objCallData.intPosn;
     //       objData.boolBroadcastXMLtoSinglePosition = true;
     //       Queue_WRK_Event(objData);
            break;
      case 314:
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fConf_Xfer_Unable_Valet_Lot_Message(),objMessage.objCallData);
           objData.intWorkStation = objMessage.objCallData.intPosn;
           objData.boolBroadcastXMLtoSinglePosition = true;
           Queue_WRK_Event(objData);          

          break;

    case 315:
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fConf_Xfer_Unable_Empty_Lot_Message(),objMessage.objCallData);
           objData.intWorkStation = objMessage.objCallData.intPosn;
           objData.boolBroadcastXMLtoSinglePosition = true;
           Queue_WRK_Event(objData);          

          break;
     case 381:
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fBlind_Xfer_Unable_PopUp_Message(),objMessage.objCallData);
           objData.intWorkStation = objMessage.objCallData.intPosn;
           objData.boolBroadcastXMLtoSinglePosition = true;
           Queue_WRK_Event(objData);          

          break;
     case 383:
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fConf_Xfer_Failed_PopUp_Message(),objMessage.objCallData);
           objData.intWorkStation = objMessage.objCallData.intPosn;
           objData.boolBroadcastXMLtoSinglePosition = true;
           Queue_WRK_Event(objData);          

          break;

// WARNING SECTION.........................................................................................................
//.........................................................................................................................
  
 

    case 143:
            //    "[WARNING]\nTransfer Blocked, Position not active on call"
            objData.stringXMLString = NotifyWorkstation.fCreateAlert(KRN_MESSAGE_143w, objMessage.objCallData);
            objData.intWorkStation = objMessage.objCallData.intPosn;
            objData.boolBroadcastXMLtoSinglePosition = true;
            Queue_WRK_Event(objData);
            break;

    case 376:
            //">[WARNING] No PBX gateway registered with Asterisk; Dial failed to %%%"
            objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fDial_Out_Failed_PopUp_Message(),objMessage.objCallData);
            objData.intWorkStation = objMessage.objCallData.intPosn;
            objData.boolBroadcastXMLtoSinglePosition = true;
            Queue_WRK_Event(objData);
            break;

// ALARM SECTION............................................................................................................
//..........................................................................................................................
 

  //    case 223: case 227: removed single port ali alarm pop up message
      case 263: case 264: case 321: case 332: case 419: case 464: case 609: case 610: case 720: case 721:
      //   "[ALARM] ALI 1A - Port Down for 3 minutes"
      //   "[ALARM] ANI 1 - Port Restored
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fPort_Down_Alarm_PopUp_Message(), objBLANK_CALL_RECORD);
           Queue_WRK_Event(objData);
           break;
/*
       case 230:
      //   "[ALARM] ALI 01 B < Broadcast Message Host Going Out of Service"
      //   "[ALARM] ALI 1B > Database going out of service"
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fALI_DB_OTS_PopUp_Message());
           Queue_WRK_Event(objData);
           break;
*/
      case 328: case 433:
      //   "[ALARM] ALI  01 B - Port Data Reception Inhibited for 3 minutes String Buffer Size > 1000000000"
      //   "[ALARM] ALI 1B – Port blocked for next 3 minutes due to excessive incoming data"
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fPort_ShutDown_Alarm_PopUp_Message(),objBLANK_CALL_RECORD);
           Queue_WRK_Event(objData);
           break;

      //case 313:
      //    "[ALARM] ANI 1 > Unable to determine which position answered the call for callback number (303) 818-3060” 
      //      objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fNo_Correlation_Alarm_PopUp_Message());
      //      Queue_WRK_Event(objData);
      //      break;

      //case 315:
      //   "[ALARM] ANI 1 > ANI-Link Permanent Seizure of ANI Trunk Card 1 (Code = C75)"
      //   "[ALARM] ANI 1 > ANI-Link Communication Lost to ANI Trunk Card 1 (Code = C71)"
      //   "[ALARM] ANI 1 > ANI-Link Communication Lost to ANI Display Card 1 (Code = C81)""              
      //      objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fANI_Alarm_Code_PopUp_Message());
     //       Queue_WRK_Event(objData);
      //      break;
     case 386: case 388:
       //  "[ALARM] %%% has Failed to Register for %%%; (Ping %%%)"
      //   "[ALARM] %%%; Communication Restored"
          objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fTel_Equipment_PopUp_Message(), objBLANK_CALL_RECORD);
          Queue_WRK_Event(objData);
          break;

// note any of the wrkstation alarms.......................
//................................................................................................................................................................
//................................................................................................................................................................

      case 463:
       // CAD UDP error..address already in use... Reboot
      
       break;

      case  740:
           // ">[WARNING] Unable to Rebid ALI, No URI in Table!" 
           objData.stringXMLString = NotifyWorkstation.fCreateAlert(objMessage.fUnable_to_Rebid_ALI_PopUp_Message(), objBLANK_CALL_RECORD);
           Queue_WRK_Event(objData);
           break;

      case 709: case 717: case 739:
           // UpdatePSAP status
           if (!boolShowPSAPstatusApplet) {break;}
           intRC = sem_wait(&sem_tMutexMainWorkTable);
           if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "b.sem_wait@sem_tMutexMainWorkTable in Main_Messaging_Monitor_Event()", 1);}
           objData.UpdatePSAPstatus();
           sem_post(&sem_tMutexMainWorkTable);
           Queue_WRK_Event(objData);                      
           break;

      case 999:
       // test area..................................
       
       
       break;	
      default:
       // just trap the kernel messages 100 series pass along all others....
       break;
       // cout << objMessage.intMessageCode << endl;                               //test code
   }// end switch

   // relock before iterate ....
   intRC = sem_wait(&sem_tMutexMainMessageQ);
   if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in Main_Messaging_Monitor_Event 2", 1);}
   intRC = sem_wait(&sem_tMutexLogMessageQ);
   if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexLogMessageQ, "sem_wait@sem_tMutexLogMessageQ in Main_Messaging_Monitor_Event 2", 1);}

    queue_objOutputMessageQ->push(objMessage);
    sem_post(&sem_tLogConsoleEmailFlag);
   
  }// end while
 sem_post(&sem_tMutexLogMessageQ);
 sem_post(&sem_tMutexMainMessageQ);

  // keep log moving.....
  sem_post(&sem_tLogConsoleEmailFlag);

  // check for how long beaconPort is/was down
  if (boolBEACON_ACTIVE){BeaconPort.UDP_Port.fCheck_Time_Port_Down();}

  if(boolNG_ALI)
   {
    ScanMainTableforNgALI();
   }


  Check_Abandoned_Calls(iLoopCount);

  Check_Disconnected_Calls(iLoopCount);

  Check_Stale_Calls(iLoopCount);

  Check_Time_in_Debug_Mode();

  Check_Single_Channel_Calls(iLoopCount);

  Check_MSRP_NO_ANSWER_Calls(iLoopCount);

  DailyFreeswitchTimeSync();
 
 // EmailDailyAlarmsSummary();

/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tHealthMonitorTimerId, 0, &itimerspecMainHealthMonitorDelay, NULL);}
*/
} while (!boolSTOP_EXECUTION);
 
 objMessage.fMessage_Create(0, 149, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_149);
 objMessage.fConsole();

return NULL;
}// end Main_Messaging_Monitor_Event()


/****************************************************************************************************
*
* Name:
*  Function: void Server_Beacon_Broadcast_Event(union sigval union_sigvalArg)
*
*
* Description:
*   An timed event handler within main() 
*
*
* Details:
*   this function is the implementation of a SIGEV_THREAD timer.  when fired it sends an XML heartbeat
*   message to the central server over the internet.
*
*
* Parameters: 
*   union_sigvalArg	                            not used
*
* Variables:
*   BEACON_IP                                   Global  - <IP_Address> object
*   BeaconPort                                  Global  - <ExperientCommPort> object
*   boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC         Global  - boolean for log and dislpay
*   boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC         Global  - boolean for log and dislpay
*   CLOCK_REALTIME                              Global  - <ctime> constant
*   EventNode                                   Local   - <XMLNode> XML class object
*   HeartbeatNode                               Local   - <XMLNode> XML class object
*   intBEACON_PORT_NUMBER                       Global  - integer Port number
*   intRC                                       Local   - return code
*   LOG_CONSOLE_FILE                            Global  - <defines.h> display to console and write to file
*   LOG_MESSAGE_531                             Global  - <defines.h> Log message template
*   MainNode                                    Local   - <XMLNode> XML class object 
*   NEXT_LINE                                   Global  - <header.h> <message_type> enumeration member
*   objBLANK_CALL_RECORD                        Global  - <CallData> blank CallData object 
*   objBLANK_KRN_PORT                           Global  - <PortData> KRN PortData object 
*   strMessage                                  Local   - <cstring>
*   .stringAddress                              Local   - <IP_Address> member <cstring> 
*   stringSERVER_HOSTNAME                       Global  - <globals.h>
*   strTimeStamp                                Local   - string conversion of timespecTimeStamp
*   timespecTimeStamp                           Local   - <struct timespec> Unix Time Stamp
*   .UDP_Port                                   Global  - <ExperientCommPort><UDPPort> object
*   XML_NODE_EVENT                              Global  - <defines.h>
*   XML_NODE_HEARTBEAT                          Global  - <defines.h>
*   XML_FIELD_VERSION                           Global  - <defines.h>
*   XML_FIELD_XML_ENCODING                      Global  - <defines.h>
*   XML_FIELD_SERVER_NAME                       Global  - <defines.h>
*   XML_FIELD_TRANSMIT_DATE_TIME                Global  - <defines.h>
*   XML_FIELD_VERSION_NUMBER                    Global  - <defines.h>
*   XML_FIELD_XML_ENCODING_VALUE                Global  - <defines.h>
*                                   
* Functions:
*   .addAttribute()                             Library <xmlParser>  
*   .addChild()                                 Library <xmlParser>
*   ASCII_String()                              Global  <globalfunctions.h> 
*   clock_gettime()                             Library <ctime>
*   .createXMLString()                          Library <xmlParser>
*   createXMLTopNode()                          Library <xmlParser>
*   .c_str()                                    Library <cstring>
*   .UDP_Port.Error_Handler                     Local   <ExperientCommPort>
*   .fSetPortActive()                           Local   <ExperientCommPort>
*   get_time_stamp()                            Global  <globalfunctions.h> 
*   int2str()                                   Global  <globalfunctions.h> 
*   .length()                                   Library <cstring>
*   .Send()                                     Library <ipworks>
*
*
* Author(s):
*   Bob McCarthy                        Original: 1/8/2009
*                                       Updated : 9/2/2022
*
****************************************************************************************************/ 
void Server_Beacon_Broadcast_Event(union sigval union_sigvalArg) {
 int                                            intRC;
 int                                            loop;
 float                                          floatData;
 long double                                    longdoubleData; 
 ostringstream                                  out;
 extern ExperientCommPort                       BeaconPort;
 extern Memory_Data                             MemoryData;
 extern vector <ExperientDataClass>             vMainWorkTable;
 ExperientDataClass                             objData;
 extern Trunk_Type_Mapping                      TRUNK_TYPE_MAP;
 extern Trunk_Sequencing                        Trunk_Map;
 string                                         strTimeStamp;
 string                                         strMessage;
 string	                                        strSystemStatus, strRA, strSA, strSV, strTkAlarm;
 MessageClass                                   objMessage;
 struct timespec                                timespecTimeStamp;
 XMLNode                                        MainNode;
 XMLNode                                        EventNode, HeartbeatNode;
 char*                                          cptrResponse = NULL;
 int                                            intNumALI, intNumSALI, intNumALSVC;
 int                                            intNumALIup, intNumSALIup, intNumALISVCup, intWRKreg;
 string                                         strData;
 ifstream                                       StratusStatusfile;
 struct stat                                    FileStat;
 extern int                                     intNUM_WRK_STATIONS;
 extern WorkStation                             WorkStationTable[NUM_WRK_STATIONS_MAX+1];
 extern Telephone_Devices                       TelephoneEquipment;
 extern Trunk_Sequencing                        Trunk_Map;
 extern ExperientCommPort                       ALIPort	[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort                       ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
 extern int                                     intNUM_ALI_PORT_PAIRS;
 extern int                                     intNUM_ALI_SERVICE_PORTS;
 extern bool                                    boolSTRATUS_SERVER_MONITOR;
 
 extern bool                                    Get_OS_Statistics();
 extern string                                  RCCstatusString();
 
 if (boolUPDATE_VARS)    { return;} //Skip this heartbeat
 if (boolSTOP_EXECUTION) { return;} 

 clock_gettime(CLOCK_REALTIME, &timespecTimeStamp);
 strTimeStamp = get_time_stamp(timespecTimeStamp);
 
 //execute Freeswitch Memory Usage Script
 MemoryData.fSendFSCLIrequest();

 // XML Header
 MainNode=XMLNode::createXMLTopNode("xml",TRUE);
 MainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
 MainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

  // GENERAL
 EventNode = MainNode.addChild(XML_NODE_EVENT);
 EventNode.addAttribute(XML_FIELD_PACKET_ID, "");
 EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);
 HeartbeatNode = EventNode.addChild(XML_NODE_HEARTBEAT);
 HeartbeatNode.addAttribute(XML_FIELD_SERVER_NAME, (char*)stringSERVER_HOSTNAME.c_str());
 HeartbeatNode.addAttribute(XML_FIELD_TRANSMIT_DATE_TIME, (char*) strTimeStamp.c_str());
 HeartbeatNode.addAttribute(XML_FIELD_CONTROLLER_VERSION, charVERSION_NUMBER);
// cout << "Stratus boolean -> " << boolSTRATUS_SERVER_MONITOR << endl;
 if (boolSTRATUS_SERVER_MONITOR) {
  FileStat.st_mtime = 0;
  // Stratus Status

  stat("/datadisk1/ng911/stratus/stratus.status.txt",&FileStat);
  if (!FileStat.st_mtime) {
   //sleep 5 second 
   for (int i = 0; i < 5; i++){
    experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
    if (boolUPDATE_VARS)    { return;} //Skip this heartbeat
    if (boolSTOP_EXECUTION) { return;} 
   }
   stat("/datadisk1/ng911/stratus/stratus.status.txt",&FileStat); 
  }

  if (FileStat.st_mtime) { 
   StratusStatusfile.open ("/datadisk1/ng911/stratus/stratus.status.txt",ios::in);
   if (StratusStatusfile.fail()) {
    strSystemStatus = "FTCONF";
   }
   else {
    getline(StratusStatusfile,strSystemStatus); 
   }
   StratusStatusfile.close();
   remove("/datadisk1/ng911/stratus/stratus.status.txt");
  }
  else {
   strSystemStatus = "FTCONF";
  }

  HeartbeatNode.addAttribute("ST", (char*) strSystemStatus.c_str());
 } // END if (boolSTRATUS_SERVER_MONITOR)


 if (boolUPDATE_VARS)    { return;} //Skip this heartbeat
 if (boolSTOP_EXECUTION) { return;} 

 intRC = sem_wait(&sem_tMutexWRKWorkTable);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in Server_Beacon_Broadcast_Event()", 1);}
  intWRKreg = 0;
  // Workstation Status 
 for (int i = 1; i <= intNUM_WRK_STATIONS; i++){
  if (WorkStationTable[i].boolActive) {
    intWRKreg++; 
  }
 }
 
 sem_post(&sem_tMutexWRKWorkTable);
 
 strData = int2strLZ(intWRKreg)+int2strLZ(intNUM_WRK_STATIONS);
 HeartbeatNode.addAttribute("W", (char*) strData.c_str());

 if (boolUPDATE_VARS)    { return;} //Skip this heartbeat
 if (boolSTOP_EXECUTION) { return;} 

 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in Server_Beacon_Broadcast_Event", 1);}
 
 // ALI Status
 strData = int2strLZ(Trunk_Map.fNumberOfAlarmingTrunks())+int2strLZ(Trunk_Map.fNumberOfMonitoredTrunks());
 HeartbeatNode.addAttribute("T", (char*) strData.c_str());

 intNumALI = intNumSALI = intNumALSVC = intNumALIup = intNumSALIup = intNumALISVCup = 0;

 for (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++) {
  if ((ALIPort[i][1].boolPhantomPort)||(ALIPort[i][2].boolPhantomPort)) {
   intNumSALI++;
   if ((!ALIPort[i][1].boolReducedHeartBeatModeFirstAlarm)&&(!ALIPort[i][1].boolPhantomPort)) {
    intNumSALIup++;
   }
   if ((!ALIPort[i][2].boolReducedHeartBeatModeFirstAlarm)&&(!ALIPort[i][2].boolPhantomPort)) {
    intNumSALIup++;
   }
  }
  else {
   intNumALI++;
   if (!ALIPort[i][1].boolReducedHeartBeatModeFirstAlarm) { 
    intNumALIup++;
   }
   if (!ALIPort[i][2].boolReducedHeartBeatModeFirstAlarm) { 
    intNumALIup++;
   } 
  }
 }
 
 for (int i = 1; i <= intNUM_ALI_SERVICE_PORTS; i++) {
  if (!ALIPort[i][3].boolReducedHeartBeatModeFirstAlarm) {  
   intNumALISVCup++;
  }
 }

 if (intNumSALI) {
  strData = int2strLZ(intNumSALI)+int2strLZ(intNumSALIup);
  HeartbeatNode.addAttribute("SA", (char*) strData.c_str());
 }

 if (intNumALI) {
  strData = int2strLZ(intNumALI)+int2strLZ(intNumALIup);
  HeartbeatNode.addAttribute("RA", (char*) strData.c_str());
 }

 if (intNUM_ALI_SERVICE_PORTS) {
  strData = int2strLZ(intNUM_ALI_SERVICE_PORTS)+int2strLZ(intNumALISVCup);
  HeartbeatNode.addAttribute("SV", (char*) strData.c_str());
 }

 //Telephone Status
 strData = TelephoneEquipment.fTelMonitorString(); 
 HeartbeatNode.addAttribute("PH", (char*) strData.c_str());
 // Audiocodes Status
 strData = TelephoneEquipment.fACMonitorString(); 
 HeartbeatNode.addAttribute("AC", (char*) strData.c_str()); 

 sem_post(&sem_tMutexMainWorkTable);

 strData = RCCstatusString();
 HeartbeatNode.addAttribute("RCC", (char*) strData.c_str() ); 
 
 // Get Controller Mem Stats
 Get_OS_Statistics();

 out.precision(1);
 out << fixed << MemoryData.OS_Memory_Usage;
 out << "," << fixed << MemoryData.OS_Swap_Usage;
 out << "," << fixed << MemoryData.Controller_APP_CPU_Usage;
 out << "," << fixed << MemoryData.Controller_APP_MEM_Usage;
 strData = out.str();
 HeartbeatNode.addAttribute("CT", (char*) strData.c_str() ); 
  
 
 // Check if FSW Memory Data updated ..... wait 3 loops if neccesary
 loop = 0;
 while ((!MemoryData.NewFreeswitchDataRX )&&(loop < 3)) {
  experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
  //  cout << "loop" << endl;
  loop++;
 }
 
 out.clear();
 out.str("");
 out.precision(1);
 out << fixed << MemoryData.Freeswitch_OS_Memory_Usage;
 out << "," << fixed << MemoryData.Freeswitch_OS_Swap_Usage;
 out << "," << fixed << MemoryData.Freeswitch_APP_CPU_Usage;
 out << "," << fixed << MemoryData.Freeswitch_APP_MEM_Usage;
 strData = out.str();
 HeartbeatNode.addAttribute("FS", (char*) strData.c_str() ); 
  
 MemoryData.fClear();
 
 
 cptrResponse = MainNode.createXMLString();
 if (cptrResponse != NULL)  {
   strMessage = cptrResponse;
   free(cptrResponse);
 }
 cptrResponse = NULL;

 intRC = BeaconPort.UDP_Port.Send(strMessage.c_str(), strMessage.length());
 if (intRC){BeaconPort.UDP_Port.Error_Handler();}
 else {
   BeaconPort.fSetPortActive();
   if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC))  {
     objMessage.fMessage_Create(LOG_CONSOLE_FILE, 163, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                                objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LOG_MESSAGE_531, BEACON_IP.stringAddress + ":"+int2str(intBEACON_PORT_NUMBER), 
                                ASCII_String((char*)strMessage.c_str(), strMessage.length()),"","","","", DEBUG_MSG, NEXT_LINE);
      enQueue_Message(MAIN,objMessage);
   }// end if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC))
   
 } //end if (intRC) else
  
}

void Telephone_Monitor_Timer_Event(union sigval union_sigvalArg) {
    
extern Telephone_Devices                TelephoneEquipment;
extern bool                             boolPBX_SHUT_DOWN;
extern unsigned long int                intTelephoneConsecutivePingFailures;
extern unsigned long int                intTelephonePingFailureThreshold;
extern unsigned long int                intTelephonePingSuccessThreshold;
extern vector <int>                     vTELEPHONE_PING_LIST;
extern int                              intTELEPHONE_PING_INTERVAL_SEC;
Ping                                    PingPort;
int                                     intRC;
bool                                    boolPingSuccess = false;
string                                  strIPaddress;
string                                  strMessage;
string                                  strReturnString;
MessageClass                            objMessage;

#ifdef IPWORKS_V16
 PingPort.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 PingPort.SetLocalHost(NULL);
 PingPort.SetTimeout(intPING_TIMEOUT);
 PingPort.SetPacketSize(1024);
 PingPort.Config((char*)"AbsoluteTimeout=true");

 for (unsigned int i = 0; i < vTELEPHONE_PING_LIST.size(); i++)
  {
   strIPaddress = TelephoneEquipment.fIPaddressFromPositionNumber(vTELEPHONE_PING_LIST[i]);
   if (strIPaddress.empty()) {continue;}
   intRC = PingPort.PingHost((char*)strIPaddress.c_str());
   if  (intRC) {continue;}
   else        {boolPingSuccess = true; break;}
  }

 switch (boolPingSuccess) 
  {
   case true:

        switch (boolPBX_SHUT_DOWN)
         {
          case true:
               if (intTelephoneConsecutivePingSuccesses > intTelephonePingSuccessThreshold)
                {
                 // start PBX 
                 strReturnString = MonitCommandLine("freeswitch", "start");
                 intTelephoneConsecutivePingSuccesses = 0;
                 boolPBX_SHUT_DOWN = false;
                 strMessage = seconds2time(intTELEPHONE_PING_INTERVAL_SEC*intTelephoneConsecutivePingFailures);
                 objMessage.fMessage_Create(LOG_ALARM, 168, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                                            objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_168, strMessage); 
                 enQueue_Message(MAIN,objMessage);
                }
               else
                {
                 intTelephoneConsecutivePingSuccesses++;
                }
               break;
          case false:
               // OK Normal Operation;
               intTelephoneConsecutivePingFailures = 0;
               break;
         }        
        break;

   case false:
        intTelephoneConsecutivePingSuccesses = 0;
        switch (boolPBX_SHUT_DOWN)
         {
          case true:
               intTelephoneConsecutivePingFailures++;
               // PBX is already shut down ...
               break;

          case false:
               if (intTelephoneConsecutivePingFailures > intTelephonePingFailureThreshold)
                {
                 strReturnString = MonitCommandLine("freeswitch", "stop");
                 boolPBX_SHUT_DOWN = true;
                 objMessage.fMessage_Create(LOG_ALARM, 167, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                                            objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_167, int2str(intTelephoneConsecutivePingFailures)); 
                 enQueue_Message(MAIN,objMessage);
                }
               else
                {
                 intTelephoneConsecutivePingFailures++;
                }
               break;
         }

        break;
  }


}

/****************************************************************************************************
*
* Name:
*  Function: Start_Thread()
*
*
* Description:
*  function which creates the CAD thread
*
*
* Details:
*   this function creates the thread and returns 0 if successful if not an abnormal exit is performed
*
*
* Parameters: 				
*   none
*
* Variables:
*   intRC                               Local   return code
*   objMessage                          Class   <MessageClass>
*   objEmailMsg                         Global  <ExperientEmail>
*                                                                      
* Functions:
*   Abnormal_Exit()			Global  <globalfunctions.h> 
*   enQueue_Message()                   Global  <globalfunctions.h> 
*   .fMessage_Create()                  Class   <MessageClass>
*   pthread_create()			Library <pthread.h> 
*
*
* Author(s):
*   Bob McCarthy 			Original: 2/1/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
int Start_Thread(threadorPorttype enumThread)
{
 int                    intRC = 0;
 MessageClass           objMessage;

 switch (enumThread)
  {
   case CAD:
    intRC = pthread_create(&pthread_tCadThread, &pthread_attr_tAttr, *Cad_Thread, (void *) &queue_objCadData);
    if (intRC){Abnormal_Exit(CAD, EX_OSERR, CAD_MESSAGE_496);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,106, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_106); 
    enQueue_Message(MAIN,objMessage);
    break;

   case ALI:
    intRC = pthread_create(&pthread_tALiThread, &pthread_attr_tAttr, *ALI_Thread, (void *) &queue_objAliData);
    if (intRC){Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_201);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,107, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                               objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_107); 
    enQueue_Message(MAIN,objMessage);
    break;

   case ANI:
    intRC = pthread_create(&pthread_tANIThread, &pthread_attr_tAttr, *ANI_Thread, (void *) &queue_objANIData);
    if (intRC){Abnormal_Exit(ANI, EX_OSERR, ANI_MESSAGE_301);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,108, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_KRN_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_108); 
    enQueue_Message(MAIN,objMessage);
    break;

   case WRK:
    intRC = pthread_create(&pthread_tWRKThread, &pthread_attr_tAttr, *WRK_Thread, (void *) &queue_objWRKData);
    if (intRC){Abnormal_Exit(WRK, EX_OSERR, WRK_MESSAGE_701);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,125, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                               objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_125); 
    enQueue_Message(MAIN,objMessage);
    break;

   case LOG:
    intRC = pthread_create(&pthread_tLogConsoleEmailThread, &pthread_attr_tAttr, *Log_Console_Email_Thread, (void *) &queue_objOutputMessageQ);
    if (intRC) {Abnormal_Exit(LOG, EX_OSERR, LOG_MESSAGE_597);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,103,__LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                               objBLANK_KRN_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_103); 
    enQueue_Message(MAIN,objMessage);
    break;

   case RCC:
    intRC = pthread_create(&pthread_tRCCThread, &pthread_attr_tAttr, *RCC_Thread, NULL);
    if (intRC) {Abnormal_Exit(RCC, EX_OSERR, RCC_MESSAGE_901);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,109, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                               objBLANK_RCC_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_109); 
    enQueue_Message(MAIN,objMessage);
    break;

   case CTI:
    intRC = pthread_create(&pthread_tCTIThread, &pthread_attr_tAttr, *CTI_Thread, NULL);
    if (intRC) {Abnormal_Exit(CTI, EX_OSERR, CTI_MESSAGE_601);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,109, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_RCC_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_109); 
    enQueue_Message(MAIN,objMessage);
    break;
   

   case AMI:
    intRC = pthread_create(&pthread_tAMIThread, &pthread_attr_tAttr, *AMI_Thread, NULL);
    if (intRC) {Abnormal_Exit(AMI, EX_OSERR, AMI_MESSAGE_801);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,113, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_AMI_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_113); 
    enQueue_Message(MAIN,objMessage);
    break;

   case LIS:
    intRC = pthread_create(&pthread_tLISThread, &pthread_attr_tAttr, *LIS_Thread, NULL);
    if (intRC) {Abnormal_Exit(LIS, EX_OSERR, AMI_MESSAGE_801);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,114, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_LIS_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_114); 
    enQueue_Message(MAIN,objMessage);
    break;

   case NFY:
    intRC = pthread_create(&pthread_tNFYThread, &pthread_attr_tAttrJoinable, *NFY_Thread, NULL);
    if (intRC) {Abnormal_Exit(NFY, EX_OSERR, AMI_MESSAGE_801);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,115, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_NFY_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_115); 
    enQueue_Message(MAIN,objMessage);
    break;

   case DSB:
    intRC = pthread_create(&pthread_tDSBThread, &pthread_attr_tAttrJoinable, *DSB_Thread, NULL);
    if (intRC) {Abnormal_Exit(NFY, EX_OSERR, AMI_MESSAGE_801);}

    objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,115, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objBLANK_DSB_PORT,  objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_115a); 
    enQueue_Message(MAIN,objMessage);
    break;

   default:
   SendCodingError( "ExperientController.cpp - coding error in Start_Thread");
   break;
  }//end switch
 
return intRC;

}//end Start_Thread()

int Start_Timed_Threads()
{
 int intRC;

 intRC = pthread_create(&pthread_tMainMessagingThread, &pthread_attr_tAttr, *Main_Messaging_Monitor_Event, (void *) &queue_objOutputMessageQ);
 if (intRC) {Abnormal_Exit(MAIN, EX_OSERR, AMI_MESSAGE_801);}

 intRC = pthread_create(&pthread_tMainScoreboardThread, &pthread_attr_tAttr, *Main_ScoreBoard_Event, NULL);
 if (intRC) {Abnormal_Exit(MAIN, EX_OSERR, AMI_MESSAGE_801);}



 return 0;
}
/****************************************************************************************************
*
* Name:
*  Function: int Process_CFG_File()
*
*
* Description:
*  function used to perform .CFG configuration
*
*
* Details:
*   this function uses the Library <SimpleIni.h> to load the INI file and assign values to configurable 
*   variables from each thread. if a value is not of the correct type, an error is generated and program execution
*   is terminated
*
*
* Parameters: 				
*   none                             
*
* Variables:
*   bool_arrayCADPORTACKREQUIRED        Global  Configurable Variable from CAD
*   bool_arrayHEARTBEATCADPORT          Global  Configurable Variable from CAD
*   CAD_LOCAL_PORT                      Global  Configurable Variable from CAD
*   CAD_PORT_REMOTE_HOST                Global  Configurable Variable from CAD
*   charINI_FILE_PATH_AND_NAME          Global  <defines.h>

*   DEFAULT_VALUE_KRN_INI_KEY_1         Global  <defines.h>
*   DEFAULT_VALUE_KRN_INI_KEY_2         Global  <defines.h>
*   DEFAULT_VALUE_KRN_INI_KEY_3         Global  <defines.h>
*   DEFAULT_VALUE_LOG_INI_KEY_1         Global  <defines.h>
*   DEFAULT_VALUE_LOG_INI_KEY_2         Global  <defines.h>
*   DEFAULT_VALUE_LOG_INI_KEY_3         Global  <defines.h>
*   DEFAULT_VALUE_LOG_INI_KEY_4         Global  <defines.h>
*   DEFAULT_VALUE_LOG_INI_KEY_5         Global  <defines.h>
*   DEFAULT_VALUE_LOG_INI_KEY_6         Global  <defines.h>
*   HOST_IP                             Global  <IP_Address> object
*   ini()                               Local   <SimpleIni.h><CSimpleIniA> object
*   intIP_ADDRESS_LENGTH                Global  <defines.h>
*   intRC                               Local   return code from functions
*   intPSAP_NAME_LENGTH                 Global  <defines.h>
*   j                                   Local   integer counter
*   KRN_INI_KEY_1                       Global  <defines.h>
*   KRN_INI_KEY_2                       Global  <defines.h>
*   KRN_INI_KEY_3                       Global  <defines.h>
*   LOG_INI_KEY_1                       Global  <defines.h>
*   LOG_INI_KEY_2                       Global  <defines.h>
*   LOG_INI_KEY_3                       Global  <defines.h>
*   LOG_INI_KEY_4                       Global  <defines.h>
*   LOG_INI_KEY_5                       Global  <defines.h>
*   LOG_INI_KEY_6                       Global  <defines.h>
*   NUM_CAD_PORTS_MAX                   Global  <defines.h>
*   rc                                  Local   <SimpleIni.h><SI_Error> return code
*   stringAddress                       Global  <IP_Address> member
*   stringPSAP_Name                     Global  
*                                                              
* Functions:
*   char2int()                          Local
*   exit()                              Library <cstdlib>
*   .GetValue()                         Library <SimpleIni.h><CSimpleIniA>
*   .LoadFile()                         Library <SimpleIni.h><CSimpleIniA>
*   strlen()                            Library <cstring>
*
* Author(s):
*   Bob McCarthy 			Original: 3/17/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
int Process_CFG_File()
{
    int         i;
    int         j;
    int         iCount;
    int         index;
    int         intRC;
    int         iGUInumberOfRows;
    long int    iRecordAgeOut;
    char        NumericAddress[32];
    bool        boolLoadSuccess;
    bool	boolA,boolB;
    string      StringData;
    string      StringDataB;
    string      StringDataC;
    string      StringDataD;
    string      Ali_Key;
    string      CAD_Key;
    string      ANI_Key;
    string      WRK_Key;
    string      WRK_KeyB;
    string      WRK_KeyC;
    string      WRK_KeyD;
    string      Phones_Key;
    string      Line_Key;
    string      Label_Key;
    string      strDISPLAY_MODE;
    string      strTrueFalse;
    string      strMSRPtextMessage;
    size_t      found;
    GUI_Line_View objLineView;
    PhoneBookEntry objPhoneRecord;



    XMLNode     MainNode;
    XMLNode     EventNode, InitializationNode, GeneralNode, GeneralSetupNode, ButtonSetupNode, AppletSetupNode, ServerNode, ServerSetupNode, SIPphoneNode, SIPphoneSetupNode;
    XMLNode     TDDnode, TDDsetupNode, TDDmessageListNode, TDDmessageNode, PhoneBookNode, PhoneBookSetupNode, PhonebookListNode, PhonebookEntryNode, SpeedDialNode;
    XMLNode     SpeedDialSetupNode, SpeedDialListNode, SpeedDialToNode, TabSetupNode, LineViewNode, LineViewRowNode;
    XMLNode     MSRPnode, MSRPsetupNode, MSRPmessageListNode, MSRPmessageNode, RingAppletNode, RingToneNode, RingImageNode;

    extern ExperientCommPort                    ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
    extern ExperientCommPort                    ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
    extern ALISteering                          ALI_Steering[intMAX_ALI_DATABASES + 1 ];
    extern ExperientCommPort                    ANIPort[NUM_ANI_PORTS_MAX+1];

    // forward function declaration
 
    int         LoadPortData( int intArg, string charArg , string stringArg, threadorPorttype enumThread );
    int         LoadPortConnectionType( int intArg, string charArg , string stringArg, threadorPorttype enumThread );
    int         LoadALIPortProtocolType( int intArg, string stringArg);

    // load data fron CFG file
    CSimpleIniA ini(true, true, true);
    SI_Error rc = ini.LoadFile(charINI_FILE_PATH_AND_NAME);
    if (rc < 0) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_003, charINI_FILE_PATH_AND_NAME);}
    CSimpleIniA::TNamesDepend::const_iterator l,m,n,o,p,q,r,s,t,u,v,w,w1,w2;
    // Get Kernel INI Variables
    stringPSAP_Name                     = ini.GetValue("GENERAL", KRN_INI_KEY_3, DEFAULT_VALUE_KRN_INI_KEY_3 );
    stringPRODUCT_NAME                  = ini.GetValue("GENERAL", KRN_INI_KEY_5, DEFAULT_VALUE_KRN_INI_KEY_5 );
    stringCOMPANY_NAME                  = ini.GetValue("GENERAL", KRN_INI_KEY_6, DEFAULT_VALUE_KRN_INI_KEY_6 );    
    stringDEFAULT_GATEWAY_DEVICE        = ini.GetValue("GENERAL", KRN_INI_KEY_1, DEFAULT_VALUE_KRN_INI_KEY_1 );  
    stringSYNC_ETHERNET_DEVICE          = ini.GetValue("GENERAL", KRN_INI_KEY_7, DEFAULT_VALUE_KRN_INI_KEY_7 ); 
    stringPSAP_ID			= ini.GetValue("GENERAL", KRN_INI_KEY_15, DEFAULT_VALUE_KRN_INI_KEY_15 );
    strCUSTOMER_SUPPORT_NUMBER          = ini.GetValue("GENERAL", KRN_INI_KEY_25, DEFAULT_VALUE_KRN_INI_KEY_25 );
    strABOUT_SPLASH_FILE                = ini.GetValue("GENERAL", KRN_INI_KEY_26, DEFAULT_VALUE_KRN_INI_KEY_26 );
    strABOUT_SPLASH_COLOR               = ini.GetValue("GENERAL", KRN_INI_KEY_27, DEFAULT_VALUE_KRN_INI_KEY_27 );
    strABOUT_SPLASH_FONT                = ini.GetValue("GENERAL", KRN_INI_KEY_28, DEFAULT_VALUE_KRN_INI_KEY_28 );
    strABOUT_SPLASH_FONT_PT             = ini.GetValue("GENERAL", KRN_INI_KEY_29, DEFAULT_VALUE_KRN_INI_KEY_29 );

    // 2.7.1 for backwards capability these will still be loaded in GENERAL but also in  LOG
    intAVAIL_DISK_SPACE_WARN_GIG        = char2int(ini.GetValue("GENERAL", KRN_INI_KEY_8, DEFAULT_VALUE_KRN_INI_KEY_8 ));
    intMONTHS_TO_KEEP_LOG_FILES         = char2int(ini.GetValue("GENERAL", KRN_INI_KEY_22, DEFAULT_VALUE_KRN_INI_KEY_22 ));

    // 2.7.1 if KRN defaults detected,  try reading in LOG
    if ( intAVAIL_DISK_SPACE_WARN_GIG == char2int(DEFAULT_VALUE_KRN_INI_KEY_8) )
     { intAVAIL_DISK_SPACE_WARN_GIG = char2int(ini.GetValue("LOG", LOG_INI_KEY_4, DEFAULT_VALUE_LOG_INI_KEY_4 ));}
    if ( intMONTHS_TO_KEEP_LOG_FILES == char2int(DEFAULT_VALUE_KRN_INI_KEY_22))
     { intMONTHS_TO_KEEP_LOG_FILES  = char2int(ini.GetValue("LOG", LOG_INI_KEY_5, DEFAULT_VALUE_LOG_INI_KEY_5 ));}    
    
    BEACON_IP.stringAddress             = ini.GetValue("GENERAL", KRN_INI_KEY_16, DEFAULT_VALUE_KRN_INI_KEY_16 );
    strESME_ID_STRING                   = ini.GetValue("GENERAL", KRN_INI_KEY_23, DEFAULT_VALUE_KRN_INI_KEY_23 );
    if (strESME_ID_STRING.length() > E2_MAX_COMPANY_ID_STRING_LENGTH) 
     {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_059, strESME_ID_STRING ); } 
    strCONFIG_FILE_VERSION              = ini.GetValue("GENERAL", KRN_INI_KEY_24, DEFAULT_VALUE_KRN_INI_KEY_24 );

    if(BEACON_IP.stringAddress == DEFAULT_VALUE_KRN_INI_KEY_16) {boolBEACON_ACTIVE = false;}
    else
     {
      if (!BEACON_IP.fIsValid_IP_Address()){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_034, BEACON_IP.stringAddress );}
      boolBEACON_ACTIVE = true;
      intBEACON_PORT_NUMBER    = char2int(ini.GetValue("GENERAL", KRN_INI_KEY_17, DEFAULT_VALUE_KRN_INI_KEY_17 ));
     } 
 
     // Load Server IP Data
     i = 1;
     do
      {
       if (i > NUM_OF_SERVERS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_033, int2str(i));}
       Ali_Key = Create_INI_Key("Server", i, "IP", 0 , "Address" );
       StringData = ini.GetValue("GENERAL", Ali_Key.c_str(), "" );
       if (StringData  == "") {continue;}
       ServerStatusTable[i].Gateway_IP.stringAddress = StringData;
       if (!ServerStatusTable[i].Gateway_IP.fIsValid_IP_Address()){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_034, ServerStatusTable[i].Gateway_IP.stringAddress );}
       i++;
    
      }while ( StringData != ""); 
     intNUM_OF_SERVERS = (i - 1);

     if (intNUM_OF_SERVERS == 1){boolSINGLE_SERVER_MODE = true; boolIsMaster = true;}
     else			{boolSINGLE_SERVER_MODE = true; boolIsMaster = false;}

     intSERVER_NUMBER = findServerNumber(get_ip_address(stringDEFAULT_GATEWAY_DEVICE));
     if (intSERVER_NUMBER == 0){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_049, stringDEFAULT_GATEWAY_DEVICE, get_ip_address(stringDEFAULT_GATEWAY_DEVICE) );}
    
     HOST_IP = ServerStatusTable[intSERVER_NUMBER].Gateway_IP;
   
    MULTICAST_GROUP.stringAddress = ini.GetValue("GENERAL", KRN_INI_KEY_18, DEFAULT_VALUE_KRN_INI_KEY_18 );
    if (MULTICAST_GROUP.stringAddress == DEFAULT_VALUE_KRN_INI_KEY_18) {boolMULTICAST = false;}
    else                                                               {boolMULTICAST = true;}

  
  if (!boolSINGLE_SERVER_MODE)
   { 
     //Create Server Name
     StringData = stringPSAP_ID + "-" + int2str(intSERVER_NUMBER);
     ServerStatusTable[intSERVER_NUMBER].ServerName = StringData;
     if (ServerStatusTable[intSERVER_NUMBER].ServerName != stringSERVER_HOSTNAME)
      { Console_Message("CFG |", CFG_MESSAGE_038, stringSERVER_HOSTNAME, ServerStatusTable[intSERVER_NUMBER].ServerName); exit(1); }

     // Load Server Sync IP Data
     i = 1;
     do
      {
       if (i > NUM_OF_SERVERS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_033, int2str(i));}
       Ali_Key = Create_INI_Key("Server", i, "Sync", 0, "IP", 0 , "Address" );
       StringData = ini.GetValue("GENERAL", Ali_Key.c_str(), "" );
       if (StringData  == "") {continue;}
       ServerStatusTable[i].Sync_IP.stringAddress = StringData;
       
       if (!ServerStatusTable[i].Sync_IP.fIsValid_IP_Address()){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_034, ServerStatusTable[i].Sync_IP.stringAddress );}

       i++;
    
      }while ( StringData != "");
     SYNC_IP =  ServerStatusTable[intSERVER_NUMBER].Sync_IP;

     
 
    // Load Server Name Data
    i = 1;
    do
     {
   //   if (i == intSERVER_NUMBER){ i++; continue;}
      ServerStatusTable[i].ServerName = stringPSAP_ID + "-" + int2str(i);
      i++;
    
     }while ( i <= intNUM_OF_SERVERS);

 //   intMASTER_SLAVE_SYNC_PORT_NUMBER    = char2int(ini.GetValue("GENERAL", KRN_INI_KEY_9, DEFAULT_VALUE_KRN_INI_KEY_9 ));
 //   intSYC_PORT_DOWN_THRESHOLD_SEC = char2int(ini.GetValue("GENERAL", KRN_INI_KEY_11, DEFAULT_VALUE_KRN_INI_KEY_11 ));
 //   intSYC_PORT_DOWN_REMINDER_SEC  = char2int(ini.GetValue("GENERAL", KRN_INI_KEY_12, DEFAULT_VALUE_KRN_INI_KEY_12 ));

   }// end if if (!boolSINGLE_SERVER_MODE)
  
    // load NPD Data   
    for (int x = 0; x < 8 ; x++){NPD[x] = "000";}

    i = 0;
    do
     {
      if (i == 0) {Ali_Key = "NPD0";}
      else        {Ali_Key = Create_INI_Key("NPD", i);}
      StringData = ini.GetValue("GENERAL", Ali_Key.c_str(), "" );
      if (StringData  == "") {continue;}
      if (i > 7 ){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_031, int2str(i) );}
      NPD[i] = StringData; 
      i++;
      
     }while ( StringData != "");   

/*
  
   // Get General Email INI Variables 2.7.1 Read from KRN, if default read from LOG
   stringEMAIL_SEND_TO = ini.GetValue("GENERAL", KRN_INI_KEY_20, DEFAULT_VALUE_KRN_INI_KEY_20);
   if (stringEMAIL_SEND_TO == DEFAULT_VALUE_KRN_INI_KEY_20) {stringEMAIL_SEND_TO = ini.GetValue("LOG", LOG_INI_KEY_2, DEFAULT_VALUE_LOG_INI_KEY_2); }    
   if (stringEMAIL_SEND_TO == DEFAULT_VALUE_LOG_INI_KEY_2) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_043, LOG_INI_KEY_2, charINI_FILE_PATH_AND_NAME ); } 
   charEMAIL_SEND_TO   = (char*) stringEMAIL_SEND_TO.c_str();

   stringEMAIL_FROM = ini.GetValue("ADVANCED", ADV_INI_KEY_9, DEFAULT_VALUE_ADV_INI_KEY_9);
   if (stringEMAIL_FROM == DEFAULT_VALUE_ADV_INI_KEY_9)
    {
     found = stringEMAIL_SEND_TO.find("@");
     if (found == string::npos) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_061, stringEMAIL_SEND_TO ); }
     StringData = stringSERVER_HOSTNAME + stringEMAIL_SEND_TO.substr(found, string::npos);
    }
   else
    {
     StringData = stringSERVER_HOSTNAME + "@" + stringEMAIL_FROM;
    }

   stringEMAIL_FROM = "\""+stringPSAP_Name+"\"<"+StringData+">";
   charEMAIL_FROM   = (char*) stringEMAIL_FROM.c_str();

   
   StringData = ini.GetValue("ADVANCED", ADV_INI_KEY_10, DEFAULT_VALUE_ADV_INI_KEY_10 );
   if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_084, StringData );}
   intALARM_POPUP_DURATION_MS = char2int(StringData.c_str());

   StringData = ini.GetValue("ADVANCED", ADV_INI_KEY_11, DEFAULT_VALUE_ADV_INI_KEY_11 );
   if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_084, StringData );}
   intTCP_RECONNECT_RECURSION_LEVEL = char2int(StringData.c_str());   

   StringData = ini.GetValue("ADVANCED", ADV_INI_KEY_12, DEFAULT_VALUE_ADV_INI_KEY_12 );
   if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_084, StringData );}
   intTCP_RECONNECT_INTERVAL_SEC = char2int(StringData.c_str()); 



   //2.7.1 if KRN defaults detected read from LOG
   stringEMAIL_SEND_TO_ALARM         = ini.GetValue("GENERAL", KRN_INI_KEY_21, DEFAULT_VALUE_KRN_INI_KEY_21 );
   if (stringEMAIL_SEND_TO_ALARM == DEFAULT_VALUE_KRN_INI_KEY_21)
    {stringEMAIL_SEND_TO_ALARM         = ini.GetValue("LOG", LOG_INI_KEY_3, DEFAULT_VALUE_LOG_INI_KEY_3 ); }

   charEMAIL_SEND_TO_ALARM           = (char*) stringEMAIL_SEND_TO_ALARM.c_str();
   boolEMAIL_MONTHLY_LOGS            = false;
*/
 
 //  cout << stringEMAIL_FROM << endl;

// 2.7.1
// Get ECATS IP Address and port
       StringData         = ini.GetValue("LOG", LOG_INI_KEY_1, DEFAULT_VALUE_LOG_INI_KEY_1 );
       if (StringData == DEFAULT_VALUE_LOG_INI_KEY_1) {BOOL_SEND_ECATS = false;}
       else 
        { 
         ECATS_IP.stringAddress = StringData;
         if (!ECATS_IP.fIsValid_IP_Address()){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_078, ECATS_IP.stringAddress );}
         StringData = ini.GetValue("LOG", LOG_INI_KEY_6, DEFAULT_VALUE_LOG_INI_KEY_6 );
         if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_079, StringData );}
         if (char2int(StringData.c_str()) > 65535) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_079, StringData );}
         if (char2int(StringData.c_str()) < 49152) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_079, StringData );}
         ECATS_PORT_NUMBER = char2int(StringData.c_str());
         StringData = ini.GetValue("LOG", LOG_INI_KEY_7, DEFAULT_VALUE_LOG_INI_KEY_7 );
         if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_080, StringData );}         
         ECATS_ALARM_DELAY_SEC    = char2int(StringData.c_str());
         StringData = ini.GetValue("LOG", LOG_INI_KEY_8, DEFAULT_VALUE_LOG_INI_KEY_8 );
         if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_081, StringData );} 
         ECATS_ALARM_REMINDER_SEC = char2int(StringData.c_str());
         StringData = ini.GetValue("LOG", LOG_INI_KEY_9, DEFAULT_VALUE_LOG_INI_KEY_9 );         
         if      ( StringData == "TCP" ) { ECATS_PORT_CONNECTION_TYPE = TCP;}
         else if ( StringData == "tcp" ) { ECATS_PORT_CONNECTION_TYPE = TCP;}
         else                            { ECATS_PORT_CONNECTION_TYPE = UDP;}
         StringData = ini.GetValue("LOG", LOG_INI_KEY_10, DEFAULT_VALUE_LOG_INI_KEY_10 );
         if (!Validate_Integer(StringData))        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_082, StringData );}
         ECATS_MSG_BUFFER_SIZE    = char2int(StringData.c_str());                  
         BOOL_SEND_ECATS = true; 
        }
       if(!char2bool(boolJSON_LOGGING, ini.GetValue("LOG", LOG_INI_KEY_11, DEFAULT_VALUE_LOG_INI_KEY_11 )))
        {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolJSON_LOGGING" );}

   // Get LIS INI Variables
   if(!char2bool(boolNG_ALI, ini.GetValue("LIS", LIS_INI_KEY_1, DEFAULT_VALUE_LIS_INI_KEY_1 )))
       {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolNG_ALI" );}
   strMSRP_LIS_SERVER               = ini.GetValue("LIS", LIS_INI_KEY_2, DEFAULT_VALUE_LIS_INI_KEY_2 );
   if(!char2bool(boolMSRP_LIS_USE_POST_ONLY, ini.GetValue("LIS", LIS_INI_KEY_3, DEFAULT_VALUE_LIS_INI_KEY_3 )))
       {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolMSRP_LIS_USE_POST_ONLY" );}

   strNG911_LIS_SERVER              = ini.GetValue("LIS", LIS_INI_KEY_4, DEFAULT_VALUE_LIS_INI_KEY_4 );
   if(!char2bool(boolNG911_LIS_USE_POST_ONLY, ini.GetValue("LIS", LIS_INI_KEY_5, DEFAULT_VALUE_LIS_INI_KEY_5 )))
       {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolNG911_LIS_USE_POST_ONLY" );}

   if(!char2bool(boolSHOW_RAPID_LITE_BUTTON, ini.GetValue("LIS", LIS_INI_KEY_6, DEFAULT_VALUE_LIS_INI_KEY_6 )))
       {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_RAPID_LITE_BUTTON" );}
 


  
   StringData = ini.GetValue("LIS", LIS_INI_KEY_7, DEFAULT_VALUE_LIS_INI_KEY_7 );
   if      (StringData == XML_FIELD_NEXTGEN) { enumI3CONVERSION_TYPE = NEXTGEN;}
   else if (StringData == XML_FIELD_ALI30X)  { enumI3CONVERSION_TYPE = ALI30X;}
   else                                      { enumI3CONVERSION_TYPE = ALI30W;}   

   strLIS_CERTIFICATE = ini.GetValue("LIS", LIS_INI_KEY_8, DEFAULT_VALUE_LIS_INI_KEY_8 );

   strNG911_ECRF_SERVER = ini.GetValue("LIS", LIS_INI_KEY_9, DEFAULT_VALUE_LIS_INI_KEY_9 ); 

   strNG911_SOS_SERVICE_BID = ini.GetValue("LIS", LIS_INI_KEY_10, DEFAULT_VALUE_LIS_INI_KEY_10 );

   strECRF_CERTIFICATE =  ini.GetValue("LIS", LIS_INI_KEY_11, DEFAULT_VALUE_LIS_INI_KEY_11 ); 


   if(!char2bool(boolUSE_URI_LEGACY_ALI, ini.GetValue("LIS", LIS_INI_KEY_12, DEFAULT_VALUE_LIS_INI_KEY_12 )))
       {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolUSE_URI_LEGACY_ALI" );}

   intLIS_HTTP_TIMEOUT_SEC  = char2int(ini.GetValue("LIS", LIS_INI_KEY_13, DEFAULT_VALUE_LIS_INI_KEY_13 )); 
   intECRF_HTTP_TIMEOUT_SEC = char2int(ini.GetValue("LIS", LIS_INI_KEY_14, DEFAULT_VALUE_LIS_INI_KEY_14 )); 

   //Get ADV init vars
   if (!INIT_VARS.fLoadADVvariables(INIT_VARS.ADVinitVariable)) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_086);}
   INIT_VARS.ADVinitVariable.Set_GLobal_VARS();

   //Get ALI init vars
   if (!INIT_VARS.fLoadALIvariables(INIT_VARS.ALIinitVariable)) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_002);}
   INIT_VARS.ALIinitVariable.Set_GLobal_VARS();

   //Get ANI init vars
   if (!INIT_VARS.fLoadANIvariables(INIT_VARS.ANIinitVariable)) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_050);}
   INIT_VARS.ANIinitVariable.Set_GLobal_VARS();

   //Get DSB init vars
   if (!INIT_VARS.fLoadDSBvariables(INIT_VARS.DSBinitVariable)) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_085);}
   INIT_VARS.DSBinitVariable.Set_GLobal_VARS();

   //Get CAD init vars
   if (!INIT_VARS.fLoadCADvariables(INIT_VARS.CADinitVariable)) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_088);}
   INIT_VARS.CADinitVariable.Set_GLobal_VARS();

/*   
   // Get CAD CFG Variables
   if(!char2bool(boolCAD_EXISTS, ini.GetValue("CAD", CAD_INI_KEY_1, DEFAULT_VALUE_CAD_INI_KEY_1 )))
    {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolCAD_EXISTS" );}
   intCAD_PORT_DOWN_THRESHOLD_SEC = char2int(ini.GetValue("CAD", CAD_INI_KEY_5, DEFAULT_VALUE_CAD_INI_KEY_5 ));
   intCAD_PORT_DOWN_REMINDER_SEC = char2int(ini.GetValue("CAD", CAD_INI_KEY_6, DEFAULT_VALUE_CAD_INI_KEY_6 ));
   if(!char2bool(boolSEND_TO_CAD_ON_CONFERENCE, ini.GetValue("CAD", CAD_INI_KEY_7, DEFAULT_VALUE_CAD_INI_KEY_7 ))) 
    {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSEND_TO_CAD_ON_CONFERENCE" );}
   if(!char2bool(boolSEND_TO_CAD_ON_TAKE_CONTROL, ini.GetValue("CAD", CAD_INI_KEY_8, DEFAULT_VALUE_CAD_INI_KEY_8 )))
    {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSEND_TO_CAD_ON_TAKE_CONTROL" );}
 
   // Load CAD IP & Port Data
   i = 1;
   do
    {
     CAD_Key = Create_INI_Key("Port", i, "IP",0,"Address");
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     intRC = LoadPortData( i, "" , StringData, CAD );
     if (intRC){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_016, int2str(i) ); }
     CAD_Key = Create_INI_Key("Port", i, "Connection_Note");
     CADPort[i].strNotes = ini.GetValue("CAD", CAD_Key.c_str(), "None." );
     i++;
    
   }while ( StringData != "");

   // set number of CAD Ports
   intNUM_CAD_PORTS = i-1;

   for (int y = 1; y <=  intNUM_CAD_PORTS; y++)
    {
     CAD_Key = Create_INI_Key("Port", y, "Connection",0,"Type");
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
     if      (StringData  == "TCP") {CADPort[y].ePortConnectionType = TCP;}
     else                           {CADPort[y].ePortConnectionType = UDP;}

     CAD_Key = Create_INI_Key("Port", y, "Positions");     
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     
     CADPort[y].WorkstationGroup.strGroupName = "CAD"+int2strLZ(y);
     CADPort[y].WorkstationGroup.fLoadWorkstations(StringData);
    }

   for (int y = 1; y <=  intNUM_CAD_PORTS; y++)
    {
     CAD_Key = Create_INI_Key("Port", y, "NENA",0,"CAD_Header"); 
     if(!char2bool( CADPort[y].boolNENA_CADheader, ini.GetValue("CAD", CAD_Key.c_str(), "true" ))){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "CADPort[y].boolNENA_CADheader" );}
    }

   for (int y = 1; y <=  intNUM_CAD_PORTS; y++)
    {
     CAD_Key = Create_INI_Key("Port", y, "Position",0,"Aliases"); 
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );     
     if (StringData  == "") {continue;}
     CADPort[y].PositionAliases.fLoadPositionAliases(StringData);


    }




   // Load CAD Port Heartbeat & ACK Rules
   i = 1;
   do
    {
     CAD_Key = Create_INI_Key("Port", i, "Rule",0,"Code");
     StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
     if (StringData  == "") {continue;}
     switch (char2int(StringData.c_str()))
       {
        case 0:
          CADPort[i].boolHeartbeatRequired    = false;
          CADPort[i].boolRecordACKRequired    = false;
          CADPort[i].boolHeartBeatACKrequired = false;
          break;
        case 1:
          CADPort[i].boolHeartbeatRequired    = true;
          CADPort[i].boolRecordACKRequired    = false;
          CADPort[i].boolHeartBeatACKrequired = false;     
          break;
        case 2:
          CADPort[i].boolHeartbeatRequired    = true;
          CADPort[i].boolRecordACKRequired    = true;
          CADPort[i].boolHeartBeatACKrequired = true;     
          break;
        case 3:
          CADPort[i].boolHeartbeatRequired    = false;
          CADPort[i].boolRecordACKRequired    = true;
          CADPort[i].boolHeartBeatACKrequired = false;     
          break;
        case 4:
          CADPort[i].boolHeartbeatRequired    = true;
          CADPort[i].boolRecordACKRequired    = true;
          CADPort[i].boolHeartBeatACKrequired = false;     
          break;
        case 5:
          CADPort[i].boolHeartbeatRequired    = true;
          CADPort[i].boolRecordACKRequired    = false;
          CADPort[i].boolHeartBeatACKrequired = true;
          break;

        default:
          Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_044, int2str(i), StringData ); 
       }// end switch
     
     i++;
    
   }while ( StringData != "");

   if (intNUM_CAD_PORTS != (i-1)){Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_017, int2str(intNUM_CAD_PORTS), int2str(i-1) );}
 
   // Load Send CAD Erase Msg boolean 
   for (int y=1; y <= i; y++){CADPort[y].boolSendCADEraseMsg = false;}          // default value = false
   i = 1;
   do
   {
    CAD_Key = Create_INI_Key("Port", i, "Erase",0,"Msg");
    StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
    if (StringData  == "") {continue;}
    if (i > NUM_CAD_PORTS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_027, int2str(i) );}
    if(!char2bool(CADPort[i].boolSendCADEraseMsg, StringData)){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "CADPort[i].boolSendCADEraseMsg" );}
    // set Global button if any are true ...
    if (CADPort[i].boolSendCADEraseMsg) {boolCAD_ERASE = true;}
    i++;
    
   }while ( StringData != "");

   // Load Erase first CR var
   for (int y=1; y <= i; y++){CADPort[y].boolCAD_EraseFirstALI_CR = false;}          // default value = false
   i = 1;
   do
   {
    CAD_Key = Create_INI_Key("Port", i, "Remove_First_ALI_CR");
    StringData = ini.GetValue("CAD", CAD_Key.c_str(), "" );
    if (StringData  == "") {continue;}
    if (i > NUM_CAD_PORTS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_027, int2str(i) );}
    if(!char2bool(CADPort[i].boolCAD_EraseFirstALI_CR, StringData)){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "CADPort[i].boolCAD_EraseFirstALI_CR" );}
    i++;
    
   }while ( StringData != "");
 
   // check for "all" Databases CC Email addresses
   StringData = ini.GetValue("CAD", KRN_INI_KEY_13, DEFAULT_VALUE_KRN_INI_KEY_13);
   // load email address into all the ports or set to false
   for (int x = 1; x <= intNUM_CAD_PORTS; x++){CADPort[x].fLoad_Vendor_Email_Address(StringData);}  

   // check for specific port cc vendor email addresses
   for (int x = 1; x <= intNUM_CAD_PORTS; x++)    {
     ANI_Key = Create_INI_Key("Port", x, "CC_Alarm_Email_Address");
     StringData = ini.GetValue("CAD", ANI_Key.c_str(), DEFAULT_VALUE_KRN_INI_KEY_13 );
     if (StringData  == DEFAULT_VALUE_KRN_INI_KEY_13) {continue;}
     CADPort[x].fLoad_Vendor_Email_Address(StringData);   
   }
*/

  // WRK CFG Options

  intNUM_WRK_STATIONS           = char2int (ini.GetValue("WRK", WRK_INI_KEY_1, DEFAULT_VALUE_WRK_INI_KEY_1 ));
  intWRK_LOCAL_LISTEN_PORT      = char2int (ini.GetValue("WRK", WRK_INI_KEY_2, DEFAULT_VALUE_WRK_INI_KEY_2 ));

  intWRK_HEARTBEAT_INTERVAL_SEC = int(intNUM_WRK_STATIONS/5)  + 5;

  intWRK_PORT_DOWN_THRESHOLD_SEC = char2int(ini.GetValue("WRK", WRK_INI_KEY_9, DEFAULT_VALUE_WRK_INI_KEY_9 ));
  intWRK_PORT_DOWN_REMINDER_SEC  = char2int(ini.GetValue("WRK", WRK_INI_KEY_10, DEFAULT_VALUE_WRK_INI_KEY_10 ));
  intWRK_GUI_TAB_BEHAVIOR_CODE   = char2int(ini.GetValue("WRK", WRK_INI_KEY_11, DEFAULT_VALUE_WRK_INI_KEY_11 )); 

  enumWORKSTATION_CONNECTION_TYPE = ConnectionType(ini.GetValue("WRK", WRK_INI_KEY_17, DEFAULT_VALUE_WRK_INI_KEY_17 ));
  stringTDD_DEFAULT_LANGUAGE    = ini.GetValue("WRK", WRK_INI_KEY_3, DEFAULT_VALUE_WRK_INI_KEY_3 );
  intTDD_SEND_DELAY             = char2int (ini.GetValue("WRK", WRK_INI_KEY_4, DEFAULT_VALUE_WRK_INI_KEY_4 ));
  stringTDD_SEND_TEXT_COLOR     = ini.GetValue("WRK", WRK_INI_KEY_5, DEFAULT_VALUE_WRK_INI_KEY_5 );
  stringTDD_RECEIVED_TEXT_COLOR = ini.GetValue("WRK", WRK_INI_KEY_6, DEFAULT_VALUE_WRK_INI_KEY_6 );
  intTDD_CPS_THRESHOLD          = char2int (ini.GetValue("WRK", WRK_INI_KEY_7, DEFAULT_VALUE_WRK_INI_KEY_7 ));
  stringTRANSFER_DEFAULT_GROUP  = ini.GetValue("WRK", WRK_INI_KEY_8, DEFAULT_VALUE_WRK_INI_KEY_8 );

  stringMSRP_DEFAULT_LANGUAGE    = ini.GetValue("WRK", WRK_INI_KEY_27, DEFAULT_VALUE_WRK_INI_KEY_27 );
  stringMSRP_SEND_TEXT_COLOR     = ini.GetValue("WRK", WRK_INI_KEY_21, DEFAULT_VALUE_WRK_INI_KEY_21 );
  stringMSRP_RECEIVED_TEXT_COLOR = ini.GetValue("WRK", WRK_INI_KEY_22, DEFAULT_VALUE_WRK_INI_KEY_22 );




  cout << WRK_INI_KEY_13 << " =? " << DEFAULT_VALUE_WRK_INI_KEY_13 << endl;

  cout << ini.GetValue("WRK", WRK_INI_KEY_13, DEFAULT_VALUE_WRK_INI_KEY_13 ) << endl;
  cout << char2bool(boolGUI_SIP_PHONE_EXISTS, ini.GetValue("WRK", WRK_INI_KEY_13, DEFAULT_VALUE_WRK_INI_KEY_13 )) << endl;
  cout << !char2bool(boolGUI_SIP_PHONE_EXISTS, ini.GetValue("WRK", WRK_INI_KEY_13, DEFAULT_VALUE_WRK_INI_KEY_13 )) << endl;
  cout << boolGUI_SIP_PHONE_EXISTS << endl;

  if(!char2bool(boolGUI_SIP_PHONE_EXISTS, ini.GetValue("WRK", WRK_INI_KEY_13, DEFAULT_VALUE_WRK_INI_KEY_13 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolGUI_SIP_PHONE_EXISTS" );}


  strVIEW_POSITION_DEFAULT = ini.GetValue("WRK", WRK_INI_KEY_14, DEFAULT_VALUE_WRK_INI_KEY_14 );

  if(!char2bool(boolSHOW_RINGING_TAB, ini.GetValue("WRK", WRK_INI_KEY_15, DEFAULT_VALUE_WRK_INI_KEY_15 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_RINGING_TAB" );}
  if(!char2bool(bool911_TRANSFER_TANDEM_ONLY, ini.GetValue("WRK", WRK_INI_KEY_16, DEFAULT_VALUE_WRK_INI_KEY_16 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "bool911_TRANSFER_TANDEM_ONLY" );}
  if(!char2bool(boolShowPSAPstatusApplet, ini.GetValue("WRK", WRK_INI_KEY_18, DEFAULT_VALUE_WRK_INI_KEY_18 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolShowPSAPstatusApplet" );}
  if(!char2bool(boolShowSMSapplet, ini.GetValue("WRK", WRK_INI_KEY_19, DEFAULT_VALUE_WRK_INI_KEY_19 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolShowSMSapplet" );}
  if(!char2bool(boolShowPSAPappletAdmin, ini.GetValue("WRK", WRK_INI_KEY_20, DEFAULT_VALUE_WRK_INI_KEY_20 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolShowPSAPappletAdmin" );}
  if(!char2bool(boolSHOW_TDD_BUTTON, ini.GetValue("WRK", WRK_INI_KEY_23, DEFAULT_VALUE_WRK_INI_KEY_23 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_TDD_BUTTON" );}
  if(!char2bool(boolSHOW_ALERT_BUTTON, ini.GetValue("WRK", WRK_INI_KEY_24, DEFAULT_VALUE_WRK_INI_KEY_24 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_ALERT_BUTTON" );}
  if(!char2bool(boolSHOW_TEXT_BUTTON, ini.GetValue("WRK", WRK_INI_KEY_25, DEFAULT_VALUE_WRK_INI_KEY_25 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_TEXT_BUTTON" );}
  if(!char2bool(boolShowCallRecorder, ini.GetValue("WRK", WRK_INI_KEY_26, DEFAULT_VALUE_WRK_INI_KEY_26 )))  
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolShowCallRecorder" );}
  if(!char2bool(boolSHOW_IM_BUTTON, ini.GetValue("WRK", WRK_INI_KEY_30, DEFAULT_VALUE_WRK_INI_KEY_30 )))  
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_IM_BUTTON" );}
  if(!char2bool(boolPLAY_DTMF_TONES, ini.GetValue("WRK", WRK_INI_KEY_31, DEFAULT_VALUE_WRK_INI_KEY_31 )))  
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolPLAY_DTMF_TONES" );}

//  if(!char2bool(boolSHOW_RING_DIALOG, ini.GetValue("WRK", WRK_INI_KEY_32, DEFAULT_VALUE_WRK_INI_KEY_32 )))  

  StringData = ini.GetValue("WRK", WRK_INI_KEY_32, DEFAULT_VALUE_WRK_INI_KEY_32 );
  found = StringData.find_first_not_of("0123456789");
  if (found != string::npos)  {
   intSHOW_RING_DIALOG = 0;
   SendCodingError("ExperientDataController.cpp - Non Integer Data for Show_Ringing_Dialog in fn Process_CFG_File(), value set to zero");
  }                      
  else {
   intSHOW_RING_DIALOG = char2int(StringData.c_str());
  }

  strRINGTONE911   = ini.GetValue("WRK", WRK_INI_KEY_34, DEFAULT_VALUE_WRK_INI_KEY_34 );
  strRINGTONETEXT  = ini.GetValue("WRK", WRK_INI_KEY_35, DEFAULT_VALUE_WRK_INI_KEY_35 );
  strRINGTONEADMIN = ini.GetValue("WRK", WRK_INI_KEY_36, DEFAULT_VALUE_WRK_INI_KEY_36 );

  strRINGIMAGE911   = ini.GetValue("WRK", WRK_INI_KEY_37, DEFAULT_VALUE_WRK_INI_KEY_37 );
  strRINGIMAGETEXT  = ini.GetValue("WRK", WRK_INI_KEY_38, DEFAULT_VALUE_WRK_INI_KEY_38 );
  strRINGIMAGEADMIN = ini.GetValue("WRK", WRK_INI_KEY_39, DEFAULT_VALUE_WRK_INI_KEY_39 );

  

  if(!char2bool(boolCLEAR_DIAL_PAD_AFTER_USE, ini.GetValue("WRK", WRK_INI_KEY_33, DEFAULT_VALUE_WRK_INI_KEY_33 )))  
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolCLEAR_DIAL_PAD_AFTER_USE" );}


  strDISPATCHER_SAYS_PREFIX_EXPORT = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, XML_VALUE_TIME_PATTERN, XML_VALUE_DISPATCHER_SAYS_PATTERN_SUBSTITUTE); 
  strCALLER_SAYS_EXPORT            = Create_Message(TDD_CALLER_SAYS_PREFIX, XML_VALUE_TIME_PATTERN);

  strMAPPING_REGEX = ini.GetValue("WRK", WRK_INI_KEY_29, DEFAULT_VALUE_WRK_INI_KEY_29 );
  
  for (int i = 1; i <= intNUM_WRK_STATIONS; i++) {
   WRK_Key = Create_INI_Key("Wrk", i, "Line_View_Filter");
   StringData = ini.GetValue("WRK", WRK_Key.c_str(), "" );
   WorkStationTable[i].strLineViewFilter = StringData;
  }

  cout << "loading groups" << endl;

  //load Workstation Groups ...... (PSAPs)
  CSimpleIniA::TNamesDepend Group_Name_Values, Group_Trunk_Values, Group_Workstation_Values, Group_PSAPURI_Values, Group_PSAPID_Values ;


   ini.GetAllValues("WRK", "Workstation_Group_Name", Group_Name_Values);
   ini.GetAllValues("WRK", "Workstation_Group_Trunks", Group_Trunk_Values);
   ini.GetAllValues("WRK", "Workstation_Group_Positions", Group_Workstation_Values);
   ini.GetAllValues("WRK", "Workstation_Group_PSAPURI", Group_PSAPURI_Values);
   ini.GetAllValues("WRK", "Workstation_Group_PSAP_ID", Group_PSAPID_Values);
   Workstation_Group objWorkstationGroup;

   Group_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Group_Trunk_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Group_Workstation_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Group_PSAPURI_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Group_PSAPID_Values.sort(CSimpleIniA::Entry::LoadOrder());

   l = Group_Name_Values.begin();
   m = Group_Trunk_Values.begin();
   n = Group_Workstation_Values.begin();
   o = Group_PSAPURI_Values.begin();
   p = Group_PSAPID_Values.begin();
  
   boolA = (p == Group_PSAPID_Values.end()); 
   iCount = 1;
  while ((l != Group_Name_Values.end())&&(m != Group_Trunk_Values.end())&&(n != Group_Workstation_Values.end())&&(o != Group_PSAPURI_Values.end())  )
    { 
     objWorkstationGroup.strGroupName = l->pItem;
     objWorkstationGroup.PSAPStatus.strPSAPname = l->pItem;
     if (!boolA) {
      objWorkstationGroup.strPSAP_ID = p->pItem;
     }
     else {
      if (iCount) {
       StringData = stringPSAP_ID + "-";
       StringData += int2str(iCount);
       objWorkstationGroup.strPSAP_ID = StringData;
      }
     }
     if(!objWorkstationGroup.fLoadTrunks(m->pItem))         {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_075, m->pItem );}
     if(!objWorkstationGroup.fLoadWorkstations(n->pItem))   {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_075, n->pItem );}
     objWorkstationGroup.strPSAPURI = o->pItem;         
     WorkstationGroups.WorkstationGroups.push_back(objWorkstationGroup); 
     objWorkstationGroup.fClear();
     ++l; ++m; ++n; ++o; ++iCount;
     if (!boolA) { ++p;}
    }

    cout << "done loading groups" << endl;
// PSAP URI's are 


// cout << strCALLER_SAYS_EXPORT << endl;
 // strCALLER_SAYS_EXPORT = "From {10-digit-number} [{time}]:";


//  Telephone_Variables test;
//  test.fLoadINIfile();
//  test.fLoadRCCremotePort();








 // cout << "RCC port = " << test.intRCC_REMOTE_TCP_PORT_NUMBER << endl;


  intRCC_REMOTE_TCP_PORT_NUMBER = char2int (ini.GetValue("TELEPHONE", RCC_INI_KEY_1, DEFAULT_VALUE_RCC_INI_KEY_1 ));

  if(!char2bool(boolRCC_DEFAULT_CONTACT_POSITIONS, ini.GetValue("TELEPHONE", RCC_INI_KEY_2, DEFAULT_VALUE_RCC_INI_KEY_2 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolRCC_DEFAULT_CONTACT_POSITIONS" );}
  // Load Contact Relay info
  for (int y = 1; y <= intNUM_WRK_STATIONS; y++)
   {
    Phones_Key = Create_INI_Key("Contact_Relay_", y, "IP_Address"); 
    StringData = ini.GetValue("TELEPHONE", Phones_Key.c_str(), "" );
    if (StringData  == "") {continue;}
    intRC = LoadPortData( y, "" , StringData, RCC );
    if (intRC){Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_002, int2str(y) );} 
   }

   // cout << "loaded RCC info" << endl;

  if(!char2bool(boolUSE_POLYCOM_CTI, ini.GetValue("TELEPHONE", CTI_INI_KEY_1, DEFAULT_VALUE_CTI_INI_KEY_1 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolUSE_POLYCOM_CTI" );}
  intPOLYCOM_CTI_HTTP_PORT        = char2int (ini.GetValue("TELEPHONE", CTI_INI_KEY_2, DEFAULT_VALUE_CTI_INI_KEY_2 ));
//  intPOLYCOM_CTI_TCP_PORT         = char2int (ini.GetValue("TELEPHONE", CTI_INI_KEY_15, DEFAULT_VALUE_CTI_INI_KEY_15 ));
  intPOLYCOM_CTI_END_CALL_SOFTKEY = char2int (ini.GetValue("TELEPHONE", CTI_INI_KEY_4, DEFAULT_VALUE_CTI_INI_KEY_4 ));
  if(!char2bool(boolADD_NINE_FOR_OUTBOUND_CALLS, ini.GetValue("TELEPHONE", CTI_INI_KEY_3, DEFAULT_VALUE_CTI_INI_KEY_3 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolADD_NINE_FOR_OUTBOUND_CALLS" );}
  boolSHOW_ATTENDED_XFER_BUTTON   = boolSHOW_VOLUME_BUTTON = boolSHOW_HANGUP_BUTTON = boolSHOW_HOLD_BUTTON = boolSHOW_BARGE_BUTTON = 
  boolSHOW_ANSWER_BUTTON = boolSHOW_DIAL_PAD_BUTTON = boolSHOW_SPEED_DIAL_BUTTON = boolSHOW_CALLBACK_BUTTON = boolUSE_POLYCOM_CTI;
  
  if (boolUSE_POLYCOM_CTI) { strTrueFalse ="true";} else {strTrueFalse = "false";}
  if(!char2bool(boolSHOW_ANSWER_BUTTON,        ini.GetValue("TELEPHONE", CTI_INI_KEY_5,  strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_ANSWER_BUTTON" );}
  if(!char2bool(boolSHOW_DIAL_PAD_BUTTON ,     ini.GetValue("TELEPHONE", CTI_INI_KEY_6,  strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_DIAL_PAD_BUTTON" );}
  if(!char2bool(boolSHOW_SPEED_DIAL_BUTTON,    ini.GetValue("TELEPHONE", CTI_INI_KEY_7,  strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_SPEED_DIAL_BUTTON" );}
  if(!char2bool(boolSHOW_CALLBACK_BUTTON,      ini.GetValue("TELEPHONE", CTI_INI_KEY_8,  strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_CALLBACK_BUTTON" );}
  if(!char2bool(boolSHOW_ATTENDED_XFER_BUTTON, ini.GetValue("TELEPHONE", CTI_INI_KEY_9,  strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_ATTENDED_XFER_BUTTON" );}
  if(!char2bool(boolSHOW_VOLUME_BUTTON,        ini.GetValue("TELEPHONE", CTI_INI_KEY_10, strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_VOLUME_BUTTON" );}
  if(!char2bool(boolSHOW_HANGUP_BUTTON,        ini.GetValue("TELEPHONE", CTI_INI_KEY_11, strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_HANGUP_BUTTON" );}
  if(!char2bool(boolSHOW_HOLD_BUTTON,          ini.GetValue("TELEPHONE", CTI_INI_KEY_12, strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_HOLD_BUTTON" );}
  if(!char2bool(boolSHOW_BARGE_BUTTON,         ini.GetValue("TELEPHONE", CTI_INI_KEY_13, strTrueFalse.c_str() )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_BARGE_BUTTON" );}
  if(!char2bool(boolSHOW_MUTE_BUTTON,          ini.GetValue("TELEPHONE", CTI_INI_KEY_14, DEFAULT_VALUE_CTI_INI_KEY_14 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_MUTE_BUTTON" );}
  if(!char2bool(boolSHOW_FLASH_XFER_BUTTON,    ini.GetValue("TELEPHONE", CTI_INI_KEY_16, DEFAULT_VALUE_CTI_INI_KEY_16 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_FLASH_XFER_BUTTON" );}
  if(!char2bool(boolSHOW_FLASH_HOOK_BUTTON,    ini.GetValue("TELEPHONE", CTI_INI_KEY_17, DEFAULT_VALUE_CTI_INI_KEY_17 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_FLASH_HOOK_BUTTON " );}
  if(!char2bool(boolSHOW_BLIND_XFER_BUTTON,    ini.GetValue("TELEPHONE", CTI_INI_KEY_18, DEFAULT_VALUE_CTI_INI_KEY_18 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_BLIND_XFER_BUTTON" );}
  if(!char2bool(boolSHOW_CONF_XFER_BUTTON,     ini.GetValue("TELEPHONE", CTI_INI_KEY_21, DEFAULT_VALUE_CTI_INI_KEY_21 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_CONF_XFER_BUTTON" );}
  if(!char2bool(boolSHOW_TANDEM_XFER_BUTTON,   ini.GetValue("TELEPHONE", CTI_INI_KEY_22, DEFAULT_VALUE_CTI_INI_KEY_22 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_TANDEM_XFER_BUTTON" );}
  if(!char2bool(boolSHOW_DROP_LAST_LEG_BUTTON, ini.GetValue("TELEPHONE", CTI_INI_KEY_23, DEFAULT_VALUE_CTI_INI_KEY_23 )))  
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolSHOW_DROP_LAST_LEG_BUTTON" );}

  //  cout << "TEL Button info" << endl;

  strCTI_USERNAME = ini.GetValue("TELEPHONE", CTI_INI_KEY_19, DEFAULT_VALUE_CTI_INI_KEY_19 );
  strCTI_PASSWORD = ini.GetValue("TELEPHONE", CTI_INI_KEY_20, DEFAULT_VALUE_CTI_INI_KEY_20 );
/*
  //write CTI TCP Port to file (works in conjunction with php "forwarding script"
  CTI_TCP_PORT_FILE.clear();
  CTI_TCP_PORT_FILE.open( CTI_TCP_EVENT_PORT_FILENAME, ios_base::trunc | ios_base::out);
  if (!CTI_TCP_PORT_FILE.is_open()) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_076, CTI_TCP_EVENT_PORT_FILENAME);}
  CTI_TCP_PORT_FILE << intPOLYCOM_CTI_TCP_PORT << endl;
  if (CTI_TCP_PORT_FILE.bad())      {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_077, CTI_TCP_EVENT_PORT_FILENAME);}
  CTI_TCP_PORT_FILE.close();
*/

  //load Telephone Monitoring
  intDEVICE_DOWN_THRESHOLD_SEC = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_1, DEFAULT_VALUE_TELEPHONE_INI_KEY_1 ));
  intDEVICE_DOWN_REMINDER_SEC  = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_2, DEFAULT_VALUE_TELEPHONE_INI_KEY_2 ));
  strCALLER_ID_NAME            =  ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_3, DEFAULT_VALUE_TELEPHONE_INI_KEY_3 );
  strCALLER_ID_NUMBER          =  ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_4, DEFAULT_VALUE_TELEPHONE_INI_KEY_4 );
  strNG911_PHONEBOOK_SIP_TRANSFER_HEADER    = ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_7, DEFAULT_VALUE_TELEPHONE_INI_KEY_7 );
  strNG911_PHONEBOOK_SIP_TRANSFER_ICON_FILE = ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_12, DEFAULT_VALUE_TELEPHONE_INI_KEY_12 );

  intTelephonePingFailureThreshold = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_9, DEFAULT_VALUE_TELEPHONE_INI_KEY_9 ));
  intTelephonePingSuccessThreshold = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_11, DEFAULT_VALUE_TELEPHONE_INI_KEY_11 ));
  intTELEPHONE_PING_INTERVAL_SEC   = char2int(ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_10, DEFAULT_VALUE_TELEPHONE_INI_KEY_10 ));

  strTELEPHONE_PING_LIST        = ini.GetValue("TELEPHONE", TELEPHONE_INI_KEY_8, DEFAULT_VALUE_TELEPHONE_INI_KEY_8 );
  boolENABLE_PING_PHONE_MONITOR = Load_Phone_Ping_Monitor_List();

 cout << "Loaded Telephone Gloabals" << endl;
 
  CSimpleIniA::TNamesDepend Area_Code_Values, NXX_Values;            // SpeedDial_Number_Values;
  ini.GetAllValues("TELEPHONE", TELEPHONE_INI_KEY_5, Area_Code_Values);
  ini.GetAllValues("TELEPHONE", TELEPHONE_INI_KEY_6, NXX_Values);

  l = Area_Code_Values.begin();

  while (l != Area_Code_Values.end())
   { 
    LocalCallRules.LocalAreaCode.push_back( l->pItem);
    ++l;
   }

  l = NXX_Values.begin();

  while (l != NXX_Values.end())
   { 
    LocalCallRules.LocalNXXprefix.push_back( l->pItem);
    ++l;
   }

  cout << "Loaded NPA" << endl;






   CSimpleIniA::TNamesDepend Telephone_Name_Values, Telephone_MAC_Values, Telephone_TYPE_Values, Telephone_IP_Values, Telephone_Workstation_Values, Telephone_Status_Values;
   CSimpleIniA::TNamesDepend Telephone_Num_Lines_Values, Telephone_Line_Reg_Name_Values, Telephone_Outbound_Dial_Values, Telephone_Shared_Line_Values;
   CSimpleIniA::TNamesDepend Telephone_Alias_Line_Values, Telephone_NextGen_URI_Values, Telephone_Caller_Name_Values, Telephone_Caller_Number_Values;
   bool                      boolMonitorDevice;
   bool                      boolLineAlias = false;
   bool                      boolSIPURI    = true;
   bool                      boolCallerName = true;
   bool                      boolCallerNumber = true;
   Phone_Line                objPhoneLine;

    ini.GetAllValues("TELEPHONE", "Phone_Device_Name", Telephone_Name_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_MAC_Addr", Telephone_MAC_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_Type", Telephone_TYPE_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_IPaddress", Telephone_IP_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_Workstation", Telephone_Workstation_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_Status", Telephone_Status_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_Num_Lines", Telephone_Num_Lines_Values);
    ini.GetAllValues("TELEPHONE", "Phone_Device_PSAP_URI", Telephone_NextGen_URI_Values);   
    ini.GetAllValues("TELEPHONE", "Phone_Device_Caller_Name", Telephone_Caller_Name_Values);   
    ini.GetAllValues("TELEPHONE", "Phone_Device_Caller_Number", Telephone_Caller_Number_Values);   

    Telephone_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_MAC_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_TYPE_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_IP_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_Workstation_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_Status_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_Num_Lines_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_NextGen_URI_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_Caller_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Telephone_Caller_Number_Values.sort(CSimpleIniA::Entry::LoadOrder());

    l = Telephone_Name_Values.begin();
    m = Telephone_MAC_Values.begin();
    n = Telephone_TYPE_Values.begin();
    o = Telephone_IP_Values.begin();
    p = Telephone_Workstation_Values.begin();
    q = Telephone_Status_Values.begin();
    r = Telephone_Num_Lines_Values.begin();
    w = Telephone_NextGen_URI_Values.begin();
    w1 = Telephone_Caller_Name_Values.begin();
    w2 = Telephone_Caller_Number_Values.begin();

    boolSIPURI       = (Telephone_NextGen_URI_Values.size() > 0);
    boolCallerName   = (Telephone_Caller_Name_Values.size() > 0);
    boolCallerNumber = (Telephone_Caller_Number_Values.size() > 0);

    vector <int> Previndex;
    i=0;
    iCount = 0;
    while ((l != Telephone_Name_Values.end())||(m != Telephone_MAC_Values.end())||(n != Telephone_TYPE_Values.end())||
           (o != Telephone_IP_Values.end())||(p != Telephone_Workstation_Values.end())||(q != Telephone_Status_Values.end())||
           (r != Telephone_Num_Lines_Values.end()) )
     {

      if (boolSIPURI)       { if (w  == Telephone_NextGen_URI_Values.end())   { break; }} 
      if (boolCallerName)   { if (w1 == Telephone_Caller_Name_Values.end())   { break; }}
      if (boolCallerNumber) { if (w2 == Telephone_Caller_Number_Values.end()) { break; }}  

      if(!Validate_Integer( p->pItem)) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066c);} 
      i = char2int(p->pItem);
      if (i > intNUM_WRK_STATIONS) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066c );} 
  
      StringData = q->pItem;
      boolMonitorDevice = (StringData != "UnMonitor"); 

      if (boolSIPURI){StringData = w->pItem;}
      else           {StringData = "NONE";}

      if (!TelephoneEquipment.fLoadDevice(i, l->pItem, StringData, boolMonitorDevice))     {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_065);} 
      if (!TelephoneEquipment.fLoadMACaddress(i, m->pItem))     {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_064);}         
      if (!TelephoneEquipment.fLoadIPaddress(i, o->pItem))      {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066b);}
      if (!TelephoneEquipment.fLoadDeviceType(i, n->pItem))     {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066);}
      if (!TelephoneEquipment.fLoadNumberofLines(i, r->pItem))  {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066a);}
      if ( i > TelephoneEquipment.HighestWorkstationNumber )    {TelephoneEquipment.HighestWorkstationNumber = i;}

      if (boolCallerName) {StringData = w1->pItem;}
      else                {StringData = strCALLER_ID_NAME;}
      TelephoneEquipment.fLoadCallerIdName(i, StringData);
      if (boolCallerNumber) {StringData = w2->pItem;}
      else                  {StringData = strCALLER_ID_NUMBER;}
      TelephoneEquipment.fLoadCallerIdNumber(i, StringData);

      // Load Line Registrations
      index = TelephoneEquipment.fIndexWithPositionNumber(i);
      if (index < 0) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_066c );}
      Previndex.push_back(index);

      // do line registrations ..... 
      for (int y = 0; y <= TelephoneEquipment.Devices[index].iNumberOfLines; y++)
       {
        if (y == 0) { objPhoneLine.fClear(); TelephoneEquipment.Devices[index].vPhoneLines.push_back(objPhoneLine); continue;}
        Line_Key = Create_INI_Key("Line_", y, "Reg_Name");
        ini.GetAllValues("TELEPHONE", Line_Key.c_str(), Telephone_Line_Reg_Name_Values);
        Line_Key = Create_INI_Key("Line_", y, "Dial_Out");
        ini.GetAllValues("TELEPHONE", Line_Key.c_str(), Telephone_Outbound_Dial_Values);
        Line_Key = Create_INI_Key("Line_", y, "Shared");
        ini.GetAllValues("TELEPHONE", Line_Key.c_str(), Telephone_Shared_Line_Values);
        Line_Key = Create_INI_Key("Line_", y, "Index");
        ini.GetAllValues("TELEPHONE", Line_Key.c_str(), Telephone_Alias_Line_Values);

        s = Telephone_Line_Reg_Name_Values.begin();
        t = Telephone_Outbound_Dial_Values.begin();
        u = Telephone_Shared_Line_Values.begin();
        v = Telephone_Alias_Line_Values.begin();

        boolLineAlias = (Telephone_Alias_Line_Values.size() > 0);

         for (int a = 0; a < iCount; a++)
          {  
           if (TelephoneEquipment.Devices[Previndex[a]].iNumberOfLines >= y) { ++s; ++t; ++u; if (boolLineAlias) {++v;}}     
          }
        if (( s == Telephone_Line_Reg_Name_Values.end()) || (t == Telephone_Outbound_Dial_Values.end())|| (u == Telephone_Shared_Line_Values.end()) ) { continue;}
        if (boolLineAlias &&((( s == Telephone_Line_Reg_Name_Values.end()) || (v == Telephone_Alias_Line_Values.end()) ) ))                           { continue;} 
        objPhoneLine.fLoadRegistrationName(s->pItem);
        objPhoneLine.fLoadOutboundDial(string(t->pItem));
        objPhoneLine.fLoadSharedLine(u->pItem);
        if (boolLineAlias) {objPhoneLine.fLoadLineIndex(char2int(v->pItem));}
        else               {objPhoneLine.fLoadLineIndex(y);}
        TelephoneEquipment.Devices[index].vPhoneLines.push_back(objPhoneLine);
       }

      
      ++l; ++m; ++n; ++o; ++p; ++q; ++r; iCount++; 
     if(boolSIPURI)       {w++;}
     if(boolCallerName)   {w1++;}
     if(boolCallerNumber) {w2++;} 
     }
   TelephoneEquipment.NumberofTelephones = iCount;   

   // Load NG911 SIP URI's

   //Load EXT MWI GATEWAYS
    CSimpleIniA::TNamesDepend MWIGateway_Name_Values, MWIPhone_Values, MWILineReg_Values;
    string strMWIgateway;
    int    intPhone;
    string strMWIlineReg;

    ini.GetAllValues("TELEPHONE", "EXTERN_MWI_GATEWAY",  MWIGateway_Name_Values);
    ini.GetAllValues("TELEPHONE", "EXTERN_MWI_PHONE",    MWIPhone_Values);
    ini.GetAllValues("TELEPHONE", "EXTERN_MWI_LINE_REG", MWILineReg_Values);


    MWIGateway_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
    MWIPhone_Values.sort(CSimpleIniA::Entry::LoadOrder());
    MWILineReg_Values.sort(CSimpleIniA::Entry::LoadOrder());

    l = MWIGateway_Name_Values.begin();
    m = MWIPhone_Values.begin();
    n = MWILineReg_Values.begin();

    while (  (l != MWIGateway_Name_Values.end())||(m != MWIPhone_Values.end())||(n != MWILineReg_Values.end())  )  {
    
     strMWIgateway = l->pItem;
     if (strMWIgateway.empty()) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_087, "EXTERN_MWI_GATEWAY" );}

     StringData = m->pItem;
     found = StringData.find_first_not_of("0123456789");
     if (found != string::npos)              {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_087, "EXTERN_MWI_PHONE" );}
     intPhone = char2int(StringData.c_str());

     strMWIlineReg = n->pItem;
     if (strMWIlineReg.empty()) {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_087, "EXTERN_MWI_LINE_REG" );}
 
     if (!TelephoneEquipment.fLoadExtMWIGateway(strMWIgateway, intPhone, (const char*)strMWIlineReg.c_str())) {
      Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_087, "EXTERN_MWI" );
     }        
     ++l; ++m; ++n;
    }


    //load IP gateways 
    CSimpleIniA::TNamesDepend Gateway_Name_Values, Gateway_IP_Values, Gateway_Dial_Out_Values,Gateway_Dial_Ext_Values;
    ini.GetAllValues("TELEPHONE", "Gateway_Name", Gateway_Name_Values);
    ini.GetAllValues("TELEPHONE", "Gateway_IP_Address", Gateway_IP_Values);
    ini.GetAllValues("TELEPHONE", "Gateway_Dial_Ext", Gateway_Dial_Ext_Values);
    ini.GetAllValues("TELEPHONE", "Gateway_Dial_Out", Gateway_Dial_Out_Values);

    Gateway_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Gateway_IP_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Gateway_Dial_Ext_Values.sort(CSimpleIniA::Entry::LoadOrder());
    Gateway_Dial_Out_Values.sort(CSimpleIniA::Entry::LoadOrder());

    // output all of the items

    l = Gateway_Name_Values.begin();
    m = Gateway_IP_Values.begin();
    n = Gateway_Dial_Ext_Values.begin();
    o = Gateway_Dial_Out_Values.begin();

    while ((l != Gateway_Name_Values.end())||(m != Gateway_IP_Values.end())||(n != Gateway_Dial_Ext_Values.end())||(o != Gateway_Dial_Out_Values.end()) )
     { 
      if(!char2bool(boolA, n->pItem))   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "Gateway_Dial_Ext" );}
      if(!char2bool(boolB, o->pItem))   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "Gateway_Dial_Out" );}
      if (!TelephoneEquipment.fLoadIPGateway(l->pItem, m->pItem, boolA, boolB, IsAudiocodes(l->pItem))) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_072, Phones_Key, StringData);} 
      ++l; ++m; ++n; ++o;

     }
   cout << "loaded gateway data" << endl; 


  // Line appearance
  i = 1;
   TelephoneEquipment.GUILineView.push_back(objLineView);
   do
   {
    Line_Key = Create_INI_Key("SLA_Line_", i);
    Label_Key = Line_Key + "_Text";
    StringData = ini.GetValue("TELEPHONE", Line_Key.c_str(), "" );
    if (StringData  == "") {continue;}
    objLineView.Name = StringData;
    StringData = ini.GetValue("TELEPHONE", Label_Key.c_str(), "" );
    objLineView.Label = StringData;
    TelephoneEquipment.GUILineView.push_back(objLineView);
    i++;    
   }while ( StringData != ""); 

   TelephoneEquipment.NumberofLineViews = i-1;
   cout << "loaded LineViews" << endl; 

    //Parking Lots

    CSimpleIniA::TNamesDepend Park_Lot_Values;
    ini.GetAllValues("TELEPHONE", "Parking_Lot", Park_Lot_Values);

    Park_Lot_Values.sort(CSimpleIniA::Entry::LoadOrder());
 
    // output all of the items

    l = Park_Lot_Values.begin();
    TelephoneEquipment.ParkingLots.clear();

    while (l != Park_Lot_Values.end() )
     { 
      StringData = l->pItem;
      found = StringData.find_first_not_of("0123456789");
      if (found != string::npos)              {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_083, StringData );}
      TelephoneEquipment.ParkingLots.push_back(l->pItem);
      ++l;
     }
    TelephoneEquipment.NumberofParkingLots = TelephoneEquipment.ParkingLots.size();
   cout << "loaded Parking Lots" << endl; 

    // Routing Prefixes
    CSimpleIniA::TNamesDepend Routing_Prefix_Values;
    ini.GetAllValues("TELEPHONE", "Routing_Prefix", Routing_Prefix_Values);

    Routing_Prefix_Values.sort(CSimpleIniA::Entry::LoadOrder());
 
    l = Routing_Prefix_Values.begin();
    TelephoneEquipment.vRoutingPrefixes.clear();

    while (l != Routing_Prefix_Values.end() )
     { 
      TelephoneEquipment.vRoutingPrefixes.push_back(l->pItem);
      ++l;
     }
    TelephoneEquipment.NumberofRoutingPrefixes = TelephoneEquipment.vRoutingPrefixes.size();
    
   if (TelephoneEquipment.NumberofRoutingPrefixes > 0) {boolROUTING_PREFIXES_USED = true;}
   cout << "loaded Routing Prefixes" << endl; 




   boolSHOW_LINE_VIEW_TAB = (TelephoneEquipment.NumberofLineViews > 0);

   if (boolSHOW_LINE_VIEW_TAB) {boolSHOW_RINGING_TAB = false;}

 

  // **************************  Build workstation init XML message ******************************************************************************************
  // Now build a diffrent message for each workstation group .......



  MainNode.emptyNode();
  // XML Header
  MainNode= XMLNode::createXMLTopNode("xml",TRUE);
  MainNode.addAttribute(XML_FIELD_VERSION, XML_FIELD_VERSION_NUMBER);
  MainNode.addAttribute(XML_FIELD_XML_ENCODING, XML_FIELD_XML_ENCODING_VALUE);

  // Event Tag
  EventNode          = MainNode.addChild(XML_NODE_EVENT);
  EventNode.addAttribute(XML_FIELD_PACKET_ID, "");
  EventNode.addAttribute(XML_FIELD_XML_SPEC, chWORKSTATION_XML_SPECIFICATION);

  // Initialization Tag
  InitializationNode = EventNode.addChild(XML_NODE_INITIALIZATION);

  // General Tag
  GeneralNode = InitializationNode.addChild(XML_NODE_GENERAL);

   // General Setup Tag
   GeneralSetupNode = GeneralNode.addChild(XML_NODE_GENERAL_SETUP);
    // Number of Positions
    GeneralSetupNode.addAttribute(XML_FIELD_WRK_NUM_POSITIONS, int2str(intNUM_WRK_STATIONS).c_str());
    // Heartbeat interval
    GeneralSetupNode.addAttribute(XML_FIELD_WRK_HEARTBEAT_INTERVAL, int2str(intWRK_HEARTBEAT_INTERVAL_SEC).c_str()); 
    // Mapping Regex (optional)
    if (!strMAPPING_REGEX.empty()) {
     GeneralSetupNode.addAttribute(XML_FIELD_MAPPING_REGEX, strMAPPING_REGEX.c_str());
    }
    // Product Name
    GeneralSetupNode.addAttribute(XML_FIELD_PRODUCT_NAME, stringPRODUCT_NAME.c_str());
    // Company Name
    GeneralSetupNode.addAttribute(XML_FIELD_COMPANY_NAME, stringCOMPANY_NAME.c_str());
    //Support Number
    GeneralSetupNode.addAttribute(XML_FIELD_SUPPORT_NUMBER, strCUSTOMER_SUPPORT_NUMBER.c_str());
    // About Splash File
    GeneralSetupNode.addAttribute(XML_FIELD_ABOUT_SPLASH_FILE, strABOUT_SPLASH_FILE.c_str());
    // About Splash Color
    if (!strABOUT_SPLASH_COLOR.empty()) {
     if (strABOUT_SPLASH_COLOR[0] != '#') {
      strABOUT_SPLASH_COLOR = "#" + strABOUT_SPLASH_COLOR;
     }
    }
    GeneralSetupNode.addAttribute(XML_FIELD_ABOUT_SPLASH_COLOR, strABOUT_SPLASH_COLOR.c_str());
    // About Splash Font
    GeneralSetupNode.addAttribute(XML_FIELD_ABOUT_SPLASH_FONT, strABOUT_SPLASH_FONT.c_str());
    // about Splash Font Pt
    GeneralSetupNode.addAttribute(XML_FIELD_ABOUT_SPLASH_FONT_PT, strABOUT_SPLASH_FONT_PT.c_str());
   // END General Setup Tag

   //Applet Setup Tag
   AppletSetupNode = GeneralNode.addChild(XML_NODE_APPLET_SETUP);
   if (boolShowPSAPstatusApplet)      {AppletSetupNode.addAttribute(XML_FIELD_SHOW_PSAP_STATUS_APPLET, XML_VALUE_TRUE);}
   else                               {AppletSetupNode.addAttribute(XML_FIELD_SHOW_PSAP_STATUS_APPLET, XML_VALUE_FALSE);}
   if (boolShowSMSapplet)             {AppletSetupNode.addAttribute(XML_FIELD_SHOW_SMS_APPLET, XML_VALUE_TRUE);}
   else                               {AppletSetupNode.addAttribute(XML_FIELD_SHOW_SMS_APPLET, XML_VALUE_FALSE);}
   if (boolShowPSAPappletAdmin)       {AppletSetupNode.addAttribute(XML_FIELD_SHOW_PSAP_APPLET_ADMIN, XML_VALUE_TRUE);}
   else                               {AppletSetupNode.addAttribute(XML_FIELD_SHOW_PSAP_APPLET_ADMIN, XML_VALUE_FALSE);}

   //use both ..... IRR init bug ......
   if (boolShowCallRecorder)          {
    AppletSetupNode.addAttribute(XML_FIELD_SHOW_CALL_RECORDER_APPLET, XML_VALUE_TRUE);
    AppletSetupNode.addAttribute(XML_FIELD_SHOW_IRR_BUTTON, XML_VALUE_TRUE);
   }
   else                               {
    AppletSetupNode.addAttribute(XML_FIELD_SHOW_CALL_RECORDER_APPLET, XML_VALUE_FALSE);
    AppletSetupNode.addAttribute(XML_FIELD_SHOW_IRR_BUTTON, XML_VALUE_FALSE);
   }
   AppletSetupNode.addAttribute(XML_FIELD_SHOW_COLOR_EDITOR_APPLET, XML_VALUE_FALSE); // this will be updated on the in initxml

   //Ringing Applet
   RingAppletNode = GeneralNode.addChild(XML_NODE_RINGING_APPLET);
   RingToneNode   = RingAppletNode.addChild(XML_NODE_RING_TONES);
   RingImageNode  = RingAppletNode.addChild(XML_NODE_RING_IMAGES);
 
   RingToneNode.addAttribute(XML_FIELD_CALL_911, strRINGTONE911.c_str());
   RingToneNode.addAttribute(XML_FIELD_TEXT_911, strRINGTONETEXT.c_str());
   RingToneNode.addAttribute(XML_FIELD_ADMIN,    strRINGTONEADMIN.c_str());

   RingImageNode.addAttribute(XML_FIELD_CALL_911, strRINGIMAGE911.c_str());
   RingImageNode.addAttribute(XML_FIELD_TEXT_911, strRINGIMAGETEXT.c_str());
   RingImageNode.addAttribute(XML_FIELD_ADMIN,    strRINGIMAGEADMIN.c_str()); 
   
   
   
   // Button Setup Tag
   ButtonSetupNode = GeneralNode.addChild(XML_NODE_BUTTON_SETUP);
    // Manual Bids
    if (boolALLOW_MANUAL_ALI_BIDS)     {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_MANUAL_REQUEST_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_MANUAL_REQUEST_BUTTON, XML_VALUE_FALSE);}
    // CAD
    if (boolCAD_EXISTS)                {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CAD_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CAD_BUTTON, XML_VALUE_FALSE);}
    // CAD ERASE
    if (boolCAD_ERASE)                 {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CAD_ERASE_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CAD_ERASE_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_GOOGLE_MAP)           {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_GOOGLE_MAP, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_GOOGLE_MAP, XML_VALUE_FALSE);}
    if (boolCONTACT_RELAY_INSTALLED )  {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_RADIO_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_RADIO_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_RAPID_LITE_BUTTON)    {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_RAPID_LITE_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_RAPID_LITE_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_CALLBACK_BUTTON)      {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CALL_BACK_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CALL_BACK_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_SPEED_DIAL_BUTTON)    {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_SPEED_DIAL_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_SPEED_DIAL_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_DIAL_PAD_BUTTON)      {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_DIAL_PAD_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_DIAL_PAD_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_ANSWER_BUTTON)        {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_ANSWER_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_ANSWER_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_BARGE_BUTTON)         {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_BARGE_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_BARGE_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_HOLD_BUTTON)          {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_HOLD_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_HOLD_BUTTON, XML_VALUE_FALSE);}    
    if (boolSHOW_HANGUP_BUTTON)        {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_HANGUP_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_HANGUP_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_MUTE_BUTTON)          {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_MUTE_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_MUTE_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_VOLUME_BUTTON)        {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_VOLUME_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_VOLUME_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_TANDEM_XFER_BUTTON)   {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TANDEM_XFER_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TANDEM_XFER_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_DROP_LAST_LEG_BUTTON) {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_DROP_LAST_LEG_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_DROP_LAST_LEG_BUTTON, XML_VALUE_FALSE);} 
   
    if (boolSHOW_ATTENDED_XFER_BUTTON) {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_ATTENDED_XFER_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_ATTENDED_XFER_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_BLIND_XFER_BUTTON)    {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_BLIND_XFER_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_BLIND_XFER_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_CONF_XFER_BUTTON)     {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CONF_XFER_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_CONF_XFER_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_FLASH_XFER_BUTTON)    {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_FLASH_XFER_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_FLASH_XFER_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_FLASH_HOOK_BUTTON)    {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_FLASH_HOOK_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_FLASH_HOOK_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_ALERT_BUTTON)         {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_ALERT_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_ALERT_BUTTON, XML_VALUE_FALSE);}
    if      (boolSHOW_TEXT_BUTTON)     {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TEXT_BUTTON, XML_VALUE_TRUE);
                                        ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TDD_BUTTON, XML_VALUE_FALSE);
                                       }
    else if (boolSHOW_TDD_BUTTON)      {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TDD_BUTTON, XML_VALUE_TRUE);
                                        ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TEXT_BUTTON, XML_VALUE_FALSE);
                                       }
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_TDD_BUTTON, XML_VALUE_FALSE);}
    if (boolSHOW_IM_BUTTON)            {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_IM_BUTTON, XML_VALUE_TRUE);}
    else                               {ButtonSetupNode.addAttribute(XML_FIELD_SHOW_IM_BUTTON, XML_VALUE_FALSE);}


   // END Button Setup Tag
  // END General Tag
           

  //TabSetup Tag
  TabSetupNode = GeneralNode.addChild(XML_NODE_TAB_SETUP);
  if(strVIEW_POSITION_DEFAULT == XML_VALUE_ALL) {TabSetupNode.addAttribute(XML_FIELD_POSITION_VIEW_DEFAULT, XML_VALUE_ALL);}
  else                                          {TabSetupNode.addAttribute(XML_FIELD_POSITION_VIEW_DEFAULT, DEFAULT_VALUE_WRK_INI_KEY_14);}
  TabSetupNode.addAttribute(XML_FIELD_TAB_BEHAVIOR_CODE,int2str(intWRK_GUI_TAB_BEHAVIOR_CODE).c_str());
  iGUInumberOfRows = WRK_ABANDONED_CALLS_PER_TRUNK * intNUM_TRUNKS_INSTALLED + 100;
  TabSetupNode.addAttribute(XML_FIELD_GUI_NUMBER_ROWS, int2str(iGUInumberOfRows).c_str());
  iRecordAgeOut = CONFERENCE_NUMBER_IN_USE_MAX_TIME_PERIOD_SEC + 100;
  TabSetupNode.addAttribute(XML_FIELD_RECORD_AGE_OUT_SEC,int2str(iRecordAgeOut).c_str());
  intGUI_MAX_ABANDONED_CALLS = WRK_ABANDONED_CALLS_PER_TRUNK * intNUM_TRUNKS_INSTALLED ;
  TabSetupNode.addAttribute(XML_FIELD_ABANDONED_CALLS_MAX, int2str(intGUI_MAX_ABANDONED_CALLS).c_str());
  if (boolSHOW_LINE_VIEW_TAB)  {TabSetupNode.addAttribute(XML_FIELD_SHOW_LINE_VIEW_TAB, XML_VALUE_TRUE);}
  else                         {TabSetupNode.addAttribute(XML_FIELD_SHOW_LINE_VIEW_TAB, XML_VALUE_FALSE);}
  if (boolSHOW_RINGING_TAB)    {TabSetupNode.addAttribute(XML_FIELD_SHOW_RINGING_TAB,   XML_VALUE_TRUE);}
  else                         {TabSetupNode.addAttribute(XML_FIELD_SHOW_RINGING_TAB,   XML_VALUE_FALSE);}
  //End TabSetup Tag

  // Server Tag
  ServerNode = InitializationNode.addChild(XML_NODE_SERVER);  
      for (int y = 1; y <= intNUM_OF_SERVERS; y++)
       {
        // ServerSetup Tag
        ServerSetupNode = ServerNode.addChild(XML_NODE_SERVER_SETUP);
        ServerSetupNode.addAttribute(XML_FIELD_SERVER_NUMBER, int2str(y).c_str());
        ServerSetupNode.addAttribute(XML_FIELD_IP_ADDRESS, ServerStatusTable[y].Gateway_IP.stringAddress.c_str());
       }
   // END ServerSetup Tag
  // END Server Tag

  cout << "boolGUI_SIP_PHONE_EXISTS -> " << boolGUI_SIP_PHONE_EXISTS << endl;
  if (boolGUI_SIP_PHONE_EXISTS)
   {

     // SIPphone Tag
     SIPphoneNode = InitializationNode.addChild(XML_NODE_SIP_PHONE); 
     // SIPphone setup Tag
     SIPphoneSetupNode = SIPphoneNode.addChild(XML_NODE_SIP_PHONE_SETUP);
     // UserName Pattern
     found = stringSERVER_HOSTNAME.find_last_of("-");

     StringData = stringSERVER_HOSTNAME.substr(0,found) +"x";
     found = StringData.find("-");
cout << "before erase a" << endl;
     StringData.erase(found,1);
cout << "after erase a" << endl;

     StringData += XML_VALUE_USERNAME_PATTERN;
   
     SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_USER_NAME_PATTERN, StringData.c_str());
     // Password
     SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_PASSWORD, XML_VALUE_SIP_PHONE_PASSWORD);

     // TBC
     //  SIPphoneSetupNode.addAttribute("ProxyUser", StringData.c_str());
     //  SIPphoneSetupNode.addAttribute("ProxyPassword", XML_VALUE_SIP_PHONE_PASSWORD);
     SIPphoneSetupNode.addAttribute("Proxy", "192.168.1.198");

     // Min Jitter
     SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_MIN_JITTER, int2str(intSIP_PHONE_MIN_JITTER).c_str());
     // Max jitter
     SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_MAX_JITTER, int2str(intSIP_PHONE_MAX_JITTER).c_str());
     // Jitter depth
     SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_JITTER_DEPTH, int2str(intSIP_PHONE_JITTER_DEPTH).c_str());
     // Echo Cancellation
     if (boolSIP_PHONE_ECHO_CANCELLATION) {SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_ECHO_CANCELLATION, XML_VALUE_ON);}
     else                                 {SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_ECHO_CANCELLATION, XML_VALUE_OFF);}
     // Silence Detection
     if (boolSIP_PHONE_SILENCE_DETECTION) {SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_SILENCE_DETECTION, XML_VALUE_ON);}
     else                                 {SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_SILENCE_DETECTION, XML_VALUE_OFF);}
     // Auto Gain Control
     if (boolSIP_PHONE_AUTO_GAIN_CONTROL) {SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_AUTO_GAIN_CONTROL, XML_VALUE_ON);}
     else                                 {SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_AUTO_GAIN_CONTROL, XML_VALUE_OFF);}
     // Port
     SIPphoneSetupNode.addAttribute(XML_FIELD_SIP_PHONE_UDP_PORT, int2str(intSIP_PHONE_UDP_PORT).c_str());
     // END SIPphone Setup tag
    //END SIPphone Tag

    
    // Speed Dial Tag
    SpeedDialNode =  InitializationNode.addChild(XML_NODE_SPEED_DIAL);
    // Speed Dial Setup Tag
    SpeedDialSetupNode = SpeedDialNode.addChild(XML_NODE_SPEED_DIAL_SETUP);
    // Default Group
    SpeedDialSetupNode.addAttribute(XML_FIELD_SPEED_DIAL_DEFAULT_GROUP, stringTRANSFER_DEFAULT_GROUP.c_str());
    // SpeedDial List
    SpeedDialListNode = SpeedDialNode.addChild(XML_NODE_SPEED_DIAL_LIST);
    CSimpleIniA::TNamesDepend SpeedDial_Name_Values, SpeedDial_Group_Values, SpeedDial_Number_Values;
    ini.GetAllValues("WRK", "SpeedDial_Name", SpeedDial_Name_Values);
    ini.GetAllValues("WRK", "SpeedDial_Group", SpeedDial_Group_Values);
    ini.GetAllValues("WRK", "SpeedDial_Number", SpeedDial_Number_Values);

    SpeedDial_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
    SpeedDial_Group_Values.sort(CSimpleIniA::Entry::LoadOrder());
    SpeedDial_Number_Values.sort(CSimpleIniA::Entry::LoadOrder());

    // output all of the items

    l = SpeedDial_Name_Values.begin();
    m = SpeedDial_Group_Values.begin();
    n = SpeedDial_Number_Values.begin();
    while ((l != SpeedDial_Name_Values.end())||(m != SpeedDial_Group_Values.end())||(n != SpeedDial_Number_Values.end()))
     { 
      SpeedDialToNode = SpeedDialListNode.addChild(XML_NODE_SPEED_DIAL_TO);
      SpeedDialToNode.addAttribute(XML_FIELD_SPEED_DIAL_NAME,   l->pItem);
      SpeedDialToNode.addAttribute(XML_FIELD_SPEED_DIAL_NUMBER, m->pItem);
      SpeedDialToNode.addAttribute(XML_FIELD_SPEED_DIAL_GROUP,  n->pItem);;
      ++l; ++m; ++n;
     }

    } // end  if (boolGUI_SIP_PHONE_EXISTS)
   
   //LineView Tag
   LineViewNode =  InitializationNode.addChild(XML_NODE_LINE_VIEW);
   // LineViewRow List
   for(unsigned int x = 1; x <= TelephoneEquipment.NumberofLineViews; x++)
    {
     LineViewRowNode =  LineViewNode.addChild(XML_NODE_LINE_VIEW_ROW);
     LineViewRowNode.addAttribute(XML_FIELD_ROW, int2str(x).c_str());
     LineViewRowNode.addAttribute(XML_FIELD_DISPLAY,   TelephoneEquipment.GUILineView[x].Label.c_str());
    }
   //lineView

   // TDD Tag
   TDDnode =  InitializationNode.addChild(XML_NODE_TDD);
   // TDD setup Tag
   TDDsetupNode = TDDnode.addChild(XML_NODE_TDD_SETUP);
   // TDD Auto Detect
   if (boolTDD_AUTO_DETECT){TDDsetupNode.addAttribute(XML_FIELD_TDD_AUTO_DETECT, "on");}
   else                    {TDDsetupNode.addAttribute(XML_FIELD_TDD_AUTO_DETECT, "off");}   
   // Default Language
   TDDsetupNode.addAttribute(XML_FIELD_TDD_DEFAULT_LANGUAGE, stringTDD_DEFAULT_LANGUAGE.c_str());
   // Send delay
   TDDsetupNode.addAttribute(XML_FIELD_TDD_SEND_DELAY, int2str(intTDD_SEND_DELAY).c_str());
   // Send Text Color
   TDDsetupNode.addAttribute(XML_FIELD_TDD_SEND_TEXT_COLOR, stringTDD_SEND_TEXT_COLOR.c_str());
   // Received Text color
   TDDsetupNode.addAttribute( XML_FIELD_TDD_RECEIVED_TEXT_COLOR, stringTDD_RECEIVED_TEXT_COLOR.c_str());
   // CPS Threshold
   TDDsetupNode.addAttribute(XML_FIELD_TDD_CPS_THRESHOLD_SEC, int2str(intTDD_CPS_THRESHOLD).c_str());
   // Dispatcher Says Pattern / internal pattern is %%%
   TDDsetupNode.addAttribute(XML_FIELD_TDD_DISPATCHER_SAYS_PATTERN, strDISPATCHER_SAYS_PREFIX_EXPORT.c_str());   
   // Caller Says Prefix
   TDDsetupNode.addAttribute(XML_FIELD_TDD_CALLER_SAYS_PREFIX, strCALLER_SAYS_EXPORT.c_str());


   // TDD Message List tag
   TDDmessageListNode =  TDDnode.addChild(XML_NODE_TDD_MESSAGE_LIST);   
   CSimpleIniA::TNamesDepend TDD_Message_Text_Values, TDD_Message_Language_Values;
   ini.GetAllValues("WRK", "TDD_Message_Text", TDD_Message_Text_Values);
   ini.GetAllValues("WRK", "TDD_Message_Language", TDD_Message_Language_Values);
   TDD_Message_Text_Values.sort(CSimpleIniA::Entry::LoadOrder());
   TDD_Message_Language_Values.sort(CSimpleIniA::Entry::LoadOrder());
   // output all of the items
   l = TDD_Message_Text_Values.begin();
   m = TDD_Message_Language_Values.begin();
   while ((l != TDD_Message_Text_Values.end())||(m != TDD_Message_Language_Values.end()))
    { 
     TDDmessageNode = TDDmessageListNode.addChild(XML_NODE_TDD_MESSAGE);
     TDDmessageNode.addAttribute(XML_FIELD_TDD_MESSAGE_TEXT,  l->pItem);
     TDDmessageNode.addAttribute(XML_FIELD_TDD_MESSAGE_LANGUAGE, m->pItem);    
     ++l; ++m;
    }

  // MSRP Tag
   MSRPnode =  InitializationNode.addChild(XML_NODE_MSRP);
   // MSRP setup Tag
   MSRPsetupNode = MSRPnode.addChild(XML_NODE_MSRP_SETUP);
 
   // Default Language
   MSRPsetupNode.addAttribute(XML_FIELD_MSRP_DEFAULT_LANGUAGE, stringMSRP_DEFAULT_LANGUAGE.c_str());
   // Send Text Color
   MSRPsetupNode.addAttribute(XML_FIELD_MSRP_SEND_TEXT_COLOR, stringMSRP_SEND_TEXT_COLOR.c_str());
   // Received Text color
   MSRPsetupNode.addAttribute( XML_FIELD_MSRP_RECEIVED_TEXT_COLOR, stringMSRP_RECEIVED_TEXT_COLOR.c_str());
   // Dispatcher Says Pattern / internal pattern is %%%
   MSRPsetupNode.addAttribute(XML_FIELD_MSRP_DISPATCHER_SAYS_PATTERN, strDISPATCHER_SAYS_PREFIX_EXPORT.c_str());   
   // Caller Says Prefix
   MSRPsetupNode.addAttribute(XML_FIELD_MSRP_CALLER_SAYS_PREFIX, strCALLER_SAYS_EXPORT.c_str());


   // TDD Message List tag
   MSRPmessageListNode =  MSRPnode.addChild(XML_NODE_MSRP_MESSAGE_LIST);   
   CSimpleIniA::TNamesDepend MSRP_Message_Text_Values, MSRP_Message_Language_Values;
   ini.GetAllValues("WRK", "MSRP_Message_Text", MSRP_Message_Text_Values);
   ini.GetAllValues("WRK", "MSRP_Message_Language", MSRP_Message_Language_Values);
   MSRP_Message_Text_Values.sort(CSimpleIniA::Entry::LoadOrder());
   MSRP_Message_Language_Values.sort(CSimpleIniA::Entry::LoadOrder());
   // output all of the items
   l = MSRP_Message_Text_Values.begin();
   m = MSRP_Message_Language_Values.begin();
   while ((l != MSRP_Message_Text_Values.end())||(m != MSRP_Message_Language_Values.end()))  { 
     strMSRPtextMessage = MSRPCheckForPound(l->pItem);
     MSRPmessageNode = MSRPmessageListNode.addChild(XML_NODE_MSRP_MESSAGE);
     MSRPmessageNode.addAttribute(XML_FIELD_MSRP_MESSAGE_TEXT,  strMSRPtextMessage.c_str());
     MSRPmessageNode.addAttribute(XML_FIELD_MSRP_MESSAGE_LANGUAGE, m->pItem);    
     ++l; ++m;
   }




   // Transfer Tag
   PhoneBookNode =  InitializationNode.addChild(XML_NODE_PHONEBOOK);
   // Transfer Setup Tag
   PhoneBookSetupNode = PhoneBookNode.addChild(XML_NODE_PHONEBOOK_SETUP);
   // Default Group
   PhoneBookSetupNode.addAttribute(XML_FIELD_TRANSFER_DEFAULT_GROUP, stringTRANSFER_DEFAULT_GROUP.c_str());
   if (boolPLAY_DTMF_TONES) {
    PhoneBookSetupNode.addAttribute(XML_FIELD_PLAY_DTMF_TONES, XML_VALUE_TRUE);
   }
   else {
    PhoneBookSetupNode.addAttribute(XML_FIELD_PLAY_DTMF_TONES, XML_VALUE_FALSE);
   }

   if (boolCLEAR_DIAL_PAD_AFTER_USE) {
    PhoneBookSetupNode.addAttribute(XML_FIELD_CLEAR_DIAL_PAD_AFTER_USE, XML_VALUE_TRUE);
   }
   else {
    PhoneBookSetupNode.addAttribute(XML_FIELD_CLEAR_DIAL_PAD_AFTER_USE, XML_VALUE_FALSE);
   }
   // Transfer List Tag
   PhonebookListNode = PhoneBookNode.addChild(XML_NODE_PHONEBOOK_LIST);
   //GroupList tag
   PhonebookListNode = PhoneBookNode.addChild(XML_NODE_GROUP_LIST);


// no longer reading in values from ini file .....
/*
   CSimpleIniA::TNamesDepend Transfer_Name_Values, Transfer_Group_Values, Transfer_Number_Values, Transfer_Code_Values;
   ini.GetAllValues("WRK", "Transfer_Name", Transfer_Name_Values);
   ini.GetAllValues("WRK", "Transfer_Group", Transfer_Group_Values);
   ini.GetAllValues("WRK", "Transfer_Number", Transfer_Number_Values);
   ini.GetAllValues("WRK", "Transfer_Code", Transfer_Code_Values);
   Transfer_Name_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Transfer_Group_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Transfer_Number_Values.sort(CSimpleIniA::Entry::LoadOrder());
   Transfer_Code_Values.sort(CSimpleIniA::Entry::LoadOrder());
   // output all of the items
   l = Transfer_Name_Values.begin();
   m = Transfer_Group_Values.begin();
   n = Transfer_Number_Values.begin();
   o = Transfer_Code_Values.begin();
   while ((l != Transfer_Name_Values.end())||(m != Transfer_Group_Values.end())||(n != Transfer_Number_Values.end())||(o != Transfer_Code_Values.end()))
    { 
     objPhoneRecord.strCallerID = n->pItem;
     objPhoneRecord.strCustName = l->pItem;
     vCALL_LIST.push_back(objPhoneRecord);
     PhonebookEntryNode = PhonebookListNode.addChild(XML_NODE_PHONEBOOK_ENTRY);
     PhonebookEntryNode.addAttribute(XML_FIELD_TRANSFER_NAME,   l->pItem);
     PhonebookEntryNode.addAttribute(XML_FIELD_TRANSFER_GROUP,  m->pItem);
     PhonebookEntryNode.addAttribute(XML_FIELD_TRANSFER_NUMBER, n->pItem);
     PhonebookEntryNode.addAttribute(XML_FIELD_TRANSFER_CODE,   o->pItem);
     ++l; ++m; ++n; ++o;
    }
*/

  INIT_VARS.MasterNode  = MainNode.deepCopy();
  INIT_VARS.MainNode[0] = MainNode.deepCopy();

  if (!INIT_VARS.fLoadPhonebook() ) {SendCodingError("Experient_Conroller.cpp - Unable to load Phonebook!");}

//  INIT_VARS.fLoadSharedLines(0);

  INIT_VARS.fInitXMLString(0);





  INIT_VARS.fLoadMessageControl(true);
  INIT_VARS.WRKinitVariable.fLoadColorPaletteUsers(true);

//

  
 // cout << INIT_VARS.strInitXML << endl;

//***************************************************************** END BUILD WORKSTATION INIT XML Message **************************************************************************************
 

/*



  // ADV CFG Options 
 
  strDISPLAY_MODE = ini.GetValue("ADVANCED", ADV_INI_KEY_1, DEFAULT_VALUE_ADV_INI_KEY_1 );
  if      (strDISPLAY_MODE == LOG_DISPLAY_MODE_1) {enumDISPLAY_MODE = MINIMAL_DISPLAY;}
  else if (strDISPLAY_MODE == LOG_DISPLAY_MODE_2) {enumDISPLAY_MODE = NORMAL_DISPLAY;}
  else  {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_053, strDISPLAY_MODE ); }  
  Set_Display_Mode(enumDISPLAY_MODE);

  


 if(!char2bool(boolEmailON, ini.GetValue("ADVANCED", ADV_INI_KEY_2, DEFAULT_VALUE_ADV_INI_KEY_2 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "boolEmailON" );}
  strEMAIL_RELAY_SERVER      = ini.GetValue("ADVANCED", ADV_INI_KEY_3, DEFAULT_VALUE_ADV_INI_KEY_3 ); 
  strEMAIL_RELAY_USER        = ini.GetValue("ADVANCED", ADV_INI_KEY_4, DEFAULT_VALUE_ADV_INI_KEY_4 ); 
  strEMAIL_RELAY_PASSWORD    = ini.GetValue("ADVANCED", ADV_INI_KEY_5, DEFAULT_VALUE_ADV_INI_KEY_5 ); 
  intDEBUG_MODE_REMINDER_SEC = char2int(ini.GetValue("ADVANCED", ADV_INI_KEY_6, DEFAULT_VALUE_ADV_INI_KEY_6 ));
  if(!char2bool(bool_POLYCOM_404_FIRMWARE, ini.GetValue("ADVANCED", ADV_INI_KEY_7, DEFAULT_VALUE_ADV_INI_KEY_7 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "bool_POLYCOM_404_FIRMWARE" );}
  if(!char2bool(bool_FREESWITCH_VERSION_212_PLUS, ini.GetValue("ADVANCED", ADV_INI_KEY_8, DEFAULT_VALUE_ADV_INI_KEY_8 )))
   {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_028, "bool_FREESWITCH_VERSION_212_PLUS" );}
*/


// cout << "loaded Init Data" << endl;
 return 0;
}






/*

bool Find_NTP_Start_Script()
{
 if (FileExists(NTP_SERVICE_FILE_LOCATION_606))
  {
   stringNTP_SERVICE_START_SCRIPT = NTP_SERVICE_START_SCRIPT_606;
   stringNTP_SERVICE_STOP_SCRIPT = NTP_SERVICE_STOP_SCRIPT_606;
   return true;
  }
 else if (FileExists(NTP_SERVICE_FILE_LOCATION_710))
  {
   stringNTP_SERVICE_START_SCRIPT = NTP_SERVICE_START_SCRIPT_710;
   stringNTP_SERVICE_STOP_SCRIPT = NTP_SERVICE_STOP_SCRIPT_710;
   return true;
  }
 else {Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_032 );}
return false;
}

*/

//constructor
ALISteering::ALISteering()
{
  intDatabaseNum 		= 0;
  eRequestKeyFormat		= NO_FORMAT;
  intNormalTimeOutSec		= 0;
  intWirelessTimeoutSec		= 0;
  intNumPortPairs		= 0;
  for (unsigned int j = 0; j <=  2; j++) 			{intCallbackLocationRow[j] = 0;}
  for (unsigned int j = 0; j <=  2; j++) 			{intCallbackLocationColumn[j] = 0;}
  for (unsigned int j = 0; j <=  intMAX_ALI_PORT_PAIRS ; j++) 	{boolPortPairInDatabase[j] = false;} 
  intNumPANIRangePairs		= 0;
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANILowRange[j] = 0;}
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANIHighRange[j] = 0;}
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANIRebidDelay[j] = 0;}
  boolDatabaseSendsSingleALI	= false;
  boolALIService		= false;
  boolTreatLinefeedasCR         = false;
  iALIServiceIndex		= 0;
  enumTrunkType			= CAMA;
  vCOSCallbackRow.clear();
  iClassOfServiceRow		= 0;
}
//Copy Constructor
ALISteering::ALISteering(const ALISteering *a)
{
  intDatabaseNum		= a->intDatabaseNum;
  eRequestKeyFormat		= a->eRequestKeyFormat;
  intNormalTimeOutSec		= a->intNormalTimeOutSec;
  intWirelessTimeoutSec		= a->intWirelessTimeoutSec;
  intNumPortPairs		= a->intNumPortPairs;
  for (unsigned int j = 0; j <=  2; j++) 			{intCallbackLocationRow[j]    = a->intCallbackLocationRow[j];}
  for (unsigned int j = 0; j <=  2; j++) 			{intCallbackLocationColumn[j] = a->intCallbackLocationColumn[j];}
  for (unsigned int j = 0; j <=  intMAX_ALI_PORT_PAIRS ; j++) 	{boolPortPairInDatabase[j]    = a->boolPortPairInDatabase[j];} 
  intNumPANIRangePairs		= a->intNumPANIRangePairs;
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANILowRange[j]   = a->intPANILowRange[j];}
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANIHighRange[j]  = a->intPANIHighRange[j];}
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANIRebidDelay[j] = a->intPANIRebidDelay[j];}
  boolDatabaseSendsSingleALI	= a->boolDatabaseSendsSingleALI;
  boolALIService		= a->boolALIService;
  iALIServiceIndex		= a->iALIServiceIndex;
  boolTreatLinefeedasCR         = a->boolTreatLinefeedasCR;
  enumTrunkType			= a->enumTrunkType;
  vCOSCallbackRow		= a->vCOSCallbackRow;
  iClassOfServiceRow		= a->iClassOfServiceRow;
}
//assignment operator
ALISteering& ALISteering::operator=(const ALISteering& a)
{
  if (&a == this ) {return *this;}
  intDatabaseNum		= a.intDatabaseNum;
  eRequestKeyFormat		= a.eRequestKeyFormat;
  intNormalTimeOutSec		= a.intNormalTimeOutSec;
  intWirelessTimeoutSec		= a.intWirelessTimeoutSec;
  intNumPortPairs		= a.intNumPortPairs;
  for (unsigned int j = 0; j <=  2; j++) 			{intCallbackLocationRow[j]    = a.intCallbackLocationRow[j];}
  for (unsigned int j = 0; j <=  2; j++) 			{intCallbackLocationColumn[j] = a.intCallbackLocationColumn[j];}
  for (unsigned int j = 0; j <=  intMAX_ALI_PORT_PAIRS ; j++) 	{boolPortPairInDatabase[j]    = a.boolPortPairInDatabase[j];} 
  intNumPANIRangePairs		= a.intNumPANIRangePairs;
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANILowRange[j]   = a.intPANILowRange[j];}
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANIHighRange[j]  = a.intPANIHighRange[j];}
  for (unsigned int j = 0; j <=  intMAX_PANI_RANGE_PAIRS; j++) 	{intPANIRebidDelay[j] = a.intPANIRebidDelay[j];}
  boolDatabaseSendsSingleALI	= a.boolDatabaseSendsSingleALI;
  boolALIService		= a.boolALIService;
  boolTreatLinefeedasCR         = a.boolTreatLinefeedasCR;
  iALIServiceIndex		= a.iALIServiceIndex;
  enumTrunkType			= a.enumTrunkType;
  vCOSCallbackRow		= a.vCOSCallbackRow;
  iClassOfServiceRow		= a.iClassOfServiceRow;
  return *this;
}

bool COSCallbackRow::fCompare(const COSCallbackRow& a) const
{
 bool boolRunningCheck = true;

 boolRunningCheck = boolRunningCheck && (this->strClassOfService == a.strClassOfService);
 boolRunningCheck = boolRunningCheck && (this->iRow		 == a.iRow);

 return boolRunningCheck;
}

bool ALISteering::fCompare(const ALISteering& a) const
{
  bool boolRunningCheck = true;

  boolRunningCheck = boolRunningCheck && (this->intDatabaseNum 			== a.intDatabaseNum);
  boolRunningCheck = boolRunningCheck && (this->eRequestKeyFormat 		== a.eRequestKeyFormat);
  boolRunningCheck = boolRunningCheck && (this->intNormalTimeOutSec 		== a.intNormalTimeOutSec);
  boolRunningCheck = boolRunningCheck && (this->intWirelessTimeoutSec 		== a.intWirelessTimeoutSec);
  boolRunningCheck = boolRunningCheck && (this->intNumPortPairs 		== a.intNumPortPairs);
 for (unsigned int i = 0; i <=  2; i++) 
  {  
   boolRunningCheck = boolRunningCheck && (this->intCallbackLocationRow[i] 	== a.intCallbackLocationRow[i]);
   boolRunningCheck = boolRunningCheck && (this->intCallbackLocationColumn[i] 	== a.intCallbackLocationColumn[i]);
  }
 for (unsigned int i = 0; i <=  intMAX_ALI_PORT_PAIRS; i++) 
  {
   boolRunningCheck = boolRunningCheck && (this->boolPortPairInDatabase[i] 	== a.boolPortPairInDatabase[i]);
  }
 boolRunningCheck = boolRunningCheck && (this->intNumPANIRangePairs 		== a.intNumPANIRangePairs);
 for (unsigned int i = 0; i <=  intMAX_PANI_RANGE_PAIRS; i++) 
  {
   boolRunningCheck = boolRunningCheck && (this->intPANILowRange[i] 		== a.intPANILowRange[i]);
   boolRunningCheck = boolRunningCheck && (this->intPANIHighRange[i] 		== a.intPANIHighRange[i]);
   boolRunningCheck = boolRunningCheck && (this->intPANIRebidDelay[i] 		== a.intPANIRebidDelay[i]);
  } 
 boolRunningCheck = boolRunningCheck && (this->boolDatabaseSendsSingleALI 	== a.boolDatabaseSendsSingleALI);
 boolRunningCheck = boolRunningCheck && (this->boolALIService 			== a.boolALIService);
 boolRunningCheck = boolRunningCheck && (this->boolTreatLinefeedasCR 		== a.boolTreatLinefeedasCR);
 boolRunningCheck = boolRunningCheck && (this->iALIServiceIndex 		== a.iALIServiceIndex); // This may be removed ????
 boolRunningCheck = boolRunningCheck && (this->iClassOfServiceRow 		== a.iClassOfServiceRow);
 boolRunningCheck = boolRunningCheck && (this->enumTrunkType 			== a.enumTrunkType);
 boolRunningCheck = boolRunningCheck && (this->vCOSCallbackRow 			== a.vCOSCallbackRow);


return boolRunningCheck;
}

void ALISteering::fClear()
{
  intDatabaseNum 		= 0;
  eRequestKeyFormat 		= NO_FORMAT;
  intNormalTimeOutSec		= 0;
  intWirelessTimeoutSec		= 0;
  intNumPortPairs		= 0;
  for (unsigned int i = 0; i <=  2; i++) 			{ intCallbackLocationRow[i] = 0;}
  for (unsigned int i = 0; i <=  2; i++) 			{ intCallbackLocationColumn[i] = 0;}
  for (unsigned int i = 0; i <=  intMAX_ALI_PORT_PAIRS; i++) 	{ boolPortPairInDatabase[i] = false;}
  intNumPANIRangePairs 		= 0;
  for (unsigned int i = 0; i <=  intMAX_PANI_RANGE_PAIRS; i++) 	{ intPANILowRange[i] = 0;}
  for (unsigned int i = 0; i <=  intMAX_PANI_RANGE_PAIRS; i++) 	{ intPANIHighRange[i] = 0;}
  for (unsigned int i = 0; i <=  intMAX_PANI_RANGE_PAIRS; i++) 	{ intPANIRebidDelay[i] = 0;}
  boolDatabaseSendsSingleALI	= false;
  boolALIService		= false;
  boolTreatLinefeedasCR         = false;
  iALIServiceIndex		= 0;
  enumTrunkType			= CAMA;
  vCOSCallbackRow.clear();
  iClassOfServiceRow		= 0;

 return;
}

/****************************************************************************************************
*
* Name:
*  Function: bool ALISteering::fLoadTrunkType(string strArg)
*
*
* Description:
*  An ALISteering initialization function
*
*
* Details:
*   this function loads the value of the trunk type for the database
*
*
* Parameters:
*   strArg                             <cstring>   
*
* Variables:
*   CAMA                                 Global  <header.h><trunk_type>    enumeration member
*   enumTrunkType                        Local   <ALISteering><trunk_type> enumeration
*   LANDLINE                             Global  <header.h><trunk_type>    enumeration member
*   WIRELESS                             Global  <header.h><trunk_type>    enumeration member
*                                                                      
* Functions:
*   none                          
*
* Author(s):
*   Bob McCarthy 			Original: 2/7/2009
*                                       Updated : N/A
*
****************************************************************************************************/ 
bool ALISteering::fLoadTrunkType(string strArg)
{
 if      (strArg == "All")      {enumTrunkType = CAMA;      return true;}
 else if (strArg == "ALL")      {enumTrunkType = CAMA;      return true;}
 else if (strArg == "all")      {enumTrunkType = CAMA;      return true;}
 else if (strArg == "LANDLINE") {enumTrunkType = LANDLINE;  return true;}
 else if (strArg == "Landline") {enumTrunkType = LANDLINE;  return true;}
 else if (strArg == "landline") {enumTrunkType = LANDLINE;  return true;}
 else if (strArg == "WIRELESS") {enumTrunkType = WIRELESS;  return true;}
 else if (strArg == "Wireless") {enumTrunkType = WIRELESS;  return true;}
 else if (strArg == "wireless") {enumTrunkType = WIRELESS;  return true;}
 else                           {return false;}
}

/****************************************************************************************************
*
* Name:
*  Function: int ALISteering::fLoadPortPairs( int intArg, string stringArg)
*
*
* Description:
*  Initializtion Function
*
*
* Details:
*   this function reads a string of numbers (1.. intMAX_ALI_PORT_PAIRS) and assigns "true" 
*   to an array of booleans at the position indicated by the number read. If the string 
*   is equal to "ALL", All positions are set to true.
*
*
* Parameters:
*   intArg                             
*   stringArg                           <cstring>
*
* Variables:
*   boolPortPairInDatabase[]    Local   <ALISteering> <bool> member array
*   CFG_MESSAGE_051             Global  <defines.h> error message
*   EX_CONFIG                   Library <sysexits.h> exit code
*   intDatabaseNum              Local   <ALISteering> member
*   intMAX_ALI_PORT_PAIRS       Global  <globals.h>
*   intPortPair                 Local   int Port Pair number read in
*   MAIN                        Global  <header.h> <threadorporttype> enumeration member
*   npos                        Library <cstring> constant
*   position                    Local   <cstring><size_t>
*   stringNumber                Local   <cstring> trunk number read in 
*   stringTemp                  Local   <cstring> temp string
*   TrunkType[]                 Local   <ANITrunkMapping> <trunk_type> member array
*                                                                      
* Functions:
*   .assign()				Library <cstring>		(void)
*   char2int()				Global  <globalfunctions.h>     (unsigned long long int)
*   .c_str()				Library <cstring>		(const char*)
*   .erase()				Library <cstring>		(void)
*   .find_first_of()		Library <cstring>		(size_t)
*   .find_last_of()			Library <cstring>		(size_t)
*
* Author(s):
*   Bob McCarthy 			Original: 5/29/2008
*                                       Updated : N/A
*
****************************************************************************************************/ 
int ALISteering::fLoadPortPairs( int intArg, string stringArg)
{
 string         stringTemp;
 string         stringNumber;
 size_t         position;
 int            intPortPair;
      
 intDatabaseNum = intArg;

 stringTemp = stringArg;

 
 if (stringArg == ""){SendCodingError( "ExperientController.cpp - empty string passed in ALISteering::fLoadPortPairs"); return 1;}
 do {
   position = stringTemp.find_first_of( ',');
   if (position == string::npos){continue;}
   stringNumber.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1); 
   intPortPair = char2int(stringNumber.c_str());
   if (intPortPair > intMAX_ALI_PORT_PAIRS)
    {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_051, stringNumber );}
   boolPortPairInDatabase[intPortPair]  = true; 
   intNumPortPairs++;
 } while (position != string::npos);

 position = 0;
 position = stringTemp.find_last_of( ",", position);
// if (position == string::npos){cout << "No Comma found in ALISteering::fLoadPortPairs" << endl; return 1;}
 stringNumber.assign(stringTemp,position+1 ,string::npos);
 intPortPair = char2int(stringNumber.c_str());
 if (intPortPair > intMAX_ALI_PORT_PAIRS)
    {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_051, stringNumber );}
 boolPortPairInDatabase[intPortPair]  = true; 
 intNumPortPairs++;

 return 0;
 
}
/****************************************************************************************************
*
* Name:
*  Function: bool ALISteering::fLoadCallbackLocation(string strArg, index)
*
*
* Description:
*  ALISteering initializtion Function
*
*
* Details:
*   this function receives either a single integer or two integers separated by a comma.
*   It loads the first integer into the Row variable and the second integer into the column
*   variable if it exists. the function returns true if the values are loaded successfully.
*
*
* Parameters: 				                             
*   strArg                              <cstring>
*   index                               int
*                                    
* Variables:
*   intCallbackLocationColumn           Local   <ALISteering> integer
*   intCallbackLocationRow              Local   <ALISteering> integer
*   npos                                Library <cstring> constant
*   position                            Local   <cstring><size_t>
*   strColumn                           Local   <cstring> Column value 
*   strRow                              Local   <cstring> Row value
*                                                                      
* Functions:
*   char2int()				Global  <globalfunctions.h>     (unsigned long long int)
*   .c_str()				Library <cstring>		(const char*)
*   .find_first_of()			Library <cstring>		(size_t)
*   RemoveLeadingSpaces()               Global  <globalfunctions.h>     (string)
*   RemoveTrailingSpaces()              Global  <globalfunctions.h>     (string)
*   .substr()                           Library <cstring>		(string)
*   Validate_Integer()                  Global  <globalfunctions.h>     (bool)
*
* Author(s):
*   Bob McCarthy 			Original: 4/17/2009
*                                       Updated : N/A
*
****************************************************************************************************/ 
bool ALISteering::fLoadCallbackLocation(string strArg, int index)
{
 size_t         position;
 string         strRow;
 string         strColumn;

 position = strArg.find_first_of( ',');
 strRow = RemoveTrailingSpaces(strArg.substr(0,position));
 if (!Validate_Integer(strRow)) {return false;}
 intCallbackLocationRow[index] = char2int(strRow.c_str());
 if (position == string::npos)
  {
   intCallbackLocationColumn[index] = 0;
  }
 else 
  {
   if ((position+1)>= (strArg.length()-1)) {SendCodingError("ExperientController.cpp - substr fLoadCallbackLocation"); return false;}
   strColumn = RemoveLeadingSpaces(strArg.substr(position+1, string::npos));
   if (!Validate_Integer(strColumn)) {return false;}
   intCallbackLocationColumn[index] = char2int(strColumn.c_str());
  }

 return true;
}
void ALISteering::fLoadClassOfServiceRow(string strInput)
{
 this->iClassOfServiceRow = 0;
 if (!Validate_Integer(strInput)) {return;}
 this->iClassOfServiceRow = char2int(strInput.c_str());
 return;
}
void ALISteering::fDisplayCOSVector(string strHeader)
{
 cout << strHeader << "Class Of Service Row  = " << this->iClassOfServiceRow << endl;
 for (vector<COSCallbackRow>::iterator it = vCOSCallbackRow.begin() ; it != vCOSCallbackRow.end(); ++it)
  {
   cout << strHeader << "Class Of Service: " << it->strClassOfService << " Row: " << it->iRow << endl;
  }
}
bool ALISteering::fLoadCOSvector(string strInput)
{
 size_t         position;
 string         strCOS;
 string         strRow;
 int            iRow;
 COSCallbackRow objData;

 position = strInput.find_first_of( ',');
 if (position == string::npos) {return false;}
 strCOS = RemoveTrailingSpaces(strInput.substr(0,position));
 strCOS = RemoveLeadingSpaces(strCOS);
 if (strCOS.length() != 4)     {return false;}

 if ((position+1)> (strInput.length()-1)) {SendCodingError("ExperientController.cpp - substr ALISteering::fLoadCOSvector"); return false;}
  
 strRow = RemoveLeadingSpaces(strInput.substr(position+1, string::npos));
 strRow = RemoveTrailingSpaces(strRow);
 if (!Validate_Integer(strRow)) {return false;}
 iRow = char2int(strRow.c_str());

 if (!objData.fLoadCOSplusRow(strCOS, iRow)) {return false;}

 this->vCOSCallbackRow.push_back(objData); 

 return true;
}
/****************************************************************************************************
*
* Name:
*  Function: bool ALISteering::fCanBidDatabase(trunk_type eTrunkType) 
*
*
* Description:
*  An ALISteering Function
*
*
* Details:
*   this function determines if the Database can be bid by a particular Trunk Type. A
*   boolean is returned.  
*   note CLID is not a valid value for an ALI database.  
*
*
* Parameters: 				
*   eTrunkType                          <header.h><trunk_type> the trunk type to be evaluated                          
*
* Variables:
*   CAMA                                Global  <header.h><trunk_type>    enumeration member
*   CLID                                Global  <header.h><trunk_type>    enumeration member
*   enumTrunkType                       Local   <ALISteering><trunk_type> the Database trunk type
*   LANDLINE                            Global  <header.h><trunk_type>    enumeration member
*   WIRELESS                            Global  <header.h><trunk_type>    enumeration member
*                                                                      
* Functions:
*   none
*
* Author(s):
*   Bob McCarthy 			Original: 2/7/2009
*                                       Updated : N/A
*
****************************************************************************************************/
bool ALISteering::fCanBidDatabase(trunk_type eTrunkType) 
{
 switch (enumTrunkType)
  {
   case CAMA:                        return true;
   case LANDLINE:
        if (eTrunkType == LANDLINE) {return true;}
        else                        {return false;}
   case WIRELESS:
        if (eTrunkType == WIRELESS) {return true;}
        else                        {return false;}
   case INDIGITAL_WIRELESS:
        if (eTrunkType == INDIGITAL_WIRELESS) {return true;}
        else                                  {return false;} 
   case INTRADO:
        if (eTrunkType == INTRADO)            {return true;}
        else                                  {return false;}        
   case CLID:
        SendCodingError( "ExperientController.cpp - CODING ERROR in ALISteering::fCanBidDatabase() Database " + int2str( intDatabaseNum) + " has Trunk Type CLID");
   case SIP_TRUNK: return true;
   case NG911_SIP: return true;
   case REFER:     return false;
   case MSRP_TRUNK: return false;

      
   return false;
  }// end switch

 return false;
}

/****************************************************************************************************
*
* Name:
*  Function: int ALISteering::fLoadTimeoutData( int intArg, string stringArg)
*
*
* Description:
*  Initializtion Function
*
*
* Details:
*   this function reads a string of numbers (1.. intMAX_ALI_PORT_PAIRS) and assigns "true" 
*   to an array of booleans at the position indicated by the number read. If the string 
*   is equal to "ALL", All positions are set to true.
*
*
* Parameters: 				
*   intArg                             
*   stringArg                           <cstring>
*
* Variables:
*   boolFoundOne                        Local
*   boolPortPairInDatabase[]            Local   <ALISteering> 
*   i                                   Local   int counter
*   intDatabaseNum                      Local   <ALISteering>
*   intNUM_ALI_PORT_PAIRS               Global  int Number of ALI Databases
*   npos                                Library <cstring> constant
*   position                            Local   <cstring><size_t> 
*   stringTemp                          Local   <cstring>
*   VstringNumbers[]                    Global  <vector><cstring> list of the numbers 1 thru intNUM_ALI_PORT_PAIRS
*                                                                      
* Functions:
*   none
*
* Author(s):
*   Bob McCarthy 			Original: 2/1/2007
*                                       Updated : N/A
*
****************************************************************************************************/
 
int ALISteering::fLoadTimeoutData( string stringArg)
{
 string         stringTemp1 = "";
 string         stringTemp2 = "";
 size_t         position1,position2;  
 

 stringTemp1 = stringArg;
 position1 = stringTemp1.find_first_of("0123456789");
 if (position1 == string::npos){SendCodingError( "ExperientController.cpp - No Number Found in ALISteering::fLoadTimeoutData()"); return 1; }
 position2 = stringTemp1.find_first_not_of("0123456789");
 if (position1 == string::npos){SendCodingError("ExperientController.cpp - Invalid Character Found in ALISteering::fLoadTimeoutData()"); return 1; }
 stringTemp2 = "";
 stringTemp2.assign(stringTemp1, position1 , position2 );
 stringTemp1.erase(position1 ,position2);

 intNormalTimeOutSec = char2int(stringTemp2.c_str());

 position1 = stringTemp1.find_first_of("0123456789");
 if (position1 == string::npos){SendCodingError("ExperientController.cpp - No Number Found in ALISteering::fLoadTimeoutData()"); return 1; }
 stringTemp2 = "";
 stringTemp2.assign(stringTemp1, position1 , stringTemp1.length() );
 
 intWirelessTimeoutSec = char2int(stringTemp2.c_str());

 return 0;
}




/****************************************************************************************************
*
* Name:
*  Function: int ALISteering::fLoadPANIRanges(int intArg, string stringArg)
*
*
* Description:
*  An <ALISteering> Function implementation
*
*
* Details:
*   this function reads a string of 10 digit number pairs then converts and assigns the first number to 
*   the low end the PANI range and the second number to the high end. If an odd number of 10 digit numbers 
*   are found or no numbers are found the function returns an error. If the number of pairs exceeds 
*   the size of the input array, the function displays an error message and terminates the program.
*
*
* Parameters: 				
*   intArg                              
*   stringArg                           <cstring>
*
* Variables:
*   CFG_MESSAGE_008                     Global  <defines.h> error message
*   charINI_FILE_PATH_AND_NAME          Global  <defines.h>
*   i                                   Local   int counter
*   intDatabaseNum                      Local   <ALISteering> member
*   intPANIHighRange[]                  Local   <ALISteering> member
*   intPANILowRange[]                   Local   <ALISteering> member
*   intMAX_PANI_RANGE_PAIRS             Global  <defines.h>
*   intNumPANIRangePairs                Local   <ALISteering> member
*   npos                                Library <cstring> constant
*   position                            Local   <cstring><size_t> 
*   stringTemp1                         Local   <cstring>
*   stringTemp2                         Local   <cstring>
*                                                                      
* Functions:
*   .assign()                           Library <cstring>
*   Console_Message()                   Global  <globalfunctions.h>
*   .c_str()                            Library <cstring>
*   .erase()                            Library <cstring>
*   exit()                              Library <stdlib.h>
*   .find_first_not_of()                Library <cstring>
*   .length()                           Library <cstring>
*
* Author(s):
*   Bob McCarthy 			Original: 5/28/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
int ALISteering::fLoadPANIRanges(int intDatabNum, int intPANIRangeNum, string stringArg)  
{
 string         stringTemp1 = "";
 string         stringTemp2 = "";
 size_t         position;   
   
 intDatabaseNum = intDatabNum;
 stringTemp1 = stringArg;
 position = stringTemp1.find_first_of("0123456789");
 if (position == string::npos){SendCodingError("ExperientController.cpp - No Number Found in ALISteering::fLoadPANIRanges().1"); return 1; }
 stringTemp2 = "";
 stringTemp2.assign(stringTemp1, position , 10 );
 stringTemp1.erase(position ,10);

 intPANILowRange[intPANIRangeNum]  = char2int(stringTemp2.c_str());
 
 position = stringTemp1.find_first_of("0123456789");
 if (position == string::npos){SendCodingError("ExperientController.cpp - No Number Found in ALISteering::fLoadPANIRanges().2"); return 1; }
 stringTemp2 = "";
 stringTemp2.assign(stringTemp1, position, 10 );
 stringTemp1.erase(position ,10);

 intPANIHighRange[intPANIRangeNum]  = char2int(stringTemp2.c_str());
 
 position = stringTemp1.find_first_of("0123456789");
 if (position == string::npos){SendCodingError("ExperientController.cpp - No Number Found in ALISteering::fLoadPANIRanges().3"); return 1; }
 stringTemp2 = "";
 stringTemp2.assign(stringTemp1, position, stringTemp1.length() );

 intPANIRebidDelay[intPANIRangeNum] = char2int(stringTemp2.c_str());
 intNumPANIRangePairs++;
 return 0;

}

bool COSCallbackRow::fLoadCOSplusRow(string strCOS, int iROW)
{
 if (iROW <=0)             {return false;}
 if (strCOS.length() != 4) {return false;}
 this->strClassOfService = strCOS;
 this->iRow = iROW;
 return true;
}


/****************************************************************************************************
*
* Name:
*  Function: int fSelectPortPair(int intNum)
*
*
* Description:
*  <ALISteering> function implementation
*
*
* Details:
*   this function reads a string of numbers (1.. intMAX_ALI_PORT_PAIRS) and assigns "true" 
*   to an array of booleans at the position indicated by the number read. If the string 
*   is equal to "ALL", All positions are set to true.
*
*
* Parameters: 				
*   intNum                              int                             
*
* Variables:
*   boolFoundOne                        Local
*   boolPortPairInDatabase[]            Local   <ALISteering> 
*   i                                   Local   int counter
*   intDatabaseNum                      Local   <ALISteering>
*   intNUM_ALI_PORT_PAIRS               Global  int Number of ALI Databases
*   npos                                Library <cstring> constant
*   position                            Local   <cstring><size_t> 
*   stringTemp                          Local   <cstring>
*   VstringNumbers[]                    Global  <vector><cstring> list of the numbers 1 thru intNUM_ALI_PORT_PAIRS
*                                                                      
* Functions:
*   none
*
* Author(s):
*   Bob McCarthy 			Original: 2/1/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
int ALISteering::fSelectPortPair(int intNum)
{
 int i = 0;
 int j = intNum;

 do
  {
   i++;
   
   if (boolPortPairInDatabase[i]) {j--;}
   

  }  while (j > 0);
 
 return i;
}


int ALISteering::fLoadALIRequestFormat(string strInput)
{
 
 if      (strInput == "8")  {eRequestKeyFormat = FOURTEEN_DIGIT;}
 else if (strInput == "10") {eRequestKeyFormat = SIXTEEN_DIGIT;}
 else if (strInput == "E2") {eRequestKeyFormat = E2_PROTOCOL;}
 else                       {eRequestKeyFormat = NO_FORMAT; return 1;} 
             
 return 0;

}
/****************************************************************************************************
*
* Name:
*  Function: int LoadPortData( int intArg, string charArg , string stringArg, threadorPorttype enumThread )
*
*
* Description:
*  An Initialization Function
*
*
* Details:
*   this function loads an IP address and then Port Number into the thread's Port by parsing stringArg.
*   The input data shall be in the form XXX.XXX.XXX.XXX , XXXXX if the data is invalid the function generates 
*   an error and terminates program execution. 
*
*
* Parameters:
*   charArg                             <cstring>
*   enumThread                          <header.h> threadorPorttype enum 				
*   intArg                              int
*   stringArg                           <cstring>
*
* Variables:
*   ALIPort[][]                         Global  <ExperientCommPort> object 2D array
*   ANIPort[]                           Global  <ExperientCommPort> object array
*   CADPort[]                           Global  <ExperientCommPort> object array
*   CFG_MESSAGE_018                     Global  <defines.h> error message
*   CFG_MESSAGE_019                     Global  <defines.h> error message
*   CFG_MESSAGE_029                     Global  <defines.h> error message
*   CFG_MESSAGE_030                     Global  <defines.h> error message
*   charINI_FILE_PATH_AND_NAME          Global  <defines.h>
*   HOST_IP                             Global  <IP_Address> object 
*   i                                   Local   integer
*   index1                              Local   <cstring><size_t> for char position within a string
*   index2                              Local   <cstring><size_t> for char position within a string
*   index3                              Local   <cstring><size_t> for char position within a string
*   IPAddress                           Local   <IP_Address> object
*   j                                   Local   integer 
*   intLocalPort                        Global  <ExperientCommPort> member 
*   intMAX_ALI_PORT_PAIRS               Global  <defines.h>
*   intRemotePortNumber                 Global  <ExperientCommPort> member
*   npos                                Library <cstring> constant
*   NUM_ANI_PORTS_MAX                   Global  <defines.h>
*   NUM_CAD_PORTS_MAX                   Global  <defines.h>
*   position                            Local   <cstring><size_t>
*   stringAddress                       Global  <IP_Address> member
*   stringPort                          Local   <cstring> for error output
*   stringPortSuffix                    Local   <cstring> for error output
*   stringTemp1                         Local   <cstring>
*   stringTemp2                         Local   <cstring>
*                                                                      
* Functions:
*   .assign()                           Library <cstring>               (string)       
*   Console_Message()                   Global  <globalfunctions.h>     (void)
*   .c_str()                            Library <cstring>               (const char*)
*   .erase()                            Library <cstring>               (string)
*   exit()                              Library <stdlib.h>              (void)
*   .find_first_of()                    Library <cstring>               (size_t)
*   int2str()                           Global  <globalfunctions.h>     (string)
*   .length()                           Library <cstring>               (size_t)
*   Thread_Calling()                    Global  <globalfunctions.h>     (string)
*
* Author(s):
*   Bob McCarthy 			Original: 10/17/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
int LoadPortData( int intArg, string charArg , string stringArg, threadorPorttype enumThread )
{
 string                         stringTemp1 = "";
 string                         stringTemp2 = "";
 size_t                         index1,index2,index3;  
 int                            i,j;     
 IP_Address                     IPAddress;
 string                         stringPort;
 string                         stringPortSuffix="";

 extern ExperientCommPort       ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ]; 
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort       ANIPort[NUM_ANI_PORTS_MAX+1];
 extern ServerData       	ServerStatusTable[MAX_NUM_OF_SERVERS+1];
 extern RCC_Device              RCCdevice[NUM_WRK_STATIONS_MAX+1];
 
 if      (charArg == "A") {i = 1;}
 else if (charArg == "B") {i = 2;}
 else if (charArg == "S") {i = 3;}
 else                     {i = 0;}

 if (charArg !=""){stringPortSuffix = " "+charArg;}

 stringTemp1 = stringArg;
 stringPort = int2strLZ(intArg)+stringPortSuffix;

 // assign string from first number to delimeter
 index1 = stringTemp1.find_first_of("0123456789.");
 index2 = stringTemp1.find_first_of(",");
 
 if ((index1 == string::npos)||(index2 == string::npos)){SendCodingError("ExperientController.cpp - No number found or no comma found in fn LoadPortData()"); return 1; }

 IPAddress.stringAddress.assign(stringTemp1, index1 , (index2-index1) );

 // strip trailing blanks ...
 j = IPAddress.stringAddress.length()-1;
 while ((IPAddress.stringAddress[j] == ' ')&& (j > 0))
  {
   IPAddress.stringAddress.erase(j);
   j--;
  }
 
 // check for valid data (numbers or .'s) 

 index3 =  IPAddress.stringAddress.find_first_not_of("0123456789.");

 if (index3 != string::npos) 
  {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_029, Thread_Calling(enumThread).erase(3), stringPort ,stringArg);}
 if (!IPAddress.fIsValid_IP_Address())  
  {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_029, Thread_Calling(enumThread).erase(3), stringPort ,stringArg);}
// if (IPAddress.stringAddress == HOST_IP.stringAddress)
//  {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_030, Thread_Calling(enumThread).erase(3), stringPort, IPAddress.stringAddress, stringArg);}

 // load the port number
 stringTemp1.erase(index1 ,(index2-index1));
 stringTemp2 = "";
 index1 = stringTemp1.find_first_of("0123456789");
 if (index1 == string::npos){SendCodingError("ExperientController.cpp - No Number found in fn LoadPortData()"); return 1; }
 stringTemp2.assign(stringTemp1, index1, string::npos );

 // load the port
 switch (enumThread)
  {
   case MAIN:
        if (intArg > MAX_NUM_OF_SERVERS) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_033, int2str(MAX_NUM_OF_SERVERS));}
        ServerStatusTable[intArg].Sync_IP.stringAddress = IPAddress.stringAddress;
        intMASTER_SLAVE_SYNC_PORT_NUMBER = char2int(stringTemp2.c_str());       
        break;
   case ALI:
        if (intArg > intMAX_ALI_PORT_PAIRS) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_018, int2str(intMAX_ALI_PORT_PAIRS));}
        switch (i)
         {
          case 1: case 2:
               ALIPort[intArg] [i].RemoteIPAddress.stringAddress = IPAddress.stringAddress;
               ALIPort[intArg] [i].intRemotePortNumber = char2int(stringTemp2.c_str() );
               ALIPort[intArg] [i].intLocalPort = ALIPort[intArg] [i].intRemotePortNumber;
               break;
          default:
               ALIServicePort[intArg].RemoteIPAddress.stringAddress = IPAddress.stringAddress;
               ALIServicePort[intArg].intRemotePortNumber = char2int(stringTemp2.c_str() );
               ALIServicePort[intArg].intLocalPort = ALIServicePort[intArg].intRemotePortNumber;
               break;
         }
        break;
   case ANI:
        if (intArg > NUM_ANI_PORTS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_022, int2str(NUM_ANI_PORTS_MAX));}
        ANIPort[intArg].RemoteIPAddress.stringAddress = IPAddress.stringAddress;
        ANIPort[intArg].intRemotePortNumber = ANIPort[intArg].intLocalPort = char2int(stringTemp2.c_str());
        ANIPort[intArg].ePortConnectionType = UDP;
        break;
   case CAD:
        if (intArg > NUM_CAD_PORTS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_019, int2str(NUM_CAD_PORTS_MAX));}
        CADPort[intArg].RemoteIPAddress.stringAddress = IPAddress.stringAddress;
        CADPort[intArg].intRemotePortNumber = char2int(stringTemp2.c_str());
        CADPort[intArg].intLocalPort = CADPort[intArg].intRemotePortNumber;
        CADPort[intArg].ePortConnectionType = UDP;
        break;
   case RCC:
        if (intArg > NUM_WRK_STATIONS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_063, int2str(NUM_WRK_STATIONS_MAX));}
        RCCdevice[intArg].RCCremoteIPaddress.stringAddress = IPAddress.stringAddress;      
        RCCdevice[intArg].intRCClocalPortNumber = char2int(stringTemp2.c_str());
        RCCdevice[intArg].intRCCremotePortNumber = intRCC_REMOTE_TCP_PORT_NUMBER;        
        RCCdevice[intArg].boolContactRelayInstalled           = true;
        boolCONTACT_RELAY_INSTALLED                           = true;
        break;
   default:
        SendCodingError("ExperientController.cpp - coding error at spot 2 in  function LoadPortData");
  }             
      
 return 0;
}

Port_Connection_Type ConnectionType(string strInput)
{
 if (strInput == "TCP") {return TCP;}

 return UDP;

}



int LoadPortConnectionType( int intArg, string charArg , string stringArg, threadorPorttype enumThread )
{
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort       ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
 extern ExperientCommPort       ANIPort[NUM_ANI_PORTS_MAX+1];

 int                   i;
 Port_Connection_Type  ePortConnectionType;

 if      (charArg == "A") {i = 1;}
 else if (charArg == "B") {i = 2;}
 else if (charArg == "S") {i = 3;}
 else                     {i = 0;}

 ePortConnectionType = ConnectionType(stringArg);

 
 switch (enumThread)
  {
   case MAIN:
               
        break;
   case ALI:
        if (intArg > intMAX_ALI_PORT_PAIRS) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_018, int2str(intMAX_ALI_PORT_PAIRS));}
        switch (i)
         {
          case 1: case 2:
                 ALIPort[intArg] [i].ePortConnectionType = ePortConnectionType; break;
          default:
                 ALIServicePort[intArg].ePortConnectionType = ePortConnectionType; break;
         }
        break;
   case ANI:
        if (intArg > NUM_ANI_PORTS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_022, int2str(NUM_ANI_PORTS_MAX));}
        ANIPort[intArg].ePortConnectionType = ePortConnectionType;
        break;
   case CAD:
        if (intArg > NUM_CAD_PORTS_MAX) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_019, int2str(NUM_CAD_PORTS_MAX));}
        CADPort[intArg].ePortConnectionType = ePortConnectionType;
        break;
   default:
        SendCodingError("ExperientController.cpp - coding error in function LoadPortConnectionType");
  } 

 

 return 0;
}           

int LoadALIPortProtocolType( int i, string stringArg )
{
  int                           iswitch;
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];


 if (i > intMAX_ALI_PORT_PAIRS) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_018, int2str(intMAX_ALI_PORT_PAIRS));}


 if      (stringArg == "Modem")   {ALIPort[i][1].eALIportProtocol = ALIPort[i][2].eALIportProtocol = ALI_MODEM;   iswitch = 1;}
 else if (stringArg == "E2")      {ALIPort[i][1].eALIportProtocol = ALIPort[i][2].eALIportProtocol = ALI_E2;      iswitch = 2;}
 else if (stringArg == "Service") {ALIPort[i][1].eALIportProtocol = ALIPort[i][2].eALIportProtocol = ALI_SERVICE; iswitch = 3;} 
 else                             {return -1;}

 switch (iswitch)
  {
   case 1:
          break;
   case 2:
          if (ALIPort[i][1].ePortConnectionType == UDP) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_058c, int2str(i), "A+B", stringArg, ConnectionType(UDP));}
          break;
   case 3:
          if (ALIPort[i][1].ePortConnectionType == UDP) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_058c, int2str(i) , "A+B", stringArg, ConnectionType(UDP));}
          break;
   default:
          return -1;
  }      

 return 0;
}




bool Load_Gateway_Order()
{
 size_t         position = 0;
 string         stringTemp;
 string         strGateway;

 extern string            strGATEWAY_TRANSFER_ORDER;
 extern vector <string>   vGATEWAY_TRANSFER_ORDER;

 stringTemp = strGATEWAY_TRANSFER_ORDER;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   strGateway.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   strGateway = RemoveLeadingSpaces(strGateway);
   strGateway = RemoveTrailingSpaces(strGateway);

   vGATEWAY_TRANSFER_ORDER.push_back(strGateway);

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 strGateway.assign(stringTemp,0 ,string::npos);
 strGateway = RemoveLeadingSpaces(strGateway);
 strGateway = RemoveTrailingSpaces(strGateway);

 if (strGateway.empty()) {return false;}

 vGATEWAY_TRANSFER_ORDER.push_back(strGateway);

 return true;
}

bool Load_Phone_Ping_Monitor_List()
{
 size_t         position = 0;
 size_t         found;
 string         stringTemp;
 string         strNumber;
 int            intNumber;
 extern string            strTELEPHONE_PING_LIST;
 extern vector <int>      vTELEPHONE_PING_LIST;

 if (strTELEPHONE_PING_LIST == DEFAULT_VALUE_TELEPHONE_INI_KEY_8) {return false;}
 stringTemp = strTELEPHONE_PING_LIST;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   strNumber.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   strNumber = RemoveLeadingSpaces(strNumber);
   strNumber = RemoveTrailingSpaces(strNumber);
   found = strNumber.find_first_not_of("0123456789");
   if (found != string::npos)                          {return false;}
   intNumber = char2int(strNumber.c_str());
   vTELEPHONE_PING_LIST.push_back(intNumber);

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 strNumber.assign(stringTemp,0 ,string::npos);
 strNumber = RemoveLeadingSpaces(strNumber);
 strNumber = RemoveTrailingSpaces(strNumber);
 found = strNumber.find_first_not_of("0123456789");
 if (found != string::npos)                          {return false;}
 if (strNumber.empty()) {return false;}
 intNumber = char2int(strNumber.c_str());
 vTELEPHONE_PING_LIST.push_back(intNumber);

 return true;
}

/****************************************************************************************************
*
* Name:
*  Function: int Process_Previous_Shutdown_File()
*
*
* Description:
*  function used to check if the program had previously shut down abnormally 
*
*
* Details:
*   The function creates or modifies a file and replaces the stored number of previous
*   conecutive abnormal shutdowns and adds 1.  The sister function Delete_Orderly_Shutdown_File()
*   will set the number to zero during a normal shut down.
*
*
* Parameters: 				
*   none
*
* Variables:
*   charPREVIOUS_SHUT_DOWN_STATUS_FILE  Global  <defines.h>
*   intNumOfShutdowns                   Local   integer counter
*   LOG                                 Global  <header.h>
*   LOG_MESSAGE_519                     Global  <defines.h>
*   objMessage                          Local   <MessageClass> object
*   PreviousBoot                        Local   <SimpleIni.h><CSimpleIniA>  object
*   ptrToPreviousShutdownFile           Local   pointer to file
*   rc                                  Local   <SimpleIni.h><SI_Error> return code
*   stringNewValue                      Local   <cstring> holds string version of intNumOfShutdowns +1
*    
*                                                              
* Functions:
*   char2int()                          Local
*   enQueue_Message()                   Global  <globalfunctions.h>
*   fclose()                            Library <cstdio>
*   fopen()                             Library <cstdio>
*   .fMessage_Create()                  Global  <MessageClass> function
*   fputs()                             Library <cstdio>
*   .GetValue()                         Library <SimpleIni.h>
*   int2strLZ()                         Global  <globalfunctions.h>
*   .LoadFile()                         Library <SimpleIni.h>
*   .SaveFile()                         Library <SimpleIni.h>
*   .SetValue()                         Library <SimpleIni.h>
*
* Author(s):
*   Bob McCarthy 			Original: 3/30/2007
*                                       Updated : N/A
*
****************************************************************************************************/
int Process_Previous_Shutdown_File()
{
 int            intNumOfShutdowns;
 string         stringNewValue;
 string         stringLastErrorCode;
 string         stringLastErrorMessage;
 string         stringTimeStamp;
 string         stringSystemRebooted;
 string         stringSegFaultData;
 FILE *         ptrToPreviousShutdownFile;
 MessageClass   objMessage;
 string         strLogMessage = LOG_MESSAGE_519;
 int            intLogCode = LOG_ALARM;

    // load data from file
    CSimpleIniA PreviousBoot(true, false, true);
    SI_Error rc = PreviousBoot.LoadFile(charPREVIOUS_SHUT_DOWN_STATUS_FILE);
    if (rc < 0) 
     {
      // file does not exist.. this is OK.. create file with a 1 in unexpected shut downs
       
       ptrToPreviousShutdownFile = fopen (charPREVIOUS_SHUT_DOWN_STATUS_FILE, "at+" );
       fputs ("[STATISTICS]\n\nUNEXPECTED SHUTDOWNS  = 1\n",ptrToPreviousShutdownFile);
       fclose (ptrToPreviousShutdownFile);
      return 0;
     }
    else 
     { 
       intNumOfShutdowns = char2int(PreviousBoot.GetValue("STATISTICS", ERR_INI_KEY_1, "0" ));
       if(intNumOfShutdowns > 0)
        {
          // unexpected shutdown detected!
         stringLastErrorCode    = PreviousBoot.GetValue("STATISTICS", ERR_INI_KEY_4, DEFAULT_VALUE_ERR_INI_KEY_4 );
         stringLastErrorMessage = PreviousBoot.GetValue("STATISTICS", ERR_INI_KEY_5, DEFAULT_VALUE_ERR_INI_KEY_5 );
         stringTimeStamp        = PreviousBoot.GetValue("STATISTICS", ERR_INI_KEY_2, DEFAULT_VALUE_ERR_INI_KEY_2 );
         stringSystemRebooted   = PreviousBoot.GetValue("STATISTICS", ERR_INI_KEY_6, DEFAULT_VALUE_ERR_INI_KEY_6 );
         stringSegFaultData     = PreviousBoot.GetValue("STATISTICS", ERR_INI_KEY_7, DEFAULT_VALUE_ERR_INI_KEY_7 );
          
         if (stringSystemRebooted != DEFAULT_VALUE_ERR_INI_KEY_6) {intLogCode = LOG_ALARM; strLogMessage = LOG_MESSAGE_519r;}
         objMessage.fMessage_Create(intLogCode,519, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT,
                                    objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, strLogMessage, int2strLZ(intNumOfShutdowns));
         objMessage.stringEmailMessageBody.append("Last Exit Code     = " + stringLastErrorCode + '\n');
         cout << "Last Exit Code     = " + stringLastErrorCode << endl; 
         if (stringSegFaultData != DEFAULT_VALUE_ERR_INI_KEY_7)
          {
           objMessage.stringEmailMessageBody.append("Last Exit Message  = " + stringSegFaultData + '\n'); 
           cout << "Last Exit Message  = " + stringSegFaultData << endl; 
          }
         else
          { objMessage.stringEmailMessageBody.append("Last Exit Message  = " + stringLastErrorMessage + '\n'); 
           cout << "Last Exit Message  = " + stringLastErrorMessage << endl; 
          }
         objMessage.stringEmailMessageBody.append("System Rebooted    = " + stringSystemRebooted); 
          
         enQueue_Message(LOG,objMessage);
          

        }// end if(intNumOfShutdowns > 0)

       
       // RESET File
       // set unexpected shutdown counter to +1
       stringNewValue = int2strLZ(intNumOfShutdowns+1);
       PreviousBoot.Delete("STATISTICS", NULL);
      
       rc = PreviousBoot.SetValue("STATISTICS", ERR_INI_KEY_1, stringNewValue.c_str());
       if (rc < 0)
        {
         objMessage.fMessage_Create(LOG_WARNING,520, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                    objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_520); 
         enQueue_Message(LOG,objMessage);
        }
       rc = PreviousBoot.SaveFile(charPREVIOUS_SHUT_DOWN_STATUS_FILE);
       if (rc < 0)
        {
         objMessage.fMessage_Create(LOG_WARNING,520,__LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                    objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_520); 
         enQueue_Message(LOG,objMessage);
        }
       return 0;
     }
}// end Process_Previous_Shutdown_File()






/****************************************************************************************************
*
* Name:
*  Function: int Delete_Orderly_Shutdown_File()
*
*
* Description:
*  function used to remove the abnormal shutdown file after a normal shut down 
*
*
* Details:
*   The function removes the file charPREVIOUS_Thread_Trap_STATUS_FILE.  The function is called
*   during shutdown.
*
*
* Parameters: 				
*   none                             
*
* Variables:
*   charPREVIOUS_SHUT_DOWN_STATUS_FILE  Global  <defines.h>
*   intRC                               Local   integer return code
*    
*                                                              
* Functions:
*   .remove()                           Library <stdio.h>

*
* Author(s):
*   Bob McCarthy 			Original: 4/6/2008
*                                       Updated : N/A
*
****************************************************************************************************/ 
int Delete_Orderly_Shutdown_File()
{
 int intRC;
 intRC = remove ( charPREVIOUS_SHUT_DOWN_STATUS_FILE );  
 if(intRC){cout << "unable to delete shut down status file " << endl;}
 return intRC;
}//end Delete_Orderly_Shutdown_File()

int Delete_PID_File()
{
 int intRC;
 if (!fexists(charPID_FILE)) {return 0;}
 intRC = remove ( charPID_FILE );  
 if(intRC){cout << "unable to delete PID file " << endl;}
 return intRC;
}


int Delete_Debug_Files()
{
 int intRC;
 string strCommand = "rm ";
 string strInput = "";
 DIR *dp;
 struct dirent *dirp;
 vector <string> files;
 size_t found;
 size_t count = 0;
 string strFileNameandpath;

 strCommand += charLOG_FILE_PATH_NAME;
 strCommand += "/*";
 strCommand += char_DEBUG_LOG_FILE_NAME_SUFFIX;

 do
  { 
   cout << "Are you sure you wish to delete the debug files ? y/n :";
   getline (cin, strInput);
  }
 while ((strInput != "y")&&(strInput != "n"));

 if (strInput == "y")
  {

    if((dp  = opendir(charLOG_FILE_PATH_NAME)) == NULL) {
        cout << "Error(" << errno << ") opening " << charLOG_FILE_PATH_NAME << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(dirp->d_name);
    }
    closedir(dp);  


    for (unsigned int i = 0;i < files.size();i++) 
     {
      found = files[i].find(char_DEBUG_LOG_FILE_NAME_SUFFIX);
      if (found != string::npos)
       {
        strFileNameandpath = charLOG_FILE_PATH_NAME;
        strFileNameandpath += "/";
        strFileNameandpath += files[i];
        intRC = remove ( strFileNameandpath.c_str() ); 
        if(intRC){cout << "unable to delete file: " << files[i] << " Error(" << errno << ")" << endl;} 
        else     {count++; cout << "deleted file: " << files[i] << endl;} 
       }

     }
    cout << "deleted " << count << " file(s)" << endl;


   return intRC;
  }
 return 0;

}//end Delete_Debug_Files()


/****************************************************************************************************
*
* Name:
*  Function: string get_ip_address(string stringDevice)
*
*
* Description:
*  Utility function 
*
*
* Details:
*   This function returns the IP address of charDEFAULT_GATEWAY_DEVICE
*
* Parameters: 				
*   none                             
*
* Variables:
*   AF_INET                             Global  <net/if.h> constant
*   charDEFAULT_GATEWAY_DEVICE          Global  <defines.h> constant
*   charVal                             Local   used to convert address data to unsigned char
*   fd                                  Local   int
*   ifr                                 Global  <net/if.h> ifreq structure
*   intVal                              Local   used to convert address data to int
*   SIOCGIFADDR                         Global  <sys/ioctl.h> constant
*   SOCK_DGRAM                          Global  <net/if.h> constant
*   stringOutput                        Local   <cstring> output string
*   rc                                  Local   <SimpleIni.h><SI_Error> return code
*    
*                                                              
* Functions:
*   exit()                              Library <stdlib.h>
*   int2str()                           Global  <globalfunctions.h>
*   ioctl()                             Library <sys/ioctl.h>
*   socket()                            Library <net/if.h>
*   strcpy()                            Library <cstring>
*
* Author(s):
*   Bob McCarthy 			Original: 4/6/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
string get_ip_address(string stringDevice)
{
     
     struct             ifreq ifr;
     int                fd;
     unsigned char      charVal;
     int                intVal;
     string             stringOutput;
     
     fd = socket(AF_INET,SOCK_DGRAM, 0);
     if (fd >= 0)
      {
       strcpy(ifr.ifr_name, stringDevice.c_str());
       if (ioctl(fd, SIOCGIFADDR, &ifr) == 0) 
        {
         for( int i=2; i<6; i++)
          {
           charVal = (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[i];
           intVal  = (int)charVal;
           stringOutput += int2str(intVal);
           if (i != 5) {stringOutput += "."; }
          }// end for i

        }
       else {Console_Message("CFG |", CFG_MESSAGE_004);Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_004);}  

      }// end if (fd >= 0)
     else {Console_Message("CFG |", CFG_MESSAGE_005); Abnormal_Exit(MAIN, EX_CONFIG , CFG_MESSAGE_005);}

     return stringOutput;
}// end string get_ip_address()



int IndexOfNewMainTableEntry(ExperientDataClass objData)
{
  // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vMainWorkTable;

 vMainWorkTable.push_back(objData);

 return vMainWorkTable.size() - 1;
}


void Manual_Bid( ExperientDataClass objData, int iIndex, bool boolRebidManualBid) 
{
 int                                            i,j;
 extern vector <ExperientDataClass>             vMainWorkTable;
 WorkStation                                    NotifyWorkstation;
 int                                            intStatus = XML_STATUS_CODE_ALI_REBID;
 MessageClass		                        objMessage;

 i = iIndex;


 // build new record if not a rebid
 if ((!boolRebidManualBid)||(i < 1))
  {
   objData.fLoad_Trunk(SOFTWARE, MAIN, strPREFERRED_MANUAL_ALI_BID_TRUNK , PREFERRED_MANUAL_ALI_BID_TRUNK );
   j = objData.CallData.intPosn;
   if (j < 0) { return;}                                                         // error tbc
   objData.fSetUniqueCallID(j); intStatus = XML_STATUS_CODE_ALI_MANUAL_BID;
   objData.fLoad_ConferenceData(SOFTWARE, MAIN, "0", 0);
   i = IndexOfNewMainTableEntry(objData);
   clock_gettime(CLOCK_REALTIME, &objData.timespecTimeRecordReceivedStamp);
   objData.CallData.boolCallBackVerified = true;
   //Add Psition to Conference History by adding to active and removing
   objData.CallData.fConferenceStringAdd(POSITION_CONF, objData.CallData.intPosn);     
   objData.CallData.fConferenceStringRemove(objData.CallData.intPosn);                     
   objData.FreeswitchData.objChannelData.fLoadConferenceDisplayP(j);

   objData.CallData.ConfData.vectConferenceChannelData.push_back(objData.FreeswitchData.objChannelData);    //
   objData.fLoadCallOriginator(POSITION_CONF, objData.CallData.intPosn);
  }

 // note if the workstation returns the wrong CALL ID on a MANUALBID rebid there is no way to verify..............
 // it won't cause any problems only workstation clutter ........

 vMainWorkTable[i]                                = objData;
 vMainWorkTable[i].ALIData.boolALIRecordReceived  = false;
 vMainWorkTable[i].intCallStateCode               = CallStateCode(XML_CALL_STATE_MANUAL_BID);
 vMainWorkTable[i].ALIData.intALIState            = intStatus;
 if(boolRebidManualBid){vMainWorkTable[i].ALIData.stringALIRebidTimeStamp   = objData.stringTimeOfEvent;}
 else                  {vMainWorkTable[i].stringRingingTimeStamp = objData.stringTimeOfEvent;}
 
 vMainWorkTable[i].boolRinging                             = true;
 objData.CallData.stringPosn                               = objData.CallData.stringTrunk;
 vMainWorkTable[i].WindowButtonData.boolALIRebidButton     = false;
 vMainWorkTable[i].TextData.TDDdata.boolTDDWindowActive    = false;
 objData.enumANIFunction                                   = vMainWorkTable[i].enumANIFunction = ALI_REQUEST;

 // send bid to ALI                        
 SendANItoALI(objData);

 // Update Workstation
 vMainWorkTable[i].fCallInfo_XML_Message(intStatus);
 vMainWorkTable[i].boolBroadcastXMLtoSinglePosition = false;
 Queue_WRK_Event(vMainWorkTable[i]);

}

void UpdateCadfromManualBid(ExperientDataClass objData)
{
 MessageClass objMessage;
 WorkStation  NotifyWorkstation;

 // output error code ???  TBC
 if (objData.ALIData.stringAliText.length() == 0){return;}

 objData.ALIData.stringType                = "2"    ; // just need to put something in there ... 

 objMessage.fMessage_Create(0,0, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT,
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "");
 objData.stringTimeOfEvent      = objMessage.stringTimeStamp;
 objData.WindowButtonData.fSetButtonsForManualALIReceived();
 objData.ALIData.boolALIRecordReceived    = true;
 objData.CallData.intPosn                 = objData.intWorkStation;
 objData.CallData.stringPosn              = int2strLZ(objData.CallData.intPosn);
 objData.boolBroadcastXMLtoSinglePosition = true;

 objData.fCallInfo_XML_Message(XML_STATUS_CODE_SEND_TO_CAD, true);

 SendALItoCAD_SinglePosition(objData);

 objData.stringXMLString = NotifyWorkstation.fCreateAlert(WORKSTATION_ALI_SENT_TO_CAD_MESSAGE, objData.CallData);
 Queue_WRK_Event(objData);
}



// don't know if we want this ..... abandoned rebid ... TBC ......
void UpdateCadfromWorkstationData(ExperientDataClass objData)
{
 MessageClass objMessage;
 WorkStation  NotifyWorkstation;

 // output error code ???  TBC
 if (objData.ALIData.stringAliText.length() == 0){return;}

 objData.ALIData.stringType                = "2"    ; // just need to put something in there ... 

 objMessage.fMessage_Create(0,0, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "");
 objData.stringTimeOfEvent      = objMessage.stringTimeStamp;
 objData.ALIData.boolALIRecordReceived           = true;
 objData.WindowButtonData.boolCADSendButton      = true;
 objData.CallData.intPosn                = objData.intWorkStation;
 objData.CallData.stringPosn             = int2strLZ(objData.CallData.intPosn);
 objData.fCallInfo_XML_Message(XML_STATUS_CODE_SEND_TO_CAD,true);
 SendALItoCAD_SinglePosition(objData);
 objData.boolBroadcastXMLtoSinglePosition = true;
 objData.stringXMLString = NotifyWorkstation.fCreateAlert(WORKSTATION_ALI_SENT_TO_CAD_MESSAGE, objData.CallData);

 Queue_WRK_Event(objData);
}








int FindOldestRingingCall()
{
 extern vector <ExperientDataClass>     vMainWorkTable;
 vector <ExperientDataClass>::size_type sz; 


 // Note Table should be locked prior to calling this function!

 // the oldest ringing call will be the first one found in the table

 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++)
  {
   if ((vMainWorkTable[i].boolRinging)&&(!vMainWorkTable[i].boolConnect)&&(!vMainWorkTable[i].boolAbandoned)&&(!vMainWorkTable[i].boolDisconnect)) { return i;}
  }    
 return -1;
}

int SharedLineThatsAvaiableForBarge(int index) {
 int         	iLineNumber             = -1;
 int 		intPosition;

 extern vector <ExperientDataClass>             vMainWorkTable;
 
/* Determine if Position is on a shared Line 
   then return SharedLine Number.... if Position is not on Hold.
*/
 
 for (unsigned int i = 0; i < vMainWorkTable[index].FreeswitchData.vectPostionsOnCall.size(); i++)
  {
   intPosition = vMainWorkTable[index].FreeswitchData.vectPostionsOnCall[i].iPositionNumber;
   iLineNumber = vMainWorkTable[index].FreeswitchData.fLineNumberofPositiononCall(intPosition);
//    cout << "line number is -> " << iLineNumber << endl;
//   vMainWorkTable[index].FreeswitchData.fDisplay();
   if(TelephoneEquipment.fLineNumberAtPositionIsShared(iLineNumber, intPosition)) {
     return iLineNumber;

    if (!vMainWorkTable[index].CallData.fIsOnHold(intPosition)) {
//      cout << "returning -> " << iLineNumber << endl; 
     return iLineNumber;
    }
   }
  } 
 return -1;
}

int LineAvailableForOnHoldPickoff(int index) {
 int         	iLineNumber             = -1;
 int 		intPosition;
 int            intParticipants;

 extern vector <ExperientDataClass>             vMainWorkTable;

/* Determine Size of Call must equal 2
   Line must be shared.
   if a Position is on Hold return line number... 
   if no Positions or not on hold return -1
*/

 intParticipants = vMainWorkTable[index].FreeswitchData.vectPostionsOnCall.size() + vMainWorkTable[index].FreeswitchData.vectDestChannelsOnCall.size(); 
// intPositionsHistory = vMainWorkTable[index].CallData.ConfData.fNumofPositionsInHistory();
// cout << "participants -> " << intParticipants << endl; 
 if (intParticipants != 2 )    {return -1;}

 for (unsigned int i = 0; i < vMainWorkTable[index].FreeswitchData.vectPostionsOnCall.size(); i++) {
   intPosition = vMainWorkTable[index].FreeswitchData.vectPostionsOnCall[i].iPositionNumber;
   iLineNumber = vMainWorkTable[index].FreeswitchData.fLineNumberofPositiononCall(intPosition);
 //   cout << "line number is -> " << iLineNumber << endl;
   if(TelephoneEquipment.fLineNumberAtPositionIsShared(iLineNumber, intPosition)) {

    if (vMainWorkTable[index].CallData.fIsOnHold(intPosition)) {
 //     cout << "returning -> " << iLineNumber << endl; 
     return iLineNumber;
    }
   }
 }
 return -1;
}

int LineNumberForOffHold(int index, int iPosition) {
 int         	iLineNumber             = -1;
 int 		intPosition;
 int            intNumberofPositions;

 extern vector <ExperientDataClass>             vMainWorkTable;

 intNumberofPositions = vMainWorkTable[index].FreeswitchData.vectPostionsOnCall.size(); 

// cout << "# Positions -> " << intNumberofPositions << endl; 
 if (intNumberofPositions < 1 ) {return -1;}

 for (unsigned int i = 0; i < vMainWorkTable[index].FreeswitchData.vectPostionsOnCall.size(); i++) {
   intPosition = vMainWorkTable[index].FreeswitchData.vectPostionsOnCall[i].iPositionNumber;
   if (iPosition != intPosition) { continue; }

   iLineNumber = vMainWorkTable[index].FreeswitchData.fLineNumberofPositiononCall(intPosition);
//   cout << "line number is -> " << iLineNumber << endl;
   if (vMainWorkTable[index].CallData.fIsOnHold(intPosition)) {
//    cout << "returning -> " << iLineNumber << endl; 
    return iLineNumber;
   }
  
 }
 return -1;
}

int NumberOfCallsOnHold(int iPosition) {
 int 						iNumberofHolds = 0;
 extern vector <ExperientDataClass>             vMainWorkTable;

 for (unsigned int i = 0; i < vMainWorkTable.size(); i++) {

  for (unsigned int j = 0; j < vMainWorkTable[i].FreeswitchData.vectPostionsOnCall.size(); j++) {
   if (vMainWorkTable[i].FreeswitchData.vectPostionsOnCall[j].iPositionNumber != iPosition) {continue;}
   if (vMainWorkTable[i].CallData.fIsOnHold(iPosition)) {
    iNumberofHolds++;
   }
  }
 }
 return iNumberofHolds;
}

bool Check_Same_Call_WRK_Functions(ExperientDataClass* objData, int iIndex)
{
 MessageClass                   objMessage;
 WorkStation                    NotifyWorkstation;
 ExperientDataClass             objDataCopy(objData);                           //copy of objData
 
 extern vector <ExperientDataClass>             vMainWorkTable;


 // if CALL-ID is different
 if (vMainWorkTable[iIndex].CallData.intUniqueCallID != objData->CallData.intUniqueCallID)
  {
   // send popup ALARM and reset workstation display 

   //test 
   objMessage.fMessage_Create(LOG_WARNING,135, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objDataCopy.fCallData(), objBLANK_TEXT_DATA, objBLANK_KRN_PORT,
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_135 , int2strLZ(objDataCopy.intWorkStation)); 
   enQueue_Message(MAIN,objMessage);

   objDataCopy.stringXMLString = NotifyWorkstation.fCreateAlert(WRK_MESSAGE_718w, objData->CallData);
   objDataCopy.boolBroadcastXMLtoSinglePosition = true;
   objDataCopy.boolReloadWorkstationGrid        = true;
   Queue_WRK_Event(objDataCopy);

   return false;
  }


// data checks good return true ..
return true;

}

void Queue_RCC_Event(rcc_state enumRCCstate , ExperientDataClass objData, int intWorkstation, rcc_function enumRCCfunction)
{
 int                                            intRC;
 extern queue <ExperientDataClass> 	        queue_objRCCData;
 
 objData.enumRCCstate    = enumRCCstate;
 objData.enumRCCfunction = enumRCCfunction;
 objData.intWorkStation  = intWorkstation;
 
 if (!intWorkstation) {return;}


 intRC = sem_wait(&sem_tMutexRCCInputQ);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCInputQ, "sem_wait@sem_tMutexRCCInputQ in Queue_RCC_Event()", 1);}

 queue_objRCCData.push(objData);
 
 sem_post(&sem_tMutexRCCInputQ);
 sem_post(&sem_tRCCflag); 
}

void Queue_CTI_Event(ExperientDataClass objData)
{
 int                                            intRC;
 extern queue <ExperientDataClass> 	        queue_objCTIData;
 
 intRC = sem_wait(&sem_tMutexCTIInputQ);
 if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexCTIInputQ, "sem_wait@sem_tMutexCTIInputQ in Queue_CTI_Event()", 1);}

 queue_objCTIData.push(objData);
 
 sem_post(&sem_tMutexCTIInputQ);
 sem_post(&sem_tCTIflag); 
}



void Queue_WRK_Event(ExperientDataClass objData)
{
 int                                            intRC;
 extern queue <ExperientDataClass> 	        queue_objWRKData;
 extern deque <ExperientDataClass>              vWorkstationHistory;
 size_t                                         sz;
 size_t 					found;
 bool                                           boolReplaced    = false; 
 enum  XML_Message_Type                         {eOTHER, eCALL, ePSAPSTATUS, eRESET_TRANSFER_WINDOW};

 XML_Message_Type 				XMLmessageType;
                                                                                 
 intRC = sem_wait(&sem_tMutexWorkstationHistory);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexWorkstationHistory, "sem_wait@sem_tMutexWorkstationHistory in Queue_WRK_Event()", 1);}

 deque <ExperientDataClass>::iterator it = vWorkstationHistory.begin();


 XMLmessageType = eOTHER;

 found = objData.stringXMLString.find(XML_NODE_PSAP_DATA);
 if (found != string::npos) {XMLmessageType = ePSAPSTATUS;}

 found = objData.stringXMLString.find(XML_NODE_RESET_TRANSFER_WINDOW);
 if (found != string::npos) {XMLmessageType = eRESET_TRANSFER_WINDOW;}
 
 found = objData.stringXMLString.find(XML_NODE_CALL_INFO);
 if (found != string::npos) {XMLmessageType = eCALL;}

 if (XMLmessageType == eOTHER) {boolReplaced = true;} 

 sz = vWorkstationHistory.size();

 for(unsigned int i = 0; i < sz; i++)
  {
   if (boolReplaced)          {continue;}

   switch (XMLmessageType)
    {
     case eCALL:
          found = vWorkstationHistory[i].stringXMLString.find(XML_NODE_CALL_INFO);
          if (found == string::npos) {break;}
          if (vWorkstationHistory[i].CallData.intUniqueCallID == objData.CallData.intUniqueCallID)
           { 
           // cout << "conv obj: " << objData.TextData.strConversation << endl; 
           // cout << "History obj: " << vWorkstationHistory[i].TextData.strConversation << endl;

            vWorkstationHistory[i] = objData;
            boolReplaced = true;
           }
        //  cout << "ecall" << endl;
          break;
     case eRESET_TRANSFER_WINDOW:
          found = vWorkstationHistory[i].stringXMLString.find(XML_NODE_RESET_TRANSFER_WINDOW);
          if (found == string::npos) {break;}    
          vWorkstationHistory[i] = objData;
          boolReplaced = true;
          break;
     case ePSAPSTATUS:
          found = vWorkstationHistory[i].stringXMLString.find(XML_NODE_PSAP_DATA);
          if (found == string::npos) {break;}
          vWorkstationHistory[i] = objData;
          boolReplaced = true;
          break;
     default:
          // keep looping  i.e boolReplaced = false;
          break;
    }

  }
 if (!boolReplaced) {vWorkstationHistory.push_back(objData);}

 sz = vWorkstationHistory.size();
 if (sz > WORKSTATION_TRANSACTION_HISTORY_SIZE)
  {
   vWorkstationHistory.pop_front();
  }

 sem_post(&sem_tMutexWorkstationHistory);
 
 intRC = sem_wait(&sem_tMutexWRKInputQ);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexWRKInputQ, "sem_wait@sem_tMutexWRKInputQ in Queue_WRK_Event()", 1);}

 queue_objWRKData.push(objData);
 
 sem_post(&sem_tMutexWRKInputQ);
 sem_post(&sem_tWRKFlag); 
}

void Queue_LIS_Event(ExperientDataClass objData)
{
 int                                            intRC;
 extern queue <ExperientDataClass> 	        queue_objLISData;
 
 intRC = sem_wait(&sem_tMutexLISInputQ);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexLISInputQ, "sem_wait@sem_tMutexLISInputQ in Queue_LIS_Event()", 1);}

 queue_objLISData.push(objData);
 
 sem_post(&sem_tMutexLISInputQ);
 sem_post(&sem_tLISflag); 
}

void Queue_ANI_Event(ExperientDataClass objData)
{
 int                                            intRC;
 extern queue <ExperientDataClass> 	        queue_objANIData;
 
 intRC = sem_wait(&sem_tMutexANIInputQ);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexANIInputQ, "sem_wait@sem_tMutexANIInputQ in Queue_ANI_Event()", 1);}

 queue_objANIData.push(objData);
 
 sem_post(&sem_tMutexANIInputQ);
 sem_post(&sem_tANIFlag); 
}



void StartUpPingChecks()
{
 CheckAllPortsDown(MAIN, true,false);
 CheckAllPortsDown(ANI, true,false);
 CheckAllPortsDown(ALI, true,false);
 CheckAllPortsDown(CAD, true,false);
}


bool CheckForDirectory(string stringFullpath)
{
 int		intRC;
 struct stat 	st;

 if(stat((char*) stringFullpath.c_str() ,&st) == 0){return true;}
       
 intRC = mkdir((char*) stringFullpath.c_str(), S_IRWXU|S_IRWXG|S_IRWXO|S_IWOTH|S_ISVTX);
      
 if (intRC)
  {Abnormal_Exit(MAIN, EX_CONFIG, CFG_MESSAGE_041, stringFullpath, int2str(intRC) ); return false;}
 else
  {Console_Message("CFG |", CFG_MESSAGE_042, stringFullpath);}
 
 return true;
} 
   
void CheckForPhoneALIdirectory()
{
 string strWorkstationDirectory;
 extern bool CheckForDirectory(string stringFullpath);

 if (!CheckForDirectory(charHTML_DIR_PATH))              {return;}
 if (!CheckForDirectory(charPHONE_ALI_FILE_PATH_PREFIX)) {return;}
 for (int i = 1; i < NUM_WRK_STATIONS_MAX+1; i++) {
   strWorkstationDirectory = charPHONE_ALI_FILE_PATH_PREFIX;
   strWorkstationDirectory += "/";
   strWorkstationDirectory += int2strLZ(i);
   if (!CheckForDirectory(strWorkstationDirectory)) {return;}     
 }     
 return;
} 

int findServerNumber(string ipaddress, int CaseSwitch)
{
 extern ServerData                       ServerStatusTable                       [MAX_NUM_OF_SERVERS+1];

 for(int i = 1; i<= intNUM_OF_SERVERS; i++)
  {
   switch (CaseSwitch)
    {
     case 0:
            if (ServerStatusTable[i].Gateway_IP.stringAddress == ipaddress){return i;}
            break;
     default:
            if (ServerStatusTable[i].Sync_IP.stringAddress == ipaddress){return i;}
    }// end switch
  }
 return 0;
}

void Force_Disconnect(string strUniqueCallID, bool boolSendWarning)
{
 // worktable must be locked before calling this function

 extern vector <ExperientDataClass>     vMainWorkTable;
 int                                    Index;
 MessageClass                           objMessage;
 Call_Data                              objCallData;

 Index = IndexWithCallUniqueIDMainTable(strUniqueCallID);
 if (Index >= 0)
  { 
   if (boolSendWarning)
    {
     vMainWorkTable[Index].fSetANIfunction(FORCE_DISCONNECT);
     objMessage.fMessage_Create(LOG_WARNING,146, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[Index].CallData, vMainWorkTable[Index].TextData, objBLANK_KRN_PORT,
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_146); 
     enQueue_Message(MAIN,objMessage);
    }
   if (vMainWorkTable[Index].boolAbandoned)
    {
     Take_Control(Index, vMainWorkTable[Index]);
     return;
    }
   vMainWorkTable[Index].enumANIFunction = FORCE_DISCONNECT;  
   Queue_ANI_Event(vMainWorkTable[Index]); 
  }
 else
  {
   if(objCallData.fLoadCallUniqueID(strUniqueCallID))
    {
     objMessage.fMessage_Create(LOG_WARNING,147, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_147); 
     enQueue_Message(MAIN,objMessage);
    }
  }
} 

void Check_MSRP_NO_ANSWER_Calls(int count){
 extern vector <ExperientDataClass>     vMainWorkTable;
 struct timespec                        timespecTimeNow;
 vector <ExperientDataClass>::size_type sz; 
 int                                    intRC;
 MessageClass                           objMessage;
 long double                            ldTimeDiff = 0.0;
 string                                 strMessage = "Please make a voice call to 911.  There is no text service to 911 available at this time.";
 string                                 strTemp;
 bool                                   bPreviousALI;
 extern long double                     ldMSRP_RING_TIMEOUT;

 //count is 1 to 1000 each loop takes at least intMAIN_SCOREBOARD_INTERVAL_NSEC  .06 sec 
 // will take appx 1 min to reach 1000
  if(!(count % 50 == 0)) {return;}     // every 1.2 secondsish



 // lock main work table
 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn Check_MSRP_NO_ANSWER_Calls", 1);}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++) {
  if (vMainWorkTable[i].boolMSRPnoAnswer) {continue;}
  if (!vMainWorkTable[i].boolSMSmessage)  {continue;}
  if (vMainWorkTable[i].boolConnect)      {continue;}
  if (vMainWorkTable[i].boolAbandoned)    {continue;}
  if (vMainWorkTable[i].boolDisconnect)   {continue;}
  
   ldTimeDiff = time_difference(timespecTimeNow, vMainWorkTable[i].timespecRinging);

   if (ldTimeDiff > ldMSRP_RING_TIMEOUT) {

       vMainWorkTable[i].boolMSRPnoAnswer = true;
       vMainWorkTable[i].enumANIFunction   = SMS_SEND_TEXT;
       vMainWorkTable[i].TextData.TextType = MSRP_MESSAGE;
       vMainWorkTable[i].TextData.SMSdata.strSMSmessage = strMessage;
       Queue_ANI_Event(vMainWorkTable[i]);

       strTemp = Create_Message(TDD_DISPATCHER_SAYS_PREFIX, get_Local_Time_Stamp(), "P"+int2str(0));
       strTemp += "\n";
       strTemp += strMessage;                   
       strTemp += "\n";
       bPreviousALI     = (vMainWorkTable[i].ALIData.boolALIRecordReceived);                      
       vMainWorkTable[i].enumANIFunction = TDD_DISPATCHER_SAYS;
       vMainWorkTable[i].TextData.TDDdata.fLoadTDDReceived(strTemp,true,false);
       vMainWorkTable[i].TextData.TDDdata.fSetWindow(true);
       vMainWorkTable[i].enumThreadSending = MAIN;
       vMainWorkTable[i].fAddSentenceToTextConversation(strTemp);
       vMainWorkTable[i].TextData.TDDdata.boolTDDthresholdMet = true;
       vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_SMS_TEXT_SENT, bPreviousALI);
       // send to WRK
       vMainWorkTable[i].CallData.fLoadPosn(0);
       vMainWorkTable[i].boolBroadcastXMLtoSinglePosition = false;
       Queue_WRK_Event(vMainWorkTable[i]);

       // Hangup
       experient_nanosleep(THREE_TENTHS_SEC_IN_NSEC);
       vMainWorkTable[i].enumANIFunction = MSRP_HANGUP;
       vMainWorkTable[i].intWorkStation = 0;
       vMainWorkTable[i].boolAbandoned = true;
       Queue_ANI_Event(vMainWorkTable[i]);

   }
 }

 sem_post(&sem_tMutexMainWorkTable);
}
void Check_Single_Channel_Calls(int count)
{
 extern vector <ExperientDataClass>     vMainWorkTable;
 struct timespec                        timespecTimeNow;
 vector <ExperientDataClass>::size_type sz; 
 int                                    intRC;
 MessageClass                           objMessage;
 long double                            ldTimeDiff = 0.0;
 struct timespec                        timespecForcedDelay;
 bool                                   boolA, boolB, boolC, boolD, boolE;
 timespecForcedDelay.tv_nsec = intMAIN_SCOREBOARD_INTERVAL_NSEC;


 //count is 1 to 1000 each loop takes at least intMAIN_SCOREBOARD_INTERVAL_NSEC  .06 sec 
 // will take appx 1 min to reach 1000
  if(!(count % 12 == 0)) {return;}     // every 5 secondsish


  // lock main work table
 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn Check_Single_Channel_Calls()", 1);}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++)  {
   boolA = (!vMainWorkTable[i].boolForceDisconnect);
   boolB = (!CheckforManualBidTrunk(vMainWorkTable[i].CallData.intTrunk));
   //check No Transfer Data,
   boolC = vMainWorkTable[i].CallData.vTransferData.empty();
   //check Single channel
   boolD = ((vMainWorkTable[i].FreeswitchData.vectPostionsOnCall.size() + vMainWorkTable[i].FreeswitchData.vectDestChannelsOnCall.size() ) == 1);
   //make sure time is not set if not single channel
   if (!boolD) {vMainWorkTable[i].timespecSingleChannel.tv_sec = vMainWorkTable[i].timespecSingleChannel.tv_nsec = 0;}
 //  else {cout << "SINGLE CHANNEL" << endl;}
   // Check if Time in single channel is set
   boolE = ( (vMainWorkTable[i].timespecSingleChannel.tv_sec == 0) && (vMainWorkTable[i].timespecSingleChannel.tv_nsec == 0) );
   if (vMainWorkTable[i].boolSMSmessage) {continue;}
   if (boolA && boolB && boolC && boolD) {
      if (boolE) {
        // set single channel time 
        clock_gettime(CLOCK_REALTIME, &vMainWorkTable[i].timespecSingleChannel);
      }
      else {
        // check time in single channel mode
        ldTimeDiff = time_difference(timespecTimeNow, vMainWorkTable[i].timespecSingleChannel);
        if (ldTimeDiff > MAX_ALLOWABLE_SINGLE_CHANNEL_DURATION_SECONDS) {
          // Only do one at a time.  Function called frequently
          vMainWorkTable[i].boolForceDisconnect = true;
         // cout << "send from Main ...." << endl;
          Force_Disconnect(vMainWorkTable[i].CallData.stringUniqueCallID, false);
          sem_post(&sem_tMutexMainWorkTable);
          return;
        }//if (ldTimeDiff > MAX_ALLOWABLE_SINGLE_CHANNEL_DURATION_SECONDS) 
      } // if (boolE) else 
   }// if (boolA && boolB && boolC && boolD)
   else {
    vMainWorkTable[i].timespecSingleChannel.tv_sec = 0;
    vMainWorkTable[i].timespecSingleChannel.tv_nsec = 0; 
   }
 }//  for (unsigned int i = 0; i < sz; i++)

 sem_post(&sem_tMutexMainWorkTable);
}



void Check_Stale_Calls(int count)
{
 extern vector <ExperientDataClass>     vMainWorkTable;
 struct timespec                        timespecTimeNow;
 vector <ExperientDataClass>::size_type sz; 
 int                                    intRC;
 MessageClass                           objMessage;
 long double                            ldTimeDiff = 0.0;
 struct timespec                        timespecForcedDelay;
 bool                                   boolA, boolB, boolC;
 timespecForcedDelay.tv_nsec = intMAIN_SCOREBOARD_INTERVAL_NSEC;

 //count is 1 to 1000 each loop takes at least intMAIN_SCOREBOARD_INTERVAL_NSEC  .06 sec 
 // will take appx 1 min to reach 1000
  if(!(count % 10 == 0)) {return;}     // every 6 secondsish

  // lock main work table
 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn Check_Stale_Calls()", 1);}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++)
  {
   ldTimeDiff = time_difference(timespecTimeNow, vMainWorkTable[i].timespecTimeRecordReceivedStamp);
   boolA = (ldTimeDiff > MAX_ALLOWABLE_CALL_DURATION_SECONDS );
   boolB = (!vMainWorkTable[i].boolForceDisconnect);
   boolC = (!CheckforManualBidTrunk(vMainWorkTable[i].CallData.intTrunk));
   if (boolA && boolB && boolC)
    {
     // Only do one at a time.  Function called frequently
     vMainWorkTable[i].boolForceDisconnect = true;
     Force_Disconnect(vMainWorkTable[i].CallData.stringUniqueCallID);
     sem_post(&sem_tMutexMainWorkTable);
     return;
    }

  }
 sem_post(&sem_tMutexMainWorkTable);
}


void Check_Disconnected_Calls(int count) {
 extern vector <ExperientDataClass>     vMainWorkTable;
 extern int                             intSecondsRebidsActivePostDisconnect;
 extern int                             intSecondsToKeepDisconnectInTable;
 vector <ExperientDataClass>::size_type sz; 
 int                                    intRC;
 MessageClass                           objMessage;
 struct timespec                        timespecTimeNow, timespecTimeDisconnect;
 long double                            ldTimeDiff, ldLargestDiff = 0.0;
 unsigned int                           iCount = 0;
 int                                    iOldestRecord = -1;
 size_t                                 found;
 string                                 strUTCtimestamp = "";
 char*                                  chUTCtimestamp;
 time_t                                 t;
 struct tm                              result;

 //count is 1 to 1000 each loop takes at least intMAIN_SCOREBOARD_INTERVAL_NSEC  .06 sec 
 // will take appx 1 min to reach 1000
 if (count < 1000) {return;}


 // lock main work table
 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn Check_Disconnected_Calls()", 1);}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++) {
  if (vMainWorkTable[i].boolDisconnect) {
/*
  stringDisConnectTimeStamp Data ->
  2023/01/16 02:23:29.563 UTC | Sun 2023/01/15 19:23:29 MST |
*/

    found = vMainWorkTable[i].stringDisConnectTimeStamp.find_first_of(".");
    if (found != string::npos) {
     strUTCtimestamp = vMainWorkTable[i].stringDisConnectTimeStamp.substr(0,found);
    }
    else {
     // do we delete at this time ?????? 
     SendCodingError("ExperientController.cpp - coding error in function Check_Disconnected_Calls() ->find_first_of(\".\")");
     sem_post(&sem_tMutexMainWorkTable);
     return;
    }

    chUTCtimestamp =  (char *) strUTCtimestamp.c_str() ;
    if (strptime( chUTCtimestamp, "%Y/%m/%d %T", &result) == NULL) {
     SendCodingError("ExperientController.cpp - coding error in function Check_Disconnected_Calls() ->strptime()");
     cout << errno << endl;
     sem_post(&sem_tMutexMainWorkTable);
     return;    
    }    
 
    t = timegm(&result);
    if (t < 0) {
     SendCodingError("ExperientController.cpp - coding error in function Check_Disconnected_Calls() ->timegm()");
     cout << errno << endl;
     sem_post(&sem_tMutexMainWorkTable);
     return;
    }    

    timespecTimeDisconnect.tv_sec  = t;
    timespecTimeDisconnect.tv_nsec = 0;
 
    ldTimeDiff = time_difference(timespecTimeNow, timespecTimeDisconnect);

    if( ldTimeDiff > (intSecondsRebidsActivePostDisconnect * 1.0) ) { 
     // check ALI Button if on then turn off ....
     //
     if ( vMainWorkTable[i].WindowButtonData.boolALIRebidButton) {

      if (vMainWorkTable[i].ALIData.boolALIRecordReceived) {
       vMainWorkTable[i].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
       vMainWorkTable[i].WindowButtonData.fSetButtonsForALIbid();
       vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, vMainWorkTable[i].ALIData.boolALIRecordReceived);
       Queue_WRK_Event(vMainWorkTable[i]);
      }
      else {
       vMainWorkTable[i].ALIData.intALIState = XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS;
       vMainWorkTable[i].WindowButtonData.fSetButtonsForALIbid();
       vMainWorkTable[i].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, vMainWorkTable[i].ALIData.boolALIRecordReceived);
       Queue_WRK_Event(vMainWorkTable[i]);

      }
     }

     vMainWorkTable.erase(vMainWorkTable.begin()+i);
     sem_post(&sem_tMutexMainWorkTable);
     return;
    } 
      
  }
 }
 sem_post(&sem_tMutexMainWorkTable);


 return;
}




void Check_Abandoned_Calls(int count) {
 extern vector <ExperientDataClass>     vMainWorkTable;
 vector <ExperientDataClass>::size_type sz; 
 int                                    intRC;
 MessageClass                           objMessage;
 struct timespec                        timespecTimeNow;
 bool                                   boolOldAbandonedCallFound = false;
 long double                            ldTimeDiff, ldLargestDiff = 0.0;
 unsigned int                           iCount = 0;
 int                                    iOldestRecord = -1;

 // count is 1 to 1000 each loop takes at least intMAIN_SCOREBOARD_INTERVAL_NSEC  .06 sec 
 // will take appx 1 min to reach 1000
  if(!(count % 50 == 0)) {return;}     // every 1.2 secondsish

 // lock main work table
 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn Check_Abandoned_Calls()", 1);}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++)
  {
   if (vMainWorkTable[i].boolAbandoned) 
    {
     iCount++;
     ldTimeDiff = time_difference(timespecTimeNow, vMainWorkTable[i].timespecTimeAbandonedStamp);
     if (ldTimeDiff > ldLargestDiff) {ldLargestDiff = ldTimeDiff; iOldestRecord = i;}

     if (ldTimeDiff > (ABANDONED_CALL_NOTIFICATION_TIME_SEC * 1.0)) {boolOldAbandonedCallFound = true; }

     if (timespecTimeNow.tv_sec > vMainWorkTable[i].timespecAbandonedPopUp.tv_sec)
      {    
       if (!boolAbandonedCallPopUpActive) 
        { 
         if(boolSTOP_EXECUTION) {return;}         
         else                   {timer_settime( timer_tAbandoned_Pop_Up_Event_TimerId, 0, &itimerspecAbandonedPopUpInterval, NULL);}	    // arm timer
         boolAbandonedCallPopUpActive = true;
        }
     
  //     objMessage.fMessage_Create(LOG_WARNING,131, vMainWorkTable[i].CallData, objBLANK_KRN_PORT, KRN_MESSAGE_131, seconds2time((long long int) ldTimeDiff));
  //     enQueue_Message(MAIN,objMessage); 

       vMainWorkTable[i].timespecAbandonedPopUp.tv_sec += ABANDONED_CALL_NOTIFICATION_TIME_SEC;
    
      }// end if (timespecTimeNow.tv_sec > vMainWorkTable[i].timespecAbandonedPopUp.tv_sec)

    }// end  if (vMainWorkTable[i].boolAbandoned) 

  }// end  for (unsigned int i = 0; i < sz; i++)

 if(boolSTOP_EXECUTION) {return;}
 else if( (!boolOldAbandonedCallFound)&&(boolAbandonedCallPopUpActive)  ){ timer_settime(timer_tAbandoned_Pop_Up_Event_TimerId, 0, &itimerspecDisableTimer, NULL); boolAbandonedCallPopUpActive = false;}

 if ((iCount > intGUI_MAX_ABANDONED_CALLS)&&(iOldestRecord >= 0))
  {
   
   vMainWorkTable[iOldestRecord].CallData.eJSONEventWRKcode = WRK_TAKE_CONTROL_SOFTWARE;           
   objMessage.fMessage_Create(LOG_WARNING, 145, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iOldestRecord].CallData, vMainWorkTable[iOldestRecord].TextData, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_145, int2strLZ(iCount)); 
   enQueue_Message(MAIN,objMessage);
   vMainWorkTable[iOldestRecord].fXML_MessageDeleteRecord();
   vMainWorkTable[iOldestRecord].boolBroadcastXMLtoSinglePosition = false;
   Queue_WRK_Event(vMainWorkTable[iOldestRecord]);   
   vMainWorkTable.erase(vMainWorkTable.begin()+iOldestRecord);
  }



 sem_post(&sem_tMutexMainWorkTable);

}


void Abandoned_Call_Pop_Up_Timed_Event(union sigval union_sigvalArg) {

 ExperientDataClass 		objData;
 WorkStation                    NotifyWorkstation;

 boolAbandonedCallPopUpActive = true;
 objData.stringXMLString = NotifyWorkstation.fCreateAlert(ABANDONED_CALL_POP_UP_MESSAGE, objData.CallData);
 Queue_WRK_Event(objData);
}

void ScanMainTableforNgALI() {

 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz;
 int                                            iTrunk;
 sem_wait(&sem_tMutexMainWorkTable);
 
 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i< sz; i++)
  {
   iTrunk = vMainWorkTable[i].CallData.intTrunk;
   CheckforNGALIinUnmatchedTable(i,iTrunk );

   }

 sem_post(&sem_tMutexMainWorkTable);


}

void DisplayUnmatchedI3data() {

 extern vector <ExperientDataClass>             vUnmatchedALIData;
 vector <ExperientDataClass>::size_type         sz = vUnmatchedALIData.size(); 
 
 cout << sz << endl;
 if (sz == 0) {cout << "Empty .." << endl; return;}
 for (unsigned int i = 0; i< sz; i++)
  {
   vUnmatchedALIData[i].FreeswitchData.fDisplay();  
  }
 return;
}


void CheckforNGALIinUnmatchedTable(int  iTableIndex, int intTrunk) {

// objData.ALIData.
 extern vector <ExperientDataClass>             vMainWorkTable;
 extern vector <ExperientDataClass>             vUnmatchedALIData;
 vector <ExperientDataClass>::size_type         sz;
 int 						index = -1;
 string                                         strData;
 MessageClass					objMessage;
 extern bool                                    boolLEGACY_ALI_ONLY;

 // vMainWorkTable must be seamphore locked before calling !
 
 sem_wait(&sem_tMutexUnmatchedALIData);
 if (!vUnmatchedALIData.size()) {sem_post(&sem_tMutexUnmatchedALIData);return;}

 sz = vUnmatchedALIData.size();

 for (unsigned int i = 0; i< sz; i++)  {
   //check ring /dial object
   if ((boolLEGACY_ALI_ONLY)&&(vUnmatchedALIData[i].ALIData.I3Data.enumLISfunction != LEGACY_ALI_DATA_RECIEVED)) {
    // remove data that is not legacy ali ... only one or boom!
    cout << "cleared some data !!!!!!" << endl;   
    vUnmatchedALIData.erase(vUnmatchedALIData.begin()+i);
    sem_post(&sem_tMutexUnmatchedALIData);
    return;
   }
   if (vMainWorkTable[iTableIndex].FreeswitchData.objRing_Dial_Data.strChannelID == vUnmatchedALIData[i].FreeswitchData.objChannelData.strChannelID) 
    {index = i; break;}

 }

 if (index < 0) {sem_post(&sem_tMutexUnmatchedALIData);return;}
 
 cout << "matched some data !!!!!!" << endl;
 strData.clear();


 //determine type of Data
 switch ( vUnmatchedALIData[index].ALIData.I3Data.enumLISfunction)
  {
   case LEGACY_ALI_DATA_RECIEVED:
        strData = vUnmatchedALIData[index].ALIData.stringAliText;
        // set clock for auto wireless rebid ..
        clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);
        vMainWorkTable[iTableIndex].ALIData.enumALIBidType = INITIAL;
        break;
   default:
        //NextGen strCallbackNum is callback 
        
        vMainWorkTable[iTableIndex].fLoad_CallbackNumber(vUnmatchedALIData[index].ALIData.I3Data.I3XMLData.CallInfo.strCallbackNum);
        switch(enumI3CONVERSION_TYPE) {
         case ALI30W:  strData = vUnmatchedALIData[index].ALIData.I3Data.strALI30WRecord; break;
         case ALI30X:  strData = vUnmatchedALIData[index].ALIData.I3Data.strALI30XRecord; break;
         case NEXTGEN: strData = vUnmatchedALIData[index].ALIData.I3Data.strNGALIRecord;  break; 
        }
        vMainWorkTable[iTableIndex].ALIData.I3Data = vUnmatchedALIData[index].ALIData.I3Data;
        vMainWorkTable[iTableIndex].ALIData.I3Data.enumLISfunction = I3_DATA_RECEIVED;
        if (vUnmatchedALIData[index].ALIData.I3Data.boolPidflobyValue) {
         vMainWorkTable[iTableIndex].strPidfloByValEntity = vUnmatchedALIData[index].ALIData.I3Data.strEntity;
        }  
        break;
  }
 vMainWorkTable[iTableIndex].ALIData.intALIState                 = XML_STATUS_CODE_ALI_RECEIVED;
 vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived       = true; 
 vMainWorkTable[iTableIndex].ALIData.stringAliText               = strData;
 vMainWorkTable[iTableIndex].ALIData.stringType                  = 2;
 vMainWorkTable[iTableIndex].fLoadCallbackfromALI();
 vMainWorkTable[iTableIndex].WindowButtonData.fSetNoALICadBid();                     
 vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
 vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();
 vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS, true);

 //update Callback In ANI
 if (vMainWorkTable[iTableIndex].CallData.intCallbackNumber){
  vMainWorkTable[iTableIndex].enumANIFunction = UPDATE_CALLBACK;
  vMainWorkTable[iTableIndex].CallData.fSetTrunkTypeString();
  Queue_ANI_Event(vMainWorkTable[iTableIndex]);
 }
 vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
 Queue_WRK_Event(vMainWorkTable[iTableIndex]);

 // set clock for auto wireless rebid ..
 //    clock_gettime(CLOCK_REALTIME, &vMainWorkTable[iTableIndex].ALIData.timespecTimeAliAutoRebidOK);

 strData = charSTX + strData;
 strData += charETX;
 // vMainWorkTable[iTableIndex].ALIData.eJSONEventCode = NG_ALI_FUNCTION;   //may not need .... fred
 objMessage.fMessage_Create(LOG_CONSOLE_FILE, 279, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].CallData, objBLANK_TEXT_DATA, objBLANK_LIS_PORT,
                            objBLANK_FREESWITCH_DATA, vMainWorkTable[iTableIndex].ALIData, LIS_MESSAGE_279, strData ,"","","","","", NORMAL_MSG, ALI_RECORD);
 enQueue_Message(LIS,objMessage);
           
 // Send to CAD if connected or out of order situation and not a manual ALI and posn != 0
 if((((vMainWorkTable[iTableIndex].boolConnect)||(vMainWorkTable[iTableIndex].boolConnectOutofOrder))&&
   (!(CheckforManualBidTrunk(intTrunk)))&&(true)))    
  {
   vMainWorkTable[iTableIndex].boolConnectOutofOrder = false;
   cout << "send to CAD from unmatched data" << endl;
   SendALItoCAD_Conference_List(iTableIndex);
  }


 vUnmatchedALIData.erase( vUnmatchedALIData.begin()+index);
 sem_post(&sem_tMutexUnmatchedALIData);
}




void DisplayEmailsSupressed() {

 extern volatile bool                           boolEmailSupress[1000];
 cout << endl;
 cout << "The Following Message Numbers (Emails) are Supressed -> ";
 for (int i = 100; i < 1000; i++)
  {
   if(boolEmailSupress[i]) {cout << i << "; "; if ((i%10) == 0) {cout << endl;}}
  }

 cout << endl;
}

void DisplayADVinfo() {

extern string				stringEMAIL_FROM;
extern char*				charEMAIL_FROM;
extern string				stringEMAIL_SEND_TO;
extern char*				charEMAIL_SEND_TO;
extern display_mode			enumDISPLAY_MODE;				
extern bool				boolEmailON;
extern string				strEMAIL_RELAY_SERVER;
extern string				strEMAIL_RELAY_USER;
extern string				strEMAIL_RELAY_PASSWORD;
extern unsigned long int		intDEBUG_MODE_REMINDER_SEC;
extern int				intALARM_POPUP_DURATION_MS;
extern int				intTCP_RECONNECT_RECURSION_LEVEL;
extern int				intTCP_RECONNECT_INTERVAL_SEC;

 cout << "ADVANCED INFO ................." << endl;
 cout << "EMAIL_ENABLED          bool  -> ";
 if (boolEmailON)       { cout << "true" << endl;}
 else                   { cout << "false" << endl;}
 cout << "EMAIL_FROM             str   -> " << stringEMAIL_FROM << endl;
 cout << "EMAIL_FROM             char* -> " << charEMAIL_FROM << endl;
 cout << "EMAIL_TO               str   -> " << stringEMAIL_SEND_TO << endl;
 cout << "EMAIL_TO               char* -> " << charEMAIL_SEND_TO << endl;
 cout << "EMAIL_RELAY            str   -> " << strEMAIL_RELAY_SERVER << endl;
 cout << "EMAIL_USER             str   -> " << strEMAIL_RELAY_USER << endl;
 cout << "EMAIL_PW               str   -> " << strEMAIL_RELAY_PASSWORD << endl;
 cout << "Display Mode                 -> " << DisplayMode(enumDISPLAY_MODE) << endl;
 cout << "Alarm PopUp            msec  -> " << int2str(intALARM_POPUP_DURATION_MS) << endl;
 cout << "Debug Reminder         sec   -> " << int2str(intDEBUG_MODE_REMINDER_SEC) << endl;
 cout << "TCP Recursion Levels         -> " << int2str(intTCP_RECONNECT_RECURSION_LEVEL) << endl;
 cout << "TCP Reconnect Interval sec   -> " << int2str(intTCP_RECONNECT_INTERVAL_SEC) << endl;
 cout << endl;
}

void DisplayDSBinfo() {
 extern bool			boolUSE_DASHBOARD;
 extern int                    	intNUM_DSB_PORTS;
 extern string                 	strDASHBOARD_API_KEY;
 extern int			intDSB_HEARTBEAT_INTERVAL_SEC;
 extern int			intDSB_PORT_DOWN_THRESHOLD_SEC;
 extern int			intDSB_PORT_DOWN_REMINDER_SEC;
 extern  ExperientTCPPort       DashboardPort;

 cout << "DASHBOARD INFO ......." << endl;
 cout << "USE DASHBOARD       -> ";
 if (boolUSE_DASHBOARD) { cout << "true" << endl;}
 else                   { cout << "false" << endl;}
 cout << "NUM DASHBOARD PORTS -> " << int2str(intNUM_DSB_PORTS) << endl;
 cout << "HEARTBEAT INTERVAL* -> " << int2str(intDSB_HEARTBEAT_INTERVAL_SEC) << " *hardcoded at " << DASHBOARD_PING_INTERVAL << " sec for now." << endl;
 cout << "PORT DOWN THRESHOLD -> " << int2str(intDSB_PORT_DOWN_THRESHOLD_SEC) << endl;
 cout << "PORT DOWN REMINDER  -> " << int2str(intDSB_PORT_DOWN_REMINDER_SEC) << endl;
 cout << "IP ADDRESS          -> " << DashboardPort.GetRemoteHost() << endl;
 cout << "LOCAL PORT          -> " << DashboardPort.GetLocalPort() << endl;
 cout << "REMOTE PORT         -> " << DashboardPort.GetRemotePort() << endl;
 cout << endl;
}

void DisplayCADinfo() {
    
 extern ExperientCommPort                   CADPort[NUM_CAD_PORTS_MAX+1];
 extern int                                 intNUM_CAD_PORTS;
 extern bool                                boolCAD_EXISTS;
 extern Initialize_Global_Variables         INIT_VARS;
 
 extern string                              truefalse(bool truefalse);
 cout << "CAD INFO" << endl;
 cout << "CAD EXISTS ....... ";

 if (boolCAD_EXISTS)    { cout << "true" << endl;}
 else                   { cout << "false" << endl;}  
 cout << "NUM CAD PORTS         -> " << int2str(intNUM_CAD_PORTS) << endl;

 
 cout << "HEARTBEAT INTERVAL*   -> " << int2str(intCAD_HEARTBEAT_INTERVAL_SEC)  <<  endl;
 cout << "PORT DOWN THRESHOLD   -> " << int2str(intCAD_PORT_DOWN_THRESHOLD_SEC) << endl;
 cout << "PORT DOWN REMINDER    -> " << int2str(intCAD_PORT_DOWN_REMINDER_SEC)  << endl;
 cout << "SEND ON CONFERENCE    -> ";
 cout << truefalse(boolSEND_TO_CAD_ON_CONFERENCE) << endl; 
 cout << "SEND ON TAKE CONTROL  -> ";
 cout << truefalse(boolSEND_TO_CAD_ON_TAKE_CONTROL) << endl << endl;
 
 
 for (int i = 1; i <= intNUM_CAD_PORTS; i++) {
  cout << "CAD PORT " << int2str(i) << endl;
  cout << "PORT ACTIVE                   -> " << truefalse(CADPort[i].UDP_Port.GetActive()) << endl;
  cout << "Heart Beat Interval           -> " << CADPort[i].intHeartbeatInterval << endl;
  cout << "PORT DOWN REMINDER            -> " << CADPort[i].intPortDownReminderThreshold << endl;
  cout << "PORT DOWN THRESHOLD           -> " << CADPort[i].intPortDownThresholdSec << endl;
  cout << "POSITIONS USING PORT          -> " << CADPort[i].fDisplayPostionsUsingPort() << endl;
  cout << "PORT POSITION ALIASES         -> " << CADPort[i].PositionAliases.fDisplay() << endl;;
  cout << "PORT WRK GROUP NAME           -> " << CADPort[i].WorkstationGroup.strGroupName << endl;
  cout << "PORT WRK GROUP PSAP ID        -> " << CADPort[i].WorkstationGroup.strPSAP_ID << endl;
  cout << "PORT RULE HEARTBEAT REQUIRED  -> " << truefalse(CADPort[i].boolHeartbeatRequired) << endl;
  cout << "PORT RULE RECORD ACK REQUIRED -> " << truefalse(CADPort[i].boolRecordACKRequired) << endl;
  cout << "PORT RULE HB ACK REQUIRED     -> " << truefalse(CADPort[i].boolHeartBeatACKrequired) << endl;
  cout << "USE NENA CAD HEADER           -> " << truefalse(CADPort[i].boolNENA_CADheader) << endl;
  cout << "ERASE FIRST ALI <CR>          -> " << truefalse(CADPort[i].boolCAD_EraseFirstALI_CR) << endl;
  cout << "USE NENA ERASE MSG            -> " << truefalse(CADPort[i].boolSendCADEraseMsg) << endl;
  cout << "PORT VENDOR EMAIL ADDR        -> " << CADPort[i].UDP_Port.strVendorEmailAddress << endl;

  cout << "PORT CONNECTION TYPE          -> ";
  switch (CADPort[i].ePortConnectionType) {
     case TCP: cout << " TCP" << endl; break;
     case UDP: cout << " UDP" << endl; break;
     default:  cout << " NOT CONFIGURED" << endl; break;
  }
  cout << "IP ADDRESS                    -> " << CADPort[i].RemoteIPAddress.stringAddress << endl;
  cout << "LOCAL PORT                    -> " << CADPort[i].intRemotePortNumber << endl;
  cout << "REMOTE PORT                   -> " << CADPort[i].intLocalPort  << endl; 
  cout << "NOTES                         -> " << CADPort[i].strNotes << endl << endl;
 }
 cout << endl;


}

void DisplayALIinfo() {
    
 extern ALISteering                          ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern ExperientCommPort                    ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
 extern ExperientCommPort                    ANIPort[NUM_ANI_PORTS_MAX+1];
 string                                      strHeader;

 cout << "LEGACY ALI                = " << boolLEGACY_ALI << endl;
 cout << "LEGACY ALI ONLY           = " << boolLEGACY_ALI_ONLY << endl;
 cout << "SEND LEGACY ALI SIP XFER  = " << boolSEND_LEGACY_ALI_ON_SIP_TRANSFER << endl;
 cout << "PORT ALARM DELAY SEC      = " << intALI_PORT_DOWN_THRESHOLD_SEC << endl;
 cout << "PORT REMINDER SEC         = " << intALI_PORT_DOWN_REMINDER_SEC << endl;
 cout << "NOISE THRESHOLD SEC       = " << intDEFAULT_ALI_NOISE_THRESHOLD << endl;
 cout << "WIRELESS REBID SEC        = " << intALI_WIRELESS_AUTO_REBID_SEC << endl;
 cout << "AUTO REBID COUNT          = " << intALI_WIRELESS_AUTO_REBID_COUNT << endl;
 cout << "AUTO BID WPH1 only        = " << boolREBID_PHASE_ONE_ALI_RECORDS_ONLY << endl;
 cout << "MAX RECORD SIZE           = " << intMAX_ALI_RECORD_SIZE << endl;
 cout << "NUMBER SERVICE PORTS      = " << intNUM_ALI_SERVICE_PORTS << endl;
 cout << "NUMBER PORT PAIRS         = " << intNUM_ALI_PORT_PAIRS << endl;
 cout << "NUMBER DATABASES          = " << intNUM_ALI_DATABASES << endl;
 cout << "CLID RULE CODE            = " << intCLID_ALI_BID_RULE_CODE << endl;
 cout << "IGNORE BAD ALI            = " << boolIGNORE_BAD_ALI << endl;
 cout << "VERIFY ALI RECORDS        = " << boolVERIFY_ALI_RECORDS << endl;
 cout << "ALLOW CLID ALI BIDS       = " << boolALLOW_CLID_ALI_BIDS << endl;
 cout << "BID CLID TEST KEY         = " << boolCLID_BID_TEST_KEY << endl;
 cout << "SHOW GOOGLE MAP           = " << boolSHOW_GOOGLE_MAP << endl;
 cout << "REBID NG911               = " << boolREBID_NG911 << endl;
 
 cout << "ALI STEERING MATRIX " << endl;
 for ( int i = 1; i <= intNUM_ALI_DATABASES; i++)
  {
   cout << "DATABASE " << i << " Port Pairs [ ";
   for ( int j = 1; j <= intNUM_ALI_PORT_PAIRS; j++){if (ALI_Steering[i].boolPortPairInDatabase[j]){cout << j << ",";} }
   cout << " ]" << endl;
  }
   
 for ( int i = 1; i <= intNUM_ALI_DATABASES; i++)
 {
  cout << "DATABASE " << i <<" Range Pairs [";
  for ( int j = 1; j <= ALI_Steering[i].intNumPANIRangePairs; j++)
   {
    cout << ALI_Steering[i].intPANILowRange[j]<< " and " << ALI_Steering[i].intPANIHighRange[j]<< " "; }
    cout << " ]" << endl;
   }

 for ( int i = 1; i <= intNUM_ALI_DATABASES; i++)
  {
   strHeader = "DATABASE " + int2str(i);
   strHeader += " ";
   cout << strHeader <<" ";
   cout << "Range Pair REBID DELAYS = [";
   for ( int j = 1; j <= ALI_Steering[i].intNumPANIRangePairs; j++){cout << ALI_Steering[i].intPANIRebidDelay[j]<< "," ; }
   cout << " ]" << endl;
//   cout << "DATABASE " << i <<" LAND Line REBID DELAY = " << ALI_Steering[i].intLandLineRebidDelay << endl;
   cout << strHeader <<"Request Key Format    = " << ALI_Steering[i].eRequestKeyFormat<< endl;
   cout << strHeader <<"Bid Single ALI        = " << ALI_Steering[i].boolDatabaseSendsSingleALI<< endl; 
   cout << strHeader <<"ALI Service           = " << ALI_Steering[i].boolALIService<< endl;
   cout << strHeader <<"Treat LF as CR        = " << ALI_Steering[i].boolTreatLinefeedasCR << endl;    
   cout << strHeader <<"Trunk Type Bids       = ";
   switch (ALI_Steering[i].enumTrunkType){case CAMA: cout << "All"; break;case WIRELESS: cout << "Wireless:";break; case LANDLINE: cout << "Landline";break; 
                                          case INDIGITAL_WIRELESS: cout << "INDGWRLS:";break; case SIP_TRUNK: cout << "SIPtrunk";break; 
                                          case MSRP_TRUNK: cout << "MSRP"; break; case INTRADO: cout << "INTRADO"; default: cout << "ERROR";}

   cout << endl;
   cout << strHeader <<"Callback LocationA    = Row " << ALI_Steering[i].intCallbackLocationRow[0] << " Col " << ALI_Steering[i].intCallbackLocationColumn[0] << endl; 
   cout << strHeader <<"Callback LocationB    = Row " << ALI_Steering[i].intCallbackLocationRow[1] << " Col " << ALI_Steering[i].intCallbackLocationColumn[1] << endl;
   ALI_Steering[i].fDisplayCOSVector(strHeader);
   cout << endl;
  }
      
 cout << "ALI Port Pair Info" << endl;
 for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
  {
   cout << "Pair " << i << endl;
   cout << "Format " << ALIPort[i] [1].eRequestKeyFormat << endl;
   cout << ALIPort[i] [1].RemoteIPAddress.stringAddress << endl;
   cout << ALIPort[i] [1].intRemotePortNumber << " remote port" << endl;
   cout << ALIPort[i] [1].intLocalPort << " local port" << endl;
   cout << ConnectionType(ALIPort[i] [1].ePortConnectionType) << endl;
   cout << ALIProtocol(ALIPort[i] [1].eALIportProtocol) << endl;
   cout << "Notes: " << ALIPort[i] [1].strNotes << endl;
   cout << "Phantom Port = " << ALIPort[i] [1].boolPhantomPort << endl; 
   cout << "vendor cc email = " << ALIPort[i] [1].UDP_Port.strVendorEmailAddress << endl;
   cout << "Format " << ALIPort[i] [2].eRequestKeyFormat << endl;
   cout << ALIPort[i] [2].RemoteIPAddress.stringAddress << endl;
   cout << ALIPort[i] [2].intRemotePortNumber << " remote port" << endl;
   cout << ALIPort[i] [2].intLocalPort << " local port" << endl;

   cout << ConnectionType(ALIPort[i] [2].ePortConnectionType) << endl;
   cout << ALIProtocol(ALIPort[i] [2].eALIportProtocol) << endl;
   cout << "Notes: " << ALIPort[i] [2].strNotes << endl;
   cout << "Phantom Port = " << ALIPort[i] [2].boolPhantomPort << endl;
   cout << "vendor cc email = " << ALIPort[i] [2].UDP_Port.strVendorEmailAddress << endl;

  }
}

void DisplayANIinfo() {

 extern ExperientCommPort                    	ANIPort[NUM_ANI_PORTS_MAX+1];

 cout << "ANI INFORMATION <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl<<endl;
 cout << "Number of ANI Ports        = " << intNUM_ANI_PORTS  << endl;

 for (int i = 1; i<= intNUM_ANI_PORTS; i++)
  {
   cout << "ANI Port " << i << endl;
   cout << ANIPort[i].RemoteIPAddress.stringAddress << ": ";
   cout << ANIPort[i].intRemotePortNumber << endl;
   cout  << "Port_Alarm_Delay_Sec      = " << intANI_PORT_DOWN_THRESHOLD_SEC << endl;
   cout  << "Port_Alarm_Reminder_Sec   = " << intANI_PORT_DOWN_REMINDER_SEC << endl;
   cout << "vendor cc email = " << ANIPort[i].UDP_Port.strVendorEmailAddress << endl; 
 
  }
// Display_Prefix_Assignments();
 cout << "Number of trunks installed = " <<  intNUM_TRUNKS_INSTALLED << endl;

 Trunk_Map.fDisplayLastCallTime();
 
  cout << endl;
  cout << "ConferenceNumber_Prefix                   = " << strCONFERENCE_NUMBER_DIAL_PREFIX << endl;
  cout << "Delay_After_Flash_Hook_ms                 = " << intDELAY_AFTER_FLASH_HOOK_MS << endl;
  cout << "Delay_Between_DTMF_ms                     = " << intDELAY_BETWEEN_DTMF_MS << endl;
  cout << "Delay_After_Flash_Hook_*8# INDIGITAL ms   = " << intDelay_AFTER_STAR_EIGHT_PD_MS << endl;
  cout << "Delay_Between_DTMF_INDIGITAL_ms           = " << intDelay_BETWEEN_DTMF_INDIGITAL_MS << endl;
  cout << "*strANI_16_DIGIT_REQ_TEST_KEY*            = " << strANI_16_DIGIT_REQ_TEST_KEY << endl; 
  cout << "*strANI_14_DIGIT_REQ_TEST_KEY*            = " << strANI_14_DIGIT_REQ_TEST_KEY << endl; 
  cout << "Gateway_Transfer_Order                    = " << strGATEWAY_TRANSFER_ORDER  << endl; 

  cout << "ANI_20_Digit_Valid_Callback               = " << boolANI_20_DIGIT_VALID_CALLBACK << endl;
  cout << "TDD_Auto_Detect                           = " << boolTDD_AUTO_DETECT << endl;
  cout << "USE_BCF_IP_KEY                            = " << bool_BCF_ENCODED_IP << endl; 
  cout << "AUDIOCODES_911_ANI_REVERSED               = " << boolAUDIOCODES_911_ANI_REVERSED  << endl; 
  cout << "NPD's                                     = ";
  for (int i=0; i<=7; i++) { cout << NPD[i] << " "; }
  cout << endl;

}

void Display_Controller_Init_Data()
{
 extern ALISteering                          	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern ExperientCommPort                    	ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];

 bool   					boolLoop;
 int						intMenu;
 cout << endl;
 Display_Init_Table_Menu();


 do
  { 
   intMenu = NonBlockingNoEchoInputChar();
                        
   switch (intMenu)
    {
     case -1: break;
     case 49:
          // KRN
         
          boolLoop = false;break;
     case 50:
          // ANI
             DisplayANIinfo();
             boolLoop = false;break;
     case 51:
          // ALI
             DisplayALIinfo();
             boolLoop = false;break;
     case 52:
          // CAD
             DisplayCADinfo();
             boolLoop = false;break;
     case 53:
          //DSB
             DisplayDSBinfo();
             boolLoop = false;break;
     case 54:
          //ADV
             DisplayADVinfo();
             boolLoop = false;break;
     default:
            cout << endl << "Invalid Selection ..." << endl;
            Display_Init_Table_Menu();
    }

  }while (boolLoop);

}

void Display_Controller_Workstation_Data()
{
 int intRC;
 size_t OS_Longest_length = 0;

 
 intRC = sem_wait(&sem_tMutexWRKWorkTable);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in Display_Controller_Workstation_Data()", 1);}

 for (int i = 1; i <= intNUM_WRK_STATIONS; i++) {if (WorkStationTable[i].strWindowsOS.length() > OS_Longest_length) {OS_Longest_length = WorkStationTable[i].strWindowsOS.length();}}
 if (OS_Longest_length < 2) {OS_Longest_length = 2;}

 cout << endl;
 cout << "Workstation Info:" << endl << endl;;
 cout << setw(5)                    << left << "POS";
 cout << setw(OS_Longest_length+2)  << left << "OS";
 cout << setw(17)                   << left << "IP_ADDRESS";
 cout << setw(8)                    << left << "TCP Id";
 cout << setw(11)                   << left << "GUI";
 cout << setw(6)                    << left << "XML";
 cout << setw(5)                    << left << "RCC";
 cout << setw(25)                   << left << "USER" << endl;

 cout << "--------------------------------------------------------------------" << endl;




 for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
  {
   cout<< setw(5) << left <<i;
   if (WorkStationTable[i].boolActive)
    {
     cout << setw(OS_Longest_length+2)  << left << FindandReplace(WorkStationTable[i].strWindowsOS, " ", "-");
     cout << setw(17) << left << WorkStationTable[i].RemoteIPAddress.stringAddress;
     cout << setw(8)  << left << WorkStationTable[i].intTCP_ConnectionID;      
     cout << setw(11)  << left << WorkStationTable[i].strGUIVersion; 
     cout << setw(6) << left << WorkStationTable[i].strGUIxmlVersion;
    }
   else
     {cout << setw(OS_Longest_length+44) << left <<" ";}
  
   if (!RCCdevice[i].boolContactRelayInstalled) {cout << setw(5) << left << "N/A";}
   else if (RCCdevice[i].boolRCCconnected)      {cout << setw(5) << left << "On";}
   else                                         {cout << setw(5) << left << "Off";}
   if (WorkStationTable[i].boolActive)
    {  
     cout << setw(25) << left <<  WorkStationTable[i].strWindowsUser << endl;
    }
   else {cout << endl;}
  }
 cout << endl;
 sem_post(&sem_tMutexWRKWorkTable);

}

void Port_Status_Report(ExperientCommPort CommPort[], int iNumPorts)
{
 string strTRUE = "True";
 string strFALSE = "False";
 for (int i = 1; i <= iNumPorts; i++)
  {
   if (CommPort[i].boolReducedHeartBeatMode){}
  }
}




void Show_Server_Status_Table()
{
 extern ExperientCommPort  ANIPort[NUM_ANI_PORTS_MAX+1];

cout << endl;
cout << "                        Server Status Table" << endl;
cout << "Number    Name      IP Address     SYC IP Address    Status      Mode" << endl;

sem_wait(&sem_tMutex_ServerStatusTable);
if (!boolSINGLE_SERVER_MODE)
{
for (int i = 1; i <= intNUM_OF_SERVERS; i++)
      {
       cout << setw(10) << left << i;
       cout << setw(10) << left << ServerStatusTable[i].ServerName;
       cout << setw(15) << left << ServerStatusTable[i].Gateway_IP.stringAddress;
       cout << setw(18) << left << ServerStatusTable[i].Sync_IP.stringAddress;

       if (ServerStatusTable[i].boolIsActive) {cout << setw(12) << left << "ACTIVE";}else {cout << setw(12) << left << "NOT ACTIVE ";}
       if (ServerStatusTable[i].boolIsMaster) {cout << "MASTER"<< endl;}else {cout <<  "SLAVE"<<endl;;}
     
      }
}
else
 {
  cout << setw(10) << left << "1";
  cout << setw(10) << left << stringSERVER_HOSTNAME;
  cout << setw(15) << left << ServerStatusTable[1].Gateway_IP.stringAddress;
  cout << setw(18) << left << "N/A";
  cout << setw(12) << left << "N/A";
  cout << setw(12) << left << "SINGLE SERVER" << endl;
 }
  cout << endl;
 sem_post(&sem_tMutex_ServerStatusTable);

 Port_Status_Report(ANIPort,intNUM_ANI_PORTS); 

  


}

void Check_Time_in_Debug_Mode()
{
 MessageClass           objMessage;
 long double            ldoubleTimeDiff;
 struct timespec        timespecTimeNow;
 Port_Data              objPortData;


// CTI Debug ? 
// LIS Debug ?
// NFY Debug - No traffic to debug

 if      (boolDEBUG)                            {objPortData = objBLANK_KRN_PORT;}
 else if (boolLOG_DISPLAY_KRN_HEARTBEAT_TRAFFIC){objPortData = objBLANK_KRN_PORT;}
 else if (boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC){objPortData = objBLANK_ALI_PORT;}
 else if (boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC){objPortData = objBLANK_ANI_PORT;}
 else if (boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC){objPortData = objBLANK_CAD_PORT;}
 else if (boolLOG_DISPLAY_WRK_HEARTBEAT_TRAFFIC){objPortData = objBLANK_WRK_PORT;}
 else if (boolLOG_DISPLAY_RCC_HEARTBEAT_TRAFFIC){objPortData = objBLANK_RCC_PORT;} 
 else if (boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)  {objPortData = objBLANK_KRN_PORT;}
 else if (boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC)  {objPortData = objBLANK_KRN_PORT;}
 else if (boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC)  {objPortData = objBLANK_ALI_PORT;}
 else if (boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC)  {objPortData = objBLANK_ANI_PORT;}
 else if (boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC)  {objPortData = objBLANK_CAD_PORT;}
 else if (boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC)  {objPortData = objBLANK_WRK_PORT;}
 else if (boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC)  {objPortData = objBLANK_RCC_PORT;}
 else if (boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC)  {objPortData = objBLANK_DSB_PORT;}
 else                                           {ldDebugReminder = 0; return; }



 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);  

 ldoubleTimeDiff = time_difference(timespecTimeNow, timespecDebugMode);
 if ( ldoubleTimeDiff > (intDEBUG_MODE_REMINDER_SEC + ldDebugReminder))
  {
   objMessage.fMessage_Create(LOG_INFO, 119, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                              objBLANK_ALI_DATA, KRN_MESSAGE_119, seconds2time((unsigned long long int)(ldoubleTimeDiff)));       
   enQueue_Message(MAIN,objMessage);  
   ldDebugReminder += intDEBUG_MODE_REMINDER_SEC;
  }


}





void Set_Display_Mode(display_mode enumMode)
{
 boolDEBUG 				= false;
 boolLOG_DISPLAY_KRN_HEARTBEAT_TRAFFIC 	= false; 
 boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC 	= false;
 boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC 	= false;
 boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC 	= false; 
 boolLOG_DISPLAY_WRK_HEARTBEAT_TRAFFIC 	= false;
 boolLOG_DISPLAY_RCC_HEARTBEAT_TRAFFIC 	= false;
 boolDISPLAY_CONSOLE_OUTPUT		= false;
 boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC    = false;
 boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC    = false;


 switch (enumMode)
  {
   case MINIMAL_DISPLAY:
        // SAME AS INIT ABOVE
        break;
   case NORMAL_DISPLAY:
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        break;
   case DEBUG_KRN:
        boolDEBUG 				= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC     = true;
        boolLOG_DISPLAY_KRN_HEARTBEAT_TRAFFIC   = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG); 
        sem_post(&sem_tMutexDebugLogName);
        break;
   case DEBUG_ALI:
        boolDEBUG 				= true;
        boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC 	= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC     = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG); 
        sem_post(&sem_tMutexDebugLogName);  
        break;
   case DEBUG_ANI:
        boolDEBUG 				= true;
        boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC 	= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC     = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG);
        sem_post(&sem_tMutexDebugLogName);      
        break;
   case DEBUG_CAD:
        boolDEBUG 				= true;
        boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC 	= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC     = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG);
        sem_post(&sem_tMutexDebugLogName); 
        break;
   case DEBUG_WRK:
        boolDEBUG 				= true;
        boolLOG_DISPLAY_WRK_HEARTBEAT_TRAFFIC 	= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC     = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG);
        sem_post(&sem_tMutexDebugLogName);          
        break;
   case DEBUG_RCC:
        boolDEBUG 				= true;
        boolLOG_DISPLAY_RCC_HEARTBEAT_TRAFFIC 	= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC     = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG);
        sem_post(&sem_tMutexDebugLogName);    
        break;
   case DEBUG_DSB:
        boolDEBUG 				= true;
        boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC     = true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG);
        sem_post(&sem_tMutexDebugLogName);  
        break;
   case DEBUG_ALL:
        boolDEBUG 				= true;
        boolLOG_DISPLAY_KRN_HEARTBEAT_TRAFFIC 	= true; 
        boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC 	= true;
        boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC 	= true;
        boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC 	= true; 
        boolLOG_DISPLAY_WRK_HEARTBEAT_TRAFFIC 	= true;
        boolLOG_DISPLAY_RCC_HEARTBEAT_TRAFFIC 	= true;
        boolDISPLAY_CONSOLE_OUTPUT		= true;
        boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC     = true;
        clock_gettime(CLOCK_REALTIME, &timespecDebugMode);
        sem_wait(&sem_tMutexDebugLogName);
        stringCurrentDebugFileName = GetLogFileName(DEBUG_LOG);
        sem_post(&sem_tMutexDebugLogName);    
        break;
   default:
        // default same as MINIMAL DISPLAY
        break;
  }
}





void Display_Controller_Up_Time(struct timespec timespecStartTime)
{
 struct timespec 	timespecTimeNow;
 long double 		ldoubleTimeDiff;
 time_t			time_tTimeStart;
 struct tm		tmLocalTimeStart;
 char                   char_arrayLocalTimeStart[45];
    
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 ldoubleTimeDiff = time_difference(timespecTimeNow, timespecStartTime);
 time_tTimeStart = timespecStartTime.tv_sec; 
 localtime_r(&time_tTimeStart,&tmLocalTimeStart);
 strftime(char_arrayLocalTimeStart, 45, "%A, %B %d, %Y at %H:%M:%S %Z", &tmLocalTimeStart);

 cout << endl;
 cout << "Controller Start time = " << char_arrayLocalTimeStart << endl;
 cout << "Controller Up time    = " << seconds2time(int(ldoubleTimeDiff)) << endl<< endl;;
}

void Display_Menu()
{
 cout << endl << "Display Options Menu:\n";
 cout << "1. Normal\n";   
 cout << "2. Minimal\n";    
 cout << "3. Debug ALL\n"; 
 cout << "4. Debug KRN\n"; 
 cout << "5. Debug ANI \n";
 cout << "6. Debug ALI \n";
 cout << "7. Debug CAD \n";
 cout << "8. Debug WRK \n";
 cout << "9. Debug RCC\n";
 cout << "a. Debug DSB" << endl << endl;
}

void Display_Thread_Table_Menu()
{
 cout << endl << "Display Thread Table Options Menu:\n";  
 cout << "1. KRN\n"; 
 cout << "2. ANI \n";
 cout << "3. ALI \n";
 cout << "4. CAD \n\n";
}

void Display_Init_Table_Menu()
{
 cout << endl << "Display Init Data Options Menu:\n";  
 cout << "1. KRN\n"; 
 cout << "2. ANI \n";
 cout << "3. ALI \n";
 cout << "4. CAD \n";
 cout << "5. DSB \n";
 cout << "6. ADV \n\n";
}

/*
void Display_Prefix_Assignments()
{
 cout << endl << "Dialing Prefix trunk types" << endl;
 cout << "Dial_Prefix_*    = " <<  strDIAL_PREFIX_STAR << endl;
 cout << "Dial_Prefix_9    = " <<  strDIAL_PREFIX_NINE << endl;
 cout << "Dial_Prefix_None = " <<  strDIAL_PREFIX_NONE << endl << endl;
}
*/

void Display_Thread_Table_Data()
{
 bool				                boolLoop;
 int                   	                        intMenu;
 int                    	                intRC;
 extern vector <ExperientDataClass>             vANIWorktable;
 extern vector <ExperientDataClass>             vALIworkTable;
 extern ExperientDataClass                      obj_arrayCadWorkTable[intALI_MAN_BID_TRUNK_LAST+2];
 Display_Thread_Table_Menu();
 boolLoop = true; 
 vector <ExperientDataClass>::size_type         szMain = vMainWorkTable.size();
 vector <ExperientDataClass>::size_type         szAni  = vANIWorktable.size();
 vector<ExperientDataClass>::size_type          szAli  = vALIworkTable.size();
                      do
                       { 
                        intMenu = NonBlockingNoEchoInputChar();
                        
                        switch (intMenu)
                         {
                          case -1: break;
                          case 49:
                               // KRN
                               intRC = sem_wait(&sem_tMutexMainWorkTable);
                               cout << "KRN Table Size = " << vMainWorkTable.size() << endl;
                               cout << "KRN" << endl << "Trunk Call-ID" << endl; 
                                          
                               for (unsigned int i = 0; i < szMain; i++) { vMainWorkTable[i].fDisplay(i); }
                               cout << endl; 
                               sem_post(&sem_tMutexMainWorkTable);
                               boolLoop = false;break;
                          case 50:
                               // ANI
                               intRC = sem_wait(&sem_tMutexANIWorkTable);
                               cout << "ANI Table Size = " << vANIWorktable.size() << endl;
                               cout << "ANI" << endl << "Trunk Call-ID" << endl;            
                               for (unsigned int i = 0; i < szAni; i++) { vANIWorktable[i].fDisplay(i); }
                               cout << endl; 
                               sem_post(&sem_tMutexANIWorkTable);
                               boolLoop = false;break;
                          case 51:
                               // ALI
                               intRC = sem_wait(&sem_tMutexAliWorkTable);
                               cout << "ALI Table Size = " << vALIworkTable.size() << endl;
                               cout << "ALI" << endl << "Trunk Call-ID" << endl;            
                               for (unsigned int i = 0; i < szAli; i++) { vALIworkTable[i].fDisplay(i); }
                               cout << endl; 
                               sem_post(&sem_tMutexAliWorkTable);
                               boolLoop = false;break;
                          case 52:
                               // CAD
                               intRC = sem_wait(&sem_tMutexCadWorkTable);
                               cout << "CAD" << endl << "Trunk Call-ID" << endl;            
                               for (int i = 1; i <= intALI_MAN_BID_TRUNK_LAST; i++) { obj_arrayCadWorkTable[i].fDisplay(i); }
                               cout << endl; 
                               sem_post(&sem_tMutexCadWorkTable);
                               boolLoop = false;break;
  
                          default:
                               cout << endl << "Invalid Selection ..." << endl;
                               Display_Thread_Table_Menu();
                         }

                        }while (boolLoop);

 DisplayCPUstatisticsPerThread();


}

bool CheckIfControllerRunning()
{
 FILE                   *fpipe;
// char                   *command;
 char                   line[128];
 string                 stringPID = "";
 int                    intLineCount = 0;

// system ("pgrep -f PSAP_Controller.exe");

 return false;    /// DISABLED THIS FUNCTION !!!!!!!!!!!!!!!!!!!!!

 if ( !(fpipe = (FILE*)popen("pgrep -f ng911.exe","r")) )
  {  // If fpipe is NULL
    cout << "Unable to Determine if 911.exe is currently running" << endl;
    sleep(5);
    return true;
  }


 while ( fgets( line, sizeof line, fpipe))
 {
  if (intLineCount == 0){stringPID = line;}
  intLineCount++;
 }
  pclose(fpipe);

 cout << "Line count =" << intLineCount << endl;
 cout << line << endl;

 if (intLineCount > 1) 
  {
   cout << "ERROR ng911.exe is already running with pid=" << stringPID << endl;
   sleep(5);
   return true;
  }

 return false;
}

void SendHTTPSdereferenceData(string strFile, string strUUID, int iPort) {
 DataPacketIn		        objData;
 int                            intRC;
 extern sem_t			sem_tMutexLISPortDataQ;
 extern sem_t			sem_tLISflag;
 extern queue <DataPacketIn>    queue_LISPortData;;

 objData.fLoadSimulatedHTTPderef(strFile, strUUID, iPort);

        cout << "LOCK PORT -f" << endl;
        intRC = sem_wait(&sem_tMutexLISPortDataQ);
        queue_LISPortData.push(objData);
        cout << "SIZE" << queue_LISPortData.size() << endl;;
        sem_post(&sem_tMutexLISPortDataQ);
        cout << "UNLOCK PORT -f" << endl;

 return;
}

void LoadADR() {
 DataPacketIn			objData;
 ADR_Data			adrData;
 string				strError;

 ifstream ifs("/datadisk1/ng911/data/adr.message.subscriberinfo.example.txt");    
 string   str((istreambuf_iterator<char>(ifs)), istreambuf_iterator<char>());

 objData.stringDataIn = str;

 
ifs.close();

if (!adrData.fLoadSubscriberInfo( objData, &strError)) {
cout << strError << endl;
}


if (!adrData.fLoadServiceInfo(objData, &strError)) {
cout << strError << endl;
}

if (!adrData.fLoadProviderInfo( objData, &strError)) {
cout << strError << endl;
}

}

void SendHTTPSdereferenceRequestZERO()
{
 extern vector <ExperientDataClass>      		vMainWorkTable;
 size_t                                                 szTable;
 string                                                 strInput;
 unsigned int                                           index = 0;

 szTable = vMainWorkTable.size(); 
 if (!szTable) {cout << "Call Table empty !" << endl; return;}

 cout << "Enter the http(s) request URL ->";
 getline (cin, strInput);

 
 vMainWorkTable[index].ALIData.I3Data.fLoadBidID("1");
 vMainWorkTable[index].ALIData.I3Data.objLocationURI.strURI = strInput;
 vMainWorkTable[index].ALIData.I3Data.enumLISfunction = DEREFERENCE_URL;
 vMainWorkTable[index].ALIData.I3Data.objLocationURI.boolDataRecieved = true;
 Queue_LIS_Event(vMainWorkTable[index]); 

}

void SendCancelTandemTransfer()
{
 extern vector <ExperientDataClass>      		vANIWorktable;
 size_t                                                 szTable;
 string                                                 strInput;
 unsigned int                                           index;
 ExperientDataClass                                     objData;
 int                                                    intWorkStation =2;

 szTable = vANIWorktable.size(); 
 if (!szTable) {cout << "ANI Table empty !" << endl; return;}


 objData.intWorkStation = intWorkStation;
 objData.CallData.fLoadPosn(intWorkStation);
 objData.enumWrkFunction = WRK_CNX_TRANSFER;
 objData.enumThreadSending = WRK;
 objData.fLoad_Transfer_Type(WRK, "Tandem", "Tandem", 1);
 objData.CallData.TransferData.strTransfertoNumber = vANIWorktable[0].CallData.TransferData.strTransfertoNumber; 
 objData.CallData.TransferData.fLoadFromPosition(intWorkStation);
 objData.CallData.TransferData.boolGUIcancel =true;
 objData.CallData.intUniqueCallID    = vANIWorktable[0].CallData.intUniqueCallID;
 objData.CallData.stringUniqueCallID = vANIWorktable[0].CallData.stringUniqueCallID;
 enqueue_Main_Input(WRK, objData);

}






void SendInfoMessage()
{
 extern vector <ExperientDataClass>      		vANIWorktable;
 size_t                                                 szTable;
 string                                                 strInput;
 unsigned int                                           index;
 ExperientDataClass                                     objData;

 szTable = vANIWorktable.size(); 
 if (!szTable) {cout << "ANI Table empty !" << endl; return;}

 objData = vANIWorktable[0];
 objData.enumANIFunction = RFAI_BRIDGE;
 Queue_AMI_Input(objData);
 sem_post(&sem_tAMIFlag);
}

void  Display_ANItableZERO(int i)
{
 extern vector <ExperientDataClass>      		vANIWorktable;
 extern vector <ExperientDataClass>      		vMainWorkTable;

 size_t                                                 szTable;
 string                                                 strInput;
 unsigned int                                           index;

 cout << "Main Table Size = " << vMainWorkTable.size() << endl;


 szTable = vANIWorktable.size(); 
 if (!szTable) {cout << "ANI Table empty !" << endl; return;}

 cout << "Which Table Entry ? 0-" << szTable -1 << " :";
 getline (cin, strInput);

 if(!Validate_Integer(strInput)) {cout << "Invalid Data !" << endl; return;}
 index = char2int(strInput.c_str());
 if (index > (szTable-1))        {cout << "Out of Range !" << endl; return;}

 vANIWorktable[index].FreeswitchData.fDisplay();
 vANIWorktable[index].CallData.fDisplay();
 vANIWorktable[index].ALIData.I3Data.fDisplay();

}

void  Display_KRNtableZERO(int i)
{
 extern vector <ExperientDataClass>      		vMainWorkTable;
 size_t                                                 szTable;
 string                                                 strInput;
 unsigned int                                           index;

 szTable = vMainWorkTable.size(); 
 if (!szTable) {cout << "WRK Table empty !" << endl; return;}

  cout << "Which Table Entry ? 0-" << szTable -1 << " :";
 getline (cin, strInput);

 if(!Validate_Integer(strInput)) {cout << "Invalid Data !" << endl; return;}
 index = char2int(strInput.c_str());
 if (index > (szTable-1))        {cout << "Out of Range !" << endl; return;}


 vMainWorkTable[index].FreeswitchData.fDisplay();
 vMainWorkTable[index].CallData.fDisplay();

}

void Display_Phonebook_List()
{
 extern vector <PhoneBookEntry>             vCALL_LIST;
 size_t                                     szList;
 bool                                       boolShowHeader = true;
 
 szList = vCALL_LIST.size();
 cout << "size -> " << szList << endl;
 for (unsigned int i = 0; i < szList; i++){
  if (i > 0) {boolShowHeader = false;}
  vCALL_LIST[i].fDisplay(i,boolShowHeader);
 }
 cout << endl;
}

void Display_Current_Calls()
{
 int                                            intRC;
 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();
 
  intRC = sem_wait(&sem_tMutexMainWorkTable);
  if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn Display_Current_Calls", 1);}
  cout << endl;
  cout << "TRUNK  STATUS            UUID                CALLBACK" << endl;
  cout << "-----------------------------------------------------------------" << endl;
  if (sz == 0) { cout << "None" << endl;}
  for (unsigned int i=0; i < sz; i++)
   {

    cout << setw(7) << left << vMainWorkTable[i].CallData.stringTrunk;
    cout << setw(18) << left << CallStateString(vMainWorkTable[i].intCallStateCode);
    cout << setw(20) << left << vMainWorkTable[i].CallData.stringUniqueCallID;
    cout << setw(30) << left << vMainWorkTable[i].CallData.strCallBackDisplay << endl;;


   }
  cout << endl;

  sem_post(&sem_tMutexMainWorkTable);
}

void InitializeClassofServiceVector()
{
  vstrCLASS_OF_SERVICE_ABBV[0]  = CLASS_OF_SERVICE_ABBV_0;
  vstrCLASS_OF_SERVICE_ABBV[1]  = CLASS_OF_SERVICE_ABBV_1;
  vstrCLASS_OF_SERVICE_ABBV[2]  = CLASS_OF_SERVICE_ABBV_2;
  vstrCLASS_OF_SERVICE_ABBV[3]  = CLASS_OF_SERVICE_ABBV_3;
  vstrCLASS_OF_SERVICE_ABBV[4]  = CLASS_OF_SERVICE_ABBV_4;
  vstrCLASS_OF_SERVICE_ABBV[5]  = CLASS_OF_SERVICE_ABBV_5;
  vstrCLASS_OF_SERVICE_ABBV[6]  = CLASS_OF_SERVICE_ABBV_6;
  vstrCLASS_OF_SERVICE_ABBV[7]  = CLASS_OF_SERVICE_ABBV_7;
  vstrCLASS_OF_SERVICE_ABBV[8]  = CLASS_OF_SERVICE_ABBV_8;
  vstrCLASS_OF_SERVICE_ABBV[9]  = CLASS_OF_SERVICE_ABBV_8;
  vstrCLASS_OF_SERVICE_ABBV[10] = CLASS_OF_SERVICE_ABBV_A;
  vstrCLASS_OF_SERVICE_ABBV[11] = CLASS_OF_SERVICE_ABBV_B;
  vstrCLASS_OF_SERVICE_ABBV[12] = CLASS_OF_SERVICE_ABBV_C;
  vstrCLASS_OF_SERVICE_ABBV[13] = CLASS_OF_SERVICE_ABBV_D;
  vstrCLASS_OF_SERVICE_ABBV[14] = CLASS_OF_SERVICE_ABBV_E;
  vstrCLASS_OF_SERVICE_ABBV[15] = CLASS_OF_SERVICE_ABBV_F;
  vstrCLASS_OF_SERVICE_ABBV[16] = CLASS_OF_SERVICE_ABBV_G;
  vstrCLASS_OF_SERVICE_ABBV[17] = CLASS_OF_SERVICE_ABBV_H;
  vstrCLASS_OF_SERVICE_ABBV[18] = CLASS_OF_SERVICE_ABBV_I;
  vstrCLASS_OF_SERVICE_ABBV[19] = CLASS_OF_SERVICE_ABBV_J;
  vstrCLASS_OF_SERVICE_ABBV[20] = CLASS_OF_SERVICE_ABBV_K;
  vstrCLASS_OF_SERVICE_ABBV[21] = CLASS_OF_SERVICE_ABBV_V;
  vstrCLASS_OF_SERVICE_ABBV[22] = CLASS_OF_SERVICE_ABBV_W;
}

int IndexWithConfNumberMainTable(unsigned long long int iConfNumber)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();

 if (!iConfNumber) {return -1;}

  for (unsigned int i = 0; i< sz; i++)
   {
    if (vMainWorkTable[i].CallData.intConferenceNumber == iConfNumber) {return i;}
   }
  return -1;
}

int IndexWithNenaCallUniqueIDMainTable(string strUniqueID)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();

 if ((strUniqueID.empty())||(strUniqueID == "0")) {return -1;}

  for (unsigned int i = 0; i< sz; i++)
   {
    if (vMainWorkTable[i].strNenaCallId == strUniqueID) {return i;}
   }
  return -1;
}


int IndexWithCallUniqueIDMainTable(string strUniqueID)
{
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();

 if ((strUniqueID.empty())||(strUniqueID == "0")) {return -1;}

  for (unsigned int i = 0; i< sz; i++)
   {
    if (vMainWorkTable[i].CallData.stringUniqueCallID == strUniqueID) {return i;}
   }
  return -1;
}
 
int IndexWithNENAcallID(string strUUID) {
// table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();

 for (unsigned int i = 0; i< sz; i++) {
  if (strUUID == vMainWorkTable[i].strNenaCallId) {return i;}
 }
 return -1;
}

int IndexWithFreeswitchChannelUUID(string strUUID) {

 // table must be semaphore protected before calling this function !
 extern vector <ExperientDataClass>             vMainWorkTable;
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();
 vector <Channel_Data>::size_type               szChannel;


 if ((strUUID.empty())||(strUUID == "0")) {return -1;}

 for (unsigned int i = 0; i< sz; i++)
  {
   //check ring /dial object
   if (vMainWorkTable[i].FreeswitchData.objRing_Dial_Data.strChannelID == strUUID) {return i;}
   //check desination vector
   szChannel = vMainWorkTable[i].FreeswitchData.vectDestChannelsOnCall.size();
   for (unsigned int j = 0; j< szChannel; j++)
    {
     if (vMainWorkTable[i].FreeswitchData.vectDestChannelsOnCall[j].strChannelID == strUUID)
      {return i;}
    }
   szChannel = vMainWorkTable[i].FreeswitchData.vectPostionsOnCall.size();
   for (unsigned int j = 0; j< szChannel; j++)
    {
     if (vMainWorkTable[i].FreeswitchData.vectPostionsOnCall[j].strChannelID == strUUID)
      {return i;}
    }
  }

 return -1;
}

 
void ShutdownConfirm()
{
 int                    charResponse = -1;

 cout << endl << "Confirm Controller Shutdown ... Press \"Y\"" << endl;

  // Make Keyboard Input NoEcho
 charResponse = NonBlockingNoEchoInputChar();

 if ((charResponse =='y')||(charResponse =='Y')) {boolSTOP_EXECUTION = true;}
 else                                            {cout << "Continuing Controller Operation ..." << endl;}

}

/*

void Determine_Dial_Prefix_Rule()
{
extern int                                      intCLID_DIAL_PREFIX_CODE;
extern int                                      int911_DIAL_PREFIX_CODE;
extern int                                      intSIP_DIAL_PREFIX_CODE;
extern string                                   strDIAL_PREFIX_STAR;
extern string                                   strDIAL_PREFIX_NINE;
extern string                                   strDIAL_PREFIX_NONE;



int911_DIAL_PREFIX_CODE  = 0;
intSIP_DIAL_PREFIX_CODE  = 0;
intCLID_DIAL_PREFIX_CODE = 0;





 

 //911 CAMA 
 if  (strDIAL_PREFIX_STAR == DEFAULT_VALUE_ANI_INI_KEY_13) {int911_DIAL_PREFIX_CODE += 1;}
 else                                                      { cout << "Invalid Trunk type assigned to * prefix" << endl; }

 if  (strDIAL_PREFIX_NINE == DEFAULT_VALUE_ANI_INI_KEY_13) {int911_DIAL_PREFIX_CODE += 2;}
 if  (strDIAL_PREFIX_NONE == DEFAULT_VALUE_ANI_INI_KEY_13) {int911_DIAL_PREFIX_CODE += 4;}
 
 //SIP
 if  (strDIAL_PREFIX_NINE == DEFAULT_VALUE_ANI_INI_KEY_14) {intSIP_DIAL_PREFIX_CODE += 2;}
 if  (strDIAL_PREFIX_NONE == DEFAULT_VALUE_ANI_INI_KEY_14) {intSIP_DIAL_PREFIX_CODE += 4;}

 // CLID
 if  (strDIAL_PREFIX_NINE == DEFAULT_VALUE_ANI_INI_KEY_15) {intCLID_DIAL_PREFIX_CODE += 2;}
 if  (strDIAL_PREFIX_NONE == DEFAULT_VALUE_ANI_INI_KEY_15) {intCLID_DIAL_PREFIX_CODE += 4;}


}

*/
void ExperientDataClass::UpdatePSAPstatus()
{
 PSAP_STATUS 					objStatus;
 trunk_type                 			eTrunkType;
 string                                         strPSAPname;
 extern vector <ExperientDataClass>             vMainWorkTable;
 extern Workstation_Groups                      WorkstationGroups;
 extern WorkStation                             WorkStationTable[NUM_WRK_STATIONS_MAX+1];
 extern bool                                    boolShowPSAPstatusApplet;
 int                                            index;
 int 						intRC;
 bool                                           bPositionOn911Call[95];
 bool                                           bPositionOnAdminCall[95];
 vector <ExperientDataClass>::size_type         sz = vMainWorkTable.size();
 // Main Worktable should be locked Prior to Calling this function !

 if (boolUSE_DASHBOARD)                           {DashboardUpdate();}
 if (WorkstationGroups.WorkstationGroups.empty()) {return;}
 if (!boolShowPSAPstatusApplet)                   {return;}

 for (unsigned int i = 0; i< 95; i++) {bPositionOn911Call[i] = bPositionOnAdminCall[i] = false;}

 intRC = sem_wait(&sem_tMutexWRKWorkTable);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in fn ExperientDataClass::UpdatePSAPstatus", 1);}
 this->vPSAPstatus.clear();
   for (unsigned int i = 0; i< WorkstationGroups.WorkstationGroups.size(); i++)
    {
     if (WorkstationGroups.WorkstationGroups[i].strGroupName == "ALL")        {continue;}
     WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnEmergencyCall = 0;
     WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnAdminCall = 0;
     WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline = 0;
     // Determine Positions OnLine

     for (unsigned int j = 0; j< WorkstationGroups.WorkstationGroups[i].WorkstationsInGroup.size(); j++) 
      {
       if (WorkStationTable[WorkstationGroups.WorkstationGroups[i].WorkstationsInGroup[j]].boolActive) {WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline++;} 
      }
    }

  intRC = sem_post(&sem_tMutexWRKWorkTable); 


  for (unsigned int i = 0; i< sz; i++)
   {
    eTrunkType = TRUNK_TYPE_MAP.Trunk[vMainWorkTable[i].CallData.intTrunk].TrunkType;
    // Don't inculde admin Calls

    if (vMainWorkTable[i].CallData.intTrunk > 94)          {continue;}
    
    for (int j = 1; j < 95; j++) 
     {
      if (vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF,j) )
       {
        switch(eTrunkType)
         {
          case CLID: case SIP_TRUNK:
           if ((vMainWorkTable[i].CallData.fIsOnHold(j) )|| ( bPositionOn911Call[j] ) ){break;}
           bPositionOnAdminCall[j] = true;
           break;
          default:
           bPositionOn911Call[j] = true; bPositionOnAdminCall[j] = false;
           break;
         }

       } // end if ((vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF,j)&&(!vMainWorkTable[i].CallData.fIsOnHold(j).)

     } // end  for (int j = 1; j < 95; j++)

   }// end for (unsigned int i = 0; i< sz; i++)

  for (int j = 1; j < 95; j++) 
   {
    index = WorkstationGroups.IndexOfGroupWithWorkstation(j);
    if (index < 0 ) {continue;}
    if  (bPositionOnAdminCall[j]) { WorkstationGroups.WorkstationGroups[index].PSAPStatus.iPositionsOnAdminCall++;}      // change later
    if  (bPositionOn911Call[j])   { WorkstationGroups.WorkstationGroups[index].PSAPStatus.iPositionsOnEmergencyCall++;}    
   }

    


  for (unsigned int i = 0; i< WorkstationGroups.WorkstationGroups.size(); i++)
   {
    if (WorkstationGroups.WorkstationGroups[i].strGroupName == "ALL")        {continue;}
    if ((WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnAdminCall + WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnEmergencyCall) > WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline)
     {
       WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline = WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnAdminCall + WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnEmergencyCall;
     }
    this->vPSAPstatus.push_back(WorkstationGroups.WorkstationGroups[i].PSAPStatus);   
    }



//Workstation_Group_Name
fPSAPappletData_XML_Message("");


}

void ForceGUIupdate()
{
 int intRC;
 ExperientDataClass objData;
 
 extern vector <ExperientDataClass>             vMainWorkTable;
 extern vector <ExperientDataClass>             vANIWorktable;

 objData.fUpdateGUIXMLMessage();
 objData.boolBroadcastXMLtoSinglePosition = false;

 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn ForceGUIupdate()", 1);}
 intRC = sem_wait(&sem_tMutexANIWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexANIWorkTable, "sem_wait@sem_tMutexANIWorkTable in ForceGUIupdate()", 1);}

 if ((vMainWorkTable.empty())&&(vANIWorktable.empty())) {Queue_WRK_Event(objData);cout << "Update Sent ..." << endl;}
 else {cout << "Unable..... Calls in Progress" << endl;}

 sem_post(&sem_tMutexANIWorkTable);
 sem_post(&sem_tMutexMainWorkTable);
}

void Kernel_Option_Force_Disconnect()
{
 string strUniqueCallID;
 int    intRC;

 cout << "Enter 18 Digit UniqueID :";
 getline (cin, strUniqueCallID); 

 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in fn ForceGUIupdate()", 1);}
 Force_Disconnect(strUniqueCallID);
 sem_post(&sem_tMutexMainWorkTable);
}

void Kernel_Option_Reload_Workstation()
{
 ExperientDataClass objData;
 MessageClass       objMessage;
 int                intStart = 1;
 int                intEnd   = 0;
 int                intRC;
 string             strInput;
 WorkStation        NotifyWorkstation;
 

 cout << "Enter Workstation to Reset ---> (a) for all :";
 getline (cin, strInput); 

 if (strInput == "a")                                                                             {intEnd = intNUM_WRK_STATIONS; }
 else if (ValidateFieldAndRange(MAIN, strInput, strInput, 1, "Position", intNUM_WRK_STATIONS, 1)) {intStart = intEnd = char2int(strInput.c_str());}
 else                                                                                             {return;}

  // Lock Work Table
 intRC = sem_wait(&sem_tMutexMainWorkTable);
 if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in  Kernel_Option_Reload_Workstation()", 1);}

 for (int i = intStart; i <= intEnd; i++)
  {
   objData.intWorkStation = i;
   objData.fLoad_Position(SOFTWARE, MAIN, int2strLZ(i), 0); 
   objMessage.fMessage_Create(LOG_CONSOLE_FILE,725, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objData.fCallData(), objBLANK_TEXT_DATA, objBLANK_WRK_PORT,
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_725 , int2strLZ(i)); 
   enQueue_Message(WRK,objMessage);
   objData.stringXMLString = NotifyWorkstation.fCreateAlert(WRK_MESSAGE_725w, objData.fCallData());
   objData.boolBroadcastXMLtoSinglePosition = true;
   objData.boolReloadWorkstationGrid        = true;
   Queue_WRK_Event(objData);
  }
 sem_post(&sem_tMutexMainWorkTable);
}

void ToggleAMIScript()
{
 boolAMI_RECORD_SCRIPT = (!boolAMI_RECORD_SCRIPT);
 string strOutput;

 if (boolAMI_RECORD_SCRIPT) {strOutput = "ON";}
 else                     {strOutput = "OFF";}
 cout << endl << "AMI Scripting is now turned " << strOutput << endl;
}

void DeleteAMIScript()
{
 int intRC;
 
 if (boolAMI_RECORD_SCRIPT) {ToggleAMIScript();}

 string strAMIscriptName = GetLogFileName(AMI_SCRIPT);
 string strCommand       = "rm " + strAMIscriptName;
 intRC = system(strCommand.c_str());
 if (!intRC) {cout << endl << "file: " << strAMIscriptName << " deleted" << endl;}
 else        {cout << endl << "problem deleteing file: " << strAMIscriptName << endl;}

}

void Check_Abandoned_List(ExperientDataClass objData)
{
 extern vector <ExperientDataClass>     vMainWorkTable;
 vector <ExperientDataClass>::size_type sz; 
 MessageClass                           objMessage;
 bool                                   bool_MATCH;                 
 bool					boolA, boolB, boolC, boolD;
 bool                                   boolE, boolF, boolG, boolH, boolJ;
 //worktable should be semaphore protected prior to call 
 do
  {
   bool_MATCH = false; 
   sz = vMainWorkTable.size();
   for (unsigned int i = 0; i < sz; i++)
    {
     if (vMainWorkTable[i].boolAbandoned){
       if (vMainWorkTable[i].CallData.ConfData.vectConferenceChannelData.size() != 1) {
        SendCodingError("Experient_Controller.cpp Check_Abandoned_List() No ConfData to compare!"); 
        continue;
       }
       boolA = (AddNineForOutgoingCalls(CheckForLongDistance(vMainWorkTable[i].CallData.ConfData.vectConferenceChannelData[0].strCallerID )) == objData.CallData.TransferData.strTransfertoNumber);
       boolB =  (vMainWorkTable[i].CallData.ConfData.vectConferenceChannelData[0].strCallerID  == objData.CallData.TransferData.strTransfertoNumber);
       boolC =  ( (CheckForLongDistance(vMainWorkTable[i].CallData.ConfData.vectConferenceChannelData[0].strCallerID )) == objData.CallData.TransferData.strTransfertoNumber);
       boolD = (AddNineForOutgoingCalls(vMainWorkTable[i].CallData.ConfData.vectConferenceChannelData[0].strCallerID ) == objData.CallData.TransferData.strTransfertoNumber);
       boolE = (vMainWorkTable[i].CallData.stringTenDigitPhoneNumber == objData.CallData.TransferData.strTransfertoNumber);
       boolF = (vMainWorkTable[i].CallData.stringSevenDigitPhoneNumber == objData.CallData.TransferData.strTransfertoNumber);
       boolG = (AddNineForOutgoingCalls(vMainWorkTable[i].CallData.stringSevenDigitPhoneNumber ) == objData.CallData.TransferData.strTransfertoNumber);
       boolH = (AddNineForOutgoingCalls(vMainWorkTable[i].CallData.stringTenDigitPhoneNumber ) == objData.CallData.TransferData.strTransfertoNumber);
       boolJ = (AddNineForOutgoingCalls(CheckForLongDistance(vMainWorkTable[i].CallData.stringTenDigitPhoneNumber )) == objData.CallData.TransferData.strTransfertoNumber);

       if (boolA||boolB||boolC||boolD||boolE||boolF||boolG||boolH||boolJ){ 
         // send take control to workstation
         Take_Control(i, objData); bool_MATCH = true; break;
       }
     }// end if vMainWorkTable[i].boolAbandoned
    }// end for (unsigned int i = 0; i < sz; i++)

  } while (bool_MATCH);
}

void Take_Control(int iTableIndex, ExperientDataClass objData)
{
 extern vector <ExperientDataClass>     vMainWorkTable;
 MessageClass                           objMessage; 
 bool                                   bPreviousALI;
 WorkStation                            NotifyWorkstation;

 //worktable should be semaphore protected prior to call 

 // Update Position to that of Workstation
 vMainWorkTable[iTableIndex].fLoad_Position(SOFTWARE, MAIN, int2strLZ(objData.intWorkStation), 0);
 vMainWorkTable[iTableIndex].CallData.fConferenceStringAdd(POSITION_CONF, objData.intWorkStation);
 vMainWorkTable[iTableIndex].CallData.fConferenceStringRemove(POSITION_CONF, objData.intWorkStation);
 vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.fLoadConferenceDisplayP(objData.intWorkStation);
 vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.iPositionNumber = objData.intWorkStation;
 vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.boolConnected = false;

 if (objData.intWorkStation > 0) { 
  vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.strCallerID = TelephoneEquipment.fCallerIDnameofTelephonewithPosition(objData.intWorkStation);
  vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.strCustName = TelephoneEquipment.fCallerIDnumberofTelephonewithPosition(objData.intWorkStation);
  vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.IPaddress.stringAddress = TelephoneEquipment.fIPaddressFromPositionNumber(objData.intWorkStation);
 } 
 else {
  vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.strCallerID = strCALLER_ID_NUMBER;
  vMainWorkTable[iTableIndex].FreeswitchData.objChannelData.strCustName = strCALLER_ID_NAME; 
 }
 vMainWorkTable[iTableIndex].CallData.ConfData.vectConferenceChannelData.push_back(vMainWorkTable[iTableIndex].FreeswitchData.objChannelData);         
 bPreviousALI     = (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived);                      

 //Send updated Position to ALI
 vMainWorkTable[iTableIndex].enumANIFunction = TAKE_CONTROL;
 SendANItoALI(vMainWorkTable[iTableIndex]);


 // Send Update Position XML to all Workstations
 vMainWorkTable[iTableIndex].fLoad_CallState(MAIN,XML_STATUS_MSG_ABANDONED_CLEARED,"SOFTWARE LOAD");
 vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_DISCONNECT, bPreviousALI);
 //  if(!vMainWorkTable[iTableIndex].fCallInfo_XML_Message(ConvertCallStateToStatus(vMainWorkTable[iTableIndex].intCallStateCode) , vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived)        ){break;}
 vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = false;
 Queue_WRK_Event(vMainWorkTable[iTableIndex]);

 if (vMainWorkTable[iTableIndex].CallData.intPosn) {
  vMainWorkTable[iTableIndex].CallData.eJSONEventWRKcode = WRK_TAKE_CONTROL; 
          
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 733, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].fCallData(), vMainWorkTable[iTableIndex].TextData,
                             objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_733);
 }
 else {
  vMainWorkTable[iTableIndex].CallData.eJSONEventWRKcode = WRK_TAKE_CONTROL_SOFTWARE;           
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 733, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vMainWorkTable[iTableIndex].fCallData(), vMainWorkTable[iTableIndex].TextData,
                             objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,WRK_MESSAGE_733k);
 }
 enQueue_Message(WRK,objMessage);  


 // send to CAD
 if((boolSEND_TO_CAD_ON_TAKE_CONTROL)&&(vMainWorkTable[iTableIndex].CallData.intPosn > 0)){SendALItoCAD_SinglePosition(vMainWorkTable[iTableIndex]);}

 vMainWorkTable[iTableIndex].boolAbandoned = false;
 vMainWorkTable[iTableIndex].boolDisconnect = true;
 vMainWorkTable[iTableIndex].UpdatePSAPstatus();
 if (vMainWorkTable[iTableIndex].ALIData.boolALIRecordReceived) {
  vMainWorkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
  vMainWorkTable[iTableIndex].WindowButtonData.fSetButtonsForALIReceived();
  vMainWorkTable[iTableIndex].fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
  Queue_WRK_Event(vMainWorkTable[iTableIndex]);
 }
 vMainWorkTable[iTableIndex].boolBroadcastXMLtoSinglePosition = true;
 vMainWorkTable[iTableIndex].stringXMLString = NotifyWorkstation.fCreateAlert(WORKSTATION_TAKE_CONTROL_MESSAGE, vMainWorkTable[iTableIndex].fCallData());
 Queue_WRK_Event(vMainWorkTable[iTableIndex]);



// vMainWorkTable.erase(vMainWorkTable.begin()+iTableIndex); don't erase ... Call now set as disconnected...
 vMainWorkTable[iTableIndex].stringDisConnectTimeStamp = get_time_stamp(vMainWorkTable[iTableIndex].timespecTimeAbandonedStamp);
 vMainWorkTable[iTableIndex].enumANIFunction = DISCONNECT;
 vMainWorkTable[iTableIndex].ALIData.intNumberOfInitialBids  = 0;

 //cout << "took control ..................... size = " << vMainWorkTable.size() << endl;

 

}

bool PositionIsOnAnotherCall(int iPosition, unsigned int index)
{
 size_t                                 sz;
 extern vector <ExperientDataClass>     vMainWorkTable;

 // Table should be semaphore protected before calling !

 if (iPosition <= 0 )                 {return false;}
 if (iPosition > intNUM_WRK_STATIONS) {return false;}

 sz = vMainWorkTable.size();

 for (unsigned int i = 0; i <  sz; i++)
  {
   if (i == index) {continue;}

   if ((vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF, iPosition)) && (!vMainWorkTable[i].CallData.fIsOnHold(iPosition))) {return true;}

  }


 return false;
}

void DailyFreeswitchTimeSync()
{
 extern volatile int    	intDayofMonthFreeswitchTimeSyncSent;
 int                    	intToday;
 ExperientDataClass 		objData;

 intToday = GetDayofMonth();

 if (intToday == intDayofMonthFreeswitchTimeSyncSent) {return;}

 intDayofMonthFreeswitchTimeSyncSent = intToday;

 objData.enumANIFunction = SYNC_TIME;
 Queue_AMI_Input(objData);
 sem_post(&sem_tAMIFlag);
 return;
}


void EmailDailyAlarmsSummary()
{
 int                    	intToday;
 bool                           boolAlarmDetected = false;
 string                         strMessage = "The Following Ports are in ALARM status:\n";

 extern volatile int    	intDailyAlarmEmail;
 extern int                     intNUM_ALI_PORT_PAIRS;
 extern ExperientCommPort  	ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1]; 
 
 
 if (!boolEmailON ) {return;}

 intToday = GetDayofMonth();
 

 if (!intDailyAlarmEmail) {intDailyAlarmEmail = intToday;}

 if (intDailyAlarmEmail == intToday) {return;}


 if (GetHour() == intEMAIL_ALARM_SUMMARY_ON_THIS_HOUR)
  {
   //check ALI
    for (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++) {for (int j = 1; j <= 2; j++)  
    if (ALIPort[i][j].boolReducedHeartBeatModeFirstAlarm)
     {
      boolAlarmDetected = true;
      strMessage += "ALI Port "; strMessage += int2strLZ(i);
      switch (j)
       {
        case 1: strMessage += "A"; break;
        case 2: strMessage += "B"; break;
       } 
      strMessage += "\n";
     }  

    }
    


   intDailyAlarmEmail = intToday;
  }


 if (boolAlarmDetected) {cout << strMessage << endl;} //TBC with email ......
 return;
}

bool WorkstationIsOnaCall(int iWorkstation)
{
 int                                            intRC;

 extern vector <ExperientDataClass>             vMainWorkTable;
 // Worktable MustbeLocked prior to Call

 size_t sz = vMainWorkTable.size();

 for (unsigned int i = 0; i < sz; i++) 
  { 
   if (vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF, iWorkstation))
                             {sem_post(&sem_tMutexMainWorkTable);return true;}
  }

 return false;
}

int FindOpenNonDialLineIndexforBarge(int iWorkstation)
{
 int                                            intRC;
 int                                            index;
 Telephone_Device                               Telephone;
 vector <ExperientDataClass>::size_type         sz;
 bool                                           boolVectorsEmpty;
 int                                            intLineIndex;
 extern vector <ExperientDataClass>             vMainWorkTable;
 extern Telephone_Devices                       TelephoneEquipment;
 

                                       
 index = TelephoneEquipment.fIndexWithPositionNumber(iWorkstation);
 if (index < 0) {return 0;} 

 //check phone state from Polycom if avail
 if (TelephoneEquipment.Devices[index].fUpdateLinesInUse() )  
  { 
  // cout << "lines updated ..." << endl;
   for (int i = 1; i <= TelephoneEquipment.Devices[index].iNumberOfLines; i++) 
    {
      if ((!TelephoneEquipment.Devices[index].vPhoneLines[i].OutboundDial)&&(!TelephoneEquipment.Devices[index].vPhoneLines[i].boolInUse))
       { return TelephoneEquipment.Devices[index].vPhoneLines[i].LineIndex; }
    }
   return 0;
  }

 // cout << "got here in line number" << endl;
 //vMainWorkTable semaphore should be locked !!!!
// intRC = sem_wait(&sem_tMutexMainWorkTable);
// if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in FindOpenLineIndexforDialing()", 1);}

 sz = vMainWorkTable.size();

 intLineIndex = TelephoneEquipment.Devices[index].fFirstOutboundLineIndex();
 if (sz == 0) { 
               if (intLineIndex > 0) { return intLineIndex;}
               SendCodingError("Experient_Controller.cpp - 2. Coding error FindOpenNonDialLineIndexforBarge() No outbound Lines defined");
               return 1;}

 for (int i = 1; i <= TelephoneEquipment.Devices[index].iNumberOfLines; i++)
  {
   // Check if Line Is set for outbound dialing
   if (TelephoneEquipment.Devices[index].vPhoneLines[i].OutboundDial)                                                                         {continue;}   
  
   for (unsigned int y = 0; y< sz; y++)
    {

     // Check if a line is ringing ...... channel vectors will be empty .....
     boolVectorsEmpty = ((vMainWorkTable[y].FreeswitchData.vectPostionsOnCall.empty()) && (vMainWorkTable[y].FreeswitchData.vectDestChannelsOnCall.empty()));

     //check if line i is ringing ......
     if ((vMainWorkTable[y].FreeswitchData.objRing_Dial_Data.iRingingLineNumber[iWorkstation] == i )&&(boolVectorsEmpty))                       {continue;}

     if (vMainWorkTable[y].FreeswitchData.fLineRegistrationNameIsOnCall(TelephoneEquipment.Devices[index].vPhoneLines[i].LineRegistrationName)) {continue;}

     //if we make it here the line is available ...
     sem_post(&sem_tMutexMainWorkTable);
     return TelephoneEquipment.Devices[index].vPhoneLines[i].LineIndex;

    }
  }
 
 //TBC

// sem_post(&sem_tMutexMainWorkTable);
 return 0;
}

bool DisconnectRequest(int iTableIndex)
{
 vector <ExperientDataClass>::size_type         sz;
 int                                            intNumPositionsOnCall;
 extern vector <ExperientDataClass>             vMainWorkTable;

 // table should be locked prior to calling this function
 sz = vMainWorkTable.size();
 if (iTableIndex > (int) sz) {return false;}
 intNumPositionsOnCall = vMainWorkTable[iTableIndex].CallData.fNumPositionsInConference();
 switch (intNumPositionsOnCall)
  {
   case 0: return false;
   case 1: return false;
   default:
    // more than 1 in conference
 

   return true;
  }



}
int FindOpenLineIndexforDialing(int iWorkstation)
{
 int                                            intRC;
 int                                            index;
 Telephone_Device                               Telephone;
 vector <ExperientDataClass>::size_type         sz;
 bool                                           boolVectorsEmpty;
 int                                            intLineIndex;
 extern vector <ExperientDataClass>             vMainWorkTable;
 extern Telephone_Devices                       TelephoneEquipment;
 
/*return index of open line need to change functionName ... */
                                       
 index = TelephoneEquipment.fIndexWithPositionNumber(iWorkstation);
 if (index < 0) {return 0;} 

 //check phone state from Polycom if avail
 if (TelephoneEquipment.Devices[index].fUpdateLinesInUse() ) { 
   for (int i = 1; i <= TelephoneEquipment.Devices[index].iNumberOfLines; i++) 
    {
      if ((TelephoneEquipment.Devices[index].vPhoneLines[i].OutboundDial)&&(!TelephoneEquipment.Devices[index].vPhoneLines[i].boolInUse))
       { return TelephoneEquipment.Devices[index].vPhoneLines[i].LineIndex; }
    }
   return 0;
 }

 // cout << "got here in line number" << endl;
 //vMainWorkTable semaphore should be locked !!!!
// intRC = sem_wait(&sem_tMutexMainWorkTable);
// if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainWorkTable, "sem_wait@sem_tMutexMainWorkTable in FindOpenLineIndexforDialing()", 1);}

 sz = vMainWorkTable.size();

 intLineIndex = TelephoneEquipment.Devices[index].fFirstOutboundLineIndex();
 if (sz == 0) { 
               if (intLineIndex > 0) { return intLineIndex;}
               SendCodingError("cti.cpp - 2. Coding error FindOpenLineIndexforDialing() No outbound Lines defined");
               return 1;}

 for (int i = 1; i <= TelephoneEquipment.Devices[index].iNumberOfLines; i++)
  {
   // Check if Line Is set for outbound dialing
   if (!TelephoneEquipment.Devices[index].vPhoneLines[i].OutboundDial)                                                                         {continue;}   
  
   for (unsigned int y = 0; y< sz; y++)
    {

     // Check if a line is ringing ...... channel vectors will be empty .....
     boolVectorsEmpty = ((vMainWorkTable[y].FreeswitchData.vectPostionsOnCall.empty()) && (vMainWorkTable[y].FreeswitchData.vectDestChannelsOnCall.empty()));

     //check if line i is ringing ......
     if ((vMainWorkTable[y].FreeswitchData.objRing_Dial_Data.iRingingLineNumber[iWorkstation] == i )&&(boolVectorsEmpty))                       {continue;}

     if (vMainWorkTable[y].FreeswitchData.fLineRegistrationNameIsOnCall(TelephoneEquipment.Devices[index].vPhoneLines[i].LineRegistrationName)) {continue;}

     //if we make it here the line is available ...
     sem_post(&sem_tMutexMainWorkTable);
     return TelephoneEquipment.Devices[index].vPhoneLines[i].LineIndex;

    }
  }
 
 //TBC

// sem_post(&sem_tMutexMainWorkTable);
 return 0;
}


// test code .....
void testTDDmodeOnIndexZero()
{
  extern vector <ExperientDataClass>             vMainWorkTable;
 ExperientDataClass                              objData;

 objData =  vMainWorkTable[0];
 objData.enumANIFunction = TDD_MODE_ON;
 objData.intWorkStation = 2;
 Queue_ANI_Event(objData);            

}

void testWorkstationDoubleAnswer()
{
 ExperientDataClass                              objWRKData1, objWRKData2;

 cout << "TEST Double answer" << endl;
 objWRKData1.enumWrkFunction= WRK_PICKUP;
 objWRKData1.intWorkStation = 1;
 objWRKData1.enumANISystem = enumANI_SYSTEM;
 objWRKData2.enumWrkFunction= WRK_PICKUP;
 objWRKData2.intWorkStation = 2;
 objWRKData2.enumANISystem = enumANI_SYSTEM;

 enqueue_Main_Input(WRK, objWRKData2);
 enqueue_Main_Input(WRK, objWRKData1);
}




void SendTestTDDMessage()
{
  extern vector <ExperientDataClass>             vMainWorkTable;
 ExperientDataClass                              objData;
 cout << "test TDD" << endl;
 objData =  vMainWorkTable[0];
 
 objData.enumANIFunction = TDD_CHAR_RECEIVED;
 objData.intWorkStation = 2;
 //objData.FreeswitchData.fDisplay();
 objData.FreeswitchData.objChannelData.strChannelID = objData.FreeswitchData.vectDestChannelsOnCall[0].strChannelID;
 cout << objData.FreeswitchData.objChannelData.strChannelID << endl;
 objData.FreeswitchData.objChannelData.strTranData = "T";
 Queue_ANI_Event(objData);
 sleep(1);
 objData.FreeswitchData.objChannelData.strTranData = "E";
 Queue_ANI_Event(objData); 
 sleep(1);         
 objData.FreeswitchData.objChannelData.strTranData = "S";
 Queue_ANI_Event(objData);
 sleep(1);
 objData.FreeswitchData.objChannelData.strTranData = "T";
 Queue_ANI_Event(objData);
 sleep(1);
 objData.FreeswitchData.objChannelData.strTranData = charCR;
 Queue_ANI_Event(objData);
 cout << "done" << endl;
}

void Write_to_Shutdown_file(int iNumber, string strMessage)
 {
  ofstream                 OutputFile;
  struct timespec          timespecTimeNow;
  string                   stringTimeStamp;
  extern string            get_time_stamp(timespec timespecArg); 


  OutputFile.open(charPREVIOUS_SHUT_DOWN_STATUS_FILE, ios::app);
  OutputFile << ERR_INI_KEY_4 << "= " << iNumber << endl;
  OutputFile << ERR_INI_KEY_5 << "= " << strMessage << endl;        
  OutputFile << ERR_INI_KEY_7 << "= false" <<endl;         

  stringTimeStamp = get_time_stamp(timespecTimeNow);
  OutputFile << setbase(10) << left << "TimeStamp = " << stringTimeStamp << endl;
                       
  OutputFile.close();  

 }

void    Reload_InitFile()
{
 struct timespec            timespecRemainingTime;
 extern struct timespec     timespecNanoDelay;

 UpdateVars.boolMAINThreadWait = true;
 boolUPDATE_VARS = true;
 SendSignaltoThreads();
 while ((!UpdateVars.fAllThreadsReady())&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);} 
 if (!boolSTOP_EXECUTION)   { 
    INIT_VARS.fReloadInitFileALI(); // updates all ALI global vars        
    INIT_VARS.fReloadInitFileANI(); // updates all ANI global vars 
    INIT_VARS.fReloadInitFileDSB(); // updates all DSB global vars
    INIT_VARS.fReloadInitFileADV(); // updates all ADV global vars 
    INIT_VARS.fReloadInitFileCAD(); // updates all CAD global vars 
 }
 boolUPDATE_VARS = false;
 UpdateVars.boolMAINThreadWait = false;

}

void AbortSignalCatcher(int iparam)
{
 signal(SIGABRT, SIG_DFL);
 char cErrorString[256];
 cout << "got abort" << endl;
 cout << "exit code = " << iparam << endl;
 cout << "error number = " << errno << endl;
 cout << "error message = " << strerror_r(errno, cErrorString, 256) << endl;
 Write_to_Shutdown_file(iparam, "Program Recieved Abort Signal");
// boolSTOP_EXECUTION  = true;
// Cleanup();
// experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC); 
// exit(iparam);
raise(SIGABRT);
}
void ILLSignalCatcher(int iparam)
{
 signal(SIGILL, SIG_DFL);
 char cErrorString[256];
 cout << "got illegal instruction" << endl;
 cout << "signal code = " << iparam << endl;
 cout << "error number = " << errno << endl;
 cout << "error message = " << strerror_r(errno, cErrorString, 256) << endl;
 Write_to_Shutdown_file(iparam, "Program Recieved Illegal Instruction Signal");
// boolSTOP_EXECUTION  = true;
// Cleanup();
// experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
// exit(iparam);
raise(SIGILL);
}
void FPESignalCatcher(int iparam)
{
 signal(SIGFPE, SIG_DFL);
  char cErrorString[256];
 cout << "got Floating Point Exception" << endl;
 cout << "signal code = " << iparam << endl;
 cout << "error number = " << errno << endl;
 cout << "error message = " << strerror_r(errno, cErrorString, 256) << endl;
 Write_to_Shutdown_file(iparam, "Program Recieved Floating Point Exception Signal");
// boolSTOP_EXECUTION  = true;
// Cleanup();
// experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
// exit(iparam);
raise(SIGFPE);
}
void ShutdownSignalCatcher(int iparam)
{
 cout << "SIGTERM" << endl;
 boolSTOP_EXECUTION  = true;
 CaughtShutdownSignal = true;
 Cleanup();
 Delete_Orderly_Shutdown_File();
 experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
 exit(iparam);
 return;

}

void    SendILLSignal()
{
 raise(SIGILL);	
}
void    SendAbortSignal()
{
 raise(SIGABRT);	
}
void    SendFPESignal()
{
 raise(SIGFPE);	
}
void    SendShutdownSignal()
{
 raise(SIGTERM);	
}
void    SendSegFaultSignal()
{
 raise(SIGSEGV);	
}

