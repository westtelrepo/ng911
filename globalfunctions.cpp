/*****************************************************************************
* FILE: globalfunctions.cpp
*  
*
* DESCRIPTION:
*  Contains functions that are global to the controller 
*
*
*
* AUTHOR: 06/12/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2022 WestTel Corporation  
******************************************************************************/

#include "header.h"
#include "globals.h"
#include "baseclasses.h"
#include "/datadisk1/src/simpleini/SimpleIni.h"          // ini software
#include "/datadisk1/src/xmlParser/xmlParser.h" 
using namespace std;

extern Text_Data                                objBLANK_TEXT_DATA;
extern Call_Data                                objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_ALI_PORT;
extern Port_Data                                objBLANK_KRN_PORT;
extern Port_Data                                objBLANK_LOG_PORT;

// fwd function declaration
void        Console_Message(string stringThreadCalling, string stringArg,  string stringArg1 = "",  string stringArg2 = "", string stringArg3 = "",
                            string stringArg4 = "", string stringArg5 = "" );

string      Create_Message(string stringArg, string stringArg1 = "", string stringArg2 = "", string stringArg3 = "",
                           string stringArg4 = "",string stringArg5 = "",string stringArg6 = "" );

void        Abnormal_Exit(enum threadorPorttype  enumThread, int RetCode, string sMessage, string sArg1 = "", string sArg2 = "",
                          string sArg3 = "", string sArg4 = "", string sArg5 = "");

string      Thread_Calling(threadorPorttype enumThreadCalling);
long double Round_ld( long double ldArg, int intDigits);
string      AsteriskPositionFromChannel(string strInput);
void        SendCodingError(string strInput);

float float_one_point_round(float value) {
 return ((float)((int)(value * 10))) / 10;
}

int IndexOfThreadWithName(string strName) {
  extern vector <Thread_Data>    ThreadData;
  size_t                         sz;
  
  sz = ThreadData.size();
  
  for (unsigned int i = 0; i < sz; i++) {
   if ( ThreadData[i].strThreadName == strName) {return i;}
  }
  
 return -1;   
}

bool OS_MEM_Usage() {
 MessageClass                   objMessage;
 string                         strMessage;
 string                         strData, strMEMtotal, strMEMavail, strSWAPtotal, strSWAPfree;
 string                         stringReturnMessage;
 FILE                           *fpipe;
 char                           *command;
 char                           line[1024];
 int                            index;
 string                         strCommand = "cat /proc/meminfo";
 size_t                         found;
 extern Memory_Data             MemoryData;

 extern string                  ParseFreeswitchData(string stringArg, string strKey);
 extern string                  FindandReplaceALL(string stringArg, string stringFind, string stringReplace);
 extern string                  RemoveAllSpaces(string strInput); 

 command = (char*) strCommand.c_str(); 
   
 if ( !(fpipe = (FILE*)popen(command,"r")) )  {
    // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,144, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_144, strCommand); 
   enQueue_Message(MAIN,objMessage);
   return false;
 } 
 
 strMessage.clear();
 while ( fgets( line, sizeof line, fpipe))  {
   strMessage += line;
 }
 
 pclose(fpipe);
 
 strMessage = FindandReplaceALL(strMessage,"\n", "\t");
 
 strMEMtotal = ParseFreeswitchData(strMessage, "MemTotal:");
 found = strMEMtotal.find_first_of("kB");
 if (found != string::npos) {
  strMEMtotal =  strMEMtotal.substr(0, found);  
 }
 strMEMtotal = RemoveAllSpaces(strMEMtotal);
 
 strMEMavail = ParseFreeswitchData(strMessage, "MemAvailable:");
 found = strMEMavail.find_first_of("kB");
 if (found != string::npos) {
  strMEMavail =  strMEMavail.substr(0, found);  
 }
 strMEMavail = RemoveAllSpaces(strMEMavail);
 
 if (!MemoryData.fLoadOSmemoryUsage(strMEMtotal,strMEMavail)) {
  return false;   
 }
 
 
 strSWAPtotal = ParseFreeswitchData(strMessage, "SwapTotal:");
 found = strSWAPtotal.find_first_of("kB");
 if (found != string::npos) {
  strSWAPtotal =  strSWAPtotal.substr(0, found);  
 }
 strSWAPtotal = RemoveAllSpaces(strSWAPtotal);
 
 
 strSWAPfree = ParseFreeswitchData(strMessage, "SwapFree:");
 found = strSWAPfree.find_first_of("kB");
 if (found != string::npos) {
  strSWAPfree =  strSWAPfree.substr(0, found);  
 }
 strSWAPfree = RemoveAllSpaces(strSWAPfree);
 
 if (!MemoryData.fLoadOSswapUsage(strSWAPtotal,strSWAPfree)) {
  return false;   
 } 
 

 return true;
}

bool Get_OS_Statistics() {
 
 int                            index;
 string                         strControllerCPU;
 string                         strControllerMEM;

 extern Memory_Data             MemoryData;
 extern vector <Thread_Data>    ThreadData;
 
 extern string CPU_USAGE(pid_t pid);
 extern string MEM_USAGE(pid_t pid);
 extern bool   OS_MEM_USAGE();
 extern int    IndexOfThreadWithName(string strName);

 
 index = IndexOfThreadWithName("MAIN");
 if (index < 0) {return false;}
 
 strControllerCPU = CPU_USAGE(ThreadData[index].ThreadPID);
 strControllerMEM = MEM_USAGE(ThreadData[index].ThreadPID);
 
 if (!MemoryData.fLoadControllerCPUusage(strControllerCPU ) ) {return false;}
 if (!MemoryData.fLoadControllerMEMusage(strControllerMEM ) ) {return false;}

 if (!OS_MEM_Usage() )                                        {return false;}
 
 return true;
}
   

string RemoveXMLnamespace(string strInput) {
 size_t found = strInput.find(":");

 if (found == string::npos)                {return strInput;}
 else if ((found +1) == strInput.length()) {return "";} 
 else                                      {return strInput.substr(found+1);}
}

string RemoveLeadingSpaces(string strInput)
{
 size_t startpos = strInput.find_first_not_of(" \t"); 
 if( string::npos != startpos ){return strInput.substr( startpos );}
 else                          {return strInput;} 
}

string RemoveTrailingSpaces(string strInput)
{
 size_t endpos = strInput.find_last_not_of(" \t");

 if( string::npos != endpos ){ return strInput.substr( 0, endpos+1 );}
 else                        { return strInput;}

}

string RemoveAllSpaces(string strInput)
{
 string strOutput = strInput;
 strOutput.erase (remove (strOutput.begin(), strOutput.end(), ' '), strOutput.end());
 strOutput.erase (remove (strOutput.begin(), strOutput.end(), '\t'), strOutput.end());
 return strOutput;
}


/****************************************************************************************************
*
* Name:
*  Function: string int2str(unsigned long long int intArg)
*
*
* Description:
*   A general purpose utility function  
*
*
* Details:
*   This function converts the parameter intArg to a string. 
*    No leading zero is added.
*          
*
* Parameters: 				
*   intArg				int     - integer
*
* Variables:
*   oss					Local   - <sstream> ostringstream
*                                                                          
* Functions:
*   .str()				Library - <sstream>                     (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string int2str(unsigned long long int intArg)
{
 ostringstream 	oss;
 
 oss << intArg;
 return  oss.str();
}//end int2str()
/****************************************************************************************************
*
* Name:
*  Function: string Create_INI_Key()
*
*
* Description:
*  Utility Function used in Function  Process_CFG_File()
*
*
* Details:
*   this function creates an INI key in the format NAMEX_NAMEX_NAMEX_NAMEX
*   where NAME shall be character only and X is numeric. Parameters 2-4 are
*   optional.
*
*
* Parameters:
*   intArg1                             int
*   intArg2                             int
*   intArg3                             int
*   intArg4                             int 				
*   stringArg1                          <cstring>   
*   stringArg2                          <cstring>   
*   stringArg3                          <cstring>   
*   stringArg4                          <cstring>   
*
* Variables:
*   stringOutput                        Local   <cstring>
*                                                                      
* Functions:
*   .append()                           Library <cstring>
*
* Author(s):
*   Bob McCarthy 			Original: 5/29/2007
*                                       Updated : N/A
*
****************************************************************************************************/ 
string Create_INI_Key(string stringArg1, int intArg1, string stringArg2, int intArg2, string stringArg3, int intArg3, string stringArg4, int intArg4)
{
 string stringOutput;

 stringOutput = stringArg1;
 if (intArg1)                   {stringOutput.append(int2str(intArg1));}
 if (stringArg2.length())       {stringOutput.append("_"); stringOutput.append(stringArg2);}
 if (intArg2)                   {stringOutput.append(int2str(intArg2));}
 if (stringArg3.length())       {stringOutput.append("_"); stringOutput.append(stringArg3);}
 if (intArg3)                   {stringOutput.append(int2str(intArg3));}
 if (stringArg4.length())       {stringOutput.append("_"); stringOutput.append(stringArg4);}
 if (intArg4)                   {stringOutput.append(int2str(intArg4));}

 return stringOutput;

}
/****************************************************************************************************
*
* Name:
*  Function: bool char2bool()
*
*
* Description:
*  function used during .CFG configuration
*
*
* Details:
*   this function takes an input string and returns the boolean value
*   if the value is non boolean an error is generated and program execution
*   is terminated
*
*
* Parameters:
*   enumArg                             <header.h><threadorPorttype>				
*   stringArg                           <cstring>
*  
* Variables:
*   CFG_MESSAGE_028                     Global  - <defines.h> Non Boolean encountered in Config file error message 
*                                                              
* Functions:
*   Console_Message()                   Local                           (void) 
*   Thread_Calling()                    Local                           (string)
*   exit()                              Library - <stdlib.h>            (void)                 
*
* AUTHOR: 10/06/2017 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2017 WestTel Corporation 
****************************************************************************************************/ 
bool char2bool(bool &boolARG, string stringArg)
{
 string strMessage = "char2bool() -> Invalid Argument: ";
 strMessage += stringArg;

 if             (stringArg == "true")  {boolARG = true;}
 else if        (stringArg == "True")  {boolARG = true;}
 else if        (stringArg == "TRUE")  {boolARG = true;}
 else if        (stringArg == "1")     {boolARG = true;}
 else if        (stringArg == "false") {boolARG = false;}
 else if        (stringArg == "False") {boolARG = false;}
 else if        (stringArg == "FALSE") {boolARG = false;}
 else if        (stringArg == "0")     {boolARG = false;}

 //  error in config routine.. exit
 else   {SendCodingError(strMessage); return false;}

 return true;
}// end char2bool(string stringArg)

/****************************************************************************************************
*
* Name:
*  Function: void enQueue_Message()
*
*
* Description:
*   A Utility function 
*
*
* Details:
*   This function places a MessageClass object onto the thread's message queue. It utilizes a mutex semaphore
*   to prevent simultanious operations on the queue.
*          
*    
* Parameters:
*   enumThread                                  - threadorPorttype enumeration representing the threads				
*   objArg1					- MessageClass object
*  
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> enumeration member
*   ANI                                 Global  - <header.h> <threadorPorttype> enumeration member
*   BAD_THREAD                          Global  - <header.h> <threadorPorttype> enumeration member (should never be used)
*   CAD                                 Global  - <header.h> <threadorPorttype> enumeration member
*   intRC                               Local   - return code
*   MAIN                                Global  - <header.h> <threadorPorttype> enumeration member
*   queue_objALiMessageQ                Global  - <queue> <MessageClass> The ALI message Q
*   queue_objANIMessageQ                Global  - <queue> <MessageClass> The ANI message Q
*   queue_objCadMessageQ		Global  - <queue> <MessageClass> The CAD message Q
*   queue_objLogMessageQ                Global  - <queue> <MessageClass> The LOG message Q
*   queue_objMainMessageQ               Global  - <queue> <MessageClass> The Main message Q
*   queue_objWRKMessageQ                Global  - <queue> <MessageClass> The WRK message Q
*   queue_objSYCMessageQ                Global  - <queue> <MessageClass> The SYC message Q
*   sem_tMutexCadMessageQ		Global	- <semaphore.h> Mutex for Cad message queue
*   sem_tMutexLogMessageQ               Global	- <semaphore.h> Mutex for Log message queue
*   SYC                                 Global  - <header.h> <threadorPorttype> enumeration member
*   WRK                                 Global  - <header.h> <threadorPorttype> enumeration member
*                                                                         
* Functions:
*   .push()				Library	- <queue>                       (void)
*   Semaphore_Error()                   Global  - <globalfunctions.h>           (void)         
*   sem_wait()				Library - <semaphore.h>                 (int)
*   sem_post()				Library - <semaphore.h>                 (int)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void enQueue_Message(threadorPorttype enumThread,MessageClass objArg1 )
{
 int                                            intRC;

 extern queue <MessageClass>	                queue_objMainMessageQ;
 extern queue <MessageClass>	                queue_objLogMessageQ;
 extern queue <MessageClass>	                queue_objCadMessageQ;
 extern queue <MessageClass>	                queue_objALiMessageQ;
 extern queue <MessageClass>	                queue_objANIMessageQ;
 extern queue <MessageClass>	                queue_objWRKMessageQ;
 extern queue <MessageClass>	                queue_objRCCMessageQ;
 extern queue <MessageClass>                    queue_objAMIMessageQ;
 extern queue <MessageClass>                    queue_objCTIMessageQ;
 extern queue <MessageClass>                    queue_objLISMessageQ;
 extern queue <MessageClass>                    queue_objDSBMessageQ;

 switch(enumThread)
  {
   case MAIN:
    intRC = sem_wait(&sem_tMutexMainMessageQ);
    if (intRC){Semaphore_Error(intRC, MAIN, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in enQueue_Message()", 1);}
    queue_objMainMessageQ.push(objArg1);
    sem_post(&sem_tMutexMainMessageQ);
    break;

   case CAD:
    intRC = sem_wait(&sem_tMutexCadMessageQ);
    if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadMessageQ, "sem_wait@sem_tMutexCadMessageQ in enQueue_Message()", 1);}
    queue_objCadMessageQ.push(objArg1);
    sem_post(&sem_tMutexCadMessageQ);
    break;

   case ALI:
    intRC = sem_wait(&sem_tMutexAliMessageQ);
    if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliMessageQ, "sem_wait@sem_tMutexAliMessageQ in enQueue_Message()", 1);}
    queue_objALiMessageQ.push(objArg1);
    sem_post(&sem_tMutexAliMessageQ);
    break;

   case ANI:
    intRC = sem_wait(&sem_tMutexANIMessageQ);
    if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIMessageQ, "sem_wait@sem_tMutexANIMessageQ in enQueue_Message()", 1);}
    queue_objANIMessageQ.push(objArg1);
    sem_post(&sem_tMutexANIMessageQ);
    break;

   case WRK:
    intRC = sem_wait(&sem_tMutexWRKMessageQ);
    if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKMessageQ, "sem_wait@sem_tMutexWRKMessageQ in enQueue_Message()", 1);}
    queue_objWRKMessageQ.push(objArg1);
    sem_post(&sem_tMutexWRKMessageQ);
    break;
   case RCC:
    intRC = sem_wait(&sem_tMutexRCCMessageQ);
    if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCMessageQ, "sem_wait@sem_tMutexRCCMessageQ in enQueue_Message()", 1);}
    queue_objRCCMessageQ.push(objArg1);
    sem_post(&sem_tMutexRCCMessageQ);
    break;
   case LOG:
    intRC = sem_wait(&sem_tMutexLogMessageQ);
    if (intRC){Semaphore_Error(intRC, LOG, &sem_tMutexLogMessageQ, "sem_wait@sem_tMutexLogMessageQ in enQueue_Message()", 1);}
    queue_objLogMessageQ.push(objArg1);
    sem_post(&sem_tMutexLogMessageQ);    
    break;
   case CTI:
    intRC = sem_wait(&sem_tMutexCTIMessageQ);
    if (intRC){Semaphore_Error(intRC, CTI, &sem_tMutexCTIMessageQ, "sem_wait@sem_tMutexCTIMessageQ in enQueue_Message()", 1);}
    queue_objCTIMessageQ.push(objArg1);
    sem_post(&sem_tMutexCTIMessageQ);    
    break;
   case AMI:
    intRC = sem_wait(&sem_tMutexCLIMessageQ);
    if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexCLIMessageQ, "sem_wait@sem_tMutexCLIMessageQ in enQueue_Message()", 1);}
    queue_objAMIMessageQ.push(objArg1);
    sem_post(&sem_tMutexCLIMessageQ);    
    break;
   case LIS:
    intRC = sem_wait(&sem_tMutexLISMessageQ);
    if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISMessageQ, "sem_wait@sem_tMutexLISMessageQ in enQueue_Message()", 1);}
    queue_objLISMessageQ.push(objArg1);
    sem_post(&sem_tMutexLISMessageQ);    
    break;
   case DSB:
    intRC = sem_wait(&sem_tMutexDSBMessageQ);
    if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexDSBMessageQ, "sem_wait@sem_tMutexDSBMessageQ in enQueue_Message()", 1);}
    queue_objDSBMessageQ.push(objArg1);
    sem_post(&sem_tMutexDSBMessageQ);    
    break;
   default:
     SendCodingError( "globalfunctions.cpp CODING ERROR in switch(enumThread) in function enQueue_Message() -> " + int2str(objArg1.intLogCode));
     SendCodingError( "globalfunctions.cpp CODING ERROR in switch(enumThread) in function enQueue_Message() -> " + int2str(enumThread));
     SendCodingError( "globalfunctions.cpp CODING ERROR in switch(enumThread) in function enQueue_Message() -> " + objArg1.stringMessageText);
   }// end switch(enumThread)


}// end enQueue_Message()

/****************************************************************************************************
*
* Name:
*  Function: enqueue_Main_Input(threadorPorttype enumThdCallng, ExperientDataClass objData, sem_t *sem_tSemptr)
*
*
* Description:
*   A Utility function 
*
*
* Details:
*   This function places a ExperientDataClass object onto the Main thread's Input Queue. It utilizes a mutex semaphore
*   to prevent simultanious operations on the queue's.
*          
*    
* Parameters:
*   enumThdCallng                               - <header.h> <threadorPorttype> enumeration representing the threads				
*   objData					- <ExperientDataClass> object
*  
* Variables:
*   intRC                               Local   - return code
*   queue_objMainData                   Global  - <queue><ExperientDataClass> objects .. Kernel Input Data Q
*   sem_tMutexCadMessageQ		Global	- <semaphore.h> Mutex for Cad message queue
*   sem_tMutexLogMessageQ               Global	- <semaphore.h> Mutex for Log message queue
*                                                                         
* Functions:
*   .push()				Library	- <queue>                       (void)
*   Semaphore_Error()                   Global  - <globalfunctions.h>           (void)
*   sem_post()				Library - <semaphore.h>                 (int)
*   sem_wait()				Library - <semaphore.h>                 (int)
*   Thread_Calling()                    Local   -                               (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
void enqueue_Main_Input(threadorPorttype enumThdCallng, ExperientDataClass objData)
{
 int intRC;
 extern queue <ExperientDataClass> 	        queue_objMainData;

 objData.enumThreadSending = enumThdCallng;
 // Lock Main Q
 intRC = sem_wait(&sem_tMutexMainInputQ);
 if (intRC){Semaphore_Error(intRC, enumThdCallng, &sem_tMutexMainInputQ, "sem_wait@sem_tMutexMainInputQ in function enqueue_Main_Input() invoked by "
                             +Thread_Calling(enumThdCallng), 1);}
 
 queue_objMainData.push(objData);


 // unlock Main Q
 sem_post(&sem_tMutexMainInputQ);
 //
}

bool ThreadsafeEmptyQueueCheck(queue <ExperientDataClass>* Queue, sem_t* MutexLock ) {
 bool ReturnValue;

  sem_wait(MutexLock);
  ReturnValue = Queue->empty();
  sem_post(MutexLock); 
 
 return ReturnValue;
}

bool ThreadsafeEmptyQueueCheck(deque <ExperientDataClass>* deQueue, sem_t* MutexLock ) {
 bool ReturnValue;

  sem_wait(MutexLock);
  ReturnValue = deQueue->empty();
  sem_post(MutexLock); 
 
 return ReturnValue;
}

bool ThreadsafeEmptyQueueCheck(queue <DataPacketIn>* DataQueue, sem_t* MutexLock ) {
 bool ReturnValue;

  sem_wait(MutexLock);
  ReturnValue = DataQueue->empty();
  sem_post(MutexLock); 
 
 return ReturnValue;
}

bool ThreadsafeEmptyQueueCheck(queue <MessageClass>* MessageQueue, sem_t* MutexLock ) {
 bool ReturnValue;

  sem_wait(MutexLock);
  ReturnValue = MessageQueue->empty();
  sem_post(MutexLock); 
 
 return ReturnValue;
}


/****************************************************************************************************
*
* Name:
*  Function: enqueue_AMI_Input(threadorPorttype enumThdCallng, ExperientDataClass objData)
*
*
* Description:
*   A Utility function 
*
*
* Details:
*   This function places a ExperientDataClass object onto the AMI thread's Input Queue. It utilizes a mutex semaphore
*   to prevent simultanious operations on the queue's.
*          
*    
* Parameters:
*   enumThdCallng                               - <header.h> <threadorPorttype> enumeration representing the threads				
*   objData					- <ExperientDataClass> object
*  
* Variables:
*   intRC                               Local   - return code
*   queue_objMainData                   Global  - <queue><ExperientDataClass> objects .. Kernel Input Data Q
*   sem_tMutexCadMessageQ		Global	- <semaphore.h> Mutex for Cad message queue
*   sem_tMutexLogMessageQ               Global	- <semaphore.h> Mutex for Log message queue
*                                                                         
* Functions:
*   .push()				Library	- <queue>                       (void)
*   Semaphore_Error()                   Global  - <globalfunctions.h>           (void)
*   sem_post()				Library - <semaphore.h>                 (int)
*   sem_wait()				Library - <semaphore.h>                 (int)
*   Thread_Calling()                    Local   -                               (string)
*
*
* AUTHOR: 2/3/2010 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2010 Experient Corporation
****************************************************************************************************/ 
void Queue_AMI_Input(ExperientDataClass objData)
{
 int intRC;
 extern queue <ExperientDataClass> 	        queue_objAMIData;

 // Lock Q
 intRC = sem_wait(&sem_tMutexAMIInputQ);
 if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexAMIInputQ, "sem_wait@sem_tMutexAMIInputQ in function enqueue_AMI_Input()",1);}
 
 queue_objAMIData.push(objData);


 // unlock Main Q
 sem_post(&sem_tMutexAMIInputQ);
 //
}
/****************************************************************************************************
*
* Name:
*  Function: float RoundToThousanth(timespec timespecArg)
*
*
* Description:
*   A general purpose utility function  
*
*
* Details:
*   This function converts nanoseconds to a decimal rounded to the thousandth digit 
*          
*
* Parameters: 				
*   timespecArg			     	- <ctime><timespec> structure holding seconds & nanosecends
*
* Variables:
*   floatNANOSEC_PER_SEC		Global	- number of nanoseconds in one second
*   floatVar				Local	- holds conversion of integer nanoseconds to decimal fraction
*   intPlaces				Local   - holds the places of the digits to be processed
*   intRoundUp				Local	- used to round up to the thousandth position
*   intSigDigit				Local	- digit to check if rounding needed
*                                                                          
* Functions:
*   none
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
float RoundToThousanth(timespec timespecArg)
{
 int		intPlaces;
 int		intSigDigit;
 int		intRoundUp;
 float		floatVar;

 floatVar 	= float ( timespecArg.tv_nsec / floatNANOSEC_PER_SEC );
 intPlaces 	= int (floatVar*10000);
 intSigDigit 	= intPlaces - (int(intPlaces /10) * 10);
 intPlaces 	/= 10;

 if (intSigDigit > 4) 	{intRoundUp = 1;}
 else			{intRoundUp = 0;}
 
 return float (( intPlaces + intRoundUp )/1000.0);
}// end RoundToThousanth



long double Round_ld( long double ldArg, int intDigits)
{
 unsigned long long int		intPlaces;
 int				intSigDigit;
 int				intRoundUp;

 intPlaces 	= (unsigned long long int) (ldArg * pow(10.0,(intDigits+1)) );
 intSigDigit 	= intPlaces - (((unsigned long long int)(intPlaces /10)) * 10);
 intPlaces 	/= 10;

 if (intSigDigit > 4) 	{intRoundUp = 1;}
 else			{intRoundUp = 0;}
 
 return (long double) (( intPlaces + intRoundUp )/(pow(10.0,intDigits)));
}// end Round_ld






/****************************************************************************************************
*
* Name:
*  Function: string DecimalsToString(float floatArg)
*
*
* Description:
*   A general purpose utility function  
*
*
* Details:
*   This function converts a decimal of the format(s) [0.NNN, 0.NN, 0.N, 0., 0] and returns it as a string 
*          
*
* Parameters: 				
*   floatArg				     	- float previously rounded to the thousandth position
*
*
* Variables:
*   ostringstreamVar			Local	- <sstream> stream to capture decimal into string
*   stringOuputstring			Local	- <cstring> output string
*
*                                                                          
* Functions:
*   .erase()				Library	- <cstring>             (*this)
*   .length()				Library	- <csrting>             (size_t)
*   .str()				Library	- <sstream>             (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string DecimalsToString(float floatArg)
{
 ostringstream	ostringstreamVar;
 string		stringOuputstring;

 ostringstreamVar << floatArg;

 // convert to string
 stringOuputstring = ostringstreamVar.str();

 //remove leading zero
 stringOuputstring.erase(0,1);
 
 // append trailing zeroes if required
 switch ( stringOuputstring.length() )
  {
	case 0:
           stringOuputstring += ".000";
	   break;
	case 1:
  	   stringOuputstring += "000";
           break;
	case 2:
           stringOuputstring += "00";
	   break;
	case 3:
	   stringOuputstring +="0";
	   break;
	case 4:
	   break;
        default:
           return ".XXX";
  }
 
 return stringOuputstring;
}// end DecimalsToString()

/****************************************************************************************************
*
* Name:
*  Function: string get_time_stamp(timespec timespecArg)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function returns a string representing a time stamp of the format YYYY MM DD HH:MM:SS UTC | DOW YYYY MM DD HH:MM TMZ|
*          
*    
* Parameters: 				
*   timespecArg				- <ctime> struct holding a timestamp
*
* Variables:
*   time_tTimeStamp			Local   - <ctime> time in sec since epoch
*   tmLocalTimeStamp			Local	- <ctime> <tm> structure to hold Local time/date data
*   tmUniversalTimeStamp                Local	- <ctime> <tm> structure to hold UTC time/date data
*   char_arrayLocalTimeStamp[28]	Local   - char array to hold Local timestamp output
*   char_arrayUniversalTimeStamp[20]    Local   - char array to hold UTC timestamp output
*   .tv_sec                             Global  - <ctime><timespec> structure member
*                                                                          
* Functions:
*   DecimalsToString()			Local	-                               (string)
*   gmtime_r()                          Library - <ctime>                       (struct tm*)  
*   localtime_r()			Library - <ctime>                       (struct tm*)                      
*   RoundToThousanth()			Local	-                               (float)
*   sizeof()                            Library - <iostream>                    (size_t)                              
*   strftime()				Library - <ctime>                       (size_t)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string get_time_stamp(timespec timespecArg)
{
 
 time_t			time_tTimeStamp;
 struct tm		tmLocalTimeStamp;
 struct tm              tmUniversalTimeStamp;
 char                   char_arrayLocalTimeStamp[28];
 char                   char_arrayUniversalTimeStamp[20];

 time_tTimeStamp = timespecArg.tv_sec;
  
 localtime_r(&time_tTimeStamp,&tmLocalTimeStamp);
 gmtime_r(&time_tTimeStamp,&tmUniversalTimeStamp);

 strftime(char_arrayUniversalTimeStamp, sizeof(char_arrayUniversalTimeStamp), "%Y %m %d %H:%M:%S", &tmUniversalTimeStamp);
 strftime(char_arrayLocalTimeStamp, sizeof(char_arrayLocalTimeStamp), "%a %Y/%m/%d %H:%M:%S %Z", &tmLocalTimeStamp);

 char_arrayUniversalTimeStamp[4] = '/';
 char_arrayUniversalTimeStamp[7] = '/';
 
 return char_arrayUniversalTimeStamp + DecimalsToString(RoundToThousanth(timespecArg))+ " UTC | "+char_arrayLocalTimeStamp+" |";
}// end get_time_stamp()

string get_ALI_Record_Local_TimeStamp(timespec timespecArg)
{
 time_t			time_tTimeStamp;
 struct tm		tmLocalTimeStamp;
 char                   char_arrayLocalTimeStamp[28];

 time_tTimeStamp = timespecArg.tv_sec;
 localtime_r(&time_tTimeStamp,&tmLocalTimeStamp);

 strftime(char_arrayLocalTimeStamp, sizeof(char_arrayLocalTimeStamp), "%m/%d %H:%M", &tmLocalTimeStamp);
 
 return char_arrayLocalTimeStamp;
}


string get_ALI_Record_Local_TimeStamp(time_t time_tArg)
{
 char                   char_arrayLocalTimeStamp[28];
 struct tm		tmLocalTimeStamp;

 localtime_r(&time_tArg,&tmLocalTimeStamp);

 strftime(char_arrayLocalTimeStamp, sizeof(char_arrayLocalTimeStamp), "%m/%d %H:%M", &tmLocalTimeStamp);
 
 return char_arrayLocalTimeStamp;
}


int get_current_year()
{
 struct timespec timespecTimeNow;
 struct tm       tmUniversalTimeStamp;
 time_t          time_tTimeNow;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 time_tTimeNow = timespecTimeNow.tv_sec;
 gmtime_r(&time_tTimeNow,&tmUniversalTimeStamp);

 return tmUniversalTimeStamp.tm_year + UNIX_STRUCT_TM_YEAR_OFFSET ;
}


string get_Local_Time_Stamp()
{
 struct timespec timespecTimeNow;
 struct tm       tmLocalTimeStamp;
 time_t          time_tTimeNow;
 string          strHour;
 string          strMinute;
 string          strSeconds;
 //string          strAMPM;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 time_tTimeNow = timespecTimeNow.tv_sec;
 localtime_r(&time_tTimeNow,&tmLocalTimeStamp);
 
/*
 if      (tmLocalTimeStamp.tm_hour == 0)  {strAMPM = "AM"; strHour = "12";}
 else if (tmLocalTimeStamp.tm_hour == 12) {strAMPM = "PM"; strHour = "12";}
 else if (tmLocalTimeStamp.tm_hour < 12)  {strAMPM = "AM"; strHour = int2str(tmLocalTimeStamp.tm_hour);}
 else                                     {strAMPM = "PM"; strHour = int2str(tmLocalTimeStamp.tm_hour - 12);}
*/

 strMinute  =  int2strLZ(tmLocalTimeStamp.tm_min);
 strSeconds =  int2strLZ(tmLocalTimeStamp.tm_sec);
 strHour    =  int2strLZ(tmLocalTimeStamp.tm_hour);

 return strHour + ":" + strMinute + ":" + strSeconds ;
}

 


/****************************************************************************************************
*
* Name:
*  Function: unsigned long long int char2int(const char* CharArg)
*
*
* Description:
*  utility function 
*
*
* Details:
*   this function takes an input string and returns the integer value
*   if the value is non integer an error is generated and program execution
*   is terminated
*
*
* Parameters: 				
*   CharArg                             const char*
*
* Variables:
*   EX_SOFTWARE                         Library <sysexits.h>           Exit Code
*   i                                   Local   integer                counter
*   intOutput                           Local   unsigned long long int output
*   strin                               Local   <sstream> istringstream 
*                                                              
* Functions:
*   isdigit()                           Library <ctype.h>               (bool)
*   strlen()                            Library <cstring>               (size_t)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
unsigned long long int char2int(const char* CharArg)
{
 unsigned long long int            intOutput;
 
 istringstream                     strin(CharArg);
  
 for (unsigned int i = 0; i < strlen(CharArg); i++) {if (!(isdigit(CharArg[i]) )) {Abnormal_Exit(MAIN, EX_SOFTWARE, "*Invalid Integer in Call to Function char2int() Data = %%%", CharArg);} }
 strin >> intOutput; 
 
 return intOutput;

}// end char2int(const char* CharArg)





/****************************************************************************************************
*
* Name:
*  Function: string FindandReplace(string stringArg, string stringFind, string stringReplace)
*
*
* Description:
*  utility function 
*
*
* Details:
*   this function takes an input string and finds the first occurence of stringFind and 
*   substitues it with the string stringReplace and returns the new string.
*   
*
*
* Parameters: 				
*   stringArg                           <cstring>
*   stringFind                          <cstring>
*   stringReplace                       <cstring>
*
* Variables:
*   index                               Local   <cstring> size_t
*   string::npos                        Library <cstring>
*                                                              
* Functions:
*   .erase()                            Library <cstring>               (string& erase )
*   .find()                             Library <cstring>               (size_t find)
*   .insert()                           Library <cstring>               (string& insert)
*   .length()                           Library <cstring>               (size_t)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string FindandReplace(string stringArg, string stringFind, string stringReplace)
{
 size_t                 index;

 index = stringArg.find(stringFind,0);
 if (index  != string::npos ) 
  {
   stringArg.erase(index,stringFind.length());
   stringArg.insert(index,stringReplace);
  }
 return stringArg;

}// end string FindandReplace(string stringArg, string stringFind, string stringReplace)
/****************************************************************************************************
*
* Name:
*  Function: string FindandReplaceALL(string stringArg, string stringFind, string stringReplace)
*
*
* Description:
*  utility function 
*
*
* Details:
*   this function takes an input string and finds the all occurences of stringFind and 
*   substitues them with the string stringReplace and returns the new string.
*   
*
*
* Parameters: 				
*   stringArg                           <cstring>
*   stringFind                          <cstring>
*   stringReplace                       <cstring>
*
* Variables:
*   index                               Local   <cstring> size_t
*   string::npos                        Library <cstring>
*                                                              
* Functions:
*   .erase()                            Library <cstring>               (string& erase )
*   .find()                             Library <cstring>               (size_t find)
*   .insert()                           Library <cstring>               (string& insert)
*   .length()                           Library <cstring>               (size_t)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string FindandReplaceALL(string stringArg, string stringFind, string stringReplace)
{
 size_t                 index;

 index = stringArg.find(stringFind,0);
 while  (index  != string::npos ) 
  {
   stringArg.erase(index,stringFind.length());
   stringArg.insert(index,stringReplace);
   index = stringArg.find(stringFind,index+stringReplace.length());
   
  }
 return stringArg;

}// end string FindandReplace(string stringArg, string stringFind, string stringReplace)

/****************************************************************************************************
*
* Name:
*  Function: Create_Message(string stringArg, string stringArg1, string stringArg2, string stringArg3,string stringArg4,string stringArg5)
*
*
*
* Description:
*   This function creates a message.
*
*
* Details:
*   stringArg contains the the base message and the subsequents arguments get inserted into the string at each subsequent occurrence of %%%.
*          
*    
* Parameters: 				
*   stringArg   			string  - the base message
*   stringArg1   			string  -
*   stringArg2   			string  - 
*   stringArg3  			string  -
*   stringArg4   			string  - 
*   stringArg5  			string  -
*   stringArg6  			string  -
*
* Variables:
*                                                                          
* Functions:
*   FindandReplace()                    Local   -                               (string)
*
*
* AUTHOR: 4/5/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/ 
string Create_Message(string stringArg, string stringArg1, string stringArg2, string stringArg3,string stringArg4,string stringArg5, string stringArg6)
{
 string                 stringMessageText;

 stringMessageText = stringArg;
 stringMessageText = FindandReplace(stringMessageText, "%%%", stringArg1);
 stringMessageText = FindandReplace(stringMessageText, "%%%", stringArg2); 
 stringMessageText = FindandReplace(stringMessageText, "%%%", stringArg3);
 stringMessageText = FindandReplace(stringMessageText, "%%%", stringArg4);
 stringMessageText = FindandReplace(stringMessageText, "%%%", stringArg5);
 stringMessageText = FindandReplace(stringMessageText, "%%%", stringArg6);
return stringMessageText;
  
}// end string Create_Message(string stringArg, string stringArg1, string stringArg2, string stringArg3,string stringArg4,string stringArg5)
/****************************************************************************************************
*
* Name:
*  Function: void Console_Message(string stringThreadCalling, string stringArg, string stringArg1, string stringArg2, 
*                                 string stringArg3,string stringArg4,string stringArg5)
*
*
* Description:
*   This function generates a message from a thread to the console 
*
*
* Details:
*   This function generates a timestamp and sends the parameters to the console
*   for output.  Used for startup before semaphores are initialized and during shut down
*   when message queues are unavailable . stringArg contains the the base message and the
*   subsequents arguments get inserted into the string at each subsequent occurrence of %%%.
*          
*    
* Parameters: 				
*   stringThreadCalling			string  - string representing the calling thread
*   stringArg   			string  - 
*   stringArg1   			string  -
*   stringArg2   			string  - 
*   stringArg3  			string  -
*   stringArg4   			string  - 
*   stringArg5  			string  -
*
*
* Variables:
*   CLOCK_REALTIME			Library	- <ctime>
*   stringSpaces                        Local   - <cstring>
*   timespecTimeNow			Local   - <ctime> current time
*                                                                          
* Functions:
*   .assign()                           Library - <cstring>                     (string& assign)
*   clock_gettime			Library	- <ctime>                       (int)
*   Create_Message()                    Local   -                               (string)
*   get_time_stamp()			Local   -                               (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
void Console_Message(string stringThreadCalling, string stringArg, string stringArg1, string stringArg2, string stringArg3,string stringArg4,string stringArg5)
{
 struct timespec	timespecTimeNow;
 string                 stringMessageText;
 string                 string18Spaces;
 string                 string3Spaces;
 string                 string10Spaces;
 string                 string7Spaces;
 string                 stringMessageBase;
 string                 stringArrowField;
 string                 stringMilliseconds;
 string                 stringPrefix;

 if (stringArg.length() < 2) {SendCodingError("globalfunctions.cpp - Coding error in fn Console_Message()"); return;}
 stringMessageBase.assign(stringArg, 1, string::npos);
 stringArrowField.assign (stringArg, 0, 1);
 string18Spaces.assign(18, ' ');
 string3Spaces.assign(3, ' ');
 string10Spaces.assign(10, ' ');
 string7Spaces.assign(7, ' ');

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 stringMessageText = Create_Message(stringMessageBase, stringArg1, stringArg2, stringArg3, stringArg4, stringArg5);
 stringPrefix      = get_time_stamp(timespecTimeNow);
 stringMilliseconds.assign(stringPrefix,19,4);
 stringPrefix.erase(0,30);
 stringPrefix.insert ( 23, stringMilliseconds ); 
 cout << stringPrefix + " " + stringThreadCalling +" "+ string18Spaces + " | " + string3Spaces + " | " +
 string3Spaces + " | " + string10Spaces + " | "+ string10Spaces + "  | " + string7Spaces + " | " + stringArrowField +" | " + stringMessageText << endl;
  
}// end Console_Message()



/****************************************************************************************************
*
* Name:
*  Function: void Set_Timer_Delay(struct itimerspec *itimerspecArg, int intArg2, int intArg3, int intArg4, int intArg5)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function loads the parameters into a struct itimerspec
*          
*    
* Parameters: 				
*   *itimerspecArg				- <ctime> struct for a timer
*   intArg2					- delay seconds 
*   intArg3					- delay nanoseconds
*   intArg4					- delay interval seconds
*   intArg5					- delay interval nanoseconds
*
* Variables:
*   it_interval                                 Global - <ctime> <itimerspec> member
*   it_value                                    Global - <ctime> <itimerspec> member
*   .tv_nsec                                    Global - <ctime> <itimerspec> member
*   .tv_sec                                     Global - <ctime> <itimerspec> member
*                                                                          
* Functions:
*   none 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
void Set_Timer_Delay(struct itimerspec *itimerspecArg, int intArg2, int intArg3, int intArg4, int intArg5)
{
 itimerspecArg->it_value.tv_sec     = intArg2;			// Delay for n sec
 itimerspecArg->it_value.tv_nsec    = intArg3;			//           n nsec
 itimerspecArg->it_interval.tv_sec  = intArg4;			// if both interval 0 run once
 itimerspecArg->it_interval.tv_nsec = intArg5;
}// end	Set_Timer_Delay 

/****************************************************************************************************
*
* Name:
*  Function: string int2strLZ(unsigned long long int intArg)
*
*
* Description:
*   A general purpose utility function  
*
*
* Details:
*   This function converts the parameter intArg to a string. If the integer is 0 to 9 a 
*   leading zero is added.
*          
*
* Parameters: 				
*   intArg				int     - integer
*
* Variables:
*   oss					Local   - <sstream> ostringstream
*                                                                          
* Functions:
*   .str()				Library - <sstream>                     (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string int2strLZ(unsigned long long int intArg)
{
 ostringstream 	oss;
      
 if ((intArg > 9)||(intArg < 0)) 
  {
   oss << intArg;
   return oss.str();
  }
 oss << "0";
 oss << intArg;
 return  oss.str();
}//end int2strLZ()

/****************************************************************************************************
*
* Name:
*  Function: string int2strTwoLZ(unsigned long long int intArg)
*
*
* Description:
*   A general purpose utility function  
*
*
* Details:
*   This function converts the parameter intArg to a string of the format XXX. If the integer is 0 to 9
*   two leading zeros are added, if the integer is 10 to 99 one leading zero is added.
*          
*
* Parameters: 				
*   intArg				int     - integer
*
* Variables:
*   oss					Local   - <sstream> ostringstream
*                                                                          
* Functions:
*   .str()				Library - <sstream>                     (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
string int2strTwoLZ(unsigned long long int intArg)
{
 ostringstream 	oss;
      
 if (intArg > 99) 
  {
   oss << intArg;
   return oss.str();
  }
 oss << "0";
 if (intArg > 9) 
  {
   oss << intArg;
   return oss.str();
  }
 oss << "0";
 oss << intArg;
 return  oss.str();
}//end int2strTwoLZ()



/****************************************************************************************************
*
* Name:
*  Function: long double time_difference(timespec timespecArg1, timespec timespecArg2)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This returns the absolute time difference of two timestamps stored in a <ctime> struct timespec
*   as a long double
*         
*    
* Parameters: 				
*   timespecArg1				- <ctime> structure
*   timespecArg2				- <ctime> structure 
*
* Variables:
*   longdoubleDifference		Local	- Output
*   longdoubleNANOSEC_PER_SEC		Local	- constant
*   longdoubleTime1			Local
*   longdoubleTime2			Local
*                                                                          
* Functions:
*   none 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
long double time_difference(timespec timespecArg1, timespec timespecArg2)
{
 const long double  	longdoubleNANOSEC_PER_SEC  = 1000000000.0;
 long double 		longdoubleTime1;
 long double 		longdoubleTime2;
 long double    	longdoubleDifference;

  longdoubleTime1 	=  (long double) (timespecArg1.tv_sec) +  ((long double) (timespecArg1.tv_nsec)/longdoubleNANOSEC_PER_SEC);
  longdoubleTime2  	=  (long double) (timespecArg2.tv_sec) +  ((long double) (timespecArg2.tv_nsec)/longdoubleNANOSEC_PER_SEC);
  
  longdoubleDifference = longdoubleTime1 - longdoubleTime2;
  if (longdoubleDifference < 0.0 ){longdoubleDifference *= -1.0;}
 
  return longdoubleDifference;
}

/****************************************************************************************************
*
* Name:
*  Function: bool TimeCheckBefore(timespec timespecArg1, timespec timespecArg2)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   The function returns true if the <ctime> struct timespec of argument 1 is less than argument 2
*          
*    
* Parameters: 				
*   timespecArg1				- <ctime> structure
*   timespecArg2				- <ctime> structure 
*
* Variables:
*   longdoubleNANOSEC_PER_SEC		Local	- constant
*   longdoubleTime1			Local
*   longdoubleTime2			Local
*                                                                          
* Functions:
*   none 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool TimeCheckBefore(timespec timespecArg1, timespec timespecArg2)
{
 const long double  	longdoubleNANOSEC_PER_SEC  = 1000000000.0;
 long double 		longdoubleTime1;
 long double 		longdoubleTime2;

 longdoubleTime1 	=  (long double) (timespecArg1.tv_sec) +  ((long double) (timespecArg1.tv_nsec)/longdoubleNANOSEC_PER_SEC);
 longdoubleTime2  	=  (long double) (timespecArg2.tv_sec) +  ((long double) (timespecArg2.tv_nsec)/longdoubleNANOSEC_PER_SEC);

 if (longdoubleTime1 < longdoubleTime2){return true;}
 else                                  {return false;}

}// end bool TimeCheckBefore(timespec timespecArg1, timespec timespecArg2)

/****************************************************************************************************
*
* Name:
*  Function: char CheckSumGenerate( string stringText, int intLength)
*
*
* Description:
*   A Utility Function for ALI checksums
*
*
* Details:
*   This function returns a character representing a check sum from a string of numbers with a length
*   of 12 or 14.  The sum generated will a number such that when added to total sum , it will be divisible
*   by the number 8. The number will be returned as an ascii character. This complies with the NENA spec 3.3.1.4
*          
*    
* Parameters: 				
*   stringText					- <cstring> 
*   intLength					- int 
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> enumeration member
*   ALI_MESSAGE_238                     Global  - <defines.h> Error creating checksum output message
*   i					Local	- counter
*   intModSum				Local	- running sum
*   LOG_WARNING                         Global  - <defines.h> log code
*   objMessage                          Local   - <MessageClass> object
*                                                                          
* Functions:
*   enQueue_Message()                   Local   -                               (void)
*   .fMessage_Create                    Local   - <MessageClass>                (void)
*   int2strLZ()                         Local   -                               (string)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
char CheckSumGenerate( string stringText, int intLength)
{
 // NENA Standard: the checksum is a single digit such that when it is added to the sum of the previous
 // digits (ie 12 or 14) the sum is divisible by 8.
 // the length of the ALI request is either 14 or 16 characters the last 2 are the check and <cr> 
 // 

 MessageClass   objMessage;
 int 	        intModSum = 0;

 if ((intLength != 12) && (intLength != 14))  {
   
   objMessage.fMessage_Create(LOG_WARNING,238, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT, 
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_238, int2strLZ(intLength));
   enQueue_Message(ALI,objMessage);
   return 'X';

 } 

 for (int i = 0; i < intLength ; i++) { intModSum +=  (int) stringText[i]; }

 if (intModSum == 0) 	{return  '0';}
 else 			{return  (char)(8 - (intModSum%8)+48);}
 
}

/****************************************************************************************************
*
* Name:
*  Function: char BlockCheckCharacter( string stringText, int intLength)
*
*
* Description:
*   A Utility Function for CAD block check characters
*
*
* Details:
*   This function returns a character representing a running XOR from a character string excluding the first character.
*   This complies with the NENA spec 3.4.6
*          
*    
* Parameters: 				
*   char_arrayText				- char * 
*   intLength					- int 
*
* Variables:
*   charOutput				Local	- char output
*   i					Local	- int counter
*
*                                                                          
* Functions:
*   none 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
char BlockCheckCharacter( string stringText, int intLength)
{
 // NENA standard: STX char posn 0 is not included in BCC check
 char	charOutput = stringText[1];
 for ( int i = 2; i <= intLength-1; i++)  { charOutput = (charOutput xor stringText[i]); }
 return charOutput;
}// end BlockCheckCharacter

/****************************************************************************************************
*
* Name:
*  Function: string Thread_Calling(threadorPorttype enumThreadCalling)
*
*
* Description:
*   A Utility Function
*
*
* Details:
*   This function returns a string representation of inputted parameter of the type
*   threadorPorttype  
*          
*    
* Parameters: 				
*   enumThreadCalling				- threadorPorttype 
*
* Variables:
*   ALI                                         Global - <header.h><threadorPorttype> enumeration member
*   ANI                                         Global - <header.h><threadorPorttype> enumeration member
*   CAD                                         Global - <header.h><threadorPorttype> enumeration member
*   LOG                                         Global - <header.h><threadorPorttype> enumeration member
*   WRK                                         Global - <header.h><threadorPorttype> enumeration member				
*   AMI                                         Global - <header.h><threadorPorttype> enumeration member
*   RCC                                         Global - <header.h><threadorPorttype> enumeration member
*   CTI                                         Global - <header.h><threadorPorttype> enumeration member
*                                                                          
* Functions:
*   none 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
string Thread_Calling(threadorPorttype enumThreadCalling)
{
 switch (enumThreadCalling)
  {
   case MAIN:
    return "KRN |";
   case CAD:
    return "CAD |";
   case ALI:
    return "ALI |";
   case ANI:
    return "ANI |";
   case WRK:
    return "WRK |";
   case LOG:
    return "LOG |";
   case SYC:
    return "SYC |";
   case AMI:
    return "AMI |";
   case RCC:
    return "RCC |";
   case CTI:
    return "CTI |";
   case LIS:
    return "LIS |";
   case NFY:
    return "NFY |";
   case DSB:
    return "DSB |";
   default:
    return "    |";
  }//end switch

}// end Thread_Calling()

/****************************************************************************************************
*
* Name:
*  Function: int Transmit_Data(threadorPorttype enumPortType, int intPortNum, char* charTextMessage, int intTextLength, int intALI_AorB)
*
*
* Description:
*  This is a universal function for transmitting data for the program.
*
*
* Details:
*  The function transmits a text message of x length on the specified port utilizing the
*  defined protocol. if the library function returns an error this function returns it.
*          
*    
* Parameters:
*   charTextMessage                             - char*				
*   enumPortType				- threadorPorttype
*   intLength					- int
*   intPortNum                                  - int
*
* Variables:
*   ALI                                 Global  - <header.h><threadorPorttype> enumeration member
*   ALIPort[][]                         Global	- <ExperientCommPort> type of port for ALI
*   ANI                                 Global  - <header.h><threadorPorttype> enumeration member
*   CAD                                 Global  - <header.h><threadorPorttype> enumeration member
*   Cad_UDP_Port[]			Global	- <ExperientCommPort> type of port for CAD
*   intRC				Local	- integer return code for functions
*   objMessage                          Local   - <MessageClass> object
*   WRKThreadPort                       Global	- <ExperientCommPort> object
*   .UDP_Port                           Global	- <ExperientCommPort><ExperientUDPPort> object
*                                                                          
* Functions: 
*   .Send()                             Library - <ExperientUDPPort>                    (int)
*   .Error_Handler()                    Global  - <ExperientCommPort>                   (void)
*  
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
int Transmit_Data(threadorPorttype enumPortType, int intPortNum, const char* charTextMessage, int intTextLength, int intALI_AorB)
{
 int                      intRC;
 MessageClass             objMessage;
 int			  intSendLogCode;
 string			  strPort;
 string			  strAddress;
 string			  strSideAorB;
 Port_Data                objPortData;
 bool                     boolShowRawData = false;
 string                   strInput =   charTextMessage;

 extern ExperientCommPort  CADPort[NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort  ANIPort[NUM_ANI_PORTS_MAX+1];
 extern ExperientCommPort  ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort  ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
 extern RCC_Device         RCCdevice[NUM_WRK_STATIONS_MAX+1];
 extern ExperientTCPPort   DashboardPort;
// extern ExperientCommPort  ServerMasterSlaveSyncPort;

 objPortData.fLoadPortData(enumPortType, intPortNum, intALI_AorB); 
 
 if (!intPortNum)     {return -1;} 
 if (!intTextLength)  {return -1;}

 
 switch(enumPortType)
  {
   case ANI:
        intRC = ANIPort[intPortNum].UDP_Port.Send(charTextMessage,intTextLength );
        if (intRC){ANIPort[intPortNum].UDP_Port.Error_Handler(); return intRC;}
        strAddress = ANIPort[intPortNum].RemoteIPAddress.stringAddress + ":"+int2str(ANIPort[intPortNum].intRemotePortNumber);
        intSendLogCode = 359;
        if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC)){boolShowRawData = true;}
        break;   
   case CAD:
        switch (CADPort[intPortNum].ePortConnectionType)  {
          case UDP:
        //     cout << "transmitting on CAD Port " << intPortNum << endl;
               intRC = CADPort[intPortNum].UDP_Port.Send(charTextMessage,intTextLength );
               if (intRC){CADPort[intPortNum].UDP_Port.Error_Handler(); return intRC;} 
               strAddress = CADPort[intPortNum].RemoteIPAddress.stringAddress + ":" +int2str(CADPort[intPortNum].intRemotePortNumber);
               intSendLogCode = 475;
               if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC)){boolShowRawData = true;}                                          
               break;
         case TCP:
               if (!CADPort[intPortNum].TCP_Server_Port.GetConnectionCount()) {return -1;}
               intRC = CADPort[intPortNum].TCP_Server_Port.Send( CADPort[intPortNum].TCP_Server_Port.LastConnectionId, charTextMessage,intTextLength );
               intSendLogCode = 475;
               if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC)){boolShowRawData = true;}                                          
               break;
         default: 
               SendCodingError("globalfunctions.cpp Transmit_Data() -NO_CONNECTION_TYPE_DEFINED -1");
               return -1;
        }
        break;
   case ALI:
        switch (intALI_AorB ) 
         {
          case 1: case 2:
                 switch (ALIPort[intPortNum][intALI_AorB].ePortConnectionType)
                  {
                   case UDP:
                        if (ALIPort[intPortNum ] [ intALI_AorB ].boolPhantomPort) {break;}
                        intRC = ALIPort[intPortNum ] [ intALI_AorB ].UDP_Port.Send(charTextMessage,intTextLength );
                        if (intRC){ALIPort[intPortNum ] [ intALI_AorB ].UDP_Port.Error_Handler(); return intRC;}
                        if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC)){boolShowRawData = true;}
                        strAddress = ALIPort[intPortNum][intALI_AorB].RemoteIPAddress.stringAddress + ":" +int2str(ALIPort[intPortNum][intALI_AorB].intRemotePortNumber);
                        intSendLogCode = 253;
                        break;
                   case TCP:
                        if (ALIPort[intPortNum ] [ intALI_AorB ].boolPhantomPort) {break;}
                        if (!ALIPort[intPortNum ] [ intALI_AorB ].TCP_Port.boolReadyToSend) {return -1;}
                        intRC = ALIPort[intPortNum ] [ intALI_AorB ].TCP_Port.Send(charTextMessage,intTextLength );
                        if (intRC) {return intRC;}
                        if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC))
                         {
                          switch (ALIPort[intPortNum][intALI_AorB].eALIportProtocol)
                           {
                            case ALI_E2:
                                 strAddress = ALIPort[intPortNum ] [ intALI_AorB ].TCP_Port.Remote_IP.stringAddress + ":" + int2str(ALIPort[intPortNum][intALI_AorB].TCP_Port.intRemotePortNumber);
                                 objMessage.fMessage_Create(LOG_CONSOLE_FILE, 253, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                                                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_531, strAddress, 
                                                            HEX_String((unsigned char *)charTextMessage, intTextLength),"","","","",  DEBUG_MSG,NEXT_LINE);
                                 enQueue_Message(enumPortType,objMessage);
                                 break;
                            default:
                                 strAddress = ALIPort[intPortNum][intALI_AorB].RemoteIPAddress.stringAddress + ":" +int2str(ALIPort[intPortNum][intALI_AorB].intRemotePortNumber);
                                 intSendLogCode = 253;
                                 boolShowRawData = true;
                                 break;
                           } // switch (ALIPort[intPortNum][intALI_AorB].eALIportProtocol) 

                         }// if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC)) 
                        break;
                   default:
                        SendCodingError( "Coding error in  Transmit_Data() switch ALIPort[intPortNum][intALI_AorB].ePortConnectionType)");
                  }//  switch (ALIPort[intPortNum][intALI_AorB].ePortConnectionType)
                 break;
          case 3:
                 switch (ALIServicePort[intPortNum].ePortConnectionType)  {
                   case UDP:
                        SendCodingError( "Coding error in  Transmit_Data() switch (ALIServicePort[intPortNum].ePortConnectionType) UDP not coded ");
                        break;
                   case TCP:
                        if (!ALIServicePort[intPortNum ].TCP_Port.boolReadyToSend) {return -1;}
                        intRC = ALIServicePort[intPortNum ].TCP_Port.Send(charTextMessage,intTextLength );
                        if (intRC) {return intRC;}
                        if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC))  {
                          strAddress = ALIServicePort[intPortNum].RemoteIPAddress.stringAddress + ":" +int2str(ALIServicePort[intPortNum].intRemotePortNumber);
                          intSendLogCode = 253;
                          boolShowRawData = true;
                        }
                   default: 
                         SendCodingError("globalfunctions.cpp Transmit_Data() -NO_CONNECTION_TYPE_DEFINED -2");
                         return -1;
                 }
                 break;                  

          default:
              SendCodingError( "Coding error in  Transmit_Data() switch (intALI_AorB ) ");
         } //switch (intALI_AorB ) 
        
        break;
   case WRK:
       //  intRC = WRKThreadPort.UDP_Port.Send(charTextMessage,intTextLength );
       //  if (intRC){WRKThreadPort.UDP_Port.Error_Handler(); return intRC;}
       //  strAddress = WRKThreadPort.UDP_Port.GetRemoteHost();
       //  strAddress += ":";
       //  strAddress += int2str(WRKThreadPort.UDP_Port.GetRemotePort());
       //  intSendLogCode = 731;
       //  if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC)){boolShowRawData = true;}
       //  break;
        SendCodingError( "globalfunctions.cpp - Transmit_DataIn WRK Transmit no longer used");
        return 0;
   case RCC:
        if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC)){
          boolShowRawData = true;
          intSendLogCode = 913;
        }
        strAddress = RCCdevice[intPortNum].RelayContactClosurePort.Remote_IP.stringAddress;
        strAddress += ":";
        strAddress += int2str(RCCdevice[intPortNum].RelayContactClosurePort.GetRemotePort());

       if      (strInput == "OPEN")  {strInput = strRCC_DEVICE_ALL_OPEN;}
       else if (strInput == "CLOSE") {strInput = strRCC_DEVICE_ALL_CLOSE;}
       else                          {strInput = strRCC_DEVICE_GET_VERSION;}
       if (!RCCdevice[intPortNum].RelayContactClosurePort.GetConnected()) {return 0;}
       intRC = RCCdevice[intPortNum].RelayContactClosurePort.SendLine((char*)strInput.c_str());
       strInput += charCR;
       if (intRC) { 
        strAddress += " Code=";
        strAddress += int2str(intRC);
        objMessage.fMessage_Create(LOG_CONSOLE_FILE, 917, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                                   objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,RCC_MESSAGE_917, strAddress,
                                   ASCII_String(strInput.c_str(), strInput.length()),"","","","",  DEBUG_MSG, NEXT_LINE);
        enQueue_Message(enumPortType,objMessage);
        return intRC;
       }
       break;
   case CTI:
        SendCodingError( "globalfunctions.cpp - Transmit_DataIn CTI Transmit not used");
        break;
   case DSB:
        if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC)){
          boolShowRawData = true;
          intSendLogCode = 664;
        }
        intRC = DashboardPort.SendLine((char*)strInput.c_str());
        strAddress = DashboardPort.Remote_IP.stringAddress;
        if (intRC) {return intRC;}
        break;
   default:
        //error
        SendCodingError( "globalfunctions.cpp - Transmit_DataIn Transmit not used");
        break;
  }//end switch(enumPortType)

 if (boolShowRawData)  {
   
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, intSendLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LOG_MESSAGE_531, strAddress, 
                              ASCII_String(strInput.c_str(), strInput.length()),"","","","",  DEBUG_MSG, NEXT_LINE);
   enQueue_Message(enumPortType,objMessage);

 }        
  
 return 0;
}//end Transmit_Data
/****************************************************************************************************
*
* Name:
*  Function: string ExperientUDPPort::fCheck_Delimiter(string stringData, bool &boolDelimeterFound, char Delimiter1,char Delimiter2, char Delimiter3 )
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function checks the string for the first occurance of up to 3 delimeters. The substring from 0 to the first delimeter
*   found is returned (position[0]+1) and the boolean boolDelimeterFound is set to true.  Otherwise the entire string is returned with
*   the boolean set to false.
*          
*    
* Parameters:
*   stringData                                <cstring>  string to search                                                         
*   boolDelimeterFound                        boolean    set to true if a delimeter is found
*   Delimiter1                                char
*   Delimiter2                                char
*   Delimiter3                                char
*
* Variables:
*  position[4]                                Local   - <cstring><size_t> array
*  string::npos                               Library - <cstring>  indicates character not found
*                                                                    
* Functions:
*  .find()                                    Library - <cstring>
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string fCheck_Delimiter(string stringData, bool &boolDelimeterFound, char Delimiter1,char Delimiter2, char Delimiter3 )
{
 size_t  position[4] = {string::npos,string::npos,string::npos,string::npos};
 
 if(Delimiter1){position[1] = stringData.find(Delimiter1,0);}
 if(Delimiter2){position[2] = stringData.find(Delimiter2,0);}
 if(Delimiter3){position[3] = stringData.find(Delimiter3,0);}

 for (int i = 1; i < 4; i++){ if (position[0] > position[i]){ position[0] = position[i];} }
 
 if (position[0] == string::npos){ boolDelimeterFound = false; return stringData;}
 else                            { boolDelimeterFound = true;  return stringData.substr(0,position[0]+1);}

}// end ExperientUDPPort::fCheck_Delimiter()

/****************************************************************************************************
*
* Name:
*  Function: string GetLogFileName(log_type enumLogtype)
*
*
* Description:
*  This is a utility function
*
*
* Details:
*  The function calculates and returns the name of the PSAP monthly Log File Name.
*          
*    
* Parameters:
*   enumLogtype                                   <log_type>                         
*
* Variables:
*   charLOG_FILE_PATH_NAME              Global  - <defines.h>
*   charLOG_FILE_NAME_SUFFIX            Global  - <defines.h>
*   charPSAP_NAME                       Global  - <defines.h>
*   charXML_LOG_FILE_NAME_SUFFIX        Global  - <defines.h>
*   CLOCK_REALTIME			Global	- <ctime> constant
*   stringYearMonth			Local	- <cstring> char representing YYYY-MM-
*   timespecTimeNow                     Local   - <ctime><timespec> struct that holds current time
*                                                                          
* Functions:
*   clock_gettime()                     Library - <ctime>                       (int)
*   .erase()                            Library - <cstring>                     (string& erase)
*   get_time_stamp()                    Global  - <globalfunctions.h>           (string)
*  
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
string GetLogFileName(log_type enumLogtype)
{
 string                 stringYearMonth, strYear, strMonth, strDayofWeek, strDayofMonth, strHour, strMinute, strSeconds;
 char                   charYear[10], charMonth[10], charDay[10], charDayofMonth[10], charSeconds[10], charMinute[10], charHour[10];
 struct tm              tmLocalTimeStamp;
 time_t                 time_tTimeStamp;
 string                 stringLogFilePathName = charLOG_FILE_PATH_NAME;
 string                 stringAMIfilePathName = charAMI_LOG_FILE_PATH;
 string                 stringJSONfilePathName = charJSON_LOG_FILE_PATH;
 string                 strTimeCreated;
//


 stringLogFilePathName  += "/";
 stringAMIfilePathName  += "/";
 stringJSONfilePathName += "/";
 stringYearMonth[4] = '-';

 time_tTimeStamp = time(NULL); 
 localtime_r(&time_tTimeStamp,&tmLocalTimeStamp);
 strftime(charYear,      10, "%Y", &tmLocalTimeStamp); 
 strftime(charMonth,     10, "%m", &tmLocalTimeStamp);
 strftime(charDay,       10, "%a", &tmLocalTimeStamp);
 strftime(charDayofMonth,10, "%d", &tmLocalTimeStamp);
 strftime(charHour      ,10, "%H", &tmLocalTimeStamp);
 strftime(charMinute    ,10, "%M", &tmLocalTimeStamp);
 strftime(charSeconds   ,10, "%S", &tmLocalTimeStamp);


 stringYearMonth = charYear;
 stringYearMonth += "-";
 stringYearMonth += charMonth;
 strDayofWeek  = charDay;
 strDayofMonth = charDayofMonth;
 strHour       = charHour;
 strMinute     = charMinute;
 strSeconds    = charSeconds;


 switch (enumLogtype)
  {
   case READABLE_LOG:
        strTimeCreated = strDayofMonth + "_" + strHour + strMinute + strSeconds;
        return stringLogFilePathName+ stringSERVER_HOSTNAME + "_"+ stringYearMonth + "_" + strTimeCreated + charLOG_FILE_NAME_SUFFIX;
   case XML_LOG: 
        strTimeCreated = strDayofMonth + "_" + strHour + strMinute + strSeconds;
        return stringLogFilePathName+ stringSERVER_HOSTNAME + "_"+ stringYearMonth + "_" + strTimeCreated + charOPEN_XML_LOG_FILE_NAME_SUFFIX;
   case DEBUG_LOG:
        strTimeCreated = strDayofMonth + "_" + strHour + strMinute + strSeconds;
        return stringLogFilePathName+ stringSERVER_HOSTNAME + "_"+ stringYearMonth + "_" + strTimeCreated + char_DEBUG_LOG_FILE_NAME_SUFFIX;
   case AMI_SCRIPT:
        return stringAMIfilePathName + stringYearMonth + "-" + strDayofMonth + "-" + strDayofWeek + "." + char_AMI_SCRIPT_LOG_NAME + char_AMI_SCRIPT_LOG_SUFFIX;
   case JSON_LOG:
        strTimeCreated = strDayofMonth + "_" + strHour + strMinute + strSeconds;
        return stringJSONfilePathName + stringSERVER_HOSTNAME + "_"+ stringYearMonth + "_" + strTimeCreated + charJSON_LOG_FILE_SUFFIX;
  }
 return "error in GetLogFileName";
}// end GetLogFileName

string MonthFromLogFileName(string strLogfile)
{
 size_t found;

 found = strLogfile.find("_");

 if (found == string::npos)  {return "UNKNOWN";}

 found = strLogfile.find('-', found);

 if (found == string::npos)  {return "UNKNOWN";}

 if (strLogfile.length() < (found +2)) {return "UNKNOWN";}

 return strLogfile.substr((found+1),2);

}

string Month_DayFromLogFileName(string strLogfile)
{
 size_t found;

 found = strLogfile.find("_");

 if (found == string::npos)  {return "UNKNOWN";}

 found = strLogfile.find('-', found);

 if (found == string::npos)  {return "UNKNOWN";}

 if (strLogfile.length() < (found +5)) {return "UNKNOWN";}

 return strLogfile.substr((found+1),5);

}






/****************************************************************************************************
*
* Name:
*  Function: bool Thread_Trap::fAllThreadsReady()
*
*
* Description:
*  This is a Thread_Trap Class function implementation
*
*
* Details:
*  The function checks to see if all threads are ready to shut down. If they are
*  it returns true.
*          
*    
* Parameters:
*   none
*
* Variables:
*   boolALiThreadReady                  Global  - <Thread_Trap> member
*   boolANIThreadReady                  Global  - <Thread_Trap> member
*   boolCadThreadReady                  Global  - <Thread_Trap> member
*   boolLogThreadReady                  Global  - <Thread_Trap> member
*   boolWRKThreadReady                  Global  - <Thread_Trap> member
*   boolAMIThreadReady                  Global  - <Thread_Trap> member 
*   boolRCCThreadReady                  Global  - <Thread_Trap> member                                                 
* Functions:
*   none
*  
* AUTHOR: 2/6/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool Thread_Trap::fAllThreadsReady()
{
 if (!boolCONTACT_RELAY_INSTALLED) {boolRCCThreadReady = true;}
 if (!boolCAD_EXISTS)              {boolCadThreadReady = true;}
 if (!boolUSE_POLYCOM_CTI)         {boolCTIThreadReady = true;}
 if (!boolNG_ALI)                  {boolLISThreadReady = true;}
 if (!boolLEGACY_ALI)              {boolALiThreadReady = true;}
 if (!boolUSE_DASHBOARD)           {boolDSBThreadReady = true;}

 return (boolCadThreadReady && boolLogThreadReady && boolALiThreadReady && boolANIThreadReady && boolWRKThreadReady &&  boolAMIThreadReady && 
         boolRCCThreadReady && boolCTIThreadReady && boolLISThreadReady && boolDSBThreadReady );
}

void Thread_Trap::fShowThreadsState()
{
 if (boolCadThreadReady) {cout << "CAD - Ready" << endl;}
 else                    {cout << "CAD - Not Ready" << endl;}
 if (boolLogThreadReady) {cout << "LOG - Ready" << endl;}
 else                    {cout << "LOG - Not Ready" << endl;}
 if (boolALiThreadReady) {cout << "ALI - Ready" << endl;}
 else                    {cout << "ALI - Not Ready" << endl;}
 if (boolANIThreadReady) {cout << "ANI - Ready" << endl;}
 else                    {cout << "ANI - Not Ready" << endl;}
 if (boolWRKThreadReady) {cout << "WRK - Ready" << endl;}
 else                    {cout << "WRK - Not Ready" << endl;}
 if (boolAMIThreadReady) {cout << "AMI - Ready" << endl;}
 else                    {cout << "AMI - Not Ready" << endl;}
 if (boolRCCThreadReady) {cout << "RCC - Ready" << endl;}
 else                    {cout << "RCC - Not Ready" << endl;}
 if (boolCTIThreadReady) {cout << "CTI - Ready" << endl;}
 else                    {cout << "CTI - Not Ready" << endl;}
 if (boolLISThreadReady) {cout << "LIS - Ready" << endl;}
 else                    {cout << "LIS - Not Ready" << endl;}
 if (boolDSBThreadReady) {cout << "DSB - Ready" << endl;}
 else                    {cout << "DSB - Not Ready" << endl;}
}

string DisplayMode(display_mode eDisplayMode) {

 switch(eDisplayMode) {
  case NO_DISPLAY_DEFINED: 	return "NO_DISPLAY_DEFINED";
  case MINIMAL_DISPLAY:		return "MINIMAL_DISPLAY";
  case NORMAL_DISPLAY:		return "NORMAL_DISPLAY";
  case DEBUG_KRN:		return "DEBUG_KRN";
  case DEBUG_ALI:		return "DEBUG_ALI";
  case DEBUG_ANI:		return "DEBUG_ANI";
  case DEBUG_CAD:		return "DEBUG_CAD";
  case DEBUG_WRK:		return "DEBUG_WRK";
  case DEBUG_AMI:		return "DEBUG_AMI";
  case DEBUG_RCC:		return "DEBUG_RCC";
  case DEBUG_DSB:		return "DEBUG_DSB";
  case DEBUG_ALL:		return "DEBUG_ALL";
 }
return "DisplayMode() NOT HANDLED IN SW";
}

void SendSignaltoThreads()
{
 extern sem_t sem_tLISflag;

 sem_post(&sem_tCTIflag);
 sem_post(&sem_tLISflag);
 sem_post(&sem_tRCCflag);
 sem_post(&sem_tLogConsoleEmailFlag);
 sem_post(&sem_tWRKFlag);
 sem_post(&sem_tAMIFlag);                                  
 sem_post(&sem_tANIFlag);
 sem_post(&sem_tCadFlag);
 sem_post(&sem_tAliFlag);
 sem_post(&sem_tDSBflag); 
}

/****************************************************************************************************
*
* Name:
*  Function: Ports_Ready_To_Shut_Down()
*
*
* Description:
*  This is a utility function
*
*
* Details:
*  The function checks to see if all ports are ready to shut down. If they are
*  it returns true.
*          
*    
* Parameters:
*   intNumPorts                         int
*   Port[]                              <ExperientCommPort>   
*
* Variables:
*   i                                   Local   - int counter
*   boolRunningcheck                    Local   - bool
*   boolReadyToShutDown                 Global  - <ExperientCommPort>
*                                                                          
* Functions:
*   none
*  
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool Ports_Ready_To_Shut_Down(ExperientCommPort Port[], int intNumPorts)
{
 bool boolRunningcheck = true;
 for (int i = 1; i <= intNumPorts; i++){boolRunningcheck = (boolRunningcheck && Port[i].boolReadyToShutDown);}
 
 return boolRunningcheck;
}
/****************************************************************************************************
*
* Name:
*  Function: string ASCII_Convert(char charARG)
*
*
* Description:
*  This is a utility function
*
*
* Details:
*  The function converts character data into it's ASCII code separated by carats if it
*  is not a normal character (codes 32 thru 127)
*          
*    
* Parameters:
*   charARG                                       char
*
* Variables:
*   intASCIICode                        Local   - integer 
*   stringTemp                          Local   - <cstring> string
*                                                                          
* Functions:
*   int2strLZ()                         Local   -                         (string) 
*  
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
string ASCII_Convert(unsigned char charARG)
{
 int            intASCIICode;
 string         stringTemp;

 intASCIICode = (int) charARG;
 stringTemp   = charARG;
          
 if ((intASCIICode > 31)&&(intASCIICode < 127)) {return stringTemp;}

 switch (intASCIICode)
  {
   case 0:
    return "<NULL>";
   case 2:
    return "<STX>";
   case 3:
    return "<ETX>";
   case 6:
    return "<ACK>";
   case 10:
    return "<LF>";
   case 13:
    return "<CR>";
   case 21:
    return "<NAK>";
   default:
    return "<"+int2strTwoLZ(intASCIICode)+">";
  }//end switch

}//end ASCII_Convert(char charARG)

/****************************************************************************************************
*
* Name:
*  Function: string ASCII_String(string stringArg, size_t size_tLen)
*
*
* Description:
*  This is a utility function
*
*
* Details: 
*  The function returns a ASCII coded string using an input string and for a specified length.
*  if size_tLen is longer than the actual string then it is ignored and the actual length is used.
*          
*    
* Parameters:
*   stringArg                                    <cstring>
*   size_tLen                                    <size_t>
*
* Variables:
*   stringTemp                          Local   - <cstring> string
*   size_tLength			Local   - <size_t>
*                                                                       
* Functions:
*   ASCII_Convert()                     Local   -                       (string)
*   .length()				Library - <cstring>             (size_t)
*
* AUTHOR: 09/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string ASCII_String(const char * charInput,  size_t size_tLen)
{
 string         stringTemp = "";
 size_t         size_tLength;

// if (stringArg.empty()){return "<00>";}
// if (strlen(charInput) > size_tLen){cout << "bigger" << endl;}
// if (size_tLen > strlen(charInput)) {size_tLength = strlen(charInput);}
// else                               {size_tLength = size_tLen;}
 if(size_tLen == 0){return "<000>";}
 size_tLength = size_tLen;
 for (size_t i = 0; i < size_tLength; i++){stringTemp+= ASCII_Convert(charInput[i]);}
 return stringTemp;

}// end ASCII_String()



string HEX_Convert(unsigned char ch)
{
 int            LeftDigit, RightDigit;
 string         HexString ="0123456789ABCDEF";
 string         strOutput = "<";

 RightDigit = (int)(ch&0x0F); 
 LeftDigit  = (int) ( ((ch>>4)&0xF) ) ;
 
 strOutput +=  HexString[LeftDigit];
 strOutput +=  HexString[RightDigit];
 strOutput += ">";
 return  strOutput;  

}//end HEX_Convert(char ch)




string HEX_String(unsigned char* chInput, size_t sLength)
{
 string  stringTemp = "";

 if(sLength == 0){return "<00>";}

 for (size_t i = 0; i < sLength; i++){stringTemp+= HEX_Convert(chInput[i]);}
 return stringTemp;

}// end HEX_String()
/****************************************************************************************************
*
* Name:
*  Function: string seconds2time(long long int intArg)
*
*
* Description:
*  This is a utility function
*
*
* Details:
*  The function converts seconds into a time phrase of the format  X Hour(s) Y Minute(s) Z Second(s)
*    
* Parameters:
*   intArg                                        integer
*
* Variables:
*   intHours                            Local   - conversion of seconds to hours 
*   intMin                              Local   - conversion of seconds to minutes
*   intSec                              Local   - remainder of seconds
*   oss                                 Local   - <sstream> ostringstream stream to receive string data
*                                                                          
* Functions:
*   .str()                              <sstream>                       (string) 
*  
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
string seconds2time(long long int intArg)
{
 int                    intDays,intHours,intMin,intSec;
 ostringstream 	oss;
 
 intDays  = (int(intArg)/86400);
 intHours = (int(int(intArg)%86400)/3600);
 intMin   = (int(int(int(intArg)%86400)%3600)/60);
 intSec   = (int(int(int(intArg)%86400)%3600)%60);

 if (intDays > 1)                               {oss <<  intDays << " Days";}
 else if (intDays == 1)                        {oss <<  intDays << " Day";}
 if (  ((intMin>0)||(intSec>0)||(intHours > 0))&&(intDays > 0) ) {oss <<  ", ";}
 
 if (intHours > 1)                              {oss <<  intHours << " Hours";}
 else if (intHours == 1)                        {oss <<  intHours << " Hour";}
 if (((intMin>0)||(intSec>0))&& (intHours > 0)) {oss <<  ", ";}
 
 if ((((intMin>0)||(intSec>0))&&(intHours>0))||(intMin>0))
  {
   oss << intMin;
   if(intMin == 1)                              {oss << " Minute";}
   else                                         {oss << " Minutes";}
   if (intSec)                                  {oss << " and ";}
  }

 if (intSec)
  {
   oss << intSec;
   if (intSec == 1)                             {oss << " Second";}           
	   else                                 {oss << " Seconds";}
	  }

 return oss.str();

}// end seconds2time(int intArg)

/****************************************************************************************************
*
* Name:
*  Function: void Check_Port_Down_Notification()
*
*
* Description:
*  Log Thread Utility Function
*
*
* Details:
*   this function checks the Port for how long it has been in reduced heartbeat mode and if the threshold
*   specified in the configuration file is met and alarm/info email & log display is sent. ALI has it's own
*   version of this function written in the ALI section
*
*
* Parameters:
*   CommPort[]                          <ExperientCommPort>  the port to check				
*   enumPortType                        threadorPorttype     the type of port
*   intNumofPorts                       int                  the number of ports  
*
* Variables:
*   ANI                                 Global  <header.h><threadorPorttype> enumeration member
*   .boolReducedHeartBeatMode           Global  <ExperientCommPort> port is in reduced heartbeat mode if true
*   .boolReducedHeartBeatModeFirstAlarm Global  <ExperientCommPort> first Alarm message has been sent if true
*   CAD                                 Global  <header.h><threadorPorttype> enumeration member
*   CLOCK_REALTIME                      Library <ctime> constant
*   i                                   Local   general purpose integer
*   intANI_PORT_DOWN_REMINDER_SEC       Global  <globals.h>
*   intCAD_PORT_DOWN_REMINDER_SEC       Global  <globals.h>
*   intLogCode                          Local   contains code for logging purpose
*   .intPortDownReminderThreshold       Global  <ExperientCommPort> member threshold for hourly reminder converted to sec (*3600)
*   .intPortDownThresholdSec            Global  <ExperientCommPort> member 
*   intRC                               Local   return code
*   KRN_MESSAGE_120                     Global  <defines.h> Port Down for xx output message
*   KRN_MESSAGE_121                     Global  <defines.h> Reminder - Port Down for xx output message
*   ldoubleTimeDiff                     Local   abs time differential
*   LOG_ALARM                           Global  <defines.h> Logging code
*   objMessage                          Local   <MessageClass> object
*   .timespecReducedHeartBeatMode       Global  <ExperientCommPort> time that port began reduced heartbeat mode 
*   timespecTimeNow                     Local   <ctime> struct time now
*   UDP_Port                            Local   <ExperientUDPPort> object
                                                                      
* Functions:
*   clock_gettime()                     Library <ctime>                         (int)
*   enQueue_Message()                   Global  <globalfunctions.h>             (void)
*   .erase()                            Library <cstring>                       (string& erase)
*   .fMessage_Create()                  Class   <MessageClass>                  (void)
*   int2strLZ()                         Local   <globalfunctions.h>             (string)
*   .fPingStatus()                      Global  <ExperientUDPPort>              (bool)
*   .fPingStatusString()                Global  <ExperientUDPPort>              (string)
*   Thread_Calling()                    Local   <globalfunctions.h>             (string)
*   seconds2time()                      Local   <globalfunctions.h>             (string)
*   time_difference()			Local   <globalfunctions.h>             (long double)  
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/ 
void Check_Port_Down_Notification(threadorPorttype enumPortType, int intNumofPorts, ExperientCommPort* CommPort)
{
 MessageClass           objMessage;
 long double            ldoubleTimeDiff;
 struct timespec        timespecTimeNow;
 int                    intLogCode = 0;
 Port_Data              objPortData;
 string                 strAlarmMessage;

 extern ExperientCommPort    CADPort    [NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort    ANIPort	[NUM_ANI_PORTS_MAX+1];
 
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);  
 for (int i = 1; i <= intNumofPorts; i++)
  {
   if (CommPort[i].boolReducedHeartBeatMode)
    {
     ldoubleTimeDiff = time_difference(timespecTimeNow, CommPort[i].timespecReducedHeartBeatMode);
     objPortData.fClear();

     //check for first alarm condition
     if (( ldoubleTimeDiff > CommPort[i].intPortDownThresholdSec) && (!CommPort[i].boolReducedHeartBeatModeFirstAlarm))  {
         
       switch(enumPortType)
        {
         case CAD:
              intLogCode = 419;
              objPortData = CADPort[i].UDP_Port.fPortData();
              break;
         case ANI:
              intLogCode = 321;
              objPortData = ANIPort[i].UDP_Port.fPortData();
              break;
         case WRK:
              //not applicable
              break;
         default:
              //Error
              break;
        }// end switch
       
       CommPort[i].UDP_Port.fPingStatus();
       strAlarmMessage = Create_Message(KRN_MESSAGE_120, seconds2time(int(ldoubleTimeDiff)),CommPort[i].UDP_Port.fPingStatusString());
       objMessage.fMessage_Create(LOG_ALARM, intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, strAlarmMessage, " ", "Note\t= " + CommPort[i].strNotes,
                                  "","","","", NORMAL_MSG, NEXT_LINE); 
       enQueue_Message(enumPortType,objMessage);
       CommPort[i].boolReducedHeartBeatModeFirstAlarm = true;
     }

     //else if check for Port Down hourly reminder interval condition........
     else if  ((ldoubleTimeDiff > CommPort[i].intPortDownReminderThreshold) && (CommPort[i].boolReducedHeartBeatModeFirstAlarm))   {
      
       switch(enumPortType)
        {
         case CAD:
              intLogCode = 420;
              CommPort[i].intPortDownReminderThreshold += (intCAD_PORT_DOWN_REMINDER_SEC);
              objPortData = CADPort[i].UDP_Port.fPortData();
              break;
         case ANI:
              intLogCode = 322;
              CommPort[i].intPortDownReminderThreshold += (intANI_PORT_DOWN_REMINDER_SEC);
              objPortData = ANIPort[i].UDP_Port.fPortData();
              break;
         case WRK:
              //not applicable
              break;
         default:
              //Error
              break;
        }// end switch
     
       CommPort[i].UDP_Port.fPingStatus();
       objMessage.fMessage_Create(LOG_ALARM, intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                                  objBLANK_ALI_DATA, KRN_MESSAGE_121, seconds2time((unsigned long long int)(ldoubleTimeDiff)), CommPort[i].UDP_Port.fPingStatusString());
        
       enQueue_Message(enumPortType,objMessage);

     } // end else if      

    }//end if (CommPort[i].boolReducedHeartBeatMode)
	
  }//end for (int i = 1; i <= intNumofPorts; i++)

}// end void Port_Down_Message()



/****************************************************************************************************
*
* Name:
*  Function: bool IsAnyPortActive(ExperientCommPort Port[], int intNumPorts)
*
*
* Description:
*   A utility function
*
*
* Details:
*   This function checks all ports to see if any port is active (awaiting ACK). If any port is
*   active the function returns true.
*          
*    
* Parameters: 				
*   Port[]                                      <ExperientCommPort>
*   intNumPorts                                 int					
*
* Variables:
*   boolCheck                                   Local   - bool output
*   .boolPortActive                             Global  - <ExperientCommPort> member
*   i                                           Local   - int counter
*
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool IsAnyPortActive(ExperientCommPort Port[], int intNumPorts)
{
     bool boolCheck = false;
     for(int i = 1; i <= intNumPorts; i++){ boolCheck = (boolCheck || Port[i].boolPortActive); }
     return boolCheck;
}//end IsAnyPortActive()

/****************************************************************************************************
*
* Name:
*  Function: void ClearPorts(ExperientCommPort Port[], int intNumPorts)
*
*
* Description:
*   A utility function
*
*
* Details:
*   This function resets certain data on all ports.
*   
*          
*    
* Parameters: 				
*   Port[]                                      <ExperientCommPort>
*   intNumPorts                                 int					
*
* Variables:
*   .boolACKReceived                            Global  - <ExperientCommPort>
*   .boolNAKReceived                            Global  - <ExperientCommPort>
*   .boolPortActive                             Global  - <ExperientCommPort>
*   .boolXmitRetry                              Global  - <ExperientCommPort>
*   .timespecTimeXmitStamp                      Global  - <ExperientCommPort>
*   .tv_nsec                                    Global  - <ctime><timespec> member
*   .tv_sec                                     Global  - <ctime><timespec> member
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
void ClearPorts(ExperientCommPort Port[], int intNumPorts)
{
 for(int i = 1; i <= intNumPorts; i++)
  {
   Port[i].boolPortActive 		                = false;
   Port[i].boolXmitRetry               		        = true;
   Port[i].boolACKReceived                              = false;
   Port[i].boolNAKReceived                              = false;
   Port[i].timespecTimeXmitStamp.tv_sec		        = 0;
   Port[i].timespecTimeXmitStamp.tv_nsec		= 0;
  }
 return;
}// end ClearPorts()

/****************************************************************************************************
*
* Name:
*  Function: int PortQueueRoundRobin(queue <DataPacketIn> *PortQueue, int intNumPorts,  int intFirst)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function recieves an array of Queues and returns the port number of 
*   the first queue that is not empty.  It begins it's search on the port
*   number passed in (intFirst) and increments thru the list until reaching
*   the end and begins at port 1 until all (intNumPorts) are traversed.
*          
*    
* Parameters: 				
*   *PortQueue				        - queue <DataPacketIn>
*   intPortNum					- int
*   intFirst                                    - int
*   MutexLock                                   - sem_t
* Variables:
*   i                                   Local   - int counter
*   j                                   Local   - int for calculated port number                         
*                                                                          
* Functions:
*   .empty()                            Library - <queue>               (bool)                           
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 10/17/2015
* added parameter MutexLock due to .empty() function not thread safe!
*
* COPYRIGHT: 2006-2015 Experient Corporation
****************************************************************************************************/
int PortQueueRoundRobin(queue <DataPacketIn> *PortQueue, int intNumPorts,  int intFirst , sem_t* MutexLock)
{
 int j;

 sem_wait(MutexLock);
 for (int i = 0; i < intNumPorts; i++)
  {
   j = intFirst + i;
   if (j > intNumPorts) { j -= intNumPorts;}

   if  (!PortQueue[j].empty() ) {sem_post(MutexLock); return j;}
  }
 sem_post(MutexLock);
return 0;
}
/****************************************************************************************************
*
* Name:
*  Function: bool Validate_Integer(string stringArg)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function returns true if a string contains only Integer Data
*   
*          
*    
* Parameters: 				
*   stringArg				        - <cstring>
*
* Variables:
*   i                                   Local   - int counter
*   j                                   Local   - int for calculated port number                         
*                                                                          
* Functions:
*   isdigit()                            Library - <cctype>                     (bool)
*   .length()                            Library - <cstring>                    (size_t)                           
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool Validate_Integer(string stringArg)
{
 if (stringArg == ""){return false;}
 for (unsigned int i = 0; i< stringArg.length(); i++){if (!isdigit(stringArg[i])){return false;} }
 return true;
}
/****************************************************************************************************
*
* Name:
*  Function: bool ValidateFieldAndRange(string stringArg, int intPortNum, string Field, int High, int Low)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function returns true if a string contains only Integer Data
*             
*    
* Parameters:
*   intHigh                                     - int
*   intLow                                      - int 				
*   stringArg				        - <cstring>
*   stringField                                 - <cstring>
*   intPortNum                                  - int
*
* Variables:
*   ANI                                 Global  - <header.h><threadorPorttype> enumeration member
*   intLogCode                          Local   - log code
*   KRN_MESSAGE_129                     Global  - <defines.h> Invalid Data Received output message
*   LOG_WARNING                         Global  - <defines.h> log code
*   objMessage                          Local   - <MessageClass> object                     
*                                                                          
* Functions:
*   char2int                             Local   - <globalfunctions.h>          (string)
*   .c_str()                             Library - <cstring>                    (const char*)  
*   enQueue_Message()                    Local   - <globalfunctions.h>          (void)
*   .erase()                             Library - <cstring>                    (string& erase)
*   .fMessage_Create()                   Global  - <MessageClass>               (void)
*   Thread_Calling()                     Local   - <globalfunctions.h>          (string)
*   Validate_Integer()                   Local   - <globalfunctions.h>          (bool)                          
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool ValidateFieldAndRange(threadorPorttype enumArg, string stringArg, string strRawData, int intPortNum, string stringField, unsigned long long int intHigh, unsigned int intLow)
{
 unsigned long long int            intNumber;
 int                               intLogCode;
 MessageClass                      objMessage;
 Port_Data                         objPortData;
 string                            stringErrorMessage;

 objPortData.fLoadPortData(enumArg,intPortNum);

 switch (enumArg)
  {
   case ANI: intLogCode = 323; break;
   case ALI: intLogCode = 218; break;
   case CAD: intLogCode = 443; break;
   case WRK: intLogCode = 710; break;
   default:
             intLogCode = 129;
  }


 if (!Validate_Integer(stringArg)) 
  {
   stringErrorMessage = Create_Message(KRN_MESSAGE_129, stringField) + ", Data=\"%%%\", (Complete Data Follows)";
   objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringErrorMessage, stringArg,
                              ASCII_String(strRawData.c_str(), strRawData.length()),"","","","",  NORMAL_MSG, NEXT_LINE);
   enQueue_Message(enumArg,objMessage);
   return false;
  }
 intNumber = char2int(stringArg.c_str());
 
 if (intHigh)
  {
   
   if (intNumber > intHigh)
    {
     stringErrorMessage = Create_Message(KRN_MESSAGE_129, stringField + ", Out of Range")+", Data=\"%%%\", (Complete Data Follows)";
     objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringErrorMessage, stringArg,
                                ASCII_String(strRawData.c_str(), strRawData.length()),"","","","",  NORMAL_MSG, NEXT_LINE);
     enQueue_Message(enumArg,objMessage);
     return false;
    }
  }
                                     
 if (intLow)
  {
   if (intNumber < intLow)
    {
     stringErrorMessage = Create_Message(KRN_MESSAGE_129, stringField + ", Out of Range")+", Data=\"%%%\", (Complete Data Follows)";
     objMessage.fMessage_Create(LOG_WARNING,intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringErrorMessage, stringArg,
                                ASCII_String(strRawData.c_str(), strRawData.length()),"","","","",  NORMAL_MSG, NEXT_LINE);
     enQueue_Message(enumArg,objMessage);
     return false;
    }
  }

 return true;
}

/****************************************************************************************************
*
* Name:
*  Function: bool Validate_Trunk_Number(int intArg)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function returns true if the Argument is a valid trunk number.
*
*                 
* Parameters:
*   intArg                                      - int
*
* Variables:
*   intNUM_TRUNKS_INSTALLED              Global   - <globals.h>                      
*                                                                                                    
* Functions:
*   none
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool Validate_Trunk_Number( int intArg)
{
 if ((intArg > 94)&&(intArg <100)) {return true;}

 if (intArg <= intNUM_TRUNKS_INSTALLED){return true;}

 return false;
}

/****************************************************************************************************
*
* Name:
*  Function: string CheckSTXtoETX(string stringInput)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function recieves a string ending with an ETX and searches from the end for the STX delimiter
*   and returns a string beginning with STX and ending with ETX. If no STX is found "" is returned.  
*          
*    
* Parameters:
*   stringInput                                 - <cstring>
*
* Variables:
*   charSTX                             Global  - <globals.h>
*   position                            Local   - <cstring> size_t
*   string::npos                        Library - <cstring>
*   stringOutput                        Local   - <cstring>                    
*                                                                          
* Functions:
*   .assign()                           Library  - <cstring>            (string& assign)
*   .find_last_of()                     Library  - <cstring>            (size_t)
*   .length()                           Library  - <cstring>            (size_t)                        
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
string BuildSTXtoETX(string stringInput)
{
 size_t         position;
 string         stringOutput = "";

 position = stringInput.find_last_of(charSTX);
 if (position == string::npos){return "";}
 stringOutput.assign(stringInput,position, stringInput.length());
 return stringOutput;
}

/****************************************************************************************************
*
* Name:
*  Function: unsigned long long int Julian_Date(timespec timespecTimeNow)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function recieves a struct timespec time stamp and returns a Julian Date calculated down to 
*   the millisecond from UTC time.  The Function assumes a date in the Gregorian calendar is passed in 
*   as it's argument. Leap seconds are not accounted for.  The formula for the conversion was adapted
*   from http://en.wikipedia.org/wiki/Julian_day
*          
*    
* Parameters:
*   timespecTimeNow                             - <ctime> struct timespec object
*
* Variables:
*   A                                   Local   - integer - adjustment
*   day                                 Local   - integer - day number
*   hour                                Local   - integer - 0-23 hour number
*   intPlaces                           Local   - Unsigned Long Integer - used to shift decimal places for rounding
*   intRoundUp                          Local   - integer - 0 or 1
*   intSigDigit                         Local   - integer - integer representing the significant digit to be rounded
*   JulianDay                           Local   - long double - calculated julian day in decimal format
*   M                                   Local   - integer - month adjustment 
*   milli_sec                           Local   - integer - 0 - 999 converted from 0.0 to .999
*   min                                 Local   - integer - minutes 0-59
*   month                               Local   - integer - month number
*   sec                                 Local   - integer - seconds 0-59
*   time_tTimeStamp                     Local   - <ctime> <time_t> - seconds since epoch
*   .tm_hour                            Library - <ctime> <tm> struct member -  hour number 0-23
*   .tm_mday                            Library - <ctime> <tm> struct member -  day number 1-31
*   .tm_min                             Library - <ctime> <tm> struct member -  minute number 0-59
*   .tm_mon                             Library - <ctime> <tm> struct member -  month number 1-12
*   .tm_sec                             Library - <ctime> <tm> struct member -  seconds number 0-59
*   tmUniversalTimeStamp                Local   - <ctime> <tm> struct - object
*   .tm_year                            Library - <ctime> struct tm member - 4 digit year
*   .tv_sec                             Library - <ctime> <timespec> struct  member - seconds since epoch
*   Y                                   Local   - integer year adjustment                   
*   year                                Local   - integer - 4 digit year
                                                                        
* Functions:
*   gmtime_r()                          Library  - <ctime>                      (struct tm)
*   RoundToThousanth()                  Local    - <globalfunctions.h>          (float)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
unsigned long long int Julian_Date(struct timespec timespecTimeNow)
{
  time_t		        time_tTimeStamp;
  struct tm		        tmUniversalTimeStamp;
  int                           year,month,day,hour,min,sec;
  int                           milli_sec ;
  int                           intSigDigit;
  int                           intRoundUp;
  unsigned long long int        intPlaces;
  unsigned long long int        A,Y,M;
  long double                   JulianDay;
  

  time_tTimeStamp = timespecTimeNow.tv_sec;
  gmtime_r(&time_tTimeStamp,&tmUniversalTimeStamp);

  year      = tmUniversalTimeStamp.tm_year+1900;
  month     = tmUniversalTimeStamp.tm_mon + 1;
  day       = tmUniversalTimeStamp.tm_mday;
  hour      = tmUniversalTimeStamp.tm_hour;
  min       = tmUniversalTimeStamp.tm_min;
  sec       = tmUniversalTimeStamp.tm_sec;
  milli_sec = int (RoundToThousanth(timespecTimeNow) *1000);

  A = int ((14 - month)/12);
  Y = year + 4800 - A;
  M = month +(12*A) - 3;

  JulianDay = day + int(((153*M)+2)/5) + (365*Y) + int(Y/4) - int(Y/100) + int(Y/400) - 32045 + ((hour-12.0)/24.0) + (min/1440.0) + (sec/86400.0) 
                  + (milli_sec/86400000.0) ;

  // round answer
  intPlaces 	= (unsigned long long int)(JulianDay * intNANOSEC_PER_SEC);
  intSigDigit 	= intPlaces - (int(intPlaces /10) * 10);
  intPlaces 	/= 10;

  if (intSigDigit > 4) 	{intRoundUp = 1;}
  else			{intRoundUp = 0;}

  // send anwer in unsigned long long format
  return ( intPlaces + intRoundUp );
 
}

/****************************************************************************************************
*
* Name:
*  Function: string ReformatLongOutputToLog(string stringInput, string stringCallData, string stringPrefix)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function recieves a long string and formats it to be displayed in the log. The string should receive trusted ASCII
*   characters typically call the function ASCII_String() to convert before using this function. 
*          
*    
* Parameters:
*   stringInput                                  <cstring> Message Text
*   stringCallData				 <cstring> Call Data (trunk, pos, CallId, Callback)
*   stringPrefix				 <cstring> Time Stamp
*
* Variables:
*   LOG_BLANK_CALL_DATA_STRING		Global  - <defines.h>
*   charCR                              Global  - <globals.h>
*   charCurrentChar                     Local   - character
*   charLF                              Global  - <globals.h>
*   i                                   Local   - integer
*   j                                   Local   - integer
*   stringALI_LOG_SPACES                Global  - <globals.h>
*   stringLineFeedSpaces                Local   - <cstring> 
*   stringNEWLINEPREFIX                 Local   - <cstring>
*   stringTemp                          Local   - <cstring>                    
*                                                                          
* Functions:
*   .append()                           Library  - <cstring>            (string& append)
*   .assign()                           Library  - <cstring>            (string& assign)
*   .clear()                            Library  - <cstring>            (void)
*   .find_last_of()                     Library  - <cstring>            (size_t)
*   .length()                           Library  - <cstring>            (size_t)                        
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
string ReformatLongOutputToLog(string stringInput, string stringCallData, string stringPrefix, bool boolNewLine)
{
 string         stringTemp = "";
 string         stringNEWLINEPREFIX= stringPrefix;
 unsigned int   i,j;

 i = 0;
 do
  {
   if (i != 0)            {stringTemp += charCR;stringTemp += charLF;stringTemp += stringNEWLINEPREFIX;stringTemp += LOG_BLANK_CALL_DATA_STRING;}
   else if (boolNewLine)  {stringTemp += stringNEWLINEPREFIX; stringTemp += LOG_BLANK_CALL_DATA_STRING;}     
   else                   {stringTemp += stringNEWLINEPREFIX; stringTemp += stringCallData;}                     
   j = 0;
   
   while (j <= intDISPLAY_SPACES)
    {
     if (stringInput[i] == '\n') {stringTemp += '\n';stringTemp += stringNEWLINEPREFIX; stringTemp += LOG_BLANK_CALL_DATA_STRING; j=0;i++;}
     else                        {stringTemp += stringInput[i]; i++;j++;}    
     if (i == stringInput.length()) {return stringTemp;}
     
    }//end  while (j <= intDISPLAY_SPACES)
  }while (i < stringInput.length());

 return stringTemp;

}
/****************************************************************************************************
*
* Name:
*  Function: string ReformatTDDConversationToLog(string stringInput, string stringCallData, string stringPrefix)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function recieves a TDD Conversation string and formats it to be displayed in the log. The string shall receive trusted ASCII
*   characters Converted from Baudot with a delimeter of <etx> between each line to be displayed.  A blank log line will be generated
*   after each caller or dispatcher sentence. LF, CR & CRLF will cause the log to move to the next line.
*   the log will look like the following:
*   <time data & call data>| Caller:
*   <time data & call data>|    hello I am the caller
*   <time data & call data>|
*   <time data & call data>| Dispatcher:
*   <time data & call data>|    hello I am the dispatcher
*   <time data & call data>|       
*    
* Parameters:
*   stringInput                                  <cstring> Message Text
*   stringCallData				 <cstring> Call Data (trunk, pos, CallId, Callback)
*   stringPrefix				 <cstring> Time Stamp
*
* Variables:
*   LOG_BLANK_CALL_DATA_STRING		Global  - <defines.h>
*   charETX                             Global  - <globals.h>
*   charCR                              Global  - <globals.h>
*   charLF                              Global  - <globals.h>
*   i                                   Local   - integer
*   intDISPLAY_SPACES                   Global  - <defines.h> the number of spaces before wrapping begins
*   j                                   Local   - integer
*   SearchPosition                      Local   - <cstring><size_t> used to find locations character within strings
*   strCallerPrefix                     Local   - <cstring> same as TDD_CALLER_SAYS_PREFIX
*   strDispatcherPrefix                 Local   - <cstring>
*   strDipatcherPrefixWithNumber        Local   - <cstring> full Dispatcher prefix with substituted number (if present)
*   strTruncatedDispatcherPrefix        Local   - <cstring> Dispatcher prefix with the %%% removed.(%%% is used for substitution) 
*   strNewLine                          Local   - <cstring> A Log line without end data
*   stringNEWLINEPREFIX                 Local   - <cstring>
*   stringTemp                          Local   - <cstring> used to build the output string
*   string::npos                        Library - <cstring><size_t> used to show character not found
*   TDD_CALLER_SAYS_PREFIX              Global  - <defines.h> Prefix added to caller sentences 
*   TDD_DISPATCHER_SAYS_PREFIX          Global  - <defines.h> Prefix added to dispatcher sentences                  
*                                                                          
* Functions:
*   .append()                           Library  - <cstring>            (*this)
*   .length()                           Library  - <cstring>            (size_t)                        
*   .find()                             Library  - <cstring>            (size_t)
*   cout                                Library  - <iostream>           (ostream)
*   .substr()                           Library  - <cstring>            (string)
*   
*
* AUTHOR: 01/19/2009 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2009 Experient Corporation
****************************************************************************************************/

string ReformatTDDConversationToLog(string stringInput, string stringCallData, string stringPrefix)
{
 string         stringTemp = "";
 string         stringNEWLINEPREFIX = stringPrefix;
 string         strCallerPrefix     = TDD_CALLER_SAYS_PREFIX;
 string         strDispatcherPrefix = TDD_DISPATCHER_SAYS_PREFIX;
 string         strTruncatedDispatcherPrefix;
 string         strTruncatedCallerPrefix; 
 string         strLinePrefix;
 string         strNewLine;
 unsigned int   i,j;
 size_t         SearchPosition;
 bool           boolLogPrefix;
  
 SearchPosition = strDispatcherPrefix.find("%%%",0);
 if (SearchPosition == string::npos) { SendCodingError( "globalfunctions.cpp - a. Format error in function ReformatTDDConversationToLog, Unable to process"); return "";}
 strTruncatedDispatcherPrefix = strDispatcherPrefix.substr(0,SearchPosition);


 SearchPosition = strCallerPrefix.find("%%%",0);
 if (SearchPosition == string::npos) {SendCodingError( "globalfunctions.cpp - b. Format error in function ReformatTDDConversationToLog, Unable to process"); return "";}
 strTruncatedCallerPrefix     = strCallerPrefix.substr(0,SearchPosition);

 strNewLine += charCR;strNewLine += charLF;strNewLine += stringNEWLINEPREFIX;

 boolLogPrefix = ((!stringCallData.empty())&&(!stringPrefix.empty()));

 if (boolLogPrefix) { strNewLine += LOG_BLANK_CALL_DATA_STRING;} 


// "Dispatcher%%% (%%%):"
// "Caller (%%%):"


 i = 0;
 do
  {
  if ((i!=0)&&(!boolLogPrefix)) {stringTemp += strNewLine;}
  else if (boolLogPrefix)       {stringTemp += strNewLine;}
                   
   j = 0;
   
   while (j <= intDISPLAY_SPACES)
    {
     if (j == 0)
      {
        // check for "Caller:" prefix
        SearchPosition = stringInput.find(strTruncatedCallerPrefix,i);     
        if (SearchPosition == i)
         {
          SearchPosition = stringInput.find("]:",i);
          if (SearchPosition != string::npos)
           {
            if (i > (stringInput.length()-1)) { SendCodingError("globalfunctions.cpp - substr ReformatTDDConversationToLog() a"); return "";}
            if (((SearchPosition - i) +2) < 0){ SendCodingError("globalfunctions.cpp - substr ReformatTDDConversationToLog() b"); return "";}
            strLinePrefix = stringInput.substr(i, (SearchPosition - i) +2);
            stringTemp += strLinePrefix; i+= strLinePrefix.length();
     //       else               {stringTemp += charCR;stringTemp += charLF; i+=1;} 
            break;
           }
         }
        // check for "Dispatcher X:" prefix
        SearchPosition = stringInput.find(strTruncatedDispatcherPrefix,i);     
        if (SearchPosition == i)
         {
          SearchPosition = stringInput.find("]:",i);
          if (SearchPosition != string::npos)
           {
            if (i > (stringInput.length()-1)) { SendCodingError("globalfunctions.cpp - substr ReformatTDDConversationToLog() c"); return "";}
            if (((SearchPosition - i) +2) < 0){ SendCodingError("globalfunctions.cpp - substr ReformatTDDConversationToLog() d"); return "";}
            strLinePrefix = stringInput.substr(i,((SearchPosition - i)+2));
            stringTemp += strLinePrefix; i+= strLinePrefix.length();
  //          else               {stringTemp += charCR;stringTemp += charLF;i+=1;} 
            break;
           }
         }
       // no prefix add indent
       stringTemp.append(4,' ');

      }// end if (j == 0)

     // start new line at <etx>, <CR><LF>, <CR>, <LF> (do not display)
     if      (stringInput[i]  == charETX)                               {stringTemp += strNewLine; i++;   break;}
     else if ((stringInput[i] == charCR)&&(stringInput[i+1] == charLF)) {i+= 2; break;}
     else if (stringInput[i]  == charCR)                                {i++;   break;}
     else if (stringInput[i]  == charLF)                                {i++;   break;}

     stringTemp += stringInput[i];    
     i++;j++;
     if (i == stringInput.length()) {return stringTemp;}
     
    }//end  while (j <= intDISPLAY_SPACES)
  }while (i < stringInput.length());

 return stringTemp;
}

/****************************************************************************************************
*
* Name:
*  Function: string ReformatALIOutputToLog(string stringInput, string stringCallData, string stringPrefix)
*
*
* Description:
*  This is a utility function
*
*
* Details:
*  The function converts an ALI text message into a readable format for logging.
*    
* Parameters:
*   stringInput                                  <cstring> Message Text
*   stringCallData				 <cstring> Call Data (trunk, pos, CallId, Callback)
*   stringPrefix				 <cstring> Time Stamp
*
* Variables:
*   LOG_BLANK_CALL_DATA_STRING		Global  - <defines.h>
*   charETX                             Global  - <globals.h>  <ETX> end of TX
*   charCR                              Global  - <globals.h>  <CR> carriage return
*   charLF                              Global  - <globals.h>  <LF> line feed
*   charSTX                             Global  - <globals.h>  <STX> start of TX
*   charCurrentChar                     Local   - character currently being processed
*   i                                   Local   - integer counter
*   intALI_RECORD_LINE_WRAP             Global  - <defines.h> width of ALI record display
*   j                                   Local   - integer counter
*   stringALI_LOG_SPACES                Global  - <globals.h> number of spaces to line up log
*   stringLineFeedSpaces                Local   - <cstring> used to offset for line feed <LF>
*   stringNEWLINEPREFIX                 Local   - <cstring> used to offset in log file
*   stringTemp                          Local   - <cstring> 
*                                                                          
* Functions:
*   .append()                           Library - <cstring>                     (*this)
*   .clear()                            Library - <cstring>                     (void)
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
string ReformatALIOutputToLog(string stringInput, string stringCallData, string stringPrefix, bool bool_LF_AS_CR)
{
 string stringTemp = "";
 
 string stringLineFeedSpaces="";
 string stringNEWLINEPREFIX = stringPrefix + LOG_BLANK_CALL_DATA_STRING;
 unsigned char   charCurrentChar = 0;
 int    i,j;


 i = 0;
 do
  {
   if (i != 0){stringTemp += charCR;stringTemp += charLF;}

   if ((!stringCallData.empty())&&(!stringPrefix.empty())) {stringTemp += stringNEWLINEPREFIX;}
   if (stringLineFeedSpaces != ""){stringTemp += stringLineFeedSpaces;}
   j = stringLineFeedSpaces.length()+1;
   stringLineFeedSpaces.clear(); 
   
   while (j <= intALI_RECORD_LINE_WRAP)
    {
     charCurrentChar = stringInput[i];
     
     if ((charCurrentChar == charCR) && (stringInput[i+1] == charLF))   {stringTemp+= "<CR><LF>"; i+=2; break;}
     else if ((charCurrentChar == charLF)&&(!bool_LF_AS_CR))            {stringTemp+= "<LF>";     stringLineFeedSpaces.append(j-1,' '); i++; break;}
     else if ((charCurrentChar == charLF)&&(bool_LF_AS_CR))             {stringTemp+= "<LF>";     i++;  break;}     
     else if (charCurrentChar == charCR)                                {stringTemp+= "<CR>";     i++;  break;}
     else if (charCurrentChar == charETX)                               {stringTemp+= "<ETX>";    i++;  break;}
     else if (charCurrentChar == charSTX)                               {stringTemp+= "<STX>";}
     else                                                               {stringTemp+= ASCII_Convert(charCurrentChar);}
     
     i++;j++;
    }//end  while (j < intALI_RECORD_LINE_WRAP)
 

  }while (charCurrentChar != charETX);

 return stringTemp;
}

/****************************************************************************************************
*
* Name:
*  Function: string ReformatALIOutputToHTML(string stringInput, bool bool_LF_AS_CR)
*
*
* Description:
*  This is a utility function
*
*
* Details:
*  The function converts an ALI text message into a HTML format.
*    
* Parameters:
*   stringInput                                  <cstring> Message Text
*   bool_LF_AS_CR                                <bool> 
*
* Variables:
*   charETX                             Global  - <globals.h>  <ETX> end of TX
*   charCR                              Global  - <globals.h>  <CR> carriage return
*   charLF                              Global  - <globals.h>  <LF> line feed
*   charSTX                             Global  - <globals.h>  <STX> start of TX
*   charCurrentChar                     Local   - character currently being processed
*   i                                   Local   - integer counter
*   intALI_RECORD_LINE_WRAP             Global  - <defines.h> width of ALI record display
*   j                                   Local   - integer counter
*   stringALI_LOG_SPACES                Global  - <globals.h> number of spaces to line up log
*   stringLineFeedSpaces                Local   - <cstring> used to offset for line feed <LF>
*   stringNEWLINEPREFIX                 Local   - <cstring> used to offset in log file
*   stringTemp                          Local   - <cstring> 
*                                                                          
* Functions:
*   .append()                           Library - <cstring>                     (*this)
*   .clear()                            Library - <cstring>                     (void)
*  
* Author(s):
* AUTHOR: 04/08/2020 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2020 WestTel Corporation 
****************************************************************************************************/
string ReformatALIOutputToHTML(string stringInput, bool bool_LF_AS_CR )
{
 string stringTemp = "";
 bool   boolLastchar = false;
 string stringLineFeedSpaces="";
// string stringNEWLINEPREFIX = stringPrefix + LOG_BLANK_CALL_DATA_STRING;
 unsigned char   charCurrentChar = 0;
 unsigned int    i,j;
 size_t found;

 found = stringInput.find(charETX);
 if (found == string::npos) {
  SendCodingError("globalfunctions.cpp - ReformatALIOutputToHTML() no ETX found");
  return "";
 }

 i = 0;
 do
  {
   if (stringLineFeedSpaces != ""){stringTemp += stringLineFeedSpaces;}
   j = stringLineFeedSpaces.length()+1;
   stringLineFeedSpaces.clear(); 
   
   while (j <= intALI_RECORD_LINE_WRAP)
    {
     charCurrentChar = stringInput[i];
     if (i == (stringInput.length()-1)) {boolLastchar = true;}
     if ((charCurrentChar == charCR) && (stringInput[i+1] == charLF))   {stringTemp+= "<br>"; i+=2; break;}
     else if ((charCurrentChar == charLF)&&(!bool_LF_AS_CR))            {stringTemp+= "<br>";     stringLineFeedSpaces.append(j-1,' '); i++; break;}
     else if ((charCurrentChar == charLF)&&(bool_LF_AS_CR))             {stringTemp+= "<br>";     i++;  break;}     
     else if (charCurrentChar == charCR)                                {stringTemp+= "<br>";     i++;  break;}
     else if (charCurrentChar == charETX)                               {i++;  break;}
     else if (charCurrentChar == charSTX)                               {stringTemp+= "<STX>";}
     else                                                               {stringTemp+= ASCII_Convert(charCurrentChar);}
     if (boolLastchar)                                                  {break;}
     i++;j++;
    }//end  while (j < intALI_RECORD_LINE_WRAP)
 
   if (boolLastchar) {break;}
  }while (charCurrentChar != charETX);

 return stringTemp;
}





string ConvertWhiteSpacestoHTML(string strInput) {

string          strReturn ="";
unsigned int    i = 0;
int             iwhitespace = 0;
bool            boolWhitespace = false;
bool            boolLastchar   = false;
string          strCharacter;
int             DividebyfourQ;
int             DividebyfourR;

while (i < strInput.length()) {
    if (i == (strInput.length() -1)) {boolLastchar = true;}
    strCharacter = strInput[i];
    if (strCharacter == " ") {iwhitespace++; boolWhitespace = true;}
    else                     {boolWhitespace = false;}

    if ((!boolWhitespace)||(boolLastchar)) {
     DividebyfourQ = iwhitespace/4;
     DividebyfourR = iwhitespace%4;
     for (int j=0; j < DividebyfourQ; j++) {strReturn += "&emsp;";}
     switch (DividebyfourR) {
      case 0:
        break;
      case 1:
        strReturn += "&nbsp;"; 
        break;
      case 2:
        strReturn += "&ensp;"; 
        break;
      case 3:
        strReturn += "&nbsp;"; strReturn += "&ensp;";      
        break;
      default:
        SendCodingError("globalfunctions.cpp ConvertWhiteSpacestoHTML() Need to retake math class!");
     }
     if (!boolWhitespace) {strReturn += strCharacter;}
     iwhitespace = 0;
    }   
    i++;
} 

return strReturn;
}

/****************************************************************************************************
*
* Name:
*  Function: int NTP_Time_Server_Control(bool TurnOn)
*
* Description:
*   A Utility Function (******************currently not used****************************************)
*
*
* Details:
*   This function turns on/off NTP time update service by executing the system start script
*   for the appropriate Linux version.
*          
*    
* Parameters:
*   TurnOn                                      - bool
*
* Variables:
*   command                             Local   - char* command to be sent to pipe
*   fpipe                               Local   - FILE
*   line[]				Local   - char array to hold results of command
*   LOG_CONSOLE_FILE			Local   - <defines.h>
*   LOG_WARNING				Global  - <defines.h>
*   NTP_SERVICE_STARTING_MESSAGE        Global  - <defines.h> message to be returned by system
*   NTP_SERVICE_STOPPING_MESSAGE        Global  - <defines.h> message to be returned by system
*   objMessage                          Local   - <MessageClass>
*   stringMessage                       Local   - <cstring> On/Off to be displayed in log message
*   string::npos			Library - <cstring> constant
*   stringNTP_SERVICE_START_SCRIPT      Global  - <globals.h> location of linux ntp start script
*   stringNTP_SERVICE_STOP_SCRIPT       Global  - <globals.h> location of linux ntp stop script
*   stringReturnMessage                 Local   - <cstring> conversion of line[] to string
*   stringSearch                        Local   - <cstring>
*   SYC					Global  - <header.h><threadorPorttype> enumeration member
*   SYC_MESSAGE_608                     Global  - <defines.h> error message
*   SYC_MESSAGE_613			Global  - <defines.h> error message
*   SYC_MESSAGE_614                     Global  - <defines.h> error message
*                                                                         
* Functions:
*   enQueue_Message()                   Global   - <globalfunctions.h>  (void)
*   fgets()				Library  - <stdio.h>            (char*)
*   .find_first_of()			Library  - <cstring>            (size_t)
*   .fMessage_Create()                  Global   - <MessageClass>       (void)
*   pclose()                            Library  - <stdio.h>            (int)    
*   popen()                             Library  - <stdio.h>            (FILE*)                        
*
*
* AUTHOR: 05/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
/*
int NTP_Time_Server_Control(threadorPorttype enumThread, bool TurnOn)
{
 string                 stringMessage;
 string                 stringReturnMessage;
 string                 stringSearch;
 MessageClass           objMessage;
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024];
 Port_Data              objThreadData;
 bool                   boolSuccess = false;

 objThreadData.fLoadThreadData(enumThread);

 if(TurnOn){stringMessage = "On";  stringSearch = NTP_SERVICE_STARTING_MESSAGE ;} 
 else      {stringMessage = "Off"; stringSearch = NTP_SERVICE_STOPPING_MESSAGE ;}
 

 if(TurnOn){ command = ((char*)stringNTP_SERVICE_START_SCRIPT.c_str());}
 else      { command = ((char*)stringNTP_SERVICE_STOP_SCRIPT.c_str());} 


 if ( !(fpipe = (FILE*)popen(command,"r")) )
  {  // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,613, objBLANK_CALL_RECORD, objThreadData, SYC_MESSAGE_613, stringMessage); 
   enQueue_Message(enumThread,objMessage);
   return 1;
  }


 while ( fgets( line, sizeof line, fpipe))
  {
   stringReturnMessage = line;
   if ((stringReturnMessage.find(stringSearch) != string::npos)){boolSuccess =true;}
  }
 
 pclose(fpipe);

 if (boolSuccess)
  {   
   boolNTP_Service_Running = TurnOn;
   objMessage.fMessage_Create(LOG_CONSOLE_FILE ,608, objBLANK_CALL_RECORD, objThreadData, SYC_MESSAGE_608, stringMessage);
   enQueue_Message(enumThread,objMessage);
   return 0;
  }
 else
  {
   objMessage.fMessage_Create(LOG_WARNING ,614, objBLANK_CALL_RECORD, objThreadData, SYC_MESSAGE_614, stringMessage, stringReturnMessage);
   enQueue_Message(enumThread,objMessage);
   return 1;
  }

return 0;       
}
*/
/****************************************************************************************************
*
* Name:
*  Function: void Abnormal_Exit(enum threadorPorttype  enumThread, int RetCode, string sMessage, 
*                               string sArg1, string sArg2, string sArg3, string sArg4, string sArg5)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function terminates the controller and saves error data in a temporary file
*   and displays the contents of the error to the terminal.
*          
*    
* Parameters:
*   enumThread                                  - <threadorPorttype> thread sending
*   sMessage					- <cstring> Message to display
*   RetCode					- int return code
*   
*
* Variables:
*   command                             Local   - char* command to be sent to pipe
*   fpipe                               Local   - FILE
*   line[]				Local   - char array to hold results of command
*   LOG_CONSOLE_FILE			Local   - <defines.h>
*   LOG_WARNING				Global  - <defines.h>
*   NTP_SERVICE_STARTING_MESSAGE        Global  - <defines.h> message to be returned by system
*   NTP_SERVICE_STOPPING_MESSAGE        Global  - <defines.h> message to be returned by system
*   objMessage                          Local   - <MessageClass>
*   stringMessage                       Local   - <cstring> On/Off to be displayed in log message
*   string::npos			Library - <cstring> constant
*   stringNTP_SERVICE_START_SCRIPT      Global  - <globals.h> location of linux ntp start script
*   stringNTP_SERVICE_STOP_SCRIPT       Global  - <globals.h> location of linux ntp stop script
*   stringReturnMessage                 Local   - <cstring> conversion of line[] to string
*   stringSearch                        Local   - <cstring>
*   SYC					Global  - <header.h><threadorPorttype> enumeration member
*   SYC_MESSAGE_608                     Global  - <defines.h> error message
*   SYC_MESSAGE_613			Global  - <defines.h> error message
*   SYC_MESSAGE_614                     Global  - <defines.h> error message
*                                                                         
* Functions:
*   enQueue_Message()                   Global   - <globalfunctions.h>  (void)
*   fgets()				Library  - <stdio.h>            (char*)
*   .find_first_of()			Library  - <cstring>            (size_t)
*   pclose()                            Library  - <stdio.h>            (int)    
*   popen()                             Library  - <stdio.h>            (FILE*)                        
*
*
* AUTHOR: 05/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void Abnormal_Exit(enum threadorPorttype  enumThread, int RetCode, string sMessage, string sArg1, string sArg2, string sArg3, string sArg4, string sArg5)
{
 MessageClass                                   objMessage; 
 struct timespec				timespecTimeNow;
 string						stringTimeStamp;
 string						stringMessageText;
 Port_Data					objPortData;
 FILE 						*pFile;
  
 // this will ensure the log thread is in a trapped state either from an intializing error or by trapping it in the pre-shutdown loop
 boolSTOP_LOG_THREAD = true;
  sleep(intTHREAD_SHUTDOWN_DELAY_SEC+1 );

 objPortData.fLoadPortData(enumThread, 0);
 objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData,
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, sMessage, sArg1, sArg2, sArg3, sArg4, sArg5);
 objMessage.fConsole();

 // create empty file
 pFile = fopen (charPREVIOUS_SHUT_DOWN_STATUS_FILE,"at+");
 
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 stringTimeStamp = get_time_stamp(timespecTimeNow);

 CSimpleIniA PreviousBoot(true, false, false);
 
 SI_Error rc = PreviousBoot.LoadFile(charPREVIOUS_SHUT_DOWN_STATUS_FILE);
 if (rc < 0){objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                                        objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "*Abnormal Exit: Unable to Load .err File");objMessage.fConsole(); }

 rc = PreviousBoot.SetValue("STATISTICS", "TimeStamp", stringTimeStamp.c_str());
 if (rc < 0){objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                                        objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "*Abnormal Exit: Unable to set TimeStamp in .err file");objMessage.fConsole(); }

 rc = PreviousBoot.SetValue("STATISTICS", "Thread", Thread_Calling(enumThread).erase(4).c_str());
 if (rc < 0){objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                                        objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "*Abnormal Exit: Unable to set Thread in .err file");objMessage.fConsole(); }

 rc = PreviousBoot.SetValue("STATISTICS", "LastErrorCode", int2str(RetCode).c_str());
 if (rc < 0){objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                                        objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "*Abnormal Exit: Unable to set Return Code in .err file");objMessage.fConsole(); }

 rc = PreviousBoot.SetValue("STATISTICS", "LastErrorMsg", objMessage.stringMessageText.c_str());
 if (rc < 0){objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                                        objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "*Abnormal Exit: Unable to set Error Message in .err file");objMessage.fConsole(); }


 rc = PreviousBoot.SaveFile(charPREVIOUS_SHUT_DOWN_STATUS_FILE);
 if (rc < 0){objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                                        objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "*Abnormal Exit: Unable to Save .err File");objMessage.fConsole();}

 exit(RetCode);



 return;
}//Abnormal_Exit(enum threadorPorttype  enumThread, int RetCode, string sMessag)

/****************************************************************************************************
*
* Name:
*  Function: string reformat_Ali_Log_to_Email(string stringArg)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function formats Log Data to be sent in an email. It locates the leading blanks in the log string and shortens them for email output.
*          
*    
* Parameters:
*   stringArg                                  - <cstring> Message to reformat
*   
*
* Variables:
*   intALI_EMAIL_SPACES			Global  - <defines.h> number of leading blanks for email conversion
*   objMessage                          Local   - <MessageClass>
*   position                            Local   - <size_t>
*   string::npos			Library - <cstring> constant
*   stringFind                          Local   - <cstring> location of linux ntp start script
*   stringALI_LOG_SPACES                Global  - <globals.h> string of blank spaces in ALI Data in log
*   stringReturnMessage                 Local   - <cstring> conversion of line[] to string
*   stringSearch                        Local   - <cstring> string to search for
*   stringReplace			Local   - <cstring> replacement string
*   stringOutPut                        Local   - <cstring> output
*
* Functions:
*   .append()                           Library  - <cstring>            (string& append)
*   .assign()                           Library  - <cstring>            (string& assign)
*   .clear()                            Library  - <cstring>            (void)
*   .find()                             Library  - <cstring>            (size_t)
*   .length()                           Library  - <cstring>            (size_t)    
*   FindandReplace()			Global   - <globalfunctions.h>  (string)                   
*
*
* AUTHOR: 05/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string reformat_Ali_Log_to_Email(string stringArg, int intPrefixLength)
{
 size_t position;
 string stringFind;
 string stringReplace;
 string stringOutPut;

 stringFind =  LOG_BLANK_CALL_DATA_STRING;
 //stringFind.append("|");
 stringReplace.assign(intALI_EMAIL_SPACES, ' ');
 stringReplace.append("|");

 stringOutPut = stringArg;
 stringOutPut.erase(0,(intPrefixLength + stringFind.length() - 2));
 do
  {
   position = stringOutPut.find(stringFind);
   if (position == string::npos){break;}

  // stringOutPut =  FindandReplace(stringOutPut, stringFind, stringReplace);
  stringOutPut.erase((position - intPrefixLength), (intPrefixLength + stringFind.length() -1));
  stringOutPut.insert((position - intPrefixLength), stringReplace);

  }while (position != string::npos);


return stringOutPut;
}

/****************************************************************************************************
*
* Name:
*  Function: bool CheckAllPortsDown(threadorPorttype enumArg, bool ShowMessage, bool ExitOnFirstSuccess)
*
* Description:
*   A startup Function 
*
*
* Details:
*   This function performs a ping check on all of the ports contained within the thread passed in. the return value
*          
*    
* Parameters:
*   enumArg                                 - <threadorPorttype> the thread containing the ports
*   ShowMessage				    - bool               show fail message after test failure
*   ExitOnFirstSuccess			    - bool               exit this function after 1st successful test
*
* Variables:
*   ALIPort[][2]                        Global  - <ExperientCommPort> 2d array (the ALI port pairs)
*   ANIPort[]                           Global  - <ExperientCommPort> the ANI Port(s)
*   boolAllPortsFail			Local   - ports considered failed till proven otherwise
*   CADPort[]                           Global  - <ExperientCommPort> the CAD Port(s)
*   .Gateway_IP                         Global  - <ServerData><IP_Address> object
*   HOST_IP				Global  - <IP_Address> Host Machine IP address
*   i					Local   - loop counter
*   intMAX_ALI_PORT_PAIRS		Global  - <defines.h>
*   intNUM_OF_SERVERS			Global  - <globals.h>
*   intPING_TIMEOUT			Global  - <globals.h>
*   intRC                               Local   - integer return code
*   KRN_MESSAGE_137			Global  - <defines.h> error message template constant char*
*   KRN_MESSAGE_138			Global  - <defines.h> error message template constant char*
*   KRN_MESSAGE_139			Global  - <defines.h> error message template constant char*
*   KRN_MESSAGE_140			Global  - <defines.h> error message template constant char*
*   KRN_MESSAGE_141			Global  - <defines.h> error message template constant char*
*   LOG_WARNING				Global  - <defines.h> log code constant integer
*   MAIN				Global  - <threadorPorttype> enumeration member
*   MAX_NUM_OF_SERVERS			Global  - <defines.h>
*   NUM_ANI_PORTS_MAX			Global  - <defines.h>
*   NUM_CAD_PORTS_MAX			Global  - <defines.h>
*   objMessage                          Local   - <MessageClass> object
*   PingPort                            Local   - <Ping> object used to ping the ports
*   .RemoteIPAddress			Global  - <ExperientCommPort><IP_Address> object
*   ServerStatusTable                   Global  - <ServerData> Contains Syncronization IP and port Data 
*   .stringAddress			Global  - <IP_Address> member
*   .Sync_IP 				Global  - <ServerData><IP_Address> object             
*                                                                          
* Functions:
*   .c_str()				Library  - <cstring>            (const char*)
*   .Config()                           Library  - <ipworks.h><Ping>    (int)
*   enQueue_Message()			Global   - <globalfunctions.h>  (void)
*   .fMessage_Create()                  Global   - <MessageClass>       (void)
*   .GetLastError()                     Library  - <ipworks.h><Ping>    (char*)
*   int2str()				Global   - <globalfunctions.h>  (string)
*   int2strLZ()				Global   - <globalfunctions.h>  (string)
*   .PingHost()                         Library  - <ipworks.h><Ping>    (int)
*   .SetLocalHost()                     Library  - <ipworks.h><Ping>    (int)
*   .SetPacketSize()                    Library  - <ipworks.h><Ping>    (int)                        
*   .SetTimeout()                       Library  - <ipworks.h><Ping>    (int)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation
****************************************************************************************************/
bool CheckAllPortsDown(threadorPorttype enumArg, bool ShowMessage, bool ExitOnFirstSuccess)
{
 extern ExperientCommPort	ANIPort[NUM_ANI_PORTS_MAX+1];
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort       CADPort[NUM_CAD_PORTS_MAX+1];
 extern IP_Address              HOST_IP;
 extern ServerData              ServerStatusTable[MAX_NUM_OF_SERVERS+1];
 MessageClass			objMessage;
 Ping   			PingPort;
 int    			intRC;
 bool				boolAllPortsFail = true;
 Port_Data                      objPortData;

#ifdef IPWORKS_V16
 PingPort.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 PingPort.SetLocalHost((char*)HOST_IP.stringAddress.c_str());
 PingPort.SetTimeout(intPING_TIMEOUT);
 PingPort.SetPacketSize(1024);
 PingPort.Config((char*)"AbsoluteTimeout=true");

 switch(enumArg)
  {
   case MAIN:
        for (int i = 1; i <= intNUM_OF_SERVERS; i++)
         {
          // ping eth0
          intRC = PingPort.PingHost((char*)ServerStatusTable[i].Gateway_IP.stringAddress.c_str());
          if (intRC)   {
           
            if (ShowMessage)   {

              objMessage.fMessage_Create(LOG_WARNING ,137, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                         objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_137, int2strLZ(i),
                                         ServerStatusTable[i].Gateway_IP.stringAddress, int2str(intRC), PingPort.GetLastError()); 
              enQueue_Message(MAIN,objMessage);
            } // endif ShowMessage
          } 
          else   {
          
           boolAllPortsFail = false; 
           if(ExitOnFirstSuccess) {return boolAllPortsFail;}
 
          } //end if else intrc

          // ping eth1
          intRC = PingPort.PingHost((char*)ServerStatusTable[i].Sync_IP.stringAddress.c_str());
          if (intRC)   {
           
            if (ShowMessage)   {

              objMessage.fMessage_Create(LOG_WARNING ,138, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, 
                                         objBLANK_ALI_DATA,KRN_MESSAGE_138, int2strLZ(i), ServerStatusTable[i].Sync_IP.stringAddress, int2str(intRC), PingPort.GetLastError() );
              enQueue_Message(MAIN,objMessage);

            } // endif ShowMessage
          }
          else  {
           
            boolAllPortsFail = false; 
            if(ExitOnFirstSuccess) {return boolAllPortsFail;} 

          } //end if else intrc


         }//end for (int i = 1; i <= intNUM_OF_SERVERS; i++)


        return boolAllPortsFail;
   case CAD:
        for (int i = 1; i <= intNUM_CAD_PORTS; i++)
         {
          intRC = PingPort.PingHost((char*)CADPort[i].RemoteIPAddress.stringAddress.c_str());
          if (intRC)   {
           
            objPortData.fClear(); objPortData.fLoadPortData(CAD,i);
            if (ShowMessage)   {

               objMessage.fMessage_Create(LOG_WARNING ,141, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                          objBLANK_ALI_DATA,KRN_MESSAGE_141, CADPort[i].RemoteIPAddress.stringAddress, int2str(intRC), PingPort.GetLastError() );
               enQueue_Message(MAIN,objMessage);

            } // endif ShowMessage
          }
          else   {
           
            boolAllPortsFail = false; 
            if(ExitOnFirstSuccess) {return boolAllPortsFail;} 

          } //end if else intrc


         } // end for (int i = 1; i <= intNUM_CAD_PORTS; i++)

        return boolAllPortsFail;
   case ALI:
        for(int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
         {
          intRC = PingPort.PingHost((char*)ALIPort[i] [1].RemoteIPAddress.stringAddress.c_str());
          if (intRC)   {
           
            objPortData.fClear(); objPortData.fLoadPortData(ALI,i,1);
            if (ShowMessage)   {

              objMessage.fMessage_Create(LOG_WARNING ,140, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                         objBLANK_ALI_DATA,KRN_MESSAGE_140, ALIPort[i] [1].RemoteIPAddress.stringAddress, int2str(intRC), PingPort.GetLastError() );
              enQueue_Message(MAIN,objMessage);

            }
          }
          else   {
           
            boolAllPortsFail = false; 
            if(ExitOnFirstSuccess) {return boolAllPortsFail;}
 
          }
    
          intRC = PingPort.PingHost((char*)ALIPort[i] [2].RemoteIPAddress.stringAddress.c_str());
          if (intRC)   {
           
            objPortData.fClear(); objPortData.fLoadPortData(ALI,i,2);
            if (ShowMessage)   {

              objMessage.fMessage_Create(LOG_WARNING ,140, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                         objBLANK_ALI_DATA,KRN_MESSAGE_140, ALIPort[i] [2].RemoteIPAddress.stringAddress, int2str(intRC), PingPort.GetLastError() ); 
              enQueue_Message(MAIN,objMessage);}
          }
          else   {
           
            boolAllPortsFail = false; 
            if(ExitOnFirstSuccess) {return boolAllPortsFail;} 
          }


         }// end for(int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)

        return boolAllPortsFail;
   case ANI:
        for (int i = 1; i <= intNUM_ANI_PORTS; i++)
         {
          intRC = PingPort.PingHost((char*)ANIPort[i].RemoteIPAddress.stringAddress.c_str());
          if (intRC)   {
           
            objPortData.fClear(); objPortData.fLoadPortData(ANI,i);
            if (ShowMessage)   {

              objMessage.fMessage_Create(LOG_WARNING ,139, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                         objBLANK_ALI_DATA,KRN_MESSAGE_139, ANIPort[i].RemoteIPAddress.stringAddress, int2str(intRC), PingPort.GetLastError() );
              enQueue_Message(MAIN,objMessage);

            }

          }
          else   {
           
            boolAllPortsFail = false; 
            if(ExitOnFirstSuccess) {return boolAllPortsFail;} 

          }

         }//for (int i = 1; i <= intNUM_ANI_PORTS; i++)

        return boolAllPortsFail;
   default: 
        SendCodingError("globalfunctions.cpp - Coding error in CheckAllPortsDown()");
        return false;
  }// end switch

}

/****************************************************************************************************
*
* Name:
*  Function: bool FileExists(string strFilename)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function checks if a file exists
*          
*    
* Parameters:
*   strFilename                                 - <string> the filename
*
* Variables:
*   blnReturn                           Local   - function return boolean
*   intStat                             Local   - file stat return value
*   stFileInfo                          Local   - <sys/stat.h><stat>
*                                                                          
* Functions:
*   .c_str()                            Library  - <cstring>            (const char*)
*   stat()				Library  - <sys/stat.h>         (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
bool FileExists(string strFilename) { 
  struct stat stFileInfo; 
  bool blnReturn; 
  int intStat; 

  // Attempt to get the file attributes 
  intStat = stat(strFilename.c_str(),&stFileInfo); 
  if(intStat == 0) { 
    // We were able to get the file attributes 
    // so the file obviously exists. 
    blnReturn = true; 
  } else { 
    // We were not able to get the file attributes. 
    // This may mean that we don't have permission to 
    // access the folder which contains this file. If you 
    // need to do that level of checking, lookup the 
    // return values of stat which will give you 
    // more details on why stat failed. 
    blnReturn = false; 
  } 
   
  return(blnReturn); 
}
/****************************************************************************************************
*
* Name:
*  Function: string StripLeadingZeroes(string stringArg)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function strips the leading zeroes off of a string of digits
*          
*    
* Parameters:
*   stringArg                                   - <string> the input string
*
* Variables:
*   charTemp                            Local   - <char *>  used for c function calls
*   i                                   Local   - <int>     counter
*   stringInteger                       Local   - <cstring> output string
*                                                                          
* Functions:
*   .c_str()                            Library  - <cstring>            (const char*)
*   .erase()                            Library  - <cstring>            (string)
*   isdigit()				Library  - <ctype.h>            (int)
*   .length()                           Library  - <cstring>            (size_t)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string StripLeadingZeroes(string stringArg)
{
 char*  charTemp;
 string stringInteger;

 if (stringArg.empty()) {return "";}

 charTemp = (char*) stringArg.c_str();
 
 for (unsigned int i = 0; i < stringArg.length(); i++) 
  {
   if (!(isdigit(charTemp[i])))     {break; } 
   stringInteger +=  charTemp[i];
  }

 for (unsigned int i = 0; i < stringInteger.length(); i++)
  {
   if (stringInteger[i] == '0'){stringInteger.erase(i,1);}
   else                        {break;}
  }

return stringInteger;
}
/****************************************************************************************************
*
* Name:
*  Function: string ReplaceNullCharacters(char* charString, size_t length)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function replaces all Null Characters in a <char *> string with (char) 255 so we can load the entire length
*   into a C++ <cstring> string.
*          
*    
* Parameters:
*   charString                                   - <char *> the input c string
*   length                                       - <size_t> length of the string
*
* Variables:
*   Newchar                             Local   - <char>    replacement character
*   Nullchar                            Local   - <char>    the Null character
*   i                                   Local   - <size_t>  counter
*   strOutput                           Local   - <cstring> return string
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string ReplaceNullCharacters(const char* charString, size_t length)
{
 string 		strOutput       = "";
 char 	Newchar 	= (char) 255;
 char 	Nullchar 	= (char) 0;

 for (size_t i = 0; i < length; i++)
  {
   if (charString[i] != Nullchar) {strOutput+=charString[i];}
   else                           {strOutput+=Newchar;} 
  }
 return strOutput;
}
/****************************************************************************************************
*
* Name:
*  Function: void CheckFreeDiskSpace(bool TerminalMode)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function checks how much diskspace remains and sends a warning if the diskspace is below a certain level
*   the function is invoked daily or can be called from the terminal menu.     
*    
* Parameters:
*   TerminalMode                               - <bool> true if function called from the terminal
*
* Variables:
*   A                                  Local   - <long double> temp var used for math
*   B                                  Local   - <long double> temp var used for math
*   BYTES_PER_GIGABYTE                 Local   - <const long double> number of bytes in a gigabyte
*   C                                  Local   - <long double> temp var used for math
*   charLOG_FILE_PATH_NAME             Global  - <defines.h> path and filename of the log file
*   D                                  Local   - <long double> temp var used for math
*   E                                  Local   - <long double> temp var used for math
*   errno                              Library - <errno.h><int>
*   .f_bavail                          Local   - <struct statvfs> <long double> member
*   .f_blocks                          Local   - <struct statvfs> <long double> member
*   .f_bfree                           Local   - <struct statvfs> <long double> member 
*   .f_bsize                           Local   - <struct statvfs> <long double> member
*   fiData                             Local   - <sys/statvfs.h><statvfs>    file system data
*   intAVAIL_DISK_SPACE_WARN_GIG       Global  - <defines.h> threshold in gigabytes to trigger a warning
*   KRN_MESSAGE_164                    Global  - <defines.h> error message
*   KRN_MESSAGE_165                    Global  - <defines.h> error message
*   ldGigAvailable                     Local   - <long double> calculated gigabytes disk space available
*   ldGigCapacity                      Local   - <long double> disk capacity
*   LOG_WARNING                        Global  - <defines.h> log code
*   MAIN                               Global  - <header.h><threadorPorttype> enumeration member
*   objBLANK_CALL_RECORD               Global  - <Call_Data> Blank Clall Data object
*   objBLANK_KRN_PORT                  Global  - <Port_Data> Blank Main Port Data object
*   objMessage                         Local   - <MessageClass> object
*   ossAvail                           Local   - <cstring><ostringstream>    available diskspace
*   ossCapacity                        Local   - <cstring><ostringstream>    disk capacity
*   Nullchar                           Local   - <char>    the Null character
*   intRC                              Local   - <int>                       return code
*   strOutput                          Local   - <cstring> return string
*                                                                          
* Functions:
*   int2strLZ()                        Global   - <globalfunctions.h>              (string)
*   setprecision()                     Library  - <iostream>                       (Unspecified. This function should only be used as a stream manipulator.)
*   statvfs()                          Library  - <sys/statvfs.h>                  (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/  
void CheckFreeDiskSpace(bool TerminalMode)
{
 ostringstream 	        ossAvail, ossCapacity;
 struct statvfs 	fiData;
 int            	intRC;
 long double            A,B,C,D,E;
 long double            ldGigAvailable;
 long double            ldGigCapacity;
 MessageClass           objMessage;
 const long double      BYTES_PER_GIGABYTE = 1073741824.0;
          
 intRC = statvfs(charLOG_FILE_PATH_NAME, &fiData);
 if (intRC)   {
  
   objMessage.fMessage_Create(LOG_WARNING,164, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,KRN_MESSAGE_164, int2strLZ(errno)); 
   enQueue_Message(MAIN,objMessage);
   return;

 }

 A= fiData.f_bavail;
 B= fiData.f_bsize;
 C= fiData.f_blocks;
 D= A*B;
 E= C*B;

 ldGigAvailable = D/BYTES_PER_GIGABYTE;
 ldGigCapacity  = E/BYTES_PER_GIGABYTE;

 if( D < (intAVAIL_DISK_SPACE_WARN_GIG *  BYTES_PER_GIGABYTE))   {
  
   ossAvail << fixed << setprecision(3) << ldGigAvailable;
   ossCapacity << fixed << setprecision(3) << ldGigCapacity;
   objMessage.fMessage_Create(LOG_WARNING,165, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_KRN_PORT, 
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_165, int2str(intAVAIL_DISK_SPACE_WARN_GIG), ossAvail.str(), ossCapacity.str() ); 
   enQueue_Message(MAIN,objMessage);

 }

 if (TerminalMode) {
                    cout << endl;
                    cout << "Available Disk Space = " << fixed << setprecision(3) << ldGigAvailable << " Gigabytes" << endl;
                    cout << "Disk Capacity        = " << fixed << setprecision(3) << ldGigCapacity  << " Gigabytes" << endl; 
                    if (boolDEBUG)   {
                     
                      cout << "File system block size = " <<  fiData.f_frsize << endl;
                      cout << "Number of blocks       = " <<  fiData.f_blocks << endl;
                      cout << "Number of free blocks  = " <<  fiData.f_bfree << endl;
                      cout << "Number of avail blocks = " <<  fiData.f_bavail << endl;
                    }
                    cout << endl;
 }


 return;

}
/****************************************************************************************************
*
* Name:
*  Function: void Display_Controller_Version()
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function displays the version number of the controller to the terminal
*          
*    
* Parameters:
*   none                               
*
* Variables:
*   stringPRODUCT_NAME                  Global  - <cstring>    PSAP Controller name
*   stringVERSION_NUMBER                Global  - <cstring>    the version number
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void Display_Controller_Version()
{
 cout << endl;
 cout << setw(35) << left << stringPRODUCT_NAME + " Version " << stringVERSION_NUMBER << endl;
 cout << setw(35) << left <<"Config File Version " << strCONFIG_FILE_VERSION << endl;
 cout << setw(35) << left <<"AGI Script Version " << strAGI_VERSION_NUMBER << endl;
 cout << setw(35) << left <<"Workstation XML Specification " << chWORKSTATION_XML_SPECIFICATION << endl << endl;;
}
/****************************************************************************************************
*
* Name:
*  Function: string CallStatusString(int intStatusCode)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function converts a call status integer into it's corresponding message string. If the code
*   does not exist "UNKNOWN" is returned.
*          
*    
* Parameters:
*   intStatusCode                       <int>                               
*
* Variables:
*   XML_STATUS_MSG_CONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_DISCONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_ON_HOLD                   Global - <defines.h>
*   XML_STATUS_MSG_OFF_HOLD_CONNECTED        Global - <defines.h>
*   XML_STATUS_MSG_ABANDONED                 Global - <defines.h>
*   XML_STATUS_MSG_RINGING                   Global - <defines.h>
*   XML_STATUS_MSG_TDD_MUTE_CHANGE           Global - <defines.h>
*   XML_STATUS_MSG_TDD_CALLER_SAYS           Global - <defines.h>
*   XML_STATUS_MSG_TDD_DISPATCHER_SAYS       Global - <defines.h>
*   XML_STATUS_MSG_TDD_CHAR_RECEIVED         Global - <defines.h>
*   XML_STATUS_MSG_ALI_MANUAL_BID            Global - <defines.h>
*   XML_STATUS_MSG_MAN_ALI_TIMEOUT           Global - <defines.h>
*   XML_STATUS_MSG_AUTO_ALI_REBID            Global - <defines.h>
*   XML_STATUS_MSG_AUTO_ALI_TIMEOUT          Global - <defines.h>
*   XML_STATUS_MSG_ALI_REBID                 Global - <defines.h>
*   XML_STATUS_MSG_ALI_TIMEOUT               Global - <defines.h>
*   XML_STATUS_MSG_ALI_RECEIVED              Global - <defines.h>
*   XML_STATUS_MSG_MAN_ALI_RECEIVED          Global - <defines.h>
*   XML_STATUS_MSG_AUTO_ALI_RECEIVED         Global - <defines.h>
*   XML_STATUS_MSG_ALI_DONT_UPDATE_STATUS    Global - <defines.h>
*   XML_STATUS_MSG_SEND_TO_CAD               Global - <defines.h>
*   XML_STATUS_MSG_CONFERENCEJOIN            Global - <defines.h>
*   XML_STATUS_MSG_CONFERENCELEAVE           Global - <defines.h>
*                                                                          
* Functions:
*   none
*
* AUTHOR: 1/8/2009 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2009 Experient Corporation
****************************************************************************************************/
string CallStatusString(int intStatusCode)
{
 switch (intStatusCode)
  {
   case 1:  return XML_STATUS_MSG_CONNECT;
   case 2:  return XML_STATUS_MSG_DISCONNECT;
   case 5:  return XML_STATUS_MSG_ON_HOLD; 
   case 6:  return XML_STATUS_MSG_OFF_HOLD_CONNECTED;
   case 7:  return XML_STATUS_MSG_ABANDONED;
   case 11: return XML_STATUS_MSG_RINGING;

   case 78: return XML_STATUS_MSG_SMS_TEXT;
   case 83: return XML_STATUS_MSG_TDD_MUTE_CHANGE;
   case 84: return XML_STATUS_MSG_TDD_CALLER_SAYS;
   case 85: return XML_STATUS_MSG_TDD_DISPATCHER_SAYS;
   case 86: return XML_STATUS_MSG_TDD_CHAR_RECEIVED;

   case 87: return XML_STATUS_MSG_ALI_MANUAL_BID;
   case 88: return XML_STATUS_MSG_MAN_ALI_TIMEOUT;
   case 89: return XML_STATUS_MSG_AUTO_ALI_REBID;
   case 90: return XML_STATUS_MSG_AUTO_ALI_TIMEOUT;
   case 91: return XML_STATUS_MSG_ALI_REBID;
   case 92: return XML_STATUS_MSG_ALI_TIMEOUT;
   case 93: return XML_STATUS_MSG_ALI_RECEIVED;
   case 94: return XML_STATUS_MSG_MAN_ALI_RECEIVED;
   case 95: return XML_STATUS_MSG_AUTO_ALI_RECEIVED;
   case 96: return XML_STATUS_MSG_ALI_DONT_UPDATE_STATUS;



   case 97: return XML_STATUS_MSG_SEND_TO_CAD;
   case 98: return XML_STATUS_MSG_CONFERENCEJOIN;
   case 99: return XML_STATUS_MSG_CONFERENCELEAVE;
   default:
            return "UNKNOWN";

  }
 return "";
}
/****************************************************************************************************
*
* Name:
*  Function: int CallStatusCode(string stringArg)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function converts a call status string into it's corresponding integer. If the code
*   does not exist 0 is returned.
*          
* note: many of the strings are identical in the definitions file.  For example
*       the call may be  XML_STATUS_MSG_TDD_DISPATCHER_SAYS = "Connected"
*       as well as       XML_STATUS_MSG_CONNECT = "Connected"
*       this should not impact the ability to determine the status of the call
*       each status message was added in case the strings change in the future ....
* 
* Parameters:
*   stringArg                                <cstring>                               
*
* Variables:
*   XML_STATUS_MSG_CONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_DISCONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_ON_HOLD                   Global - <defines.h>
*   XML_STATUS_MSG_OFF_HOLD_CONNECTED        Global - <defines.h>
*   XML_STATUS_MSG_ABANDONED                 Global - <defines.h>
*   XML_STATUS_MSG_RINGING                   Global - <defines.h>
*   XML_STATUS_MSG_TDD_MUTE_CHANGE           Global - <defines.h>
*   XML_STATUS_MSG_TDD_CALLER_SAYS           Global - <defines.h>
*   XML_STATUS_MSG_TDD_DISPATCHER_SAYS       Global - <defines.h>
*   XML_STATUS_MSG_TDD_CHAR_RECEIVED         Global - <defines.h>
*   XML_STATUS_MSG_ALI_MANUAL_BID            Global - <defines.h>
*   XML_STATUS_MSG_MAN_ALI_TIMEOUT           Global - <defines.h>
*   XML_STATUS_MSG_AUTO_ALI_REBID            Global - <defines.h>
*   XML_STATUS_MSG_AUTO_ALI_TIMEOUT          Global - <defines.h>
*   XML_STATUS_MSG_ALI_REBID                 Global - <defines.h>
*   XML_STATUS_MSG_ALI_TIMEOUT               Global - <defines.h>
*   XML_STATUS_MSG_ALI_RECEIVED              Global - <defines.h>
*   XML_STATUS_MSG_MAN_ALI_RECEIVED          Global - <defines.h>
*   XML_STATUS_MSG_AUTO_ALI_RECEIVED         Global - <defines.h>
*   XML_STATUS_MSG_ALI_DONT_UPDATE_STATUS    Global - <defines.h>
*   XML_STATUS_MSG_SEND_TO_CAD               Global - <defines.h>
*   XML_STATUS_MSG_CONFERENCEJOIN            Global - <defines.h>
*   XML_STATUS_MSG_CONFERENCELEAVE           Global - <defines.h>
*                                                                          
* Functions:
*   none
*
* AUTHOR: 01/8/2009 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2009 Experient Corporation
****************************************************************************************************/
int CallStatusCode(string stringArg)
{
 if      (stringArg == XML_STATUS_MSG_CONNECT)                {return  1;}
 else if (stringArg == XML_STATUS_MSG_DISCONNECT)             {return  2;} 
 else if (stringArg == XML_STATUS_MSG_ON_HOLD)                {return  5;}
 else if (stringArg == XML_STATUS_MSG_OFF_HOLD_CONNECTED)     {return  6;}
 else if (stringArg == XML_STATUS_MSG_ABANDONED)              {return  7;}
 else if (stringArg == XML_STATUS_MSG_RINGING)                {return 11;}
 else if (stringArg == XML_STATUS_MSG_DIALING)                {return 11;}
 else if (stringArg == XML_STATUS_MSG_TDD_MUTE_CHANGE)        {return 83;}
 else if (stringArg == XML_STATUS_MSG_SMS_TEXT)               {return 78;}
 else if (stringArg == XML_STATUS_MSG_TDD_CALLER_SAYS)        {return 84;}
 else if (stringArg == XML_STATUS_MSG_TDD_DISPATCHER_SAYS)    {return 85;}
 else if (stringArg == XML_STATUS_MSG_TDD_CHAR_RECEIVED)      {return 86;}
 else if (stringArg == XML_STATUS_MSG_ALI_MANUAL_BID)         {return 87;}
 else if (stringArg == XML_STATUS_MSG_MAN_ALI_TIMEOUT)        {return 88;}
 else if (stringArg == XML_STATUS_MSG_AUTO_ALI_REBID)         {return 89;}
 else if (stringArg == XML_STATUS_MSG_AUTO_ALI_TIMEOUT)       {return 90;}
 else if (stringArg == XML_STATUS_MSG_ALI_REBID)              {return 91;}
 else if (stringArg == XML_STATUS_MSG_ALI_TIMEOUT)            {return 92;}
 else if (stringArg == XML_STATUS_MSG_ALI_RECEIVED)           {return 93;}
 else if (stringArg == XML_STATUS_MSG_MAN_ALI_RECEIVED)       {return 94;}
 else if (stringArg == XML_STATUS_MSG_AUTO_ALI_RECEIVED)      {return 95;}
 else if (stringArg == XML_STATUS_MSG_ALI_DONT_UPDATE_STATUS) {return 96;}
 else if (stringArg == XML_STATUS_MSG_SEND_TO_CAD)            {return 97;}
 else if (stringArg == XML_STATUS_MSG_CONFERENCEJOIN)         {return 98;}
 else if (stringArg == XML_STATUS_MSG_CONFERENCELEAVE)        {return 99;}
 else                                                         {return  0;}
}
/****************************************************************************************************
*
* Name:
*  Function: int CallStateCode(string stringArg)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function converts a call state string into it's corresponding integer. If the code
*   does not exist 0 is returned.
*          
*    
* Parameters:
*   stringArg                                <cstring>                               
*
* Variables:
*   XML_STATUS_MSG_DISCONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_CONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_ABANDONED                 Global - <defines.h>
*   XML_STATUS_MSG_RINGING                   Global - <defines.h>
*   XML_STATUS_MSG_ALI_MANUAL_BID            Global - <defines.h>
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
int CallStateCode(string stringArg)
{
 if      (stringArg == XML_STATUS_MSG_RINGING)                {return 1;}
 else if (stringArg == XML_STATUS_MSG_CONNECT)                {return 2;}
 else if (stringArg == XML_STATUS_MSG_DISCONNECT)             {return 3;} 
 else if (stringArg == XML_STATUS_MSG_ABANDONED)              {return 4;}
 else if (stringArg == XML_CALL_STATE_MANUAL_BID)             {return 5;}
 else if (stringArg == XML_STATUS_MSG_DIALING)                {return 6;}
 else if (stringArg == XML_STATUS_MSG_ON_HOLD)                {return 7;}
 else if (stringArg == XML_STATUS_MSG_ABANDONED_CLEARED)      {return 8;}
 else if (stringArg == XML_STATUS_MSG_CANCELED)               {return 9;}
 else if (stringArg == XML_STATUS_MSG_CANCELED_BUSY)          {return 10;}
 else if (stringArg == XML_STATUS_MSG_SMS_TEXT)               {return 2;}
 else                                                         {return 0;}
}
/****************************************************************************************************
*
* Name:
*  Function: string CallStateString(int intStateCode)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function converts a call state code into it's corresponding string. If the code
*   does not exist "UNKNOWN" is returned.
*          
*    
* Parameters:
*   intStateCode                                <int>                               
*
* Variables:
*   XML_STATUS_MSG_DISCONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_CONNECT		     Global - <defines.h>
*   XML_STATUS_MSG_ABANDONED                 Global - <defines.h>
*   XML_STATUS_MSG_RINGING                   Global - <defines.h>
*   XML_STATUS_MSG_ALI_MANUAL_BID            Global - <defines.h>
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string CallStateString(int intStateCode)
{
 switch(intStateCode)
  {
   case 1:  return XML_STATUS_MSG_RINGING;
   case 2:  return XML_STATUS_MSG_CONNECT;
   case 3:  return XML_STATUS_MSG_DISCONNECT;
   case 4:  return XML_STATUS_MSG_ABANDONED;
   case 5:  return XML_CALL_STATE_MANUAL_BID;
   case 6:  return XML_STATUS_MSG_DIALING;
   case 7:  return XML_STATUS_MSG_ON_HOLD;
   case 8:  return XML_STATUS_MSG_ABANDONED_CLEARED;
   case 9:  return XML_STATUS_MSG_CANCELED;
   case 10: return XML_STATUS_MSG_CANCELED_BUSY;

 //  case 11: return XML_STATUS_MSG_SMS_TEXT;
   default:
           return "UNKNOWN";
  }

}
/****************************************************************************************************
*
* Name:
*  Function: int ConvertCallStateToStatus(int intStateCode)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function converts a call state code into a call Status Code. If the code
*   does not exist 0 is returned.
*          
*    
* Parameters:
*   intStateCode                                <int>                               
*
* Variables:
*  none
*                                                                          
* Functions:
*   none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
int ConvertCallStateToStatus(int intStateCode)
{
 switch(intStateCode)
  {
   case 1:  return 11;
   case 2:  return 1;
   case 3:  return 2;
   case 4:  return 7;
   case 5:  return 94;
   case 6:  return 11;
   case 7:  return 5;
   case 8:  return 5;
   case 9:  return 5;
   case 10: return 5;
   case 11: return 2;
   default:
           return 0;
  }
}


/****************************************************************************************************
*
* Name:
*  Function: int GetDayofMonth()
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function returns the current day of the month
*          
*    
* Parameters:
*   none                                                           
*
* Variables:
*  charDay                            Local - char array
*  time_tTimeStamp                    Local - <ctime><time_t>
*  tmLocalTimeStamp                   Local - <ctime><struct tm>
*                                                                        
* Functions:
*   char2int()                        Global  - <globalfunctions.h>        (int)
*   time()                            Library - <ctime>                    (time_t)
*   local_time_r()                    Library - <ctime>                    (struct tm)
*   strftime()                        Library - <ctime>                    (size_t)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
int GetDayofMonth()
{
 time_t		        time_tTimeStamp;
 struct tm		tmLocalTimeStamp;
 char                   charDay[3];
 
 time_tTimeStamp = time(NULL); 
 localtime_r(&time_tTimeStamp,&tmLocalTimeStamp);
 strftime(charDay, sizeof(charDay), "%d", &tmLocalTimeStamp); 
 return char2int(charDay);
}// GetDayofMonth()
/****************************************************************************************************
*
* Name:
*  Function: int GetHour()
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function returns the current Hour in 24h format (0-23)
*          
*    
* Parameters:
*   none                                                           
*
* Variables:
*  charHour                           Local - char array
*  time_tTimeStamp                    Local - <ctime><time_t>
*  tmLocalTimeStamp                   Local - <ctime><struct tm>
*                                                                        
* Functions:
*   char2int()                        Global  - <globalfunctions.h>        (int)
*   time()                            Library - <ctime>                    (time_t)
*   local_time_r()                    Library - <ctime>                    (struct tm)
*   strftime()                        Library - <ctime>                    (size_t)
*
* AUTHOR: 12/12/2014 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2014 Experient Corporation
****************************************************************************************************/
int GetHour()
{
 time_t		        time_tTimeStamp;
 struct tm		tmLocalTimeStamp;
 char                   charHour[3];
 
 time_tTimeStamp = time(NULL); 
 localtime_r(&time_tTimeStamp,&tmLocalTimeStamp);
 strftime(charHour, sizeof(charHour), "%H", &tmLocalTimeStamp); 
 return char2int(charHour);

}
/****************************************************************************************************
*
* Name:
*  Function: string ParseFreeswitchData(string stringArg, string strKey)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function parses asterisk AMI data for the key passed in
*   format is     key: DATA<charTAB>
*   the Data is extracted any leading spaces are removed and the string
*   of Data is returned.  Any failure returns the empty string.    
*    
* Parameters:
*   stringArg                                 <cstring> the string to be parsed
*   strKey                                    <cstring> the key to look for                                                           
*
* Variables:
*  charTAB                            Global  - <globals.h>       ASCII TAB character
*  EndPosition                        Local   - <cstring><size_t> The end position of the Data
*  StartPosition                      Local   - <cstring><size_t> The starting position of the Key/Data
*  strOutput                          Local   - <cstring>         The Data found
*  string::npos                       Library - <cstring>         Used to indicate 'not found'
*                                                              
* Functions:
*   .find()                           Library  - <cstring>                 (size_t)
*   .length()                         Library  - <cstring>                 (size_t)
*
* AUTHOR: 12/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string ParseFreeswitchData(string stringArg, string strKey)
{
 size_t StartPosition;
 size_t EndPosition;
 string strOutput = "";

 StartPosition = stringArg.find(strKey);
 if (StartPosition == string::npos)                   {return "";}
 StartPosition = stringArg.find(":", StartPosition);
 if (StartPosition == string::npos)                   {return "";}
 EndPosition = stringArg.find(charTAB, StartPosition);
 if (EndPosition == string::npos)                     {return "";}
 strOutput.assign(stringArg,StartPosition+1,(EndPosition - StartPosition - 1));
 //remove leading blanks ...
 strOutput= RemoveLeadingSpaces(strOutput);
 return strOutput;
}
/****************************************************************************************************
*
* Name:
*  Function: string ParseAsteriskMeetmeListData(string stringArg, string strKey)
*
* Description:
*   A Utility Function for Parsing Asterisk 1.4.22 AMI MeetmeCount Return Data and returning
*   the number of users as a string.
*
*
* Details:
*   the AMI Data to Parse:
*
*   Response: Follows<TAB>
*   Privelege: Command<TAB>
*   ActionID: 12345678.0<TAB>
*   User #: 02 303-818-3060 911                Channel: SIP/audiocodes-2-08b6f820 (unmonitored) 00:02:55<LF>
*   1 users in that conference.<LF>
*   --END COMMAND--<TAB><ETX>
*
* or
*   Response: Follows<TAB>
*   Privelege: Command<TAB>
*   ActionID: 12345678.0<TAB>
*   No active conferences.<LF>
*   --END COMMAND--<TAB><ETX>
*
*   This function parses the asterisk AMI data for the key (\n) passed in
*   format is     keyDATA<space>
*   the Data is extracted any leading spaces are removed and the string
*   of Data is returned.  Any failure returns the empty string.    
*    
* Parameters:
*   stringArg                                 <cstring> the string to be parsed
*   strKey                                    <cstring> the key to look for                                                           
*
* Variables:
*  EndPosition                        Local   - <cstring><size_t> The end position of the Data
*  StartPosition                      Local   - <cstring><size_t> The starting position of the Key/Data
*  strOutput                          Local   - <cstring>         The Data found
*  string::npos                       Library - <cstring>         Used to indicate 'not found'
*                                                              
* Functions:
*   .find()                           Library  - <cstring>                 (size_t)
*   .length()                         Library  - <cstring>                 (size_t)
*
* AUTHOR: 12/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string ParseAsteriskMeetmeListData(string stringArg, string strKey)
{
 size_t StartPosition;
 size_t EndPosition;
 string strOutput;

 StartPosition = stringArg.find(strKey);
 if (StartPosition == string::npos)                   {return "";}
 StartPosition+=strKey.length();
 EndPosition = stringArg.find(" ", StartPosition);
 if (EndPosition == string::npos)                     {return "";}
 strOutput.assign(stringArg,StartPosition,(EndPosition - StartPosition ));
 //remove leading blanks ...
 strOutput= RemoveLeadingSpaces(strOutput);
 return strOutput;
}

bool IsZombieChannel(string strInput)
{
 size_t SearchPosition;

 SearchPosition = strInput.find("<ZOMBIE>");
 if (SearchPosition != string::npos) {return true;}
 else                                {return false;} 
}

bool IsMASQChannel(string strInput)
{
 size_t SearchPosition;

 SearchPosition = strInput.find("<MASQ>");
 if (SearchPosition != string::npos) {return true;}
 else                                {return false;} 
}


bool IsCloneChannel(string strInput)
{
 size_t SearchPosition;

 SearchPosition = strInput.find(ASTERISK_ASYNC_CLONE_PREFIX);
 if (SearchPosition != string::npos) {return true;}
 SearchPosition = strInput.find(ASTERISK_BRIDGE_CLONE_PREFIX); 
 if (SearchPosition != string::npos) {return true;}
 else                                {return false;} 
}
/*
bool IsPositionChannel(string strInput)
{
 return (!AsteriskPositionFromChannel(strInput).empty());
}
*/
int DecodeAsteriskActionId(string strInput)
{
 size_t SearchPosition;
 string strNumber;

 SearchPosition = strInput.find('-',0);
 if (SearchPosition == string::npos){return 0;}

 strNumber = strInput.substr(0,SearchPosition);
 if(!Validate_Integer(strNumber)){return 0;}
 else                            {return char2int(strNumber.c_str());}
}

string CreateAbandonedList()
{
 // MainWorktable should be semaphore protected before calling .....
  extern vector <ExperientDataClass>             vMainWorkTable; 
  size_t sz = vMainWorkTable.size();
  string strReturn = "";
  for (unsigned int i = 0; i < sz; i++)
   {
    if (vMainWorkTable[i].boolAbandoned) 
     {
      strReturn += charTAB;
      strReturn += ANI_CONF_MEMBER_CALLER_PREFIX;
      strReturn += int2str(vMainWorkTable[i].CallData.intTrunk);
      strReturn += charTAB;
     }

   }

 return strReturn;
}




string EraseLocalTransferChannelSuffix(string strArg)
{
 size_t FoundInString;

 FoundInString = strArg.find("Local/1");
 if (FoundInString == string::npos) {return strArg;}
 FoundInString = strArg.find(",");
 if (FoundInString == string::npos) {return strArg;}
 strArg.erase(FoundInString, string::npos);

 return strArg;
}   

bool IS_ABANDONED_POPUP(string strInput)
{
 size_t found;

 found = strInput.find("Alarm Type=\"Popup\"");
 if (found == string::npos) {return false;}

 found = strInput.find("Abandoned Call");
 if (found == string::npos) {return false;}

 return true;
}

bool IS_PSAP_STATUS( string strInput)
{
 size_t found;

 found = strInput.find(XML_NODE_PSAP_APPLET_DATA);
 if (found == string::npos) {return false;}

 return true;

}

transfer_method TransferMethod(string strInput) {

  if (strInput == "Blank") 			{return mBLANK;}
  if (strInput == "GUI TRANSFER") 		{return mGUI_TRANSFER;}
  if (strInput == "BLIND TRANSFER") 		{return mBLIND_TRANSFER;}   
  if (strInput == "ATTENDED TRANSFER") 		{return mATTENDED_TRANSFER;}
  if (strInput == "POOR MAN BLIND TRANSFER") 	{return mPOORMAN_BLIND_TRANSFER;}
  if (strInput == "RING BACK") 			{return mRING_BACK;}
  if (strInput == "TANDEM") 			{return mTANDEM;}
  if (strInput == "NG911 SIP TRANSFER") 	{return mNG911_TRANSFER;}
  if (strInput == "FLASH HOOK") 		{return mFLASHOOK;}
  if (strInput == "INDIGITAL") 			{return mINDGITAL;}
  if (strInput == "INTRADO") 			{return mINTRADO;}
  if (strInput == "REFER") 			{return mREFER;}

 return mBLANK;
}


string ParseConferencefromAsteriskLocalChannel(string stringInput)
{
 size_t StartPosition, EndPosition;
 string strKey;
 string strReturn;
 
 strKey = ASTERISK_GUI_XFER_CONF_KEY;
 StartPosition = stringInput.find(strKey);
 if (StartPosition == string::npos) {return "";}
 EndPosition =  stringInput.find("@", StartPosition );
 if (EndPosition == string::npos) {return "";}
 StartPosition+= strKey.length();
 if (StartPosition >= (stringInput.length()-1)) {SendCodingError("globalfunctions.cpp - substr ParseConferencefromAsteriskLocalChannel() a" ); return "";}
 if ((EndPosition-StartPosition) < 0)           {SendCodingError("globalfunctions.cpp - substr ParseConferencefromAsteriskLocalChannel() b" ); return "";}

 strReturn = stringInput.substr(StartPosition, (EndPosition-StartPosition));
 if (strReturn.length() != 3) {return "";}
 else                         {return strReturn;}
}


bool IsGUITransferLinkChannelOne(string strInput)
{
 size_t found;
 string strConference;

 // GUI Transfer link has the Format of Local/XXX@abcdefg where XXX is a
 // conference number.  

 // first we will make sure it is not a Position channel which has 
 // Local/10XX.Y@ext10XX or
 found = strInput.find(ASTERISK_AMI_LOCAL_POSITION_SUFFIX_911);
 if (found != string::npos)    {return false;}

 found = strInput.find(ASTERISK_GUI_XFER_CHANNEL1_KEY_PREFIX);
 if (found == string::npos)    {return false;}

 found = strInput.find(ASTERISK_GUI_XFER_CHANNEL1_KEY_SUFFIX);
 if (found == string::npos)    {return false;}

 strConference = ParseConferencefromAsteriskLocalChannel(strInput);

 if (strConference.empty()) {return false;}
 
 found = strConference.find_first_not_of("0123456789");
 if (found != string::npos)    {return false;}

 return true;
}


string AMITestScript(string strInput)
{
 string strOutput = strInput;
 string strTimeStamp =  "[" + get_Local_Time_Stamp() + "]";
 string strCRLF = "\\r\\n";
 string strTAB = "\t";
 string strETX;
 string strDoubleCRLF = strCRLF+strCRLF;

 strETX += charETX;
 strTimeStamp += charCR;
 strTimeStamp += charLF;
 
 do
  {
   strInput = strOutput;
   strOutput = FindandReplace(strInput, strTAB, strCRLF);
  } while (strInput != strOutput);

 strOutput = FindandReplace(strOutput, strETX, strDoubleCRLF);
 
 return strTimeStamp + strOutput;
}



/*
string ParseAsteriskSIPpositionChannel(string stringInput)
{
 // This function Parses an Asterisk Channel to decode Position Numbers
 // Position Numbers are encoded in the following ways
 // SIP/CO999x1002.3-02eb90c0 or SIP/CO999x1002-02eb90c0 where position number is  02
 // or
 // SIP/CO999x9002.3-02eb90c0 or SIP/CO999x9002-02eb90c0 where position number is  02
 // or
 //   Local/1002.1@ext1002-e09f where where position number is  02

 size_t StartPosition, EndPosition, DotPosition;
 string strKey;
 string strReturn;

 strReturn.clear();

 strKey = strASTERISK_AMI_POSITION_HEADER_911;                   //CO999x10
 StartPosition = stringInput.find(strKey);
 if (StartPosition != string::npos)
  {
   EndPosition =  stringInput.find("-", StartPosition);
   if (EndPosition == string::npos){return "";}
   DotPosition = stringInput.find(".", StartPosition);
   if ((DotPosition != string::npos)&&(EndPosition > DotPosition)){EndPosition = DotPosition;}
   StartPosition+= strKey.length();
   if (StartPosition >= (stringInput.length()-1)) {SendCodingError("globalfunctions.cpp - substr ParseAsteriskSIPpositionChannel() a"); return "";}
   if ((EndPosition-StartPosition) < 0)           {SendCodingError("globalfunctions.cpp - substr ParseAsteriskSIPpositionChannel() b"); return "";}
   strReturn = stringInput.substr(StartPosition, (EndPosition-StartPosition));
   
   return strReturn;
  }

 strKey = strASTERISK_AMI_POSITION_HEADER_FIRE;                  //CO999x90    this is going away ....      
 StartPosition = stringInput.find(strKey);
 if (StartPosition != string::npos)
  {
   EndPosition =  stringInput.find("-", StartPosition);
   if (EndPosition == string::npos){return "";}
   DotPosition = stringInput.find(".", StartPosition);
   if ((DotPosition != string::npos)&&(EndPosition > DotPosition)){EndPosition = DotPosition;}
   StartPosition+= strKey.length();
   if (StartPosition >= (stringInput.length()-1)) {SendCodingError("globalfunctions.cpp - substr ParseAsteriskSIPpositionChannel() c"); return "";}
   if ((EndPosition-StartPosition) < 0)           {SendCodingError("globalfunctions.cpp - substr ParseAsteriskSIPpositionChannel() d"); return "";}
   
   return stringInput.substr(StartPosition, (EndPosition-StartPosition));
  }

 strKey = ASTERISK_AMI_LOCAL_POSITION_PREFIX;                  //Local/10
 StartPosition = stringInput.find(strKey);
 if (StartPosition != string::npos)
  {
   StartPosition += strKey.length();
   strKey = ASTERISK_AMI_LOCAL_POSITION_SUFFIX_911;           //@ext10
   StartPosition = stringInput.find(strKey);
   if (StartPosition == string::npos){return "";}   
   EndPosition = stringInput.find("-", StartPosition);
   if (EndPosition == string::npos){return "";}   
   StartPosition+= strKey.length();
   if (StartPosition >= (stringInput.length()-1)) {SendCodingError("globalfunctions.cpp - substr ParseAsteriskSIPpositionChannel() e"); return "";}
   if ((EndPosition-StartPosition) < 0)           {SendCodingError("globalfunctions.cpp - substr ParseAsteriskSIPpositionChannel() f"); return "";}
   return stringInput.substr(StartPosition, (EndPosition-StartPosition));
  }


 return "";
}
*/
/*
int ParseLineNumberFromChannelName(string strInput)
{
 // This function Parses an Asterisk Channel to decode Line Numbers
 // Line Numbers are encoded in the following way
 // SIP/CO999x1002.3-02eb90c0 or where Line number is 3
 // or
 // Local/1002.1@ext1002-e09f where where Line number is  1
 // In order to keep other SIP adresses from giving us false positives
 // we must find a valid Position Channel first.

 string strPosition, strLineNumber;
 size_t StartPosition, DotPosition, DashPosition;
 strPosition.clear();

 strPosition = ParseAsteriskSIPpositionChannel(strInput);
 if (strPosition.empty())                    {return -1;}

 DotPosition = strInput.find("."); 
 if (DotPosition == string::npos)            {return -1;}

 DashPosition = strInput.find("-", DotPosition);
 if (DashPosition == string::npos)           {return -1;}

 StartPosition = DotPosition+1;
  
 if (StartPosition >= (strInput.length()-1)) {SendCodingError("globalfunctions.cpp - substr ParseLineNumberFromChannelName() a"); return -1;}
 if ((DashPosition-StartPosition) < 0)       {SendCodingError("globalfunctions.cpp - substr ParseLineNumberFromChannelName() b"); return -1;}

 strLineNumber = strInput.substr(StartPosition, (DashPosition-StartPosition));
 
 if (!Validate_Integer(strLineNumber))      {return -1;}

 return char2int(strLineNumber.c_str());
}
*/


string IPaddressFromChannelName(string strInput)
{
 size_t ATposition;

 ATposition = strInput.find("@"); 
 if (ATposition == string::npos)            {return "";}
 if (ATposition >= strInput.length()-1)     {return "";}

 return strInput.substr(ATposition+1,string::npos);
}




int PositionNumberfromExtension(string strInput)
{
 size_t found;

 if (strInput.length() != 4) {return 0;}
  
 found = strInput.find_first_not_of("0123456789");
 if (found != string::npos) {return 0;}

 if (strInput.substr(0,2) != POSITION_EXTENSION_PREFIX) {return 0;}

 return char2int(strInput.substr(2,2).c_str());
}
/*
string AsteriskPositionFromChannel(string strInput)
{
 string stringPosn;
 
 stringPosn =  ParseAsteriskSIPpositionChannel(strInput);
   
 return stringPosn;
}
*/
string AsteriskPositionFromANI(string strInput)
{
 string strPosn, strExtension;
 int    intPosn;

 strExtension = ParseFreeswitchData(strInput,ASTERISK_AMI_USEREVENT_ANISTRING_HEADER);
 intPosn = PositionNumberfromExtension(strExtension);

 if (intPosn) {return int2strLZ(intPosn);}
 else         {return "";}
}




/*
string AsteriskExtensionFromPositionChannel(string strInput)
{
 string stringExtension;
 
 stringExtension = AsteriskPositionFromChannel(strInput);
 if (stringExtension.empty())               {return "";}
   
 return "10"+stringExtension;
}
*/
int PositionNumberFromMSRPpresenceID(string strInput)
{
 size_t start, end;
 int    iPosition;
 string strInt;
 string strPrefix = MSRP_POSITION_NUMBER_PREFIX;

/*
   Naming Construct for MSRP users is Dispatch.2.msrp.911@IPaddress
   Prefix is "CCCCC." ie "Dispatch."
   Position number is between the dots.
*/
start = strInput.find(strPrefix);
if (start == string::npos) {return 0;}

start += strPrefix.length();
end = strInput.find_first_not_of("0123456789", start);
if (end == string::npos)  {return 0;}
if (strInput[end] != '.') {return 0;}
if (start == end) {return 0;}
strInt = strInput.substr(start, end - start);

return char2int(strInt.c_str());

}

string ExtensionFromConferenceDisplay(string strInput)
{
 string stringExtension;
 size_t FoundInString;
 int    iPosition;

 FoundInString = strInput.find(ANI_CONF_MEMBER_POSITION_PREFIX);
 if (FoundInString == string::npos) {return "";}

 stringExtension = strInput.substr(FoundInString+1, string::npos);

 FoundInString = stringExtension.find_first_not_of("0123456789");
 if (FoundInString != string::npos) {return "";}

 
 iPosition = char2int(stringExtension.c_str());
 iPosition += 1000;

 return int2str(iPosition);
}

bool IsCallerInConferenceDisplay(string strInput)
{
 size_t FoundInString;
 FoundInString = strInput.find(ANI_CONF_MEMBER_CALLER_PREFIX);
 
 if (FoundInString == string::npos) {return false;}
 else                               {return true;}




}




string fCallerIDGUIformat(string strInput)
{
 string strAllNumbers = "";
 string strOutput;

 for (unsigned int i=0; i < strInput.length(); i++) 
  {
   if (isdigit(strInput[i])) {strAllNumbers += strInput[i];}
  }

 switch (strAllNumbers.length())
  {
   case 7:
          strOutput = strAllNumbers.substr(0,3)+"-";
          strOutput+= strAllNumbers.substr(3,4);
          break;
   case 10:
          strOutput ="(";
          strOutput+= strAllNumbers.substr(0,3);
          strOutput+= ") " + strAllNumbers.substr(3,3)+"-";
          strOutput+= strAllNumbers.substr(6,4);
          break;
   case 11:
          strOutput ="(";
          strOutput+= strAllNumbers.substr(1,3);
          strOutput+= ") " + strAllNumbers.substr(4,3)+"-";
          strOutput+= strAllNumbers.substr(7,4);
          break;
   case 3:
          if (strInput[0] == '1') { strOutput = "";}
          else                    { strOutput = strInput;}   
          break;   
   default:
          strOutput = strInput;
          break;
  }

 return strOutput;
}



/*
string Set911TransferPrefix(string strInput)
{
 string       strStar = "*";
 string       strOutput;

 if (strInput[0] == '*') {return strInput;}

 switch (int911_DIAL_PREFIX_CODE)
  {
   case 7:  
          strOutput = strStar+strInput;
          return strOutput;
   case 3:
         if (strInput[0] != '9')      {break;}
         if (strInput.length() <= 10) {break;}
         strOutput = strStar+strInput;
         return strOutput;
   case 5:
        if ((strInput[0] == '9')&&(strInput.length() > 10)) {break;}  
        strOutput = strStar+strInput;
        return strOutput; 
   default:
        break;
  }

return strInput;
}
*/
/*
int DetermineTrunkTypeFromNumberDialed(string strInput)
{
 size_t found;
 bool   bAllNumbers = false;

 if (strInput.empty()) {return 0;}

 

 found = strInput.find_first_not_of("0123456789");
 if (found == string::npos) {bAllNumbers = true;}

 if (bAllNumbers)
  {
   // Check if CLID Call
   switch (intCLID_DIAL_PREFIX_CODE)
    {
     case 6:
          // Both 9 and No Prefix
          if      ((strInput[0] == '9')&&(strInput.length() > 10)) {return PBX_TELCO;}
          else if ((strInput[0] == '1')&&(strInput.length() > 10)) {return PBX_TELCO;}
          else if (strInput.length() == 10)                        {return PBX_TELCO;}
          break;
     case 2:
          // 9 Prefix only
          if ((strInput[0] == '9')&&(strInput.length() > 10))      {return PBX_TELCO;}
          break;
     case 4:
          // No Prefix
          if ((strInput[0] == '9')&&(strInput.length() > 10))      {break;}
          if ((strInput.length() >= 10))                           {return PBX_TELCO;}
          break;
     default:
          break;

    }// end  switch (intCLID_DIAL_PREFIX_CODE)


   if (strInput.length() == 4)                         {return SIP_LOCAL;}
   else                                                {return SIP;}


  }// end if bAllNumbers


 if (strInput[0] == '*')                               {return TANDEM;}


 return SIP;
}
*/
/*
int ConvertTransferTypeToTrunkType(transfer_method eXferMethod)
{
 
 switch (eXferMethod)
  {
   case mTANDEM:   return CAMA;
   default:        return SIP_TRUNK;
  }

}
*/

string TrunkType(int i) {
 extern Trunk_Type_Mapping     TRUNK_TYPE_MAP;

 if ((i > intMAX_NUM_TRUNKS)||(i < 0)) {return "UNKNOWN";}

 switch (TRUNK_TYPE_MAP.Trunk[i].TrunkType) {
  case CAMA:			{return "CAMA";}
  case CLID:			{return "CLID";}
  case WIRELESS:		{return "WIRELESS";}
  case LANDLINE:		{return "LANDLINE";}
  case SIP_TRUNK:		{return "SIP_TRUNK";}
  case NG911_SIP:		{return "NG911_SIP";}
  case REFER:	  		{return "REFER";}
  case INDIGITAL_WIRELESS:	{return "INDIGITAL_WRLS";}
  case MSRP_TRUNK:		{return "MSRP";}
  case INTRADO:                 {return "INTRADO";}
  default:
   SendCodingError( "globalfunctions.cpp - Coding Error in string TrunkType() unknown trunk type " + int2str(i));
 }
return "UNKNOWN";
}

string TrunkType(trunk_type  eTrunkType) {

//enum          trunk_type                                {CAMA, CLID, LANDLINE, WIRELESS, SIP_TRUNK, NG911_SIP, INDIGITAL_WIRELESS, MSRP_TRUNK};
 switch (eTrunkType) {
  case CAMA:                      	{return "CAMA";}
  case CLID:               	        {return "CLID";}
  case LANDLINE:             		{return "LANDLINE";}
  case WIRELESS:          		{return "WIRELESS";}
  case SIP_TRUNK:                      	{return "SIP_TRUNK";}
  case NG911_SIP:               	{return "NG911_SIP";}
  case REFER:			  	{return "REFER";}
  case INDIGITAL_WIRELESS:		{return "INDIGITAL_WRLS";}
  case MSRP_TRUNK:			{return "MSRP";}
  case INTRADO:                         {return "INTRADO";}  

  default:
   SendCodingError("globalfunctions.cpp - coding error TrunkType() unhandled value in case switch");
 }

 return "";
}

void DisplayTrunkType(int i)
{
 extern Trunk_Type_Mapping     TRUNK_TYPE_MAP;
 extern Trunk_Sequencing       Trunk_Map;

 switch (TRUNK_TYPE_MAP.Trunk[i].TrunkType)
  {
   case CAMA:      		cout << setw(15) << left << "911";       break;
   case CLID:      		cout << setw(15) << left << "CLID";      break;
   case WIRELESS:  		cout << setw(15) << left << "WIRELESS";  break;
   case LANDLINE:  		cout << setw(15) << left << "LANDLINE";  break;
   case SIP_TRUNK: 		cout << setw(15) << left << "SIPtrunk";  break;
   case NG911_SIP: 		cout << setw(15) << left << "NG911SIP";  break;
   case REFER:	 		cout << setw(15) << left << "REFER";  	 break;
   case INDIGITAL_WIRELESS: 	cout << setw(15) << left << "IDGTWRLS";  break; 
   case MSRP_TRUNK:             cout << setw(15) << left << "MSRP";      break;
   case INTRADO:                cout << setw(15) << left << "INTRADO";   break;
  } 
 cout << setw(10) << left << TRUNK_TYPE_MAP.Trunk[i].RotateGroup;
 cout << setw(15) << left << get_ALI_Record_Local_TimeStamp(TRUNK_TYPE_MAP.Trunk[i].time_tLastCall) << endl;
}



transfer_method DetermineTransferMethod(string strInput)
{
if (strInput == "_9_1_1")                           {return mTANDEM;}
if (strInput == XML_VALUE_TRANSFER_TYPE_TANDEM)     {return mTANDEM;}
if (strInput == XML_VALUE_TRANSFER_TYPE_ATTENDED)   {return mATTENDED_TRANSFER;}
if (strInput == XML_VALUE_TRANSFER_TYPE_BLIND)      {return mBLIND_TRANSFER;}
if (strInput == XML_VALUE_TRANSFER_TYPE_CONFERENCE) {return mGUI_TRANSFER;}
if (strInput == XML_VALUE_TRANSFER_TYPE_FLASH)      {return mFLASHOOK;}
// INDIGITAL AND INTRADO ARE .... TANDEM

return mBLANK;
}

string CardinalHeading(string strInput) {

strInput = RemoveLeadingSpaces(strInput);
strInput = RemoveTrailingSpaces(strInput);

if (strInput.empty())         {return "  ";}

if      (strInput == "N")     {return "N";}
else if (strInput == "n")     {return "N";}
else if (strInput == "north") {return "N";}
else if (strInput == "North") {return "N";}
else if (strInput == "NORTH") {return "N";}
if      (strInput == "S")     {return "S";}
else if (strInput == "s")     {return "S";}
else if (strInput == "south") {return "S";}
else if (strInput == "South") {return "S";}
else if (strInput == "SOUTH") {return "S";}
if      (strInput == "E")     {return "E";}
else if (strInput == "e")     {return "E";}
else if (strInput == "east")  {return "E";}
else if (strInput == "East")  {return "E";}
else if (strInput == "EAST")  {return "E";}
if      (strInput == "W")     {return "W";}
else if (strInput == "w")     {return "W";}
else if (strInput == "west")  {return "W";}
else if (strInput == "West")  {return "W";}
else if (strInput == "WEST")  {return "W";}

if      (strInput == "NE")         {return "NE";}
else if (strInput == "ne")         {return "NE";}
else if (strInput == "Ne")         {return "NE";}
else if (strInput == "northeast")  {return "NE";}
else if (strInput == "NorthEast")  {return "NE";}
else if (strInput == "Northeast")  {return "NE";}

if      (strInput == "NW")         {return "NW";}
else if (strInput == "nw")         {return "NW";}
else if (strInput == "Nw")         {return "NW";}
else if (strInput == "northwest")  {return "NW";}
else if (strInput == "NorthWest")  {return "NW";}
else if (strInput == "Northwest")  {return "NW";}

if      (strInput == "SE")         {return "SE";}
else if (strInput == "se")         {return "SE";}
else if (strInput == "Se")         {return "SE";}
else if (strInput == "southeast")  {return "SE";}
else if (strInput == "SouthEast")  {return "SE";}
else if (strInput == "Southeast")  {return "SE";}

if      (strInput == "SW")         {return "SW";}
else if (strInput == "sw")         {return "SW";}
else if (strInput == "Sw")         {return "SW";}
else if (strInput == "southwest")  {return "SW";}
else if (strInput == "SouthWest")  {return "SW";}
else if (strInput == "Southwest")  {return "SW";}

// should not get here !!!
SendCodingError("globalfunctions.cpp fn CardinalHeading() unable to process string -> " + strInput);
return "  ";
}

string PostalStreetSuffix(string strInput) {
/*
  https://pe.usps.com/text/pub28/28apc_002.htm
*/

strInput = RemoveLeadingSpaces(strInput);
strInput = RemoveTrailingSpaces(strInput);

if      (strInput == "ALLEY") {return "ALY";}
else if (strInput == "Alley") {return "ALY";}
else if (strInput == "alley") {return "ALY";}
else if (strInput == "ALLEE") {return "ALY";}
else if (strInput == "Allee") {return "ALY";}
else if (strInput == "allee") {return "ALY";}
else if (strInput == "ALLY")  {return "ALY";}
else if (strInput == "Ally")  {return "ALY";}
else if (strInput == "ally")  {return "ALY";}
else if (strInput == "ALY")   {return "ALY";}
else if (strInput == "Aly")   {return "ALY";}
else if (strInput == "aly")   {return "ALY";}

if      (strInput == "ANEX") {return "ANX";}
else if (strInput == "Anex") {return "ANX";}
else if (strInput == "anex") {return "ANX";}
else if (strInput == "ANNEX") {return "ANX";}
else if (strInput == "Annex") {return "ANX";}
else if (strInput == "annex") {return "ANX";}
else if (strInput == "ANNX")  {return "ANX";}
else if (strInput == "Annx")  {return "ANX";}
else if (strInput == "annx")  {return "ANX";}
else if (strInput == "ANX")   {return "ANX";}
else if (strInput == "Anx")   {return "ANX";}
else if (strInput == "anx")   {return "ANX";}

if      (strInput == "ARCADE") {return "ARC";}
else if (strInput == "Arcade") {return "ARC";}
else if (strInput == "arcade") {return "ARC";}
else if (strInput == "ARC")    {return "ARC";}
else if (strInput == "Arc")    {return "ARC";}
else if (strInput == "arc")    {return "ARC";}

if      (strInput == "AVENUE") {return "AVE";}
else if (strInput == "Avenue") {return "AVE";}
else if (strInput == "avenue") {return "AVE";}
else if (strInput == "AV")     {return "AVE";}
else if (strInput == "Av")     {return "AVE";}
else if (strInput == "av")     {return "AVE";}
else if (strInput == "AVE")    {return "AVE";}
else if (strInput == "Ave")    {return "AVE";}
else if (strInput == "ave")    {return "AVE";}
else if (strInput == "AVEN")   {return "AVE";}
else if (strInput == "Aven")   {return "AVE";}
else if (strInput == "aven")   {return "AVE";}
else if (strInput == "AVENU")   {return "AVE";}
else if (strInput == "Avenu")   {return "AVE";}
else if (strInput == "avenu")   {return "AVE";}
else if (strInput == "AVN")     {return "AVE";}
else if (strInput == "Avn")     {return "AVE";}
else if (strInput == "avn")     {return "AVE";}
else if (strInput == "AVNUE")   {return "AVE";}
else if (strInput == "Avnue")   {return "AVE";}
else if (strInput == "avnue")   {return "AVE";}

if      (strInput == "BAYOU")   {return "BYU";}
else if (strInput == "Bayou")   {return "BYU";}
else if (strInput == "bayou")   {return "BYU";}
else if (strInput == "BAYOO")   {return "BYU";}
else if (strInput == "Bayoo")   {return "BYU";}
else if (strInput == "bayoo")   {return "BYU";}
else if (strInput == "BYU")     {return "BYU";}
else if (strInput == "Byu")     {return "BYU";}
else if (strInput == "byu")     {return "BYU";}


if      (strInput == "BEACH")   {return "BCH";}
else if (strInput == "Beach")   {return "BCH";}
else if (strInput == "beach")   {return "BCH";}
else if (strInput == "BCH")     {return "BCH";}
else if (strInput == "Bch")     {return "BCH";}
else if (strInput == "bch")     {return "BCH";}

if      (strInput == "BEND")    {return "BND";}
else if (strInput == "Bend")    {return "BND";}
else if (strInput == "bend")    {return "BND";}
else if (strInput == "BND")     {return "BND";}
else if (strInput == "Bnd")     {return "BND";}
else if (strInput == "bnd")     {return "BND";}

if      (strInput == "BLUFF")    {return "BLF";}
else if (strInput == "Bluff")    {return "BLF";}
else if (strInput == "bluff")    {return "BLF";}
else if (strInput == "BLF")      {return "BLF";}
else if (strInput == "Blf")      {return "BLF";}
else if (strInput == "blf")      {return "BLF";}
else if (strInput == "BLUF")     {return "BLF";}
else if (strInput == "Bluf")     {return "BLF";}
else if (strInput == "bluf")     {return "BLF";}

if      (strInput == "BLUFFS")    {return "BLFS";}
else if (strInput == "Bluffs")    {return "BLFS";}
else if (strInput == "bluffs")    {return "BLFS";}
else if (strInput == "BLFS")      {return "BLFS";}
else if (strInput == "Blfs")      {return "BLFS";}
else if (strInput == "blfs")      {return "BLFS";}

if      (strInput == "BOTTOM")    {return "BTM";}
else if (strInput == "Bottom")    {return "BTM";}
else if (strInput == "bottom")    {return "BTM";}
else if (strInput == "BTM")       {return "BTM";}
else if (strInput == "Btm")       {return "BTM";}
else if (strInput == "btm")       {return "BTM";}
else if (strInput == "BOT")       {return "BTM";}
else if (strInput == "Bot")       {return "BTM";}
else if (strInput == "bot")       {return "BTM";}
else if (strInput == "BOTTM")     {return "BTM";}
else if (strInput == "Bottm")     {return "BTM";}
else if (strInput == "bottm")     {return "BTM";}

if      (strInput == "BOULEVARD")  {return "BLVD";}
else if (strInput == "Boulevard")  {return "BLVD";}
else if (strInput == "boulevard")  {return "BLVD";}
else if (strInput == "BLVD")       {return "BLVD";}
else if (strInput == "Blvd")       {return "BLVD";}
else if (strInput == "blvd")       {return "BLVD";}
else if (strInput == "BOUL")       {return "BLVD";}
else if (strInput == "Boul")       {return "BLVD";}
else if (strInput == "boul")       {return "BLVD";}
else if (strInput == "BOULV")      {return "BLVD";}
else if (strInput == "Boulv")      {return "BLVD";}
else if (strInput == "boulv")      {return "BLVD";}

if      (strInput == "BRANCH")    {return "BR";}
else if (strInput == "Branch")    {return "BR";}
else if (strInput == "branch")    {return "BR";}
else if (strInput == "BR")        {return "BR";}
else if (strInput == "Br")        {return "BR";}
else if (strInput == "br")        {return "BR";}
else if (strInput == "BRNCH")     {return "BR";}
else if (strInput == "Brnch")     {return "BR";}
else if (strInput == "brnch")     {return "BR";}

if      (strInput == "BRIDGE")    {return "BRG";}
else if (strInput == "Bridge")    {return "BRG";}
else if (strInput == "bridge")    {return "BRG";}
else if (strInput == "BRG")       {return "BRG";}
else if (strInput == "Brg")       {return "BRG";}
else if (strInput == "brg")       {return "BRG";}
else if (strInput == "BRDGE")     {return "BRG";}
else if (strInput == "Brdge")     {return "BRG";}
else if (strInput == "brdge")     {return "BRG";}

if      (strInput == "BROOK")    {return "BRK";}
else if (strInput == "Brook")    {return "BRK";}
else if (strInput == "brook")    {return "BRK";}
else if (strInput == "BRK")      {return "BRK";}
else if (strInput == "Brk")      {return "BRK";}
else if (strInput == "brk")      {return "BRK";}

if      (strInput == "BROOKS")    {return "BRKS";}
else if (strInput == "Brooks")    {return "BRKS";}
else if (strInput == "brooks")    {return "BRKS";}
else if (strInput == "BRKS")      {return "BRKS";}
else if (strInput == "Brks")      {return "BRKS";}
else if (strInput == "brks")      {return "BRKS";}


if      (strInput == "BURG")    {return "BG";}
else if (strInput == "Burg")    {return "BG";}
else if (strInput == "burg")    {return "BG";}
else if (strInput == "BG")      {return "BG";}
else if (strInput == "Bg")      {return "BG";}
else if (strInput == "bg")      {return "BG";}


if      (strInput == "BURGS")    {return "BGS";}
else if (strInput == "Burgs")    {return "BGS";}
else if (strInput == "burgs")    {return "BGS";}
else if (strInput == "BGS")      {return "BGS";}
else if (strInput == "Bgs")      {return "BGS";}
else if (strInput == "bgs")      {return "BGS";}

if      (strInput == "BYPASS")    {return "BYP";}
else if (strInput == "Bypass")    {return "BYP";}
else if (strInput == "bypass")    {return "BYP";}
else if (strInput == "BYP")       {return "BYP";}
else if (strInput == "Byp")       {return "BYP";}
else if (strInput == "byp")       {return "BYP";}
else if (strInput == "BYPA")      {return "BYP";}
else if (strInput == "Bypa")      {return "BYP";}
else if (strInput == "bypa")      {return "BYP";}
else if (strInput == "BYPAS")     {return "BYP";}
else if (strInput == "Bypas")     {return "BYP";}
else if (strInput == "bypas")     {return "BYP";}
else if (strInput == "BYPS")      {return "BYP";}
else if (strInput == "Byps")      {return "BYP";}
else if (strInput == "byps")      {return "BYP";}

if      (strInput == "CAMP")    {return "CP";}
else if (strInput == "Camp")    {return "CP";}
else if (strInput == "camp")    {return "CP";}
else if (strInput == "CP")      {return "CP";}
else if (strInput == "Cp")      {return "CP";}
else if (strInput == "cp")      {return "CP";}
else if (strInput == "CMP")     {return "CP";}
else if (strInput == "Cmp")     {return "CP";}
else if (strInput == "cmp")     {return "CP";}

if      (strInput == "CANYON")    {return "CYN";}
else if (strInput == "Canyon")    {return "CYN";}
else if (strInput == "canyon")    {return "CYN";}
else if (strInput == "CYN")       {return "CYN";}
else if (strInput == "Cyn")       {return "CYN";}
else if (strInput == "cyn")       {return "CYN";}
else if (strInput == "CNYN")      {return "CYN";}
else if (strInput == "Cnyn")      {return "CYN";}
else if (strInput == "cnyn")      {return "CYN";}
else if (strInput == "CANYN")     {return "CYN";}
else if (strInput == "Canyn")     {return "CYN";}
else if (strInput == "canyn")     {return "CYN";}

if      (strInput == "CAPE")    {return "CPE";}
else if (strInput == "Cape")    {return "CPE";}
else if (strInput == "cape")    {return "CPE";}
else if (strInput == "CPE")     {return "CPE";}
else if (strInput == "Cpe")     {return "CPE";}
else if (strInput == "cpe")     {return "CPE";}

if      (strInput == "CAUSEWAY")    {return "CSWY";}
else if (strInput == "Causeway")    {return "CSWY";}
else if (strInput == "causeway")    {return "CSWY";}
else if (strInput == "CSWY")        {return "CSWY";}
else if (strInput == "Cswy")        {return "CSWY";}
else if (strInput == "cswy")        {return "CSWY";}
else if (strInput == "CAUSWA")      {return "CSWY";}
else if (strInput == "Causwa")      {return "CSWY";}
else if (strInput == "causwa")      {return "CSWY";}

if      (strInput == "CEN")      {return "CTR";}
else if (strInput == "Cen")      {return "CTR";}
else if (strInput == "cen")      {return "CTR";}
else if (strInput == "CENT")     {return "CTR";}
else if (strInput == "Cent")     {return "CTR";}
else if (strInput == "cent")     {return "CTR";}
else if (strInput == "CENTER")   {return "CTR";}
else if (strInput == "Center")   {return "CTR";}
else if (strInput == "center")   {return "CTR";}
else if (strInput == "CENTR")    {return "CTR";}
else if (strInput == "Centr")    {return "CTR";}
else if (strInput == "centr")    {return "CTR";}
else if (strInput == "CENTRE")   {return "CTR";}
else if (strInput == "Centre")   {return "CTR";}
else if (strInput == "centre")   {return "CTR";}
else if (strInput == "CNTR")     {return "CTR";}
else if (strInput == "Cntr")     {return "CTR";}
else if (strInput == "cntr")     {return "CTR";}
else if (strInput == "CTR")      {return "CTR";}
else if (strInput == "Ctr")      {return "CTR";}
else if (strInput == "ctr")      {return "CTR";}

if      (strInput == "CENTERS")      {return "CTRS";}
else if (strInput == "Centers")      {return "CTRS";}
else if (strInput == "centers")      {return "CTRS";}
else if (strInput == "CTRS")         {return "CTRS";}
else if (strInput == "Ctrs")         {return "CTRS";}
else if (strInput == "ctrs")         {return "CTRS";}

if      (strInput == "CIR")      {return "CIR";}
else if (strInput == "Cir")      {return "CIR";}
else if (strInput == "cir")      {return "CIR";}
else if (strInput == "CIRC")     {return "CIR";}
else if (strInput == "Circ")     {return "CIR";}
else if (strInput == "circ")     {return "CIR";}
else if (strInput == "CIRCL")    {return "CIR";}
else if (strInput == "Circl")    {return "CIR";}
else if (strInput == "circl")    {return "CIR";}
else if (strInput == "CIRCLE")   {return "CIR";}
else if (strInput == "Circle")   {return "CIR";}
else if (strInput == "circle")   {return "CIR";}
else if (strInput == "CRCL")     {return "CIR";}
else if (strInput == "Crcl")     {return "CIR";}
else if (strInput == "crcl")     {return "CIR";}
else if (strInput == "CRCLE")    {return "CIR";}
else if (strInput == "Crcle")    {return "CIR";}
else if (strInput == "crcle")    {return "CIR";}

if      (strInput == "CIRCLES")   {return "CIRS";}
else if (strInput == "Circles")   {return "CIRS";}
else if (strInput == "circles")   {return "CIRS";}
else if (strInput == "CIRS")      {return "CIRS";}
else if (strInput == "Cirs")      {return "CIRS";}
else if (strInput == "cirs")      {return "CIRS";}

if      (strInput == "CLIFF")   {return "CLF";}
else if (strInput == "Cliff")   {return "CLF";}
else if (strInput == "cliff")   {return "CLF";}
else if (strInput == "CLF")     {return "CLF";}
else if (strInput == "Clf")     {return "CLF";}
else if (strInput == "clf")     {return "CLF";}

if      (strInput == "CLIFFS")   {return "CLFS";}
else if (strInput == "Cliffs")   {return "CLFS";}
else if (strInput == "cliffs")   {return "CLFS";}
else if (strInput == "CLFS")     {return "CLFS";}
else if (strInput == "Clfs")     {return "CLFS";}
else if (strInput == "clfs")     {return "CLFS";}

if      (strInput == "CLUB")    {return "CLB";}
else if (strInput == "Club")    {return "CLB";}
else if (strInput == "club")    {return "CLB";}
else if (strInput == "CLB")     {return "CLB";}
else if (strInput == "Clb")     {return "CLB";}
else if (strInput == "clb")     {return "CLB";}

if      (strInput == "COMMON")    {return "CMN";}
else if (strInput == "Common")    {return "CMN";}
else if (strInput == "common")    {return "CMN";}
else if (strInput == "CMN")       {return "CMN";}
else if (strInput == "Cmn")       {return "CMN";}
else if (strInput == "cmn")       {return "CMN";}

if      (strInput == "COMMONS")    {return "CMNS";}
else if (strInput == "Commons")    {return "CMNS";}
else if (strInput == "commons")    {return "CMNS";}
else if (strInput == "CMNS")       {return "CMNS";}
else if (strInput == "Cmns")       {return "CMNS";}
else if (strInput == "cmns")       {return "CMNS";}

if      (strInput == "CORNER")    {return "COR";}
else if (strInput == "Corner")    {return "COR";}
else if (strInput == "corner")    {return "COR";}
else if (strInput == "COR")       {return "COR";}
else if (strInput == "Cor")       {return "COR";}
else if (strInput == "cor")       {return "COR";}

if      (strInput == "CORNERS")    {return "CORS";}
else if (strInput == "Corners")    {return "CORS";}
else if (strInput == "corners")    {return "CORS";}
else if (strInput == "CORS")       {return "CORS";}
else if (strInput == "Cors")       {return "CORS";}
else if (strInput == "cors")       {return "CORS";}

if      (strInput == "COURSE")     {return "CRSE";}
else if (strInput == "Course")     {return "CRSE";}
else if (strInput == "course")     {return "CRSE";}
else if (strInput == "CRSE")       {return "CRSE";}
else if (strInput == "Crse")       {return "CRSE";}
else if (strInput == "crse")       {return "CRSE";}

if      (strInput == "COURT")     {return "CT";}
else if (strInput == "Court")     {return "CT";}
else if (strInput == "court")     {return "CT";}
else if (strInput == "CT")        {return "CT";}
else if (strInput == "Ct")        {return "CT";}
else if (strInput == "ct")        {return "CT";}

if      (strInput == "COURTS")     {return "CTS";}
else if (strInput == "Courts")     {return "CTS";}
else if (strInput == "courts")     {return "CTS";}
else if (strInput == "CTS")        {return "CTS";}
else if (strInput == "Cts")        {return "CTS";}
else if (strInput == "cts")        {return "CTS";}

if      (strInput == "COVE")     {return "CV";}
else if (strInput == "Cove")     {return "CV";}
else if (strInput == "cove")     {return "CV";}
else if (strInput == "CV")       {return "CV";}
else if (strInput == "Cv")       {return "CV";}
else if (strInput == "cv")       {return "CV";}

if      (strInput == "COVES")     {return "CVS";}
else if (strInput == "Coves")     {return "CVS";}
else if (strInput == "coves")     {return "CVS";}
else if (strInput == "CVS")       {return "CVS";}
else if (strInput == "Cvs")       {return "CVS";}
else if (strInput == "cvs")       {return "CVS";}

if      (strInput == "CREEK")     {return "CRK";}
else if (strInput == "Creek")     {return "CRK";}
else if (strInput == "creek")     {return "CRK";}
else if (strInput == "CRK")       {return "CRK";}
else if (strInput == "Crk")       {return "CRK";}
else if (strInput == "crk")       {return "CRK";}

if      (strInput == "CRESCENT") {return "CRES";}
else if (strInput == "Crescent") {return "CRES";}
else if (strInput == "crescent") {return "CRES";}
else if (strInput == "CRES")     {return "CRES";}
else if (strInput == "Cres")     {return "CRES";}
else if (strInput == "cres")     {return "CRES";}
else if (strInput == "CRSENT")   {return "CRES";}
else if (strInput == "Crsent")   {return "CRES";}
else if (strInput == "crsent")   {return "CRES";}
else if (strInput == "CRSNT")    {return "CRES";}
else if (strInput == "Crsnt")    {return "CRES";}
else if (strInput == "crsnt")    {return "CRES";}

if      (strInput == "CREST")     {return "CRST";}
else if (strInput == "Crest")     {return "CRST";}
else if (strInput == "crest")     {return "CRST";}
else if (strInput == "CRST")      {return "CRST";}
else if (strInput == "Crst")      {return "CRST";}
else if (strInput == "crst")      {return "CRST";}

if      (strInput == "CROSSING") {return "XING";}
else if (strInput == "Crossing") {return "XING";}
else if (strInput == "crossing") {return "XING";}
else if (strInput == "CRSSNG")   {return "XING";}
else if (strInput == "Crssng")   {return "XING";}
else if (strInput == "crssng")   {return "XING";}
else if (strInput == "XING")     {return "XING";}
else if (strInput == "Xing")     {return "XING";}
else if (strInput == "xing")     {return "XING";}

if      (strInput == "CROSSROAD")     {return "XRD";}
else if (strInput == "Crossroad")     {return "XRD";}
else if (strInput == "crossroad")     {return "XRD";}
else if (strInput == "XRD")           {return "XRD";}
else if (strInput == "Xrd")           {return "XRD";}
else if (strInput == "xrd")           {return "XRD";}

if      (strInput == "CROSSROADS")     {return "XRDS";}
else if (strInput == "Crossroads")     {return "XRDS";}
else if (strInput == "crossroads")     {return "XRDS";}
else if (strInput == "XRDS")           {return "XRDS";}
else if (strInput == "Xrds")           {return "XRDS";}
else if (strInput == "xrds")           {return "XRDS";}

if      (strInput == "CURVE")     {return "CURV";}
else if (strInput == "Curve")     {return "CURV";}
else if (strInput == "curve")     {return "CURV";}
else if (strInput == "CURV")      {return "CURV";}
else if (strInput == "Curv")      {return "CURV";}
else if (strInput == "curv")      {return "CURV";}

if      (strInput == "DALE")     {return "DL";}
else if (strInput == "Dale")     {return "DL";}
else if (strInput == "dale")     {return "DL";}
else if (strInput == "DL")       {return "DL";}
else if (strInput == "Dl")       {return "DL";}
else if (strInput == "dl")       {return "DL";}

if      (strInput == "DAM")     {return "DM";}
else if (strInput == "Dam")     {return "DM";}
else if (strInput == "dam")     {return "DM";}
else if (strInput == "DM")      {return "DM";}
else if (strInput == "Dm")      {return "DM";}
else if (strInput == "dm")      {return "DM";}

if      (strInput == "DIVIDE") {return "DV";}
else if (strInput == "Divide") {return "DV";}
else if (strInput == "divide") {return "DV";}
else if (strInput == "DV")     {return "DV";}
else if (strInput == "Dv")     {return "DV";}
else if (strInput == "dv")     {return "DV";}
else if (strInput == "DIV")    {return "DV";}
else if (strInput == "Div")    {return "DV";}
else if (strInput == "div")    {return "DV";}
else if (strInput == "DVD")    {return "DV";}
else if (strInput == "Dvd")    {return "DV";}
else if (strInput == "dvd")    {return "DV";}

if      (strInput == "DRIVE")  {return "DR";}
else if (strInput == "Drive")  {return "DR";}
else if (strInput == "drive")  {return "DR";}
else if (strInput == "DR")     {return "DR";}
else if (strInput == "Dr")     {return "DR";}
else if (strInput == "dr")     {return "DR";}
else if (strInput == "DRIV")   {return "DR";}
else if (strInput == "Driv")   {return "DR";}
else if (strInput == "driv")   {return "DR";}
else if (strInput == "DRV")    {return "DR";}
else if (strInput == "Drv")    {return "DR";}
else if (strInput == "drv")    {return "DR";}

if      (strInput == "DRIVES")  {return "DRS";}
else if (strInput == "Drives")  {return "DRS";}
else if (strInput == "drives")  {return "DRS";}
else if (strInput == "DRS")     {return "DRS";}
else if (strInput == "Drs")     {return "DRS";}
else if (strInput == "drs")     {return "DRS";}

if      (strInput == "ESTATE")  {return "EST";}
else if (strInput == "Estate")  {return "EST";}
else if (strInput == "estate")  {return "EST";}
else if (strInput == "EST")     {return "EST";}
else if (strInput == "Est")     {return "EST";}
else if (strInput == "est")     {return "EST";}

if      (strInput == "ESTATES")  {return "ESTS";}
else if (strInput == "Estates")  {return "ESTS";}
else if (strInput == "estates")  {return "ESTS";}
else if (strInput == "ESTS")     {return "ESTS";}
else if (strInput == "Ests")     {return "ESTS";}
else if (strInput == "ests")     {return "ESTS";}

if      (strInput == "EXP")       {return "EXPY";}
else if (strInput == "Exp")       {return "EXPY";}
else if (strInput == "exp")       {return "EXPY";}
else if (strInput == "EXPR")      {return "EXPY";}
else if (strInput == "Expr")      {return "EXPY";}
else if (strInput == "expr")      {return "EXPY";}
else if (strInput == "EXPRESS")   {return "EXPY";}
else if (strInput == "Express")   {return "EXPY";}
else if (strInput == "express")   {return "EXPY";}
else if (strInput == "EXPRESSWAY"){return "EXPY";}
else if (strInput == "Expressway"){return "EXPY";}
else if (strInput == "expressway"){return "EXPY";}
else if (strInput == "EXPW")      {return "EXPY";}
else if (strInput == "Expw")      {return "EXPY";}
else if (strInput == "expw")      {return "EXPY";}
else if (strInput == "EXPY")      {return "EXPY";}
else if (strInput == "Expy")      {return "EXPY";}
else if (strInput == "expy")      {return "EXPY";}

if      (strInput == "EXTENSION")    {return "EXT";}
else if (strInput == "Extension")    {return "EXT";}
else if (strInput == "extension")    {return "EXT";}
else if (strInput == "EXT")          {return "EXT";}
else if (strInput == "Ext")          {return "EXT";}
else if (strInput == "ext")          {return "EXT";}
else if (strInput == "EXTN")         {return "EXT";}
else if (strInput == "Extn")         {return "EXT";}
else if (strInput == "extn")         {return "EXT";}
else if (strInput == "EXTNSN")       {return "EXT";}
else if (strInput == "Extnsn")       {return "EXT";}
else if (strInput == "extnsn")       {return "EXT";}

if      (strInput == "EXTENSIONS")    {return "EXTS";}
else if (strInput == "Extensions")    {return "EXTS";}
else if (strInput == "extensions")    {return "EXTS";}
else if (strInput == "EXTS")          {return "EXTS";}
else if (strInput == "Exts")          {return "EXTS";}
else if (strInput == "exts")          {return "EXTS";}

if      (strInput == "FALL")  {return "FALL";}
else if (strInput == "Fall")  {return "FALL";}
else if (strInput == "fall")  {return "FALL";}

if      (strInput == "FALLS")    {return "FLS";}
else if (strInput == "Falls")    {return "FLS";}
else if (strInput == "falls")    {return "FLS";}
else if (strInput == "FLS")      {return "FLS";}
else if (strInput == "Fls")      {return "FLS";}
else if (strInput == "fls")      {return "FLS";}

if      (strInput == "FERRY")    {return "FRY";}
else if (strInput == "Ferry")    {return "FRY";}
else if (strInput == "ferry")    {return "FRY";}
else if (strInput == "FRRY")     {return "FRY";}
else if (strInput == "Frry")     {return "FRY";}
else if (strInput == "frry")     {return "FRY";}
else if (strInput == "FRY")      {return "FRY";}
else if (strInput == "Fry")      {return "FRY";}
else if (strInput == "fry")      {return "FRY";}

if      (strInput == "FIELD")    {return "FLD";}
else if (strInput == "Field")    {return "FLD";}
else if (strInput == "field")    {return "FLD";}
else if (strInput == "FLD")      {return "FLD";}
else if (strInput == "Fld")      {return "FLD";}
else if (strInput == "fld")      {return "FLD";}

if      (strInput == "FIELDS")    {return "FLDS";}
else if (strInput == "Fields")    {return "FLDS";}
else if (strInput == "fields")    {return "FLDS";}
else if (strInput == "FLDS")      {return "FLDS";}
else if (strInput == "Flds")      {return "FLDS";}
else if (strInput == "flds")      {return "FLDS";}

if      (strInput == "FLAT")     {return "FLT";}
else if (strInput == "Flat")     {return "FLT";}
else if (strInput == "flat")     {return "FLT";}
else if (strInput == "FLT")      {return "FLT";}
else if (strInput == "Flt")      {return "FLT";}
else if (strInput == "flt")      {return "FLT";}

if      (strInput == "FLATS")    {return "FLTS";}
else if (strInput == "Flats")    {return "FLTS";}
else if (strInput == "flats")    {return "FLTS";}
else if (strInput == "FLTS")     {return "FLTS";}
else if (strInput == "Flts")     {return "FLTS";}
else if (strInput == "flts")     {return "FLTS";}

if      (strInput == "FORD")    {return "FRD";}
else if (strInput == "Ford")    {return "FRD";}
else if (strInput == "ford")    {return "FRD";}
else if (strInput == "FRD")     {return "FRD";}
else if (strInput == "Frd")     {return "FRD";}
else if (strInput == "frd")     {return "FRD";}

if      (strInput == "FORDS")    {return "FRDS";}
else if (strInput == "Fords")    {return "FRDS";}
else if (strInput == "fords")    {return "FRDS";}
else if (strInput == "FRDS")     {return "FRDS";}
else if (strInput == "Frds")     {return "FRDS";}
else if (strInput == "frds")     {return "FRDS";}

if      (strInput == "FOREST")   {return "FRST";}
else if (strInput == "Forest")   {return "FRST";}
else if (strInput == "forest")   {return "FRST";}
else if (strInput == "FRST")     {return "FRST";}
else if (strInput == "Frst")     {return "FRST";}
else if (strInput == "frst")     {return "FRST";}
else if (strInput == "FORESTS")  {return "FRST";}
else if (strInput == "Forests")  {return "FRST";}
else if (strInput == "forests")  {return "FRST";}

if      (strInput == "FORG")   {return "FRG";}
else if (strInput == "Forg")   {return "FRG";}
else if (strInput == "forg")   {return "FRG";}
else if (strInput == "FORGE")  {return "FRG";}
else if (strInput == "Forge")  {return "FRG";}
else if (strInput == "forge")  {return "FRG";}
else if (strInput == "FRG")    {return "FRG";}
else if (strInput == "Frg")    {return "FRG";}
else if (strInput == "frg")    {return "FRG";}

if      (strInput == "FRGS")    {return "FRGS";}
else if (strInput == "Frgs")    {return "FRGS";}
else if (strInput == "frgs")    {return "FRGS";}
else if (strInput == "FORGES")  {return "FRGS";}
else if (strInput == "Forges")  {return "FRGS";}
else if (strInput == "forges")  {return "FRGS";}

if      (strInput == "FORK")    {return "FRK";}
else if (strInput == "Fork")    {return "FRK";}
else if (strInput == "fork")    {return "FRK";}
else if (strInput == "FRK")     {return "FRK";}
else if (strInput == "Frk")     {return "FRK";}
else if (strInput == "frk")     {return "FRK";}

if      (strInput == "FORKS")    {return "FRKS";}
else if (strInput == "Forks")    {return "FRKS";}
else if (strInput == "forks")    {return "FRKS";}
else if (strInput == "FRKS")     {return "FRKS";}
else if (strInput == "Frks")     {return "FRKS";}
else if (strInput == "frks")     {return "FRKS";}

if      (strInput == "FORT")    {return "FT";}
else if (strInput == "Fort")    {return "FT";}
else if (strInput == "fort")    {return "FT";}
else if (strInput == "FRT")     {return "FT";}
else if (strInput == "Frt")     {return "FT";}
else if (strInput == "frt")     {return "FT";}
else if (strInput == "FT")      {return "FT";}
else if (strInput == "Ft")      {return "FT";}
else if (strInput == "ft")      {return "FT";}

if      (strInput == "FREEWAY")    {return "FWY";}
else if (strInput == "Freeway")    {return "FWY";}
else if (strInput == "freeway")    {return "FWY";}
else if (strInput == "FREEWY")     {return "FWY";}
else if (strInput == "Freewy")     {return "FWY";}
else if (strInput == "freewy")     {return "FWY";}
else if (strInput == "FRWAY")      {return "FWY";}
else if (strInput == "Frway")      {return "FWY";}
else if (strInput == "frway")      {return "FWY";}
else if (strInput == "FRWY")       {return "FWY";}
else if (strInput == "Frwy")       {return "FWY";}
else if (strInput == "frwy")       {return "FWY";}
else if (strInput == "FWY")        {return "FWY";}
else if (strInput == "Fwy")        {return "FWY";}
else if (strInput == "fwy")        {return "FWY";}

if      (strInput == "GARDEN")    {return "GDN";}
else if (strInput == "Garden")    {return "GDN";}
else if (strInput == "garden")    {return "GDN";}
else if (strInput == "GARDN")     {return "GDN";}
else if (strInput == "Gardn")     {return "GDN";}
else if (strInput == "gardn")     {return "GDN";}
else if (strInput == "GRDN")      {return "GDN";}
else if (strInput == "Grdn")      {return "GDN";}
else if (strInput == "grdn")      {return "GDN";}
else if (strInput == "GRDEN")     {return "GDN";}
else if (strInput == "Grden")     {return "GDN";}
else if (strInput == "grden")     {return "GDN";}
else if (strInput == "GDN")       {return "GDN";}
else if (strInput == "Gdn")       {return "GDN";}
else if (strInput == "gdn")       {return "GDN";}

if      (strInput == "GARDENS")    {return "GDNS";}
else if (strInput == "Gardens")    {return "GDNS";}
else if (strInput == "gardens")    {return "GDNS";}
else if (strInput == "GRDNS")      {return "GDNS";}
else if (strInput == "Grdns")      {return "GDNS";}
else if (strInput == "grdns")      {return "GDNS";}
else if (strInput == "GDNS")       {return "GDNS";}
else if (strInput == "Gdns")       {return "GDNS";}
else if (strInput == "gdns")       {return "GDNS";}

if      (strInput == "GATEWAY")    {return "GTWY";}
else if (strInput == "Gateway")    {return "GTWY";}
else if (strInput == "gateway")    {return "GTWY";}
else if (strInput == "GATEWY")     {return "GTWY";}
else if (strInput == "Gatewy")     {return "GTWY";}
else if (strInput == "gatewy")     {return "GTWY";}
else if (strInput == "GATWAY")     {return "GTWY";}
else if (strInput == "Gatway")     {return "GTWY";}
else if (strInput == "gatway")     {return "GTWY";}
else if (strInput == "GTWAY")      {return "GTWY";}
else if (strInput == "Gtway")      {return "GTWY";}
else if (strInput == "gtway")      {return "GTWY";}
else if (strInput == "GTWY")       {return "GTWY";}
else if (strInput == "Gtwy")       {return "GTWY";}
else if (strInput == "gtwy")       {return "GTWY";}

if      (strInput == "GLEN")    {return "GLN";}
else if (strInput == "Glen")    {return "GLN";}
else if (strInput == "glen")    {return "GLN";}
else if (strInput == "GLN")     {return "GLN";}
else if (strInput == "Gln")     {return "GLN";}
else if (strInput == "gln")     {return "GLN";}

if      (strInput == "GLENS")    {return "GLNS";}
else if (strInput == "Glens")    {return "GLNS";}
else if (strInput == "glens")    {return "GLNS";}
else if (strInput == "GLNS")     {return "GLNS";}
else if (strInput == "Glns")     {return "GLNS";}
else if (strInput == "glns")     {return "GLNS";}

if      (strInput == "GREEN")    {return "GRN";}
else if (strInput == "Green")    {return "GRN";}
else if (strInput == "green")    {return "GRN";}
else if (strInput == "GRN")      {return "GRN";}
else if (strInput == "Grn")      {return "GRN";}
else if (strInput == "grn")      {return "GRN";}

if      (strInput == "GREENS")    {return "GRNS";}
else if (strInput == "Greens")    {return "GRNS";}
else if (strInput == "greens")    {return "GRNS";}
else if (strInput == "GRNS")      {return "GRNS";}
else if (strInput == "Grns")      {return "GRNS";}
else if (strInput == "grns")      {return "GRNS";}

if      (strInput == "GROV")    {return "GRV";}
else if (strInput == "Grov")    {return "GRV";}
else if (strInput == "grov")    {return "GRV";}
else if (strInput == "GROVE")   {return "GRV";}
else if (strInput == "Grove")   {return "GRV";}
else if (strInput == "grove")   {return "GRV";}
else if (strInput == "GRV")     {return "GRV";}
else if (strInput == "Grv")     {return "GRV";}
else if (strInput == "grv")     {return "GRV";}

if      (strInput == "GRVS")    {return "GRVS";}
else if (strInput == "Grvs")    {return "GRVS";}
else if (strInput == "grvs")    {return "GRVS";}
else if (strInput == "GROVES")  {return "GRVS";}
else if (strInput == "Groves")  {return "GRVS";}
else if (strInput == "groves")  {return "GRVS";}

if      (strInput == "HARB")    {return "HBR";}
else if (strInput == "Harb")    {return "HBR";}
else if (strInput == "harb")    {return "HBR";}
else if (strInput == "HARBOR")  {return "HBR";}
else if (strInput == "Harbor")  {return "HBR";}
else if (strInput == "harbor")  {return "HBR";}
else if (strInput == "HARBR")   {return "HBR";}
else if (strInput == "Harbr")   {return "HBR";}
else if (strInput == "harbr")   {return "HBR";}
else if (strInput == "HBR")     {return "HBR";}
else if (strInput == "Hbr")     {return "HBR";}
else if (strInput == "hbr")     {return "HBR";}
else if (strInput == "HRBOR")   {return "HBR";}
else if (strInput == "Hrbor")   {return "HBR";}
else if (strInput == "hrbor")   {return "HBR";}

if      (strInput == "HBRS")     {return "HBRS";}
else if (strInput == "Hbrs")     {return "HBRS";}
else if (strInput == "hbrs")     {return "HBRS";}
else if (strInput == "HARBORS")  {return "HBRS";}
else if (strInput == "Harbors")  {return "HBRS";}
else if (strInput == "harbors")  {return "HBRS";}

if      (strInput == "HAVEN")     {return "HVN";}
else if (strInput == "Haven")     {return "HVN";}
else if (strInput == "haven")     {return "HVN";}
else if (strInput == "HVN")       {return "HVN";}
else if (strInput == "Hvn")       {return "HVN";}
else if (strInput == "hvn")       {return "HVN";}

if      (strInput == "HEIGHTS")   {return "HTS";}
else if (strInput == "Heights")   {return "HTS";}
else if (strInput == "heights")   {return "HTS";}
else if (strInput == "HT")        {return "HTS";}
else if (strInput == "Ht")        {return "HTS";}
else if (strInput == "ht")        {return "HTS";}
else if (strInput == "HTS")       {return "HTS";}
else if (strInput == "Hts")       {return "HTS";}
else if (strInput == "htx")       {return "HTS";}

if      (strInput == "HIGHWAY") {return "HWY";}
else if (strInput == "Highway") {return "HWY";}
else if (strInput == "highway") {return "HWY";}
else if (strInput == "HIGHWY")  {return "HWY";}
else if (strInput == "Highwy")  {return "HWY";}
else if (strInput == "highwy")  {return "HWY";}
else if (strInput == "HIWAY")   {return "HWY";}
else if (strInput == "Hiway")   {return "HWY";}
else if (strInput == "hiway")   {return "HWY";}
else if (strInput == "HIWY")    {return "HWY";}
else if (strInput == "Hiwy")    {return "HWY";}
else if (strInput == "hiwy")    {return "HWY";}
else if (strInput == "HWAY")    {return "HWY";}
else if (strInput == "Hway")    {return "HWY";}
else if (strInput == "hway")    {return "HWY";}
else if (strInput == "HWY")     {return "HWY";}
else if (strInput == "Hwy")     {return "HWY";}
else if (strInput == "hway")    {return "HWY";}

if      (strInput == "HILL")     {return "HL";}
else if (strInput == "Hill")     {return "HL";}
else if (strInput == "hill")     {return "HL";}
else if (strInput == "HL")       {return "HL";}
else if (strInput == "Hl")       {return "HL";}
else if (strInput == "hl")       {return "HL";}

if      (strInput == "HILLS")     {return "HLS";}
else if (strInput == "Hills")     {return "HLS";}
else if (strInput == "hills")     {return "HLS";}
else if (strInput == "HLS")       {return "HLS";}
else if (strInput == "Hls")       {return "HLS";}
else if (strInput == "hls")       {return "HLS";}

if      (strInput == "HLLW")    {return "HOLW";}
else if (strInput == "Hllw")    {return "HOLW";}
else if (strInput == "hllw")    {return "HOLW";}
else if (strInput == "HOLLOW")  {return "HOLW";}
else if (strInput == "Hollow")  {return "HOLW";}
else if (strInput == "hollow")  {return "HOLW";}
else if (strInput == "HOLLOWS") {return "HOLW";}
else if (strInput == "Hollows") {return "HOLW";}
else if (strInput == "hollows") {return "HOLW";}
else if (strInput == "HOLW")    {return "HOLW";}
else if (strInput == "Holw")    {return "HOLW";}
else if (strInput == "holw")    {return "HOLW";}
else if (strInput == "HOLWS")   {return "HOLW";}
else if (strInput == "Holws")   {return "HOLW";}
else if (strInput == "holws")   {return "HOLW";}

if      (strInput == "INLET")     {return "INLT";}
else if (strInput == "Inlet")     {return "INLT";}
else if (strInput == "inlet")     {return "INLT";}
else if (strInput == "INLT")      {return "INLT";}
else if (strInput == "Inlt")      {return "INLT";}
else if (strInput == "inlt")      {return "INLT";}

if      (strInput == "IS")      {return "IS";}
else if (strInput == "Is")      {return "IS";}
else if (strInput == "is")      {return "IS";}
else if (strInput == "ISLAND")  {return "IS";}
else if (strInput == "Island")  {return "IS";}
else if (strInput == "island")  {return "IS";}
else if (strInput == "ISLND")   {return "IS";}
else if (strInput == "Islnd")   {return "IS";}
else if (strInput == "islnd")   {return "IS";}

if      (strInput == "ISS")      {return "ISS";}
else if (strInput == "Iss")      {return "ISS";}
else if (strInput == "iss")      {return "ISS";}
else if (strInput == "ISLANDS")  {return "ISS";}
else if (strInput == "Islands")  {return "ISS";}
else if (strInput == "islands")  {return "ISS";}
else if (strInput == "ISLNDS")   {return "ISS";}
else if (strInput == "Islnds")   {return "ISS";}
else if (strInput == "islnds")   {return "ISS";}

if      (strInput == "ISLE")     {return "ISLE";}
else if (strInput == "Isle")     {return "ISLE";}
else if (strInput == "isle")     {return "ISLE";}
else if (strInput == "ISLES")    {return "ISLE";}
else if (strInput == "Isles")    {return "ISLE";}
else if (strInput == "isles")    {return "ISLE";}

if      (strInput == "JCT")     {return "JCT";}
else if (strInput == "Jct")     {return "JCT";}
else if (strInput == "jct")     {return "JCT";}
else if (strInput == "JCTION")  {return "JCT";}
else if (strInput == "Jction")  {return "JCT";}
else if (strInput == "jction")  {return "JCT";}
else if (strInput == "JCTN")    {return "JCT";}
else if (strInput == "Jctn")    {return "JCT";}
else if (strInput == "jctn")    {return "JCT";}
else if (strInput == "JUNCTION"){return "JCT";}
else if (strInput == "Junction"){return "JCT";}
else if (strInput == "junction"){return "JCT";}
else if (strInput == "JUNCTN")  {return "JCT";}
else if (strInput == "Junctn")  {return "JCT";}
else if (strInput == "junctn")  {return "JCT";}
else if (strInput == "JUNCTON") {return "JCT";}
else if (strInput == "Juncton") {return "JCT";}
else if (strInput == "juncton") {return "JCT";}

if      (strInput == "JCTS")         {return "JCTS";}
else if (strInput == "Jcts")         {return "JCTS";}
else if (strInput == "jcts")         {return "JCTS";}
else if (strInput == "JCTNS")        {return "JCTS";}
else if (strInput == "Jctns")        {return "JCTS";}
else if (strInput == "jctns")        {return "JCTS";}
else if (strInput == "JUNCTIONS")    {return "JCTS";}
else if (strInput == "Junctions")    {return "JCTS";}
else if (strInput == "junctions")    {return "JCTS";}

if      (strInput == "KY")         {return "KY";}
else if (strInput == "Ky")         {return "KY";}
else if (strInput == "ky")         {return "KY";}
else if (strInput == "KEY")        {return "KY";}
else if (strInput == "Key")        {return "KY";}
else if (strInput == "key")        {return "KY";}

if      (strInput == "KYS")         {return "KYS";}
else if (strInput == "Kys")         {return "KYS";}
else if (strInput == "kys")         {return "KYS";}
else if (strInput == "KEYS")        {return "KYS";}
else if (strInput == "Keys")        {return "KYS";}
else if (strInput == "keys")        {return "KYS";}

if      (strInput == "KNL")         {return "KNL";}
else if (strInput == "Knl")         {return "KNL";}
else if (strInput == "knl")         {return "KNL";}
else if (strInput == "KNOL")        {return "KNL";}
else if (strInput == "Knol")        {return "KNL";}
else if (strInput == "knol")        {return "KNL";}
else if (strInput == "KNOLL")       {return "KNL";}
else if (strInput == "Knoll")       {return "KNL";}
else if (strInput == "knoll")       {return "KNL";}

if      (strInput == "KNLS")         {return "KNLS";}
else if (strInput == "Knls")         {return "KNLS";}
else if (strInput == "knls")         {return "KNLS";}
else if (strInput == "KNOLLS")       {return "KNLS";}
else if (strInput == "Knolls")       {return "KNLS";}
else if (strInput == "knolls")       {return "KNLS";}

if      (strInput == "LK")         {return "LK";}
else if (strInput == "Lk")         {return "LK";}
else if (strInput == "lk")         {return "LK";}
else if (strInput == "LAKE")       {return "LK";}
else if (strInput == "Lake")       {return "LK";}
else if (strInput == "lake")       {return "LK";}

if      (strInput == "LKS")         {return "LKS";}
else if (strInput == "Lks")         {return "LKS";}
else if (strInput == "lks")         {return "LKS";}
else if (strInput == "LAKES")       {return "LKS";}
else if (strInput == "Lakes")       {return "LKS";}
else if (strInput == "lakes")       {return "LKS";}

if      (strInput == "LAND")         {return "LAND";}
else if (strInput == "Land")         {return "LAND";}
else if (strInput == "land")         {return "LAND";}

if      (strInput == "LANDING")    {return "LNDG";}
else if (strInput == "Landing")    {return "LNDG";}
else if (strInput == "landing")    {return "LNDG";}
else if (strInput == "LNDG")       {return "LNDG";}
else if (strInput == "Lndg")       {return "LNDG";}
else if (strInput == "lndg")       {return "LNDG";}
else if (strInput == "LNDNG")      {return "LNDG";}
else if (strInput == "Lndng")      {return "LNDG";}
else if (strInput == "lndng")      {return "LNDG";}

if      (strInput == "LANE")    {return "LN";}
else if (strInput == "Lane")    {return "LN";}
else if (strInput == "lane")    {return "LN";}
else if (strInput == "LN")      {return "LN";}
else if (strInput == "Ln")      {return "LN";}
else if (strInput == "ln")      {return "LN";}

if      (strInput == "LIGHT")    {return "LGT";}
else if (strInput == "Light")    {return "LGT";}
else if (strInput == "light")    {return "LGT";}
else if (strInput == "LGT")      {return "LGT";}
else if (strInput == "Lgt")      {return "LGT";}
else if (strInput == "lgt")      {return "LGT";}

if      (strInput == "LIGHTS")    {return "LGTS";}
else if (strInput == "Lights")    {return "LGTS";}
else if (strInput == "lights")    {return "LGTS";}
else if (strInput == "LGTS")      {return "LGTS";}
else if (strInput == "Lgts")      {return "LGTS";}
else if (strInput == "lgts")      {return "LGTS";}

if      (strInput == "LOAF")    {return "LF";}
else if (strInput == "Loaf")    {return "LF";}
else if (strInput == "loaf")    {return "LF";}
else if (strInput == "LF")      {return "LF";}
else if (strInput == "Lf")      {return "LF";}
else if (strInput == "lf")      {return "LF";}

if      (strInput == "LOCK")    {return "LCK";}
else if (strInput == "Lock")    {return "LCK";}
else if (strInput == "lock")    {return "LCK";}
else if (strInput == "LCK")     {return "LCK";}
else if (strInput == "Lck")     {return "LCK";}
else if (strInput == "lck")     {return "LCK";}

if      (strInput == "LOCKS")    {return "LCKS";}
else if (strInput == "Locks")    {return "LCKS";}
else if (strInput == "locks")    {return "LCKS";}
else if (strInput == "LCKS")     {return "LCKS";}
else if (strInput == "Lcks")     {return "LCKS";}
else if (strInput == "lcks")     {return "LCKS";}

if      (strInput == "LDG")    {return "LDG";}
else if (strInput == "Ldg")    {return "LDG";}
else if (strInput == "ldg")    {return "LDG";}
else if (strInput == "LDGE")   {return "LDG";}
else if (strInput == "Ldge")   {return "LDG";}
else if (strInput == "ldge")   {return "LDG";}
else if (strInput == "LODG")   {return "LDG";}
else if (strInput == "Lodg")   {return "LDG";}
else if (strInput == "lodg")   {return "LDG";}
else if (strInput == "LODGE")  {return "LDG";}
else if (strInput == "Lodge")  {return "LDG";}
else if (strInput == "lodge")  {return "LDG";}

if      (strInput == "LOOP")    {return "LOOP";}
else if (strInput == "Loop")    {return "LOOP";}
else if (strInput == "loop")    {return "LOOP";}
else if (strInput == "LOOPS")   {return "LOOP";}
else if (strInput == "Loops")   {return "LOOP";}
else if (strInput == "loops")   {return "LOOP";}

if      (strInput == "MALL")    {return "MALL";}
else if (strInput == "Mall")    {return "MALL";}
else if (strInput == "mall")    {return "MALL";}

if      (strInput == "MANOR")    {return "MNR";}
else if (strInput == "Manor")    {return "MNR";}
else if (strInput == "manor")    {return "MNR";}
else if (strInput == "MNR")      {return "MNR";}
else if (strInput == "Mnr")      {return "MNR";}
else if (strInput == "mnr")      {return "MNR";}

if      (strInput == "MANORS")    {return "MNRS";}
else if (strInput == "Manors")    {return "MNRS";}
else if (strInput == "manors")    {return "MNRS";}
else if (strInput == "MNRS")      {return "MNRS";}
else if (strInput == "Mnrs")      {return "MNRS";}
else if (strInput == "mnrs")      {return "MNRS";}

if      (strInput == "MEADOW")    {return "MDW";}
else if (strInput == "Meadow")    {return "MDW";}
else if (strInput == "meadow")    {return "MDW";}
else if (strInput == "MDW")       {return "MDW";}
else if (strInput == "Mdw")       {return "MDW";}
else if (strInput == "mdw")       {return "MDW";}

if      (strInput == "MEADOWS")    {return "MDWS";}
else if (strInput == "Meadows")    {return "MDWS";}
else if (strInput == "meadows")    {return "MDWS";}
else if (strInput == "MDWS")       {return "MDWS";}
else if (strInput == "Mdws")       {return "MDWS";}
else if (strInput == "mdws")       {return "MDWS";}
else if (strInput == "MEDOWS")     {return "MDWS";}
else if (strInput == "Medows")     {return "MDWS";}
else if (strInput == "medows")     {return "MDWS";}

if      (strInput == "MEWS")    {return "MEWS";}
else if (strInput == "Mews")    {return "MEWS";}
else if (strInput == "mews")    {return "MEWS";}

if      (strInput == "MILL")    {return "ML";}
else if (strInput == "Mill")    {return "ML";}
else if (strInput == "mill")    {return "ML";}
else if (strInput == "ML")      {return "ML";}
else if (strInput == "Ml")      {return "ML";}
else if (strInput == "ml")      {return "ML";}

if      (strInput == "MILLS")    {return "MLS";}
else if (strInput == "Mills")    {return "MLS";}
else if (strInput == "mills")    {return "MLS";}
else if (strInput == "MLS")      {return "MLS";}
else if (strInput == "Mls")      {return "MLS";}
else if (strInput == "mls")      {return "MLS";}

if      (strInput == "MISSION")    {return "MSN";}
else if (strInput == "Mission")    {return "MSN";}
else if (strInput == "mission")    {return "MSN";}
else if (strInput == "MISSN")      {return "MSN";}
else if (strInput == "Missn")      {return "MSN";}
else if (strInput == "missn")      {return "MSN";}
else if (strInput == "MSSN")       {return "MSN";}
else if (strInput == "Mssn")       {return "MSN";}
else if (strInput == "mssn")       {return "MSN";}
else if (strInput == "MSN")        {return "MSN";}
else if (strInput == "Msn")        {return "MSN";}
else if (strInput == "msn")        {return "MSN";}

if      (strInput == "MOTORWAY")    {return "MTWY";}
else if (strInput == "Motorway")    {return "MTWY";}
else if (strInput == "motorway")    {return "MTWY";}
else if (strInput == "MTWY")        {return "MTWY";}
else if (strInput == "Mtwy")        {return "MTWY";}
else if (strInput == "mtwy")        {return "MTWY";}

if      (strInput == "MOUNT")    {return "MT";}
else if (strInput == "Mount")    {return "MT";}
else if (strInput == "mount")    {return "MT";}
else if (strInput == "MNT")      {return "MT";}
else if (strInput == "Mnt")      {return "MT";}
else if (strInput == "mnt")      {return "MT";}
else if (strInput == "MT")       {return "MT";}
else if (strInput == "Mt")       {return "MT";}
else if (strInput == "mt")       {return "MT";}

if      (strInput == "MNTAIN")    {return "MTN";}
else if (strInput == "Mntain")    {return "MTN";}
else if (strInput == "mntain")    {return "MTN";}
else if (strInput == "MNTN")      {return "MTN";}
else if (strInput == "Mntn")      {return "MTN";}
else if (strInput == "mntn")      {return "MTN";}
else if (strInput == "MOUNTAIN")  {return "MTN";}
else if (strInput == "Mountain")  {return "MTN";}
else if (strInput == "mountain")  {return "MTN";}
else if (strInput == "MOUNTIN")   {return "MTN";}
else if (strInput == "Mountin")   {return "MTN";}
else if (strInput == "mountin")   {return "MTN";}
else if (strInput == "MTIN")      {return "MTN";}
else if (strInput == "Mtin")      {return "MTN";}
else if (strInput == "mtin")      {return "MTN";}
else if (strInput == "MTN")       {return "MTN";}
else if (strInput == "Mtn")       {return "MTN";}
else if (strInput == "mtn")       {return "MTN";}

if      (strInput == "MTNS")       {return "MTNS";}
else if (strInput == "Mtns")       {return "MTNS";}
else if (strInput == "mtns")       {return "MTNS";}
else if (strInput == "MNTNS")      {return "MTNS";}
else if (strInput == "Mntns")      {return "MTNS";}
else if (strInput == "mntns")      {return "MTNS";}
else if (strInput == "MOUNTAINS")  {return "MTNS";}
else if (strInput == "Mountains")  {return "MTNS";}
else if (strInput == "mountains")  {return "MTNS";}

if      (strInput == "NECK")       {return "NCK";}
else if (strInput == "Neck")       {return "NCK";}
else if (strInput == "neck")       {return "NCK";}
else if (strInput == "NCK")        {return "NCK";}
else if (strInput == "Nck")        {return "NCK";}
else if (strInput == "nck")        {return "NCK";}

if      (strInput == "ORCH")       {return "ORCH";}
else if (strInput == "Orch")       {return "ORCH";}
else if (strInput == "orch")       {return "ORCH";}
else if (strInput == "ORCHARD")    {return "ORCH";}
else if (strInput == "Orchard")    {return "ORCH";}
else if (strInput == "orchard")    {return "ORCH";}
else if (strInput == "ORCHRD")     {return "ORCH";}
else if (strInput == "Orchrd")     {return "ORCH";}
else if (strInput == "orchrd")     {return "ORCH";}

if      (strInput == "OVAL")       {return "OVAL";}
else if (strInput == "Oval")       {return "OVAL";}
else if (strInput == "oval")       {return "OVAL";}
else if (strInput == "OVL")        {return "OVAL";}
else if (strInput == "Ovl")        {return "OVAL";}
else if (strInput == "ovl")        {return "OVAL";}

if      (strInput == "OVERPASS")    {return "OPAS";}
else if (strInput == "Overpass")    {return "OPAS";}
else if (strInput == "overpass")    {return "OPAS";}
else if (strInput == "OPAS")        {return "OPAS";}
else if (strInput == "Opas")        {return "OPAS";}
else if (strInput == "opas")        {return "OPAS";}

if      (strInput == "PARK")    {return "PARK";}
else if (strInput == "Park")    {return "PARK";}
else if (strInput == "park")    {return "PARK";}
else if (strInput == "PRK")     {return "PARK";}
else if (strInput == "Prk")     {return "PARK";}
else if (strInput == "prk")     {return "PARK";}
else if (strInput == "PARKS")   {return "PARK";}
else if (strInput == "Parks")   {return "PARK";}
else if (strInput == "parks")   {return "PARK";}

if      (strInput == "PARKWAY")  {return "PKWY";}
else if (strInput == "Parkway")  {return "PKWY";}
else if (strInput == "parkway")  {return "PKWY";}
else if (strInput == "PARKWY")   {return "PKWY";}
else if (strInput == "Parkwy")   {return "PKWY";}
else if (strInput == "parkwy")   {return "PKWY";}
else if (strInput == "PKWAY")    {return "PKWY";}
else if (strInput == "Pkway")    {return "PKWY";}
else if (strInput == "pkway")    {return "PKWY";}
else if (strInput == "PKWY")     {return "PKWY";}
else if (strInput == "Pkwy")     {return "PKWY";}
else if (strInput == "pkwy")     {return "PKWY";}
else if (strInput == "PKY")      {return "PKWY";}
else if (strInput == "Pky")      {return "PKWY";}
else if (strInput == "pky")      {return "PKWY";}
else if (strInput == "PARKWAYS") {return "PKWY";}
else if (strInput == "Parkways") {return "PKWY";}
else if (strInput == "parkways") {return "PKWY";}
else if (strInput == "PKWYS")    {return "PKWY";}
else if (strInput == "Pkwys")    {return "PKWY";}
else if (strInput == "pkwys")    {return "PKWY";}

if      (strInput == "PASS")    {return "PASS";}
else if (strInput == "Pass")    {return "PASS";}
else if (strInput == "pass")    {return "PASS";}

if      (strInput == "PASSAGE")    {return "PSGE";}
else if (strInput == "Passage")    {return "PSGE";}
else if (strInput == "passage")    {return "PSGE";}
else if (strInput == "PSGE")       {return "PSGE";}
else if (strInput == "Psge")       {return "PSGE";}
else if (strInput == "psge")       {return "PSGE";}

if      (strInput == "PATH")    {return "PATH";}
else if (strInput == "Path")    {return "PATH";}
else if (strInput == "path")    {return "PATH";}
else if (strInput == "PATHS")   {return "PATH";}
else if (strInput == "Paths")   {return "PATH";}
else if (strInput == "paths")   {return "PATH";}

if      (strInput == "PIKE")    {return "PIKE";}
else if (strInput == "Pike")    {return "PIKE";}
else if (strInput == "pike")    {return "PIKE";}
else if (strInput == "PIKES")   {return "PIKE";}
else if (strInput == "Pikes")   {return "PIKE";}
else if (strInput == "pikes")   {return "PIKE";}

if      (strInput == "PINE")    {return "PNE";}
else if (strInput == "Pine")    {return "PNE";}
else if (strInput == "pine")    {return "PNE";}
else if (strInput == "PNE")     {return "PNE";}
else if (strInput == "Pne")     {return "PNE";}
else if (strInput == "pne")     {return "PNE";}

if      (strInput == "PINES")    {return "PNES";}
else if (strInput == "Pines")    {return "PNES";}
else if (strInput == "pines")    {return "PNES";}
else if (strInput == "PNES")     {return "PNES";}
else if (strInput == "Pnes")     {return "PNES";}
else if (strInput == "pnes")     {return "PNES";}

if      (strInput == "PLACE")    {return "PL";}
else if (strInput == "Place")    {return "PL";}
else if (strInput == "place")    {return "PL";}
else if (strInput == "PL")       {return "PL";}
else if (strInput == "Pl")       {return "PL";}
else if (strInput == "pl")       {return "PL";}

if      (strInput == "PLAIN")    {return "PLN";}
else if (strInput == "Plain")    {return "PLN";}
else if (strInput == "plain")    {return "PLN";}
else if (strInput == "PLN")      {return "PLN";}
else if (strInput == "Pln")      {return "PLN";}
else if (strInput == "pln")      {return "PLN";}

if      (strInput == "PLAINS")    {return "PLNS";}
else if (strInput == "Plains")    {return "PLNS";}
else if (strInput == "plains")    {return "PLNS";}
else if (strInput == "PLNS")      {return "PLNS";}
else if (strInput == "Plns")      {return "PLNS";}
else if (strInput == "plns")      {return "PLNS";}

if      (strInput == "PLAZA")    {return "PLZ";}
else if (strInput == "Plaza")    {return "PLZ";}
else if (strInput == "plaza")    {return "PLZ";}
else if (strInput == "PLZ")      {return "PLZ";}
else if (strInput == "Plz")      {return "PLZ";}
else if (strInput == "plz")      {return "PLZ";}
else if (strInput == "PLZA")     {return "PLZ";}
else if (strInput == "Plza")     {return "PLZ";}
else if (strInput == "plza")     {return "PLZ";}

if      (strInput == "POINT")   {return "PT";}
else if (strInput == "Point")   {return "PT";}
else if (strInput == "point")   {return "PT";}
else if (strInput == "PT")      {return "PT";}
else if (strInput == "Pt")      {return "PT";}
else if (strInput == "pt")      {return "PT";}

if      (strInput == "POINTS")   {return "PTS";}
else if (strInput == "Points")   {return "PTS";}
else if (strInput == "points")   {return "PTS";}
else if (strInput == "PTS")      {return "PTS";}
else if (strInput == "Pts")      {return "PTS";}
else if (strInput == "pts")      {return "PTS";}

if      (strInput == "PORT")   {return "PRT";}
else if (strInput == "Port")   {return "PRT";}
else if (strInput == "port")   {return "PRT";}
else if (strInput == "PRT")    {return "PRT";}
else if (strInput == "Prt")    {return "PRT";}
else if (strInput == "prt")    {return "PRT";}

if      (strInput == "PORTS")   {return "PRTS";}
else if (strInput == "Ports")   {return "PRTS";}
else if (strInput == "ports")   {return "PRTS";}
else if (strInput == "PRTS")    {return "PRTS";}
else if (strInput == "Prts")    {return "PRTS";}
else if (strInput == "prts")    {return "PRTS";}

if      (strInput == "PRAIRIE")   {return "PR";}
else if (strInput == "Prairie")   {return "PR";}
else if (strInput == "prairie")   {return "PR";}
else if (strInput == "PRR")       {return "PR";}
else if (strInput == "Prr")       {return "PR";}
else if (strInput == "prr")       {return "PR";}
else if (strInput == "PR")        {return "PR";}
else if (strInput == "Pr")        {return "PR";}
else if (strInput == "pr")        {return "PR";}

if      (strInput == "RAD")          {return "RADL";}
else if (strInput == "Rad")          {return "RADL";}
else if (strInput == "rad")          {return "RADL";}
else if (strInput == "RADIAL")       {return "RADL";}
else if (strInput == "Radial")       {return "RADL";}
else if (strInput == "radial")       {return "RADL";}
else if (strInput == "RADIEL")       {return "RADL";}
else if (strInput == "Radiel")       {return "RADL";}
else if (strInput == "radiel")       {return "RADL";}
else if (strInput == "RADL")         {return "RADL";}
else if (strInput == "Radl")         {return "RADL";}
else if (strInput == "radl")         {return "RADL";}

if      (strInput == "RAMP")   {return "RAMP";}
else if (strInput == "Ramp")   {return "RAMP";}
else if (strInput == "ramp")   {return "RAMP";}

if      (strInput == "RANCH")         {return "RNCH";}
else if (strInput == "Ranch")         {return "RNCH";}
else if (strInput == "ranch")         {return "RNCH";}
else if (strInput == "RANCHES")       {return "RNCH";}
else if (strInput == "Ranches")       {return "RNCH";}
else if (strInput == "ranches")       {return "RNCH";}
else if (strInput == "RNCH")          {return "RNCH";}
else if (strInput == "Rnch")          {return "RNCH";}
else if (strInput == "rnch")          {return "RNCH";}
else if (strInput == "RNCHS")         {return "RNCH";}
else if (strInput == "Rnchs")         {return "RNCH";}
else if (strInput == "rnchs")         {return "RNCH";}

if      (strInput == "RAPID")     {return "RPD";}
else if (strInput == "Rapid")     {return "RPD";}
else if (strInput == "rapid")     {return "RPD";}
else if (strInput == "RPD")       {return "RPD";}
else if (strInput == "Rpd")       {return "RPD";}
else if (strInput == "rpd")       {return "RPD";}

if      (strInput == "RAPIDS")     {return "RPDS";}
else if (strInput == "Rapids")     {return "RPDS";}
else if (strInput == "rapids")     {return "RPDS";}
else if (strInput == "RPDS")       {return "RPDS";}
else if (strInput == "Rpds")       {return "RPDS";}
else if (strInput == "rpds")       {return "RPDS";}

if      (strInput == "REST")     {return "RST";}
else if (strInput == "Rest")     {return "RST";}
else if (strInput == "rest")     {return "RST";}
else if (strInput == "RST")      {return "RST";}
else if (strInput == "Rst")      {return "RST";}
else if (strInput == "rst")      {return "RST";}

if      (strInput == "RIDGE")     {return "RDG";}
else if (strInput == "Ridge")     {return "RDG";}
else if (strInput == "ridge")     {return "RDG";}
else if (strInput == "RDGE")      {return "RDG";}
else if (strInput == "Rdge")      {return "RDG";}
else if (strInput == "rdge")      {return "RDG";}
else if (strInput == "RDG")       {return "RDG";}
else if (strInput == "Rdg")       {return "RDG";}
else if (strInput == "rdg")       {return "RDG";}

if      (strInput == "RIDGES")     {return "RDGS";}
else if (strInput == "Ridges")     {return "RDGS";}
else if (strInput == "ridges")     {return "RDGS";}
else if (strInput == "RDGS")       {return "RDGS";}
else if (strInput == "Rdgs")       {return "RDGS";}
else if (strInput == "rdgs")       {return "RDGS";}

if      (strInput == "RIV")     {return "RIV";}
else if (strInput == "Riv")     {return "RIV";}
else if (strInput == "riv")     {return "RIV";}
else if (strInput == "RIVER")   {return "RIV";}
else if (strInput == "River")   {return "RIV";}
else if (strInput == "river")   {return "RIV";}
else if (strInput == "RVR")     {return "RIV";}
else if (strInput == "Rvr")     {return "RIV";}
else if (strInput == "rvr")     {return "RIV";}
else if (strInput == "RIVR")    {return "RIV";}
else if (strInput == "Rivr")    {return "RIV";}
else if (strInput == "rivr")    {return "RIV";}

if      (strInput == "ROAD")     {return "RD";}
else if (strInput == "Road")     {return "RD";}
else if (strInput == "road")     {return "RD";}
else if (strInput == "RD")       {return "RD";}
else if (strInput == "Rd")       {return "RD";}
else if (strInput == "rd")       {return "RD";}

if      (strInput == "ROADS")     {return "RDS";}
else if (strInput == "Roads")     {return "RDS";}
else if (strInput == "roads")     {return "RDS";}
else if (strInput == "RDS")       {return "RDS";}
else if (strInput == "Rds")       {return "RDS";}
else if (strInput == "rds")       {return "RDS";}

if      (strInput == "ROUTE")     {return "RTE";}
else if (strInput == "Route")     {return "RTE";}
else if (strInput == "route")     {return "RTE";}
else if (strInput == "RTE")       {return "RTE";}
else if (strInput == "Rte")       {return "RTE";}
else if (strInput == "rte")       {return "RTE";}

if      (strInput == "ROW")     {return "ROW";}
else if (strInput == "Row")     {return "ROW";}
else if (strInput == "row")     {return "ROW";}

if      (strInput == "RUE")     {return "RUE";}
else if (strInput == "Rue")     {return "RUE";}
else if (strInput == "rue")     {return "RUE";}

if      (strInput == "RUN")     {return "RUN";}
else if (strInput == "Run")     {return "RUN";}
else if (strInput == "run")     {return "RUN";}

if      (strInput == "SHOAL")     {return "SHL";}
else if (strInput == "Shoal")     {return "SHL";}
else if (strInput == "shoal")     {return "SHL";}
else if (strInput == "SHL")       {return "SHL";}
else if (strInput == "Shl")       {return "SHL";}
else if (strInput == "shl")       {return "SHL";}

if      (strInput == "SHOALS")     {return "SHLS";}
else if (strInput == "Shoals")     {return "SHLS";}
else if (strInput == "shoals")     {return "SHLS";}
else if (strInput == "SHLS")       {return "SHLS";}
else if (strInput == "Shls")       {return "SHLS";}
else if (strInput == "shls")       {return "SHLS";}

if      (strInput == "SHOAR")     {return "SHR";}
else if (strInput == "Shoar")     {return "SHR";}
else if (strInput == "shoar")     {return "SHR";}
else if (strInput == "SHORE")     {return "SHR";}
else if (strInput == "Shore")     {return "SHR";}
else if (strInput == "shore")     {return "SHR";}
else if (strInput == "SHR")       {return "SHR";}
else if (strInput == "Shr")       {return "SHR";}
else if (strInput == "shr")       {return "SHR";}

if      (strInput == "SHOARS")     {return "SHRS";}
else if (strInput == "Shoars")     {return "SHRS";}
else if (strInput == "shoars")     {return "SHRS";}
else if (strInput == "SHORES")     {return "SHRS";}
else if (strInput == "Shores")     {return "SHRS";}
else if (strInput == "shores")     {return "SHRS";}
else if (strInput == "SHRS")       {return "SHRS";}
else if (strInput == "Shrs")       {return "SHRS";}
else if (strInput == "shrs")       {return "SHRS";}

if      (strInput == "SKYWAY")     {return "SKWY";}
else if (strInput == "Skyway")     {return "SKWY";}
else if (strInput == "skyway")     {return "SKWY";}
else if (strInput == "SKWY")       {return "SKWY";}
else if (strInput == "Skwy")       {return "SKWY";}
else if (strInput == "skwy")       {return "SKWY";}

if      (strInput == "SPRING")     {return "SPG";}
else if (strInput == "Spring")     {return "SPG";}
else if (strInput == "spring")     {return "SPG";}
else if (strInput == "SPRNG")      {return "SPG";}
else if (strInput == "Sprng")      {return "SPG";}
else if (strInput == "sprng")      {return "SPG";}
else if (strInput == "SPNG")       {return "SPG";}
else if (strInput == "Spng")       {return "SPG";}
else if (strInput == "spng")       {return "SPG";}
else if (strInput == "SPG")        {return "SPG";}
else if (strInput == "Spg")        {return "SPG";}
else if (strInput == "spg")        {return "SPG";}

if      (strInput == "SPRINGS")     {return "SPGS";}
else if (strInput == "Springs")     {return "SPGS";}
else if (strInput == "springs")     {return "SPGS";}
else if (strInput == "SPRNGS")      {return "SPGS";}
else if (strInput == "Sprngs")      {return "SPGS";}
else if (strInput == "sprngs")      {return "SPGS";}
else if (strInput == "SPNGS")       {return "SPGS";}
else if (strInput == "Spngs")       {return "SPGS";}
else if (strInput == "spngs")       {return "SPGS";}
else if (strInput == "SPGS")        {return "SPGS";}
else if (strInput == "Spgs")        {return "SPGS";}
else if (strInput == "spgs")        {return "SPGS";}

if      (strInput == "SPUR")     {return "SPUR";}
else if (strInput == "Spur")     {return "SPUR";}
else if (strInput == "spur")     {return "SPUR";}
else if (strInput == "SPURS")    {return "SPUR";}
else if (strInput == "Spurs")    {return "SPUR";}
else if (strInput == "spurs")    {return "SPUR";}

if      (strInput == "SQ")     {return "SQ";}
else if (strInput == "Sq")     {return "SQ";}
else if (strInput == "sq")     {return "SQ";}
else if (strInput == "SQR")    {return "SQ";}
else if (strInput == "Sqr")    {return "SQ";}
else if (strInput == "sqr")    {return "SQ";}
else if (strInput == "SQRE")   {return "SQ";}
else if (strInput == "Sqre")   {return "SQ";}
else if (strInput == "sqre")   {return "SQ";}
else if (strInput == "SQU")    {return "SQ";}
else if (strInput == "Squ")    {return "SQ";}
else if (strInput == "squ")    {return "SQ";}
else if (strInput == "SQUARE") {return "SQ";}
else if (strInput == "Square") {return "SQ";}
else if (strInput == "square") {return "SQ";}

if      (strInput == "SQRS")     {return "SQS";}
else if (strInput == "Sqrs")     {return "SQS";}
else if (strInput == "sqrs")     {return "SQS";}
else if (strInput == "SQS")      {return "SQS";}
else if (strInput == "Sqs")      {return "SQS";}
else if (strInput == "sqs")      {return "SQS";}
else if (strInput == "SQUARES")  {return "SQS";}
else if (strInput == "Squares")  {return "SQS";}
else if (strInput == "squares")  {return "SQS";}

if      (strInput == "STA")     {return "STA";}
else if (strInput == "Sta")     {return "STA";}
else if (strInput == "sta")     {return "STA";}
else if (strInput == "STATION") {return "STA";}
else if (strInput == "Station") {return "STA";}
else if (strInput == "station") {return "STA";}
else if (strInput == "STATN")   {return "STA";}
else if (strInput == "Statn")   {return "STA";}
else if (strInput == "statn")   {return "STA";}
else if (strInput == "STN")     {return "STA";}
else if (strInput == "Stn")     {return "STA";}
else if (strInput == "stn")     {return "STA";}

if      (strInput == "STRA")      {return "STRA";}
else if (strInput == "Stra")      {return "STRA";}
else if (strInput == "stra")      {return "STRA";}
else if (strInput == "STRAV")     {return "STRA";}
else if (strInput == "Strav")     {return "STRA";}
else if (strInput == "strav")     {return "STRA";}
else if (strInput == "STRAVEN")   {return "STRA";}
else if (strInput == "Straven")   {return "STRA";}
else if (strInput == "straven")   {return "STRA";}
else if (strInput == "STRAVENUE") {return "STRA";}
else if (strInput == "Stravenue") {return "STRA";}
else if (strInput == "stravenue") {return "STRA";}
else if (strInput == "STRAVN")    {return "STRA";}
else if (strInput == "Stravn")    {return "STRA";}
else if (strInput == "stravn")    {return "STRA";}
else if (strInput == "STRVN")     {return "STRA";}
else if (strInput == "Strvn")     {return "STRA";}
else if (strInput == "strvn")     {return "STRA";}
else if (strInput == "STRVNUE")   {return "STRA";}
else if (strInput == "Strvnue")   {return "STRA";}
else if (strInput == "strvnue")   {return "STRA";}

if      (strInput == "STREAM") {return "STRM";}
else if (strInput == "Stream") {return "STRM";}
else if (strInput == "stream") {return "STRM";}
else if (strInput == "STREME") {return "STRM";}
else if (strInput == "Streme") {return "STRM";}
else if (strInput == "streme") {return "STRM";}
else if (strInput == "STRM")   {return "STRM";}
else if (strInput == "Strm")   {return "STRM";}
else if (strInput == "strm")   {return "STRM";}

if      (strInput == "STREET")     {return "ST";}
else if (strInput == "Street")     {return "ST";}
else if (strInput == "street")     {return "ST";}
else if (strInput == "STRT")       {return "ST";}
else if (strInput == "Strt")       {return "ST";}
else if (strInput == "strt")       {return "ST";}
else if (strInput == "ST")         {return "ST";}
else if (strInput == "St")         {return "ST";}
else if (strInput == "st")         {return "ST";}
else if (strInput == "STR")        {return "ST";}
else if (strInput == "Str")        {return "ST";}
else if (strInput == "str")        {return "ST";}

if      (strInput == "STREETS")     {return "STS";}
else if (strInput == "Streets")     {return "STS";}
else if (strInput == "streets")     {return "STS";}
else if (strInput == "STS")         {return "STS";}
else if (strInput == "Sts")         {return "STS";}
else if (strInput == "sts")         {return "STS";}

if      (strInput == "SMT")     {return "SMT";}
else if (strInput == "Smt")     {return "SMT";}
else if (strInput == "smt")     {return "SMT";}
else if (strInput == "SUMIT")   {return "SMT";}
else if (strInput == "Sumit")   {return "SMT";}
else if (strInput == "sumit")   {return "SMT";}
else if (strInput == "SUMITT")  {return "SMT";}
else if (strInput == "Sumitt")  {return "SMT";}
else if (strInput == "sumitt")  {return "SMT";}
else if (strInput == "SUMMIT")  {return "SMT";}
else if (strInput == "Summit")  {return "SMT";}
else if (strInput == "summit")  {return "SMT";}

if      (strInput == "TER")      {return "TER";}
else if (strInput == "Ter")      {return "TER";}
else if (strInput == "ter")      {return "TER";}
else if (strInput == "TERR")     {return "TER";}
else if (strInput == "Terr")     {return "TER";}
else if (strInput == "terr")     {return "TER";}
else if (strInput == "TERRACE")  {return "TER";}
else if (strInput == "Terrace")  {return "TER";}
else if (strInput == "terrace")  {return "TER";}

if      (strInput == "TRWY")           {return "TRWY";}
else if (strInput == "Trwy")           {return "TRWY";}
else if (strInput == "trwy")           {return "TRWY";}
else if (strInput == "THROUGHWAY")     {return "TRWY";}
else if (strInput == "Throughway")     {return "TRWY";}
else if (strInput == "throughway")     {return "TRWY";}

if      (strInput == "TRACE")      {return "TRCE";}
else if (strInput == "Trace")      {return "TRCE";}
else if (strInput == "trace")      {return "TRCE";}
else if (strInput == "TRACES")     {return "TRCE";}
else if (strInput == "Traces")     {return "TRCE";}
else if (strInput == "traces")     {return "TRCE";}
else if (strInput == "TRCE")       {return "TRCE";}
else if (strInput == "Trce")       {return "TRCE";}
else if (strInput == "trce")       {return "TRCE";}

if      (strInput == "TRACK")      {return "TRAK";}
else if (strInput == "Track")      {return "TRAK";}
else if (strInput == "track")      {return "TRAK";}
else if (strInput == "TRACKS")     {return "TRAK";}
else if (strInput == "Tracks")     {return "TRAK";}
else if (strInput == "tracks")     {return "TRAK";}
else if (strInput == "TRAK")       {return "TRAK";}
else if (strInput == "Trak")       {return "TRAK";}
else if (strInput == "trak")       {return "TRAK";}
else if (strInput == "TRK")        {return "TRAK";}
else if (strInput == "Trk")        {return "TRAK";}
else if (strInput == "trk")        {return "TRAK";}
else if (strInput == "TRKS")       {return "TRAK";}
else if (strInput == "Trks")       {return "TRAK";}
else if (strInput == "trks")       {return "TRAK";}

if      (strInput == "TRFY")           {return "TRFY";}
else if (strInput == "Trfy")           {return "TRFY";}
else if (strInput == "trfy")           {return "TRFY";}
else if (strInput == "TRAFFICWAY")     {return "TRFY";}
else if (strInput == "Trafficway")     {return "TRFY";}
else if (strInput == "trafficway")     {return "TRFY";}

if      (strInput == "TRAIL")      {return "TRL";}
else if (strInput == "Trail")      {return "TRL";}
else if (strInput == "trail")      {return "TRL";}
else if (strInput == "TRAILS")     {return "TRL";}
else if (strInput == "Trails")     {return "TRL";}
else if (strInput == "trails")     {return "TRL";}
else if (strInput == "TRL")        {return "TRL";}
else if (strInput == "Trl")        {return "TRL";}
else if (strInput == "trl")        {return "TRL";}
else if (strInput == "TRLS")       {return "TRL";}
else if (strInput == "Trls")       {return "TRL";}
else if (strInput == "trls")       {return "TRL";}

if      (strInput == "TRAILER")      {return "TRLR";}
else if (strInput == "Trailer")      {return "TRLR";}
else if (strInput == "trailer")      {return "TRLR";}
else if (strInput == "TRLR")         {return "TRLR";}
else if (strInput == "Trlr")         {return "TRLR";}
else if (strInput == "trlr")         {return "TRLR";}
else if (strInput == "TRLRS")        {return "TRLR";}
else if (strInput == "Trlrs")        {return "TRLR";}
else if (strInput == "trlrs")        {return "TRLR";}

if      (strInput == "TUNEL")    {return "TUNL";}
else if (strInput == "Tunel")    {return "TUNL";}
else if (strInput == "tunel")    {return "TUNL";}
else if (strInput == "TUNL")     {return "TUNL";}
else if (strInput == "Tunl")     {return "TUNL";}
else if (strInput == "tunl")     {return "TUNL";}
else if (strInput == "TUNLS")    {return "TUNL";}
else if (strInput == "Tunls")    {return "TUNL";}
else if (strInput == "tunls")    {return "TUNL";}
else if (strInput == "TUNNEL")   {return "TUNL";}
else if (strInput == "Tunnel")   {return "TUNL";}
else if (strInput == "tunnel")   {return "TUNL";}
else if (strInput == "TUNNELS")  {return "TUNL";}
else if (strInput == "Tunnels")  {return "TUNL";}
else if (strInput == "tunnels")  {return "TUNL";}
else if (strInput == "TUNNL")    {return "TUNL";}
else if (strInput == "Tunnl")    {return "TUNL";}
else if (strInput == "tunnl")    {return "TUNL";}

if      (strInput == "TRNPK")     {return "TPKE";}
else if (strInput == "Trnpk")     {return "TPKE";}
else if (strInput == "trnpk")     {return "TPKE";}
else if (strInput == "TURNPIKE")  {return "TPKE";}
else if (strInput == "Turnpike")  {return "TPKE";}
else if (strInput == "turnpike")  {return "TPKE";}
else if (strInput == "TURNPK")    {return "TPKE";}
else if (strInput == "Turnpk")    {return "TPKE";}
else if (strInput == "turnpk")    {return "TPKE";}
else if (strInput == "TPKE")      {return "TPKE";}
else if (strInput == "Tpke")      {return "TPKE";}
else if (strInput == "tpke")      {return "TPKE";}

if      (strInput == "UPAS")       {return "UPAS";}
else if (strInput == "Upas")       {return "UPAS";}
else if (strInput == "upas")       {return "UPAS";}
else if (strInput == "UNDERPASS")  {return "UPAS";}
else if (strInput == "Underpass")  {return "UPAS";}
else if (strInput == "underpass")  {return "UPAS";}

if      (strInput == "UN")     {return "UN";}
else if (strInput == "Un")     {return "UN";}
else if (strInput == "un")     {return "UN";}
else if (strInput == "UNION")  {return "UN";}
else if (strInput == "Union")  {return "UN";}
else if (strInput == "union")  {return "UN";}

if      (strInput == "UNS")     {return "UNS";}
else if (strInput == "Uns")     {return "UNS";}
else if (strInput == "uns")     {return "UNS";}
else if (strInput == "UNIONS")  {return "UNS";}
else if (strInput == "Unions")  {return "UNS";}
else if (strInput == "unions")  {return "UNS";}

if      (strInput == "VALLEY")  {return "VLY";}
else if (strInput == "Valley")  {return "VLY";}
else if (strInput == "valley")  {return "VLY";}
else if (strInput == "VALLY")   {return "VLY";}
else if (strInput == "Vally")   {return "VLY";}
else if (strInput == "vally")   {return "VLY";}
else if (strInput == "VLLY")    {return "VLY";}
else if (strInput == "Vlly")    {return "VLY";}
else if (strInput == "vlly")    {return "VLY";}
else if (strInput == "VLY")     {return "VLY";}
else if (strInput == "Vly")     {return "VLY";}
else if (strInput == "vly")     {return "VLY";}

if      (strInput == "VALLEYS")  {return "VLYS";}
else if (strInput == "Valleys")  {return "VLYS";}
else if (strInput == "valleys")  {return "VLYS";}
else if (strInput == "VLYS")     {return "VLYS";}
else if (strInput == "Vlys")     {return "VLYS";}
else if (strInput == "vlys")     {return "VLYS";}

if      (strInput == "VDCT")      {return "VIA";}
else if (strInput == "Vdct")      {return "VIA";}
else if (strInput == "vdct")      {return "VIA";}
else if (strInput == "VIA")       {return "VIA";}
else if (strInput == "Via")       {return "VIA";}
else if (strInput == "via")       {return "VIA";}
else if (strInput == "VIADCT")    {return "VIA";}
else if (strInput == "Viadct")    {return "VIA";}
else if (strInput == "viadct")    {return "VIA";}
else if (strInput == "VIADUCT")   {return "VIA";}
else if (strInput == "Viaduct")   {return "VIA";}
else if (strInput == "viaduct")   {return "VIA";}

if      (strInput == "VIEW")   {return "VW";}
else if (strInput == "View")   {return "VW";}
else if (strInput == "view")   {return "VW";}
else if (strInput == "VW")     {return "VW";}
else if (strInput == "Vw")     {return "VW";}
else if (strInput == "vw")     {return "VW";}

if      (strInput == "VIEWS")   {return "VWS";}
else if (strInput == "Views")   {return "VWS";}
else if (strInput == "views")   {return "VWS";}
else if (strInput == "VWS")     {return "VWS";}
else if (strInput == "Vws")     {return "VWS";}
else if (strInput == "vws")     {return "VWS";}

if      (strInput == "VILL")      {return "VLG";}
else if (strInput == "Vill")      {return "VLG";}
else if (strInput == "vill")      {return "VLG";}
else if (strInput == "VLG")       {return "VLG";}
else if (strInput == "Vlg")       {return "VLG";}
else if (strInput == "vlg")       {return "VLG";}
else if (strInput == "VILLAG")    {return "VLG";}
else if (strInput == "Villag")    {return "VLG";}
else if (strInput == "villag")    {return "VLG";}
else if (strInput == "VILLAGE")   {return "VLG";}
else if (strInput == "Village")   {return "VLG";}
else if (strInput == "village")   {return "VLG";}
else if (strInput == "VILLG")     {return "VLG";}
else if (strInput == "Villg")     {return "VLG";}
else if (strInput == "villg")     {return "VLG";}
else if (strInput == "VILLIAGE")  {return "VLG";}
else if (strInput == "Villiage")  {return "VLG";}
else if (strInput == "villiage")  {return "VLG";}

if      (strInput == "VILLAGES")   {return "VLGS";}
else if (strInput == "Villages")   {return "VLGS";}
else if (strInput == "villages")   {return "VLGS";}
else if (strInput == "VLGS")       {return "VLGS";}
else if (strInput == "Vlgs")       {return "VLGS";}
else if (strInput == "vlgs")       {return "VLGS";}

if      (strInput == "VILLE")   {return "VL";}
else if (strInput == "Ville")   {return "VL";}
else if (strInput == "ville")   {return "VL";}
else if (strInput == "VL")      {return "VL";}
else if (strInput == "Vl")      {return "VL";}
else if (strInput == "vl")      {return "VL";}

if      (strInput == "VIS")      {return "VIS";}
else if (strInput == "Vis")      {return "VIS";}
else if (strInput == "vis")      {return "VIS";}
else if (strInput == "VIST")     {return "VIS";}
else if (strInput == "Vist")     {return "VIS";}
else if (strInput == "vist")     {return "VIS";}
else if (strInput == "VISTA")    {return "VIS";}
else if (strInput == "Vista")    {return "VIS";}
else if (strInput == "vista")    {return "VIS";}
else if (strInput == "VST")      {return "VIS";}
else if (strInput == "Vst")      {return "VIS";}
else if (strInput == "vst")      {return "VIS";}
else if (strInput == "VSTA")     {return "VIS";}
else if (strInput == "Vsta")     {return "VIS";}
else if (strInput == "vsta")     {return "VIS";}

if      (strInput == "WALK")      {return "WALK";}
else if (strInput == "Walk")      {return "WALK";}
else if (strInput == "walk")      {return "WALK";}
else if (strInput == "WALKS")     {return "WALK";}
else if (strInput == "Walks")     {return "WALK";}
else if (strInput == "walks")     {return "WALK";}

if      (strInput == "WALL")      {return "WALL";}
else if (strInput == "Wall")      {return "WALL";}
else if (strInput == "wall")      {return "WALL";}

if      (strInput == "WAY")    {return "WAY";}
else if (strInput == "Way")    {return "WAY";}
else if (strInput == "way")    {return "WAY";}
else if (strInput == "WY")     {return "WAY";}
else if (strInput == "Wy")     {return "WAY";}
else if (strInput == "wy")     {return "WAY";}

if      (strInput == "WAYS")    {return "WAYS";}
else if (strInput == "Ways")    {return "WAYS";}
else if (strInput == "ways")    {return "WAYS";}

if      (strInput == "WELL")   {return "WL";}
else if (strInput == "Well")   {return "WL";}
else if (strInput == "well")   {return "WL";}
else if (strInput == "WL")     {return "WL";}
else if (strInput == "Wl")     {return "WL";}
else if (strInput == "wl")     {return "WL";}

if      (strInput == "WELLS")   {return "WLS";}
else if (strInput == "Wells")   {return "WLS";}
else if (strInput == "wells")   {return "WLS";}
else if (strInput == "WLS")     {return "WLS";}
else if (strInput == "Wls")     {return "WLS";}
else if (strInput == "wls")     {return "WLS";}

// should not get here !!!
SendCodingError("globalfunctions.cpp fn PostalStreetSuffix() unable to process string -> " + strInput);
return strInput;
}

string StateAbbreviation(string strInput) {

strInput = RemoveLeadingSpaces(strInput);
strInput = RemoveTrailingSpaces(strInput);


if      (strInput == "ALABAMA")     {return "AL";}
else if (strInput == "Alabama")     {return "AL";}
else if (strInput == "alabama")     {return "AL";}
else if (strInput == "AL")          {return "AL";}
else if (strInput == "Al")          {return "AL";}
else if (strInput == "al")          {return "AL";}
else if (strInput == "ALA")         {return "AL";}
else if (strInput == "Ala")         {return "AL";}
else if (strInput == "ala")         {return "AL";}

if      (strInput == "ALASKA")      {return "AK";}
else if (strInput == "Alaska")      {return "AK";}
else if (strInput == "alaska")      {return "AK";}
else if (strInput == "AK")          {return "AK";}
else if (strInput == "Ak")          {return "AK";}
else if (strInput == "ak")          {return "AK";}

if      (strInput == "ARIZONA")     {return "AZ";}
else if (strInput == "Arizona")     {return "AZ";}
else if (strInput == "arizona")     {return "AZ";}
else if (strInput == "AZ")          {return "AZ";}
else if (strInput == "Az")          {return "AZ";}
else if (strInput == "az")          {return "AZ";}
else if (strInput == "ARIZ")        {return "AZ";}
else if (strInput == "Ariz")        {return "AZ";}
else if (strInput == "ariz")        {return "AZ";}

if      (strInput == "ARKANSAS")     {return "AR";}
else if (strInput == "Arkansas")     {return "AR";}
else if (strInput == "arkansas")     {return "AR";}
else if (strInput == "AR")           {return "AR";}
else if (strInput == "Ar")           {return "AR";}
else if (strInput == "ar")           {return "AR";}
else if (strInput == "ARK")          {return "AR";}
else if (strInput == "Ark")          {return "AR";}
else if (strInput == "ark")          {return "AR";}


if      (strInput == "CALIFORNIA")   {return "CA";}
else if (strInput == "California")   {return "CA";}
else if (strInput == "california")   {return "CA";}
else if (strInput == "CA")           {return "CA";}
else if (strInput == "Ca")           {return "CA";}
else if (strInput == "ca")           {return "CA";}

if      (strInput == "COLORADO")   {return "CO";}
else if (strInput == "Colorado")   {return "CO";}
else if (strInput == "colorado")   {return "CO";}
else if (strInput == "CO")         {return "CO";}
else if (strInput == "Co")         {return "CO";}
else if (strInput == "co")         {return "CO";}
else if (strInput == "COLOR")      {return "CO";}
else if (strInput == "Color")      {return "CO";}
else if (strInput == "color")      {return "CO";}

if      (strInput == "CONNECTICUT")   {return "CT";}
else if (strInput == "Connecticut")   {return "CT";}
else if (strInput == "connecticut")   {return "CT";}
else if (strInput == "CT")            {return "CT";}
else if (strInput == "Ct")            {return "CT";}
else if (strInput == "ct")            {return "CT";}
else if (strInput == "CONN")          {return "CT";}
else if (strInput == "Conn")          {return "CT";}
else if (strInput == "conn")          {return "CT";}


if      (strInput == "DELAWARE")   {return "DE";}
else if (strInput == "Delaware")   {return "DE";}
else if (strInput == "delaware")   {return "DE";}
else if (strInput == "DE")         {return "DE";}
else if (strInput == "De")         {return "DE";}
else if (strInput == "de")         {return "DE";}
else if (strInput == "DEL")        {return "DE";}
else if (strInput == "Del")        {return "DE";}
else if (strInput == "del")        {return "DE";}


if      (strInput == "FLORIDA")   {return "FL";}
else if (strInput == "Florida")   {return "FL";}
else if (strInput == "florida")   {return "FL";}
else if (strInput == "FL")        {return "FL";}
else if (strInput == "Fl")        {return "FL";}
else if (strInput == "fl")        {return "FL";}
else if (strInput == "FLA")       {return "FL";}
else if (strInput == "Fla")       {return "FL";}
else if (strInput == "fla")       {return "FL";}

if      (strInput == "GEORGIA")   {return "GA";}
else if (strInput == "Georgia")   {return "GA";}
else if (strInput == "georgia")   {return "GA";}
else if (strInput == "GA")        {return "GA";}
else if (strInput == "Ga")        {return "GA";}
else if (strInput == "ga")        {return "GA";}

if      (strInput == "HAWAII")   {return "HI";}
else if (strInput == "Hawaii")   {return "HI";}
else if (strInput == "hawaii")   {return "HI";}
else if (strInput == "HI")       {return "HI";}
else if (strInput == "Hi")       {return "HI";}
else if (strInput == "hi")       {return "HI";}

if      (strInput == "IDAHO")   {return "ID";}
else if (strInput == "Idaho")   {return "ID";}
else if (strInput == "idaho")   {return "ID";}
else if (strInput == "ID")      {return "ID";}
else if (strInput == "Id")      {return "ID";}
else if (strInput == "id")      {return "ID";}

if      (strInput == "ILLINOIS")   {return "IL";}
else if (strInput == "Illinois")   {return "IL";}
else if (strInput == "illinois")   {return "IL";}
else if (strInput == "IL")         {return "IL";}
else if (strInput == "Il")         {return "IL";}
else if (strInput == "il")         {return "IL";}
else if (strInput == "ILL")        {return "IL";}
else if (strInput == "Ill")        {return "IL";}
else if (strInput == "ill")        {return "IL";}


if      (strInput == "INDIANA")   {return "IN";}
else if (strInput == "Indiana")   {return "IN";}
else if (strInput == "indiana")   {return "IN";}
else if (strInput == "IN")        {return "IN";}
else if (strInput == "In")        {return "IN";}
else if (strInput == "in")        {return "IN";}
else if (strInput == "IND")       {return "IN";}
else if (strInput == "Ind")       {return "IN";}
else if (strInput == "ind")       {return "IN";}

if      (strInput == "IOWA")   {return "IA";}
else if (strInput == "Iowa")   {return "IA";}
else if (strInput == "iowa")   {return "IA";}
else if (strInput == "IA")     {return "IA";}
else if (strInput == "Ia")     {return "IA";}
else if (strInput == "ia")     {return "IA";}

if      (strInput == "KANSAS")   {return "KS";}
else if (strInput == "Kansas")   {return "KS";}
else if (strInput == "kansas")   {return "KS";}
else if (strInput == "KS")        {return "KS";}
else if (strInput == "Ks")        {return "KS";}
else if (strInput == "ks")        {return "KS";}
else if (strInput == "KAN")       {return "KS";}
else if (strInput == "Kan")       {return "KS";}
else if (strInput == "kan")       {return "KS";}

if      (strInput == "KENTUCKY")   {return "KY";}
else if (strInput == "Kentucky")   {return "KY";}
else if (strInput == "kentucky")   {return "KY";}
else if (strInput == "KY")         {return "KY";}
else if (strInput == "Ky")         {return "KY";}
else if (strInput == "ky")         {return "KY";}

if      (strInput == "LOUISIANA")   {return "LA";}
else if (strInput == "Louisiana")   {return "LA";}
else if (strInput == "louisiana")   {return "LA";}
else if (strInput == "LA")          {return "LA";}
else if (strInput == "La")          {return "LA";}
else if (strInput == "la")          {return "LA";}

if      (strInput == "MAINE")   {return "ME";}
else if (strInput == "Maine")   {return "ME";}
else if (strInput == "maine")   {return "ME";}
else if (strInput == "ME")      {return "ME";}
else if (strInput == "Me")      {return "ME";}
else if (strInput == "me")      {return "ME";}

if      (strInput == "MARYLAND")   {return "MD";}
else if (strInput == "Maryland")   {return "MD";}
else if (strInput == "maryland")   {return "MD";}
else if (strInput == "MD")         {return "MD";}
else if (strInput == "Md")         {return "MD";}
else if (strInput == "md")         {return "MD";}

if      (strInput == "MASSACHUSETTS")   {return "MA";}
else if (strInput == "Massachusetts")   {return "MA";}
else if (strInput == "massachusetts")   {return "MA";}
else if (strInput == "MA")              {return "MA";}
else if (strInput == "Ma")              {return "MA";}
else if (strInput == "ma")              {return "MA";}
else if (strInput == "MASS")            {return "MA";}
else if (strInput == "Mass")            {return "MA";}
else if (strInput == "mass")            {return "MA";}

if      (strInput == "MICHIGAN")   {return "MI";}
else if (strInput == "Michigan")   {return "MI";}
else if (strInput == "michigan")   {return "MI";}
else if (strInput == "MI")         {return "MI";}
else if (strInput == "Mi")         {return "MI";}
else if (strInput == "mi")         {return "MI";}
else if (strInput == "MICH")       {return "MI";}
else if (strInput == "Mich")       {return "MI";}
else if (strInput == "mich")       {return "MI";}

if      (strInput == "MINNESOTA")   {return "MN";}
else if (strInput == "Minnesota")   {return "MN";}
else if (strInput == "minnesota")   {return "MN";}
else if (strInput == "MN")          {return "MN";}
else if (strInput == "Mn")          {return "MN";}
else if (strInput == "mn")          {return "MN";}
else if (strInput == "MINN")        {return "MN";}
else if (strInput == "Minn")        {return "MN";}
else if (strInput == "minn")        {return "MN";}

if      (strInput == "MISSISSIPPI")   {return "MS";}
else if (strInput == "Mississippi")   {return "MS";}
else if (strInput == "mississippi")   {return "MS";}
else if (strInput == "MS")            {return "MS";}
else if (strInput == "Ms")            {return "MS";}
else if (strInput == "ms")            {return "MS";}
else if (strInput == "MISS")          {return "MS";}
else if (strInput == "Miss")          {return "MS";}
else if (strInput == "miss")          {return "MS";}

if      (strInput == "MISSOURI")   {return "MO";}
else if (strInput == "Missouri")   {return "MO";}
else if (strInput == "missouri")   {return "MO";}
else if (strInput == "MO")         {return "MO";}
else if (strInput == "Mo")         {return "MO";}
else if (strInput == "mo")         {return "MO";}

if      (strInput == "MONTANA")   {return "MT";}
else if (strInput == "Montana")   {return "MT";}
else if (strInput == "montana")   {return "MT";}
else if (strInput == "MT")        {return "MT";}
else if (strInput == "Mt")        {return "MT";}
else if (strInput == "mt")        {return "MT";}
else if (strInput == "MONT")      {return "MT";}
else if (strInput == "Mont")      {return "MT";}
else if (strInput == "mont")      {return "MT";}

if      (strInput == "NEBRASKA")   {return "NE";}
else if (strInput == "Nebraska")   {return "NE";}
else if (strInput == "nebraska")   {return "NE";}
else if (strInput == "NE")         {return "NE";}
else if (strInput == "Ne")         {return "NE";}
else if (strInput == "ne")         {return "NE";}
else if (strInput == "NEB")        {return "NE";}
else if (strInput == "Neb")        {return "NE";}
else if (strInput == "neb")        {return "NE";}

if      (strInput == "NEVADA")   {return "NV";}
else if (strInput == "Nevada")   {return "NV";}
else if (strInput == "nevada")   {return "NV";}
else if (strInput == "NV")       {return "NV";}
else if (strInput == "Nv")       {return "NV";}
else if (strInput == "nv")       {return "NV";}
else if (strInput == "NEV")      {return "NV";}
else if (strInput == "Nev")      {return "NV";}
else if (strInput == "nev")      {return "NV";}

if      (strInput == "NEW HAMPSHIRE")   {return "NH";}
else if (strInput == "New Hampshire")   {return "NH";}
else if (strInput == "new hampshire")   {return "NH";}
else if (strInput == "NEWHAMPSHIRE")    {return "NH";}
else if (strInput == "NewHampshire")    {return "NH";}
else if (strInput == "newhampshire")    {return "NH";}
else if (strInput == "NH")              {return "NH";}
else if (strInput == "Nh")              {return "NH";}
else if (strInput == "nh")              {return "NH";}

if      (strInput == "NEW JERSEY")   {return "NJ";}
else if (strInput == "New Jersey")   {return "NJ";}
else if (strInput == "new jersey")   {return "NJ";}
else if (strInput == "NEWJERSEY")    {return "NJ";}
else if (strInput == "NewJersey")    {return "NJ";}
else if (strInput == "newjersey")    {return "NJ";}
else if (strInput == "NJ")           {return "NJ";}
else if (strInput == "Nj")           {return "NJ";}
else if (strInput == "nj")           {return "NJ";}

if      (strInput == "NEW MEXICO")   {return "NM";}
else if (strInput == "New Mexico")   {return "NM";}
else if (strInput == "new mexico")   {return "NM";}
else if (strInput == "NEWMEXICO")    {return "NM";}
else if (strInput == "NewMexico")    {return "NM";}
else if (strInput == "newmexico")    {return "NM";}
else if (strInput == "NM")           {return "NM";}
else if (strInput == "Nm")           {return "NM";}
else if (strInput == "nm")           {return "NM";}

if      (strInput == "NEW YORK")   {return "NY";}
else if (strInput == "New York")   {return "NY";}
else if (strInput == "new york")   {return "NY";}
else if (strInput == "NEWYORK")    {return "NY";}
else if (strInput == "NewYork")    {return "NY";}
else if (strInput == "newyork")    {return "NY";}
else if (strInput == "NY")         {return "NY";}
else if (strInput == "Ny")         {return "NY";}
else if (strInput == "ny")         {return "NY";}

if      (strInput == "NORTH CAROLINA")   {return "NC";}
else if (strInput == "North Carolina")   {return "NC";}
else if (strInput == "north carolina")   {return "NC";}
else if (strInput == "NORTHCAROLINA")    {return "NC";}
else if (strInput == "NorthCarolina")    {return "NC";}
else if (strInput == "northcarolina")    {return "NC";}
else if (strInput == "NC")               {return "NC";}
else if (strInput == "Nc")               {return "NC";}
else if (strInput == "nc")               {return "NC";}

if      (strInput == "NORTH DAKOTA")   {return "ND";}
else if (strInput == "North Dakota")   {return "ND";}
else if (strInput == "north dakota")   {return "ND";}
else if (strInput == "NORTHDAKOTA")    {return "ND";}
else if (strInput == "NorthDakota")    {return "ND";}
else if (strInput == "northdakota")    {return "ND";}
else if (strInput == "ND")             {return "ND";}
else if (strInput == "Nd")             {return "ND";}
else if (strInput == "nd")             {return "ND";}

if      (strInput == "OHIO")   {return "OH";}
else if (strInput == "Ohio")   {return "OH";}
else if (strInput == "ohio")   {return "OH";}
else if (strInput == "OH")     {return "OH";}
else if (strInput == "Oh")     {return "OH";}
else if (strInput == "oh")     {return "OH";}

if      (strInput == "OKLAHOMA")   {return "OK";}
else if (strInput == "Oklahoma")   {return "OK";}
else if (strInput == "oklahoma")   {return "OK";}
else if (strInput == "OK")         {return "OK";}
else if (strInput == "Ok")         {return "OK";}
else if (strInput == "ok")         {return "OK";}
else if (strInput == "OKLA")       {return "OK";}
else if (strInput == "Okla")       {return "OK";}
else if (strInput == "okla")       {return "OK";}
 
if      (strInput == "OREGON")   {return "OR";}
else if (strInput == "Oregon")   {return "OR";}
else if (strInput == "oregon")   {return "OR";}
else if (strInput == "OR")       {return "OR";}
else if (strInput == "Or")       {return "OR";}
else if (strInput == "or")       {return "OR";}
else if (strInput == "ORE")      {return "OR";}
else if (strInput == "Ore")      {return "OR";}
else if (strInput == "ore")      {return "OR";}

if      (strInput == "PENNSYLVANIA")   {return "PA";}
else if (strInput == "Pennsylvania")   {return "PA";}
else if (strInput == "pennsylvania")   {return "PA";}
else if (strInput == "PA")             {return "PA";}
else if (strInput == "Pa")             {return "PA";}
else if (strInput == "pa")             {return "PA";}

if      (strInput == "RHODE ISLAND")   {return "RI";}
else if (strInput == "Rhode Island")   {return "RI";}
else if (strInput == "rhode island")   {return "RI";}
else if (strInput == "RHODEISLAND")    {return "RI";}
else if (strInput == "RhodeIsland")    {return "RI";}
else if (strInput == "rhodeisland")    {return "RI";}
else if (strInput == "RI")             {return "RI";}
else if (strInput == "Ri")             {return "RI";}
else if (strInput == "ri")             {return "RI";}

if      (strInput == "SOUTH CAROLINA")   {return "SC";}
else if (strInput == "South Carolina")   {return "SC";}
else if (strInput == "south carolina")   {return "SC";}
else if (strInput == "SOUTHCAROLINA")    {return "SC";}
else if (strInput == "SouthCarolina")    {return "SC";}
else if (strInput == "southcarolina")    {return "SC";}
else if (strInput == "SC")               {return "SC";}
else if (strInput == "Sc")               {return "SC";}
else if (strInput == "sc")               {return "SC";}

if      (strInput == "SOUTH DAKOTA")   {return "SD";}
else if (strInput == "South Dakota")   {return "SD";}
else if (strInput == "south dakota")   {return "SD";}
else if (strInput == "SOUTHDAKOTA")    {return "SD";}
else if (strInput == "SouthDakota")    {return "SD";}
else if (strInput == "southdakota")    {return "SD";}
else if (strInput == "SD")             {return "SD";}
else if (strInput == "Sd")             {return "SD";}
else if (strInput == "sd")             {return "SD";}

if      (strInput == "TENNESSEE")   {return "TN";}
else if (strInput == "Tennessee")   {return "TN";}
else if (strInput == "tennessee")   {return "TN";}
else if (strInput == "TN")          {return "TN";}
else if (strInput == "Tn")          {return "TN";}
else if (strInput == "tn")          {return "TN";}
else if (strInput == "TENN")        {return "TN";}
else if (strInput == "Tenn")        {return "TN";}
else if (strInput == "tenn")        {return "TN";}
 
if      (strInput == "TEXAS")   {return "TX";}
else if (strInput == "Texas")   {return "TX";}
else if (strInput == "texas")   {return "TX";}
else if (strInput == "TX")      {return "TX";}
else if (strInput == "Tx")      {return "TX";}
else if (strInput == "tx")      {return "TX";}
else if (strInput == "TEX")     {return "TX";}
else if (strInput == "Tex")     {return "TX";}
else if (strInput == "tex")     {return "TX";}
 
if      (strInput == "UTAH")   {return "UT";}
else if (strInput == "Utah")   {return "UT";}
else if (strInput == "utah")   {return "UT";}
else if (strInput == "UT")     {return "UT";}
else if (strInput == "Ut")     {return "UT";}
else if (strInput == "ut")     {return "UT";}

if      (strInput == "VERMONT")   {return "VT";}
else if (strInput == "Vermont")   {return "VT";}
else if (strInput == "vermont")   {return "VT";}
else if (strInput == "VT")        {return "VT";}
else if (strInput == "Vt")        {return "VT";}
else if (strInput == "vt")        {return "VT";}

if      (strInput == "VIRGINIA")   {return "VA";}
else if (strInput == "Virginia")   {return "VA";}
else if (strInput == "virginia")   {return "VA";}
else if (strInput == "VA")         {return "VA";}
else if (strInput == "Va")         {return "VA";}
else if (strInput == "va")         {return "VA";}

if      (strInput == "WASHINGTON")   {return "WA";}
else if (strInput == "Washington")   {return "WA";}
else if (strInput == "washington")   {return "WA";}
else if (strInput == "WA")           {return "WA";}
else if (strInput == "Wa")           {return "WA";}
else if (strInput == "wa")           {return "WA";}
else if (strInput == "WASH")         {return "WA";}
else if (strInput == "Wash")         {return "WA";}
else if (strInput == "wash")         {return "WA";}

if      (strInput == "WEST VIRGINIA")   {return "WV";}
else if (strInput == "West Virginia")   {return "WV";}
else if (strInput == "west viginia")    {return "WV";}
else if (strInput == "WESTVIRGINIA")    {return "WV";}
else if (strInput == "WestVirginia")    {return "WV";}
else if (strInput == "westvirginia")    {return "WV";}
else if (strInput == "WV")              {return "WV";}
else if (strInput == "Wv")              {return "WV";}
else if (strInput == "wv")              {return "WV";}
else if (strInput == "WVA")             {return "WV";}
else if (strInput == "Wva")             {return "WV";}
else if (strInput == "wva")             {return "WV";}

if      (strInput == "WISCONSIN")   {return "WI";}
else if (strInput == "Wisconsin")   {return "WI";}
else if (strInput == "wisconsin")   {return "WI";}
else if (strInput == "WI")          {return "WI";}
else if (strInput == "Wi")          {return "WI";}
else if (strInput == "wi")          {return "WI";}
else if (strInput == "WIS")         {return "WI";}
else if (strInput == "Wis")         {return "WI";}
else if (strInput == "wis")         {return "WI";}

if      (strInput == "WYOMING")   {return "WY";}
else if (strInput == "Wyoming")   {return "WY";}
else if (strInput == "wyoming")   {return "WY";}
else if (strInput == "WY")        {return "WY";}
else if (strInput == "Wy")        {return "WY";}
else if (strInput == "wy")        {return "WY";}
else if (strInput == "WYO")       {return "WY";}
else if (strInput == "Wyo")       {return "WY";}
else if (strInput == "wyo")       {return "WY";}

if      (strInput == "DISTRICT OF COLUMBIA")   {return "DC";}
else if (strInput == "District of Columbia")   {return "DC";}
else if (strInput == "District Of Columbia")   {return "DC";}
else if (strInput == "district of columbia")   {return "DC";}
else if (strInput == "DISTRICTOFCOLUMBIA")     {return "DC";}
else if (strInput == "DistrictOfColumbia")     {return "DC";}
else if (strInput == "DistrictofColumbia")     {return "DC";}
else if (strInput == "districtofcolumbia")     {return "DC";}
else if (strInput == "DC")                     {return "DC";}
else if (strInput == "Dc")                     {return "DC";}
else if (strInput == "dc")                     {return "DC";}

if      (strInput == "GUAM")   {return "GU";}
else if (strInput == "Guam")   {return "GU";}
else if (strInput == "guam")   {return "GU";}
else if (strInput == "GU")     {return "GU";}
else if (strInput == "Gu")     {return "GU";}
else if (strInput == "gu")     {return "GU";}

if      (strInput == "MARSHALL ISLANDS")   {return "MH";}
else if (strInput == "Marshall Islands")   {return "MH";}
else if (strInput == "marshall islands")   {return "MH";}
else if (strInput == "MARSHALLISLANDS")    {return "MH";}
else if (strInput == "MarshallIslands")    {return "MH";}
else if (strInput == "marshallislands")    {return "MH";}
else if (strInput == "MH")                 {return "MH";}
else if (strInput == "Mh")                 {return "MH";}
else if (strInput == "mh")                 {return "MH";}

if      (strInput == "NORTHERN MARIANA ISLAND")  {return "MP";}
else if (strInput == "Northern Mariana Island")  {return "MP";}
else if (strInput == "northern mariana island")  {return "MP";}
else if (strInput == "NORTHERNMARIANAISLAND")    {return "MP";}
else if (strInput == "NorthernMarianaIsland")    {return "MP";}
else if (strInput == "northernmarianaisland")    {return "MP";}
else if (strInput == "MP")                       {return "MP";}
else if (strInput == "Mp")                       {return "MP";}
else if (strInput == "mp")                       {return "MP";}
else if (strInput == "CNMI")                     {return "MP";}
else if (strInput == "Cnmi")                     {return "MP";}
else if (strInput == "cnmi")                     {return "MP";}

if      (strInput == "PUERTO RICO")  {return "PR";}
else if (strInput == "Puerto Rico")  {return "PR";}
else if (strInput == "puerto rico")  {return "PR";}
else if (strInput == "PUERTORICO")   {return "PR";}
else if (strInput == "PuertoRico")   {return "PR";}
else if (strInput == "puertorico")   {return "PR";}
else if (strInput == "PR")           {return "PR";}
else if (strInput == "Pr")           {return "PR";}
else if (strInput == "pr")           {return "PR";}
else if (strInput == "PUR")          {return "PR";}
else if (strInput == "Pur")          {return "PR";}
else if (strInput == "pur")          {return "PR";}

if      (strInput == "VIRGIN ISLANDS")   {return "VI";}
else if (strInput == "Virgin Islands")   {return "VI";}
else if (strInput == "virgin islands")   {return "VI";}
else if (strInput == "VIRGINISLANDS")    {return "VI";}
else if (strInput == "VirginIslands")    {return "VI";}
else if (strInput == "virginislands")    {return "VI";}
else if (strInput == "VI")               {return "VI";}
else if (strInput == "Vi")               {return "VI";}
else if (strInput == "vi")               {return "VI";}

SendCodingError("globalfunctions.cpp fn StateAbbreviation() unable to process string -> " + strInput);
return strInput;
}



string CustNameGUIformat(string strInput, bool boolWithBrackets)
{
 string strFormattedCustName;
 size_t found;

 if     ((strInput.empty())&&(boolWithBrackets)) {return " [No Data]";}
 else if (strInput.empty())                      {return "No Data";} 

 strFormattedCustName = RemoveLeadingSpaces(strInput);
 strFormattedCustName = RemoveTrailingSpaces(strFormattedCustName);
 if (strFormattedCustName.find_first_not_of(" \t") == string::npos) {strFormattedCustName.clear();}

 if     ((strFormattedCustName.empty())&&(boolWithBrackets)) {return " [No Data]";}
 else if (strFormattedCustName.empty())                      {return "No Data";} 

  //check for 101-199
 if (strFormattedCustName.length() == 3)
  {
   found = strFormattedCustName.find_first_not_of("0123456789");
   if ((strFormattedCustName[0] == '1')&&(found == string::npos))
    {
     if (boolWithBrackets) {return " [Unknown]";}
     else                  {return "Unknown";}
    }

  }

 if ((!strFormattedCustName.empty())&&(boolWithBrackets)) {return  " [" + strFormattedCustName + "]";}
  
 return strFormattedCustName;
}


unsigned long long int Find_10_Digit_Telephone_Number(string strInput, size_t Column)
{
 // patterns to search for
 // (303) 000-0000  strTelnoParensSpDash
 // (303) 000 0000  strTelnoParensSpSp
 // (303)000-0000   strTelnoParensNoSpDash
 // (303)000 0000   strTelnoParensNoSpSp
 //  303-000-0000   strTelnoDashes
 //  303 000-0000   strTelnoSpDash
 //  303 000 0000   strTelnoSpSp
 //  3030000000     strTenDigits 

 size_t position = Column;
 string strTenDigits;
 string strAreaCode;
 string strNXX;
 string strLastFour;
 string strTelnoParensSpDash;
 string strTelnoParensSpSp;
 string strTelnoParensNoSpDash;
 string strTelnoParensNoSpSp;
 string strTelnoDashes; 
 string strTelnoSpDash;
 string strTelnoSpSp;


 while (position < strInput.length())
  {
   position = strInput.find_first_of("0123456789", position);
   if (position == string::npos)   {return 0;/*error*/}
   else                            {strTenDigits += strInput[position];} 
   if (strTenDigits.length() == 10){break;}

   position++;
  }// end while (position != string::npos)


  if (strTenDigits.length() != 10){return 0;/*error*/}
  
  strAreaCode = strTenDigits.substr(0,3);
  strNXX      = strTenDigits.substr(3,3);
  strLastFour = strTenDigits.substr(6,4);
 
  strTelnoParensSpDash     = "("+strAreaCode +") " + strNXX + "-" + strLastFour;
  strTelnoParensSpSp       = "("+strAreaCode +") " + strNXX + " " + strLastFour;
  strTelnoParensNoSpDash   = "("+strAreaCode +")"  + strNXX + "-" + strLastFour;
  strTelnoParensNoSpSp     = "("+strAreaCode +")"  + strNXX + " " + strLastFour;
  strTelnoDashes           =     strAreaCode + "-" + strNXX + "-" + strLastFour;   
  strTelnoSpDash           =     strAreaCode + " " + strNXX + "-" + strLastFour; 
  strTelnoSpSp             =     strAreaCode + " " + strNXX + " " + strLastFour;


 position = strInput.find(strTelnoParensSpDash);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTelnoParensSpSp);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTelnoParensNoSpDash);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTelnoParensNoSpSp);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTelnoDashes);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTelnoSpDash);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTelnoSpSp);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 position = strInput.find(strTenDigits);
 if (position!=string::npos) {return char2int(strTenDigits.c_str());}
 

 return 0;
}

bool CheckforManualBidTrunk(int intTrunk)
{
 if ((intTrunk >= intALI_MAN_BID_TRUNK_FIRST)&&(intTrunk <= intALI_MAN_BID_TRUNK_LAST)) {return true;}
 else                                                                                   {return false;}
}



bool spc_email_isvalid(const char *address) {
  int        count = 0;
  const char *c, *domain;
  static char *rfc822_specials = (char*) "()<>@,;:\\\"[]";

  /* first we validate the name portion (name@domain) */
  for (c = address;  *c;  c++) {
    if (*c == '\"' && (c == address || *(c - 1) == '.' || *(c - 1) == 
        '\"')) {
      while (*++c) {
        if (*c == '\"') break;
        if (*c == '\\' && (*++c == ' ')) continue;
        if (*c <= ' ' || *c >= 127) return false;
      }
      if (!*c++) return false;
      if (*c == '@') break;
      if (*c != '.') return false;
      continue;
    }
    if (*c == '@') break;
    if (*c <= ' ' || *c >= 127) return false;
    if (strchr(rfc822_specials, *c)) return false;
  }
  if (c == address || *(c - 1) == '.') return false;

  /* next we validate the domain portion (name@domain) */
  if (!*(domain = ++c)) return false;
  do {
    if (*c == '.') {
      if (c == domain || *(c - 1) == '.') return false;
      count++;
    }
    if (*c <= ' ' || *c >= 127) return false;
    if (strchr(rfc822_specials, *c)) return false;
  } while (*++c);

  return (count >= 1);
}


bool ValidTenDigitNumber(string strNumber)
{
 size_t found;

 found = strNumber.find_first_not_of("0123456789");
 if (found != string::npos)     {return false;}
 if (strNumber.length() != 10)  {return false;}
 return true;
}

string AddParensToTenDigitNumber(string strInput)
{
 string strOutput = strInput;

 if (!ValidTenDigitNumber(strInput)){return "(000) 000-0000";}
 strOutput.insert(0,"("); 
 strOutput.insert(4,") ");
 strOutput.insert(9,"-");
 return strOutput;
}


bool ValidExtension(string strNumber)
{
 size_t found;

 found = strNumber.find_first_not_of("0123456789");
 if (found != string::npos)     {return false;}
 if (strNumber.length() < 4)    {return false;}
 if (strNumber.length() > 6)    {return false;}
 return true;

}

unsigned long int NextTransactionID()
{
 extern unsigned long int intE2TransactionID;
 int                      intRC;

 intRC = sem_wait(&sem_tMutexE2TransactionID);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexE2TransactionID, "sem_wait@sem_tMutexE2TransactionID in NextTransactionID()", 1);}
 if (intE2TransactionID >= 0xFFFFFFFF) {intE2TransactionID = 0;}
 intE2TransactionID++;
 sem_post(&sem_tMutexE2TransactionID);
 return intE2TransactionID;
}

string UUID_Generate_String()
{
 uuid_t 	t;
 char 		ch[36];
 string 	returnString;

 returnString.clear();
 uuid_generate(t);
 uuid_unparse(t,ch);

 for( int i=0; i<36; i++){
 returnString += ch[i];
 }

 return returnString;
}

Thread_Data ThreadObjectWithTID( pid_t TID)
{
 size_t                         sz;
 Thread_Data 			nfReturnObject;
 extern vector<Thread_Data> 	ThreadData;

  nfReturnObject.strThreadName  = "NOT_FOUND";
  nfReturnObject.strThread_UUID = "NOT_FOUND";

  sz = ThreadData.size();

  for (unsigned int i = 0; i< sz; i++) {

  if ( ThreadData[i].ThreadID == TID) {return ThreadData[i];} 
  }

 return nfReturnObject;
}







void Controller_Status_Check()
{






}

string ConvertDialedNumber(string strInput)
{
 string strTemp = strInput;
 size_t found;
 bool   bAllNumbers = false;

 if (strInput.empty()) {return "";}

 found = strInput.find_first_not_of("0123456789");
 if (found == string::npos) {bAllNumbers = true;}

 if (bAllNumbers)
  {
   if (strInput[0] == '9') {strTemp.erase(0,1);}

  }

 return strTemp;
}

string ChannelPrefix(string strInput)
{
 size_t found;

 string strDelimiter = "-";

 if (strInput.empty()) {return "";}

 found = strInput.rfind(strDelimiter);
 if (found == string::npos)      {return "";}

 return strInput.substr(0,found);
}

string Return_Content(string strInput)
{
 size_t found;
 size_t end;
 string strContent = strInput;
 string strContent_Length;
 size_t ContentLength;

 found = strInput.find("Content-Length: ",0);
 if (found!= string::npos)  {
   end =strInput.find_first_not_of("01234567890", found+16);
   if (end == string::npos)                                          {return "";}
   strContent_Length = strInput.substr(found+16, (end -(found+16)));
   ContentLength =  char2int(strContent_Length.c_str());
 }
 else {return "";}

 found = strContent.find("\t",found);
 if (found == string::npos)                                          {return "";}

 strContent.erase(0, found+1);

 if (strContent.length() > ContentLength)  {
   strContent = strContent.erase(ContentLength, string::npos);
 }
 
 return strContent;

}

string MSRPCheckForPound(string strInput) {

 size_t found;
 string strReturn = strInput;

 found = strInput.find("#T",0);

 if (found!= string::npos)  {
  strReturn =  FindandReplaceALL(strInput, "\"", "");
 }

 return strReturn;
}

unsigned char EncodeTwoDigits(string strInput) {
 if (strInput.length() != 2) {return 0;}
 int   intFirstDigit, intSecondDigit, intComboDigits;
 intFirstDigit  = char2int(strInput.substr(0,1).c_str());
 intSecondDigit = (char2int(strInput.substr(1,1).c_str())*16);
 intComboDigits = intFirstDigit + intSecondDigit;
// cout <<  intComboDigits << endl;       
 return (unsigned char) intComboDigits;

}

string DecodeTwoDigits(const char* ch)
{
 
 // TBC ... may not need


return "";
}

string InsertString(string strData, string strSUB, size_t Start, size_t Length)
{
 size_t LEN = Length;
 string strOutput =""; 
 
 if (Start > strData.length()) {SendCodingError( "globalfunctions.cpp - Coding Error in InsertString() "); return strData;}

 if (strSUB.empty())                  {return strData;}

 if (Start + (LEN -1) > 32)
  {
   LEN = 32 - (Start +1);
  }     
 if ( LEN > strSUB.length())
  {
   LEN = strSUB.length();
  }
 if (strSUB.length() > LEN)
  {
   strSUB = strSUB.substr(0,LEN );
  }

 if (strSUB.empty())                  {return strData;}

 strData.replace ( Start, LEN, strSUB.c_str() );

 for (unsigned int i =0; i < strData.length(); i++)
  {
   strOutput += toupper(strData[i]);
  }

return strOutput;
}



Telephone_Data_type TelephoneDataType(string strInput)
{
 size_t found;

 // check for NON GUI first !!
 found = strInput.find(NON_GUI_PHONE_INI_KEY);
 if (found != string::npos) {return NON_GUI_PHONE;}

 found = strInput.find(GUI_PHONE_INI_KEY);
 if (found != string::npos) {return GUI_PHONE;}

 found = strInput.find(BOTH_PHONES_INI_KEY);
 if (found != string::npos) {return BOTH_PHONES;}

 found = strInput.find(YEALINK_INI_KEY);
 if (found != string::npos) {return YEALINK;}

 found = strInput.find(POLYCOM_670_INI_KEY);
 if (found != string::npos) {return POLYCOM_670;}

 found = strInput.find(POLYCOM_VVX_INI_KEY);
 if (found != string::npos) {return POLYCOM_VVX;}

 return NO_PHONE_DEFINED;
}

string TelephoneDataType(Telephone_Data_type eType) {
//NO_PHONE_DEFINED, GUI_PHONE, NON_GUI_PHONE, BOTH_PHONES, AUDIOCODES, IPGATEWAY, NG911SIPTRUNK, POLYCOM_670, POLYCOM_VVX, YEALINK}
 switch (eType) {
  case NO_PHONE_DEFINED: 	return "not_defined";  
  case NON_GUI_PHONE: 		return "generic_phone";
  case GUI_PHONE:     		return "softphone";
  case BOTH_PHONES:    		return "soft_plus_hard_phone";
  case AUDIOCODES:    		return "audiocodes";
  case IPGATEWAY:    		return "ip_gateway";
  case NG911SIPTRUNK:    	return "ng_siptrunk";
  case POLYCOM_670: 	   	return "670";
  case POLYCOM_VVX: 	   	return "VVX";
  case YEALINK: 	   	return "Yealink";
 }
 return "";
}

string FindIP_AddressAfterComma(string strInput)
{
 size_t found;
 IP_Address IPaddress;

 string strOutput;

 found = strInput.find(",");
 if (found == string::npos) {return "";}

 found = strInput.find_first_not_of(", ",found);
 if (found == string::npos) {return "";}

 IPaddress.stringAddress = strInput.substr(found, string::npos);

 IPaddress.stringAddress = RemoveTrailingSpaces(IPaddress.stringAddress);
 if (IPaddress.fIsValid_IP_Address()) {return IPaddress.stringAddress;}
 else                                 {return "";}
}



void set_mode(int want_key)
{
	static struct termios old, newt;
	if (!want_key) {
		tcsetattr(STDIN_FILENO, TCSANOW, &old);
		return;
	}
 
	tcgetattr(STDIN_FILENO, &old);
	newt = old;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
}
 
int get_key()
{
	int c = 0;
	struct timeval tv;
	fd_set fs;
	tv.tv_usec = tv.tv_sec = 0;
 
	FD_ZERO(&fs);
	FD_SET(STDIN_FILENO, &fs);
	select(STDIN_FILENO + 1, &fs, 0, 0, &tv);
 
	if (FD_ISSET(STDIN_FILENO, &fs)) {
		c = getchar();
		set_mode(0);
	}
	return c;
}





int NonBlockingNoEchoInputChar()
{
 int returnvalue;

 set_mode(1);
 while (!(returnvalue = get_key())) {usleep(300); if (boolSTOP_EXECUTION) {return -1;}}

 return returnvalue;
}
 






void CloseOutXMLlogfile(string strXMLlogName)
{
 size_t found;

 string strYear, strMonth;
 struct stat     st;
 string          strLine;
 ifstream         XMLread;
 MessageClass    objMessage;


 extern ofstream   XMLlogfile;

 if (strXMLlogName.empty()) {return;}

 
 //check if file exists return if it does not return
 if(stat(strXMLlogName.c_str(),&st) != 0) {return;}

 // check to ensure that end tag is not present
 XMLread.open(strXMLlogName.c_str(), ios::in);
 if (!XMLread.is_open())   {
  
   objMessage.fMessage_Create(LOG_WARNING, 544, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_544, strXMLlogName);
   enQueue_Message(LOG,objMessage);
   return;

 }  
                         
 while (! XMLread.eof() )   {
  
   getline(XMLread,strLine);
   found = strLine.find(XML_LOG_TAG_END);
   if (found != string::npos) {return;}

 }

 XMLread.close();
 
 // write end Tag at end of file
 XMLlogfile.open(strXMLlogName.c_str(), ios::app);
 if (!XMLlogfile.is_open())   {
  
   objMessage.fMessage_Create(LOG_WARNING, 544, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_LOG_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_544, strXMLlogName);
   enQueue_Message(LOG,objMessage);
   return;

 } 
        
 XMLlogfile << XML_LOG_TAG_END << endl;
 XMLlogfile.close();

}

void CloseOutXMLfiles()
{
 struct  dirent  *de=NULL;
 DIR             *d=NULL;
 string          strName, strPathplusName, strNewName;
 size_t          found;


 d=opendir(charLOG_FILE_PATH_NAME);

 if (d == NULL) {return;}

 //; 
 while ((de = readdir(d))!= NULL)
  {
   strName = de->d_name ;
   strPathplusName = charLOG_FILE_PATH_NAME;
   strPathplusName += "/";
   strPathplusName += strName;
   found = strPathplusName.find(charOPEN_XML_LOG_FILE_NAME_SUFFIX);

   if (found != string::npos)
    {

     CloseOutXMLlogfile(strPathplusName);
     strNewName = strPathplusName;
     strNewName.erase(found, string::npos);
     strNewName+= charXML_LOG_FILE_NAME_SUFFIX;
     rename(strPathplusName.c_str(), strNewName.c_str());
    }


  }

 closedir(d);

}



/****************************************************************************************************
*
* Name:
*  Function: wstring StringToWString(const std::string& s)
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function converts a std:string to a std:wstring
*    
* Parameters:
*   s                                 <cstring> the string to converted
*
* Variables:
*  temp                               Local   - <wstring>         Wide string
*                                                              
* Functions:
*    copy()                           Library  - <cstring>                 (size_t)
*   .length()                         Library  - <cstring>                 (size_t)
*   .begin()                          Library  - <cstring>                 (size_t)
*   .end()                            Library  - <cstring>                 (size_t)
*
* AUTHOR: 9/16/2011 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2011 Experient Corporation
****************************************************************************************************/
wstring StringToWString(const string& s)
{
    wstring temp(s.length(),L' ');
    copy(s.begin(), s.end(), temp.begin());
    return temp; 
}


string ConvertToXML(string strInput)
{
 string          strOutput;
 ToXMLStringTool convertmessage;

 strOutput = convertmessage.toXML(strInput.c_str());
 convertmessage.freeBuffer();
 
 return strOutput;
}
/****************************************************************************************************
*
* Name:
*  Function: ReplacelastCommaswithTabs(string strInput)
*
* Description:
*   This function is called from fn CheckAGIUserEventFormatBug()
*   it is designed to convert Asterisk 1.4 AGI UserEvent Error messages
*   into a format readable from the AsteriskAMI thread.
*
* Details:
*   This function takes a string formatted like "Varname: Data, Varnametwo: Data, Varnamethree: Data\t"
*   and converts it to the format : "Varname: Data\t Varnametwo: Data\t Varnamethree: Data\t"
*
*   if the Data were to contain comma's the function avoids corrupting it by replacing the comma closest to the last colon.
*   the Data MUST NOT CONTAIN COLON'S otherwise the output of the function will be undefined.
*   if an error is encountered the original string is returned. 
*    
* Parameters:
*   strInput                                 <cstring> the string to converted
*
* Variables:
*  boolExit                           Local   - bool                       loop exit conditional
*  EndPosition                        Local   - <size_t>                   position within string
*  StartPosition                      Local   - <size_t>                   position within string
*  index                              Local   - <size_t>                   position within string
*  strOutput                          Local   - <cstring>                  output string
*  npos                               Local   - <cstring::size_t>          cstring constant delineating eos
*                                                              
* Functions:
*   .replace()                        Library  - <cstring>                 (string&)
*   .rfind()                          Library  - <cstring>                 (size_t)
*
* AUTHOR: 12/28/2011 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2011 Experient Corporation
****************************************************************************************************/
string ReplacelastCommaswithTabs(string strInput)
{
 string strOutput = strInput;
 bool   boolExit = false;
 size_t index; 
 size_t StartPosition;
 size_t EndPosition;

 StartPosition = strInput.rfind(":");
 if (StartPosition == string::npos)                   {return strOutput;}

 do
  {
   if (StartPosition == 0)                               {return strOutput;}
   EndPosition = strOutput.rfind(":", StartPosition -1 );
   if (StartPosition == string::npos)                    {return strOutput;}
   index = strOutput.rfind(",", StartPosition);
   if (index  != string::npos ) 
    {
     strOutput.replace(index,1,"\t");
    }
   else {boolExit = true;}
  StartPosition =  EndPosition;
  } while (!boolExit);

 return strOutput;
}
/****************************************************************************************************
*
* Name:
*  Function: URLdecode(string strInput)
*
*   note from RFC1630: "Sequences which start with a percent sign but are not followed by 2 hexadecimal
*   characters (0-9, A-F) are reserved for future extension."...Only alphanumerics [0-9a-zA-Z], the special 
*   characters "$-_.+!*'()," [not including the quotes - ed], and reserved characters used for their reserved 
*   purposes may be used unencoded within a URL."
*
* Description:
*   This function takes a URL encoded string and decodes it into a normal string.     
*
* Details:
*   This function travereses the string looking for the % sign followed by two HEX digits.
*   if it encounters a string that has something other than two Hex Digits following
*   the % sign it will fail and return a null string.
*    
* Parameters:
*   strInput                                 <cstring> the string to be converted
*
* Variables:
*  boolExit                           Local   - bool                       loop exit conditional
*  EndPosition                        Local   - <size_t>                   position within string
*  found                              Local   - <size_t>                   position within string
*  Hexdigits                          Local   - <cstring>                  Hex Value culled from strInput
*  index                              Local   - <size_t>                   position within string
*  instream                           Local   - <istringstream>            stream to convert string to hex
*  strOutput                          Local   - <cstring>                  output string
*  intCharacter                       Local   - <int>                      decimal value of character
*  npos                               Local   - <cstring::size_t>          cstring constant delineating eos
*  strASCIIchar                       Local   - <cstring>                  the derived charater
*                                                              
* Functions:
*   .replace()                        Library  - <cstring>                 (string&)
*   .find()                           Library  - <cstring>                 (size_t)
*   .find_first_not_of()              Library  - <cstring>                 (size_t)
*   .clear()                          Library  - <istringstream>           (size_t) 
*   .str()                            Library  - <istringstream>           (size_t)
*   .substr()                         Library  - <cstring>                 (string&)
*   .length()                         Library  - <cstring>                 (size_t)
*
* AUTHOR: 3/10/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation
****************************************************************************************************/
string URLdecode(string strInput)
{
 string 			strOutput = strInput;
 size_t 			index = 0;
 size_t 			found;
 string 			Hexdigits;
 istringstream                 	instream;
 int                           	intCharacter;
 string                        	strASCIIchar;

do
 {
  index = strOutput.find("%", index);

  if (index == string::npos )             {return strOutput;}
//  if (index == 0)                         {return "";}

  if ((index+2)>strOutput.length())         {return "";}
  Hexdigits = strOutput.substr(index+1,2);

  found = Hexdigits.find_first_not_of("0123456789ABCDEF");
  if (found != string::npos)              {return "";}

  instream.clear();
  instream.str(Hexdigits);
  instream >> hex >> intCharacter;

  strASCIIchar = (char) intCharacter;
  strOutput.replace(index,3,strASCIIchar);
 
 }
 while (index != string::npos); 


 return strOutput;
}

string TDD_Decode(string strInput)
{
 string 			strOutput = strInput;
 size_t 			index = 0;
 size_t 			found;
 string 			Hexdigits;
 istringstream                 	instream;
 int                           	intCharacter;
 string                        	strASCIIchar;

do
 {
  index = strOutput.find("%", index);

  if (index == string::npos )             {return strOutput;}

  if ((index+2)>strOutput.length())         {return "";}
  Hexdigits = strOutput.substr(index+1,2);

  found = Hexdigits.find_first_not_of("0123456789ABCDEF");
  if (found != string::npos)              {return "";}

  instream.clear();
  instream.str(Hexdigits);
  instream >> hex >> intCharacter;

  strASCIIchar = (char) intCharacter;
  strOutput.replace(index,3,strASCIIchar);
 
 }
 while (index != string::npos); 


 return strOutput;
}


/****************************************************************************************************
*
* Name:
*  Function: Ip4addressFromSDP(string strInput)
*
* Description:
*   This function takes an SDP string and returns the Ip4 address.     
*
* Details:
* The Ip Address will be parsed based on this sample SDP message:
* v=0
* o=- 1332093251 1332093251 IN IP4 192.168.57.102
* s=Polycom IP Phone
* c=IN IP4 192.168.57.102
* t=0 0
* m=audio 2260 RTP/AVP 8 101
* a=rtpmap:8 PCMA/8000
* a=rtpmap:101 telephone-event/8000
* 
* (if no IP address can be found an empty string is returned)
*
*   
* Parameters:
*   strInput                                 <cstring> the string to be converted
*
* Variables:
*  EndPosition                        Local   - <size_t>                   position within string
*  StartPosition                      Local   - <size_t>                   position within string
*  strOutput                          Local   - <csring>                   return string
*                                                              
* Functions:
*   .find()                           Library  - <cstring>                 (size_t)
*   .substr()                         Library  - <cstring>                 (string&)
*   .length()                         Library  - <cstring>                 (size_t)
*
* AUTHOR: 3/10/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation
****************************************************************************************************/
string Ip4addressFromSDP(string strInput)
{
 size_t StartPosition, EndPosition;
 string strOutput;
 string strKey                      = "IN IP4 ";

 StartPosition = strInput.find(strKey);
 if (StartPosition == string::npos)       {return "";}

 EndPosition = strInput.find("\n", StartPosition);
 if (EndPosition == string::npos)         {return "";}

 StartPosition+=strKey.length();
 strOutput = RemoveLeadingSpaces(strInput.substr(StartPosition, (EndPosition-StartPosition-1)));
 strOutput = RemoveTrailingSpaces(strOutput);

return strOutput;
 
}
/****************************************************************************************************
*
* Name:
*  Function: FreeswitchUsernameFromChannel(string strInput)
*
* Description:
*   This function returns the "Username" from a Freeswitch Channel.     
*
* Details:
* from the following sample channel names:
*
* sofia/internal/sip:CO999x1001.3@192.168.57.102 would return CO999x1001.3
* sofia/internal/103@192.168.57.31               would return 103
* sofia/internal/CO999x1001.3@192.168.57.11      would return CO999x1001.3
*
*   
* Parameters:
*   strInput                                 <cstring> the string to be converted
*
* Variables:
*  end                                Local   - <size_t>                   position within string
*  start                              Local   - <size_t>                   position within string
*  string::npos                       Library - <cstring><size_t>          past end of string 
*  strOutput                          Local   - <cstring>                  return var
*                                                         
* Functions:
*   .find()                           Library  - <cstring>                 (size_t)
*   .substr()                         Library  - <cstring>                 (string&)
*
* AUTHOR: 3/10/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation
****************************************************************************************************/
string FreeswitchUsernameFromChannel(string strInput)
{
 size_t start, end;
 string strOutput;

 start = strInput.find("al/");
 if (start == string::npos)        {return "";}
 start += 3;

 end = strInput.find("@",start);
 if (end == string::npos)          {return "";}

 
 strOutput= strInput.substr(start, (end - start));

 start = strOutput.find("sip:");
 if (start != string::npos) {strOutput=strOutput.substr(start+4,string::npos);}

 start = strOutput.find("-");

 if (start != string::npos) {strOutput=strOutput.erase(start,string::npos);}

 return strOutput;
 
}

string FreeswitchUsernameFromReferedBy(string strInput)
{
 size_t start, end;
 string strOutput;

 start = 0;
 end = strInput.find("@",start);
 if (end == string::npos)          {return "";}

 
 strOutput= strInput.substr(start, (end - start));

 start = strOutput.find("sip:");
 if (start != string::npos) {strOutput=strOutput.substr(start+4,string::npos);}

 start = strOutput.find("-");

 if (start != string::npos) {strOutput=strOutput.erase(start,string::npos);}

 return strOutput;
 
}



string AddNineForOutgoingCalls(string strInput)
{
 string 	strAllDigits = "";
 extern bool 	boolADD_NINE_FOR_OUTBOUND_CALLS;

 if (!boolADD_NINE_FOR_OUTBOUND_CALLS) {return strInput;}


 for (unsigned int i = 0; i < strInput.length(); i++)
  {
   if (!isdigit(strInput[i])) {continue;}
   strAllDigits += strInput[i];
  }

 switch (strAllDigits.length())
  {
   case 11:    case 8:
           if (strAllDigits[0] != '1') {return strAllDigits;}
           return "9" + strAllDigits;
   case 10:    case 7:
           return "9" + strAllDigits;           

   default:
           return strAllDigits;
  }


}

bool IsAudiocodes(string strInput)
{
 size_t found;
 size_t foundsecond;

 found = strInput.find("audiocodes"); if(found != string::npos) {return true;}
 found = strInput.find("AudioCodes"); if(found != string::npos) {return true;}
 found = strInput.find("Audiocodes"); if(found != string::npos) {return true;}
 found = strInput.find("AUDIOCODES"); if(found != string::npos) {return true;}
 found = strInput.find("lng.");
 foundsecond = strInput.find("experient.");
 if((found != string::npos)&&(foundsecond != string::npos))     {return true;}
  
 return false;
}

bool FreeswitchCurrentApplicationIsConference(string strInput)
{
 size_t found;

 found = strInput.find(FREESWITCH_CLI_VARIABLE_CURRENT_APPLICATION_CONFERENCE);

 return (found != string::npos);

}

bool FreeswitchDestNumberIsConfJoin(string strInput)
{
 size_t found;

 found = strInput.find(FREESWITCH_CLI_DEST_NUMBER_CONF_JOIN );

 return (found != string::npos);

}


bool IsCLIregistrationPacket(string strInput)
{
 size_t SearchPosition;

 SearchPosition =  strInput.find(FREESWITCH_CLI_EVENT_SUBCLASS_REGISTER);
 if (SearchPosition != string::npos) {return true;}
 return false;
}

bool IsCLIContentTypePacket(string strInput)
{
 size_t SearchPosition;

 
 string strSearch = "Content-Type: text/event-plain";
 strSearch += charTAB;
 strSearch += charETX;

 SearchPosition =  strInput.find(strSearch);
 if (SearchPosition != string::npos) {return true;}
 

 return false;
}



string CheckForLongDistance(string strInput)
{
 string strAllDigits = "";
 string strAreaCode;
 string strNXXprefix;

 extern Local_Call_Rules LocalCallRules;


 for (unsigned int i = 0; i < strInput.length(); i++)
  {
   if (!isdigit(strInput[i])) {continue;}
   strAllDigits += strInput[i];
  }

 switch (strAllDigits.length())
  {
   case 11:
          strAreaCode  = strAllDigits.substr(1,3);
          strNXXprefix = strAllDigits.substr(4,3);

          if (strAllDigits[0] == '1') 
           {
            //If there is a rule for area code continue checking,  otherwise return the 11 digit number beginning with a 1.
            if (LocalCallRules.fRuleForAreaCodeExists())
             {
              // if it is not a Local area code all is well return the number.
              if (!LocalCallRules.fIsALocalAreaCode(strAreaCode) )       {return strAllDigits;}

             }
            else                                                         {return strAllDigits;}

            // continue checking if there is a rule for Local Exchange, otherwise the number is a Local areacode ... Strip the 1 and return
            if (LocalCallRules.fRuleForNXXexists())
             {
              // if it is a local exhange number strip the 1 and return otherwise the number is good and return
              if (LocalCallRules.fIsALocalExchangePrefix(strNXXprefix) ) {return  strAllDigits.substr(1,10);}
              else                                                       {return  strAllDigits;}
             }
            else                                                         {return  strAllDigits.substr(1,10);}

           } 
          // check to see if the number starts witha 9
          else if (strAllDigits[0] != '9')                               {return strAllDigits;} // bad number, let it get caught by the Telco :(

          // the number starts with a 9
          if (LocalCallRules.fRuleForAreaCodeExists())
           {
            // if it is a not a Local area code , insert a 1 after the 9 and return the number. otherwise return the number it is good
            if (!LocalCallRules.fIsALocalAreaCode(strAreaCode) )        {return "91" + strAllDigits.substr(1,10);}
               
           }
          else                                                          {return strAllDigits;}

            // continue checking if there is a rule for Local Exchange, otherwise the number is a Local areacode 
          if (LocalCallRules.fRuleForNXXexists())
             {
            // if it is a local exhange number return the number, otherwise insert a 1 after the 9 and return the number
            if (LocalCallRules.fIsALocalExchangePrefix(strNXXprefix) ) {return  strAllDigits;}
            else                                                       {return "91" + strAllDigits.substr(1,10);}
           }
          else                                                         {return  strAllDigits;}

          // should not get here
          SendCodingError("globalfunctions.cpp - Coding Error in CheckForLongDistance() a");

   case 10:
          strAreaCode  = strAllDigits.substr(0,3);
          strNXXprefix = strAllDigits.substr(3,3);

          //If there is a rule for area code continue checking,  otherwise return the number
          if (LocalCallRules.fRuleForAreaCodeExists())
           {
            // if it is not a Local area code append a 1 to the number and return 
            if (!LocalCallRules.fIsALocalAreaCode(strAreaCode) )       {return "1" + strAllDigits;}

           }
          else                                                         {return strAllDigits;}

          // continue checking if there is a rule for Local Exchange, otherwise the number is a Local areacode 
          if (LocalCallRules.fRuleForNXXexists())
           {
            // if it is a local exhange number strip the 1 and return otherwise the number is good and return
            if (LocalCallRules.fIsALocalExchangePrefix(strNXXprefix) ) {return  strAllDigits;}
            else                                                       {return  "1" + strAllDigits;}
           }
          else                                                         {return  strAllDigits;}


          // should not get here
          SendCodingError("globalfunctions.cpp - Coding Error in CheckForLongDistance() b");


   default:
           return strAllDigits;
  }

 // should not get here
 SendCodingError("globalfunctions.cpp - Coding Error in CheckForLongDistance() c");
 return strAllDigits;
}



// Functions no longer needed *********************************************************************************


/*
string ConvertCRLFtoEscapeSequence(string strInput)
{
 int            intASCIICode;
 string         strOutput = "";
 unsigned char  uChar;


 for (size_t i = 0; i < strInput.length(); i++)
  {
   uChar = strInput[i];
   intASCIICode = (int) uChar;

   switch (intASCIICode)
   {
    case 10:
     strOutput.append("\\n");break;
    case 13:
     strOutput.append("\\r>");break;
    default:
     strOutput += uChar;
   }//end switch
 }
 return strOutput;
}//end ConvertCRLFtoEscapeSequence

*/




//test functions****************************************************************************************
/*
void enterSyncTableTimeToBroadcastData(long double ldArg)
{
 sem_wait(&sem_tMutexSyncBroadcastData);
 if (SyncTableNumberofEntries < 86400) {SyncTableNumberofEntries++;}
 SyncTableTimeToBroadcast[SyncTableIndex] = ldArg;
 SyncTableIndex++;
 if (SyncTableIndex == 86400){SyncTableIndex = 0;}
 sem_post(&sem_tMutexSyncBroadcastData);
}


void Statistics(long double array[], int size)
{
 int				counter = 1;
 long long int			seconds;
 long double 			sum;
 long double			sumofSquaresofDeviations;
 float 			        max = LDBL_MIN; 
 float			        min = LDBL_MAX;
 float			        average;
 float			        stddev;

 sum 				= (long double) 0.0;
 sumofSquaresofDeviations 	= (long double) 0.0;


 if (size < 2) {return;}

 //calculate mean max min 
 while (counter <= size)
  {
   if (array[counter-1] > max) {max = array[counter-1];}
   if (array[counter-1] < min) {min = array[counter-1];}
   sum =  (sum + array[counter-1]);
   counter++;
  }

 cout << "entries = " << size << endl;

 average = (float) (sum/size);

 counter = 1;
 while (counter <= size)
  {
   sumofSquaresofDeviations = (long double) (sumofSquaresofDeviations + pow( (array[counter-1] - average) , 2 )                   );
   counter++;
  }

 stddev = (float) (sumofSquaresofDeviations/(size-1));

 stddev = sqrt (stddev );

 seconds = (long long int) ((intSERVER_MASTER_SLAVE_BROADCAST_INTERVAL_NSEC/floatNANOSEC_PER_SEC)* size);
 
 cout << "Number of Trunks     = " << intNUM_TRUNKS_INSTALLED << endl;
 cout << "approx time coverage = " << seconds2time(seconds) << endl;
 cout.precision( 20 );   
 cout << "MAX = " << max << endl;
 cout << "MIN = " << min << endl;
 cout << "AVG = " << average << endl;
 cout << "STD = " << stddev << endl;
}



void SyncTableTimeToBroadcastStatistics()
{
 sem_wait(&sem_tMutexSyncBroadcastData);

 cout << endl;
 cout << "Time to Transmit Entire Sync Tables Statistics" << endl;

 Statistics(SyncTableTimeToBroadcast, SyncTableNumberofEntries);

 sem_post(&sem_tMutexSyncBroadcastData);

}


void enterSyncTimeStampData(struct timespec timespecArg)
{
 sem_wait(&sem_tMutexSyncTimeStampData);
 if (SyncTimeStampEntries < 86400) {SyncTimeStampEntries++; Timestamp.push_back(timespecArg);}
 else {Timestamp.pop_front();Timestamp.push_back(timespecArg);}
 sem_post(&sem_tMutexSyncTimeStampData);
}


void SyncTableBroadcastStatistics()
{
 list<struct timespec>::iterator i;
 list<struct timespec>::iterator j;
 
 if (SyncTimeStampEntries < 2){return;}

 long double array[Timestamp.size()-1];

 sem_wait(&sem_tMutexSyncTimeStampData);

 int counter = 0;
 i = Timestamp.begin();
 j = Timestamp.begin(); j++;


 while ( j != Timestamp.end() )
  {
    array[counter] = time_difference(*j, *i);
    i++;j++;counter++;
  }
 cout << endl; 
 cout << "Broadcast Sync Table Interval Statistics" << endl;
 cout.precision( 3 );
 cout << endl; 
 cout << "Programmed Transmission rate = 1 per " << intSERVER_MASTER_SLAVE_BROADCAST_INTERVAL_NSEC/floatNANOSEC_PER_SEC << " Sec" << endl;
 Statistics(array, Timestamp.size()-1);

 sem_post(&sem_tMutexSyncTimeStampData);

}
*/


void FillConferenceObject()
{
 extern Conference_Number               objConferenceList;

 for (int i=0; i< 890; i++) {objConferenceList.AssignConferenceNumber();}
}



struct timespec SecondsInTheFuture(int i)
{
 struct timespec tm;

 clock_gettime(CLOCK_REALTIME, &tm);
 tm.tv_sec += i;
 return tm;
}




void SendRCC_Signal(rcc_state enumRCCstate, int i, bool withSemaphore)
{
 extern RCC_Device              RCCdevice[NUM_WRK_STATIONS_MAX+1];
 string                         strRadio, strTelephone;
 MessageClass                   objMessage;
 int                            intRC;

 if (i > intNUM_WRK_STATIONS)                       {return;}
 if (i < 1)                                         {return;}

 if (withSemaphore)
  {
   intRC = sem_wait(&sem_tMutexRCCtable);
   if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in  SendRCC_Signal() a", 1);}
  }

 RCCdevice[i].boolRCCSendDelayedSignal = false;

 if((!RCCdevice[i].boolContactRelayInstalled)||(!RCCdevice[i].RelayContactClosurePort.GetConnected()))     
  {if (withSemaphore){sem_post(&sem_tMutexRCCtable);}  return;}

 if (boolRCC_DEFAULT_CONTACT_POSITIONS) { strRadio = "CLOSE"; strTelephone = "OPEN";}
 else                                   { strRadio = "OPEN";  strTelephone = "CLOSE";}



 switch (enumRCCstate)
  {
   case RADIO:       
         intRC = Transmit_Data(RCC, i, (char*)strRadio.c_str(), 1, 0);
         if (intRC) {

         }
         break;
    
   case TELEPHONE:
         intRC = Transmit_Data(RCC, i, (char*)strTelephone.c_str(), 1, 0);
         if (intRC) {

         }
         break;
  }

 if (withSemaphore) {sem_post(&sem_tMutexRCCtable);}
}



void Delayed_RCC_RADIO_Signal(union sigval union_sigvalArg) 
{
 int                            intRC;
 extern RCC_Device              RCCdevice[NUM_WRK_STATIONS_MAX+1];

 intRC = sem_wait(&sem_tMutexRCCtable);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in  Delayed_RCC_RADIO_Signal()", 1);}
 if ( RCCdevice[union_sigvalArg.sival_int].boolRCCSendDelayedSignal)
  {SendRCC_Signal(RADIO, union_sigvalArg.sival_int, false);}
 sem_post(&sem_tMutexRCCtable);
}

void Delayed_RCC_TELEPHONE_Signal(union sigval union_sigvalArg) 
{
 int                            intRC;
 extern RCC_Device              RCCdevice[NUM_WRK_STATIONS_MAX+1];

 intRC = sem_wait(&sem_tMutexRCCtable);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in  Delayed_RCC_TELEPHONE_Signal", 1);}
 if ( RCCdevice[union_sigvalArg.sival_int].boolRCCSendDelayedSignal)
  {SendRCC_Signal(TELEPHONE, union_sigvalArg.sival_int, false);}
 sem_post(&sem_tMutexRCCtable);
}



void SendRCC_Signal(rcc_state enumRCCstate, int i, int iDelay)
{
 struct sigevent	        sigeventRCCsignalEvent;
 int                            intRC;
 extern RCC_Device              RCCdevice[NUM_WRK_STATIONS_MAX+1];
 
 if (i > intNUM_WRK_STATIONS)                       {return;}
 if (i < 1)                                         {return;}
 
 switch (enumRCCstate)
  {
   case TELEPHONE:
        sigeventRCCsignalEvent.sigev_notify_function = Delayed_RCC_TELEPHONE_Signal; break;
   case RADIO:
        sigeventRCCsignalEvent.sigev_notify_function = Delayed_RCC_RADIO_Signal; break;
  }
  
 sigeventRCCsignalEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventRCCsignalEvent.sigev_notify_attributes                 = NULL;
 sigeventRCCsignalEvent.sigev_value.sival_int                   = i;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventRCCsignalEvent, &timer_tRCC_Send_Event_TimerId);
 if (intRC){cout << "Unable to Create Time in fn SendRCC_Signal()"; return;}
 // load settings into stucture and then stucture into the timer
 itimerspecRCCSendDelay.it_value.tv_sec                       = iDelay;
 itimerspecRCCSendDelay.it_value.tv_nsec                      = 0;	                				
 itimerspecRCCSendDelay.it_interval.tv_sec                    = 0;			               
 itimerspecRCCSendDelay.it_interval.tv_nsec                   = 0;
 if(boolSTOP_EXECUTION) {return;}         
 else                   {timer_settime(timer_tRCC_Send_Event_TimerId, 0, &itimerspecRCCSendDelay, NULL);}

 intRC = sem_wait(&sem_tMutexRCCtable);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in  SendRCC_Signal() b", 1);}
 RCCdevice[i].boolRCCSendDelayedSignal = true;
 sem_post(&sem_tMutexRCCtable);
}

string ConnectionType(Port_Connection_Type ePortConnectionType) {
 switch (ePortConnectionType)  {
   case UDP: return "UDP";
   case TCP: return "TCP";
   default:
        break; 
 }
 return "Error";
}
 
string ALIProtocol(Port_ALI_Protocol_Type eALIportProtocol) {
 switch (eALIportProtocol)  {
   case ALI_MODEM:      return "Modem";
   case ALI_E2:         return "E2";
   case ALI_SERVICE:    return "Service";
   default: break;
 }
 return "Error";
}


void SendRCC_Signal(rcc_state enumRCCstate, ExperientDataClass objData)
{
 string                                         strPosition;
 int                                            intPosOne, intPosTwo;
 extern Telephone_Devices                       TelephoneEquipment;         
 extern bool                                    bool_BCF_ENCODED_IP;

 if (objData.FreeswitchData.objLinkData.bBothChannelsPositions)
  {
   if (bool_BCF_ENCODED_IP) 
    {
     intPosOne = TelephoneEquipment.fPositionNumberFromChannelName(objData.FreeswitchData.objLinkData.strChannelone);
     intPosTwo = TelephoneEquipment.fPositionNumberFromChannelName(objData.FreeswitchData.objLinkData.strChanneltwo);    
    }
   else
    {
     intPosOne = TelephoneEquipment.fPostionNumberFromIPaddress(objData.FreeswitchData.objLinkData.IPaddressOne.stringAddress);
     intPosTwo = TelephoneEquipment.fPostionNumberFromIPaddress(objData.FreeswitchData.objLinkData.IPaddressTwo.stringAddress);    
    }

   if (intPosOne)
    {SendRCC_Signal(enumRCCstate, intPosOne , true);}

   if (intPosTwo)
    {SendRCC_Signal(enumRCCstate, intPosTwo , true);}    
  }
/*
 else if (objData.FreeswitchData.objDialData.bBothChannelsPositions)
  {
   strPosition = AsteriskPositionFromChannel(objData.FreeswitchData.objDialData.strChannelone);
   if (!strPosition.empty())
    {SendRCC_Signal(enumRCCstate, char2int(strPosition.c_str()) , true);}
   strPosition = AsteriskPositionFromChannel(objData.FreeswitchData.objDialData.strChanneltwo);
   if (!strPosition.empty())
    {SendRCC_Signal(enumRCCstate, char2int(strPosition.c_str()) , true);}    
  }
*/
 else { SendRCC_Signal(enumRCCstate, objData.CallData.intPosn, true);}

}

string SpaceSeparatedNthField(string str, int N) {
    // Used to split string around spaces.
    istringstream ss(str);
    int i = 0;
    string field; // for storing each word
 
    // Traverse through all words
    // while loop till we get to Nth Field
    while (ss >> field) {
        i++;
        // the read word
        if (i == N) {return field;}
    }
    return "";
}


string CPU_USAGE(pid_t pid) {
 string                 stringReturnMessage;
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024]; 
 MessageClass           objMessage;
 string                 strCommand = "top -b -n 1 -p " + int2str(pid);

 extern string SpaceSeparatedNthField(string str, int N);

 command = (char*) strCommand.c_str(); 
 if ( !(fpipe = (FILE*)popen(command,"r")) )   {

    // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,144, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_144, CPU_USAGE_SYSTEM_COMMAND); 
   enQueue_Message(MAIN,objMessage);
   return "ERROR";

 }

 while ( fgets( line, sizeof line, fpipe))  {
 
   stringReturnMessage = line; // this will set it to the last line
 }
 
 pclose(fpipe);

 return SpaceSeparatedNthField(stringReturnMessage,TOP_OUTPUT_CPU_FIELD_COLUMN_NUMBER);

}

string MEM_USAGE(pid_t pid) {
 string                 stringReturnMessage;
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024]; 
 MessageClass           objMessage;
 string                 strCommand = "top -b -n 1 -p " + int2str(pid);
// string                 strCommand = MEM_USAGE_SYSTEM_COMMAND + int2str(pid);

extern string SpaceSeparatedNthField(string str, int N);

 command = (char*) strCommand.c_str(); 
 if ( !(fpipe = (FILE*)popen(command,"r")) )   {

    // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,144, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_144, CPU_USAGE_SYSTEM_COMMAND); 
   enQueue_Message(MAIN,objMessage);
   return "ERROR";

 }

 while ( fgets( line, sizeof line, fpipe))  {
 
   stringReturnMessage = line; // this will set it to the last line 
 }
 
 pclose(fpipe);

 
 return SpaceSeparatedNthField(stringReturnMessage,TOP_OUTPUT_MEM_FIELD_COLUMN_NUMBER);
}


string MonitCommandLine(string strProgram, string strCommand)
{
 string                 strMessage;
 string                 stringReturnMessage;
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024]; 
 MessageClass           objMessage;
 
 
 strMessage = "monit ";
 strMessage += strCommand;
 strMessage += " ";
 strMessage += strProgram;

 command = (char*) strMessage.c_str(); 
 if ( !(fpipe = (FILE*)popen(command,"r")) )  {
    // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,144, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_144, strMessage); 
   enQueue_Message(MAIN,objMessage);
   return "ERROR";

 }

 while ( fgets( line, sizeof line, fpipe))  {
  
   stringReturnMessage = line;

 }
 
 pclose(fpipe);
return stringReturnMessage;
}




int GMT_Offset() {
 string                 stringReturnMessage;
 MessageClass           objMessage;
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024];
 int                    iSign;
 size_t                 found;

 command = (char*) GMT_OFFSET_SYSTEM_COMMAND; 


 if ( !(fpipe = (FILE*)popen(command,"r")) )
  {  // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,144, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_144, GMT_OFFSET_SYSTEM_COMMAND); 
   enQueue_Message(MAIN,objMessage);
   return 0;
  }

 while ( fgets( line, sizeof line, fpipe))
  {
   stringReturnMessage = line;
  }
 
 pclose(fpipe);

 if (stringReturnMessage.length() < 5) { SendCodingError( "globalfunctions.cpp - error in GMT Offset length less than 5"); return 0;}
 if (stringReturnMessage.length() > 5) { stringReturnMessage.erase(5,string::npos);}
 if (stringReturnMessage[0] == '-')    {iSign = -1;}
 else                                  {iSign =  1;}
 stringReturnMessage.erase(0,1);
 found = stringReturnMessage.find_first_not_of("0123456789");
 if (found != string::npos)            {SendCodingError( "globalfunctions.cpp - Invalid characters in GMT offset return string" ); return 0;}
 

return iSign * char2int(stringReturnMessage.c_str());       
}

string NAPTR(string strURI, int i) {
 MessageClass           objMessage;
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024];
 int                    iSign;
 size_t                 found;
 string                 stringReturnMessage ="";
 string                 strCommand = "dig ";
 string 		strSubstr;
 int                    count = 0;
 strCommand += strURI;
 strCommand += " naptr | grep LoST:http";

 //check if address is already resolved ....
 found = strURI.find("http", 0);
 if (found != string::npos) {return strURI;}


 command = (char*) strCommand.c_str();

 if ( !(fpipe = (FILE*)popen(command,"r")) )
  {  // If fpipe is NULL
   objMessage.fMessage_Create(LOG_WARNING ,144, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_144, strCommand); 
   enQueue_Message(MAIN,objMessage);
   return "";
  }

 while ( fgets( line, sizeof line, fpipe)) {
   
  stringReturnMessage = line;
  if (count == i) {break;}
  stringReturnMessage.clear();
  count ++;
 }
 
 pclose(fpipe);

 found = stringReturnMessage.find("!.*!", 0);
 if (found == string::npos) {return "";}

 strSubstr.assign(stringReturnMessage, found+4, string::npos);
 found = strSubstr.find("!\"",0);
 if (found == string::npos) {return "";}

 stringReturnMessage.assign(strSubstr, 0, found);

return stringReturnMessage;       
}




bool RestartFreeswitch()
{
 char                   *command;
 char                   line[1024];
 FILE                   *fpipe;
 string                 stringReturnMessage = "";

 command = (char*) "service freeswitch restart"; 

 if ( !(fpipe = (FILE*)popen(command,"r")) )
  {  // If fpipe is NULL
   cout << "unable to open pipe for Freeswitch Restart" << endl;
   return false;
  }

 while ( fgets( line, sizeof line, fpipe))
  {
   stringReturnMessage += line;
  }

 pclose(fpipe);

 if (stringReturnMessage.empty()) {return false;}

 return true;
}





void DisplayCPUstatisticsPerThread()
{
 extern vector <Thread_Data> 	ThreadData;
 const long double  	        longdoubleNANOSEC_PER_SEC  = 1000000000.0; 
 string                         CPU_USAGE(pid_t pid);
 long double                    dTimeProcess;
 long double                    dTimeThread;
 clockid_t              	cid;
 struct timespec        	timespecProcess;
 struct timespec        	timespecThread;


 clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timespecProcess);

 dTimeProcess = timespecProcess.tv_sec + timespecProcess.tv_nsec/longdoubleNANOSEC_PER_SEC;
 cout << "CPU Utilization Per Thread:" << endl;
 cout << "NG911 = " << setprecision (10) << dTimeProcess << endl;

 for (vector<Thread_Data>::iterator it = ThreadData.begin() ; it != ThreadData.end(); ++it)
  {   
   pthread_getcpuclockid(it->pth_SelfID, &cid);
   clock_gettime(cid, &timespecThread);
   dTimeThread = timespecThread.tv_sec + timespecThread.tv_nsec/longdoubleNANOSEC_PER_SEC;
   cout << it->strThreadName << ": " << setprecision (10) << dTimeThread <<  endl;
   
  }


}




void MemoryUsage()
{
 int                    intSec;
 struct timespec        timespecTimeNow;
 string                 stringReturnMessage = "";
 string                 stringTemp = "";
 FILE                   *fpipe;
 char                   *command;
 char                   line[1024];
 long long int          iKB;
 size_t                 found;
 size_t                 start;
 size_t                 pos;
 size_t                 end;
 string                 strPctMem;
 string                 strPID;
 string                 strVSZ;
 string                 strRSS;

 extern long long int   intKBused;


 command = (char*) "ps aux | grep 911.exe"; 
 clock_gettime(CLOCK_REALTIME,&timespecTimeNow);
 intSec   = (int(int(int(timespecTimeNow.tv_sec)%86400)%3600)%60);
 if (intSec != 0) return;

 if ( !(fpipe = (FILE*)popen(command,"r")) )
  {  // If fpipe is NULL
   cout << "unable to open pipe for memcheck" << endl;
   return;
  }

 while ( fgets( line, sizeof line, fpipe))
  {
   stringTemp = line;
        
   found = stringTemp.find("grep");
   if (found == string::npos) {stringReturnMessage = stringTemp;} 
  }


 pclose(fpipe);

 if (stringReturnMessage.empty()) {return;}


 start = stringReturnMessage.find_first_of(" \t");
 if (start == string::npos) {return;} 


  for (int i = 1; i < 6; i++)
  {
   pos = stringReturnMessage.find_first_not_of(" \t", start+1);
   if (pos == string::npos) {return;}
   start = pos;
   end = stringReturnMessage.find_first_of(" \t", start+1); 
   if (end == string::npos) {return;} 
 
   stringTemp = stringReturnMessage.substr(start,(end -start)); 
   switch (i)
    {
     case 4:
           iKB = char2int(stringTemp.c_str()); break; 
 
     case 5:
           iKB += char2int(stringTemp.c_str()); break;

     default: break;
    }
   start = end;
  }

 if (iKB > intKBused)
  {
   cout << "USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND" << endl;
   cout << stringReturnMessage << endl;
   cout << "Total KB = " << iKB << endl;
   cout << "************************** Memory Increase at " << get_Local_Time_Stamp () << " ***************" << endl;
   intKBused = iKB;
  }


return;       
}

string RemoveIPaddressFromPhoneNumber(string strInput)
{
 size_t found;

 found = strInput.find("@");
 if (found == string::npos) {return strInput;}
 if (found == 0)            {return "Unknown";}

return strInput.substr(0, found);

}
string RemoveSIPcolon(string strInput)
{
 size_t found;

 found = strInput.find("sip:");
 if (found == string::npos) {return strInput;}

 return strInput.substr(found+4,string::npos);
}

string RemoveSIPcolonPlusATaddress(string strInput) {

string returnstring;
size_t found;

 returnstring = RemoveSIPcolon(strInput);

 found = returnstring.find("@");

 if (found == 0)            {return "";}
 if (found == string::npos) {return returnstring;}

return returnstring.substr(0, found);
}


bool SIPnumberhasAllNumbers(string strInput)
{
 size_t found;
 string strAllNumbers;

 found = strInput.find("@");
 if (found == string::npos) {return false;}
 if (found == 0)            {return false;}

 strAllNumbers = strInput.substr(0, found);

 found = strAllNumbers.find_first_not_of("0123456789");
 if (found != string::npos)  {return false;}

 return true;

}

string FQDNfromURI(string strInput)
{
 size_t found;
 string ReturnString = strInput;

 found = strInput.find("@");
 if (found == string::npos) {return ReturnString;}

 ReturnString.erase(0, found+1);
 return ReturnString;
}
bool IsSIP_URI(string strInput)
{
 size_t found;
 found = strInput.find("@");
 if (found == string::npos) {return false;}

 return true;
}

string MakeXMLstring(XMLNode XMLData) {
 char* cptrResponse = NULL;
 string strReturn = "";

 cptrResponse = XMLData.createXMLString();
 if (cptrResponse != NULL)   {
  strReturn = cptrResponse;
  free(cptrResponse);
 }
 cptrResponse = NULL;

 return strReturn;
}


int EventIdInteger(string strInput)
{
 XMLNode			MainNode, EventNode;
 XMLResults			xe;
 string                         strEventUniqueID;
 size_t                         found;

 MainNode = XMLNode::parseString(strInput.c_str(),NULL,&xe);
 if (xe.error)                 { cout << "xe error" << endl; return 0;}
 if (MainNode.isEmpty())       { cout << "main node empty" << endl;return 0;}
 EventNode = MainNode.getChildNode(XML_NODE_EVENT);
 if (EventNode.isEmpty())      {cout << "event node empty" << endl; return 0;}
 if (EventNode.getAttribute(XML_FIELD_PACKET_ID))  {strEventUniqueID = EventNode.getAttribute(XML_FIELD_PACKET_ID);}
 else                          {cout << "no attribute" << endl; return 0;}
 if (strEventUniqueID.empty()) { cout << "attribute empty" << endl; return 0;} 
 found = strEventUniqueID.find_first_not_of("0123456789");
 if (found != string::npos)    {cout << "found non digit" << endl;return 0;}

 return char2int(strEventUniqueID.c_str());
}



string ServerNametoPositionChannelKey(unsigned int intSuffix)
{
 size_t found;
 string strOutput = stringSERVER_HOSTNAME;
 string strSuffix;
 if (intSuffix > 99) {return "error";}
 found = strOutput.find("-");
 if (found == string::npos) {return "error";}
 strOutput.erase(found,1);
 found = strOutput.find("-");
 if (found == string::npos) {return "error";}
 strOutput.erase(found,string::npos);
 strOutput += "x";
 strOutput += int2strLZ(intSuffix);

 return strOutput;

}

void SendCodingError(string strInput)
{
 MessageClass objMessage;
 cout << strInput << endl;
 objMessage.fMessage_Create(LOG_EMAIL_WARNING,184, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                            objBLANK_KRN_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_184 , strInput); 
 enQueue_Message(MAIN,objMessage);
}

int Test_return_position_from_raw_xml(const char* charString)
{
 XMLNode            MainNode, EventNode, WorkingNode;
 XMLResults         xe;
 ExperientDataClass Test;

 MainNode=XMLNode::parseString(charString,NULL,&xe);
 EventNode = MainNode.getChildNode(XML_NODE_EVENT);
 WorkingNode = EventNode.getChildNode( "Heartbeat" );

  if(WorkingNode.getAttribute(XML_FIELD_POSITION))
   {Test.fLoad_Position(XML_DATA_FIELD, WRK, WorkingNode.getAttribute(XML_FIELD_POSITION), 1, charString, false, false);}

  return Test.CallData.intPosn;
}

void SetAnswerButtonONPositionsWithUsername(string strInput, int iFromPosition)
{
 int						iDeviceIndex;
 int						intRC;
 extern int                                     intNUM_WRK_STATIONS;
 extern Telephone_Devices                       TelephoneEquipment; 

 for (int i = 1; i<= intNUM_WRK_STATIONS; i++){
  if (i == iFromPosition) {continue;}

  iDeviceIndex = TelephoneEquipment.fIndexWithPositionNumber(i);
  if (iDeviceIndex < 0) {continue;}

  intRC = TelephoneEquipment.Devices[iDeviceIndex].fLineNumberWithRegistrationName(strInput); 
  if (intRC <= 0) {continue;}

  

 }

}





string CallerNameLookup( string strNumber)
{
 int                                            intRC;
 string                                         strOutput;
 size_t                                         sz;
 extern vector <PhoneBookEntry>                 vCALL_LIST;
 extern Telephone_Devices                       TelephoneEquipment; 

 intRC = sem_wait(&sem_tMutexCall_LIST);
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexCall_LIST, "sem_wait@sem_tMutexCall_LIST in fn CallerNameLookup()", 1);}

 sz = vCALL_LIST.size();
 strOutput = "Unknown";

 for (unsigned int i=0; i < sz; i++)
  {
   if (strNumber ==  vCALL_LIST[i].strCallerID) {strOutput = vCALL_LIST[i].strCustName; break;}
  }

 sem_post(&sem_tMutexCall_LIST);

 if (strOutput == "Unknown"){
  strOutput = TelephoneEquipment.fCallerIDnameofTelephonewithRegistrationName(strNumber);
 }

 return strOutput;
}


string findWindowsUser(string strInput)
{
 size_t found;
 size_t start, end;

 found = strInput.find(XML_FIELD_WINDOWS_USER);
 if (found == string::npos) {return "";}

 start = strInput.find("\"", found);
 if (start == string::npos) {return "";}

 if (strInput.length() <= start+1) {return "";}

 end = strInput.find( "\"", start+1);
 if (end == string::npos) {return "";}


 return strInput.substr(start+1, (end - (start+1)));

}


bool fexists(const char *filename)
{
  struct stat buf;
  return (stat(filename, &buf) == 0);
}

bool UpdateSupressMessageNumberList(string strList, bool boolNewValue)
{
 string strTemp = strList;
 size_t start, end, found;
 string strNumber;
 int    intNumber = 0;
 extern volatile bool                           boolEmailSupress[1000];

 strTemp = RemoveAllSpaces(strTemp);
 start   = 0;
  do
   {
    start = strTemp.find_first_of("0123456789", start);
    if (start == string::npos)                         { break; }
    end   = strTemp.find_first_of(",|", start);
    if (end == string::npos)                           { end =  strTemp.length(); }
    if ((end - start)!= 3)                             { return false; }
    strNumber.assign(strTemp, start, (end - start));
    found = strNumber.find_first_not_of("0123456789");
    if (found != string::npos)                         { return false; }
    intNumber = char2int(strNumber.c_str());
    boolEmailSupress[intNumber] = boolNewValue;
    start = end;
   } while (true);

 return true;
}
  
void testaddconnection()
{
 extern ExperientCommPort                               WRKThreadPort;
 for (int i = 1; i <=intNUM_WRK_STATIONS*3; i++)
  {
   WRKThreadPort.TCP_Server_Port.vConnectionList[i].boolConnected = true;
  }


}

int msleep(unsigned long milisec)
{
    struct timespec req={0};
    time_t sec=(int)(milisec/1000);
    milisec=milisec-(sec*1000);
    req.tv_sec=sec;
    req.tv_nsec=milisec*1000000L;
    while(nanosleep(&req,&req)==-1 && errno == EINTR)
         continue;
    return 1;
}

void experient_nanosleep(long int iNumberofNanoSecods)
{
 string strMessage = "globalfunctions.cpp: experient_nanosleep() error->";
 struct timespec timespecSleep;
 struct timespec timespecRemainingTime;
 struct timespec timespecTryAgain;
 int intRC;

 timespecSleep.tv_sec = 0;
 timespecSleep.tv_nsec = iNumberofNanoSecods;
 timespecTryAgain.tv_sec = 0;

 intRC = nanosleep(&timespecSleep, &timespecRemainingTime);

 while (intRC)
  {
   
   if (errno == EINTR) 
    {
     timespecTryAgain.tv_nsec = timespecRemainingTime.tv_nsec;
     nanosleep(&timespecTryAgain, &timespecRemainingTime);
    }
   else
    {
     strMessage += int2str(errno);
     SendCodingError(strMessage );
     intRC = 0;
    }
  }
 return;
}

string Determine_BCF_Key(string strData)
{
 size_t found;

 found = strData.find("endpoint=");
 if (found != string::npos) {return "endpoint=";}

 found = strData.find("Endpoint=");
 if (found != string::npos) {return "Endpoint=";}

 found = strData.find("firewall=");
 if (found != string::npos) {return "firewall=";}

 found = strData.find("Firewall=");
 if (found != string::npos) {return "Firewall=";}


 return "NOKEY";
}

string ParseCallID_NumberFromAudiocodes(string strInput, bool boolReversed)
{
 string strOutput;
 string strCheckRingANI;

 switch (boolReversed)
  {
   case true:
        strOutput = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_OTHER_LEG_CALLER_ID_NUMBER_WO_COLON));
        break;
   case false:
        strOutput = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_OTHER_LEG_DESTINATION_NUMBER_WO_COLON));
        break;
  }
 //check for Variable Ring ANI ......
 strCheckRingANI = URLdecode(ParseFreeswitchData(strInput,FREESWITCH_CLI_VARIABLE_RING_ANI_WO_COLON));
 
 if (strCheckRingANI.length()) {return strCheckRingANI;}

return strOutput;
}

string  NG911_TRANSFER_TYPE(sip_ng911_transfer_type etype)
{
 switch (etype)
 {
  case NO_TRANSFER_DEFINED: 		return "legacy";
  case SIP_URI_INTERNAL:    		return "internal";
  case SIP_URI_EXTERNAL:    		return "external";
  case SIP_URI_EXTERNAL_EXPERIENT: 	return "external.experient";

 }
 return ""; //should not get here ..
}

sip_ng911_transfer_type NG911_TRANSFER_TYPE(string strInput)
{
 if (strInput.empty())                 {return NO_TRANSFER_DEFINED;}
 if (strInput == "internal")           {return SIP_URI_INTERNAL;}
 if (strInput == "external")           {return SIP_URI_EXTERNAL;}
 if (strInput == "external.experient") {return SIP_URI_EXTERNAL_EXPERIENT;}
 if (strInput == "legacy")             {return NO_TRANSFER_DEFINED;}
 if (strInput == "BLIND_TRANSFER")     {return NO_TRANSFER_DEFINED;}

 SendCodingError("globalfunctions.cpp - CODING ERROR in NG911_TRANSFER_TYPE() ->" + strInput);
 return NO_TRANSFER_DEFINED;
}

bool IsControllerBlindTransfer(string strInput)
{
 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_BLIND_TRANSFER_WO_COLON );

 return (!strData.empty());
}
bool SIP_TRANSFER_FROM_POSTION(string strInput)
{
 string strSIP_Transfer_From = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_TRANSFER_FROM_POSITION_WO_COLON );

 return(!strSIP_Transfer_From.empty());
}

bool Channel_NAME_IS_A_POSITION(string strInput)
{

return false;
}
sip_ng911_transfer_type NG911_TRANSFER_TYPE_FROM_CLI(string strCLI_Input)
{
 string strResult = "";

 strResult = ParseFreeswitchData(strCLI_Input, FREESWITCH_CLI_VARIABLE_TRANSFER_TYPE_W_COLON );

 return NG911_TRANSFER_TYPE(strResult);
}

bool IsPositionPartofAGroupThatIsOnTheCall(int iPosition, Call_Data objCallData)
{
 int 						index1,index2;
 extern Workstation_Groups                	WorkstationGroups;

 index2 = WorkstationGroups.IndexOfGroupWithWorkstation(iPosition);
 if (index2 < 0)                                    {return false;}

 for (unsigned int i = 1; i <= NUM_WRK_STATIONS_MAX; i++) {
  if (!objCallData.ConfData.fPositionInHistory(i) ) {continue;}
  index1 = WorkstationGroups.IndexOfGroupWithWorkstation(i);
  if (index1 < 0)                                   {continue;}
  if (index1 == index2)                             {return true;}
 }
 return false;
}

string after_last_colon(string strInput) {
size_t found;

 found = strInput.find_last_of(":");
 if (found == string::npos) {
  return strInput;
 }

 if (found == strInput.length()-1) {
  return strInput;
 }

 return strInput.substr(found+1, string::npos);
}

string RemoveSemicolonToEnd(string strInput) {
size_t found;

 found = strInput.find_first_of(";");
 if (found == string::npos) {
  return strInput;
 }

 return strInput.substr(0, found);
 

}


string RemoveTelColon(string strInput) {
string 			strReturn;
extern string  		FindandReplace(string stringArg, string stringFind, string stringReplace);

 strReturn = FindandReplace(strInput,  "tel:", "");
 strReturn = FindandReplace(strReturn, "Tel:", "");
 strReturn = FindandReplace(strReturn, "TEL:", "");

 return strReturn;
}

string StripPlusOne(string strInput)
{
 string strOutput = strInput;

 if (strInput.length() < 3) {
  SendCodingError("globalfunctions.cpp - Coding Error in StripPlusOne() ->"+ strOutput);
  return strOutput;
 }

 if ((strInput[0] == '+')&&(strInput[1] == '1')) {
  strOutput = strInput.substr(2,string::npos);
 }

 return strOutput;
}

string StripOnePlus(string strInput)
{
 string strOutput = strInput;

 if (strInput.length() < 3) {
  SendCodingError("globalfunctions.cpp - Coding Error in StripPlusOne() ->"+ strOutput);
  return strOutput;
 }

 if ((strInput[0] == '1')&&(strInput[1] == '+')) {
  strOutput = strInput.substr(2,string::npos);
 }

 return strOutput;
}

nena_adr_service_environment DetermineServiceEnvironment(string strInput) {

if (strInput == "Residence") 	{return seResidence;}
if (strInput == "RESIDENCE") 	{return seResidence;}
if (strInput == "residence") 	{return seResidence;}

if (strInput == "Business") 	{return seBusiness;}
if (strInput == "BUSINESS") 	{return seBusiness;}
if (strInput == "business") 	{return seBusiness;}

 return seOther;
}

nena_adr_service_mobility DetermineServiceMobility(string strInput) {

if (strInput == "Fixed") 	{return smFixed;}
if (strInput == "FIXED") 	{return smFixed;}
if (strInput == "fixed") 	{return smFixed;}

if (strInput == "Mobile") 	{return smMobile;}
if (strInput == "MOBILE") 	{return smMobile;}
if (strInput == "mobile") 	{return smMobile;}

if (strInput == "Nomadic") 	{return smNomadic;}
if (strInput == "NOMADIC") 	{return smNomadic;}
if (strInput == "nomadic") 	{return smNomadic;}

return smOther;
}

nena_adr_service_type DetermineServiceType(string strInput) {
string strData = FindandReplaceALL(strInput, " ", "");

if (strData == "Pots") 	{return stPOTS;}
if (strData == "POTS") 	{return stPOTS;}
if (strData == "pots") 	{return stPOTS;}

if (strData == "Pots;Remote") 	{return stPOTSremote;}
if (strData == "POTS;REMOTE") 	{return stPOTSremote;}
if (strData == "POTS;remote") 	{return stPOTSremote;}
if (strData == "pots;remote") 	{return stPOTSremote;}


if (strData == "Coin") 	{return stCOCoin;}
if (strData == "COIN") 	{return stCoin;}
if (strData == "coin") 	{return stCoin;}

if (strData == "Wireless") 	{return stWireless;}
if (strData == "WIRELESS") 	{return stWireless;}
if (strData == "wireless") 	{return stWireless;}

if (strData == "MLTS-hosted") 	{return stMLTShosted;}
if (strData == "MLTS-Hosted") 	{return stMLTShosted;}
if (strData == "MLTS-HOSTED") 	{return stMLTShosted;}
if (strData == "mlts-hosted") 	{return stMLTShosted;}

if (strData == "MLTS-local") 	{return stMLTSlocal;}
if (strData == "MLTS-Local") 	{return stMLTSlocal;}
if (strData == "MLTS-LOCAL") 	{return stMLTSlocal;}
if (strData == "mlts-local") 	{return stMLTSlocal;}

if (strData == "Coin;One-way") 	{return stOneWay;}
if (strData == "Coin;One-Way") 	{return stOneWay;}
if (strData == "COIN;ONE-WAY") 	{return stOneWay;}
if (strData == "coin;one-way") 	{return stOneWay;}

if (strData == "VOIP;COIN") 	{return stVOIPcoin;}
if (strData == "VOIP;coin") 	{return stVOIPcoin;}
if (strData == "voip;coin") 	{return stVOIPcoin;}
if (strData == "VoIP;coin") 	{return stVOIPcoin;}

if (strData == "VOIP") 	{return stVOIP;}
if (strData == "VoIP") 	{return stVOIP;}
if (strData == "voip") 	{return stVOIP;}

return stOther;
}

nena_geopriv_method DetermineMethod(string strInput) {

if (strInput == "Manual") 	{return ngManual;}
if (strInput == "MANUAL") 	{return ngManual;}
if (strInput == "manual") 	{return ngManual;}

if (strInput == "Cell") 	{return ngCell;}
if (strInput == "CELL") 	{return ngCell;}
if (strInput == "cell") 	{return ngCell;}

if (strInput == "Wireless") 	{return ngWireless;}
if (strInput == "WIRELESS") 	{return ngWireless;}
if (strInput == "wireless") 	{return ngWireless;}

return ngOther;
}



bool PhantomPort(string strData) {
 size_t found;

 found = strData.find("Phantom");
 if (found != string::npos) {return true;}
 found = strData.find("phantom");
 if (found != string::npos) {return true;}
 found = strData.find("PHANTOM");
 if (found != string::npos) {return true;}

 return false;
}


int ConvertLOGcodetoSYSlogCode(int intLogCode)
{
/*
 https://en.wikipedia.org/wiki/Syslog#Severity_level

Value 	Severity 	Keyword 	Deprecated keywords 	Description 				Condition
0 	Emergency 	emerg 		panic[7] 		System is unusable 			A panic condition.[8]
1 	Alert 		alert 					Action must be taken immediately 	A condition that should be corrected immediately, such as a corrupted system database.[8]
2 	Critical 	crit 					Critical conditions 			Hard device errors.[8]
3 	Error 		err 		error[7] 		Error conditions 	
4 	Warning 	warning 	warn[7] 		Warning conditions 	
5 	Notice 		notice 					Normal but significant conditions 	Conditions that are not error conditions, but that may require special handling.[8]
6 	Informational 	info 					Informational messages
7 	Debug 		debug 					Debug-level messages 			Messages that contain information normally of use only when debugging a program.[8]
 

*/
 switch (intLogCode)
  {
   case LOG_ALARM:   		return 2;
   case LOG_WARNING:     	return 4;
   case LOG_INFO:    		return 5;
   case LOG_CONSOLE_FILE:	return 6; 

   default:          		return 7;

  }

 return 0;
}

#ifdef IPWORKS_V16
void AddTimeStamptoJSONobject(JSON *JSONobject, struct timespec RealtimeStamp, struct timespec MonotonictimeStamp) {

extern string			strCONTROLLER_UUID;

// Time object 
JSONobject->StartObject();
JSONobject->PutName("t");
JSONobject->StartObject();
JSONobject->PutName("ut");
JSONobject->StartObject();
JSONobject->PutProperty("s",  (char*) int2str(RealtimeStamp.tv_sec).c_str(), 3);
JSONobject->PutProperty("ns", (char*) int2str(RealtimeStamp.tv_nsec).c_str(), 3);
JSONobject->EndObject();
JSONobject->StartObject();
JSONobject->PutName("m");
JSONobject->StartObject();
JSONobject->PutProperty("s",  (char*) int2str(MonotonictimeStamp.tv_sec).c_str(), 3);
JSONobject->PutProperty("ns", (char*) int2str(MonotonictimeStamp.tv_nsec).c_str(), 3);
JSONobject->EndObject();
JSONobject->EndObject();
JSONobject->EndObject();
// Time object   
return;
}




void AddPortInfotoJSONobject(JSON *JSONobject, Port_Data objPortArg) {

ExperientCommPort                       CADPort                                 [NUM_CAD_PORTS_MAX+1];
size_t					sz;
vector <Position_Alias>                 vPositionAlias;

 if (objPortArg.fRemotePortNumber() == 0) {return;}

 string strThread = Thread_Calling(objPortArg.enumPortType).erase(3);

 // Port Object
 JSONobject->StartObject();
 JSONobject->PutName("commport");
 JSONobject->StartObject();
 JSONobject->PutProperty("name", (char*) strThread.c_str() , 2);
 JSONobject->StartObject();
 JSONobject->PutProperty("number", (char*) int2str(objPortArg.intPortNum).c_str() , 3);
 if (objPortArg.strSideAorB.length() > 0) {
  JSONobject->StartObject();
  JSONobject->PutProperty("AB", (char*) objPortArg.strSideAorB.c_str() , 2);
 }
 JSONobject->StartObject();
 JSONobject->PutProperty("protocol", (char*) objPortArg.fConnectionProtocol().c_str() , 2);
 if (objPortArg.fNotes().length() > 0) {
  JSONobject->StartObject();
  JSONobject->PutProperty("notes", (char*) objPortArg.fNotes().c_str() , 2);
 }
 switch (objPortArg.enumPortType) {
  case CAD:
       if (objPortArg.fRemoveFirstALiCR()) {
        JSONobject->PutProperty("Remove_First_ALI_CR", "true" , 4);
       }
       else {
        JSONobject->PutProperty("Remove_First_ALI_CR", "false" , 4); 
       } 
       if (objPortArg.fUseNENACADheader()) {
        JSONobject->PutProperty("Use_NENA_Header", "true" , 4);
       }
       else {
        JSONobject->PutProperty("Use_NENA_Header", "false" , 4); 
       }
       if (objPortArg.intPortNum > NUM_CAD_PORTS_MAX) {break;/*coding error ?????*/}

       vPositionAlias = objPortArg.fCadPortAliasVector();
       sz = vPositionAlias.size();

       if (!vPositionAlias.empty()) { 
        JSONobject->StartObject();
        JSONobject->PutName("aliases");
        JSONobject->StartArray();
        for (unsigned int i = 0; i < sz; i++) {
         JSONobject->StartObject();
         JSONobject->PutProperty("position", (char*) int2str(vPositionAlias[i].iPosition).c_str() , 2);
         JSONobject->PutProperty("alias", (char*) int2str(vPositionAlias[i].iAlias).c_str() , 2);
         JSONobject->EndObject();        
        }
        JSONobject->EndArray();
       }        
       break;
  default:
 
       break;
 }

 JSONobject->StartObject();
 JSONobject->PutName("ip");
 JSONobject->StartObject(); 
 JSONobject->PutProperty("address", (char*) objPortArg.fRemoteIPaddressofPort().c_str() , 2);
 JSONobject->StartObject();
 JSONobject->PutProperty("port", (char*) int2str(objPortArg.fRemotePortNumber()).c_str() , 3);
 JSONobject->EndObject();
 JSONobject->EndObject();
 JSONobject->EndObject();
 return;
}

void AddCADInfotoJSONobject(JSON *JSONobject, Call_Data objCALLdata, ALI_Data objALIdata, Port_Data objPortArg) {

 extern string 				EncodeBase64(string strInput);

 //CAD object
 JSONobject->StartObject();
 JSONobject->PutName("cad");

 JSONobject->StartObject();
 JSONobject->PutName("record");
 JSONobject->StartObject();
 JSONobject->PutProperty("data", (char*) objALIdata.strCADmessageSent.c_str() , 2);
 JSONobject->PutProperty("position", (char*) int2str(objCALLdata.intPosn).c_str() , 3);
 JSONobject->PutProperty("alias", (char*) int2str(objPortArg.fCadPortAlias(objCALLdata.intPosn)).c_str() , 3); 
 JSONobject->EndObject();

 return;
}


void AddConferenceDatatoJSONobject(JSON *JSONobject, Call_Data objCALLdata) {

 size_t sz = objCALLdata.ConfData.vectConferenceChannelData.size();
 bool   boolDuplicateUUID;

 if (sz == 0) {return;} // check ring dial object ?????

 JSONobject->StartObject();
 JSONobject->PutName("participants");
 JSONobject->StartArray();
 for (unsigned int i = 0; i < sz; i++) {
   
   // Put check to not show temp objects (before connect) for blind and attended transfers ...... 
   // they will have duplicate Channel UUID's the later in the list are ignored.
   boolDuplicateUUID = false;
   for (unsigned int j = 0; j < sz; j++) {
    if (j == i) {continue;}
    if (objCALLdata.ConfData.vectConferenceChannelData[i].strChannelID == objCALLdata.ConfData.vectConferenceChannelData[j].strChannelID) {
    if (i > j ) {boolDuplicateUUID = true;}
    }
    if (boolDuplicateUUID) {break;}
   }
   if (objCALLdata.ConfData.vectConferenceChannelData[i].strChannelID.empty()) {continue;}
   if (boolDuplicateUUID) {continue;}
   
   JSONobject->StartObject();
   JSONobject->PutProperty("position", (char*) int2str(objCALLdata.ConfData.vectConferenceChannelData[i].iPositionNumber).c_str() , 2);
   JSONobject->PutProperty("Conf_Dsply", (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strConfDisplay.c_str() , 2);
   JSONobject->PutProperty("IP_Address", (char*) objCALLdata.ConfData.vectConferenceChannelData[i].IPaddress.stringAddress.c_str() , 2);
   JSONobject->PutProperty("Channel_Name", (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strChannelName.c_str() , 2);
   JSONobject->PutProperty("Channel_ID", (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strChannelID.c_str() , 2);
   JSONobject->PutProperty("Line_Number", (char*) int2str(objCALLdata.ConfData.vectConferenceChannelData[i].iLineNumber).c_str() , 2);
   JSONobject->PutProperty("GUI_Line_View", (char*) int2str(objCALLdata.ConfData.vectConferenceChannelData[i].iGUIlineView).c_str() , 2);
// ringing line numbers?   
   if (objCALLdata.ConfData.vectConferenceChannelData[i].boolOnHold) {
    JSONobject->PutProperty("on_hold", "true" , 4); 
   } else {
    JSONobject->PutProperty("on_hold", "false" , 4); 
   } 
   if (objCALLdata.ConfData.vectConferenceChannelData[i].boolConnected) {
    JSONobject->PutProperty("connected", "true" , 4); 
   } else {
    JSONobject->PutProperty("connected", "false" , 4); 
   }    
   JSONobject->PutProperty("Cust_Name", (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strCustName.c_str(), 2);
   JSONobject->PutProperty("Call_Back", (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strCallerID.c_str(), 2);
   if (objCALLdata.ConfData.vectConferenceChannelData[i].boolIsOutbound) {
    JSONobject->PutProperty("Is_Outbound", "true" , 4); 
   } else {
    JSONobject->PutProperty("Is_Outbound", "false" , 4); 
   } 
   JSONobject->PutProperty("Trunk_Number", (char*) int2str(objCALLdata.ConfData.vectConferenceChannelData[i].iTrunkNumber).c_str(), 2);      
   JSONobject->PutProperty("Trunk_Type",   (char*) TrunkType( (trunk_type) objCALLdata.ConfData.vectConferenceChannelData[i].intTrunkType).c_str(), 2);    
   if (objCALLdata.ConfData.vectConferenceChannelData[i].boolTDDMode) {
    JSONobject->PutProperty("TDD_Mode_ON", "true" , 4); 
   } else {
    JSONobject->PutProperty("TDD_Mode_ON", "false" , 4); 
   } 
   JSONobject->PutProperty("Presence_ID",   (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strPresenceID.c_str(), 2); 
   JSONobject->PutProperty("Hangup_Cause",   (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strHangupCause.c_str(), 2);       
   JSONobject->PutProperty("Endpoint_Disposition",   (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strEndpointDisposition.c_str(), 2);    
   JSONobject->PutProperty("Hangup_Disposition",   (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strHangupDisposition.c_str(), 2);    
   JSONobject->PutProperty("Transfer_Disposition",   (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strTransferDisposition.c_str(), 2);
   if (objCALLdata.ConfData.vectConferenceChannelData[i].boolParked) {
    JSONobject->PutProperty("Is_Parked", "true" , 4);
    JSONobject->PutProperty("Valet_Extension",   (char*) objCALLdata.ConfData.vectConferenceChannelData[i].strValetExtension.c_str(), 2);     
   } else {
    JSONobject->PutProperty("Is_Parked", "false" , 4); 
   } 
  JSONobject->EndObject();
 }
 JSONobject->EndObject();
 JSONobject->EndArray();

 return;
}


string LISfunction(lis_functions eLISfunction) {
// {NO_LIS_FUNCTION, URL_RECIEVED, DEREFERENCE_URL, I3_DATA_RECEIVED, LEGACY_ALI_DATA_RECIEVED, POST_LIS_LOCATION_REQUEST};
 switch (eLISfunction) {
  case NO_LIS_FUNCTION: 			        {return "UNCLASSIFIED";}
  case URL_RECIEVED:      				{return "URL_RECIEVED";}
  case DEREFERENCE_URL:                        		{return "DEREFERENCE_URL";}
  case LIS_HTTP_TIMEOUT:				{return "LIS_HTTP_TIMEOUT";}
  case I3_DATA_RECEIVED:                                {return "I3_DATA_RECEIVED";}
  case LEGACY_ALI_DATA_RECIEVED:			{return "LEGACY_ALI_DATA_RECIEVED";}
  case POST_LIS_LOCATION_REQUEST:			{return "POST_LIS_LOCATION_REQUEST";}
  default: 
   SendCodingError("globalfunctions.cpp - coding error LISfunction() unhandled value in case switch");
 }
 return "";
}



string ALIfunction(ali_functions eALIfunction) {
//fred
// NO_ALI_FUNCTION_DEFINED, INITIAL_ALI_BID, MANUAL_ALI_BID
 switch (eALIfunction) {
  case NO_ALI_FUNCTION_DEFINED: 			{return "UNCLASSIFIED";}
  case ALI_BID_REQUEST:      				{return "ALI_BID_REQUEST";}
  case ALI_RECEIVED:                                    {return "ALI_RECEIVED";}
  case ALI_TIMEOUT:					{return "ALI_TIMEOUT";}
  case ALI_UNABLE_TO_BID:				{return "ALI_UNABLE_TO_BID";}
  case SEND_TO_CAD:					{return "SEND_TO_CAD";}
  case CAD_SEND_FAIL:					{return "CAD_SEND_FAIL";}
  default: 
   SendCodingError("globalfunctions.cpp - coding error ALIfunction() unhandled value in case switch");
 }
 return "";
}

string ANIfunction(ani_functions eANIfunction) {

 switch (eANIfunction) {
  case UNCLASSIFIED: 			{return "UNCLASSIFIED";}
  case CONNECT:      			{return "CONNECT";}
  case DISCONNECT:   			{return "DISCONNECT";}
  case TRANSFER:     			{return "TRANSFER";}
  case CONFERENCE:                      {return "CONFERENCE";}
  case HOLD_ON:      			{return "HOLD_ON";}
  case HOLD_OFF:     			{return "HOLD_OFF";}
  case ABANDONED:    			{return "ABANDONED";}
  case ABANDONED_BUSY: 			{return "ABANDONED_BUSY";}
  case ALARM:				{return "ALARM";}
  case RINGING:				{return "RINGING";}
  case ALI_REQUEST:			{return "ALI_REQUEST";}
  case PLAY_SORRY_MSG:			{return "PLAY_SORRY_MSG";}
  case TIME_DATE_CHANGE:		{return "TIME_DATE_CHANGE";}
  case TRANSFER_NUMBER_PROGRAM:		{return "TRANSFER_NUMBER_PROGRAM";}
  case HEARTBEAT:			{return "HEARTBEAT";}
  case ALI_REPEAT_SCROLL_BACK:		{return "ALI_REPEAT_SCROLL_BACK";}
  case SILENT_ENTRY_LOG_OFF:		{return "SILENT_ENTRY_LOG_OFF";}
  case CANCEL_TRANSFER:			{return "CANCEL_TRANSFER";}
  case TRANSFER_CONNECT:		{return "TRANSFER_CONNECT";}
  case TRANSFER_CONNECT_BLIND:		{return "TRANSFER_CONNECT_BLIND";}
  case TRANSFER_CONNECT_INTERNAL_NG911:	{return "TRANSFER_CONNECT_INTERNAL_NG911";}
  case TDD_DISPATCHER_SAYS:		{return "TDD_DISPATCHER_SAYS";}
  case TDD_CHAR_RECEIVED:		{return "TDD_CHAR_RECEIVED";}
  case TDD_CALLER_SAYS:			{return "TDD_CALLER_SAYS";}
  case TDD_MUTE:			{return "TDD_MUTE";}
  case TDD_MODE_ON:			{return "TDD_MODE_ON";}
  case TDD_MODE_OFF:			{return "TDD_MODE_OFF";}
  case DIAL_DEST:			{return "DIAL_DEST";}
  case BARGE_ON_POSITION:		{return "BARGE_ON_POSITION";}
  case BARGE_ON_CONFERENCE:		{return "BARGE_ON_CONFERENCE";}
  case BARGE_ON_POSITION_LINE_ROLL:	{return "BARGE_ON_POSITION_LINE_ROLL";}
  case BARGE_ON_CONFERENCE_LINE_ROLL:	{return "BARGE_ON_CONFERENCE_LINE_ROLL";}
  case NON_TEN_DIGIT_NUMBER_CONNECT:	{return "NON_TEN_DIGIT_NUMBER_CONNECT";}
  case PANI_CONNECT:			{return "PANI_CONNECT";}
  case AGI_ERROR:			{return "AGI_ERROR";}
  case AGI_TIMEOUT:			{return "AGI_TIMEOUT";}
  case AGI_FAIL:			{return "AGI_FAIL";}
  case LOCATION_URI:			{return "LOCATION_URI";}
  case LOCATION_DATA:			{return "LOCATION_DATA";}
  case LEGACY_ALI_DATA:			{return "LEGACY_ALI_DATA";}
  case SMS_MSG_RECEIVED:		{return "SMS_MSG_RECEIVED";}
  case SMS_MSG_SENT:			{return "SMS_MSG_SENT";}
  case SMS_SEND_TEXT:			{return "SMS_SEND_TEXT";}
  case TAKE_CONTROL:			{return "TAKE_CONTROL";}
  case TAKE_CONTROL_SOFTWARE:           {return "TAKE_CONTROL_SOFTWARE";}
  case UN_CONFERENCE:			{return "UN_CONFERENCE";}
  case DELETE_RECORD:			{return "DELETE_RECORD";}
  case UPDATE_CALLBACK:			{return "UPDATE_CALLBACK";}
  case FORCE_DISCONNECT:		{return "FORCE_DISCONNECT";}
  case BLIND_TRANSFER:			{return "BLIND_TRANSFER";}
  case ATTENDED_TRANSFER:		{return "ATTENDED_TRANSFER";}
  case RING_BACK:			{return "RING_BACK";}
  case RING_BACK_CONNECT:		{return "RING_BACK_CONNECT";}
  case TRANSFER_FAIL:			{return "TRANSFER_FAIL";}
  case ATT_TRANSFER_FAIL:		{return "ATT_TRANSFER_FAIL";}
  case ATT_TRANSFER_REJ:		{return "ATT_TRANSFER_REJ";}
  case CONFERENCE_JOIN:			{return "CONFERENCE_JOIN";}
  case CONFERENCE_LEAVE:		{return "CONFERENCE_LEAVE";}
  case CONFERENCE_ATT_XFER:		{return "CONFERENCE_ATT_XFER";}
  case CONFERENCE_JOIN_LIST:		{return "CONFERENCE_JOIN_LIST";}
  case CONFERENCE_BUILD_LIST:		{return "CONFERENCE_BUILD_LIST";}
  case CONFERENCE_JOIN_SLA:		{return "CONFERENCE_JOIN_SLA";}
  case CONFERENCE_ADD_MEMBER:		{return "CONFERENCE_ADD_MEMBER";}
  case SEND_FLASH:			{return "SEND_FLASH";}
  case KILL_CHANNEL:			{return "KILL_CHANNEL";}
  case CONFERENCE_ATT_TRANSFER_FAIL:	{return "CONFERENCE_ATT_TRANSFER_FAIL";}
  case EAVESDROP:			{return "EAVESDROP";}
  case UPDATE_ANI_DATA:			{return "UPDATE_ANI_DATA";}
  case RETRY_CONF_BLIND_TRANSFER:	{return "RETRY_CONF_BLIND_TRANSFER";}
  case CLI_TRANSFER:			{return "CLI_TRANSFER";}
  case VALET_JOIN:			{return "VALET_JOIN";}
  case VALET_PARK:                      {return "VALET_PARK";}
  case MSRP_RINGING:			{return "MSRP_RINGING";}
  case MSRP_ANSWER:			{return "MSRP_ANSWER";}
  case MSRP_CONNECT:			{return "MSRP_CONNECT";}
  case MSRP_HANGUP:			{return "MSRP_HANGUP";}
  case IRR_FILE_DATA:			{return "IRR_FILE_DATA";}
  case RINGING_POSITION_TO_POSITION:	{return "RINGING_POSITION_TO_POSITION";}
  case MSRP_ON_HOLD:			{return "MSRP_ON_HOLD";}
  case MSRP_OFF_HOLD:			{return "MSRP_OFF_HOLD";}
  case MSRP_CONFERENCE_LEAVE:		{return "MSRP_CONFERENCE_LEAVE";}
  case MSRP_CONFERENCE_JOIN:		{return "MSRP_CONFERENCE_JOIN";}
  case SYNC_TIME:			{return "SYNC_TIME";}
  case MERGE_CALL:			{return "MERGE_CALL";}
  case TEXT_LOG_CONVERSATION:           {return "TEXT_LOG_CONVERSATION";}
  case RFAI_DROP_LEG:                   {return "RFAI_DROP_LEG";}
  case RFAI_BRIDGE:                     {return "RFAI_BRIDGE";}
  default: 
   SendCodingError("globalfunctions.cpp - coding error ANIfunction() unhandled value in case switch");
 }
return "";
}

string WRKfunction(wrk_functions eJSONEventWRKcode) {

 switch (eJSONEventWRKcode) {
  case BAD_KEY:                      	{return "UNCLASSIFIED";}
  case WORKSTATION_LOG_IN:		{return "WORKSTATION_LOG_IN";}
  case WORKSTATION_LOG_OUT:		{return "WORKSTATION_LOG_OUT";}
  case WRK_TAKE_CONTROL:		{return "WORKSTATION_CLEAR_ABANDONED";}
  case WRK_TAKE_CONTROL_SOFTWARE:       {return "SOFTWARE_CLEAR_ABANDONED";}  
  default:
   SendCodingError("globalfunctions.cpp - coding error WRKfunction() unhandled value in case switch");
 }

return "";
}

string TransferMethod(transfer_method eTransferMethod) {

 switch (eTransferMethod) {
  case mBLANK:                      {return "BLANK";}
  case mGUI_TRANSFER:               {return "GUI_TRANSFER";}
  case mBLIND_TRANSFER:             {return "BLIND_TRANSFER";}
  case mATTENDED_TRANSFER:          {return "ATTENDED_TRANSFER";}
  case mPOORMAN_BLIND_TRANSFER:     {return "UN_ATTENDED_BLIND_TRANSFER";}
  case mRING_BACK:                  {return "RING_BACK";}
  case mTANDEM:                     {return "TANDEM_TRANSFER";}
  case mINDGITAL:                   {return "INDGITAL_TRANSFER";}
  case mNG911_TRANSFER:             {return "NG911_TRANSFER";}
  case mFLASHOOK:                   {return "FLASHOOK_TRANSFER";}
  case mINTRADO:		    {return "RFAI_TRANSFER";}
  default:
   SendCodingError("globalfunctions.cpp - coding error TransferMethod() unhandled value in case switch");
 }

return "";
}

string TextType(text_type eTextType) {
//text_type                                 {NO_TEXT_TYPE, TDD_MESSAGE, MSRP_MESSAGE};
 switch (eTextType) {
  case NO_TEXT_TYPE:                    {return "NO_TEXT_TYPE";}
  case TDD_MESSAGE:               	{return "TDD_MESSAGE";}
  case MSRP_MESSAGE:             	{return "MSRP_MESSAGE";}
  default:
   SendCodingError("globalfunctions.cpp - coding error TextType() unhandled value in case switch");
 }

 return "";
}

string ALIformat(ali_format  eRequestKeyFormat) {

//enum          ali_format                                {NO_FORMAT, FOURTEEN_DIGIT, SIXTEEN_DIGIT, E2_PROTOCOL};
 switch (eRequestKeyFormat) {
  case NO_FORMAT:                      	{return "NO_FORMAT";}
  case FOURTEEN_DIGIT:               	{return "FOURTEEN_DIGIT";}
  case SIXTEEN_DIGIT:             	{return "SIXTEEN_DIGIT";}
  case E2_PROTOCOL:          		{return "E2_PROTOCOL";}
  default:
   SendCodingError("globalfunctions.cpp - coding error ALIformat() unhandled value in case switch");
 }

 return "";
}

bool ISmain( string strInput) {
string strCompare;

strCompare = RemoveLeadingSpaces(strInput);
strCompare = RemoveTrailingSpaces(strCompare);

if      (strCompare == "main") {return true;}
else if (strCompare == "Main") {return true;}
else if (strCompare == "MAIN") {return true;}
else                           {return false;}
}



int AddWRKEventtoJSONobject(JSON *JSONobject, Call_Data objCalldata, Text_Data objTextData) {
/*
 int	                intPosition;
 IP_Address             RemoteIPAddress;
 unsigned int           intRemotePortNumber;
 struct timespec        timespecTimeLastHeartbeat;
 bool                   boolActive;
 string                 strGUIVersion;
 string                 strGUIxmlVersion;
 string                 strWindowsUser;
 string                 strWindowsOS;
 bool                   boolContactRelayInstalled;
 int                    intRCCrelayPin;
 IP_Address             RCCremoteIPaddress;
 unsigned int           intRCCremotePortNumber;
 unsigned int           intRCClocalPortNumber;
 volatile bool          boolRCCconnected;
 volatile bool          boolRCCSendDelayedSignal;
 int                    intTCP_ConnectionID;
*/
 extern WorkStation                     WorkStationTable[NUM_WRK_STATIONS_MAX+1];
 string					strEvent;
 WorkStation                            objWorkstation;
 string					strMessage;
 int                                    i;

 extern void   AddCallEventtoJSONobject(JSON *JSONobject, Call_Data objCALLdata, Text_Data objTextData);
 extern void   AddANiInfotoJSONobject(JSON *JSONobject, Call_Data objCALLdata); 

 if ((objCalldata.intPosn < 0)||(objCalldata.intPosn > NUM_WRK_STATIONS_MAX)) {
  strMessage = "globalfunctions.cpp - coding error AddWRKEventtoJSONobject() Position out of range -> ";
  strMessage += int2str(objCalldata.intPosn);
  SendCodingError(strMessage);
  JSONobject->EndObject();
  return 0;
 }

 objWorkstation = WorkStationTable[objCalldata.intPosn];
 i = objCalldata.intPosn;


 if ((objCalldata.eJSONEventWRKcode == WRK_TAKE_CONTROL)||(objCalldata.eJSONEventWRKcode == WRK_TAKE_CONTROL_SOFTWARE)) {

   switch (objCalldata.eJSONEventWRKcode) {
    case WRK_TAKE_CONTROL:
       objCalldata.eJSONEventCode = TAKE_CONTROL; break;
    case WRK_TAKE_CONTROL_SOFTWARE:
       objCalldata.eJSONEventCode = TAKE_CONTROL_SOFTWARE; break;
    default: 
     //not possible ...
     break;
  } 
  AddCallEventtoJSONobject(JSONobject, objCalldata, objTextData);     
  AddANiInfotoJSONobject(JSONobject, objCalldata);
  return -1;
 }

 JSONobject->StartObject();
 JSONobject->PutName("Event");
 JSONobject->StartObject();

 strEvent = WRKfunction(objCalldata.eJSONEventWRKcode);
 JSONobject->PutProperty("event_name", (char*)  strEvent.c_str(), 2);
 JSONobject->EndObject();

 switch (objCalldata.eJSONEventWRKcode) {
  case WORKSTATION_LOG_IN:  case WORKSTATION_LOG_OUT:
       JSONobject->StartObject();
       JSONobject->PutName("workstation");
       JSONobject->StartObject();
       JSONobject->PutProperty("position",(char*)  int2str(objCalldata.intPosn).c_str(), 3);
       JSONobject->PutProperty("ver",(char*)  objWorkstation.strGUIVersion.c_str(), 2);
       JSONobject->PutProperty("xml_ver",(char*)  objWorkstation.strGUIxmlVersion.c_str(), 2); 
       JSONobject->PutProperty("user",(char*)  objWorkstation.strWindowsUser.c_str(), 2);
       JSONobject->PutProperty("os",(char*)  objWorkstation.strWindowsOS.c_str(), 2);
       JSONobject->StartObject();
       JSONobject->PutName("ip");
       JSONobject->StartObject(); 
       JSONobject->PutProperty("address", (char*) objWorkstation.RemoteIPAddress.stringAddress.c_str() , 2);
       JSONobject->StartObject();
       JSONobject->PutProperty("port", (char*) int2str(objWorkstation.intRemotePortNumber).c_str() , 3);
       JSONobject->EndObject();             
       JSONobject->EndObject();      
       break;
  case WRK_TAKE_CONTROL:

       break;

  default:
       SendCodingError("globalfunctions.cpp - coding error AddWRKEventtoJSONobject() unhandled value in case switch");
       break;
 }



 
 
 cout << "********************************JSON WRK EVENT CODE *****************************->" << strEvent << endl;
 return 0;
}



void AddTextConversationToJSONobject(JSON *JSONobject, Call_Data objCallData, Text_Data objTextData) {

 string strPrefix;
 string strName;
 string strProperty;

 JSONobject->StartObject();
 switch (objTextData.TextType) {
  case TDD_MESSAGE: case NO_TEXT_TYPE:
   strPrefix = "TDD";
   break;
  case MSRP_MESSAGE:
   strPrefix = "MSRP";
   break;
 }
 
 strName     = strPrefix + "_Data";
 strProperty = strPrefix + "_Conversation";

 JSONobject->PutName( (char*) strName.c_str());
 JSONobject->StartObject();
 JSONobject->PutProperty((char*) strProperty.c_str(),(char*)  objTextData.strConversation.c_str(), 2);
 JSONobject->PutProperty("text_type",(char*)  TextType(objTextData.TextType).c_str(), 2);
// JSONobject->EndObject();
 return;
}

void AddTextDataToJSONobject(JSON *JSONobject, Call_Data objCallData, Text_Data objTextData) {
/*
 public:
 string           strSMSmessage;
 LocationURI      objLocationURI;
 string           strBidId;
 string           strFromUser;
 string           strFromHost;
 string           strToUser;
 string           strToHost;
 string           strConversation;
*/

 JSONobject->StartObject();
 JSONobject->PutName("MSRP_Data");
 switch (objCallData.eJSONEventCode) {
  case SMS_MSG_RECEIVED:
   JSONobject->StartObject();
   JSONobject->PutProperty("caller",(char*)  int2str(objCallData.intCallbackNumber).c_str(), 2); 
   JSONobject->PutProperty("text_type",(char*)  TextType(objTextData.TextType).c_str(), 2);
//   JSONobject->PutProperty("bid_id",(char*)  objTextData.SMSdata.strBidId.c_str(), 2); 
 //  JSONobject->PutProperty("loc_uri",(char*)  objTextData.SMSdata.objLocationURI.strURI.c_str(), 2);
//   JSONobject->PutProperty("loc_exp",(char*)  objTextData.SMSdata.objLocationURI.strExpires.c_str(), 2);   
   JSONobject->PutProperty("MSRP_Text",(char*)  objTextData.SMSdata.strSMSmessage.c_str(), 2); 
   JSONobject->PutProperty("from_user",(char*)  objTextData.SMSdata.strFromUser.c_str(), 2);
   JSONobject->PutProperty("to_user",(char*)  objTextData.SMSdata.strToUser.c_str(), 2);
   JSONobject->PutProperty("to_host",(char*)  objTextData.SMSdata.strToHost.c_str(), 2); 
   break;
  case SMS_SEND_TEXT:
   JSONobject->StartObject();
   JSONobject->PutProperty("dispatcher",(char*)  int2str(objCallData.intPosn).c_str(), 2);
   JSONobject->PutProperty("text_type",(char*)  TextType(objTextData.TextType).c_str(), 2);
   JSONobject->PutProperty("MSRP_Text",(char*)  objTextData.SMSdata.strSMSmessage.c_str(), 2); 
   JSONobject->PutProperty("from_user",(char*)  objTextData.SMSdata.strFromUser.c_str(), 2);
   JSONobject->PutProperty("to_user",(char*)  objTextData.SMSdata.strToUser.c_str(), 2);
   JSONobject->PutProperty("to_host",(char*)  objTextData.SMSdata.strToHost.c_str(), 2); 
   break;
  default:
   SendCodingError("globalfunctions.cpp - AddTextDataToJSONobject() unhandled value in case switch.");
  break;
 }
 JSONobject->EndObject();
return;
}

void AddTDDdataToJSONobject(JSON *JSONobject, Call_Data objCallData, Text_Data objTextData) {
 JSONobject->StartObject();
 JSONobject->PutName("TDD_Data");
/* TDD_DATA
 public:
 bool                    boolTDDWindowActive;
 bool	                 boolTDDSendButton;
 bool                    boolTDDmute;
 bool                    boolTDDthresholdMet;
 string                  strTDDstring;
 string                  strTDDCallerBuffer;
 string                  strTDDcharacter;
 string                  strTDDconversation;
 struct timespec         timespecTimeTDDcharRecv;
 SMS_Data                SMSdata;
*/
/* Text_Data
 text_type               TextType;
 bool                    boolTextWindowActive;                   
 bool                    boolTextSendButton;
 string                  strConversation;
 LocationURI             objLocationURI;
*/
 switch (objCallData.eJSONEventCode) {
  case TDD_DISPATCHER_SAYS:
   JSONobject->StartObject();
   JSONobject->PutProperty("dispatcher",(char*)  int2str(objCallData.intPosn).c_str(), 2);
   JSONobject->PutProperty("text_type",(char*)  TextType(objTextData.TextType).c_str(), 2);
   JSONobject->PutProperty("TDD_Sentence",(char*)  objTextData.TDDdata.strTDDstring.c_str(), 2);
   if (objTextData.TDDdata.boolTDDmute) {JSONobject->PutProperty("TDD_Mute","true",  4);}
   else                                 {JSONobject->PutProperty("TDD_Mute","false", 4);}
   break;

  case TDD_CHAR_RECEIVED:
   JSONobject->StartObject();
   JSONobject->PutProperty("caller",(char*)  int2str(objCallData.intCallbackNumber).c_str(), 2);  
   JSONobject->PutProperty("text_type",(char*)  TextType(objTextData.TextType).c_str(), 2);
   JSONobject->PutProperty("char_rx",(char*)  objTextData.TDDdata.strTDDcharacter.c_str(), 2);
   if (objTextData.TDDdata.boolTDDmute) {JSONobject->PutProperty("TDD_Mute","true",  4);}
   else                                 {JSONobject->PutProperty("TDD_Mute","false", 4);}
   break;

  case TDD_CALLER_SAYS:
   JSONobject->StartObject();
   JSONobject->PutProperty("caller",(char*)  int2str(objCallData.intCallbackNumber).c_str(), 2);  
   JSONobject->PutProperty("text_type",(char*)  TextType(objTextData.TextType).c_str(), 2);
   JSONobject->PutProperty("TDD_Sentence",(char*)  objTextData.TDDdata.strTDDstring.c_str(), 2);
   if (objTextData.TDDdata.boolTDDmute) {JSONobject->PutProperty("TDD_Mute","true",  4);}
   else                                 {JSONobject->PutProperty("TDD_Mute","false", 4);}
   break;

  default:
   SendCodingError("globalfunctions.cpp - AddTDDdataToJSONobject() unhandled value in case switch.");
   break;
 } 
// JSONobject->EndObject();
return;
}

void AddTransferDatatoJSONobject(JSON *JSONobject, Transfer_Data objTransferdata) {
 JSONobject->StartObject();
 JSONobject->PutName("Transfer_Data");
 JSONobject->StartObject(); 
/*  strTransferFromPosition      = b.strTransferFromPosition;
  strTransferFromChannel       = b.strTransferFromChannel;
  strTransferFromUniqueid      = b.strTransferFromUniqueid;
  strTransferMatchingChannelId = b.strTransferMatchingChannelId;
  strTransferTargetUUID        = b.strTransferTargetUUID;
  strTransfertoNumber          = b.strTransfertoNumber;
  strNG911PSAP_TransferNumber  = b.strNG911PSAP_TransferNumber;
  strConfDisplay               = b.strConfDisplay;
  strRDNIS                     = b.strRDNIS;
  boolGUIcancel                = b.boolGUIcancel;
  strHangupCause               = b.strHangupCause;
  eTransferMethod              = b.eTransferMethod;
  strTransferName              = b.strTransferName;
  boolIsRingBack               = b.boolIsRingBack;
  IPaddress                    = b.IPaddress;
  strValetExtension            = b.strValetExtension;
*/

 JSONobject->PutProperty("transfer_from_position",(char*)  objTransferdata.strTransferFromPosition.c_str(), 2);
 JSONobject->PutProperty("transfer_from_channel",(char*)  objTransferdata.strTransferFromChannel.c_str(), 2);
 JSONobject->PutProperty("transfer_from_uuid", (char*)  objTransferdata.strTransferFromUniqueid.c_str(), 2);
 JSONobject->PutProperty("transfer_from_MatchingChannelId",(char*)  objTransferdata.strTransferMatchingChannelId.c_str(), 2);
 JSONobject->PutProperty("transfer_target_uuid", (char*)  objTransferdata.strTransferTargetUUID.c_str(), 2);
 JSONobject->PutProperty("transfer_to_number",(char*)  objTransferdata.strTransfertoNumber.c_str(), 2);
 JSONobject->PutProperty("transfer_to_ng911psap_number",(char*)  objTransferdata.strNG911PSAP_TransferNumber.c_str(), 2);
 JSONobject->PutProperty("transfer_conference_display",(char*)  objTransferdata.strConfDisplay.c_str(), 2);
 JSONobject->PutProperty("transfer_rdnis",(char*)  objTransferdata.strRDNIS.c_str(), 2);
 JSONobject->PutProperty("transfer_hangup_cause",(char*)  objTransferdata.strHangupCause.c_str(), 2);
 JSONobject->PutProperty("transfer_name",(char*)  objTransferdata.strTransferName.c_str(), 2);
 JSONobject->PutProperty("transfer_valet_ext",(char*)  objTransferdata.strValetExtension.c_str(), 2);
 JSONobject->EndObject();
 return;
}

void AddCallDatatoJSONobject(JSON *JSONobject, Channel_Data objCalldata) {
 JSONobject->StartObject();
 JSONobject->PutName("Transfer_Data");
 JSONobject->StartObject(); 
}

void AddCallEventtoJSONobject(JSON *JSONobject, Call_Data objCALLdata, Text_Data objTextData) {


 string strEvent        = "";
 string strTransferUUID = "";
 string strTransferNumber = "";

 JSONobject->StartObject();
 JSONobject->PutName("Event");
 JSONobject->StartObject();

 switch (objCALLdata.eJSONEventCode) {
  case TRANSFER:
       strEvent = TransferMethod(objCALLdata.eJSONEventTransferMethod);
       break;      
  default:
   strEvent = ANIfunction(objCALLdata.eJSONEventCode);
 
 }
 
 JSONobject->PutProperty("event_name", (char*)  strEvent.c_str(), 2);
// JSONobject->PutProperty("event_trunk",(char*)  objCALLdata.stringTrunk.c_str(), 2);
 JSONobject->PutProperty("event_position",(char*)  objCALLdata.stringPosn.c_str(), 2);

 switch (objCALLdata.eJSONEventCode) {
  case MERGE_CALL:
       JSONobject->PutProperty("event_merge_with_call_uuid",(char*)  objCALLdata.strJSONEventMergedCallID.c_str(), 2);
       JSONobject->EndObject();
       break;

  case TRANSFER: case BLIND_TRANSFER: case ATTENDED_TRANSFER: case CANCEL_TRANSFER: case TRANSFER_CONNECT_BLIND: case TRANSFER_CONNECT_INTERNAL_NG911: case TRANSFER_FAIL: case ATT_TRANSFER_FAIL:
  case TRANSFER_CONNECT: case ATT_TRANSFER_REJ: case RETRY_CONF_BLIND_TRANSFER: case CONFERENCE: case RFAI_BRIDGE: case RFAI_DROP_LEG:
       JSONobject->EndObject();
       AddTransferDatatoJSONobject(JSONobject,objCALLdata.TransferData);
       break; 

  case TDD_DISPATCHER_SAYS:   case TDD_CHAR_RECEIVED: case TDD_CALLER_SAYS:
       JSONobject->EndObject();
       AddTDDdataToJSONobject(JSONobject, objCALLdata, objTextData);
       break;

  case SMS_MSG_RECEIVED: case SMS_SEND_TEXT:
       JSONobject->EndObject();
       AddTextDataToJSONobject(JSONobject, objCALLdata, objTextData);
       break;

  case TEXT_LOG_CONVERSATION:
       JSONobject->EndObject();
       AddTextConversationToJSONobject(JSONobject, objCALLdata, objTextData);
       break;
  default:
     //SendCodingError("globalfunctions.cpp - in AddCallEventtoJSONobject() unhandled var in case switch ");
   JSONobject->EndObject();
   break;
 } 


 //JSONobject->EndObject();
 
 cout << "********************************JSON EVENT CODE *****************************->" << strEvent << endl;
 return;
}

void AddANiInfotoJSONobject(JSON *JSONobject, Call_Data objCALLdata) {

 JSONobject->StartObject();
 JSONobject->PutName("Call_Data");
 AddConferenceDatatoJSONobject(JSONobject, objCALLdata);
 //JSONobject->EndObject();



 return;
}

void AddNgALIrecordDataToJSONobject(JSON *JSONobject, ALI_Data objALIdata) {

 extern ALISteering                  	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern string 				EncodeBase64(string strInput);

   if (objALIdata.stringAliText.length() > 0) {
    JSONobject->StartObject();
    JSONobject->PutName("location_data");

    if (objALIdata.I3Data.boolByValue) {
     JSONobject->StartObject();
     JSONobject->PutProperty("type", "by_value" , 2);
    }
    else {
     JSONobject->StartObject();
     JSONobject->PutProperty("type", "by_reference" , 2);
    }

    if (objALIdata.I3Data.strPidfXMLData.length() > 0) {
     JSONobject->StartObject();
     JSONobject->PutProperty("xml", (char*) objALIdata.I3Data.strPidfXMLData.c_str() , 2);
    }
    if (objALIdata.I3Data.strALI30WRecord.length() > 0) {
     JSONobject->StartObject();
     JSONobject->PutProperty("ali30w", (char*) objALIdata.I3Data.strALI30WRecord.c_str() , 2);
    }
    if (objALIdata.I3Data.strALI30XRecord.length() > 0) {
     JSONobject->StartObject();
     JSONobject->PutProperty("ali30x", (char*) objALIdata.I3Data.strALI30XRecord.c_str() , 2);
    }
    if (objALIdata.I3Data.strNGALIRecord.length() > 0) {
     JSONobject->StartObject();
     JSONobject->PutProperty("westtel_legacy", (char*) objALIdata.I3Data.strNGALIRecord.c_str() , 2);
    }    
    if (objALIdata.I3Data.objLocationURI.strURI.length() > 0) {
     JSONobject->StartObject();
     JSONobject->PutProperty("uri", (char*) objALIdata.I3Data.objLocationURI.strURI.c_str() , 2);
     if (objALIdata.I3Data.objLocationURI.strExpires.length() > 0) {
      JSONobject->StartObject();
      JSONobject->PutProperty("expires", (char*) objALIdata.I3Data.objLocationURI.strExpires.c_str() , 2);
     }
    }
    JSONobject->EndObject();
   }
return;
}

void AddALIrecordDataToJSONobject(JSON *JSONobject, ALI_Data objALIdata) {

 extern ALISteering                  	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern string 				EncodeBase64(string strInput);

   if (objALIdata.stringAliText.length() > 0) {
    JSONobject->StartObject();
    JSONobject->PutName("legacy_record");
    JSONobject->StartObject();

    JSONobject->PutProperty("data", (char*) objALIdata.stringAliText.c_str() , 2);
    JSONobject->StartObject();
    if (ALI_Steering[objALIdata.intLastDatabaseBid ].boolTreatLinefeedasCR) {
     JSONobject->PutProperty("linefeed_behavior",  "carriagereturn" , 2); 
    }
    else {
     JSONobject->PutProperty("linefeed_behavior",  "linefeed" , 2);
    }
 
    JSONobject->StartObject();
    //rebid manual autorebid initial
    JSONobject->PutProperty("state", (char *) objALIdata.fALIStateString().c_str()    , 2);
    JSONobject->EndObject();
   }
return;
}

void AddALIrequestDataToJSONobject(JSON *JSONobject, ALI_Data objALIdata) {
 
 size_t					sz;
 extern ALISteering                  	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern string 				EncodeBase64(string strInput);

   JSONobject->StartObject();
   JSONobject->PutName("request");
   JSONobject->StartObject();
   JSONobject->PutProperty("type", (char *) objALIdata.fALIbidType().c_str()    , 2);
   JSONobject->StartObject();
   JSONobject->PutProperty("requestkey", (char *) objALIdata.stringAliRequest.c_str()    , 2);
   JSONobject->StartObject();
   JSONobject->PutProperty("index", (char *) int2str(objALIdata.intALIBidIndex).c_str()    , 3);
   JSONobject->StartObject();
   JSONobject->PutProperty("bidtrunk", (char *) objALIdata.strALIRequestTrunk.c_str()    , 2);
   JSONobject->StartObject();
   JSONobject->PutProperty("db", (char *) int2str(objALIdata.intLastDatabaseBid).c_str()    , 3);
   JSONobject->StartObject();
   JSONobject->PutProperty("pp", (char *) int2str(objALIdata.intALIPortPairUsedtoTX).c_str()    , 3);
   JSONobject->StartObject();
   JSONobject->PutProperty("transaction_id", (char *) int2str(objALIdata.intTransactionIDBid[1]).c_str()    , 3);
   JSONobject->StartObject();
   if (boolUSE_ALI_SERVICE_SERVER){
    JSONobject->PutProperty("ali_service", "true"    , 4);
   }
   else{
    JSONobject->PutProperty("ali_service", "false"    , 4);
   }
   JSONobject->EndObject();

   JSONobject->StartObject();
   JSONobject->PutName("Database");  
   JSONobject->StartObject();
   JSONobject->PutProperty("number", (char *) int2str(objALIdata.intLastDatabaseBid).c_str()    , 3);
   JSONobject->StartObject();
   JSONobject->PutProperty("request_key_format", (char *) ALIformat(ALI_Steering[objALIdata.intLastDatabaseBid].eRequestKeyFormat).c_str()    , 2);
   JSONobject->StartObject();
   JSONobject->PutProperty("trunk_type", (char *) TrunkType(ALI_Steering[objALIdata.intLastDatabaseBid].enumTrunkType).c_str()    , 2);
   JSONobject->StartObject();
   if (ALI_Steering[objALIdata.intLastDatabaseBid].boolDatabaseSendsSingleALI){
    JSONobject->PutProperty("sends_single_ali", "true"    , 4);
   }
   else{
    JSONobject->PutProperty("sends_single_ali", "false"    , 4);
   }
   JSONobject->StartObject();
   if (ALI_Steering[objALIdata.intLastDatabaseBid].boolTreatLinefeedasCR){
    JSONobject->PutProperty("treat_lf_as_cr", "true"    , 4);
   }
   else{
    JSONobject->PutProperty("treat_lf_as_cr", "false"    , 4);
   }  
   JSONobject->StartObject();
   JSONobject->PutProperty("class_of_service_row", (char *) int2str(ALI_Steering[objALIdata.intLastDatabaseBid].iClassOfServiceRow).c_str()    , 3);
  
   sz = ALI_Steering[objALIdata.intLastDatabaseBid].vCOSCallbackRow.size();

   if (sz > 0) { 
    JSONobject->StartObject();
    JSONobject->PutName("callback_rows");
    JSONobject->StartArray();
    for ( unsigned int j = 1; j <sz; j++) {
     JSONobject->StartObject();
     JSONobject->PutProperty((char*) ALI_Steering[objALIdata.intLastDatabaseBid].vCOSCallbackRow[j].strClassOfService.c_str() , 
                             (char*) int2str(ALI_Steering[objALIdata.intLastDatabaseBid].vCOSCallbackRow[j].iRow).c_str() , 3);
    }
    JSONobject->EndObject();
    JSONobject->EndArray();  
   }

   JSONobject->StartObject();
   JSONobject->PutName("port_pairs");
   JSONobject->StartArray();
   for ( int j = 1; j <= intNUM_ALI_PORT_PAIRS; j++) {
     if (ALI_Steering[objALIdata.intLastDatabaseBid].boolPortPairInDatabase[j]) {
     JSONobject->StartObject();
     JSONobject->PutProperty("portpair", (char*) int2str(j).c_str() , 3);
     }
   }
  JSONobject->EndObject();
  JSONobject->EndArray();  



  JSONobject->StartObject();
  JSONobject->PutName("range_pairs");
  JSONobject->StartArray();
  for ( int j = 1; j <= ALI_Steering[objALIdata.intLastDatabaseBid].intNumPANIRangePairs; j++) {
    JSONobject->StartObject();
    JSONobject->PutProperty("LowRange", (char*) int2str(ALI_Steering[objALIdata.intLastDatabaseBid].intPANILowRange[j]).c_str() , 3);
    JSONobject->PutProperty("HighRange", (char*) int2str(ALI_Steering[objALIdata.intLastDatabaseBid].intPANIHighRange[j]).c_str() , 3);   
    JSONobject->PutProperty("Bid_Timeout_sec", (char*) int2str(ALI_Steering[objALIdata.intLastDatabaseBid].intPANIRebidDelay[j]).c_str() , 3);   
  }
  JSONobject->EndObject();
  JSONobject->EndArray();
 return;
}

void AddNgAliEventtoJSONobject(JSON *JSONobject, ALI_Data objALIdata) {
 extern ALISteering                  	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern string 				EncodeBase64(string strInput);
 string					strEvent;

 JSONobject->StartObject();
 JSONobject->PutName("Event");
 JSONobject->StartObject();

 switch (objALIdata.I3Data.enumLISfunction) {
      
  default:
   strEvent = LISfunction(objALIdata.I3Data.enumLISfunction);
 
 }

 JSONobject->PutProperty("event_name", (char*)  strEvent.c_str(), 2);
 JSONobject->PutProperty("event_ali_type","NG911_ALI", 2);

// JSONobject->PutProperty("event_index",(char*)  int2str(objALIdata.intALIBidIndex).c_str(), 2);

 
 JSONobject->EndObject();
 
 cout << "********************************JSON ALI EVENT CODE *****************************->" << strEvent << endl;
 return;
}



void AddAliEventtoJSONobject(JSON *JSONobject, ALI_Data objALIdata) {
 extern ALISteering                  	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern string 				EncodeBase64(string strInput);
 string					strEvent;

 JSONobject->StartObject();
 JSONobject->PutName("Event");
 JSONobject->StartObject();

 switch (objALIdata.eJSONEventCode) {
      
  default:
   strEvent = ALIfunction(objALIdata.eJSONEventCode);
 
 }

 JSONobject->PutProperty("event_name", (char*)  strEvent.c_str(), 2);
 JSONobject->PutProperty("event_ali_type","LEGACY_ALI", 2);

// JSONobject->PutProperty("event_index",(char*)  int2str(objALIdata.intALIBidIndex).c_str(), 2);

 
 JSONobject->EndObject();
 
 cout << "********************************JSON ALI EVENT CODE *****************************->" << strEvent << endl;
 return;
}

void AddNgALIdereferenceToJSONobject(JSON *JSONobject, ALI_Data objALIdata) {   
   extern bool 				boolNG911_LIS_USE_POST_ONLY;
   extern bool 				boolMSRP_LIS_USE_POST_ONLY;
   extern Trunk_Type_Mapping		TRUNK_TYPE_MAP; 
   
   unsigned int intTrunk;

  // cout << objALIdata.strALIRequestTrunk << endl;
   intTrunk = char2int(objALIdata.strALIRequestTrunk.c_str());
   if (intTrunk <= intMAX_NUM_TRUNKS) {
    
    switch (objALIdata.I3Data.enumLISfunction) {

     case DEREFERENCE_URL: 
       JSONobject->StartObject();     
       JSONobject->PutName("httpgetderef");
       break;
     case POST_LIS_LOCATION_REQUEST:
       JSONobject->StartObject();     
       JSONobject->PutName("httppostderef");
       break;
     case LIS_HTTP_TIMEOUT:
       switch ( TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType) {
        case MSRP_TRUNK: 
         if (boolMSRP_LIS_USE_POST_ONLY) {
          JSONobject->StartObject();     
          JSONobject->PutName("httppostderef");
         }
         else {
          JSONobject->StartObject();     
          JSONobject->PutName("httpgetderef");
         }
        break;
       
        default:
         if (boolNG911_LIS_USE_POST_ONLY) {
          JSONobject->StartObject();     
          JSONobject->PutName("httppostderef");
         }
         else {
          JSONobject->StartObject();     
          JSONobject->PutName("httpgetderef");
         }
        break;
       }//end switch ( TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType) 
      break;

     default:
      SendCodingError( "globalfunctions.cpp - Coding Error in AddNgALIdereferenceToJSONobject() in switch(objLISData.ALIData.I3Data.enumLISfunction)");
      break;
    }// end switch (objLISData.ALIData.I3Data.enumLISfunction)    
   }
   else {
    JSONobject->StartObject();     
    JSONobject->PutName("httpderef");
   } //end if (intTrunk < TRUNK_TYPE_MAP.size) else
  
   JSONobject->StartObject();
   JSONobject->PutProperty("bid_id",(char*) objALIdata.I3Data.strBidId.c_str(), 2);
   JSONobject->StartObject();
   JSONobject->PutProperty("uri",(char*) objALIdata.I3Data.objLocationURI.strURI.c_str(), 2);
   JSONobject->EndObject();
 return;
}


void AddNgALiInfotoJSONobject(JSON *JSONobject, ALI_Data objALIdata) {

 switch (objALIdata.I3Data.enumLISfunction) {

   case I3_DATA_RECEIVED: 
    JSONobject->StartObject();
    JSONobject->PutName("NGali");
    AddNgALIrecordDataToJSONobject(JSONobject,objALIdata);
    break;

   case URL_RECIEVED:
    cout << "got url in AddNgALiInfotoJSONobject" << endl;
    break;

   case LEGACY_ALI_DATA_RECIEVED:

    break;

   case DEREFERENCE_URL: case LIS_HTTP_TIMEOUT: case POST_LIS_LOCATION_REQUEST:
    JSONobject->StartObject();
    JSONobject->PutName("NGali");
    AddNgALIdereferenceToJSONobject(JSONobject,objALIdata);    
    break;

   default:
    SendCodingError("globalfunctions.cpp - coding error AddNgALiInfotoJSONobject() unhandled value in case switch");
    break;
  }
 JSONobject->EndObject();
 return;
}
void AddALiInfotoJSONobject(JSON *JSONobject, ALI_Data objALIdata) {

 extern ALISteering                  	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
 extern string 				EncodeBase64(string strInput);
 
 switch (objALIdata.eJSONEventCode) {
  case ALI_BID_REQUEST: 
   JSONobject->StartObject();
   JSONobject->PutName("ali");
   AddALIrequestDataToJSONobject(JSONobject,objALIdata);

   break;

  case ALI_ERROR:
   cout << "got ALI Error ......" << endl;
   break;

  case ALI_RECEIVED:
   JSONobject->StartObject();
   JSONobject->PutName("ali");
   AddALIrecordDataToJSONobject(JSONobject,objALIdata); 
   AddALIrequestDataToJSONobject(JSONobject,objALIdata);
   break;

  case ALI_TIMEOUT:
   JSONobject->StartObject();
   JSONobject->PutName("ali");
   AddALIrequestDataToJSONobject(JSONobject,objALIdata);
   break;

  case ALI_UNABLE_TO_BID:
   return;

  default: 
   SendCodingError("globalfunctions.cpp - coding error AddALiInfotoJSONobject() unhandled value in case switch");
   break;
 } 
 JSONobject->EndObject();

 return;
}
#endif

string StripURNnenaCompanyID(string stringInput) {
size_t found;
string returnString;

 string strFind = "urn:nena:companyid:";

 found = stringInput.find(strFind);

 if (found != string::npos) {
  returnString = stringInput.substr( (found + strFind.length() -1), string::npos);
  return returnString;
 }

 return stringInput;
}

void BidNextGenURI(int iTableIndex, ExperientDataClass objData) {

 // MainWorktable should be semaphore protected before calling .....
 extern vector <ExperientDataClass>             vMainWorkTable; 
 extern Trunk_Type_Mapping			TRUNK_TYPE_MAP;
 extern bool					boolMSRP_LIS_USE_POST_ONLY;
 extern bool 					boolNG911_LIS_USE_POST_ONLY;
 extern string					strNG911_LIS_SERVER;
 extern string					strMSRP_LIS_SERVER;
 
 int 						intTrunk;
 ExperientDataClass				objLisData;

 void Queue_LIS_Event(ExperientDataClass objData);

 cout << "fn -> BIDNEXTGENURI" << endl;
 if (!boolNG_ALI)         {return;}
 if (boolLEGACY_ALI_ONLY) {return;}

 intTrunk = objData.CallData.intTrunk;

 if (iTableIndex < 0) {
  objLisData = objData;
 }
 else {
  objLisData = vMainWorkTable[iTableIndex];
 }


 switch ( TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType) {
                         
  case MSRP_TRUNK:
       if (boolMSRP_LIS_USE_POST_ONLY)                                   { 
        objLisData.ALIData.I3Data.enumLISfunction = POST_LIS_LOCATION_REQUEST; 
        if (objLisData.ALIData.I3Data.objLocationURI.strURI.empty()) {
         objLisData.ALIData.I3Data.objLocationURI.strURI = strMSRP_LIS_SERVER;
        }
       }
       else if (objData.ALIData.I3Data.objLocationURI.boolDataRecieved)  { 
         objLisData.ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; 
       } 
       else if (objData.objGeoLocation.boolDataRecieved)  {
        objLisData.ALIData.I3Data.objLocationURI = objData.objGeoLocation;
        objLisData.ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; 
       }       
       else    { 
        SendCodingError("globalfunctions.cpp BidNextGenURI() No MSRP URI to Bid!");
        break;
       }                                                          
       Queue_LIS_Event(objLisData); 
       break;

  case NG911_SIP: case REFER: case CAMA:
       //// USE POST /////
       if (boolNG911_LIS_USE_POST_ONLY)                                    { 
        objLisData.ALIData.I3Data.enumLISfunction = POST_LIS_LOCATION_REQUEST;
        if (objLisData.ALIData.I3Data.objLocationURI.strURI.empty()) {
         if (objLisData.objGeoLocation.strURI.empty()) {
          objLisData.ALIData.I3Data.objLocationURI.strURI = strNG911_LIS_SERVER;
         }
         else {
          objLisData.ALIData.I3Data.objLocationURI.strURI = objLisData.objGeoLocation.strURI;
          cout << "uri for post is -> " << objLisData.objGeoLocation.strURI << endl;
         }
        } 
       }
       // USE GET //
       else if (objData.ALIData.I3Data.objLocationURI.boolDataRecieved)    { 
        objLisData.ALIData.I3Data.enumLISfunction = DEREFERENCE_URL;
        objLisData.ALIData.I3Data.objLocationURI = objData.ALIData.I3Data.objLocationURI;
        cout << "uri for get is -> " << objData.ALIData.I3Data.objLocationURI.strURI << endl;       
       }
       else if (!objLisData.ALIData.I3Data.objGeoLocationHeaderURI.strURI.empty()) {
        objLisData.ALIData.I3Data.enumLISfunction = DEREFERENCE_URL; 
        objLisData.ALIData.I3Data.objLocationURI = objLisData.ALIData.I3Data.objGeoLocationHeaderURI;
         cout << "uri for get is -> " << objLisData.ALIData.I3Data.objLocationURI.strURI << endl;       
      }
      else if (!objLisData.objGeoLocation.strURI.empty()) {
        objLisData.ALIData.I3Data.enumLISfunction = DEREFERENCE_URL;
        objLisData.ALIData.I3Data.objLocationURI = objLisData.objGeoLocation;
        cout << "uri for get is -> " << objLisData.ALIData.I3Data.objLocationURI.strURI << endl;       
      }
      else                                                                { 
       SendCodingError("globalfunctions.cpp BidNextGenURI() No NG911 URI to Bid!");
       
       break;
      }                                                          
      Queue_LIS_Event(objLisData); 
      break;

  default:

      break;

 }

}

string truefalse(bool truefalse) {
    if (truefalse) {return "true";}
    else           {return "false";}
}
