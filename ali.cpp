/*****************************************************************************
* FILE: ali.cpp
*
* DESCRIPTION: This file contains all of the functions integral to the ALI Thread
*
* Features:
*
*    1. ALI Databases:                      Ability to connect to unlimited number of ALI databases;
*                                           Ability to connect unlimited number of ALI line pairs to each ALI database;
*                                           Incorporates load balancing when using multiple ALI line pairs with a databases
*    2. ALI Steering:                       Ability to select ALI database by callback/pANI number ranges
*    3. Request Formats:                    Supports both 8 and 10 digit ALI request formats
*    4. Requests on Ring:                   Makes ALI request on ringing of incoming call
*    5. Automatic Requests:                 Ability to enable/disable and set interval of automatic wireless ALI requests
*    6. Manual Requests:                    Ability to enable/disable manual ALI requests
*    7. Carrier based Request Timeouts:     Ability to set custom ALI request timeouts based on carrier.
*    8. Out of Area NPD Requests:           Ability to make 8 Digit ALI requests when receiving area codes outside of NPD table
*    9. Database Format Independent:        Not Tied to ALI database formats
*   10. Heartbeats:                         Sends heartbeat once every minute when no activity on connection and connection is up/available;
*                                           Sends heartbeat once every ten seconds when connection is down/unavailable
*   11. Email/Alarm Notification:           Informational messages on startup;
*                                           Warning messages for unusual activity;
*                                           Alarm messages for connection down/unavailable;
*                                           Reminder messages that alarm condition still exists
*   12. Denial of Service Protection:       Ignores packets from non-registered IP addresses;
*                                           Timed shutdown when approaching full buffer;
*                                           Ports are turned off when not expecting data
*   13. Communications:                     Utilizes UDP communications for connections;
*                                           If necessary converted to serial communication via Ethernet to serial converter
*   14. Self-Diagnostics:                   Built in diagnostics to help troubleshoot line errors, etc.
*   15. Threaded Implementation:            Provides efficient utilization and reduces resource monopolization
*   16. NENA Compliant:                     Complies with the “NENA Recommended Generic Standards for E9-1-1 PSAP Equipment”
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//external class based variables
extern Thread_Trap                              OrderlyShutDown;
extern Thread_Trap                              UpdateVars;
extern queue <MessageClass>	                queue_objMainMessageQ;
extern queue <MessageClass>	                queue_objALiMessageQ;
extern queue <ExperientDataClass> 	        queue_objAliData;
extern queue <ExperientDataClass>               queue_objMainData;
extern IP_Address                               HOST_IP;
extern IP_Address                               MULTICAST_GROUP;
extern vector <string>                          NPD[8];
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data				objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_ALI_PORT;
extern Initialize_Global_Variables              INIT_VARS;

//forward function Declarations
//void    ALI_Messaging_Monitor_Event(union sigval union_sigvalArg);
void    *Ali_Port_A_Listen_Thread(void *voidArg);
void    *Ali_Port_B_Listen_Thread(void *voidArg);
void    *Ali_Service_Port_Listen_Thread(void *voidArg);
bool    Check_Double_NAK(int intPortpair);
void    ALI_NAK_Received( int intPortPair, int intSideAorB);
void    ALI_ACK_Received( int intPortPair, int intSideAorB);
void    Process_ALI_Response_Received(int intPortPair, int intSideAorB, DataPacketIn DataIn);
void    Check_ALI_Heartbeat_Interval();
void    Check_ALI_Service_Heartbeat_Interval();
void    Check_ALI_Heartbeat_Timeout();
void    Check_ALI_Service_Heartbeat_Timeout();
void    Check_ALI_Record_Timeout();
//void    ALI_ScoreBoard_Event(union sigval union_sigvalArg);
void    Generate_Invalid_ALI_Data_Received_Message(int intPortPair, int intSideAorB, string stringDataIn );
void    Check_ALI_Port_Down_Notification();
void    CheckALIServiceDownNotification();
bool    ALI_Ports_Ready_To_Shut_Down( int intNumPorts);
bool    Assign_ACK_To_Active_Record(int intPortNumber, int intSideAorB);
int     DataBaseContainingPortPair(int intPortPair);
void    Refresh_Disconnect_DataList();
bool    FindTelnoInDisconnectVector(ani_type enumANItype, string strData,ExperientDataClass objData, Port_Data  objPortData );
int     find_next_avialable_index(int intBidIndex, int intTrunk);
int     Find_Index_from_Call_ID(unsigned long long int uliCallId);
int     Find_Index_from_ALI_Received(string stringALIData, Port_Data objPortData);
int     Find_Index_from_TransactionID(DataPacketIn DataIn, Port_Data objPortData);
bool    Found_TranscationID_In_DisconnectVector(DataPacketIn DataIn, ExperientDataClass objData, Port_Data objPortData); 
void    SendALIRecordFailToMain(int intBidIndex);
bool    SelectALIDatabaseandPortPair(int* intALiDatabaseNumber, int* intAliPortPair, int intBidIndex);
void    TransmitInitialBidtoDatabase(int* intALiDatabaseNumber, int intAliPortPair, int intBidIndex);
bool    TransmitBidtoALIservice(int* intALIserviceIndex, int iTableIndex);    
bool    Rebid_ALI_Record(int i, struct timespec timespecTimeNow);
void    SendUnMatchedTransactionID(int intPortPair, int intBidIndex, DataPacketIn DataIn, Port_Data objPortData);
int     Find_Table_Index_from_Bid_Index(int iBidIndex);
void    ResetPortPairStatus(int i);
int     IndexinDisconnectVectorUUID(ExperientDataClass objData);
bool    ProcessALIserviceData(DataPacketIn DataIn);

extern void *ALI_Messaging_Monitor_Event(void *voidArg);
extern void *ALI_ScoreBoard_Event(void *voidArg);

// ALI Global VARS
struct itimerspec	        itimerspecALIHealthMonitorDelay;
struct itimerspec               itimerspecALIScoreboardDelay;
timer_t			        timer_tALIHealthMonitorTimerId;
timer_t                         timer_tALIScoreboardTimerId;
ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
ExperientCommPort               ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
queue <DataPacketIn>	        queueALIPortData[intMAX_ALI_PORT_PAIRS  + 1];
ExperientDataClass		obj_arrayALIWorkTable[intMAX_NUM_TRUNKS+2];
ExperientDataClass		objQALIData;
int                             intAliPortPairRotater[intMAX_ALI_DATABASES + 1];
int                             intNUM_ALI_PORT_PAIRS;
int                             intNUM_ALI_SERVICE_PORTS;
int                             intALI_REQUEST_LENGTH;
int                             intNUM_ALI_DATABASES;
int				intCLID_ALI_BID_RULE_CODE;
unsigned int                    intMAX_ALI_RECORD_SIZE;
unsigned long int               intE2TransactionID;
bool				boolALLOW_CLID_ALI_BIDS;
bool				boolCLID_BID_TEST_KEY;
bool                            boolSHOW_GOOGLE_MAP;
bool                            boolIGNORE_BAD_ALI;
ALISteering                     ALI_Steering[intMAX_ALI_DATABASES + 1 ];
string                          strALI_NOT_ALLOWED_MESSAGE;
string                          strALI_FAILED_MESSAGE;
string                          strALI_NO_GEOLOCATION_URI;
string				strALI_BID_IN_PROGRESS_MESSAGE;
sem_t				sem_tMutexAliWorkTable;
sem_t				sem_tMutexAliInputQ;
sem_t				sem_tMutexAliMessageQ;
sem_t				sem_tMutexAliPortDataQ;
sem_t				sem_tAliFlag;
sem_t				sem_tMutexAliDisconnectList;
sem_t			        sem_tMutexE2TransactionID;
vector <ExperientDataClass>     objDisconnectDataVector;
vector <ExperientDataClass>     vALIworkTable;
vector <string>		        vALI_XML_KEYS;

/****************************************************************************************************
*
* Name:
*  Function: void *ALI_Thread(void *voidArg)
*
*
* Description:
*   Handles all of the ALI related activity for the controller. 
*
*
* Details: 
*   This function is implemented as a detached thread from the main program. The program recieves a 
*   pointer to the queue queue_objAliData from main() as its argument.
*
*   The Thread spawns (N) * 2 (A & B) UDP Port listening threads  and creates two timer event threads:
*  
*    a. ScoreBoard Event:       Utilized to process incoming ALI Records for transmission to main
*    b. Message Monitor Event:  Utilized to send error communications to main.
*
*   
*   A global semaphore is used to recieve a signal from three sources :
*
*     a. Data (from Main) has been pushed onto the ALI's input queue.
*     b. Data received from an ALI port pushed onto the Data queue.
*     c. Shutdown signal from main.
*          
*
*   The main loop idles waiting for a signal, it checks for the shutdown signal, if able to continue it processes 2 queues the first is 
*   queue_objALiInputData, the second is the queueALIPortData.
*
*   1. queue_objALiInputData: data is popped from the queue onto a table (array) of records and is processed 
*      by 2 timer event handlers ALI_ScoreBoard_Event and ALI_Messaging_Monitor_Event. 
*
*      a. Records are placed into the table by trunk number and remain there until 2 X timeout or a valid record is recieved
*
*      b. Upon receiving a record, the phone number is searched on a round robin basis for the first matching ALI Database (ALI-Steering)
*
*      c. A key is created to bid the available Database and the record is bid.
*
*   2. queueALIPortData:  data is popped from the queue and deciphered for type (ACK NAK or Record) then length.
*     
*      a. If the data is ACK reset heartbeat time
*      b. If the Data is NAK generate error message if 3 NAKs in a row set reduced Hearbeat Mode (No bids to port, reduced HB interval)
*      c. If Record Load into object clear table and pass to main
*      d. If Bad Data generate error message
*
*
*
*  
* Parameters:
*   voidArg                                       queue <ExperientDataClass>* pointer
*
* Variables:
*   ALI                                         Global  - <header.h> threadorPorttype enumeration member
*   ALI_MESSAGE_200                             Global  - <defines.h> thread created output message
*   ALI_MESSAGE_202                             Global  - <defines.h> Message Monitor Timer Event Initialized output message
*   ALI_MESSAGE_203                             Global  - <defines.h> Thread Signalling Semaphore failed to Lock output message
*   ALI_MESSAGE_214                             Global  - <defines.h> ScoreBoard Timer Event Initialized output message
*   ALI_MESSAGE_215                             Global  - <defines.h> Unexpected Character Count output message
*   ALI_MESSAGE_217                             Global  - <defines.h> Response Received too Long output message
*   ALI_MESSAGE_222                             Global  - <defines.h> ALI Request sent output message
*   ALI_MESSAGE_225                             Global  - <defines.h> No Available Port Pairs to bid output message
*   ALI_MESSAGE_239                             Global  - <defines.h> thread failed to initialize output message
*   ALIPort[][2]                                Global  - <ExperientCommPort> 2d array (the port pairs)
*   ALiPortAListenThread[]                      Local   - <pthread> UDP Port 'A' Listen threads
*   ALiPortBListenThread[]                      Local   - <pthread> UDP Port 'B' Listen threads
*   AliPortDataRecieved                         Local   - <DataPacketIn> used to store Port Data recieved from external queue
*   ALI_Steering[]                              Global  - <ALISteering> object array
*   boolACK                                     Local   - bool .. true if data is an ACK
*   .boolACKReceived                            Global  - <ExperientCommPort> member
*   .boolHeartbeatActive                     Global  - <ExperientCommPort> member
*   .boolALIRecordActive                        Global  - <ExperientDataClass><ALI_Data> member 
*   .boolALIRecordFail                          Global  - <ExperientDataClass><ALI_Data> member 
*   .boolALiThreadReady                         Global  - <Shutdown> member
*   boolDEBUG                                   Global  - bool .. turns on/off debug messages
*   .boolEmergencyShutdown                      Global  - <ExperientCommPort> member
*   boolETX                                     Local   - bool .. true if data is an ETX
*   .boolFirstACKRcvd                           Global  - <ExperientCommPort> member
*   boolNAK                                     Local   - bool .. true if data is an NAK
*   .boolNAKReceived                            Global  - <ExperientCommPort> member
*   .boolPingStatus                             Global  - <ExperientCommPort> member
*   .boolReadyToShutDown                        Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatModeFirstAlarm         Global  - <ExperientCommPort> member
*   boolSTOP_EXECUTION                          Global  - <globals.h> used to stop program execution
*   .boolStringBufferExceeded                   Global  - <DataPacketIn> member
*   .boolUnexpectedDataShowMessage              Global  - <ExperientCommPort> member
*   .boolXmitRetry                              Global  - <ExperientCommPort> member
*   CLOCK_REALTIME                              Library - <ctime>
*   CPU_AFFINITY_ALL_OTHER_THREADS_SET          Global  - <globals.h><cpu_set_t>
*   .enumPortType                               Global  - <ExperientCommPort> member
*   .enumThreadSending                          Global  - <ExperientDataClass> member
*   errno                                       Library - <errno.h>
*   FOURTEEN_DIGIT                              Global  - <header.h> <ali_format> enumeration member
*   HOST_IP                                     Global  - <IP_Address>
*   intALiDatabaseNumber                        Local   - stores database number to be bid
*   intALI_CLEANUP_INTERVAL_NSEC                Global  - <defines.h>
*   intALI_HEARTBEAT_INTERVAL_SEC               Global  - <globals.h>
*   LOG_FIXED_FIELD_DISPLAY_SPACES              Global  - <defines.h> number of spaces in to be placed in stringALI_LOG_SPACES
*   intALI_MESSAGE_MONITOR_INTERVAL_NSEC        Global  - <defines.h>
*   intALI_PORT_DOWN_REMINDER_SEC               Global  - <globals.h>
*   intALI_PORT_DOWN_THRESHOLD_SEC              Global  - <globals.h>
*   intAliPortPair                              Local   - integer representing a port pair
*   intAliPortPairRotater[]                     Local   - stores integer of last port pair bid on a [database] (used to rotate bids on port pairs)
*   .intALITimeoutSec                           Global  - <ExperientDataClass><ALI_Data> member
*   .intCallbackNumber                          Global  - <ExperientDataClass> member
*   .intConsecutiveBadRecordCounter             Global  - <ExperientCommPort> member
*   intDataToDisplay                            Local   - number of characters to display for bad data
*   .intHeartbeatInterval                       Global  - <ExperientCommPort> member
*   intFirstLook                                Local   - first database to look at (round robin)
*   .intLength                                  Global  - <DataPacketIn> member
*   intLogCode                                  Local   - code to indicate to the Log Thread how to handle the message ALARM, INFO, LOG_WARNING
*   intMAX_DATA_TO_DISPLAY                      Global  - <defines.h>
*   intNUM_ALI_DATABASES                        Global  - <globals.h> number of installed ALI Databases
*   intNUM_ALI_PORT_PAIRS                       Global  - <globals.h> number of installed ALI port pairs
*   .intNumPortPairs                            Global  - <ALISteering> member
*   intPING_TIMEOUT                             Global  - <defines.h>
*   .intPosn                                    Global  - <ExperientDataClass> member
*   .intPortDownReminderThreshold               Global  - <ExperientCommPort> member
*   .intPortDownThresholdSec                    Global  - <ExperientCommPort> member
*   .intPortNum                                 Global  - <ExperientCommPort> member
*   .intPortNum                                 Global  - <DataPacketIn> member
*   intPortPair                                 Local   - ALI port pair (A & B)
*   .intALIPortPairUsedtoTX                     Global  - <ExperientDataClass><ALI_Data> member Keeps track of port pair data was sent on 
*   intRC                                       Local   - integer function return code
*   .eRequestKeyFormat                          Global  - <ALISteering> member
*   intSideAorB                                 Local   - integer representing individal port pair elements
*   .intSideAorB                                Global  - <ExperientCommPort> member
*   .intSideAorB                                Global  - <DataPacketIn> member
*   intTHREAD_SHUTDOWN_DELAY_SEC                Global  - <defines.h>
*   intTrunk                                    Local   - Trunk of phone call being bid
*   .intTrunk                                   Global  - <ExperientDataClass> member
*   .intUnexpectedDataCharacterCount            Global  - <ExperientCommPort> member
*   .intUniqueCallID                            Global  - <ExperientDataClass> member
*   itimerspecALIHealthMonitorDelay             Global  - <ctime><itimerspec> structure
*   itimerspecALIScoreboardDelay                Global  - <ctime><itimerspec> structure
*   j                                           Local   - general purpose integer
*   KRN_MESSAGE_142				Global  - <defines.h> CPU affinity Error message
*   LOG_CONSOLE_FILE                            Global  - <defines.h> integer log code
*   LOG_WARNING                                 Global  - <defines.h> integer log code
*   intMAX_ALI_RECORD_SIZE                         Global  - <defines.h> 
*   objALIData                                  Local   - <ExperientDataClass> object used to store incoming ali data
*   objMessage                                  Local   - <MessageClass> object
*   OrderlyShutDown                             Global  - <Thread_Trap> object used to store thread(s) shutdown status
*   .PingPort                                   Global  - <ExperientCommPort> member
*   pthread_attr_tAttr                          Local   - <pthread> thread attribute
*   PTHREAD_CREATE_JOINABLE                     Library - <pthread>
*   queue_objALiInputData                       Local   - <queue><ExperientDataClass>* ptr to queue in main used to pass records for ALI bid
*   sem_tAliFlag                                Global  - <semaphore.h> RX signal used to initiate main ALI loop
*   sem_tMutexAliInputQ                         Global  - <semaphore.h><sem_t> mutex semaphore 
*   sem_tMutexAliWorkTable                      Global  - <semaphore.h><sem_t> mutex semaphore 
*   .sem_tMutexUDPPortBuffer                    Global  - <ExperientCommPort> member
*   sigeventALIMessageMonitorEvent              Local   - <csignal> struct sigev ent that contains the data that defines the event handler
*   sigeventALIScoreboardEvent                  Local   - <csignal> struct sigevent that contains the data that defines the event handler
*   .sigev_notify                               Library - <csignal><sigevent> member
*   .sigev_notify_attributes                    Library - <csignal><sigevent> member
*   .sigev_notify_function                      Library - <csignal><sigevent> member
*   SIGEV_THREAD                                Library - <csignal>
*   SIXTEEN_DIGIT                               Global  - <header.h> <ali_format> enumeration member
*   .stringAddress                              Global  - <IP_Address> member
*   stringALI_LOG_SPACES                        Global  - <globals.h> used to line up ALI Data in the log
*   .stringALiRequestKey                        Global  - <ExperientDataClass><ALI_Data> member
*   .stringBuffer                               Global  - <ExperientCommPort> member
*   .stringDataIn                               Global  - <DataPacketIn> member
*   stringSideAorB(3)                           Local   - <vector><cstring> used to store the Letters A & B for output
*   size_tStringPosition                        Local   - <cstring><size_t> used to indicate character position within a string
*   .stringAliRequest                           Global  - <ExperientDataClass><ALI_Data> member
*   string::npos                                Library - <cstring>
*   stringTemp                                  Local   - <cstring> temp data string
*   stringTemp2                                 Local   - <cstring> another temp data string
*   .stringTenDigitPhoneNumber                  Global  - <ExperientDataClass> member
*   .stringTimeOfEvent                          Global  - <ExperientDataClass> member
*   .stringTimeStamp                            Global  - <MessageClass> member
*   timespecNanoDelay                           Global  - <ctime><timespec> structure holds nanoseconds for a shutdown delay
*   timespecRemainingTime                       Global  - <ctime><timespec> structure dummy variable to used in nanosleep() function
*   .timespecTimeRecordReceivedStamp            Global  - <ExperientDataClass> member
*   timer_tALIHealthMonitorTimerId              Global  - <sys/types.h> <timer_t> structure 
*   timer_tALIScoreboardTimerId                 Global  - <sys/types.h> <timer_t> structure   
*   .UDP_Port                                   Global  - <ExperientCommPort> <ExperientUDPPort> member object
*
*                                                            
* Functions:
*   Abnormal_Exit()				Global  - <globalfunctions.h>                           (void)
*   ASCII_String()                              Global  - <globalfunctions.h>                           (string)
*   ALI_ACK_Received()                          Local                                                   (void)
*   ALI_NAK_Received()                          Local                                                   (void)
*   ALI_Ports_Ready_To_Shut_Down()              Local                                                   (bool)
*   .append()                                   Library - <cstring>                                     (*this)
*   BuildSTXtoETX()                             Global  - <globalfunctions.h>                           (string)                             
*   clock_gettime()                             Library - <ctime>                                       (int)
*   .Config()                                   Library - <IPworks.h<Ping>                              (int)
*   .Config()                                   Library - <IPworks.h><UDPPort>                          (char*)
*   Console_Message()                           Global  - <globalfunctions.h>                           (void)
*   .c_str()                                    Library - <cstring>                                     (const char*)
*   .empty()                                    Library - <queue>                                       (bool)
*   .end()                                      Library - <cstring>                                     (string::iterator)
*   enQueue_Message()                           Global  - <globalfunctions.h>                           (void)
*   .erase()                                    Library - <cstring>                                     (*this)
*   .Error_Handler()                            Global  - <ExperientCommPort>                           (void)
*   .fAllThreadsReady()                         Global  - <Shutdown>                                    (bool)
*   .fClear_Record()                            Global  - <ExperientDataClass>                          (void)
*   .fCreateAliRequestKey()                     Global  - <ExperientDataClass>                          (void)
*   .fEmergencyPortShutDown()                   Global  - <ExperientCommPort>                           (void)
*   .find_first_of()                            Library - <cstring>                                     (size_t)
*   .fMessage_Create()                          Global  - <MessageClass>                                (void)
*   .front()                                    Library - <queue>                                       (*this)
*   .fSelectALiDatabase()                       Global  - <ExperientDataClass>                          (int)
*   .fSelectPortPair()                          Global  - <ALISteering>                                 (int)
*   .fSetPortActive()                           Global  - <ExperientCommPort>                           (void)
*   .fSetPortInactive()                         Global  - <ExperientCommPort>                           (void)
*   .fSet_Time_Recieved()                       Global  - <ExperientDataClass>                          (void)
*   int2strLZ()                                 Global  - <globalfunctions.h>                           (string)
*   Generate_Invalid_ALI_Data_Received_Message()  Local   -                                               (void)
*   getpid()                                    Library - <unistd.h>                                    (pid_t)
*   .length()                                   Library - <cstring>                                     (size_t)
*   nanosleep()                                 Library - <ctime>                                       (int)
*   PortQueueRoundRobin()                       Global  - <globalfunctions.h>                           (int)
*   .pop()                                      Library - <queue>                                       (void)
*   Process_ALI_Response_Received()             Local                                                   (void)
*   pthread_attr_init()                         Library - <pthread.h>                                   (int)
*   pthread_attr_setdetachstate()               Library - <pthread.h>                                   (int)
*   pthread_create()                            Library - <pthread.h>                                   (int)
*   pthread_self()                              Library - <pthread.h>                                   (pthread_t)
*   pthread_setaffinity_np()			Library - <pthread.h>                                   (int)
*   .push()                                     Library - <queue>                                       (void)
*   Semaphore_Error()                           Global  - <globalfunctions.h>                           (void)
*   sem_init()                                  Library - <semaphore.h>                                 (int)
*   sem_post()                                  Library - <semaphore.h>                                 (int)
*   sem_wait()                                  Library - <semaphore.h>                                 (int)
*   .SetAcceptData()                            Library - <IPworks.h><UDPPort>                          (int)
*   .SetDontRoute()                             Library - <IPworks.h><UDPPort>                          (int)
*   .SetLocalHost()                             Library - <IPworks.h><Ping>                             (int)
*   .SetLocalHost()                             Library - <IPworks.h><UDPPort>                          (int)
*   .SetLocalPort()                             Library - <IPworks.h><UDPPort>                          (int)
*   .SetPacketSize()                            Library - <IPworks.h><Ping>                             (int)
*   .SetRemoteHost()                            Library - <IPworks.h><UDPPort>                          (int)
*   .SetRemotePort()                            Library - <IPworks.h><UDPPort>                          (int)
*   .SetTimeout()                               Library - <IPworks.h><Ping>                             (int)
*   Set_Timer_Delay()                           Global  - <globalfunctions.h>                           (void) 
*   sleep()                                     Library - <unistd.h>                                    (unsigned int)
*   timer_create()                              Library - <ctime>                                       (int)
*   timer_delete()                              Library - <ctime>                                       (int)
*   timer_settime()                             Library - <ctime>                                       (int)
*   Transmit_Data()                             Global  - <globalfunctions.h>                           (int)                     
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2018 WestTel Corporation 
****************************************************************************************************/

void *ALI_Thread(void *voidArg)
{

 struct sigevent		                 sigeventALIMessageMonitorEvent;
 struct sigevent                                 sigeventALIScoreboardEvent;
 pthread_t                                       ALiPortAListenThread[intMAX_ALI_PORT_PAIRS + 1];
 pthread_t                                       ALiServicePortListenThread[intMAX_ALI_SERVICE_PORTS + 1];
 param_t                                         paramsS[intMAX_ALI_SERVICE_PORTS + 1];
 param_t                                         paramsA[intMAX_ALI_PORT_PAIRS + 1];
 pthread_t                                       ALiPortBListenThread[intMAX_ALI_PORT_PAIRS + 1];
 pthread_t		 			 pthread_tAliMessagingThread;
 pthread_t		 			 pthread_tAliScoreboardThread;
 param_t                                         paramsB[intMAX_ALI_PORT_PAIRS + 1];
 pthread_attr_t                                  pthread_attr_tAttr;
 int                                             intRC;
 int                                             intTrunk;
 int                                             intDataToDisplay;
 int                                             intFirstLook = 0;
 int                                             intNumberofALIQueues;
 int                                             intPortPair;
 int                                             intSideAorB;
 string                                          stringTemp;
 string                                          stringTemp2;
 MessageClass                                    objMessage;
 ExperientDataClass		                 objALIData;
 queue <ExperientDataClass>*  	                 queue_objALiInputData;
 int                                             intALiDatabaseNumber;
 int                                             intALIserviceBid;
 int                                             intAliPortPair;
 DataPacketIn                                    AliPortDataRecieved;
 bool                                            boolACK, boolNAK, boolETX;
 size_t                                          size_tStringPosition;
 vector <string>                                 stringSideAorB(3);
 Port_Data                                       objPortData;
 int                                             intBidIndex;
 int                                             intDiscIndex;
 int                                             iTableIndex;
 struct timespec                                 timespecRemainingTime;
 Thread_Data                     		 ThreadDataObj;      
 extern vector <Thread_Data> 			 ThreadData;

 bool StartALIServiceListenThreads(pthread_t ALiServicePortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t paramsS[]);
 bool StartALIportListenThreads(pthread_t ALiPortAListenThread[], pthread_t ALiPortBListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t paramsA[], param_t paramsB[]);
 bool InitializeALIPortPairRotate(int intAliPortPairRotater[]);
 bool InitializeALIlegacyPorts();
 bool InitializeALIservicePorts();
 bool ShutDownALIservicePorts(pthread_t ALiServicePortListenThread[], int inumber);
 bool ShutDownALIlegacyPorts(pthread_t ALiPortAListenThread[], pthread_t ALiPortBListenThread[], int numberofports);      
 queue_objALiInputData 	= (queue <ExperientDataClass>*) voidArg;				//recast voidArg

 stringSideAorB[1] = "A";
 stringSideAorB[2] = "B";
 strALI_NOT_ALLOWED_MESSAGE = charSTX;
 strALI_NOT_ALLOWED_MESSAGE.append(intALI_RECORD_LINE_WRAP*6, ' ');       
 strALI_NOT_ALLOWED_MESSAGE.append(7, ' ');
 strALI_NOT_ALLOWED_MESSAGE += "ALI NOT APPLICABLE";
 strALI_NOT_ALLOWED_MESSAGE.append(14, ' ');
 strALI_NOT_ALLOWED_MESSAGE += "FOR THIS CALL TYPE";
 strALI_NOT_ALLOWED_MESSAGE += charETX;

 strALI_FAILED_MESSAGE = charSTX;
 strALI_FAILED_MESSAGE.append(intALI_RECORD_LINE_WRAP*6, ' ');       
 strALI_FAILED_MESSAGE.append(7, ' ');
 strALI_FAILED_MESSAGE += "UNABLE TO RETRIEVE";
 strALI_FAILED_MESSAGE.append(17, ' ');
 strALI_FAILED_MESSAGE += "ALI RECORD";
 strALI_FAILED_MESSAGE += charETX;

 strALI_BID_IN_PROGRESS_MESSAGE = charSTX;
 strALI_BID_IN_PROGRESS_MESSAGE.append(intALI_RECORD_LINE_WRAP*6, ' ');
 strALI_BID_IN_PROGRESS_MESSAGE.append(7, ' ');       
 strALI_BID_IN_PROGRESS_MESSAGE += "ALI BID IN PROGRESS";
 strALI_BID_IN_PROGRESS_MESSAGE += charETX;

 strALI_NO_GEOLOCATION_URI = charSTX;
 strALI_NO_GEOLOCATION_URI.append(intALI_RECORD_LINE_WRAP*6, ' ');       
 strALI_NO_GEOLOCATION_URI.append(7, ' ');
 strALI_NO_GEOLOCATION_URI += "NO GEOLOCATION URI";
 strALI_NO_GEOLOCATION_URI.append(17, ' ');
 strALI_NO_GEOLOCATION_URI += "RECEIVED";
 strALI_NO_GEOLOCATION_URI += charETX;

 stringALI_LOG_SPACES.append(LOG_FIXED_FIELD_DISPLAY_SPACES,' ');

 vALI_XML_KEYS.push_back("ACK");
 vALI_XML_KEYS.push_back("ALIDATA");
 vALI_XML_KEYS.push_back("ALI_RECORD_FAIL");
 vALI_XML_KEYS.push_back("EncodedLegacyRecord");

/*
 // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,248, objBLANK_CALL_RECORD, objBLANK_ALI_PORT, KRN_MESSAGE_142); enQueue_Message(ALI,objMessage);}
*/
 ThreadDataObj.strThreadName ="ALI";
 //ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ali.cpp::ALI_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Send Initialized message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,200, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT,
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_200, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(ALI,objMessage);

/* Now set in initial config .......
 // Load Key formats
 for (int i = 1; i <= intNUM_ALI_DATABASES; i++)
  {
   for (int j = 1; j <= intNUM_ALI_PORT_PAIRS; j++)
    {  
     if (ALI_Steering[i].boolPortPairInDatabase[j])
      {
       for (int k = 1; k <=2; k++) { ALIPort[j] [k].eRequestKeyFormat = ALI_Steering[i].eRequestKeyFormat;}
      }     
    }
  
  }
*/
/*
 // Initalize ALI Ports
 for(int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
  {
   for (int j = 1; j <=2; j++) { ALIPort[i] [j].fInitializeALIport(i,j);}
  }
*/
// for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++) {ALIServicePort[i].fInitializeALIport(i,3); } 



 // Initialize some variables before the ALI main loop
 intALIserviceBid = 1;
 intALiDatabaseNumber = 1;

 InitializeALIPortPairRotate(intAliPortPairRotater);

// for (int i=1; i < intMAX_ALI_DATABASES; i++){intAliPortPairRotater[i] = 0;}

  pthread_attr_init(&pthread_attr_tAttr);
  pthread_attr_setdetachstate(&pthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);



 InitializeALIservicePorts();
 if (!StartALIServiceListenThreads(ALiServicePortListenThread, &pthread_attr_tAttr, paramsS)) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_239);}
/*
  // start ALI Service listen thread(s)
  for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++) 
   {
    paramsS[i].intPassedIn = i;
    intRC = pthread_create(&ALiServicePortListenThread[i], &pthread_attr_tAttr, *Ali_Service_Port_Listen_Thread, (void*) &paramsS[i]);
    if (intRC) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_239, int2str(i), "S");}
   }
*/
  // start port pair (A&B) listen thread(s)
 InitializeALIlegacyPorts();
 if (!StartALIportListenThreads(ALiPortAListenThread, ALiPortBListenThread, &pthread_attr_tAttr, paramsA, paramsB)) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_239);}

/*
  for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
   {
    paramsA[i].intPassedIn = paramsB[i].intPassedIn = i;
    intRC = pthread_create(&ALiPortAListenThread[i], &pthread_attr_tAttr, *Ali_Port_A_Listen_Thread, (void*) &paramsA[i]);
    if (intRC) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_239, int2str(i), "A");}
    intRC = pthread_create(&ALiPortBListenThread[i], &pthread_attr_tAttr, *Ali_Port_B_Listen_Thread, (void*) &paramsB[i]);
    if (intRC) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_239, int2str(i), "B");}
   }
 
*/





 
  //start timed threads
  intRC = pthread_create(&pthread_tAliMessagingThread, &pthread_attr_tAttr, *ALI_Messaging_Monitor_Event, NULL);
  if (intRC) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_296);}
  intRC = pthread_create(&pthread_tAliScoreboardThread, &pthread_attr_tAttr, *ALI_ScoreBoard_Event, NULL);
  if (intRC) {Abnormal_Exit(ALI, EX_OSERR, ALI_MESSAGE_295);}

/*
 // set up ALI Message monitor timer event
 sigeventALIMessageMonitorEvent.sigev_notify 			= SIGEV_THREAD;
 sigeventALIMessageMonitorEvent.sigev_notify_function 		= ALI_Messaging_Monitor_Event;
 sigeventALIMessageMonitorEvent.sigev_notify_attributes 	= NULL;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventALIMessageMonitorEvent, &timer_tALIHealthMonitorTimerId);
 if (intRC){Abnormal_Exit(ALI, EX_OSERR, KRN_MESSAGE_180, "timer_tALIHealthMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecALIHealthMonitorDelay, 0,intALI_MESSAGE_MONITOR_INTERVAL_NSEC , 0, intALI_MESSAGE_MONITOR_INTERVAL_NSEC  );
 Set_Timer_Delay(&itimerspecALIHealthMonitorDelay, 0,intALI_MESSAGE_MONITOR_INTERVAL_NSEC , 0, 0  );
 timer_settime( timer_tALIHealthMonitorTimerId, 0, &itimerspecALIHealthMonitorDelay, NULL);

 // arm timer
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,202, objBLANK_CALL_RECORD, objBLANK_ALI_PORT, ALI_MESSAGE_202); 
 enQueue_Message(ALI,objMessage);
*/
/*
 // setup sigeventALIScoreboardEvent timer event
 sigeventALIScoreboardEvent.sigev_notify 		        = SIGEV_THREAD;
 sigeventALIScoreboardEvent.sigev_notify_function 		= ALI_ScoreBoard_Event;
 sigeventALIScoreboardEvent.sigev_notify_attributes 	        = NULL;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventALIScoreboardEvent, & timer_tALIScoreboardTimerId);
 if (intRC){Abnormal_Exit(ALI, EX_OSERR, KRN_MESSAGE_180, "timer_tALIScoreboardTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecALIScoreboardDelay, 0,intALI_CLEANUP_INTERVAL_NSEC, 0, intALI_CLEANUP_INTERVAL_NSEC );
 Set_Timer_Delay(&itimerspecALIScoreboardDelay, 0,intALI_CLEANUP_INTERVAL_NSEC, 0, 0 );
 timer_settime( timer_tALIScoreboardTimerId, 0, &itimerspecALIScoreboardDelay, NULL);			// arm timer

 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,214, objBLANK_CALL_RECORD, objBLANK_ALI_PORT, ALI_MESSAGE_214); 
 enQueue_Message(ALI,objMessage);  
*/
 intBidIndex = 0;


 // ALI main loop
 
 do  {
   // wait for data signal from main or data queue
   intRC = sem_wait(&sem_tAliFlag);							

   // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
   if (intRC)     {
     objMessage.fMessage_Create(LOG_WARNING,203, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT,
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_203); 
     enQueue_Message(ALI,objMessage);
   } 

   // Shut Down Trap Area
   if (boolSTOP_EXECUTION )    {
    // timer_delete(timer_tALIHealthMonitorTimerId);
    // timer_delete(timer_tALIScoreboardTimerId); 
    for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++) {
     pthread_join( ALiPortAListenThread[i], NULL);
     pthread_join( ALiPortBListenThread[i], NULL);
    }
    pthread_join( pthread_tAliMessagingThread, NULL); 
    pthread_join( pthread_tAliScoreboardThread, NULL); 
    //    cout << "waiting for service portlisten thread" << endl;
    for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++) { pthread_join( ALiServicePortListenThread[i], NULL); }  
     //    cout << "service portlisten thread done" << endl;  
    OrderlyShutDown.boolALiThreadReady = true;

    while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
     //    cout << "jump" << endl;
     // jump to while (!boolSTOP_EXECUTION  )
    break;
										
   }// end if (boolSTOP_EXECUTION  )

  // Update Trap ----------
  if (boolUPDATE_VARS)    {
    
    UpdateVars.boolALiThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    if (!boolSTOP_EXECUTION) {
     if (INIT_VARS.ALIinitVariable.boolRestartALIservicePorts){
      // shut down listen threads()
      //     cout << "Changing ALI Service Ports" << endl;
      ShutDownALIservicePorts(ALiServicePortListenThread, INIT_VARS.ALIinitVariable.intOldNumberOfALIservicePorts);
      INIT_VARS.ALIinitVariable.boolRestartALIservicePorts = false;
      InitializeALIservicePorts();
      if (!StartALIServiceListenThreads(ALiServicePortListenThread, &pthread_attr_tAttr, paramsS)) { SendCodingError("ali.cpp - listen thread failure - recommend restart!");}
     }
     if (INIT_VARS.ALIinitVariable.boolRestartALILegacyPorts){
      //      cout << "Changing ALI Legacy Ports" << endl;
      ShutDownALIlegacyPorts(ALiPortAListenThread, ALiPortBListenThread, INIT_VARS.ALIinitVariable.intOldNumberOfLegacyALIports);
      INIT_VARS.ALIinitVariable.boolRestartALILegacyPorts = false;
      InitializeALIlegacyPorts();
      if (!StartALIportListenThreads(ALiPortAListenThread, ALiPortBListenThread, &pthread_attr_tAttr, paramsA, paramsB)) {SendCodingError("ali.cpp - listen thread failure - recommend restart!");}
     }
    }
    UpdateVars.boolALiThreadReady = false;       
  }




 //  cout << "intNUM_ALI_SERVICE_PORTS " << intNUM_ALI_SERVICE_PORTS << endl;
 //   cout << "intNUM_ALI_PORT_PAIRS " << intNUM_ALI_PORT_PAIRS << endl; 
 //   cout << "boolUSE_ALI_SERVICE_SERVER -> " << boolUSE_ALI_SERVICE_SERVER << endl; 
   // Process Inbound Data from Main ALI Input Q..........................................................
   // Lock ALI INPUT Q
   intRC = sem_wait(&sem_tMutexAliInputQ);						
   if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliInputQ, "ALI    - sem_wait@sem_tMutexAliInputQ in ALI_Thread", 1);}
             
   while (!queue_objALiInputData->empty())
    {
    // cout << "In ALI INPUT Q" << endl;
     // Lock Work Table
     intRC = sem_wait(&sem_tMutexAliWorkTable);					
     if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliWorkTable, "ALI    - sem_wait@sem_tMutexAliWorkTable in ALI_Thread", 1);}

     switch (queue_objALiInputData->front().enumANIFunction)
      {       
       case RINGING: 
       case ALI_REQUEST:
                   // cout << "IN ALI ringing: ALI_REQUEST" << endl;
                    intTrunk = queue_objALiInputData->front().CallData.intTrunk;
                    intBidIndex = find_next_avialable_index(intBidIndex, intTrunk);
                    vALIworkTable.push_back(queue_objALiInputData->front());
                    iTableIndex = vALIworkTable.size() -1;                 
                    vALIworkTable[iTableIndex].fSet_Time_Recieved();
                    if (intBidIndex < 0) {
                      vALIworkTable[iTableIndex].ALIData.intALIBidIndex = 0;
                      SendALIRecordFailToMain(iTableIndex);
                      objMessage.fMessage_Create(LOG_WARNING, 260, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA,
                                                 objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, vALIworkTable[iTableIndex].ALIData, ALI_MESSAGE_260); 
                                  
                      enQueue_Message(ALI,objMessage);
                      vALIworkTable.erase(vALIworkTable.begin()+iTableIndex);
                      queue_objALiInputData->pop();
                      sem_post(&sem_tMutexAliWorkTable);
                      continue;
                    }
                    vALIworkTable[iTableIndex].CallData.stringPosn    = int2strLZ(intBidIndex);
                    vALIworkTable[iTableIndex].ALIData.intALIBidIndex = intBidIndex;
                    queue_objALiInputData->pop();
                    // cout << "ALI TABLE SIZE" << vALIworkTable.size() << endl;
                    break;
       case TAKE_CONTROL:
                    // update position number in abandoned ALI 
                   // cout << "IN TAKE CTL / ALI" << endl;
                    intRC = sem_wait(&sem_tMutexAliDisconnectList);					
                    if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliDisconnectList, "ALI    - sem_wait@sem_tMutexAliDisconnectList in TC ALI_Thread", 1);}
                    intDiscIndex = IndexinDisconnectVectorUUID(queue_objALiInputData->front());
                    if (intDiscIndex >= 0)
                     {
                      objDisconnectDataVector[intDiscIndex].CallData.fLoadPosn(queue_objALiInputData->front().CallData.intPosn);
                      objDisconnectDataVector[intDiscIndex].CallData.fConferenceStringAdd(POSITION_CONF, queue_objALiInputData->front().CallData.intPosn);
                      objDisconnectDataVector[intDiscIndex].CallData.fConferenceStringRemove(queue_objALiInputData->front().CallData.intPosn);
                      objDisconnectDataVector[intDiscIndex].FreeswitchData.objChannelData.fLoadConferenceDisplayP(queue_objALiInputData->front().CallData.intPosn);
                      objDisconnectDataVector[intDiscIndex].FreeswitchData.objChannelData.strCallerID = strCALLER_ID_NUMBER;
                      objDisconnectDataVector[intDiscIndex].FreeswitchData.objChannelData.strCustName = strCALLER_ID_NAME;
                      objDisconnectDataVector[intDiscIndex].CallData.ConfData.vectConferenceChannelData.push_back(objDisconnectDataVector[intDiscIndex].FreeswitchData.objChannelData);        
                     }
                    sem_post(&sem_tMutexAliDisconnectList);
                    queue_objALiInputData->pop();
                    sem_post(&sem_tMutexAliWorkTable);
                    continue;
       case DISCONNECT: case ABANDONED:
                   // cout << "IN DISC / Abandoned" << endl;
                    intDiscIndex = Find_Index_from_Call_ID(queue_objALiInputData->front().CallData.intUniqueCallID);
                    if (intDiscIndex >= 0)
                     {
                      // ALI Record Active ... place into disconnect vector
                      intRC = sem_wait(&sem_tMutexAliDisconnectList);					
                      if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliDisconnectList, "ALI    - sem_wait@sem_tMutexAliDisconnectList in DA ALI_Thread", 1);}
                 
                      if (queue_objALiInputData->front().enumANIFunction == ABANDONED) {vALIworkTable[intDiscIndex].boolAbandoned = true;}
                      vALIworkTable[intDiscIndex].fLoad_Position(SOFTWARE, ALI, queue_objALiInputData->front().CallData.stringPosn, 0, "", true);
                      vALIworkTable[intDiscIndex].intCallStateCode = queue_objALiInputData->front().intCallStateCode;
                      vALIworkTable[intDiscIndex].CallData = queue_objALiInputData->front().CallData;
                      vALIworkTable[intDiscIndex].WindowButtonData = queue_objALiInputData->front().WindowButtonData;
                      objDisconnectDataVector.push_back(vALIworkTable[intDiscIndex]); 
                      vALIworkTable.erase(vALIworkTable.begin()+intDiscIndex);
                      sem_post(&sem_tMutexAliDisconnectList);
                     }
                    queue_objALiInputData->pop();


                    sem_post(&sem_tMutexAliWorkTable);
                    continue;					 	 				// while (!queue_objALiInputData->empty())
       default:
               SendCodingError("ali.cpp - Coding Error in ALI_Thread switch (queue_objALiInputData->front().enumANIFunction)");
          //     cout << "ani function = " << queue_objALiInputData->front().enumANIFunction << endl;
               queue_objALiInputData->pop();
               sem_post(&sem_tMutexAliWorkTable);
               continue;										// while (!queue_objALiInputData->empty())

      }// end switch (queue_objALiInputData->front().enumANIFunction)


     switch (boolUSE_ALI_SERVICE_SERVER )
      {
       case true:
                  // Using ALI Service..
                  if ((!intALIserviceBid)||(intALIserviceBid > intNUM_ALI_SERVICE_PORTS)) {intALIserviceBid = 1;}
                  if (!TransmitBidtoALIservice(&intALIserviceBid, iTableIndex))           { vALIworkTable.erase(vALIworkTable.begin()+iTableIndex); }
                  break;

       case false:
                  // Using Database
                  if ((!intALiDatabaseNumber)||(intALiDatabaseNumber > intNUM_ALI_DATABASES)) {intALiDatabaseNumber = 1;}

                 // cout << "INPUT " << intALiDatabaseNumber << " " << intAliPortPair << " " << iTableIndex << endl;


                  if(SelectALIDatabaseandPortPair(&intALiDatabaseNumber, &intAliPortPair, iTableIndex)) {
                    // cout << "HERE A............................................................................." << endl;
                     TransmitInitialBidtoDatabase(&intALiDatabaseNumber, intAliPortPair, iTableIndex);
                   }
                   else {vALIworkTable.erase(vALIworkTable.begin()+iTableIndex); }
                   break;
       }



     intRC = sem_post(&sem_tMutexAliWorkTable);					// UNLOCK Work Table
             
    }//end while (!queue_objALiInputData->empty())


   

   // Process Data from ALI Port Pairs............................................................................
   // UNLock ALI INPUT Q
   intRC = sem_post(&sem_tMutexAliInputQ);						  

   intFirstLook++;
   if ( boolUSE_ALI_SERVICE_SERVER ) { intNumberofALIQueues = intNUM_ALI_SERVICE_PORTS ;}
   else                              { intNumberofALIQueues = intNUM_ALI_PORT_PAIRS;}

   if (intFirstLook > intNumberofALIQueues) {intFirstLook = 1;}

   while (PortQueueRoundRobin(queueALIPortData, intNumberofALIQueues, intFirstLook, &sem_tMutexAliPortDataQ) != 0 )
    {
     // sequentially select port queue to pop() data (this prevents a DOS attack from 
     intPortPair = PortQueueRoundRobin(queueALIPortData, intNumberofALIQueues, intFirstLook, &sem_tMutexAliPortDataQ); 
     intFirstLook++;
     if (intFirstLook > intNumberofALIQueues) {intFirstLook = 1;}
     

     // Lock ALI WORK Table
     intRC = sem_wait(&sem_tMutexAliWorkTable);
     if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliWorkTable, "[WARNING] ALI - sem_wait@sem_tMutexAliWorkTable in ALI_Thread", 1);}

     boolNAK = boolACK = boolETX = false;

     //Pop data off Data Q
     intRC = sem_wait(&sem_tMutexAliPortDataQ);
     if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliPortDataQ, "ALI    - sem_wait@sem_tMutexAliPortDataQ in ALI_Thread", 1);}
     AliPortDataRecieved = queueALIPortData[intPortPair].front();
     queueALIPortData[intPortPair].pop();

     sem_post(&sem_tMutexAliPortDataQ);

     // if the data packet is from the ALI Service
     if (ProcessALIserviceData(AliPortDataRecieved)) { sem_post(&sem_tMutexAliWorkTable); continue;} 


     intPortPair = AliPortDataRecieved.intPortNum;
     intSideAorB = AliPortDataRecieved.intSideAorB;

     if(ALIPort[intPortPair][intSideAorB].boolEmergencyShutdown) 
      {
       ALIPort[intPortPair][intSideAorB].intUnexpectedDataCharacterCount = 0; 
       
       sem_post(&sem_tMutexAliWorkTable); 
       continue;
      }

     // shut down port if string buffer size exceeded .. 
     if(AliPortDataRecieved.boolStringBufferExceeded)
      {
       ALIPort[intPortPair][intSideAorB].fEmergencyPortShutDown(objBLANK_CALL_RECORD, "String Buffer Size >", AliPortDataRecieved.intLength);
       
       sem_post(&sem_tMutexAliWorkTable);
       continue;
      }     


     // Data will contain either one delimiter (ACK NAK or ETX) or none
    
     size_tStringPosition = AliPortDataRecieved.stringDataIn.find_first_of(charACK);
     if (size_tStringPosition != string::npos){boolACK = true;}
     else
      { 
       size_tStringPosition = AliPortDataRecieved.stringDataIn.find_first_of(charNAK);
       if (size_tStringPosition != string::npos){boolNAK = true;}
       else
        {
         size_tStringPosition = AliPortDataRecieved.stringDataIn.find_first_of(charETX);
         if (size_tStringPosition != string::npos){boolETX = true;}
         else
          {
           //Error no delimiter found
           Generate_Invalid_ALI_Data_Received_Message(intPortPair, intSideAorB, AliPortDataRecieved.stringDataIn );

           // skip to while (!boolSTOP_EXECUTION)
           sem_post(&sem_tMutexAliWorkTable);
           continue;

          }// end if (size_tStringPosition != string::npos) else

        }// end if (size_tStringPosition != string::npos) else

      }// end if (size_tStringPosition != string::npos) else

     //check for proper response lengths for ACK NAK
     if ((boolACK||boolNAK)&&(AliPortDataRecieved.intLength > 1))
      {
       //remove ACK/NAK from string (it is not unexpected) generate error then process the ACK/NAK
       AliPortDataRecieved.stringDataIn.erase(AliPortDataRecieved.stringDataIn.end()-1);
       Generate_Invalid_ALI_Data_Received_Message(intPortPair, intSideAorB, AliPortDataRecieved.stringDataIn );
      }// end if ((boolACK||boolNAK)&&(AliPortDataRecieved.intlength > 1))

     //check for proper response lengths for ALI Text Message
     if (boolETX)              
      {
       stringTemp = BuildSTXtoETX(AliPortDataRecieved.stringDataIn);
       if (stringTemp.length() != AliPortDataRecieved.stringDataIn.length())
        {
         // Error.... data contains no STX or data is present before the STX
         if (stringTemp == "")
          {
           // no STX
           Generate_Invalid_ALI_Data_Received_Message(intPortPair, intSideAorB, AliPortDataRecieved.stringDataIn );

           // skip to while (!boolSTOP_EXECUTION)
           sem_post(&sem_tMutexAliWorkTable);
           continue;
          }
         else
          {
           // data is present before the STX
           size_tStringPosition = AliPortDataRecieved.stringDataIn.find(stringTemp);

           // put bad data into temp2
           stringTemp2.assign(AliPortDataRecieved.stringDataIn,0, size_tStringPosition -1);          
           Generate_Invalid_ALI_Data_Received_Message(intPortPair, intSideAorB, stringTemp2 );
  
           // put good data into structure
           AliPortDataRecieved.stringDataIn = stringTemp;
           AliPortDataRecieved.intLength = stringTemp.length();    
          
          }// end (stringTemp == "") else
         
        }// end if (stringTemp.length() != AliPortDataRecieved.stringDataIn.length())    
       
       // Data now bounded by STX ETX check it's length
       if(AliPortDataRecieved.intLength > intMAX_ALI_RECORD_SIZE)
        {
         if (AliPortDataRecieved.intLength > intMAX_DATA_TO_DISPLAY) {intDataToDisplay = intMAX_DATA_TO_DISPLAY;}
         else                                                        {intDataToDisplay = AliPortDataRecieved.intLength;}
         stringTemp = ASCII_String(AliPortDataRecieved.stringDataIn.c_str(), intDataToDisplay);
         objPortData.fClear(); objPortData.fLoadPortData(ALI,intPortPair,intSideAorB); 
         objMessage.fMessage_Create(LOG_WARNING,217, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                                    objBLANK_ALI_DATA, ALI_MESSAGE_217, int2strLZ(intPortPair), stringSideAorB[intSideAorB], int2strLZ(AliPortDataRecieved.intLength), stringTemp); 
         enQueue_Message(ALI,objMessage);
         ALIPort[intPortPair][intSideAorB].fIncrementConsecutiveBadRecordCounter();
         // skip to while (!boolSTOP_EXECUTION)
         sem_post(&sem_tMutexAliWorkTable);
         continue;

        }// end if(AliPortDataRecieved.intLength > intMAX_ALI_RECORD_SIZE)

     }// end if (boolETX)


     ALIPort[intPortPair][intSideAorB].boolUnexpectedDataShowMessage = true;
     if(ALIPort[intPortPair][intSideAorB].intUnexpectedDataCharacterCount > 0)
      {
       objPortData.fClear(); objPortData.fLoadPortData(ALI,intPortPair,intSideAorB); 
       objMessage.fMessage_Create(LOG_WARNING*boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC,215, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_215, int2strLZ(intPortPair), stringSideAorB[intSideAorB],
                                  int2strLZ(ALIPort[intPortPair][intSideAorB].intUnexpectedDataCharacterCount), "","","", DEBUG_MSG); 
       enQueue_Message(ALI,objMessage);
       ALIPort[intPortPair][intSideAorB].intUnexpectedDataCharacterCount = 0;
      }
        

     if      (boolACK)                   {ALI_ACK_Received              ( intPortPair, intSideAorB);}
     else if (boolNAK)                   {ALI_NAK_Received              ( intPortPair, intSideAorB);}
     else if (boolETX)                   {Process_ALI_Response_Received (intPortPair, intSideAorB, AliPortDataRecieved);}

 
     // Unlock Work table
     sem_post(&sem_tMutexAliWorkTable);

    }//end while (PortQueueRoundRobin(queueALIPortData, intNUM_ALI_PORT_PAIRS, intFirstLook) != 0 )





 } while (!boolSTOP_EXECUTION);
 //cout << "ending ali" << endl;
 objMessage.fMessage_Create(0, 299, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_299);
 objMessage.fConsole();
 return NULL;

}// end ALI_Thread()

/****************************************************************************************************
*
* Name:
*  Function: void Process_ALI_Response_Received( int intPortPair, int intSideAorB, DataPacketIn DataIn)
*
* Description:
*   A function within Ali_Thread()
*
*
* Details:
*   This function Processes an ALI Record received from an ALI Port.  It validates and loads the data into an ExperientDataClass object.
*   If the data is valid, the object is sent to Main for further processing. The record is cleared from the work table upon exit. 
*
*
*
* Parameters: 
*   DataIn                              <DataPacketIn>
*   intPortPair                         integer
*   intSideAorB                         integer				
*					
*
* Variables:
*   ALI                                         Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_218                             Global  - <defines.h> Bad Data Message
*   ALI_MESSAGE_226                             Global  - <defines.h> Data Received Message
*   ALI_MESSAGE_229                             Global  - <defines.h> Broadcast Message from ALI Database
*   ALI_MESSAGE_230                             Global  - <defines.h> Broadcast Message Host going out of Service
*   ALI_MESSAGE_232                             Global  - <defines.h> Extra Data Received
*   ALIPort[][]                                 Global  - <ExperientCommPort> object
*   .boolALIRecordReceived                      Global  - <ExperientDataClass><ALI_Data> member
*   .boolHeartbeatActive                        Global  - <ExperientCommPort> member
*   .boolALI_Test_Key_Was_Bid                   Global  - <ExperientDataClass><ALI_Data> member
*   CLOCK_REALTIME                              Library - <ctime> Constant
*   .enumThreadSending                          Global  - <ExperientDataClass><threadorPorttype> enumeration member
*   .intConsecutiveNAKCounter                   Global  - <ExperientCommPort> member
*   .intLength                                  Global  - <DataPacketIn> member
*   intTrunk                                    Local   - 
*   .intTrunk                                   Global  - <ExperientDataClass> member
*   LOG_WARNING                                 Global  - <defines.h> Log Code
*   objALIData                                  Local   - <ExperientDataClass> object
*   objMessage                                  Local   - <MessageClass> object
*   queue_objMainData                           Global  - <queue><ExperientDataClass> objects
*   sem_tMutexMainInputQ                        Global  - <globals.h>
*   .stringAliText                              Global  - <ExperientDataClass><ALI_Data> member
*   stringDataIn                                Global  - <DataPacketIn> member
*   stringSideAorB                              Local   - <cstring> 
*   stringTemp                                  Local   - <cstring>
*   .stringTenDigitPhoneNumber                  Global  - <ExperientDataClass> member
*   .stringTimeOfEvent                          Global  - <ExperientDataClass> member
*   .stringTimeStamp                            Global  - <MessageClass> member
*   .stringTrunk                                Global  - <ExperientDataClass> member
*   .timespecTimeLastHeartbeat                  Global  - <ExperientCommPort> member
*   
*
*                                                                          
* Functions:
*   .assign()                                   Library <cstring>                       (*this)
*   char2int()                                  Global  <globalfunctions.h>             (unsigned long long int)
*   clock_gettime()                             Library <ctime>                         (int)
*   .end()                                      Library - <cstring>                     (string::iterator)
*   enQueue_Message()                           Global  <MessageClass>                  (void)
*   .erase()                                    Library <cstring>                       (*this)
*   .fClear_Record()                            Global  <ExperientDataClass>            (void)
*   .fFindpANI()                                Global  <ExperientDataClass>            (bool)
*   .fFindTelephoneNumber()                     Global  <ExperientDataClass>            (bool)
*   .fFindZeroTelno()                           Global  <ExperientDataClass>            (string)
*   .fInt_of_Trunk()                            Global  <ExperientDataClass>            (int)
*   .fLoad_ALI_Type()                           Global  <ExperientDataClass>            (bool)
*   .fLoad_ALI_Trunk_from_Position()            Global  <ExperientDataClass>            (bool)
*   .fMessage_Create()                          Global  <MessageClass>                  (void)                 
*   int2strLZ()                                 Global  <globalfunctions.h>             (string)
*   .push()                                     Library <queue>                         (void)                         
*   sem_post()                                  Library <csignal>                       (int)
*   sem_wait()                                  Library <csignal>                       (int)
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void Process_ALI_Response_Received(int intPortPair, int intSideAorB, DataPacketIn DataIn)
{
 int                    intBidIndex;
 int                    iTableIndex;
 int                    iLogcode;
 ExperientDataClass     objALIData;
 MessageClass           objMessage;
 string                 stringTemp;
 Call_Data              objCallData;
 Port_Data              objPortData;
 struct timespec        timespecTimeNow;
 bool                   boolRebidNotFound = true;
 string                 strMessage;

 extern vector <ExperientDataClass>     vALIworkTable;
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientDataClass       	objQALIData;
 extern ALISteering                     ALI_Steering[intMAX_ALI_DATABASES + 1 ];

 switch (ALIPort[intPortPair] [intSideAorB].ePortConnectionType)
  {
   case UDP: objPortData = ALIPort[intPortPair][intSideAorB].UDP_Port.fPortData();break;
   case TCP: objPortData = ALIPort[intPortPair][intSideAorB].TCP_Port.fPortData();break;
   default: SendCodingError("ali.cpp - coding error in switch 1. in fn Process_ALI_Response_Received()");
  }

 // Process Type Field
 if(!objALIData.fLoad_ALI_Type(DataIn.stringDataIn))
  {
   objMessage.fMessage_Create(LOG_WARNING,218, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_218a, 
                              ASCII_String(objALIData.ALIData.stringType.c_str(),objALIData.ALIData.stringType.length()) , 
                              ASCII_String(DataIn.stringDataIn.c_str(), DataIn.intLength),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(ALI,objMessage);
   // TBC do we want to abort for this ????
  }
 else if (objALIData.ALIData.stringType == "3")
       {
        // Broadcast Message from ALI Database
        objMessage.fMessage_Create(LOG_WARNING,229, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_229, DataIn.stringDataIn, "","","","","", NORMAL_MSG, ALI_RECORD);
        enQueue_Message(ALI,objMessage);
        return;
       }
 else if (objALIData.ALIData.stringType == "5")
       {
        //Broadcast Message Host going out of Service
        objMessage.fMessage_Create(LOG_ALARM,230, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData,
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_230, stringTemp);
        enQueue_Message(ALI,objMessage); 
        return;
       }




  // retrieve index number from matching Transaction Id or from position field returned in ALI record
  if (DataIn.intTransactionID) 
   {
    iTableIndex   = Find_Index_from_TransactionID(DataIn, objPortData); 
    if (iTableIndex < 0){ if( Found_TranscationID_In_DisconnectVector(DataIn, objALIData, objPortData)) {return;} }
    if (iTableIndex < 0){SendUnMatchedTransactionID(intPortPair, DataIn.intTransactionID, DataIn, objPortData); return;}
   }
  else                                
   {
    intBidIndex = objALIData.ALIData.intALIBidIndex = Find_Index_from_ALI_Received(DataIn.stringDataIn, objPortData);
    if (intBidIndex < 0) {ALIPort[intPortPair][intSideAorB].fIncrementConsecutiveBadRecordCounter(); return;}   
    iTableIndex = Find_Table_Index_from_Bid_Index(intBidIndex);
    if (iTableIndex < 0) { if (FindTelnoInDisconnectVector(CALLBACK, DataIn.stringDataIn, objALIData, objPortData)){return;} }
    if (iTableIndex < 0) { if (FindTelnoInDisconnectVector(PSUEDO_ANI, DataIn.stringDataIn, objALIData, objPortData)){return;} }
    if (iTableIndex < 0) {SendUnMatchedTransactionID(intPortPair, intBidIndex, DataIn, objPortData); return;}    
   }

  objALIData.ALIData.intALIBidIndex  = intBidIndex = vALIworkTable[iTableIndex].ALIData.intALIBidIndex;
 

 // check for special case NPD[0]-000-0000 telephone number and replace string with correct number
 if ((objALIData.ALIData.stringType == "9")&&(vALIworkTable[iTableIndex].ALIData.boolALI_Test_Key_Was_Bid))
  {
   DataIn.stringDataIn = vALIworkTable[iTableIndex].fFindZeroTelno(DataIn.stringDataIn);
   boolRebidNotFound = false;
  }


 //ensure telno from data matches telno from record ................
 if   ((boolVERIFY_ALI_RECORDS)&&((!vALIworkTable[iTableIndex].fFindTelephoneNumber(DataIn.stringDataIn) ) && (!vALIworkTable[iTableIndex].fFindpANI(DataIn.stringDataIn))))
  {
   // phone number not found in ALI record
   vALIworkTable[iTableIndex].ALIData.eJSONEventCode = ALI_ERROR;
   objCallData.fLoadCallbackNumber( char2int(vALIworkTable[iTableIndex].CallData.stringTenDigitPhoneNumber.c_str()));
   objMessage.fMessage_Create(LOG_WARNING,232, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                              vALIworkTable[iTableIndex].ALIData, ALI_MESSAGE_232, DataIn.stringDataIn, "","","","","",  NORMAL_MSG, ALI_RECORD);
   enQueue_Message(ALI,objMessage);
   return;
  }

 // differentiate by type of bid for logging purposes 
 switch  (vALIworkTable[iTableIndex].ALIData.enumALIBidType)
  {
   case NO_BID:  case H_BEAT:  case LATE_RECORD: 
         strMessage =  ALI_MESSAGE_265; 
         iLogcode= 265; 
         SendCodingError("ali.cpp - Coding Error in Process_ALI_Response_Received() bid type = "+int2str(vALIworkTable[iTableIndex].ALIData.enumALIBidType)); 
         break;
   case INITIAL: strMessage =  ALI_MESSAGE_226; iLogcode = 226; vALIworkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED; break;
   case REBID:   strMessage =  ALI_MESSAGE_266; iLogcode = 266; vALIworkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED; break;
   case MANUAL:  strMessage =  ALI_MESSAGE_267; iLogcode = 267; vALIworkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_MAN_ALI_RECEIVED; break; 
   case AUTO:    strMessage =  ALI_MESSAGE_268; iLogcode = 268; vALIworkTable[iTableIndex].ALIData.intALIState = XML_STATUS_CODE_AUTO_ALI_RECEIVED; break;
  }
 // Log Data may need to move lower .....
// objMessage.fMessage_Create(LOG_CONSOLE_FILE, iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objPortData, objBLANK_FREESWITCH_DATA,
//                            vALIworkTable[iTableIndex].ALIData,strMessage, DataIn.stringDataIn, "", "", "", "", "",  NORMAL_MSG, ALI_RECORD);
// objALIData.stringTimeOfEvent = objMessage.stringTimeStamp;
// enQueue_Message(ALI,objMessage);


 vALIworkTable[iTableIndex].ALIData.eJSONEventCode = ALI_RECEIVED;
 objALIData                                        = vALIworkTable[iTableIndex];
 objALIData.enumThreadSending                      = ALI;
 objALIData.ALIData.boolALIRecordReceived          = true;
 if (DataIn.stringDataIn.length() < 2) {objALIData.ALIData.stringAliText.assign("ERROR IN ALI DATA");} 
 else                                  {objALIData.ALIData.stringAliText.assign(DataIn.stringDataIn,1,DataIn.intLength-2);}
 objALIData.fSet_Trunk(vALIworkTable[iTableIndex].CallData.intTrunk);
 objALIData.ALIData.intLastDatabaseBid             = vALIworkTable[iTableIndex].ALIData.intLastDatabaseBid;
 objALIData.ALIData.bool_LF_AS_CR                  = ALI_Steering[objALIData.ALIData.intLastDatabaseBid ].boolTreatLinefeedasCR;
 objALIData.fLoadCallbackfromALI();
 objALIData.fLoad_ALI_Type(DataIn.stringDataIn);                  

// objALIData.ALIData.stringAliText.erase(objALIData.ALIData.stringAliText.end()-1);
 
 // reset heartbeat time on port
 clock_gettime(CLOCK_REALTIME, &ALIPort[intPortPair][intSideAorB].timespecTimeLastHeartbeat);
// ALIPort[intPortPair][intSideAorB].boolHeartbeatActive   = false; 
 ALIPort[intPortPair][intSideAorB].intConsecutiveNAKCounter = 0;
 ALIPort[intPortPair][intSideAorB].intConsecutiveBadRecordCounter = 0;

 // Log Data ... Moved Logging here .....
 objMessage.fMessage_Create(LOG_CONSOLE_FILE, iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA, 
                            objPortData, objBLANK_FREESWITCH_DATA, objALIData.ALIData, strMessage, DataIn.stringDataIn, "", "", "", "", "",  NORMAL_MSG, ALI_RECORD);
 objALIData.stringTimeOfEvent = objMessage.stringTimeStamp;
 enQueue_Message(ALI,objMessage);


 // push ALI Record onto main Q
 objQALIData = objALIData;

 enqueue_Main_Input(ALI,objQALIData);
// sem_wait(&sem_tMutexMainInputQ);
// queue_objMainData.push(objALIData);
// sem_post(&sem_tMutexMainInputQ);

 // if "record not found" rebid it if able on another Database
 // but do not retransmit on same Database.

/*
 if ((objALIData.ALIData.stringType == "9")&&(boolRebidNotFound))
  {
   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
   vALIworkTable[iTableIndex].ALIData.enumALIBidType = REBID;
   vALIworkTable[iTableIndex].ALIData.boolALIRecordRetransmit[1] = false;
   vALIworkTable[iTableIndex].ALIData.boolALIRecordRetransmit[2] = false;
   if (Rebid_ALI_Record(iTableIndex, timespecTimeNow)) {return;} 
  }
*/
 vALIworkTable.erase(vALIworkTable.begin()+iTableIndex);

 return; 

}
/****************************************************************************************************
*
* Name:
*  Function: void ALI_ScoreBoard_Event(union sigval union_sigvalArg)
*
* Description:
*   A Timer Event Thread within Ali_Thread() a pointer to the ALI Worktable is passed as its argument
*
*
* Details:
*   This function is an Timed Event thread within Ali_Thread. The function conducts primary oversight of the state of
*   the ALI Work Table and the ALI Ports. 
*
*    When fired it:
*    1. Disables the timer
*    2. Locks the WorkTable
*    3. Checks the ports to see if heartbeats need to be sent ->   Check_ALI_Heartbeat_Interval()
*    4. Checks the ports to see if heartbeats have timed out  ->   Check_ALI_Heartbeat_Timeout()
*    5. Checks to see if Records have timed out               ->   Check_ALI_Record_Timeout()
*    6. Checks to see if port down reminders need to be sent  ->   Check_ALI_Port_Down_Notification()
*    7. Checks if Ports can be restarted (after a shutdown)   ->   .Check_Port_Restart()
*    8. Checks/ erases stale data in the UDP Port buffer      ->   .UDP_Port.fCheck_Buffer()
*   10. Re-Arms the timer
*
*
* Parameters: none				
*   
*					
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALIPort[][]                         Global  - <ExperientCommPort> object 2d array
*   boolDEBUG                           Global  - <globals.h>
*   intMAX_ALI_PORT_PAIRS               Global  - <defines.h>
*   intRC                               Local   - return code
*   itimerspecALIScoreboardDelay        Global  - <ctime> struct itimerspec
*   itimerspecDisableTimer              Global  - <ctime> struct itimerspec
*   sem_tMutexAliWorkTable              Global  - <globals.h> <csignal> sem_t                          
*   timer_tALIScoreboardTimerId         Global  - <ctime> timer_t
*
*                                                                          
* Functions: 
*   Check_ALI_Heartbeat_Interval()      Local                                           (void)
*   Check_ALI_Heartbeat_Timeout()       Local                                           (void)
*   Check_ALI_Record_Timeout()          Local                                           (void)
*   Check_ALI_Port_Down_Notification()  Local                                           (void)
*   .Check_Port_Restart()               Global  <ExperientCommPort>                     (void)                    
*   .fCheck_Buffer()                    Global  <ExperientCommPort><ExperientUDPPort>   (void)
*   Semaphore_Error()	                Global  <globalfunctions.h>                     (void)
*   sem_post()                          Library <csignal>                               (int)
*   sem_wait()                          Library <csignal>                               (int)
*   timer_settime()                     Library - <ctime>                               (int)
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *ALI_ScoreBoard_Event(void *voidArg)
{
 int                    intRC;
 struct timespec	timespecTimeNow;
 MessageClass		objMessage;
 extern ExperientCommPort  ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1]; 
 
do
{

 experient_nanosleep(intALI_CLEANUP_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}
 
 Refresh_Disconnect_DataList();
/*
 if(!boolIsMaster)
  {
   //reset hearbeats and return
   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
   for (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++) {
                                                     for (int j = 1; j <= 2; j++)
                                                      {
                                                       ALIPort[i][j].fClearReducedHeartbeatMode(true);
                                                       ALIPort[i][j].timespecTimeLastHeartbeat.tv_sec = timespecTimeNow.tv_sec;
                                                       ALIPort[i][j].timespecTimeLastHeartbeat.tv_nsec = timespecTimeNow.tv_nsec;
                                                      }//end for (int j = 1; j <= 2; j++)
                                                    }// end for  (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++)
   return;

  }
								
 // disable timer
 if(boolSTOP_EXECUTION) {return;}
 //else                   {timer_settime(timer_tALIScoreboardTimerId, 0, &itimerspecDisableTimer, NULL);}
*/

 // Lock Work Table
 intRC = sem_wait(&sem_tMutexAliWorkTable);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliWorkTable, "sem_wait@sem_tMutexAliWorkTable in ALI_ScoreBoard_Event", 1);}


 Check_ALI_Heartbeat_Interval();
 Check_ALI_Service_Heartbeat_Interval();
 Check_ALI_Heartbeat_Timeout();
 Check_ALI_Service_Heartbeat_Timeout();
 Check_ALI_Record_Timeout();


 // UnLock Work Table  
 sem_post(&sem_tMutexAliWorkTable);
 

 Check_ALI_Port_Down_Notification();
 CheckALIServiceDownNotification();
 
 // check for stale data in ALI Ports
 for (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++) {for (int j = 1; j <= 2; j++)
  {
   ALIPort[i][j].Check_Port_Restart();         
 
   switch (ALIPort[i][j].ePortConnectionType) {
     case TCP:
          switch (ALIPort[i][j].eALIportProtocol) {
            case ALI_E2:
                        sem_wait(&ALIPort[i][j].TCP_Port.sem_tMutexTCPPortBuffer);
                        ALIPort[i][j].TCP_Port.fProcessE2TCPbuffer();
                        sem_post(&ALIPort[i][j].TCP_Port.sem_tMutexTCPPortBuffer);
                        break;
            default:
                        sem_wait(&ALIPort[i][j].TCP_Port.sem_tMutexTCPPortBuffer);
                      //  ALIPort[i][j].TCP_Port.fProcessALITCPbuffer();
                        sem_post(&ALIPort[i][j].TCP_Port.sem_tMutexTCPPortBuffer);
                        break;
           }
          
     case UDP: 
          ALIPort[i][j].UDP_Port.fCheck_Buffer();
          break;
     default: 
          SendCodingError("ali.cpp *ALI_ScoreBoard_Event() -NO_CONNECTION_TYPE");
          break;
    }



  }}
/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tALIScoreboardTimerId, 0, &itimerspecALIScoreboardDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);
 
 objMessage.fMessage_Create(0, 294, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_294);
 objMessage.fConsole();

return NULL;	
								
}// end ALI_ScoreBoard_Event

/****************************************************************************************************
*
* Name:
*  Function: void ALI_Messaging_Monitor_Event()
*
*
* Description:
*   An event handler within ALI_Thread() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from ALI.  When activated it
*
*   1. Checks for messages on the message Q, if none then exit.
*
*   2. retrieves messages until Q is empty and processes them (send to main Q).
*
*
* Parameters: 				
*   union_sigvalArg			Unused
*
* Variables:
*   CLOCK_REALTIME                      Global  - <ctime> constant
*   intRC				Local   - return code
*   itimerspecALiHealthMonitorDelay     Local   - <ctime> struct which contains timing data that for the timer
*   itimerspecDisableTimer              Global  - <ctime> struct which contains data that disables the timer
*   objMessage                          Local   - <MessageClass> object
*   queue_objALiMessageQ                Global  - <queue><MessageClass> queue of ALI messages
*   sem_tMutexALiMessageQ               Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   sem_tMutexMainMessageQ		Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   timer_tALIHealthMonitorTimerId      Global  - <ctime> timer id for this function
*                                                                       
* Functions:  
*   .empty()				Library <queue>                 (bool)
*   .front()				Library <queue>                 (*this)
*   .pop()				Library <queue>                 (void)
*   .push()				Library <queue>                 (void)
*   Semaphore_Error()                   Global  <globalfunctions.h>     (void)
*   sem_wait()             		Library <semaphore.h>           (int)
*   sem_post()				Library <semaphore.h>           (int)
*   timer_settime()                     Library <ctime>                 (int)
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *ALI_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 Thread_Data            	ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="ALI Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ali.cpp::ALI_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);


/*
 //Disable Timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tALIHealthMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}
*/


do
{

 experient_nanosleep(intALI_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in ALI_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexAliMessageQ);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliMessageQ, "sem_wait@sem_tMutexAliMessageQ in ALI_Messaging_Monitor_Event", 1);}

 while (!queue_objALiMessageQ.empty())
  {
    objMessage = queue_objALiMessageQ.front();
    queue_objALiMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;
      case 200:						
       // "[DEBUG] ALI Thread Created, PID %%% ThreadSelf %%%" 
       break; 
      case 201:						
       // ALI Thread Failed to Initialize, System Rebooting" should not pass through here.......
       break;
      case 202:						
       // "[DEBUG] ALI Health Monitor Timer Event Initialized"
       break;
      case 203:
       // "[WARNING] ALI Thread Signalling Semaphore failed to Lock"						
       break;
      case 204:						
       // "[WARNING] ALI ANI-Key %%% Not Contained In Any Database, Unable To Send ALI Request"
       break;
      case 205:						
       // ""[DEBUG] ALI %%% %%% Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 206:						
       //  "[WARNING] ALI Mutex Semaphore Error (Code=%%%, MSG=%%%)"
       break;
      case 207:
       // "[WARNING] ALI %%% %%% < NAK Recieved"						
       break;
      case 208:						
       // "[WARNING] ALI %%% %%% - Begin Reduced Heartbeat Interval; Interval=%%% (Ping %%%)"
       break;
      case 209:
       // "ALI %%% %%% < ACK Received"						
       break;
      case 210:						
       // "[INFO] ALI %%% %%% - First ACK Received"
       break;
      case 211:						
       // "[WARNING] ALI %%% %%% - End Reduced Heartbeat Interval"
       break;
      case 212:						
       // "[WARNING] ALI %%% %%% - Heartbeat Send Failure"
       break;
      case 213:						
       // "ALI %%% %%% > Heartbeat Sent"
       break;
      case 214:                                           
       // "[DEBUG] ALI ScoreBoard Timer Event Initialized"
       break;
      case 215:                                           
       // "[WARNING] ALI %%% %%% Unexpected Character Count %%%"
       break;
      case 216:
       // "[WARNING] ALI %%% %%% Invalid ALI Data Received, /START OF SAMPLE/%%%/END OF SAMPLE/"
       break;
      case 217:						
       // "[WARNING] ALI %%% %%% < Response Received too Long, Length = (%%%) /START OF SAMPLE/%%%/END OF SAMPLE/"
       break;
      case 218:						
       // "[WARNING] ALI %%% %%% < Invalid Data Received for %%%, Data=\"%%%\""
       break;
      case 219:						
       // "[WARNING] ALI %%% %%% < Telephone Number %%% Not Found for Trunk %%%, Data=\"%%%\"
       break;
      case 220:                                           
       // "[WARNING] ALI %%% %%% - Record Timeout (Trunk=%%%, Phone=%%%)"
       break;
      case 221:                                           
       // 
       break;
      case 222:						
       // "ALI %%% %%% > Request Sent to Database=%%%; RequestKey=%%%"
       break;
      case 223:						
       // "[ALARM] ALI %%% %%% - Port Down for %%%"
       break;
      case 224:						
       // "[WARNING] ALI %%% %%% - Reminder - Port Down for %%%" 
       break;
      case 225:                                           
       // "[WARNING] ALI No Port Pairs Available for Database=%%%; RequestKey=%%%"
       break;
      case 226:                                           
       //  "ALI %%% %%% < Data Recieved: %%%"
       break;
      case 227:                                           
       // "[ALARM] %%% - Port Restored"
       break;
      case 228:                                           
       // "[WARNING] ALI %%% %%% < Heartbeat ACK Timeout"
       break;
      case 229:                                           
       // "[WARNING] ALI %%% %%% < Broadcast Message Recieved: %%%"
       break;
      case 230:                                           
       // "[ALARM] ALI %%% %%% < Broadcast Message Host Going Out of Service"
       break;
      case 231:                                           
       // "[WARNING] ALI %%% %%% > ALI Request Retransmited; RequestKey=%%%"
       break;
      case 232:
       // "[WARNING] ALI %%% %%% < Extra Data Recieved:%%%"
       break;
      case 233:                                           
       // "[WARNING] %%% - UDP Error (Code=%%%, MSG=%%%)"
       break;
      case 234:                                           
       // "[WARNING] %%% - Port Restart Failed (Code=%%%)"
       break;
      case 235:                                           
       // "[WARNING] %%% - Port Data Reception Restored"
       break;
      case 236:                                           
       // "[ALARM] %%% - Port Data Reception Inhibited for %%% String Buffer Size > %%%"
       break;
      case 237:
       // "[WARNING] ALI %%% %%% - Unexpected Data Received from Outside IP Address %%%"
       break;
      case 238:
       // "[WARNING] ALI - Error creating checksum: ALI Request key length = %%%"
       break;
      case 239:
       // "[WARNING] ALI - Port Listen Thread Failed to Initialize, System Rebooting"
       break;
      case 240:
       // "[WARNING] ALI %%% %%% < Unexpected Data Recieved:%%%"
        break;
      case 241:
       //"[WARNING] %%% %%% %%% - Buffer Cleared Length = %%%"
       break;
      case 242:
       // "[WARNING] %%% - %%% Count Rollover=%%%"
       break;
      case 243:
       // "[WARNING] %%% - %%% Count Rollover=%%%"
       break;
      case 244:
       // "[WARNING] ALI %%% %%% < Consecutive NAK(s) over Past 24 Hours = %%%"
       break;
      case 245:
       //  "[WARNING] ALI %%% %%% < Stray ACK(s) over Past 24 Hours = %%%"
       break;
      case 246:
       // "[WARNING] ALI %%% %%% < Stray ACK Received"
       break;
      case 247:
       // "[WARNING] ALI %%% %%% < %%% Stray ACKs Received Between Valid ACKs"
       break;
      case 248:
       //  "[WARNING] Unable to set CPU affinity for ALI Thread"
       break;
      case 249:
       // Consecutive Invalid ALI Records Counter
       break;
      case 250:
       //  Discarded Packet Counter Rollover
       break;
      case 251:
       // "[WARNING] %%% - Incoming Network Packets Discarded over Past 24 Hours = %%%" 
       break;
      case 252:
       // "ALI %%% < Raw Data Received from IP=%%% (Data Follows) %%%"
       break;
      case 253:
       // "%%% > Data sent to IP=%%% (Data Follows)"
       break;
      case 254:
       // ">[WARNING] Configuration Error: ALI Database %%% Request Key Digits=%%%" 
       break;
      case 255:
       // ">[WARNING] ALI Record Fail on Database=%%%, Retry on Database=%%%" 
       break;
      case 256:
       // special rebid Tracker to main to count rebids
       break;
     case 257:
       //">[WARNING] Error Decoding ALI Record ; Message=%%%"
       break;
     case 258:
       //"">[WARNING] ALI E2 Record XML Field %%% Truncated; Length=%%%; Max=%%%"
       break;
     case 259:
       // ">[WARNING] ALI E2 Record Unable to Parse XML; Error=%%% (Data Follows)"
       break;
     case 260:
       // ">[WARNING] ALI Record Fail; No index available to Bid"
       break;
     case 261:
       // "-[WARNING] Area Code %%% not found in NPD Table, System Will Bid Test Key"
       break;  
     case 262: 
       // "[WARNING] ALI Bid Failed for %%%"
       break;
     case 263:
       // "[ALARM] ALI Port Pair %%% Down"
       break;
     case 264:
       // "[ALARM] ALI Port Pair %%% Restored"
       break;
     case 265:
       // 
       break;
     case 266:
       // 
       break;
     case 267:
       // 
       break;
     case 268:
       // 
       break;
     case 269:
       // 
       break;
     case 270:
       // 
       break;
     case 271:
       // 
       break;
     case 272:
        break;
     case 273:
        break;
     case 274:
        break;
     case 275:
        break;
     case 276:
        break;
     case 291:
//       "-[INFO] No Changes to ALI Global Variables"
        break;
    case 292:
//      "-[INFO] Reconfiguring ALI Global Variables"
        break;
    case 293:
//      "-[INFO] Reconfiguring ALI Global Variables"
        break;
   case 294:
//      "-ALI Scoreboard Event Thread Complete"
        break;
   case 295:
//     "-[WARNING] Scoreboard Thread Failed to Initialize, System Rebooting"
        break;
   case 296:
//     "-[WARNING] Scoreboard Thread Failed to Initialize, System Rebooting"
        break;
   case 297:
//     "-ALI Messaging Event Thread Complete"
        break;
     case 298:
       //  "-[WARNING] Mutex Semaphore Error (Code=%%%, MSG=%%%)"
       break;
     case 299:                                           
      // thread complete     
      break;
      
     case 999:
      // test area..................................
             
      break;	
     default:
       // used to find uncoded messages
       cout << objMessage.intMessageCode << " Message Code Unrecognized in ALI Monitor" << endl;
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock ALi Message Q then Main Message Q
 sem_post(&sem_tMutexAliMessageQ);
 sem_post(&sem_tMutexMainMessageQ);
/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tALIHealthMonitorTimerId, 0, &itimerspecALIHealthMonitorDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);
 
 objMessage.fMessage_Create(0, 297, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT,
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_297);
 objMessage.fConsole();

return NULL;	


}// end ALI_Messaging_Monitor_Event()

/****************************************************************************************************
*
* Name:
*  Function: void *Ali_Port_A_Listen_Thread(void *voidArg)
*
*
* Description:
*   An thread within Ali_Thread() which receives the port number as it's argument 
*
*
* Details:
*   This function is an implemented thread from Ali_Thread. It's main purpose is to listen on a UDP Port
*   defined by the ALI thread. When activated it runs the Do_Events() function from the ipworks library until the boolean
*   boolSTOP_EXECUTION is true.
*
*
* Parameters: 				
*   voidArg			        a (recast) integer representing the port number 
*					
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_205                     Global  - <defines.h> Listen Thread Created output message
*   ALIPort[][]                         Global  - <ExperientCommPort> object 2d array
*   boolDEBUG                           Global  - <globals.h>
*   .boolReadyToShutDown                Global  - <ExperientCommPort> member
*   intPortPair				Local   - port number to listen to
*   LOG_CONSOLE_FILE                    Global  - <defines.h> Log code
*   objMessage                          Local   - <MessageClass> object
*   .UDP_Port                           Global  - <ExperientCommPort> <ExperientUDPPort> member object
*                                                                          
* Functions: 
*   .DoEvents()                         Library - <ipworks><UDPPort>              (int)
*   getpid()                            Library - <unistd.h>                      (pid_t)           
*   int2strLZ()				Global  - <globalfunctions.h>             (string)
*   pthread_self()                      Library - <pthread.h>                     (pthread_t)                    
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *Ali_Port_A_Listen_Thread(void *voidArg)
{
 // re-cast voidArg
 param_t* p           = (param_t*) voidArg;
 int      intPortPair = p->intPassedIn;
 int				intRC;
 MessageClass           	objMessage;
 Port_Data              	objPortData;
 Thread_Data                    ThreadDataObj; 
     
 extern vector <Thread_Data> 			ThreadData;
 extern Initialize_Global_Variables             INIT_VARS;

 ThreadDataObj.strThreadName ="ALI Port A "+ int2strLZ(intPortPair);
// ThreadDataObj.ThreadPID     = getpid();
 //ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ali.cpp::Ali_Port_A_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);
 objPortData.fLoadPortData(ALI, intPortPair, 1);
 objMessage.fMessage_Create(0, 205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,objPortData, objBLANK_FREESWITCH_DATA,
                            objBLANK_ALI_DATA, ALI_MESSAGE_205, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 while ((!boolSTOP_EXECUTION)&&(!INIT_VARS.ALIinitVariable.boolRestartALILegacyPorts))
  {
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
   switch(ALIPort[intPortPair][1].ePortConnectionType)
    {
     case UDP: ALIPort[intPortPair][1].UDP_Port.DoEvents(); break;
     case TCP:
      
       ALIPort[intPortPair][1].TCP_Port.DoEvents();

       if (!ALIPort[intPortPair][1].TCP_Port.GetConnected())
        {
          ALIPort[intPortPair][1].TCP_Port.boolIntialConnect = false;
         sleep(8);
 //       cout << "ALI " << int2strLZ(intPortPair) << " A connecting to " << ALIPort[intPortPair][1].TCP_Port.GetRemoteHost() << " Port " << ALIPort[intPortPair][1].TCP_Port.GetRemotePort() << endl;
         ALIPort[intPortPair][1].TCP_Port.ConnectToALIServer();
        }
       else { ALIPort[intPortPair][1].TCP_Port.boolIntialConnect = true;}      
       break; 
     default:
             SendCodingError("ali.cpp - coding error in  void *Ali_Port_A_Listen_Thread(void *voidArg)");
    }     
  } // end  while (!boolSTOP_EXECUTION)

 objMessage.fMessage_Create(0, 205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_205b, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 ALIPort[intPortPair][1].UDP_Port.SetActive(false); 
 ALIPort[intPortPair][1].TCP_Port.Disconnect();
 ALIPort[intPortPair][1].boolReadyToShutDown = true;
 return NULL;
								
}// end Ali_Port_A_Listen_Thread

/****************************************************************************************************
*
* Name:
*  Function: void *Ali_Port_B_Listen_Thread()
*
*
* Description:
*   An thread within Ali_Thread() which receives the port number as it's argument 
*
*
* Details:
*   This function is an implemented thread from Ali_Thread. It's main purpose is to listen on a UDP Port
*   defined by the ALI thread. When activated it runs the Do_Events() function from the ipworks library until the boolean
*   boolSTOP_EXECUTION is true.
*
*
* Parameters: 				
*   voidArg			        a (recast) integer representing the port number 
*					
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_205                     Global  - <defines.h> Listen Thread Created output message
*   ALIPort[][]                         Global  - <ExperientCommPort> object 2d array
*   boolDEBUG                           Global  - <globals.h>
*   .boolReadyToShutDown                Global  - <ExperientCommPort> member
*   intPortPair				Local   - port number to listen to
*   LOG_CONSOLE_FILE                    Global  - <defines.h> Log code
*   objMessage                          Local   - <MessageClass> object
*   .UDP_Port                           Global  - <ExperientCommPort> <ExperientUDPPort> member object
*                                                                          
* Functions: 
*   .DoEvents()                         Library - <ipworks><UDPPort>              (int)
*   getpid()                            Library - <unistd.h>                      (pid_t)           
*   int2strLZ()				Global  - <globalfunctions.h>             (string)
*   pthread_self()                      Library - <pthread.h>                     (pthread_t)                    
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *Ali_Port_B_Listen_Thread(void *voidArg)
{
 // cast voidArg
 param_t* p           = (param_t*) voidArg;
 int      intPortPair = p->intPassedIn;
 int				intRC;
 MessageClass           	objMessage;
 Port_Data              	objPortData;
 Thread_Data                    ThreadDataObj; 
     
 extern vector <Thread_Data> 			ThreadData;
 extern Initialize_Global_Variables             INIT_VARS;

 ThreadDataObj.strThreadName ="ALI Port B " + int2strLZ(intPortPair);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ali.cpp::Ali_Port_B_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 objPortData.fLoadPortData(ALI, intPortPair, 2);
 objMessage.fMessage_Create(0, 205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_205, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 while ((!boolSTOP_EXECUTION)&&(!INIT_VARS.ALIinitVariable.boolRestartALILegacyPorts))
  {
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
   switch(ALIPort[intPortPair][2].ePortConnectionType)
    {
     case UDP: ALIPort[intPortPair][2].UDP_Port.DoEvents(); break;
     case TCP:
       ALIPort[intPortPair][2].TCP_Port.DoEvents();

      if (!ALIPort[intPortPair][2].TCP_Port.GetConnected())
        {
          ALIPort[intPortPair][2].TCP_Port.boolIntialConnect = false;
         sleep(8);
 //       cout << "ALI " << int2strLZ(intPortPair) << " B connecting ......" << endl;
        ALIPort[intPortPair][2].TCP_Port.ConnectToALIServer();
        }
       else { ALIPort[intPortPair][2].TCP_Port.boolIntialConnect = true;}
       break; 
     default:
             SendCodingError("ali.cpp - coding error in  void *Ali_Port_B_Listen_Thread(void *voidArg)");
    }     
  } // end  while (!boolSTOP_EXECUTION)

 objMessage.fMessage_Create(0, 205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_205b, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 


 ALIPort[intPortPair][2].UDP_Port.SetActive(false); 
 ALIPort[intPortPair][2].TCP_Port.Disconnect();
 ALIPort[intPortPair][2].boolReadyToShutDown = true;
 return NULL;
								
}// end Ali_Port_B_Listen_Thread
/****************************************************************************************************
*
* Name:
*  Function: void *Ali_Service_Port_Listen_Thread()
*
*
* Description:
*   An thread within Ali_Thread() which receives the port number as it's argument 
*
*
* Details:
*   This function is an implemented thread from Ali_Thread. It's main purpose is to listen on a Port
*   defined by the ALI thread. When activated it runs the Do_Events() function from the ipworks library until the boolean
*   boolSTOP_EXECUTION is true.
*
*
* Parameters: 				
*   voidArg			        a (recast) integer representing the port number 
*					
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_205                     Global  - <defines.h> Listen Thread Created output message
*   ALIPort[][]                         Global  - <ExperientCommPort> object 2d array
*   boolDEBUG                           Global  - <globals.h>
*   .boolReadyToShutDown                Global  - <ExperientCommPort> member
*   intPortPair				Local   - port number to listen to
*   LOG_CONSOLE_FILE                    Global  - <defines.h> Log code
*   objMessage                          Local   - <MessageClass> object
*   .UDP_Port                           Global  - <ExperientCommPort> <ExperientUDPPort> member object
*                                                                          
* Functions: 
*   .DoEvents()                         Library - <ipworks><UDPPort>              (int)
*   getpid()                            Library - <unistd.h>                      (pid_t)           
*   int2strLZ()				Global  - <globalfunctions.h>             (string)
*   pthread_self()                      Library - <pthread.h>                     (pthread_t)                    
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *Ali_Service_Port_Listen_Thread(void *voidArg)
{
 // cast voidArg
 param_t* p           = (param_t*) voidArg;
 int      intPortPair = p->intPassedIn;
 int				intRC;
 MessageClass           	objMessage;
 Port_Data              	objPortData;
 Thread_Data                    ThreadDataObj; 
     
 extern vector <Thread_Data> 			ThreadData;
 extern Initialize_Global_Variables             INIT_VARS;

 ThreadDataObj.strThreadName ="ALI SVC Port " + int2strLZ(intPortPair);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in ali.cpp::Ali_Service_Port_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 objPortData.fLoadPortData(ALI, intPortPair, 3);
objMessage.fMessage_Create(0, 205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_205, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 while (!boolSTOP_EXECUTION)
  {
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
   if (INIT_VARS.ALIinitVariable.boolRestartALIservicePorts) {break;}
   switch(ALIServicePort[intPortPair].ePortConnectionType)
    {
     case UDP: ALIServicePort[intPortPair].UDP_Port.DoEvents(); break;
     case TCP:
      if (boolSTOP_EXECUTION) {break;}
      if (!ALIServicePort[intPortPair].TCP_Port.GetConnected())
        {
         ALIServicePort[intPortPair].TCP_Port.boolIntialConnect = false;
         ALIServicePort[intPortPair].TCP_Port.ConnectToALIServer();
         if ((boolSTOP_EXECUTION)||(INIT_VARS.ALIinitVariable.boolRestartALIservicePorts)) {break;}
         for (int i=1; i<=8;i++){sleep(1); if((boolSTOP_EXECUTION)||(INIT_VARS.ALIinitVariable.boolRestartALIservicePorts)){break;}}
         
        }
       else { ALIServicePort[intPortPair].TCP_Port.boolIntialConnect = true;}
       ALIServicePort[intPortPair].TCP_Port.DoEvents();
       break; 
     default:
             SendCodingError("ali.cpp - coding error in  void *Ali_Service_Port_Listen_Thread(void *voidArg)");
    }     
  } // end  while (!boolSTOP_EXECUTION)

objMessage.fMessage_Create(0, 205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_205b, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 ALIServicePort[intPortPair].UDP_Port.SetActive(false); 
 ALIServicePort[intPortPair].TCP_Port.Disconnect();
 ALIServicePort[intPortPair].boolReadyToShutDown = true;
 return NULL;
								
}// end Ali_Service_Port_Listen_Thread


/****************************************************************************************************
*
* Name:
*  Function: void ALI_NAK_Received( int intPortPair, int intSideAorB, int intTrunk) 
*
*
* Description:
*   A function called from within ALI_Thread() 
*
*
* Details:
*   This function is called if a ALI NAK is received and accomplishes the following:
*          
*    1. Send NAK message 
*    2. Increment NAK counter
*    3. Reset Heartbeat Time
*    4. Check for 3 consecutive NAK's on Port and enter reduced HB Mode if true
*        
*       note: reduced Heartbeat Mode in ALI sets a reduced Heartbeat Interval and prevents Requests from being made on the port
*             until an ACK is recieved on the port.             
*
* Parameters: 				
*   intPortPair				        integer Port Pair receiving NAK
*   intSideAorB                                 integer Port Side (1 or 2) receiving NAK
*
* Variables:
*   ALI                                         Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_207                             Global  - <defines.h> NAK Recieved output message
*   ALI_MESSAGE_208                             Global  - <defines.h> Begin Reduced Heartbeat Interval output message
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member.. true if reduced heartbeat mode active on port[][]
*   ALIPort[][]                                 Global  - <ExperientCommPort> object array 
*   CLOCK_REALTIME                              Library - <ctime>
*   .intConsecutiveNAKCounter                   Global  - <ExperientCommPort> member
*   .intHeartbeatInterval                       Global  - <ExperientCommPort> member.. current heartbeat interval on port[][]
*   intLogCode                                  Local   - integer code to log thread
*   intREDUCED_ALI_HEARTBEAT_INTERVAL_SEC       Global  - <defines.h>
*   LOG_CONSOLE_FILE                            Global  - <defines.h> code to display to console, file to disk
*   LOG_WARNING                                 Global  - <defines.h> code to email warning, display to console, file to disk
*   objMessage				        Local   - <MessageClass> object
*   stringSideAorB                              Local   - <cstring> ALI port pair side "A" or Side "B"
*   .timespecTimeLastHeartbeat                  Global  - <ExperientDataClass><ctime> struct timespecmember.. time of last heartbeat TX or ACK or NAK on port[][]
*   timespecTimeNow                             Local   - <ctime> <timespec> current time data
*                                                                          
* Functions:
*   clock_gettime()                     Library <ctime>                         (int)                
*   enQueue_Message()		        Global  <globalfunctions.h>             (void)
*   .fIncrementNAKCounter()             Global  <ExperientCommPort>             (void)
*   .fPingStatusString()                Global  <ExperientUDPPort>              (string)            
*   .fSetReducedHeartbeatMode()         Global  <ExperientCommPort>             (void)            
*   int2strLZ()			        Global  <globalfunctions.h>             (string)
*   .fMessage_Create()			Local   <MessageClass>                  (void)
*   seconds2time()                      Global  <globalfunctions.h>             (string)
*
*
* Author(s):
* AUTHOR: 2/2/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation 
****************************************************************************************************/
void ALI_NAK_Received( int intPortPair, int intSideAorB)              
{
 MessageClass		objMessage;
 struct timespec        timespecTimeNow;
 int                    intLogCode = LOG_WARNING;
 Port_Data              objPortData;

 objPortData.fLoadPortData(ALI,intPortPair,intSideAorB);
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 // ALI NAK Recieved Msg
 if (ALIPort[intPortPair][intSideAorB].boolReducedHeartBeatMode){intLogCode = LOG_CONSOLE_FILE * boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC;}
 if (intLogCode)
  {
   objMessage.fMessage_Create(intLogCode, 207, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_207); 
   enQueue_Message(ALI,objMessage);
  }

 // ALIPort[intPortPair][intSideAorB].boolNAKReceived = true;
 ALIPort[intPortPair][intSideAorB].fIncrementNAKCounter();

 if (ALIPort[intPortPair][intSideAorB].intConsecutiveNAKCounter > MAX_CONSECUTIVE_NAKS_FOR_PORT_SHUTDOWN)
  {ALIPort[intPortPair][intSideAorB].fEmergencyPortShutDown(objBLANK_CALL_RECORD, "Consecutive NAKs > ", MAX_CONSECUTIVE_NAKS_FOR_PORT_SHUTDOWN);}



 // reset HBeat time
 ALIPort[intPortPair] [intSideAorB].timespecTimeLastHeartbeat = timespecTimeNow;

 
 // 3 consecutive NAKs enter reduced Hearbeat Mode
 if (ALIPort[intPortPair][intSideAorB].intConsecutiveNAKCounter > 2)
  { 
   // don't set/send message if already in reduced HB mode....
   if (!ALIPort[intPortPair][intSideAorB].boolReducedHeartBeatMode)
    {
     ALIPort[intPortPair][intSideAorB].fSetReducedHeartbeatMode(objBLANK_CALL_RECORD,timespecTimeNow);
    }

  }// end if (ALIPort[intPortPair][intSideAorB].intConsecutiveNAKCounter > 2) 
  
}// end ALI_NAK_Received()

/****************************************************************************************************
*
* Name:
*  Function: void ALI_ACK_Received( int intPortPair, int intSideAorB)
*
*
* Description:
*   A function called from within ALI_Thread() 
*
*
* Details:
*   This function is called if an ACK is received on an ALI Port and accomplishes the following:
*          
*    1. send ACK message if configured 
*    2  check if this is first ACK /send info message if it is
*    3. set/reset heartbeat interval to normal on port
*    4. set/reset time of last heartbeat
*    5. reset consecutive NAK counter 
*
*
* Parameters: 				
*   intPortPair				int  	  Port pair receiving ACK
*   intSideAorB                         int       Port side (A or B) receiving ACK
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_209                     Global  - <defines.h> ACK Received output message
*   ALI_MESSAGE_210                     Global  - <defines.h> First ACK Received output message
*   ALI_MESSAGE_211                     Global  - <defines.h> End Reduced Heartbeat Interval output message
*   ALIPort[][]                         Global  - <ExperientCommPort> object 2D array
*   .boolHeartbeatActive             Global  - <ExperientCommPort> member
*   .boolFirstACKRcvd                   Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member.. true if reduced hearbeat interval activated on port
*   CLOCK_REALTIME                      Library - <ctime> constant
*   .intConsecutiveNAKCounter           Global  - <ExperientCommPort> member
*   LOG_CONSOLE_FILE                    Global  - <defines.h> code to display to console and file to disk
*   LOG_WARNING                         Global  - <defines.h> code to email, display to console and file to disk
*   objMessage                          Local   - <MessageClass> object
*   stringSideAorB                      Local   - <cstring> "A" or "B"
*   .timespecTimeLastHeartbeat          Global  - <ExperientCommPort> <ctime> <timespec> member
*                                                                       
* Functions:
*   Assign_ACK_To_Active_Record()       Local                                                   (bool)
*   clock_gettime()                     Library  - <ctime>                                      (int)
*   .fClearReducedHeartbeatMode()       Global   - <ExperientCommPort>                          (void)
*   enQueue_Message()                   Global   - <globalfunctions.h>                          (void)
*   .fMessage_Create()                  Global   - <MessageClass>                               (void)
*   int2strLZ()                         Global   - <globalfunctions.h>                          (string)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void ALI_ACK_Received( int intPortPair, int intSideAorB)
{ 
 MessageClass		objMessage;
 int                    intLogCode;
 Port_Data              objPortData;

 objPortData.fLoadPortData(ALI,intPortPair,intSideAorB);

 //cout << "ACK RECIEVED" << endl;


 // check for HB ACK
 if       (ALIPort[intPortPair][intSideAorB].boolHeartbeatActive)
            {intLogCode = LOG_CONSOLE_FILE*(boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC||ALIPort[intPortPair][intSideAorB].boolReducedHeartBeatMode);}
 // check for unexpected "stray" ACK
 else if  (!Assign_ACK_To_Active_Record(intPortPair,intSideAorB))
       {
        if(!ALIPort[intPortPair][intSideAorB].boolFirstStrayAck)
         {
          objMessage.fMessage_Create(LOG_WARNING,246, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_246);
          enQueue_Message(ALI,objMessage);
         }
        ALIPort[intPortPair][intSideAorB].boolFirstStrayAck = true;
        ALIPort[intPortPair][intSideAorB].fIncrementStrayACKCounter();
        if (ALIPort[intPortPair][intSideAorB].intStrayACKCounter > ALI_MAX_STRAY_ACKS_BETWEEN_HEARTBEATS)
         {
          ALIPort[intPortPair][intSideAorB].fEmergencyPortShutDown(objBLANK_CALL_RECORD, "Stray ACKs > ", ALI_MAX_STRAY_ACKS_BETWEEN_HEARTBEATS);
         }
        ALIPort[intPortPair][intSideAorB].intConsecutiveNAKCounter = 0;
        return;
       } 
 // it is a record ACK 
 else                                                    
       {intLogCode = LOG_CONSOLE_FILE;}
 
 
 objMessage.fMessage_Create(intLogCode,209, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_209,"","","","","","",DEBUG_MSG);
 enQueue_Message(ALI,objMessage);
 
 // display number of stray acks since last valid ack .. 
 if((ALIPort[intPortPair][intSideAorB].boolFirstStrayAck)&&(ALIPort[intPortPair][intSideAorB].intStrayACKCounter > 1))
  {
   objMessage.fMessage_Create(LOG_WARNING,247, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData,
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_247, int2strLZ(ALIPort[intPortPair][intSideAorB].intStrayACKCounter));
   enQueue_Message(ALI,objMessage);
  }


 ALIPort[intPortPair][intSideAorB].boolACKReceived = true;
 ALIPort[intPortPair][intSideAorB].boolFirstStrayAck = false;
 ALIPort[intPortPair][intSideAorB].intStrayACKCounter = 0;

 //first ALI ACK Message
 if (!ALIPort[intPortPair][intSideAorB].boolFirstACKRcvd)
  {
   ALIPort[intPortPair][intSideAorB].boolFirstACKRcvd = true;
   objMessage.fMessage_Create(LOG_INFO,210, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_210); 
   enQueue_Message(ALI,objMessage);
  }
   
 // set/reset Heartbeat interval on port [intPortNum] to normal
 if (ALIPort[intPortPair][intSideAorB].boolReducedHeartBeatMode)
  {
   objMessage.fMessage_Create(LOG_WARNING,211, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_211);
   enQueue_Message(ALI,objMessage);
  }
 ALIPort[intPortPair][intSideAorB].fClearReducedHeartbeatMode();
 ResetPortPairStatus(intPortPair);

 // reset heartbeat time on port
 ALIPort[intPortPair][intSideAorB].boolHeartbeatActive = false; 
 clock_gettime(CLOCK_REALTIME, &ALIPort[intPortPair][intSideAorB].timespecTimeLastHeartbeat);
 ALIPort[intPortPair][intSideAorB].intConsecutiveNAKCounter = 0;

}// end ALI_ACK_Received

/****************************************************************************************************
*
* Name:
*  Function: void Generate_Invalid_ALI_Data_Received_Message(int intPortPair, int intSideAorB, string stringDataIn )
*
*
* Description:
*  This function is implemented in  ALI_Thread()
*
*
* Details:
*  The function outputs an Unexpected Response error message to the Log thread if it is the first occurance 
*  after receiving good data. The "bad data" will be dispalyed up to intMAX_DATA_TO_DISPLAY number of characters.
*  ASCII special characters will be displayed <***> where *** is thier ASCII code.
*          
*    
* Parameters:
*   intPortPair                         int
*   intSideAorB                         int
*   stringDataIn                        <cstring>
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_216                     Global  - <defines.h> Unexpected Response Received output message
*   ALIPort[][]                         Global  - <ExperientCommPort> 2d array objects
*   boolDEBUG                           Global  - <globals.h> debug mode
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort>  member  
*   .boolUnexpectedDataShowMessage      Global  - <ExperientCommPort>  member
*   .intConsecutiveBadRecordCounter     Global  - <ExperientCommPort>  member  
*   intDataToDisplay                    Local   - length of data to display
*   intLogCode                          Local   - Code for Log thread
*   intMAX_DATA_TO_DISPLAY              Global  - <defines.h> Max string length to display to log
*   .intUnexpectedDataCharacterCount    Global  - <ExperientCommPort> member
*   LOG_WARNING                         Global  - <defines.h> Log Code
*   objMessage                          Local   - <MessageClass>
*   stringSideAorB                      Local   - <cstring>
*   stringTemp                          Local   - <cstring>

*                                                                          
* Functions:
*   ASCII_String()                      Global  - <globalfunctions.h>                   (string)
*   .length()                           Library - <cstring>                             (size_t)
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Generate_Invalid_ALI_Data_Received_Message(int intPortPair, int intSideAorB, string stringDataIn )
{
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
 int                            intDataToDisplay;
 int                            intLogCode;
 string                         stringTemp = "";
 string                         stringTemp2 = "";
 MessageClass                   objMessage;
 string                         stringSideAorB;
 Port_Data                      objPortData;



 if (boolIGNORE_BAD_ALI)         {return;}

 if (boolUSE_ALI_SERVICE_SERVER) {return;}

 objPortData.fLoadPortData(ALI, intPortPair, intSideAorB); 
 if (intSideAorB == 1){stringSideAorB = "A";}
 else                 {stringSideAorB = "B";} 




 ALIPort[intPortPair][intSideAorB].fIncrementBadCharacterCount(stringDataIn.length());

 if (stringDataIn.length() > intDEFAULT_ALI_NOISE_THRESHOLD)
  {ALIPort[intPortPair][intSideAorB].fIncrementConsecutiveBadRecordCounter();}

 
 if (ALIPort[intPortPair][intSideAorB].boolUnexpectedDataShowMessage)
  {
 
   if (stringDataIn.length() > intMAX_DATA_TO_DISPLAY) {intDataToDisplay = intMAX_DATA_TO_DISPLAY;}
   else                                                {intDataToDisplay = stringDataIn.length()-1;}

   stringTemp = stringDataIn.substr(0, intDataToDisplay);
   
   // show message under certain conditions
   if (ALIPort[intPortPair][intSideAorB].boolReducedHeartBeatMode) 
    {intLogCode = LOG_WARNING*ALIPort[intPortPair][intSideAorB].boolUnexpectedDataShowMessage*boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC;}
   else
    {intLogCode = LOG_WARNING*ALIPort[intPortPair][intSideAorB].boolUnexpectedDataShowMessage;}

   if (intLogCode)
    { 
     stringTemp =  ASCII_String(stringTemp.c_str(),stringTemp.length());       
     objMessage.fMessage_Create(intLogCode,216, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_216, stringTemp); 
     enQueue_Message(ALI,objMessage);

    }// end if (intLogCode) 

   // flag to keep from getting continuous messages ......
   ALIPort[intPortPair][intSideAorB].boolUnexpectedDataShowMessage = false;

  }//end if (ALIPort[intPortPair][intSideAorB].boolUnexpectedDataShowMessage) 

}// end void Generate_Invalid_ALI_Data_Received_Message()

/****************************************************************************************************
*
* Name:
*  Function: void Check_ALI_Heartbeat_Timeout() 
*
*
* Description:
*  A function called in function void ALI_ScoreBoard_Event()
*
*
* Details:
*  This function checks for a timeout to an ALI heartbeat on all ALI ports.  Heartbeats timeouts are
*  tracked on the port class <ExperientCommPort> while records timeouts are tracked within the data class.
*  If a Timeout condition is found:
* 
*  1. If Port is not already down( ie reduced heartbeat mode)
*     a. Send timeout error message.
*     a. Send outside IP (denial of service) error message if unexpected ip traffic is occurring
*     b. Set reduced heartbeat mode 
*     c. Reset heartbeatactive flag to false
*
*    
* Parameters:
*   none
*
* Variables:
*   ALI                                 Global  - <header.h> threadorPorttype enumeration member
*   ALI_MESSAGE_221                     Global  - <defines.h> Begin Reduced Heartbeat Interval output message
*   ALI_MESSAGE_228                     Global  - <defines.h> Heartbeat ACK Timeout output message
*   ALI_MESSAGE_237                     Global  - <defines.h> Unexpected Data Received from Outside IP Address output message
*   ALIPort[][]                         Global  - <ExperientCommPort>  2d array of ALI ports
*   .boolHeartbeatActive             Global  - <ExperientCommPort> member
*   boolDEBUG                           Global  - <globals.h>
*   .boolOutsideIPTraffic               Global  - <ExperientCommPort> <ExperientUDPPort> member
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member
*   CLOCK_REALTIME                      Library - <ctime>  constant
*   doubleTimeDiff			Local   - time differential xx.xx seconds
*   i                                   Local   - integer counter
*   intALI_ACK_THRESHOLD_SEC            Global  - <defines.h>
*   .intHeartbeatInterval               Global  - <ExperientCommPort> member
*   intLogCode                          Local   - integer for Log codes
*   intMAX_ALI_PORT_PAIRS               Global  - <defines.h>
*   intNUM_ALI_PORT_PAIRS               Global  - <globals.h>
*   j                                   Local   - integer counter
*   LOG_WARNING                         Global  - <defines.h>
*   objMessage                          Local   - <MessageClass> object
*   .stringOutsideIPAddr                Global  - <ExperientCommPort> <ExperientUDPPort> member
*   stringSideAorB[]                    Local   - <vector> <cstring> object member
*   timespecTimeNow                     Local   - <ctime> struct timespec holding current time
*   .timespecTimeXmitStamp              Global  - <ExperientCommPort> <ctime> struct timespec - time heartbeat was transmitted
*   UDP_Port                            Local   - <ExperientUDPPort> object
*                                                                          
* Functions:
*   clock_gettime()                     Library - <ctime>                       (int)
*   enQueue_Message()                   Global  - <globalfunctions.h>           (void)
*   .fMessage_Create()                  Global  - <MessageClass>                (void)
*   .fPingStatusString()                Global  - <ExperientUDPPort>            (string)
*   .fSetReducedHeartbeatMode()         Global  - <ExperientCommPort>           (void)
*   int2str()                           Global  - <globalfunctions.h>           (string)
*   int2strLZ()                         Global  - <globalfunctions.h>           (string)
*   time_difference()                   Global  - <globalfunctions.h>           (long double)
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_ALI_Heartbeat_Timeout() 
{
 long double	        doubleTimeDiff;			
 struct timespec	timespecTimeNow;
 MessageClass		objMessage;
 int                    intLogCode      = LOG_WARNING;
 Port_Data              objPortData;
                      
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
  { 
   for (int j = 1; j<= 2; j++)
    {
     doubleTimeDiff = 0;
   
     // determine time difference..if port is active
     if (ALIPort[i][j].boolHeartbeatActive )
      {doubleTimeDiff = time_difference(timespecTimeNow, ALIPort[i][j].timespecTimeXmitStamp);}
  
     // Check to see if there is a timeout condition. If not do nothing....
     if (doubleTimeDiff > intALI_ACK_THRESHOLD_SEC)
      {
       objPortData.fClear(); 
       switch (ALIPort[i][j].ePortConnectionType)  {
         case UDP: objPortData = ALIPort[i][j].UDP_Port.fPortData(); break;
         case TCP: objPortData = ALIPort[i][j].TCP_Port.fPortData(); break;
         default: 
          SendCodingError("ali.cpp Check_ALI_Heartbeat_Timeout() -NO_CONNECTION_TYPE");
          break;
       } 
       if(ALIPort[i][j].boolReducedHeartBeatMode) {intLogCode = LOG_WARNING*boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC ;}
       if (intLogCode)
        {
         objMessage.fMessage_Create(intLogCode,228, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_228); 
         enQueue_Message(ALI,objMessage);
        }      
        
       // if outside traffic detected and timeout occurs send message
       if (ALIPort[i][j].UDP_Port.boolOutsideIPTraffic)
        {
         if (intLogCode)
          {
           objMessage.fMessage_Create(intLogCode,237, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                                      objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_237, ALIPort[i][j].UDP_Port.stringOutsideIPAddr); 
           enQueue_Message(ALI,objMessage);

          }// end if (intLogCode)
         ALIPort[i][j].UDP_Port.boolOutsideIPTraffic = false;

        }// end if (ALIPort[intPortPair][j].UDP_Port.boolOutsideIPTraffic)


       // set reduced HB mode if not already in reduced HB mode
       if(!ALIPort[i][j].boolReducedHeartBeatMode){ ALIPort[i][j].fSetReducedHeartbeatMode(objBLANK_CALL_RECORD, timespecTimeNow);}

       // reset heartbeat 
       ALIPort[i][j].boolHeartbeatActive = false;        

      }//end if (doubleTimeDiff > intALI_ACK_THRESHOLD_SEC)

    }// end for (int j = 1; i<= 2; j++)

  }// end for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)

}// end void Check_ALI_Heartbeat_Timeout() 

void Check_ALI_Service_Heartbeat_Timeout() 
{
 long double	        doubleTimeDiff;			
 struct timespec	timespecTimeNow;
 MessageClass		objMessage;
 int                    intLogCode      = LOG_WARNING;
 Port_Data              objPortData;
                      
 extern ExperientCommPort       ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++)
  {
   doubleTimeDiff = 0;
   if (ALIServicePort[i].boolHeartbeatActive ) {doubleTimeDiff = time_difference(timespecTimeNow, ALIServicePort[i].timespecTimeXmitStamp);}
  
   if (doubleTimeDiff > intALI_ACK_THRESHOLD_SEC)
    {
     objPortData.fClear(); 
     switch (ALIServicePort[i].ePortConnectionType)      {
       case UDP: objPortData = ALIServicePort[i].UDP_Port.fPortData(); break;
       case TCP: objPortData = ALIServicePort[i].TCP_Port.fPortData(); break;
       default: 
           SendCodingError("ali.cpp Check_ALI_Service_Heartbeat_Timeout() -NO_CONNECTION_TYPE");
           break;
     } 
     objMessage.fMessage_Create(intLogCode,228, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_228); 
     enQueue_Message(ALI,objMessage);
     ALIServicePort[i].boolHeartbeatActive = false;
     
    }        
  }

}


/****************************************************************************************************
*
* Name:
*  Function: Check_ALI_Heartbeat_Interval()
*
*
* Description:
*   A function called in function void ALI_ScoreBoard_Event() 
*
*
* Details:
*   This function:
*
*   1. Is called when there is no active record (one that has been transmitted awaiting ACK) and checks if a the
*      heartbeat interval time has been exceeded.
*      
*   2. If time has been exceeded:		transmit heartbeat
*
*      a. if TX fails :                         1. if in reduced hearbeat mode : set for no retransmit
*                                               2. else set for retransmit 
*      
*   3. if time has not been exceeded:		exit
*  
*
* Parameters: 				
*   none
*						
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> member
*   ALI_MESSAGE_212                     Global  - <defines.h> Heartbeat Send Failure output message
*   ALI_MESSAGE_213                     Global  - <defines.h> Heartbeat Sent output message
*   .boolACKRequired                    Global  - <ExperientCommPort> member .. true if ACK required on port
*   boolDEBUG                           Global  - <defines.h> show/log debug data
*   .boolPortActive                     Global  - <ExperientCommPort> member .. true if port is active
*   .boolHeartbeatRequired              Global  - <ExperientCommPort> member .. true if heartbeat required on port
*   boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC   Global  - <globals.h> diplay heartbeats if true 
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member .. true if port in reduced heartbeat mode
*   ALIPort[][]                         Global  - <ExperientCommPort> 2d array
*   charALIHEARTBEATMSG                 Global  - <globals.h> encoded heartbeat message
*   CLOCK_REALTIME			Library - <ctime> constant
*   doubleTimeDiff			Local   - time differential
*   i					Local   - general purpose integer 
*   intALI_ACK_THRESHOLD_SEC		Global  - <defines.h>  how long after xmit an ACK recv is required
*   intLogCode                          Local   - code to send to log thread
*   .intHeartbeatInterval               Global  - <ExperientCommPort> member .. current heartbeat interval on port
*   intNUM_ALI_PORT_PAIRS               Global  - <globals.h> number of installed ALI ports pairs
*   intRC                               Local   - return code
*   j                                   Local   - general purpose integer 
*   LOG_CONSOLE_FILE                    Global  - <defines.h> log code 
*   LOG_WARNING                         Global  - <defines.h> log code
*   objMessage				Local	- <MessageClass> object
*   .timespecTimeLastHeartbeat          Global  - <ExperientCommPort> <ctime> <timespec> member
*   .timespecTimeXmitStamp              Global  - <ExperientCommPort> <ctime> <timespec> member
*   timespecTimeNow			Local   - <ctime> struct timespec holding current time
*                                                                          
* Functions:  
*   clock_gettime()			Library <ctime>                         (int)
*   enQueue_Message()		        Global  <globalfunctions.h>             (void)
*   .fMessage_Create()			Local   <MessageClass>                  (void)
*   .fSetPortActive()                   Global  <ExperientCommPort>             (void)
*   int2strLZ()                         Global  <globalfunctions.h>             (string)
*   time_difference()                   Global  <globalfunctions.h>             (long double)
*   Transmit_Data()                     Global  <globalfunctions.h>             (int)
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_ALI_Heartbeat_Interval()
{
 long double            doubleTimeDiff;
 struct timespec        timespecTimeNow;
 MessageClass           objMessage;
 int                    intRC = 0;
 int                    intLogCode = LOG_CONSOLE_FILE*boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC;
 Port_Data              objPortData;
 unsigned char          chHeartbeatRequest[E2_MAX_REQUEST_LENGTH];
 int                    intRequestLength = E2_MAX_REQUEST_LENGTH;
 ExperientDataClass     objData;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 
 for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
   
  {
   for (int j = 1; j<= 2; j++)
    {
     if( ALIPort[i][j].boolHeartbeatActive ) {continue;}
     if( ALIPort[i][j].boolPhantomPort ) { ALIPort[i][j].timespecTimeLastHeartbeat = timespecTimeNow; continue;}
     doubleTimeDiff = time_difference(timespecTimeNow, ALIPort[i][j].timespecTimeLastHeartbeat);
     //  Check if heartbeat interval is exceeded
     if (doubleTimeDiff > ALIPort[i][j].intHeartbeatInterval)
      {
       // send heartbeat on all ports requiring heartbeats      
       objPortData.fClear();
       objPortData = ALIPort[i][j].UDP_Port.fPortData();
       switch (ALIPort[i][j].eALIportProtocol)
        {
         case ALI_E2:
              objData.fGenerateE2ALIRequest(chHeartbeatRequest, &intRequestLength, H_BEAT);
              //     cout <<  ASCII_String(chHeartbeatRequest,  intRequestLength) << endl;
              intRC = Transmit_Data(ALI, i,(char*) chHeartbeatRequest, intRequestLength, j);
              break;
         case ALI_MODEM:
              intRC = Transmit_Data(ALI, i,charALIHEARTBEATMSG, 2, j);
              //        cout << "send HB -> code = " << intRC << endl;
              break;
         default:
              SendCodingError("ali.cpp Coding error in fn Check_ALI_Heartbeat_Interval(): wrong protocol"); return;
        }
 
       clock_gettime(CLOCK_REALTIME, &ALIPort[i][j].timespecTimeLastHeartbeat);
       ALIPort[i][j].timespecTimeXmitStamp = ALIPort[i][j].timespecTimeLastHeartbeat;
         if (intRC)
          {
           // TX failed
           if (ALIPort[i][j].boolReducedHeartBeatMode){intLogCode = LOG_WARNING*boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC ;}
           else                                       {intLogCode = LOG_WARNING;}

           objMessage.fMessage_Create(intLogCode,212, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                                      objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_212,"","","","","","",DEBUG_MSG); 
           enQueue_Message(ALI,objMessage);

           ALIPort[i][j].boolHeartbeatActive = true;
           //skip to next port in loop
           continue;
          }
         else
          {
           // TX successful
           if (ALIPort[i][j].boolReducedHeartBeatMode){intLogCode = LOG_CONSOLE_FILE * boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC;}
           else                                       {intLogCode = LOG_CONSOLE_FILE * boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC;}

           objMessage.fMessage_Create(intLogCode,213, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                                      objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_213,"","","","","","",DEBUG_MSG); 
           enQueue_Message(ALI,objMessage);
           ALIPort[i][j].fSetPortActive();
           ALIPort[i][j].boolHeartbeatActive = true;
          }// end if else
    
      }//end if (doubleTimeDiff > ALIPort[i][j].intHeartbeatInterval)

    }// end for (int j = 1; i<= 2; j++)

  }//end for (int i = 1; i<= intNUM_ALI_PORTS; i++)

}//end Check_ALI_Heartbeat_Interval

void Check_ALI_Service_Heartbeat_Interval()
{
 long double            doubleTimeDiff;
 struct timespec        timespecTimeNow;
 MessageClass           objMessage;
 int                    intRC = 0;
 Port_Data              objPortData;
 ExperientDataClass     objData;

 string                 strALIServiceHeartbeat, strTransactionId;
 string                 strPrefix = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Event id=\"";
 string                 strSuffix = "\"><Heartbeat /></Event>";

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++)
  {
   switch (ALIServicePort[i].ePortConnectionType)  {
     case UDP: break;
     case TCP:
              if      (!ALIServicePort[i].TCP_Port.GetConnected())    { continue;}
              else if (!ALIServicePort[i].TCP_Port.boolIntialConnect) { continue;}
              else                                                    { break;}
     default: 
         SendCodingError("ali.cpp Check_ALI_Service_Heartbeat_Interval() -NO_CONNECTION_TYPE");
         break;
   }

   if (ALIServicePort[i].boolHeartbeatActive)                         { continue;}
   doubleTimeDiff = time_difference(timespecTimeNow, ALIServicePort[i].timespecTimeLastHeartbeat);
   if (doubleTimeDiff > ALIServicePort[i].intHeartbeatInterval)
    {
     strTransactionId = int2str(ALIServicePort[i].fNextHBTransactionId());
     strALIServiceHeartbeat = strPrefix + strTransactionId + strSuffix;
     intRC = Transmit_Data(ALI, i,(char*) strALIServiceHeartbeat.c_str(), strALIServiceHeartbeat.length(), 3);
     clock_gettime(CLOCK_REALTIME, &ALIServicePort[i].timespecTimeXmitStamp);
     ALIServicePort[i].boolHeartbeatActive = true;
    }
  }

}

/****************************************************************************************************
*
* Name:
*  Function: void Check_ALI_Port_Down_Notification()
*
*
* Description:
*  A function called in function void ALI_ScoreBoard_Event()
*
*
* Details:
*  The function checks each ALI port for how long it has been a down status.
*  If a port has been down exceeding a configured threshold (intPortDownThresholdSec), it sends out 
*  an Alarm message.  After that message has been sent the function sends reminder alarms at 
*  another configured interval (intPortDownReminderThreshold).
*
*    
* Parameters:
*   none
*
* Variables:
*   ALI                                 Global  - <header.h> threadorPorttype enumeration member
*   ALI_MESSAGE_223                     Global  - <defines.h> Port Down error message
*   ALI_MESSAGE_224                     Global  - <defines.h> Port Down Reminder message
*   ALIPort[][]                         Global  - <ExperientCommPort> 2d array of ALI ports
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatModeFirstAlarm Global  - <ExperientCommPort> member
*   CLOCK_REALTIME                      Library - <ctime> constant
*   i                                   Local   - general purpose integer
*   intMAX_ALI_PORT_PAIRS               Global  - <defines.h>
*   intNUM_ALI_PORT_PAIRS               Global  - <globals.h>
*   intALI_PORT_DOWN_REMINDER_SEC       Global  - <globals.h>
*   .intPortDownReminderThreshold       Global  - <ExperientCommPort> member
*   .intPortDownThresholdSec            Global  - <ExperientCommPort> member
*   j                                   Local   - general purpose integer
*   ldoubleTimeDiff                     Local   - long double to store difference between time stamps
*   LOG_ALARM                           Global  - <defines.h>
*   LOG_WARNING                         Global  - <defines.h>
*   objMessage                          Local   - <MessageClass> object
*   stringSideAorB[]                    Local   - <vector> <cstring> used for output
*   .timespecReducedHeartBeatMode       Global  - <ExperientCommPort> member
*   timespecTimeNow                     Local   - <ctime> struct timespec holding current time
*   UDP_Port                            Local   - <ExperientUDPPort> object
*                                                                          
* Functions:
*   clock_gettime()                     Library - <ctime>                       (int)
*   enQueue_Message()                   Global  - <globalfunctions.h>           (void)
*   .fMessage_Create()                  Global  - <MessageClass>                (void)
*   int2strLZ()                         Global  - <globalfunctions.h>           (string)
*   .fPingStatus()                      Global  - <ExperientUDPPort>            (bool)
*   .fPingStatusString()                Global  - <ExperientUDPPort>            (string)
*   seconds2time()                      Global  - <globalfunctions.h>           (string) 
*   time_difference()                   Global  - <globalfunctions.h>           (long double) 
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_ALI_Port_Down_Notification()
{
 MessageClass           objMessage;
 long double            ldoubleTimeDiff;
 struct timespec        timespecTimeNow;
 Port_Data              objPortData;
 Port_Data              objPortPairData;
 string                 strPingStatus;
 string                 strAlarmMessage;

 extern ExperientCommPort   ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);  
 for (int i = 1; i <= intNUM_ALI_PORT_PAIRS; i++)
  {
   for (int j = 1; j <= 2; j++)
    {
     if (ALIPort[i][j].boolReducedHeartBeatMode)
      {
       ldoubleTimeDiff = time_difference(timespecTimeNow, ALIPort[i][j].timespecReducedHeartBeatMode);
       objPortData.fClear(); 
       switch (ALIPort[i][j].ePortConnectionType) {
         case UDP: objPortData = ALIPort[i][j].UDP_Port.fPortData();objPortPairData = ALIPort[i][j].UDP_Port.fPortPairData();break;
         case TCP: objPortData = ALIPort[i][j].TCP_Port.fPortData();objPortPairData = ALIPort[i][j].TCP_Port.fPortPairData();break;
         default: 
          SendCodingError("ali.cpp Check_ALI_Port_Down_Notification() -NO_CONNECTION_TYPE -1");
          break;        
       }

        //check for first alarm condition
        if (( ldoubleTimeDiff > ALIPort[i][j].intPortDownThresholdSec) && (!ALIPort[i][j].boolReducedHeartBeatModeFirstAlarm))
         {
          switch (ALIPort[i][j].ePortConnectionType) {
           case UDP: ALIPort[i][j].UDP_Port.fPingStatus();strPingStatus =  ALIPort[i][j].UDP_Port.fPingStatusString(); break;
           case TCP: ALIPort[i][j].TCP_Port.fPingStatus();strPingStatus =  ALIPort[i][j].TCP_Port.fPingStatusString(); break;
           default: 
            SendCodingError("ali.cpp Check_ALI_Port_Down_Notification() -NO_CONNECTION_TYPE -2");
           break; 
          }

          strAlarmMessage = Create_Message(ALI_MESSAGE_223, seconds2time(int(ldoubleTimeDiff)),strPingStatus);
          objMessage.fMessage_Create(LOG_ALARM, 223, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                     objBLANK_ALI_DATA, strAlarmMessage, " ", "Note\t= " + ALIPort[i][j].strNotes,"","","","", NORMAL_MSG, NEXT_LINE);
          enQueue_Message(ALI,objMessage);
          ALIPort[i][j].boolReducedHeartBeatModeFirstAlarm = true;

          // check if both ports are down
          if (ALIPort[i][1].boolReducedHeartBeatModeFirstAlarm&&ALIPort[i][2].boolReducedHeartBeatModeFirstAlarm)
           {
            objMessage.fMessage_Create(LOG_ALARM, 263, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortPairData, 
                                       objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_263);                            
            enQueue_Message(ALI,objMessage);
            ALIPort[i][1].boolALIportPairDown = ALIPort[i][2].boolALIportPairDown = true; 
           }


         }

        //else if check for Port Down hourly reminder interval condition........
        else if  ((ldoubleTimeDiff > ALIPort[i][j].intPortDownReminderThreshold) && (ALIPort[i][j].boolReducedHeartBeatModeFirstAlarm))
         {
          switch (ALIPort[i][j].ePortConnectionType)  {
            case UDP: ALIPort[i][j].UDP_Port.fPingStatus();strPingStatus =  ALIPort[i][j].UDP_Port.fPingStatusString(); break;
            case TCP: ALIPort[i][j].TCP_Port.fPingStatus();strPingStatus =  ALIPort[i][j].TCP_Port.fPingStatusString(); break;
            default: 
             SendCodingError("ali.cpp Check_ALI_Port_Down_Notification() -NO_CONNECTION_TYPE -3");
             break;    
          }
          ALIPort[i][j].intPortDownReminderThreshold += (intALI_PORT_DOWN_REMINDER_SEC);
          objMessage.fMessage_Create(LOG_ALARM, 224, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData,
                                     objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_224, seconds2time(int(ldoubleTimeDiff)),strPingStatus);
          enQueue_Message(ALI,objMessage);
         } 

     
      }//end if (CommPort[i].boolReducedHeartBeatMode)

    }// end for (int j = 1; j <= 2; j++)


  //check if both Ports are in either emergency shutdown or reduced HB
   if (!(ALIPort[i][1].boolALIportPairDown&&ALIPort[i][2].boolALIportPairDown))
    {
     if (ALIPort[i][1].boolEmergencyShutdown&&ALIPort[i][2].boolReducedHeartBeatModeFirstAlarm)
      {
        objMessage.fMessage_Create(LOG_ALARM, 263, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                   objPortPairData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_263);                            
        enQueue_Message(ALI,objMessage);
        ALIPort[i][1].boolALIportPairDown = ALIPort[i][2].boolALIportPairDown = true; 
      }
    }
	
  }//end for (int i = 1; i <= intNumofPorts; i++)



}// end void Port_Down_Message()

void CheckALIServiceDownNotification()
{
 MessageClass           objMessage;
 long double            ldoubleTimeDiff;
 struct timespec        timespecTimeNow;
 Port_Data              objPortData;
 string                 strPingStatus;

 extern ExperientCommPort   ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
 extern struct timespec     timespecControllerStartTime;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);  
 for (int i = 1; i <= intNUM_ALI_SERVICE_PORTS; i++)
  {
   objPortData.fLoadPortData(ALI, i, 3);


   if (ALIServicePort[i].timespecTimeLastHeartbeat.tv_sec < timespecControllerStartTime.tv_sec)
    {ldoubleTimeDiff = time_difference(timespecTimeNow, timespecControllerStartTime);}
   else
    {   ldoubleTimeDiff = time_difference(timespecTimeNow, ALIServicePort[i].timespecTimeLastHeartbeat); }

   if (( ldoubleTimeDiff > ALIServicePort[i].intPortDownThresholdSec) && (!ALIServicePort[i].boolReducedHeartBeatModeFirstAlarm))
    {
     switch (ALIServicePort[i].ePortConnectionType) {
       case UDP: ALIServicePort[i].UDP_Port.fPingStatus();strPingStatus =  ALIServicePort[i].UDP_Port.fPingStatusString(); break;
       case TCP: ALIServicePort[i].TCP_Port.fPingStatus();strPingStatus =  ALIServicePort[i].TCP_Port.fPingStatusString(); break;
       default: 
            SendCodingError("ali.cpp CheckALIServiceDownNotification() -NO_CONNECTION_TYPE -1");
            break;   
     }
     objMessage.fMessage_Create(LOG_ALARM, 273, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,objPortData, 
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_273, seconds2time(int(ldoubleTimeDiff)), strPingStatus);                            
     enQueue_Message(ALI,objMessage);
     ALIServicePort[i].boolReducedHeartBeatMode = true;
     ALIServicePort[i].boolReducedHeartBeatModeFirstAlarm = true;
     ALIServicePort[i].intHeartbeatInterval = intREDUCED_ALI_HEARTBEAT_INTERVAL_SEC;
    }
   else if  ((ldoubleTimeDiff > ALIServicePort[i].intPortDownReminderThreshold) && (ALIServicePort[i].boolReducedHeartBeatModeFirstAlarm))
    {
     switch (ALIServicePort[i].ePortConnectionType)  {
       case UDP: ALIServicePort[i].UDP_Port.fPingStatus();strPingStatus =  ALIServicePort[i].UDP_Port.fPingStatusString(); break;
       case TCP: ALIServicePort[i].TCP_Port.fPingStatus();strPingStatus =  ALIServicePort[i].TCP_Port.fPingStatusString(); break;
       default: 
            SendCodingError("ali.cpp CheckALIServiceDownNotification() -NO_CONNECTION_TYPE -2");
            break;
     }
     ALIServicePort[i].intPortDownReminderThreshold += (intALI_PORT_DOWN_REMINDER_SEC);
     objMessage.fMessage_Create(LOG_ALARM, 224, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_224, seconds2time(int(ldoubleTimeDiff)),strPingStatus);
     enQueue_Message(ALI,objMessage);
    } 




  }



}


/****************************************************************************************************
*
* Name:
*  Function: void Check_ALI_Record_Timeout(ExperientDataClass WorkTable[])
*
*
* Description:
*  This function is implemented in ALI_ScoreBoard_Event()
*
*
* Details:
*  The function checks the WorkTable[] for records that have timed out.
*   
*  1. If a record has Timed out :
*     a. Generate Error message 
*     b. attempt retransmit
*        if retransmit is unsuccessful or has been previously retransmitted:
*        (1) Set ALI Record Fail Flag and transmit to Main
*        (2) Reset Xmit Retry Flag on Port
*        (3) Clear Record from Table 
*    
* Parameters:
*   WorkTable[]                                   <ExperientDataClass>
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> enumeration member
*   ALI_MESSAGE_220                     Global  - <defines.h> Record Timeout Output message
*   ALI_MESSAGE_221                     Global  - <defines.h> Begin Reduced Heartbeat Interval Output message
*   ALI_MESSAGE_231                     Global  - <defines.h> ALI Request Retransmited Output message
*   ALI_MESSAGE_237                     Global  - <defines.h> Unexpected Data Received from Outside IP Address Output message
*   ALIPort[][]                         Global  - <ExperientCommPort> 
*   .boolALIRecordActive                Global  - <ExperientDataClass><ALI_Data> member
*   .boolALIRecordFail                  Global  - <ExperientDataClass><ALI_Data> member
*   boolDEBUG                           Global  - debug mode 
*   .boolOutsideIPTraffic               Global  - <ExperientCommPort> <ExperientUDPPort> member
*   .boolRecordReTransmitted            Local   - bool - was record re transmitted ..
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member
*   .boolXmitRetry                      Global  - <ExperientCommPort> member
*   CLOCK_REALTIME                      Global  - <ctime> Constant
*   doubleTimeDiff                      Local   - holds absolute time difference
*   .enumThreadSending                  Global  - <ExperientDataClass> <threadorPorttype>
*   i                                   Local   - integer counter
*   .intALIPortPairUsedtoTX             Global  - <ExperientDataClass><ALI_Data> member 
*   .intALITimeoutSec                   Global  - <ExperientDataClass><ALI_Data> member 
*   intEnd                              Local   - Used to check the special trunks 95-99 (manual bids)
*   intFailedDatabase                   Local   - integer Database that failed Bid.
*   .intHeartbeatInterval               Global  - <ExperientCommPort> member
*   intLogCode                          Local   - Code for Log Thread Processing
*   intNUM_TRUNKS_INSTALLED             Global  - Number of installed trunks
*   intPortPair                         Local   - Port Pair that record was transmitted on
*   intRebidDatabase                    Local   - integer the database to retry new initial bid
*   intRebidPortPair                    Local   - integer the new portpair to retry initial bid
*   intStart                            Local   - Used to check the special trunks 95-99 (manual bids)
*   .intUniqueCallID                    Global  - <ExperientDataClass> member
*   j                                   Local   - integer counter
*   LOG_CONSOLE_FILE                    Global  - <defines.h> Log Code
*   LOG_WARNING                         Global  - <defines.h> Log Code
*   objMessage                          Local   - <MessageClass>  member
*   queue_objMainData                   Global  - <queue><ExperientDataClass> objects
*   sem_tMutexMainInputQ                Global  - <globals.h> Mutex semaphore  
*   .stringAliRequest                   Global  - <ExperientDataClass><ALI_Data> member
*   .stringOutsideIPAddr                Global  - <ExperientCommPort> <ExperientUDPPort> member
*   stringSideAorB[]                    Local   - <vector><cstring> used to convert 1 to "A" , 2 to "B" for output
*   stringTemp                          Local   - <cstring>
*   .stringTimeOfEvent                  Global  - <ExperientDataClass> member
*   .stringTimeStamp                    Global  - <MessageClass> member
*   .stringTrunk                        Global  - <ExperientDataClass> member
*   timespecTimeNow                     Local   - <ctime> struct timespec - Current time
*   .timespecTimeRecordReceivedStamp    Global  - <ExperientDataClass> member
*   UDP_Port                            Local   - <ExperientUDPPort> object 
*   y                                   Local   - integer counter
*                                                                          
* Functions:
*   clock_gettime()                     Library - <ctime>                       (int)
*   .c_str()                            Library - <cstring>                     (const char*)
*   enQueue_Message()                   Global  - <globalfunctions.h>           (void)
*   .fMessage_Create()                  Global  - <MessageClass>                (void)
*   .fPingStatusString()                Global  - <ExperientUDPPort>            (string)          
*   .fSetPortActive()                   Global  - <ExperientCommPort>           (void)
*   .fSetReducedHeartbeatMode()         Global  - <ExperientCommPort>           (void)
*   int2str()                           Global  - <globalfunctions.h>           (string)
*   int2strLZ()                         Global  - <globalfunctions.h>           (string)
*   .length()                           Library - <cstring>                     (size_t)
*   .push()                             Library - <queue>                       (void)
*   sem_post()                          Library - <csignal>                     (int)
*   sem_wait()                          Library - <csignal>                     (int)
*   time_difference()                   Global  - <globalfunctions.h>           (long double)
*   Transmit_Data()                     Global  - <globalfunctions.h>           (int)
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_ALI_Record_Timeout()
{
 extern vector <ExperientDataClass>     vALIworkTable; 
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
 long double		                doubleTimeDiff;			
 struct timespec	                timespecTimeNow;
 MessageClass		                objMessage;
 int                                    intLogCode      = LOG_WARNING;
 int                                    intPortPair;
 Port_Data                              objPortData;                      

 vector<ExperientDataClass>::size_type          sz       = vALIworkTable.size();


 // note worktable must be semaphore locked prior to calling this function

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 
  // check through list "start to end" 
  for (unsigned int i = 0; i < sz; i++)
   {
    doubleTimeDiff = 0;
   
    // determine time difference..if record is active
    if (vALIworkTable[i].ALIData.boolALIRecordActive )
     {doubleTimeDiff = time_difference(timespecTimeNow, vALIworkTable[i].timespecTimeRecordReceivedStamp);}
           
    // Check to see if there is a timeout condition. If not do nothing....
    if (doubleTimeDiff > vALIworkTable[i].ALIData.intALITimeoutSec)
     {
      // time threshold exceeded....
      intPortPair = vALIworkTable[i].ALIData.intALIPortPairUsedtoTX;
      vALIworkTable[i].ALIData.eJSONEventCode = ALI_TIMEOUT;
      // if outside traffic detected and timeout occurs send message
      for (int j = 1; j <=2; j++)
       {
        if(ALIPort[intPortPair][j].boolReducedHeartBeatMode){intLogCode = LOG_WARNING*boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC ;}
        else 						    {intLogCode = LOG_WARNING;}

         objPortData.fClear(); objPortData.fLoadPortData(ALI, intPortPair, j);
         if (intLogCode)
          {
           objMessage.fMessage_Create(intLogCode,220, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[i].fCallData(), objBLANK_TEXT_DATA, objPortData,
                                      objBLANK_FREESWITCH_DATA, vALIworkTable[i].ALIData, ALI_MESSAGE_220);
           enQueue_Message(ALI,objMessage);
          }

        if (ALIPort[intPortPair][j].UDP_Port.boolOutsideIPTraffic)
         {
          if (intLogCode)
           {
            objMessage.fMessage_Create(intLogCode,237, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData,
                                       objBLANK_FREESWITCH_DATA, vALIworkTable[i].ALIData, ALI_MESSAGE_237, ALIPort[intPortPair][j].UDP_Port.stringOutsideIPAddr); 
            enQueue_Message(ALI,objMessage);
 
           }// end if (intLogCode)
            ALIPort[intPortPair][j].UDP_Port.boolOutsideIPTraffic = false;

         }// end if (ALIPort[intPortPair][j].UDP_Port.boolOutsideIPTraffic)

       }// end for (int j = 1; j <=2; j++)

     // attempt reBid
       
     if (!Rebid_ALI_Record(i, timespecTimeNow)) 
      {
       SendALIRecordFailToMain(i); 
       vALIworkTable.erase(vALIworkTable.begin()+i);
      }

     }//end if (doubleTimeDiff > vALIworkTable[i].ALIData.intALITimeoutSec)

   }// end for (int i = 1; i <= intMAX_NUM_TRUNKS; i++)


}// end void Check_ALI_Record_Timeout()


bool Rebid_ALI_Record(int index, struct timespec timespecTimeNow)
{
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
 extern vector <ExperientDataClass>     vALIworkTable;
 MessageClass		                objMessage;
 int                                    intPortPair;
 bool                                   boolRecordReTransmitted = false;
 Port_Data                              objPortData;                      
 int                                    intRebidDatabase;
 int                                    intRebidPortPair;
 int                                    intFailedDatabase;
 string                                 strALIrequestKey;
 string                                 strALIrequestTrunk;

 cout << "Rebid ALI RECORD" << endl;


 intPortPair = vALIworkTable[index].ALIData.intALIPortPairUsedtoTX;

    // attempt retransmit on same database if this was an initial bid ....
 //    if ( vALIworkTable[index].ALIData.enumALIBidType != REBID){strALIrequestKey = vALIworkTable[index].ALIData.stringAliRequest; strALIrequestTrunk = vALIworkTable[index].CallData.stringTrunk;}
 //    else                                                      {strALIrequestKey = vALIworkTable[index].ALIData.stringAutoRepeatAliRequest; strALIrequestTrunk = "97";}
     switch (vALIworkTable[index].ALIData.enumALIBidType)
          {
           case INITIAL: 
                strALIrequestKey = vALIworkTable[index].ALIData.stringAliRequest;
                strALIrequestTrunk = vALIworkTable[index].ALIData.strALIRequestTrunk = vALIworkTable[index].CallData.stringTrunk; 
                break;
           case REBID: case AUTO: case MANUAL:
                strALIrequestTrunk = vALIworkTable[index].ALIData.strALIRequestTrunk = strREPEAT_ALI_BID_TRUNK;
                strALIrequestKey = vALIworkTable[index].ALIData.stringAutoRepeatAliRequest;
                break;
           default:
                strALIrequestTrunk = vALIworkTable[index].ALIData.strALIRequestTrunk = "XX";
                strALIrequestKey = vALIworkTable[index].ALIData.stringAliRequest;
                break;
          }
       
       for (int j = 1; j <= 2; j++)
        {  
         if((vALIworkTable[index].ALIData.boolALIRecordRetransmit[j])&&(!ALIPort[intPortPair][j].boolReducedHeartBeatMode))			
          {
           objPortData.fClear(); objPortData.fLoadPortData(ALI, intPortPair, j);
           vALIworkTable[index].ALIData.eJSONEventCode = ALI_BID_REQUEST;
           switch (ALIPort [intPortPair] [j].eALIportProtocol) {
             case ALI_MODEM: case ALI_SERVICE:
                  Transmit_Data(ALI, intPortPair, (char*) strALIrequestKey.c_str(), strALIrequestKey.length(),j);
                  vALIworkTable[index].ALIData.boolALIRecordACKReceived[j] = false;
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE,231, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[index].fCallData(), objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[index].ALIData, ALI_MESSAGE_231, vALIworkTable[index].ALIData.stringALiRequestKey,
                                             int2strLZ(vALIworkTable[index].ALIData.intALIBidIndex), strALIrequestTrunk, int2strLZ(vALIworkTable[index].ALIData.intLastDatabaseBid),
                                             int2strLZ(intPortPair) );


                  vALIworkTable[index].ALIData.boolALIRecordACKReceived[j] = false;
                  
                  break;
             case ALI_E2:
                  vALIworkTable[index].ALIData.intTransactionIDBid[j] = 
                  vALIworkTable[index].fGenerateE2ALIRequest(vALIworkTable[index].ALIData.chE2BidRequest, &vALIworkTable[index].ALIData.intE2RequestLength, REBID);

                  Transmit_Data(ALI, intPortPair, (char*)vALIworkTable[index].ALIData.chE2BidRequest, vALIworkTable[index].ALIData.intE2RequestLength,j);
                  vALIworkTable[index].ALIData.boolALIRecordACKReceived[j] = true;
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE,231, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[index].fCallData(), objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[index].ALIData, ALI_MESSAGE_231a, 
                                             int2str(vALIworkTable[index].ALIData.intTransactionIDBid[j]));

                  vALIworkTable[index].ALIData.boolALIRecordACKReceived[j] = true;
                  break;
            default: 
                  SendCodingError("ali.cpp Rebid_ALI_Record() -NO_PROTOCOL_DEFINED");
                  return false;
           }


           ALIPort[intPortPair][j].fSetPortActive();
           vALIworkTable[index].ALIData.boolALIRecordRetransmit[j]  = false;
           vALIworkTable[index].timespecTimeRecordReceivedStamp = timespecTimeNow;         
           enQueue_Message(ALI,objMessage);
           boolRecordReTransmitted = true;
          }

        }// end for (int j = 1, j <= 2, j++)
     
     // if record was not retransmitted .......
     if (!boolRecordReTransmitted)
      {
       // ALI failed try again from another Database if Possible    
        
       for (int j = 1; j <= 2; j++)
        {
         // reset retry on port
         ALIPort[intPortPair][j].boolXmitRetry = true;
        }// end for (int j = 1; j <= 2; j++)

         intFailedDatabase = vALIworkTable[index].ALIData.intLastDatabaseBid;

        //attempt an New Initial bid on a different Database
        if(SelectALIDatabaseandPortPair(&intRebidDatabase, &intRebidPortPair, index))
         {
          if(intFailedDatabase == intRebidDatabase) {return false;}
            
            objMessage.fMessage_Create(LOG_CONSOLE_FILE,255, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[index].fCallData(), 
                                       objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                       ALI_MESSAGE_255, int2strLZ(intFailedDatabase), int2strLZ(intRebidDatabase) );
            enQueue_Message(ALI,objMessage);
            vALIworkTable[index].ALIData.boolALIRecordRetransmit[1] = true;
            vALIworkTable[index].ALIData.boolALIRecordRetransmit[2] = true;
            //         cout << "HERE B............................................................................." << endl;

            TransmitInitialBidtoDatabase(&intRebidDatabase, intRebidPortPair, index);          
         }
        else { return false;}

      }
     else
     {
      // send tracking message to KRN
      objMessage.fMessage_Create(0, 256, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[index].fCallData(), objBLANK_TEXT_DATA, 
                                 objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, " ");
      enQueue_Message(ALI,objMessage);
     }

  return true;
}

/****************************************************************************************************
*
* Name:
*  Function: bool ALI_Ports_Ready_To_Shut_Down( int intNumPorts)
*
*
* Description:
*  This function is implemented in the shut down trap area of ALI_Thread()
*
*
* Details:
*  The function checks to see if all ALI ports are ready to shut down. If they are
*  it returns true.
*          
*    
* Parameters:
*   intNumPorts                         int
*
* Variables:
*   ALIPort[][]                         Global  - <ExperientCommPort>   
*   i                                   Local   - int counter
*   boolRunningcheck                    Local   - bool
*   .boolReadyToShutDown                Global  - <ExperientCommPort> member
*                                                                          
* Functions:
*   none
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
bool ALI_Ports_Ready_To_Shut_Down( int intNumPorts)
{
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];

 bool boolRunningcheck = true;
 for (int i = 1; i <= intNumPorts; i++){boolRunningcheck = ((boolRunningcheck) && ((ALIPort[i][1].boolReadyToShutDown) && (ALIPort[i][2].boolReadyToShutDown)) );}
 
 return boolRunningcheck;
}

/****************************************************************************************************
*
* Name:
*  Function: bool Assign_ACK_To_Active_Record(int intPortNumber, int intSideAorB)
*
*
* Description:
*  This function is implemented in ALI_ACK_Received()
* 
*
* Details:
*  The function checks to see if an ALI port (intPortnumber)(intSideAorB) is actively awaiting ALI data.  It first scans the Data table and looks
*  for any active records and determines the port used to TX the ALI request. It then arbitrarily assigns an ack to that record.  This is used
*  to identify stray ACKs (ACKs that cannot be assigned to a record/ heartbeat)
*  The function returns the a bool reflecting whether the port (intPortnumber)(intSideAorB)  is awaiting data.
*
*  note: this function must be used inside of a mutex semaphore for thr ALI Work Table ....
*          
*    
* Parameters:
*   intSideAorB                         int
*   PortNumber                          int                         
*
* Variables:
*   ALI                                 Global  - <header.h> <threadorPorttype> enumeration member
*   .boolALIRecordACKReceived           Global  - <ExperientDataClass><ALI_Data> member
*   .intALIPortPairUsedtoTX             Global  - <ExperientDataClass><ALI_Data> member
*   intMAX_NUM_TRUNKS                   Global  - <defines.h> Max number of trunks 
*   intNUM_TRUNKS_INSTALLED             Global  - <globals.h> number of trunks installed
*   j                                   Local   - placeholder integer
*   vALIworkTable[]                     Global  - <ExperientDataClass> array of objects
*   boolPortPairAwaitingData[][]        Local   - bool array to calculate port status
*                                                                          
* Functions:
*  none
*  
* Author(s):
* AUTHOR: 02/02/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation 
****************************************************************************************************/
bool Assign_ACK_To_Active_Record(int intPortNumber, int intSideAorB)
{
 int                                    j;
 int                                    intRC;
 extern vector <ExperientDataClass>     vALIworkTable;
 extern vector <ExperientDataClass>     objDisconnectDataVector;
 

 intRC = sem_wait(&sem_tMutexAliDisconnectList);					
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliDisconnectList, "ALI    - sem_wait@sem_tMutexAliDisconnectList in fn Assign_ACK_To_Active_Record()", 1);}

 vector<ExperientDataClass>::size_type 	sz  = objDisconnectDataVector.size();
 vector<ExperientDataClass>::size_type 	sze = vALIworkTable.size();


 // check thru disconnect list first ...
 if(!objDisconnectDataVector.empty())
  {
   for (unsigned int i = 0; i< sz; i++)
    {
     j = objDisconnectDataVector[i].ALIData.intALIPortPairUsedtoTX;
     if ((j == intPortNumber)&&(!objDisconnectDataVector[i].ALIData.boolALIRecordACKReceived[intSideAorB]))
     {
      objDisconnectDataVector[i].ALIData.boolALIRecordACKReceived[intSideAorB] = true;
      sem_post(&sem_tMutexAliDisconnectList);
      return true;
     }// end if (j)
    }// end for (unsigned int i = 0; i< sz; i++)
  }// end if(!objDisconnectDataVector.empty())
 sem_post(&sem_tMutexAliDisconnectList);


 // check thru Table
  
 for (unsigned int i = 0; i< sze; i++)
  {
   j = vALIworkTable[i].ALIData.intALIPortPairUsedtoTX;
   if ((j == intPortNumber)&&(!vALIworkTable[i].ALIData.boolALIRecordACKReceived[intSideAorB]))
    {
     vALIworkTable[i].ALIData.boolALIRecordACKReceived[intSideAorB] = true;
     return true;
    }// end if (j)
  }// end for (int i = 1; i<= intMAX_NUM_TRUNKS; i++)


 return false;
}

void SendALIRecordFailToMain(int intIndex)
{
 int                                    intRC;
 MessageClass                           objMessage;
 extern vector <ExperientDataClass>     vALIworkTable;
 ExperientDataClass                     objDataCopy(vALIworkTable[intIndex]);

 // semaphore shall already be locked before calling this function !!!
  objMessage.fMessage_Create(0,0,__LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "");
  objDataCopy.enumThreadSending         = ALI;
  objDataCopy.stringTimeOfEvent         = objMessage.stringTimeStamp;
  objDataCopy.ALIData.boolALIRecordFail = true;
  clock_gettime(CLOCK_REALTIME, &objDataCopy.ALIData.timespecTimeAliAutoRebidOK);

  // push Blank ALI Record onto main 
  enqueue_Main_Input(ALI, objDataCopy);
//  intRC = sem_wait(&sem_tMutexMainInputQ);
//  if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexMainInputQ, "sem_wait@sem_tMutexMainInputQ in SendALIRecordFailToMain()", 1);}
//  queue_objMainData.push(objDataCopy);
//  sem_post(&sem_tMutexMainInputQ);

  
  //Send Pop Up
  objDataCopy.ALIData.eJSONEventCode = ALI_ERROR;
  objMessage.fMessage_Create(0, 262, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objDataCopy.fCallData(), objBLANK_TEXT_DATA, objBLANK_ALI_PORT, 
                             objBLANK_FREESWITCH_DATA, objDataCopy.ALIData, ALI_MESSAGE_262p);
  enQueue_Message(MAIN,objMessage);
  return;
}

int DataBaseContainingPortPair(int intPortPair)
{
 extern ALISteering                     ALI_Steering[intMAX_ALI_DATABASES + 1 ];

 for (int i = 1; i <= intNUM_ALI_DATABASES; i++)
  {
   if (ALI_Steering[i].boolPortPairInDatabase[intPortPair]){return i;}
  }

return 0;
}

void Refresh_Disconnect_DataList()
{
 int     				intRC;
 struct timespec                        timespecTimeNow;
 long double                            ldTimeDifference;
 MessageClass                           objMessage;
 bool                                   boolRecordDeleted;
 extern ExperientDataClass       	objQALIData;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 
 if (boolUSE_ALI_SERVICE_SERVER) {return;}

 intRC = sem_wait(&sem_tMutexAliDisconnectList);					
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliDisconnectList, "ALI    - sem_wait@sem_tMutexAliDisconnectList in fn Refresh_Disconnect_DataList()", 1);}

 if (objDisconnectDataVector.empty()){sem_post(&sem_tMutexAliDisconnectList);return;}

 // remove expired timeouts from the list
 do
 {
  boolRecordDeleted = false;
  for(vector<ExperientDataClass>::iterator l = objDisconnectDataVector.begin(); l!= objDisconnectDataVector.end(); ++l)
   {
    ldTimeDifference = time_difference(timespecTimeNow, l->timespecTimeRecordReceivedStamp);
    if (ldTimeDifference > l->ALIData.intALITimeoutSec)
     {
      objMessage.fMessage_Create(0,0, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD,objBLANK_TEXT_DATA, objBLANK_ALI_PORT,objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, "");
      l->enumThreadSending         = ALI;
      l->stringTimeOfEvent         = objMessage.stringTimeStamp;
      l->ALIData.boolALIRecordFail = true;
      l->WindowButtonData.fClear();
      clock_gettime(CLOCK_REALTIME, &l->ALIData.timespecTimeAliAutoRebidOK);

      //  push Blank ALI Record onto main
      objQALIData = *l; 
      enqueue_Main_Input(ALI, objQALIData); 
      objDisconnectDataVector.erase(l);
      boolRecordDeleted = true;
      // Only delete One or loop may segfault     
      break;
     }

   }//for(vector<ExperientDataClass>::iterator l = objDisconnectDataVector.begin(); l!= objDisconnectDataVector.end(); ++l)
  
 } while (boolRecordDeleted == true);


 sem_post(&sem_tMutexAliDisconnectList);
}




bool FindTelnoInDisconnectVector(ani_type enumANItype, string strData, ExperientDataClass objData, Port_Data  objPortData)
{
 int					intRC;
 MessageClass				objMessage;
 ExperientDataClass 			objOutPutData;
 bool                                   boolTelnoCheck;
 extern ExperientDataClass       	objQALIData;

 intRC = sem_wait(&sem_tMutexAliDisconnectList);					
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliDisconnectList, "ALI    - sem_wait@sem_tMutexAliDisconnectList in fn FindTelnoInDisconnectVector()", 1);}

 vector<ExperientDataClass>::size_type 	sz = objDisconnectDataVector.size();

 if (objDisconnectDataVector.empty()){sem_post(&sem_tMutexAliDisconnectList); return false;}

 for (unsigned int i=0; i< sz; i++)
  {
   switch (enumANItype)
    {
     case CALLBACK:   boolTelnoCheck = objDisconnectDataVector[i].fFindTelephoneNumber(strData); break;
     case PSUEDO_ANI: boolTelnoCheck = objDisconnectDataVector[i].fFindpANI(strData); break; 
     default:         boolTelnoCheck = false;
    }
   if ((boolTelnoCheck)&&(objDisconnectDataVector[i].ALIData.intALIBidIndex == objData.ALIData.intALIBidIndex) )
    {

     objOutPutData = objDisconnectDataVector[i];
     objOutPutData.ALIData.stringType                       = objData.ALIData.stringType;
     objOutPutData.enumThreadSending                        = ALI;
     objOutPutData.ALIData.enumALIBidType                   = LATE_RECORD;
     objOutPutData.ALIData.boolALIRecordReceived            = true;
     // assign string without the first and last char ie strip stx and etx ...
     if (strData.length() < 2) {SendCodingError("ali.cpp - Coding error in fn FindTelnoInDisconnectVector()");return false;}
     objOutPutData.ALIData.stringAliText.assign(strData,1,strData.length()-2);
     objOutPutData.ALIData.eJSONEventCode = ALI_RECEIVED;
     objMessage.fMessage_Create(LOG_CONSOLE_FILE,226, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objOutPutData.fCallData(), objBLANK_TEXT_DATA, objPortData, 
                                objBLANK_FREESWITCH_DATA, objOutPutData.ALIData, ALI_MESSAGE_226, strData, "", "", "", "", "",  NORMAL_MSG, ALI_RECORD);
     objOutPutData.ALIData.stringALIReceivedTimeStamp = objMessage.stringTimeStamp;
     objOutPutData.fLoadCallbackfromALI();
     enQueue_Message(ALI,objMessage);
      // push ALI Record onto main Q
     objQALIData = objOutPutData;
     enqueue_Main_Input(ALI, objQALIData); 
//     sem_wait(&sem_tMutexMainInputQ);
//     queue_objMainData.push(objOutPutData);
//     sem_post(&sem_tMutexMainInputQ);

     objDisconnectDataVector.erase(objDisconnectDataVector.begin()+i);
     sem_post(&sem_tMutexAliDisconnectList); 
     return true;
    }
  }// end for(list<ExperientDataClass>::iterator ....
 sem_post(&sem_tMutexAliDisconnectList);
 return false;
}

int IndexinDisconnectVectorUUID(ExperientDataClass objData)
{
 // sem_tMutexAliDisconnectList must be called before calling this function

  vector<ExperientDataClass>::size_type 	sz = objDisconnectDataVector.size();

  for (unsigned int i=0; i< sz; i++)
   {
    if (objData.CallData.intUniqueCallID == objDisconnectDataVector[i].CallData.intUniqueCallID)
     {
       return i;
     }

   }

 return -1;
}


int find_next_avialable_index(int intBidIndex, int intTrunk)
{
// if((intTrunk >= intALI_MAN_BID_TRUNK_FIRST)&&(intTrunk <= intALI_MAN_BID_TRUNK_LAST))
//  {return intTrunk;}

 int intCounter = 0;
 do
  {
   // table must be locked by semaphore before this function called ...
   intBidIndex++;
   intCounter++;


   if (intBidIndex >= intALI_MAN_BID_TRUNK_FIRST){intBidIndex = 1;}

   if(Find_Table_Index_from_Bid_Index(intBidIndex) < 0){return intBidIndex;}
   
  }while (intCounter <= intALI_MAN_BID_TRUNK_FIRST);

 // error

 return -1;
}






int Find_Index_from_Call_ID(unsigned long long int uliCallId)
{
 vector<ExperientDataClass>::size_type          sz = vALIworkTable.size();

 // table must be locked by semaphore before this function called ...
 for (unsigned int i = 0; i < sz; i++)
  {
   if(uliCallId == vALIworkTable[i].CallData.intUniqueCallID){return i;}
  }

 //not found
 return -1;
}


int Find_Index_from_ALI_Received(string stringALIData, Port_Data objPortData)
{ 
 string 	stringIndex;
 MessageClass 	objMessage;
 int    	intIndex = -1;
 bool   	BadData  = false;

 if      (stringALIData.length() == 4){stringIndex.assign(stringALIData,2,1); BadData = true;}
 else if (stringALIData.length() < 4) {stringIndex = "";                      BadData = true;}
 else                                 {stringIndex.assign(stringALIData,2,2);}

 if (!BadData)
  {
   if (Validate_Integer(stringIndex)){intIndex = char2int(stringIndex.c_str());}
   else                              {BadData  = true;}
  }

 if ((intIndex < 0)||(intIndex >99)) {BadData = true;}
 
 if (BadData)
 {
  objMessage.fMessage_Create(LOG_WARNING,218, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                             objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_218b, ASCII_String(stringIndex.c_str(),stringIndex.length()), 
                             ASCII_String(stringALIData.c_str(), stringALIData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
  enQueue_Message(ALI,objMessage);
 }

 return intIndex;
}

int Find_Index_from_TransactionID(DataPacketIn DataIn, Port_Data objPortData)
{
  // table must be locked by semaphore before this function called ...
 vector<ExperientDataClass>::size_type          sz       = vALIworkTable.size();
 unsigned long int                              SearchID = DataIn.E2Data.fTransactionID();

 // seearch thru table
 for (unsigned int i= 0; i < sz; i++)
  {
   for (int j =1; j <= 2; j++) {if (SearchID == vALIworkTable[i].ALIData.intTransactionIDBid[j]) {return i;} }
  }

 return -1;
}

int Find_Table_Index_from_Bid_Index(int iBidIndex)
{
  // table must be locked by semaphore before this function called ...
 vector<ExperientDataClass>::size_type          sz       = vALIworkTable.size();

 // seearch thru table
 for (unsigned int i= 0; i < sz; i++)
  {
   if (iBidIndex == vALIworkTable[i].ALIData.intALIBidIndex) {return i;}
  }

 return -1;
}









bool Found_TranscationID_In_DisconnectVector(DataPacketIn DataIn, ExperientDataClass objData, Port_Data objPortData)
{
 int					intRC;
 MessageClass				objMessage;
 ExperientDataClass 			objOutPutData;
 unsigned long int                      SearchID = DataIn.E2Data.fTransactionID();
 extern ExperientDataClass       	objQALIData;

 intRC = sem_wait(&sem_tMutexAliDisconnectList);					
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliDisconnectList, "ALI    - sem_wait@sem_tMutexAliDisconnectList in fn FindTelnoInDisconnectVector()", 1);}

 vector<ExperientDataClass>::size_type 	sz = objDisconnectDataVector.size();

 if (objDisconnectDataVector.empty()){sem_post(&sem_tMutexAliDisconnectList); return false;}

 for (unsigned int i=0; i< sz; i++)
  {
   if ((objDisconnectDataVector[i].ALIData.intTransactionIDBid[1] == SearchID)||(objDisconnectDataVector[i].ALIData.intTransactionIDBid[2] == SearchID))
    {
     objOutPutData = objDisconnectDataVector[i];
     objOutPutData.ALIData.stringType                       = objData.ALIData.stringType;
     objOutPutData.enumThreadSending                        = ALI;
     objOutPutData.ALIData.enumALIBidType                   = LATE_RECORD;
     objOutPutData.ALIData.boolALIRecordReceived            = true;
     // assign string without the first and last char (remove STX/ETX)
     if (DataIn.stringDataIn.length() < 2) {SendCodingError("ali.cpp - Coding error in fn Found_TranscationID_In_DisconnectVector()");return false;}
     objOutPutData.ALIData.stringAliText.assign(DataIn.stringDataIn,1, DataIn.stringDataIn.length()-2);
     objMessage.fMessage_Create(LOG_CONSOLE_FILE,226, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objOutPutData.fCallData(), objBLANK_TEXT_DATA, objPortData, 
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_226, DataIn.stringDataIn, "", "", "", "", "",  NORMAL_MSG, ALI_RECORD);
     objOutPutData.ALIData.stringALIReceivedTimeStamp = objMessage.stringTimeStamp;
     enQueue_Message(ALI,objMessage);
      // push ALI Record onto main Q
     objQALIData = objOutPutData;
     enqueue_Main_Input(ALI, objQALIData); 
//     sem_wait(&sem_tMutexMainInputQ);
//     queue_objMainData.push(objOutPutData);
//     sem_post(&sem_tMutexMainInputQ);

     objDisconnectDataVector.erase(objDisconnectDataVector.begin()+i);
     sem_post(&sem_tMutexAliDisconnectList); 
     return true;
    }
  }// end for(list<ExperientDataClass>::iterator ....
 sem_post(&sem_tMutexAliDisconnectList);
 return false;
}

void SendUnMatchedTransactionID(int intPortPair, int intTransactionID, DataPacketIn DataIn, Port_Data objPortData)
{
 MessageClass        objMessage;


 if (!boolUSE_ALI_SERVICE_SERVER )
  {
    // disregard if we expect to get 2 ali responses
   if(!ALI_Steering[DataBaseContainingPortPair(intPortPair)].boolDatabaseSendsSingleALI){return;}
  }

 // send error message
 objMessage.fMessage_Create(LOG_WARNING,232, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                            objBLANK_ALI_DATA, Create_Message(ALI_MESSAGE_232a,int2strLZ(intTransactionID)), DataIn.stringDataIn, "","","","","", NORMAL_MSG,ALI_RECORD);
 enQueue_Message(ALI,objMessage);
 
 return;
}

void ResetPortPairStatus(int i)
{
 extern ExperientCommPort                        ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 MessageClass                                    objMessage;
 Port_Data                                       objPortPairData;

 if (i > intMAX_ALI_PORT_PAIRS) {return;}
 switch (ALIPort[i][1].ePortConnectionType)  {
   case UDP: objPortPairData = ALIPort[i][1].UDP_Port.fPortPairData();break;
   case TCP: objPortPairData = ALIPort[i][1].TCP_Port.fPortPairData();break;
   default: 
        SendCodingError("ali.cpp ResetPortPairStatus() -NO_CONNECTION_TYPE_DEFINED");
        return;        
 }

 if (ALIPort[i][1].boolALIportPairDown&&ALIPort[i][2].boolALIportPairDown)
  {
   objMessage.fMessage_Create(LOG_ALARM, 264, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortPairData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_264);                            
   enQueue_Message(ALI,objMessage);
  }
 ALIPort[i][1].boolALIportPairDown = ALIPort[i][2].boolALIportPairDown = false; 
}





bool SelectALIDatabaseandPortPair(int* intALiDatabaseNumber, int* intAliPortPair, int intTableIndex)
{ 
  ali_format                                      enumALiReqFormat = NO_FORMAT;
  int                                             intDataBaseandPortPairLoopCounter;
  int                                             intPortPairLoopCounter;
  MessageClass                                    objMessage; 
  string                                          strData;
  int                                             intStartDatabase = *intALiDatabaseNumber;

  extern ExperientCommPort                        ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
  extern  int                                     intAliPortPairRotater[intMAX_ALI_DATABASES + 1];
  
  // note this function should be semaphore locked prior to being called...
//cout << "tableIndex is " << intTableIndex << endl;
//cout << "Database # is " << *intALiDatabaseNumber << endl;
//cout << "PANI       is " <<  vALIworkTable[intTableIndex].CallData.stringPANI << endl;
//cout << "#init bids is " <<  vALIworkTable[intTableIndex].ALIData.intNumberOfInitialBids << endl;
//cout << "#DB   bids is " <<  vALIworkTable[intTableIndex].ALIData.fHowManyDatabasesCanbeBid()<< endl;
                 
     // Select ALI Database and Port Pair for ALI Request
     intDataBaseandPortPairLoopCounter = 0;
     do { 
       intPortPairLoopCounter = 0;
       intDataBaseandPortPairLoopCounter++;
       intStartDatabase++; 
       if (intStartDatabase > intNUM_ALI_DATABASES) {intStartDatabase = 1;} 

       // select Database
       *intALiDatabaseNumber = vALIworkTable[intTableIndex].fSelectALiDatabase(intStartDatabase);

      // cout << "Loop Database number -> " << *intALiDatabaseNumber << endl;
  
       if (!*intALiDatabaseNumber) {continue; } // skip to while ((intDataBaseandPortPairLoopCounter <= intNUM_ALI_DATABASES)&&(intPortPairLoopCounter == 0))

       enumALiReqFormat = ALI_Steering[*intALiDatabaseNumber].eRequestKeyFormat;
       if(enumALiReqFormat == NO_FORMAT)
         {
              objMessage.fMessage_Create(LOG_WARNING,254, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_ALI_PORT, 
                                         objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                         ALI_MESSAGE_254, int2strLZ(*intALiDatabaseNumber), 
                                         int2str(ALI_Steering[*intALiDatabaseNumber].eRequestKeyFormat) );
              enQueue_Message(ALI,objMessage);
              continue;                       // skip to while ((intDataBaseandPortPairLoopCounter <= intNUM_ALI_DATABASES)&&(intPortPairLoopCounter == 0))
        }

      if (!vALIworkTable[intTableIndex].fCreateAliRequestKey(enumALiReqFormat)) {continue;}
      

       // select functional PortPair 
       intPortPairLoopCounter = ALI_Steering[*intALiDatabaseNumber].intNumPortPairs;    
       do {
         intAliPortPairRotater[*intALiDatabaseNumber]+=1;
         if (intAliPortPairRotater[*intALiDatabaseNumber] > ALI_Steering[*intALiDatabaseNumber].intNumPortPairs){intAliPortPairRotater[*intALiDatabaseNumber]=1;}
         *intAliPortPair = ALI_Steering[*intALiDatabaseNumber].fSelectPortPair(intAliPortPairRotater[*intALiDatabaseNumber]);
  //       cout << "Port Pair = " << *intAliPortPair << endl;
  //       cout << " Reduced HB Mode 1 = " << ALIPort[*intAliPortPair][1].boolReducedHeartBeatMode << " Reduced HB Mode 2 = " << ALIPort[*intAliPortPair][2].boolReducedHeartBeatMode << endl;
         if    (    (!ALIPort[*intAliPortPair][1].boolReducedHeartBeatMode)||(!ALIPort[*intAliPortPair][2].boolReducedHeartBeatMode)   )  {intPortPairLoopCounter = 0; }
         intPortPairLoopCounter--;

       } while (intPortPairLoopCounter > 0);

     } while ((intDataBaseandPortPairLoopCounter <= intNUM_ALI_DATABASES)&&(intPortPairLoopCounter >= 0));

     //check for failure to find Database, PortPair, or Bid Format
     if (enumALiReqFormat == NO_FORMAT){
      SendCodingError("ali.cpp enumALiReqFormat:NO_FORMAT");
      return false;
     }

     if ((!*intALiDatabaseNumber)||(intPortPairLoopCounter == 0)) {
       if (vALIworkTable[intTableIndex].ALIData.stringALiRequestKey.empty()) { strData = "NULL";}
       else                                                                  { strData =  vALIworkTable[intTableIndex].ALIData.stringALiRequestKey;}
 
       // No ALI Database can be bid
       if(vALIworkTable[intTableIndex].ALIData.intLastDatabaseBid < 1) {
         vALIworkTable[intTableIndex].ALIData.eJSONEventCode = ALI_UNABLE_TO_BID;
         objMessage.fMessage_Create(LOG_WARNING,225, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[intTableIndex].fCallData(), objBLANK_TEXT_DATA,
                                    objBLANK_ALI_PORT, objBLANK_FREESWITCH_DATA, vALIworkTable[intTableIndex].ALIData,ALI_MESSAGE_225, strData);
         enQueue_Message(ALI,objMessage);
       }
       SendALIRecordFailToMain(intTableIndex);
       return false;
      }
return true;
}

bool  TransmitBidtoALIservice(int* intALIserviceIndex, int iTableIndex)
{
 MessageClass                                    objMessage;
 Port_Data                                       objPortData;
 string                                          strTrunkinRequest;
 string                                          strMessage;
 int                                             iLogcode;
 int                                             intRC;

 extern ExperientCommPort                        ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ]; 

 // note this function should be protected by a semaphore lock before calling

 for (int i = 0; i < intNUM_ALI_SERVICE_PORTS; i++)
  {
   *intALIserviceIndex += i;
   if (*intALIserviceIndex > intNUM_ALI_SERVICE_PORTS) {*intALIserviceIndex = 1;}
   objPortData.fLoadPortData(ALI, *intALIserviceIndex, 3);

   // find Service that is "up"
   switch (ALIServicePort[i].ePortConnectionType)    {
     case UDP:
          if (ALIServicePort[*intALIserviceIndex].boolReducedHeartBeatModeFirstAlarm) {continue;}
          break;
     case TCP:
          if (!ALIServicePort[*intALIserviceIndex].TCP_Port.GetConnected())           {continue;}
          break;
     default: 
          SendCodingError("ali.cpp TransmitBidtoALIservice() -NO_CONNECTION_TYPE_DEFINED "+int2str(i));
          continue;
   }
  
   if (!vALIworkTable[iTableIndex].fALI_ServiceBid_XML_Message())
    {
     cout << "unable to create Ali Service XML Bid!" << endl;
    }

   intRC = Transmit_Data(ALI, *intALIserviceIndex, (char*) vALIworkTable[iTableIndex].stringXMLString.c_str(), vALIworkTable[iTableIndex].stringXMLString.length(),3);

   if (intRC) {continue;}

   switch (vALIworkTable[iTableIndex].ALIData.enumALIBidType)
          {
           case INITIAL: 
                strMessage = ALI_MESSAGE_222s;
                iLogcode = 222;
                strTrunkinRequest = vALIworkTable[iTableIndex].CallData.stringTrunk; 
                break;
           case MANUAL:
                strMessage = ALI_MESSAGE_269s; 
                iLogcode = 269;
                strTrunkinRequest = vALIworkTable[iTableIndex].CallData.stringTrunk; 
                break;
           case AUTO:
                strMessage = ALI_MESSAGE_271s;
                iLogcode = 271; 
                strTrunkinRequest = strREPEAT_ALI_BID_TRUNK;
                break;
           case REBID:
                strMessage = ALI_MESSAGE_270s;
                iLogcode = 270;
                strTrunkinRequest = strREPEAT_ALI_BID_TRUNK;
                break; 
           default:
                SendCodingError("ali.cpp - Coding Error in TransmitBidtoALIservice() bid type = "+int2str(vALIworkTable[iTableIndex].ALIData.enumALIBidType));                
                strTrunkinRequest = "XX";
                break;
          }

   objMessage.fMessage_Create(LOG_CONSOLE_FILE,iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, strMessage, 
                              int2strLZ(vALIworkTable[iTableIndex].ALIData.intALIBidIndex),
                              strTrunkinRequest, int2strLZ(*intALIserviceIndex));
   enQueue_Message(ALI,objMessage);

   clock_gettime(CLOCK_REALTIME, &vALIworkTable[iTableIndex].timespecTimeRecordReceivedStamp);

   return true;


  }// end  for (int i = 0; i < intNUM_ALI_SERVICE_PORTS; i++)


  objMessage.fMessage_Create(LOG_CONSOLE_FILE,225, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA,
                             objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_225s, 
                             int2strLZ(vALIworkTable[iTableIndex].ALIData.intALIBidIndex),
                             vALIworkTable[iTableIndex].CallData.stringTrunk, int2strLZ(*intALIserviceIndex));
  enQueue_Message(ALI,objMessage);


 return false;
}

void TransmitInitialBidtoDatabase(int* intALiDatabaseNumber, int intAliPortPair, int intTableIndex)
{ 
 MessageClass                                    objMessage;
 Port_Data                                       objPortData;
 string                                          strTrunkinRequest;
 string                                          strMessage;
 int                                             iLogcode;
 extern ExperientCommPort                        ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ]; 
 extern ALISteering                     	 ALI_Steering[intMAX_ALI_DATABASES + 1 ];

 // note this function should be protected by a semaphore lock before calling

   // cout << "ALI: TransmitInitialBidtoDatabase" << endl;


    // these for loops were separated to ensure minimal time delay between transmissions
     for (int j = 1; j <= 2; j++)
      {
       if((!ALIPort[intAliPortPair][j].boolReducedHeartBeatMode)&&(!ALIPort[intAliPortPair][j].boolPhantomPort))
        {
         switch(ALIPort[intAliPortPair][j].eRequestKeyFormat)
          {
           case E2_PROTOCOL:
                if(!vALIworkTable[intTableIndex].ALIData.E2Data.boolValidRecord) {vALIworkTable[intTableIndex].fCreateE2ALIPhoneData();}
                vALIworkTable[intTableIndex].ALIData.intTransactionIDBid[j] = vALIworkTable[intTableIndex].fGenerateE2ALIRequest(vALIworkTable[intTableIndex].ALIData.chE2BidRequest, 
                                                                                                           &vALIworkTable[intTableIndex].ALIData.intE2RequestLength, INITIAL);
                Transmit_Data(ALI, intAliPortPair, (char*) vALIworkTable[intTableIndex].ALIData.chE2BidRequest, vALIworkTable[intTableIndex].ALIData.intE2RequestLength,j);
                vALIworkTable[intTableIndex].ALIData.boolALIRecordACKReceived[j] = true;
                vALIworkTable[intTableIndex].ALIData.intLastDatabaseBid = *intALiDatabaseNumber;
                vALIworkTable[intTableIndex].ALIData.bool_LF_AS_CR = ALI_Steering[ *intALiDatabaseNumber ].boolTreatLinefeedasCR;
                vALIworkTable[intTableIndex].ALIData.intNumberOfInitialBids++;
                break;
           default:
                Transmit_Data(ALI, intAliPortPair, (char*) vALIworkTable[intTableIndex].ALIData.stringAliRequest.c_str(), vALIworkTable[intTableIndex].ALIData.stringAliRequest.length(),j);
                vALIworkTable[intTableIndex].ALIData.boolALIRecordACKReceived[j] = false;
                vALIworkTable[intTableIndex].ALIData.intLastDatabaseBid = *intALiDatabaseNumber;
                vALIworkTable[intTableIndex].ALIData.bool_LF_AS_CR = ALI_Steering[ *intALiDatabaseNumber ].boolTreatLinefeedasCR;
                vALIworkTable[intTableIndex].ALIData.intNumberOfInitialBids++;
                break;
          }
        }

      }// end for (int j = 1; j <= 2; j++)

     for (int j = 1; j <= 2; j++)
      {
       if((!ALIPort[intAliPortPair][j].boolReducedHeartBeatMode)&&(!ALIPort[intAliPortPair][j].boolPhantomPort))
        {
         ALIPort[intAliPortPair][j].fSetPortActive();
         vALIworkTable[intTableIndex].ALIData.boolALIRecordActive = true;
         objPortData.fClear();
         objPortData.fLoadPortData(ALI,intAliPortPair,j);
         vALIworkTable[intTableIndex].ALIData.intALIPortPairUsedtoTX = intAliPortPair;
         vALIworkTable[intTableIndex].ALIData.eJSONEventCode = ALI_BID_REQUEST;
         switch (vALIworkTable[intTableIndex].ALIData.enumALIBidType)
          {
           case INITIAL: 
                strMessage = ALI_MESSAGE_222;
                if (ALIPort[intAliPortPair][j].ePortConnectionType == TCP) {strMessage = ALI_MESSAGE_222a;}
                iLogcode = 222;
                strTrunkinRequest = vALIworkTable[intTableIndex].ALIData.strALIRequestTrunk = vALIworkTable[intTableIndex].CallData.stringTrunk; 
                break;
           case MANUAL:
                strMessage = ALI_MESSAGE_269;
                if (ALIPort[intAliPortPair][j].ePortConnectionType == TCP) {strMessage = ALI_MESSAGE_269;} 
                iLogcode = 269;
                strTrunkinRequest = vALIworkTable[intTableIndex].ALIData.strALIRequestTrunk = vALIworkTable[intTableIndex].CallData.stringTrunk; 
                break;
           case REBID:
                strMessage = ALI_MESSAGE_270;
                if (ALIPort[intAliPortPair][j].ePortConnectionType == TCP) {strMessage = ALI_MESSAGE_270a;}
                iLogcode = 270; 
                strTrunkinRequest = vALIworkTable[intTableIndex].ALIData.strALIRequestTrunk = strREPEAT_ALI_BID_TRUNK;
                break; 
           case AUTO:
                strMessage = ALI_MESSAGE_271;
                if (ALIPort[intAliPortPair][j].ePortConnectionType == TCP) {strMessage = ALI_MESSAGE_271a;} 
                iLogcode = 271; 
                strTrunkinRequest = vALIworkTable[intTableIndex].ALIData.strALIRequestTrunk = strREPEAT_ALI_BID_TRUNK;
                break;
           default:
                SendCodingError("ali.cpp - Coding Error in Process_ALI_Response_Received() bid type = "+int2str(vALIworkTable[intTableIndex].ALIData.enumALIBidType));                
                strTrunkinRequest = "XX";
                break;
          }
 



         switch(ALIPort[intAliPortPair][j].eALIportProtocol)
          {
           case ALI_E2:
                objMessage.fMessage_Create(LOG_CONSOLE_FILE,iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[intTableIndex].fCallData(), 
                                           objBLANK_TEXT_DATA,objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[intTableIndex].ALIData, strMessage, 
                                           int2strLZ(vALIworkTable[intTableIndex].ALIData.intALIBidIndex),
                                           int2strLZ(*intALiDatabaseNumber),int2strLZ(intAliPortPair), int2str(vALIworkTable[intTableIndex].ALIData.intTransactionIDBid[j]));
                break;
           default:
                objMessage.fMessage_Create(LOG_CONSOLE_FILE,iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,vALIworkTable[intTableIndex].fCallData(), 
                                           objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[intTableIndex].ALIData, strMessage, 
                                           vALIworkTable[intTableIndex].ALIData.stringALiRequestKey,
                                           int2strLZ(vALIworkTable[intTableIndex].ALIData.intALIBidIndex), strTrunkinRequest, 
                                           int2strLZ(*intALiDatabaseNumber),int2strLZ(intAliPortPair) );
                break;
          }

         enQueue_Message(ALI,objMessage);
        }
      }// end for (int j = 1; j <= 2; j++)

     // record port pair used into table
     vALIworkTable[intTableIndex].ALIData.intALIPortPairUsedtoTX = intAliPortPair;

     // set time in worktable
     clock_gettime(CLOCK_REALTIME, &vALIworkTable[intTableIndex].timespecTimeRecordReceivedStamp);

     // this causes the starting search point to shift
  //   *intALiDatabaseNumber +=1;

}
bool InitializeALIPortPairRotate(int intAliPortPairRotater[])
{
 // internal function to ALI thread
 for (int i=1; i < intMAX_ALI_DATABASES; i++){intAliPortPairRotater[i] = 0;}
 return true;
}

bool ShutDownALIservicePorts(pthread_t ALiServicePortListenThread[], int numberofports)
{
 extern ExperientCommPort                        	ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ]; 

 for (int i = 1; i<= numberofports; i++) {
  ALIServicePort[i].TCP_Server_Port.Shutdown();
  ALIServicePort[i].UDP_Port.SetActive(false);
  ALIServicePort[i].TCP_Port.Disconnect();
 }
 for (int i = 1; i<= numberofports; i++) {ALIServicePort[i].boolRestartListenThread = true;}

 for (int i = 1; i<= numberofports; i++) { pthread_join( ALiServicePortListenThread[i], NULL); }  

 return true;
}

bool ShutDownALIlegacyPorts(pthread_t ALiPortAListenThread[], pthread_t ALiPortBListenThread[], int numberofports)
{
 extern ExperientCommPort                        	ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];; 

 for (int i = 1; i<= numberofports; i++) {
  for (int j = 1; j <= 2; j++) {
   ALIPort[i][j].TCP_Server_Port.Shutdown();
   ALIPort[i][j].UDP_Port.SetActive(false);
   ALIPort[i][j].TCP_Port.Disconnect();  
   ALIPort[i][j].boolRestartListenThread = true;
  }
 }

 for (int i = 1; i<= numberofports; i++) { 
  pthread_join( ALiPortAListenThread[i], NULL);
  pthread_join( ALiPortBListenThread[i], NULL);
 }  

 return true;
}

bool InitializeALIservicePorts() {
 extern int						intNUM_ALI_SERVICE_PORTS;
 extern ExperientCommPort                        	ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ]; 
 extern Initialize_Global_Variables             	INIT_VARS;
/*
 int			intPORT_NUMBER;
 int			intHEARTBEAT_INTERVAL_SEC;
 int			intPORT_DOWN_THRESHOLD_SEC;
 int			intPORT_DOWN_REMINDER_SEC;
 int			intREMOTE_PORT_NUMBER;
 int			intLOCAL_PORT_NUMBER;
 IP_Address		REMOTE_IP_ADDRESS;
 string                 strNotes;
 bool                   boolPhantomPort;
 string			strVendorEmailAddress;
 Port_Connection_Type   ePortConnectionType;
 Port_ALI_Protocol_Type eALIportProtocol;
 ali_format             eRequestKeyFormat;
*/

 for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++) {

//  ALIServicePort[i].intHEARTBEAT_INTERVAL_SEC = ALI_SERVICE_PORT_VARS[i].intHEARTBEAT_INTERVAL_SEC;
  ALIServicePort[i].intRemotePortNumber       = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].intREMOTE_PORT_NUMBER;
  ALIServicePort[i].intLocalPort              = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].intLOCAL_PORT_NUMBER;
  ALIServicePort[i].boolPhantomPort           = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].boolPhantomPort;
  ALIServicePort[i].ePortConnectionType       = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].ePortConnectionType;
  ALIServicePort[i].eALIportProtocol          = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].eALIportProtocol;
  ALIServicePort[i].eRequestKeyFormat         = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].eRequestKeyFormat;
  ALIServicePort[i].RemoteIPAddress           = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].REMOTE_IP_ADDRESS;

  ALIServicePort[i].strNotes                  = INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].strNotes;
  ALIServicePort[i].fLoad_Vendor_Email_Address(INIT_VARS.ALIinitVariable.ALI_SERVICE_PORT_VARS[i].strVendorEmailAddress); 
  ALIServicePort[i].fInitializeALIport(i,3); 
 } 
return true;
}
bool InitializeALIlegacyPorts()
{
 extern int						intNUM_ALI_PORT_PAIRS;
 extern ExperientCommPort                        	ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ]; 
 extern Initialize_Global_Variables             	INIT_VARS;
 
 for(int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)  {
   for (int j = 1; j <=2; j++) {

    ALIPort[i] [j].intRemotePortNumber       = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].intREMOTE_PORT_NUMBER;
    ALIPort[i] [j].intLocalPort              = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].intLOCAL_PORT_NUMBER;
    ALIPort[i] [j].boolPhantomPort           = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].boolPhantomPort;
    ALIPort[i] [j].ePortConnectionType       = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].ePortConnectionType;
    ALIPort[i] [j].eALIportProtocol          = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].eALIportProtocol;
    ALIPort[i] [j].eRequestKeyFormat         = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].eRequestKeyFormat;
    ALIPort[i] [j].RemoteIPAddress           = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].REMOTE_IP_ADDRESS;
    ALIPort[i] [j].strNotes                  = INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].strNotes;
    ALIPort[i] [j].fLoad_Vendor_Email_Address(INIT_VARS.ALIinitVariable.ALI_PORT_VARS[i] [j].strVendorEmailAddress);
    ALIPort[i] [j].fInitializeALIport(i,j);
   }
 }
return true;
}

bool StartALIServiceListenThreads(pthread_t ALiServicePortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t paramsS[])
{
 // internal function to ALI thread
 extern int						intNUM_ALI_SERVICE_PORTS;
 int							intRC;

  if (intNUM_ALI_SERVICE_PORTS > intMAX_ALI_SERVICE_PORTS) {SendCodingError("ali.cpp ->StartALIServiceListenThreads() -> Number of ALI SVC ports > MAX!"); return false;}
  for (int i = 1; i<= intNUM_ALI_SERVICE_PORTS; i++) 
   {
    paramsS[i].intPassedIn = i;
    intRC = pthread_create(&ALiServicePortListenThread[i], pthread_attr_tAttr, *Ali_Service_Port_Listen_Thread, (void*) &paramsS[i]);
    if (intRC) {SendCodingError("ali.cpp ->StartALIServiceListenThreads() -> Unable to start ALI Service thread!"); return false;}
   }
return true;
}

bool StartALIportListenThreads(pthread_t ALiPortAListenThread[], pthread_t ALiPortBListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t paramsA[], param_t paramsB[])
{
 // internal function to ALI thread
 extern int						intNUM_ALI_PORT_PAIRS;
 int							intRC;
 string							strErrorString;

 if (intNUM_ALI_PORT_PAIRS > intMAX_ALI_PORT_PAIRS) {SendCodingError("ali.cpp ->StartALIportListenThreads() -> Number of ALI ports > MAX!"); return false;}
 
  for (int i = 1; i<= intNUM_ALI_PORT_PAIRS; i++)
   {
    paramsA[i].intPassedIn = paramsB[i].intPassedIn = i;
    intRC = pthread_create(&ALiPortAListenThread[i], pthread_attr_tAttr, *Ali_Port_A_Listen_Thread, (void*) &paramsA[i]);
    if (intRC) {
     strErrorString = "ali.cpp ->StartALIportListenThreads() -> Unable to start ALI Port "; 
     strErrorString += int2strLZ(i); strErrorString += " A Listen Thread!";
     SendCodingError(strErrorString); 
     return false;
    }
    intRC = pthread_create(&ALiPortBListenThread[i], pthread_attr_tAttr, *Ali_Port_B_Listen_Thread, (void*) &paramsB[i]);
    if (intRC) {
     strErrorString = "ali.cpp ->StartALIportListenThreads() -> Unable to start ALI Port "; 
     strErrorString += int2strLZ(i); strErrorString += " B Listen Thread!";
     SendCodingError(strErrorString);
     return false; 
    }
   }
return true;
}

bool ProcessALIserviceData(DataPacketIn DataIn)
{
 if (!DataIn.boolALIservicePacket) {return false;}
 
 XMLNode			MainNode, EventNode, WorkingNode, ALIBodyNode, LegacyRecordNode, ClassOfServiceNode;
 XMLResults			xe;
 string                         strMessage;
 MessageClass                   objMessage;
 Port_Data                      objPortData;
 string                         strACKType;
 bool                           boolHeartbeatACK = false;
 int                            iTableIndex;
 char*                          cptrResponse = NULL;
 int*                           iptrSize = 0;
 string                         strResponse;
 int                            iLogcode;
 ExperientDataClass             objExpData;
 string                         strLegacyRecord;
 string                         strLegacyALIText;
 extern vector <string>		vALI_XML_KEYS;
 extern ExperientDataClass	objQALIData;
 ALI_Data                       objALIData;
 ALISteering                    objALISteering;

 objPortData.fLoadPortData(ALI, DataIn.intPortNum, 3);
 MainNode = XMLNode::parseString(DataIn.stringDataIn.c_str(),NULL,&xe);
 if (xe.error)
  {
    objMessage.fMessage_Create(LOG_WARNING,272, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                               objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               ALI_MESSAGE_272, XMLNode::getError(xe.error),
                               DataIn.stringDataIn,"","","","", NORMAL_MSG, NEXT_LINE); 
   enQueue_Message(ALI,objMessage);
   return true;
  }        

 if (MainNode.isEmpty())
  {
   objMessage.fMessage_Create(LOG_WARNING,272, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              ALI_MESSAGE_272, "Main XML Node Empty",
                              DataIn.stringDataIn,"","","","", NORMAL_MSG, NEXT_LINE); 
   enQueue_Message(ALI,objMessage);
   return true;
  } 

 EventNode = MainNode.getChildNode(XML_NODE_EVENT);
 if (EventNode.isEmpty())     
 {
   objMessage.fMessage_Create(LOG_WARNING,272, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              ALI_MESSAGE_272, "Event XML Node Empty",
                              DataIn.stringDataIn,"","","","", NORMAL_MSG, NEXT_LINE); 
   enQueue_Message(ALI,objMessage);
   return true;
  } 


 for (unsigned int i = 0; i < vALI_XML_KEYS.size(); i++)
  {
   WorkingNode = EventNode.getChildNode( vALI_XML_KEYS[i].c_str() ); 
   if (WorkingNode.isEmpty()) {continue;}

   switch (i)
    {
     case 0: //ACK Recieved
            if (!ALIServicePort[DataIn.intPortNum].boolFirstACKRcvd)
             {
              ALIServicePort[ DataIn.intPortNum].boolFirstACKRcvd = true;
              objMessage.fMessage_Create(LOG_INFO,210, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_210); 
              enQueue_Message(ALI,objMessage);
             }
            // Determine Heartbeat ACK or Record ACK ...

           if (WorkingNode.getAttribute(XML_FIELD_TYPE)) {strACKType = WorkingNode.getAttribute(XML_FIELD_TYPE);}

           if (strACKType == XML_VALUE_HEARTBEAT) {boolHeartbeatACK = true;}
           switch(boolHeartbeatACK)
            {
             case true:
                  if ((int) DataIn.intTransactionID != ALIServicePort[ DataIn.intPortNum].intHBTransactionID) { SendCodingError( "ali.cpp fn  ProcessALIserviceData() HB TransactionId Mismatch"); }
                  ALIServicePort[ DataIn.intPortNum].boolHeartbeatActive = false; 
                  break;

             case false:
                  iTableIndex = Find_Table_Index_from_Bid_Index(DataIn.intTransactionID);
                  if (iTableIndex < 0) {SendCodingError( "ali.cpp fn  ProcessALIserviceData() ALI Record ACK TransactionId Mismatch"); break;}
                  vALIworkTable[iTableIndex].ALIData.boolALIRecordACKReceived[1] = vALIworkTable[iTableIndex].ALIData.boolALIRecordACKReceived[2] = true;
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE,209, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(),
                                             objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA,
                                             vALIworkTable[iTableIndex].ALIData,ALI_MESSAGE_209s, int2strLZ(DataIn.intTransactionID)); 
                  enQueue_Message(ALI,objMessage); 
                  break;
            }
           if(ALIServicePort[DataIn.intPortNum].boolReducedHeartBeatMode) {ALIServicePort[DataIn.intPortNum].fClearReducedHeartbeatMode();}  
           clock_gettime(CLOCK_REALTIME, &ALIServicePort[DataIn.intPortNum].timespecTimeLastHeartbeat);       
           break;
        
     case 1: //ALI Data

            ALIBodyNode = WorkingNode.getChildNode( XML_NODE_ALIBODY ); 
            if (ALIBodyNode.isEmpty()) {SendCodingError("ali.cpp: ProcessALIserviceData() I3 ALIBody XML Node Missing!"); break;}

            objALIData.I3Data.fLoadI3ALIData(ALIBodyNode);
            cptrResponse = ALIBodyNode.createXMLString(0, iptrSize);
            if (cptrResponse != NULL) 
             {
              strResponse = cptrResponse;
              free(cptrResponse); 
             }
            cptrResponse = NULL;
            cout << "ali.cpp case1 load bidID -> " << objALIData.I3Data.I3XMLData.CallInfo.strCallingPartyNum << endl;
            objALIData.I3Data.fLoadBidID(objALIData.I3Data.I3XMLData.CallInfo.strCallingPartyNum); //fred
            objALIData.I3Data.fCreateLegacyALIrecord();
            objALIData.I3Data.fCreateLegacyALI30Xrecord();
            objALIData.I3Data.fCreateNextGenALIrecord();
           
            switch(enumI3CONVERSION_TYPE) {
             case ALI30W: 
                  strLegacyALIText = objALIData.I3Data.strALI30WRecord; 
                  strLegacyRecord  = charSTX + objALIData.I3Data.strALI30WRecord + charETX; break;

             case ALI30X:
                  strLegacyALIText = objALIData.I3Data.strALI30XRecord; 
                  strLegacyRecord = charSTX + objALIData.I3Data.strALI30XRecord + charETX; break;

             case NEXTGEN: 
                  strLegacyRecord = charSTX + objALIData.I3Data.strNGALIRecord + charETX;     
                  break; 
            }


            objExpData.fLoad_ALI_Type(strLegacyRecord);

            iTableIndex = Find_Table_Index_from_Bid_Index(DataIn.intTransactionID);
            if (iTableIndex < 0) 
             {
              DataIn.stringDataIn = strLegacyRecord;

              if (Found_TranscationID_In_DisconnectVector(DataIn, objExpData, objPortData)) {return true;}
              else                                                                          {SendUnMatchedTransactionID(3, DataIn.intTransactionID, DataIn, objPortData); return false;}
              // should not get here !
              SendCodingError("ali.cpp: ProcessALIserviceData() Should never execute this instruction!");
              return false;
             }
            
            vALIworkTable[iTableIndex].ALIData.stringAliText = strLegacyALIText;
            vALIworkTable[iTableIndex].ALIData.I3Data = objALIData.I3Data;

            switch  (vALIworkTable[iTableIndex].ALIData.enumALIBidType)
             {
              case NO_BID:  case H_BEAT:  case LATE_RECORD: 
                   strMessage =  ALI_MESSAGE_265; 
                   iLogcode= 265; 
                   SendCodingError("ali.cpp - Coding Error in Process_ALI_Response_Received() bid type = "+int2str(vALIworkTable[iTableIndex].ALIData.enumALIBidType)); 
                   break;
              case INITIAL: strMessage =  ALI_MESSAGE_226; iLogcode = 226; break;
              case REBID:   strMessage =  ALI_MESSAGE_266; iLogcode = 266; break;
              case MANUAL:  strMessage =  ALI_MESSAGE_267; iLogcode = 267; break; 
              case AUTO:    strMessage =  ALI_MESSAGE_268; iLogcode = 268; break;
             }
            objMessage.fMessage_Create(LOG_CONSOLE_FILE, iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA,
                                       objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[iTableIndex].ALIData, strMessage, strLegacyRecord, "", "", "", "", "",  NORMAL_MSG, ALI_RECORD);
            objExpData.stringTimeOfEvent = objMessage.stringTimeStamp;
            enQueue_Message(ALI,objMessage);

            objExpData                                        = vALIworkTable[iTableIndex];
            objExpData.enumThreadSending                      = ALI;
            objExpData.ALIData.boolALIRecordReceived          = true;
            objExpData.fSet_Trunk(vALIworkTable[iTableIndex].CallData.intTrunk);
            // objALIData.ALIData.intLastDatabaseBid             = vALIworkTable[iTableIndex].ALIData.intLastDatabaseBid;
            objExpData.fLoadCallbackfromALI();

            // push ALI Record onto main Q
            objQALIData = objExpData;
            enqueue_Main_Input(ALI,objQALIData);
//            sem_wait(&sem_tMutexMainInputQ);
//            queue_objMainData.push(objExpData);
//            sem_post(&sem_tMutexMainInputQ);
         
            //delete table record .......
            vALIworkTable.erase(vALIworkTable.begin()+iTableIndex); 
            break;

     case 2: //ALI_RECORD_FAIL
     //       cout << "got ALI record fail!" << endl;

           iTableIndex = Find_Table_Index_from_Bid_Index(DataIn.intTransactionID);
           if (iTableIndex < 0) 
            {
             DataIn.stringDataIn = strALI_FAILED_MESSAGE;
             if (Found_TranscationID_In_DisconnectVector(DataIn, objExpData, objPortData)) {return true;}
             else                                                                          {return false;}
             return false;
            }

           if (WorkingNode.getText()) { strMessage = WorkingNode.getText();}
           else                       { strMessage = "Unknown";}

           objMessage.fMessage_Create(LOG_WARNING,221, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA, 
                                      objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[iTableIndex].ALIData,ALI_MESSAGE_221, strMessage);
           enQueue_Message(ALI,objMessage);



           SendALIRecordFailToMain(iTableIndex);
           //delete table record .......
           vALIworkTable.erase(vALIworkTable.begin()+iTableIndex); 

            break;
     case 3: //EncodedLegacyRecord
            LegacyRecordNode = WorkingNode;
            if (LegacyRecordNode.getText())  {strLegacyRecord = LegacyRecordNode.getText();}             
            else {SendCodingError("ali.cpp: ProcessALIserviceData() LegacyRecordNode XML Test is Missing!"); break;}
            if (!objExpData.fLoad_Decode_ALI(XML_DATA_FIELD, ALI, strLegacyRecord, 1, DataIn.stringDataIn)) {break;}

            strLegacyRecord = objExpData.ALIData.stringAliText;
            objExpData.fLoad_ALI_Type(objExpData.ALIData.stringAliText);

            iTableIndex = Find_Table_Index_from_Bid_Index(DataIn.intTransactionID);
            if (iTableIndex < 0) 
             {
              DataIn.stringDataIn = strLegacyRecord;

              if (Found_TranscationID_In_DisconnectVector(DataIn, objExpData, objPortData)) {return true;}
              else                                                                          {SendUnMatchedTransactionID(3, DataIn.intTransactionID, DataIn, objPortData); return false;}
              // should not get here !
              SendCodingError("ali.cpp: ProcessALIserviceData() Should never execute this instruction!");
              return false;
             }
            
            //vALIworkTable[iTableIndex].ALIData.stringAliText = strLegacyRecord;
            if (strLegacyRecord.length() < 2) {SendCodingError("ali.cpp - Coding error in fn ProcessALIserviceData()");return false;}
            //remove stx and etx
            vALIworkTable[iTableIndex].ALIData.stringAliText.assign(strLegacyRecord,1,strLegacyRecord.length()-2);
  //          vALIworkTable[iTableIndex].ALIData.I3Data = objALIData.I3Data;

            switch  (vALIworkTable[iTableIndex].ALIData.enumALIBidType)
             {
              case NO_BID:  case H_BEAT:  case LATE_RECORD: 
                   strMessage =  ALI_MESSAGE_265; 
                   iLogcode= 265; 
                   SendCodingError("ali.cpp - Coding Error in Process_ALI_Response_Received() bid type = "+int2str(vALIworkTable[iTableIndex].ALIData.enumALIBidType)); 
                   break;
              case INITIAL: strMessage =  ALI_MESSAGE_226; iLogcode = 226; break;
              case REBID:   strMessage =  ALI_MESSAGE_266; iLogcode = 266; break;
              case MANUAL:  strMessage =  ALI_MESSAGE_267; iLogcode = 267; break; 
              case AUTO:    strMessage =  ALI_MESSAGE_268; iLogcode = 268; break;
             }
            objMessage.fMessage_Create(LOG_CONSOLE_FILE, iLogcode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vALIworkTable[iTableIndex].fCallData(), objBLANK_TEXT_DATA,
                                       objPortData, objBLANK_FREESWITCH_DATA, vALIworkTable[iTableIndex].ALIData, strMessage, strLegacyRecord, "", "", "", "", "",  NORMAL_MSG, ALI_RECORD);
            objExpData.stringTimeOfEvent = objMessage.stringTimeStamp;
            enQueue_Message(ALI,objMessage);

            objExpData                                        = vALIworkTable[iTableIndex];
            objExpData.enumThreadSending                      = ALI;
            objExpData.ALIData.boolALIRecordReceived          = true;
            objExpData.fSet_Trunk(vALIworkTable[iTableIndex].CallData.intTrunk);
            // objALIData.ALIData.intLastDatabaseBid             = vALIworkTable[iTableIndex].ALIData.intLastDatabaseBid;
            
            // Get Class of Service
            ClassOfServiceNode = WorkingNode.getChildNode(XML_NODE_COS);
            if (!ClassOfServiceNode.isEmpty()) {
             objALISteering.fLoadClassofServiceData(ClassOfServiceNode);           
            }              


            objExpData.fLoadCallbackfromALI(objALISteering);
 //           cout << "callback is -> " << objExpData.CallData.intCallbackNumber << endl;
            // push ALI Record onto main Q
            objQALIData = objExpData;
            enqueue_Main_Input(ALI,objQALIData);
//            sem_wait(&sem_tMutexMainInputQ);
//            queue_objMainData.push(objExpData);
//            sem_post(&sem_tMutexMainInputQ);
         
            //delete table record .......
            vALIworkTable.erase(vALIworkTable.begin()+iTableIndex); 
            break;



            break;
     default:
          SendCodingError( "ali.cpp fn  ProcessALIserviceData() switch(i)");    
    }
  }




 return true;
}

