/*****************************************************************************
* FILE: sms_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the SMS_Data Class 
*
*
*
* AUTHOR: 08/17/2012 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"


SMS_Data::SMS_Data()
{
 strSMSmessage.clear();
 objLocationURI.fClear();
 strBidId.clear();
 strFromUser.clear();
 strFromHost.clear();
 strToUser.clear();
 strToHost.clear();
 strConversation.clear();
}

void SMS_Data::fClear()
{
 strSMSmessage.clear();
 objLocationURI.fClear();
 strBidId.clear();
 strFromUser.clear();
 strFromHost.clear();
 strToUser.clear();
 strToHost.clear();
 strConversation.clear();
}

bool SMS_Data::CIVICA_TEST()
{
 if (objLocationURI.strURI == "CIVICA_TEST.COM") {return true;}
return false;
}


bool SMS_Data::fLoadLocationData(string strInput)
{
 size_t StartPosition;
 size_t EndPosition;
 string strOutput;
 string strContent;

 strContent = Return_Content(strInput);
 
 StartPosition = strContent.find("<L><");
 if (StartPosition == string::npos)                   {return false;}
 EndPosition = strContent.find("><l>", StartPosition);
 if (EndPosition == string::npos)                     {return false;}

 strOutput.assign(strContent,StartPosition+4,(EndPosition - StartPosition - 4));
 //remove leading blanks ...
 objLocationURI.strURI = RemoveLeadingSpaces(strOutput);

 return true;
}

bool SMS_Data::fLoadUUIDinBidID(string strInput)
{
 strBidId.clear();

 strBidId = ParseFreeswitchData(strInput, FREESWITCH_CLI_CHANNEL_CALL_UUID_WO_COLON);
 
 if (strBidId.empty()) {return false;}

 return true;
}



bool SMS_Data::fLoadBidID(string strInput)
{
 size_t StartPosition;
 size_t EndPosition;
 string strOutput;
 string strContent;

 strContent = Return_Content(strInput);
 
 StartPosition = strContent.find("<B>");
 if (StartPosition == string::npos)                   {return false;}
 EndPosition = strContent.find("<b>", StartPosition);
 if (EndPosition == string::npos)                     {return false;}

 strOutput.assign(strContent,StartPosition+3,(EndPosition - StartPosition - 3));
 //remove leading blanks ...
 strBidId = RemoveLeadingSpaces(strOutput);

 return true;
}

bool SMS_Data::fLoadSMSmessage(string strInput)
{
 size_t EndPosition;
 string strOutput;
 string strContent;

 strContent = Return_Content(strInput);

 EndPosition = strContent.find("<L>");
 if (EndPosition == string::npos) {
  strSMSmessage = strContent;
 }
 else {
  strSMSmessage = strContent.substr(0, EndPosition);
 }

 return true;
}

bool SMS_Data::fLoadTo(string strInput)
{
 string	 strUser;
 string  strHost;

 strToUser = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_SMS_TO_USER_WO_COLON ));
 if(strToUser.empty()) {return false;}
 strToHost = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_SMS_TO_HOST_WO_COLON ));
 if(strToHost.empty()) {return false;}

 return true;
}

bool SMS_Data::fLoadFrom(string strInput)
{
 string	 strUser;
 string  strHost;

 strFromUser = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_SMS_FROM_USER_WO_COLON ));
 if(strFromUser.empty()) {return false;}
 strFromHost = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_SMS_FROM_HOST_WO_COLON ));
 if(strFromHost.empty()) {return false;}

 return true;
}

bool SMS_Data::CreateCallerSays()
{
 string Preamble;


 Preamble = Create_Message(TDD_CALLER_SAYS_PREFIX, get_Local_Time_Stamp());
 
// strSMSmessage =  "From " + strFromUser  + " [" +  get_Local_Time_Stamp() +"]:\r\n" + strSMSmessage + "\r" +"\n";

strSMSmessage = Preamble + "\r\n" + strSMSmessage +"\r\n";

 //fAddSentenceToTDDConversation(TDDdata.strTDDstring);
// if(TDDdata.strTDDstring.length() > ((size_t)intTDD_CPS_THRESHOLD)) {TDDdata.boolTDDthresholdMet = true;}
// TDDdata.strTDDCallerBuffer.clear();
 
 return true;
}




/*
bool SMS_Data::fLoadSMSConversation(string strMessage, bool IsEncoded)
{
 XMLParserBase64Tool    b64;
 unsigned char*         ptrUcharDecodedTXT; 
 int                    intLength;
 XMLError               xe;
 string                 strError;
 string                 strTemp = "";
 
 if (strMessage.empty()){ b64.freeBuffer();return false;}
 if (IsEncoded)
  {
   ptrUcharDecodedTXT    = b64.decode(strTDDchar.c_str(),&intLength, &xe);
   if(xe != eXMLErrorNone){ b64.freeBuffer();return false;}
   for (int i=0; i < intLength; i++){strTemp += ptrUcharDecodedTXT[i];}
  }
 else {strTemp = strMessage;}

 if      (strTemp == "\\r") {strTemp = charCR;}
 else if (strTemp == "\\n") {strTemp = charLF;}

 if ((!IsString)&&(strTemp.length()>1)){ b64.freeBuffer(); return false;}
 else if (IsString) {strTDDstring    = strTemp;}
 else               
  {
   strTDDcharacter = strTemp;
   fAddCharacterToBuffer();
  }
 b64.freeBuffer();
 return true;
}
*/

void TDD_Data::fClear()
{
 boolTDDWindowActive             = false;                   
 boolTDDSendButton               = false;
 boolTDDmute                     = false;
 boolTDDthresholdMet             = false;
 strTDDstring                    = "";
 strTDDCallerBuffer              = "";   
 strTDDcharacter                 = "";
 strTDDconversation              = "";
 timespecTimeTDDcharRecv.tv_sec  = 0;
 timespecTimeTDDcharRecv.tv_nsec = 0;
}

void TDD_Data::fSetWindow(bool SetActive)
{
 boolTDDWindowActive =  SetActive;
 fSetTDDSendButton(); 
}

bool TDD_Data::fSetTDDSendButton()
{
 struct timespec timespecTimeNow;
 long double     ldblTimeDiff;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 ldblTimeDiff = time_difference(timespecTimeNow, timespecTimeTDDcharRecv);
 if (ldblTimeDiff > intTDD_CPS_THRESHOLD_SEC) {boolTDDSendButton = true;}
 else                                         {boolTDDSendButton = false;}
 return boolTDDSendButton;
}

void TDD_Data::fLoad_TDDmute(string stringArg)
{
 // if invalid data TDD mute will be set to off 

 if (stringArg == XML_VALUE_ON) {boolTDDmute = true;}
 else                           {boolTDDmute = false;}
}

void TDD_Data::fAddCharacterToBuffer()
{
 // don't add cr/lf reset buffer time so that it will flush
 if ((strTDDcharacter == "\n")||(strTDDcharacter == "\r"))
  {
   timespecTimeTDDcharRecv.tv_sec  = 0;
   timespecTimeTDDcharRecv.tv_nsec = 0;
  }
 else if (strTDDcharacter[0] == charBKSP)
  {
   if (!strTDDCallerBuffer.empty()){strTDDCallerBuffer.erase(strTDDCallerBuffer.end()-1);}
   clock_gettime(CLOCK_REALTIME,&timespecTimeTDDcharRecv);
  }
 else
  {
   strTDDCallerBuffer += strTDDcharacter;
   clock_gettime(CLOCK_REALTIME,&timespecTimeTDDcharRecv);
  }
}

bool TDD_Data::fLoadTDDReceived(string strTDDchar, bool IsString, bool IsEncoded)
{
 XMLParserBase64Tool    b64;
 unsigned char*         ptrUcharDecodedTXT; 
 int                    intLength;
 XMLError               xe;
 string                 strError;
 string                 strTemp = "";
 
 if (strTDDchar.empty()){ b64.freeBuffer();return false;}
 if (IsEncoded)
  {
   ptrUcharDecodedTXT    = b64.decode(strTDDchar.c_str(),&intLength, &xe);
   if(xe != eXMLErrorNone){ b64.freeBuffer();return false;}
   for (int i=0; i < intLength; i++){strTemp += ptrUcharDecodedTXT[i];}
  }
 else {strTemp = strTDDchar;}

 if      (strTemp == "\\r") {strTemp = charCR;}
 else if (strTemp == "\\n") {strTemp = charLF;}

 if ((!IsString)&&(strTemp.length()>1)){ b64.freeBuffer(); return false;}
 else if (IsString) {strTDDstring    = strTemp;}
 else               
  {
   strTDDcharacter = strTemp;
   fAddCharacterToBuffer();
  }
 b64.freeBuffer();

 return true;
}

bool TDD_Data::CreateCallerSays()
{
 string Preamble;

 Preamble = Create_Message(TDD_CALLER_SAYS_PREFIX, get_Local_Time_Stamp());
 
 strTDDstring = Preamble + "\r\n" + strTDDstring +"\r\n";

 return true;
}

void Text_Data::fClear()
{
 TextType = NO_TEXT_TYPE;
 TDDdata.fClear();
 SMSdata.fClear();
 boolTextWindowActive = false;                   
 boolTextSendButton   = false;
 strConversation.clear();
}

void Text_Data::fSetWindow(bool SetActive)
{
 boolTextWindowActive =  SetActive;
 fSetTextSendButton(); 
}

bool Text_Data::fSetTextSendButton()
{
 switch(this->TextType)
  {
   case NO_TEXT_TYPE: 
         this->boolTextSendButton = false;
         break;
   case TDD_MESSAGE:
         this->boolTextSendButton = this->TDDdata.fSetTDDSendButton();
         break;
   case MSRP_MESSAGE:
         this->boolTextSendButton = true;
         break;
  }   
 return boolTextSendButton;
}
bool Text_Data::fLoadTextType(string strArg)
{
 if      (strArg == "TDD")  {this->TextType = TDD_MESSAGE;  return true;}
 else if (strArg == "tdd")  {this->TextType = TDD_MESSAGE;  return true;}
 else if (strArg == "MSRP") {this->TextType = MSRP_MESSAGE; return true;}  
 else if (strArg == "msrp") {this->TextType = MSRP_MESSAGE; return true;} 
 else                       {this->TextType = NO_TEXT_TYPE;} 
return false;
}

bool SMS_Data::fLoadMSRPdispatcherSays(string strText, bool IsEncoded)
{
 XMLParserBase64Tool    b64;
 unsigned char*         ptrUcharDecodedTXT; 
 int                    intLength;
 XMLError               xe;
 string                 strError;
 string                 strTemp = "";
 string                 strReturnText = "";
 
 if (strText.empty()){ b64.freeBuffer();return false;}
 if (IsEncoded)
  {
   ptrUcharDecodedTXT    = b64.decode(strText.c_str(),&intLength, &xe);
   if(xe != eXMLErrorNone){ b64.freeBuffer();return false;}
   for (int i=0; i < intLength; i++){strTemp += ptrUcharDecodedTXT[i];}
  }
 else {strTemp = strText;}

 if      (strTemp == "\\r") {strTemp = charCR;}
 else if (strTemp == "\\n") {strTemp = charLF;}

 this->strSMSmessage    = strTemp;
 b64.freeBuffer();

 return true;
}




