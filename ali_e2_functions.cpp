/*****************************************************************************
* FILE: ali_e2_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the ALI_E2_Digits Class 
*
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/


#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"


#include "/datadisk1/src/xmlParser/xmlParser.h"                          // XML Software


extern Call_Data	                       	objBLANK_CALL_RECORD; 
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern Text_Data				objBLANK_TEXT_DATA;

 // Constructor
 ALI_E2_XML_Data::ALI_E2_XML_Data()
  {
   strClassofService                  = "";
   strTypeofService                   = "";
   strCellId                          = "";
   strCompanyID1                      = "";
   strCountyID                        = "";
   strCustomerName                    = "";
   strEmergencyMedicalServiceProvider = ""; 
   strEmergencyServicesNumber         = "";
   strFireDepartmentServiceProvider   = "";
   strHouseNumber                     = "";
   strHouseNumberSuffix               = "";
   strLawEnforcementServiceProvider   = "";
   strLocation                        = "";
   strMSAGCommunity                   = "";
   strPostDirectional                 = "";
   strPrefixDirectional               = "";
   strSectorID                        = "";
   strState                           = "";
   strStreetname                      = "";
   strStreetNameSuffix                = "";
  }

ALI_E2_XML_Data&  ALI_E2_XML_Data::operator= (const ALI_E2_XML_Data& b)
  {
   if (&b == this ) {return *this;}
   strClassofService                  = b.strClassofService;
   strTypeofService                   = b.strTypeofService;
   strCellId                          = b.strCellId;
   strCompanyID1                      = b.strCompanyID1;
   strCountyID                        = b.strCountyID;
   strCustomerName                    = b.strCustomerName;
   strEmergencyMedicalServiceProvider = b.strEmergencyMedicalServiceProvider; 
   strEmergencyServicesNumber         = b.strEmergencyServicesNumber;
   strFireDepartmentServiceProvider   = b.strFireDepartmentServiceProvider;
   strHouseNumber                     = b.strHouseNumber;
   strHouseNumberSuffix               = b.strLawEnforcementServiceProvider;
   strLawEnforcementServiceProvider   = b.strLawEnforcementServiceProvider;
   strLocation                        = b.strLocation;
   strMSAGCommunity                   = b.strMSAGCommunity;
   strPostDirectional                 = b.strPostDirectional;
   strPrefixDirectional               = b.strPrefixDirectional;
   strSectorID                        = b.strSectorID;
   strState                           = b.strState;
   strStreetname                      = b.strStreetname;
   strStreetNameSuffix                = b.strStreetNameSuffix;
   return *this;
  }

 ALI_E2_Digits::ALI_E2_Digits()
  {
   chTypeofDigits             = 0x00;  
   chNatureofNumber           = 0x00; 
   chNumberingPlanandEncoding = 0x00;
   chNumberofDigits           = 0x00;
   chDigitTwoandOne           = 0x00;
   chDigitFourandThree        = 0x00;
   chDigitSixandFive          = 0x00;
   chDigitEightandSeven       = 0x00;
   chDigitTenandNine          = 0x00;
   chDigitTwelveandEleven     = 0x00;
   chDigitFourteenandThirteen = 0x00;
   chDigitFillerandFifteen    = 0x00;
  }

 ALI_E2_Digits& ALI_E2_Digits::operator= (const ALI_E2_Digits& b)
  {
   if (&b == this ) {return *this;}
   chTypeofDigits             = b.chTypeofDigits;  
   chNatureofNumber           = b.chNatureofNumber; 
   chNumberingPlanandEncoding = b.chNumberingPlanandEncoding;
   chNumberofDigits           = b.chNumberofDigits;
   chDigitTwoandOne           = b.chDigitTwoandOne;
   chDigitFourandThree        = b.chDigitFourandThree;
   chDigitSixandFive          = b.chDigitSixandFive;
   chDigitEightandSeven       = b.chDigitEightandSeven;
   chDigitTenandNine          = b.chDigitTenandNine;
   chDigitTwelveandEleven     = b.chDigitTwelveandEleven;
   chDigitFourteenandThirteen = b.chDigitFourteenandThirteen;
   chDigitFillerandFifteen    = b.chDigitFillerandFifteen;
   return *this;
  }

  ALI_E2_Data::ALI_E2_Data()
   {
    boolValidRecord                   = false;
    chComponentType                   = 0x00;     
    chComponentId                     = 0x00;     
    chOperationFamily                 = 0x00;     
    chOperationSpecifier              = 0x00;
    chErrorCode                       = 0x00;     
    chProblemType                     = 0x00;     
    chProblemSpecifier                = 0x00;     
    chPositionRequestType             = 0x00;
    chPositionResult                  = 0x00;
    chPositionInformationParameters   = 0x00;
    chPositionInformationLength       = 0x00;
    chPositionYear                    = 0x00;
    chPositionMonth                   = 0x00;
    chPositionDay                     = 0x00;
    chYear                            = 0x00;
    chMonth                           = 0x00;
    chDay                             = 0x00;
    chPositionGeoLength               = 0x00;
    chPositionGeoLPRI                 = 0x00;
    chPositionGeoScreening            = 0x00;
    chPositionGeoExt                  = 0x00;
    chPositionGeoTypeofShape          = 0x00;
    chPositionGeoLatSign              = 0x00;
    chPositionUncertaintyCode         = 0x00;
    chPositionConfidence              = 0x00;
    chPositionAltitudeSign            = 0x00;
    chPositionAltitudeUncertaintyCode = 0x00;
    chPositionSource                  = 0x00;
    chMobileCallStatus                = 0x00;
    chCompanyIdStringLength           = 0x00; 
    intLocationDescriptionLength      = 0; 
    for (int i = 0; i < 3; i++)                                    {chTimeofDay[i] = 0x00; chPositionTimeofDay[i] = 0x00;}
    for (int i = 0; i < 3; i++)                                    {chPositionGeoLatDegrees[i] = 0x00; chPositionGeoLongDegrees[i] = 0x00;}
    for (int i = 0; i < 2; i++)                                    {chPositionAltitude[i] = 0x00;} 
    for (int i = 0; i < E2_TRANSACTIONID_LENGTH; i++)              {chTransactionId[i] = 0x00;}
    for (int i = 0; i < E2_MAX_COMPANY_ID_STRING_LENGTH; i++)      {chCompanyIdString[i] = 0x00;}
    for (int i = 0; i < E2_MAX_LOCATION_DESCRIPTION_LENGTH+1; i++) {chLocationDescription[i] = 0x00;}
    CallbackNumber.fClear();
    EmergencyServicesRoutingDigits.fClear();  
    MobileIdentificationNumber.fClear();  
    InternationalMobileSubcriberIdentity.fClear();
   }

  ALI_E2_Data& ALI_E2_Data::operator= (const ALI_E2_Data& b)
   {
    if (&b == this ) {return *this;}
    boolValidRecord                   = b.boolValidRecord;
    chComponentType                   = b.chComponentType;     
    chComponentId                     = b.chComponentId;     
    chOperationFamily                 = b.chOperationFamily;     
    chOperationSpecifier              = b.chOperationSpecifier;
    chErrorCode                       = b.chErrorCode;     
    chProblemType                     = b.chProblemType;     
    chProblemSpecifier                = b.chProblemSpecifier;     
    chPositionRequestType             = b.chPositionRequestType;
    chPositionResult                  = b.chPositionResult;
    chPositionInformationParameters   = b.chPositionInformationParameters;
    chPositionInformationLength       = b.chPositionInformationLength;
    chPositionYear                    = b.chPositionYear;
    chPositionMonth                   = b.chPositionMonth;
    chPositionDay                     = b.chPositionDay;
    chYear                            = b.chYear;
    chMonth                           = b.chMonth;
    chDay                             = b.chDay;
    chPositionGeoLength               = b.chPositionGeoLength;
    chPositionGeoLPRI                 = b.chPositionGeoLPRI;
    chPositionGeoScreening            = b.chPositionGeoScreening;
    chPositionGeoExt                  = b.chPositionGeoExt;
    chPositionGeoTypeofShape          = b.chPositionGeoTypeofShape;
    chPositionGeoLatSign              = b.chPositionGeoLatSign;
    chPositionUncertaintyCode         = b.chPositionUncertaintyCode;
    chPositionConfidence              = b.chPositionConfidence;
    chPositionAltitudeSign            = b.chPositionAltitudeSign;
    chPositionAltitudeUncertaintyCode = b.chPositionAltitudeUncertaintyCode;
    chPositionSource                  = b.chPositionSource;
    chMobileCallStatus                = b.chMobileCallStatus;
    chCompanyIdStringLength           = b.chCompanyIdStringLength; 
    intLocationDescriptionLength      = b.intLocationDescriptionLength; 
    for (int i = 0; i < 3; i++)                                    {chTimeofDay[i]             = b.chTimeofDay[i]; chPositionTimeofDay[i] = b. chPositionTimeofDay[i];}
    for (int i = 0; i < 3; i++)                                    {chPositionGeoLatDegrees[i] = b.chPositionGeoLatDegrees[i]; chPositionGeoLongDegrees[i] = b.chPositionGeoLongDegrees[i];}
    for (int i = 0; i < 2; i++)                                    {chPositionAltitude[i]      = b.chPositionAltitude[i];} 
    for (int i = 0; i < E2_TRANSACTIONID_LENGTH; i++)              {chTransactionId[i]         = b.chTransactionId[i];}
    for (int i = 0; i < E2_MAX_COMPANY_ID_STRING_LENGTH; i++)      {chCompanyIdString[i]       = b.chCompanyIdString[i];}
    for (int i = 0; i < E2_MAX_LOCATION_DESCRIPTION_LENGTH+1; i++) {chLocationDescription[i]   = b.chLocationDescription[i];}
    CallbackNumber                       = b.CallbackNumber;
    EmergencyServicesRoutingDigits       = b.EmergencyServicesRoutingDigits;  
    MobileIdentificationNumber           = b.MobileIdentificationNumber;  
    InternationalMobileSubcriberIdentity = b.InternationalMobileSubcriberIdentity;
    return *this;
   } 

void SendTruncationMessage(string strField, int iLength, int iMaxLength, Port_Data objPortData)
{
 MessageClass  objMessage;

 objMessage.fMessage_Create(LOG_WARNING,258, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                            objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_258, strField, int2strLZ(iLength), int2strLZ(iMaxLength) ); 
 enQueue_Message(ALI,objMessage);
}




void ALI_E2_XML_Data::fDisplay()
{
 cout << "Class of Service                   =" << strClassofService << endl;
 cout << "Type of Service                    =" << strTypeofService << endl;
 cout << "Cell ID                            =" << strCellId       << endl;
 cout << "Company ID 1                       =" << strCompanyID1   << endl;
 cout << "County ID                          =" << strCountyID     << endl;
 cout << "Customer Name                      =" << strCustomerName << endl;
 cout << "Emergency Medical Service Provider =" << strEmergencyMedicalServiceProvider << endl;
 cout << "Emergency Services Number          =" << strEmergencyServicesNumber << endl;
 cout << "Fire Department Service Provider   =" << strFireDepartmentServiceProvider << endl;
 cout << "House Number                       =" << strHouseNumber << endl;
 cout << "House Number Suffix                =" << strHouseNumberSuffix << endl;
 cout << "Law Enforcement Service Provider   =" << strLawEnforcementServiceProvider << endl;
 cout << "Location                           =" << strLocation << endl;
 cout << "MSAG Community                     =" << strMSAGCommunity << endl;
 cout << "Post Directional                   =" << strPostDirectional << endl;
 cout << "Prefix Directional                 =" << strPrefixDirectional << endl;
 cout << "Sector ID                          =" << strSectorID << endl;
 cout << "State                              =" << strState << endl;
 cout << "Street Name                        =" << strStreetname << endl;
 cout << "Street Name Suffix                 =" << strStreetNameSuffix << endl;

}

bool ALI_E2_XML_Data::fLoadClassofService(string strInput)
{
 size_t found;
 if (strInput.length() != 1) {return false; /*error message TBC*/}
 found = strInput.find_first_not_of("0123456789ABCDEFGHIJKVW");
 if (found != string::npos)  {return false; /*error message TBC*/}

 strClassofService = strInput;
 return true;
}



string ALI_E2_XML_Data::fClassofService()
{
 extern vector <string>  vstrCLASS_OF_SERVICE_ABBV;

 if      (strClassofService == "0") {return  vstrCLASS_OF_SERVICE_ABBV[0];}
 else if (strClassofService == "1") {return  vstrCLASS_OF_SERVICE_ABBV[1];}
 else if (strClassofService == "2") {return  vstrCLASS_OF_SERVICE_ABBV[2];}
 else if (strClassofService == "3") {return  vstrCLASS_OF_SERVICE_ABBV[3];}
 else if (strClassofService == "4") {return  vstrCLASS_OF_SERVICE_ABBV[4];}
 else if (strClassofService == "5") {return  vstrCLASS_OF_SERVICE_ABBV[5];}
 else if (strClassofService == "6") {return  vstrCLASS_OF_SERVICE_ABBV[6];}
 else if (strClassofService == "7") {return  vstrCLASS_OF_SERVICE_ABBV[7];}
 else if (strClassofService == "8") {return  vstrCLASS_OF_SERVICE_ABBV[8];}
 else if (strClassofService == "9") {return  vstrCLASS_OF_SERVICE_ABBV[9];}
 else if (strClassofService == "A") {return  vstrCLASS_OF_SERVICE_ABBV[10];}
 else if (strClassofService == "B") {return  vstrCLASS_OF_SERVICE_ABBV[11];}
 else if (strClassofService == "C") {return  vstrCLASS_OF_SERVICE_ABBV[12];}
 else if (strClassofService == "D") {return  vstrCLASS_OF_SERVICE_ABBV[13];}
 else if (strClassofService == "E") {return  vstrCLASS_OF_SERVICE_ABBV[14];}
 else if (strClassofService == "F") {return  vstrCLASS_OF_SERVICE_ABBV[15];}
 else if (strClassofService == "G") {return  vstrCLASS_OF_SERVICE_ABBV[16];}
 else if (strClassofService == "H") {return  vstrCLASS_OF_SERVICE_ABBV[17];}
 else if (strClassofService == "I") {return  vstrCLASS_OF_SERVICE_ABBV[18];}
 else if (strClassofService == "J") {return  vstrCLASS_OF_SERVICE_ABBV[19];}
 else if (strClassofService == "K") {return  vstrCLASS_OF_SERVICE_ABBV[20];}
 else if (strClassofService == "V") {return  vstrCLASS_OF_SERVICE_ABBV[21];}
 else if (strClassofService == "W") {return  vstrCLASS_OF_SERVICE_ABBV[22];}
 else                               {return  vstrCLASS_OF_SERVICE_ABBV[11];}
}


void ALI_E2_XML_Data::fClear()
{
  strClassofService                  = "";
  strTypeofService                   = "";
  strCellId                          = "";
  strCompanyID1                      = "";
  strCountyID                        = "";
  strCustomerName                    = "";
  strEmergencyMedicalServiceProvider = ""; 
  strEmergencyServicesNumber         = "";
  strFireDepartmentServiceProvider   = "";
  strHouseNumber                     = "";
  strHouseNumberSuffix               = "";
  strLawEnforcementServiceProvider   = "";
  strLocation                        = "";
  strMSAGCommunity                   = "";
  strPostDirectional                 = "";
  strPrefixDirectional               = "";
  strSectorID                        = "";
  strState                           = "";
  strStreetname                      = "";
  strStreetNameSuffix                = "";
}





bool ALI_E2_XML_Data::fLoadLocationData(const char* chXMLstring, Port_Data objPortData)
{
 XMLNode            xNode, yNode;
 XMLResults         xe;
 MessageClass       objMessage;


// cout << chXMLstring << endl;


 xNode=XMLNode::parseString(chXMLstring,NULL,&xe);
 if (xe.error) 
  {
   objMessage.fMessage_Create(LOG_WARNING,259, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData,
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_259, XMLNode::getError(xe.error), chXMLstring,"","","","", NORMAL_MSG, NEXT_LINE); 
   enQueue_Message(ALI,objMessage);
   return false;
  }

 if (xNode.isEmpty()) {cout << "e2 XML node Is Empty" << endl; return false;}

  // Class of Service
 yNode = xNode.getChildNode(XML_NODE_CLASS_OF_SERVICE);
 if (!yNode.isEmpty())
  {
   if( yNode.getAttribute(XML_FIELD_E2_TYPE) ){ fLoadClassofService(yNode.getAttribute(XML_FIELD_E2_TYPE));}
  } 
 




 // Cell ID
 yNode = xNode.getChildNode(XML_NODE_CELL_ID);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strCellId = yNode.getText(); }
 if (strCellId.length() > XML_NODE_CELL_ID_MAX_CHAR)
 {
  SendTruncationMessage("Cell ID", strCellId.length(), XML_NODE_CELL_ID_MAX_CHAR, objPortData);
  strCellId = strCellId.erase(XML_NODE_CELL_ID_MAX_CHAR, string::npos);
 }
 // Company ID 1
 yNode = xNode.getChildNode( XML_NODE_COMPANY_ID_1);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strCompanyID1 = yNode.getText(); } 
 if (strCompanyID1.length() > XML_NODE_COMPANY_ID_1_MAX_CHAR)
 {
  SendTruncationMessage("Company ID 1", strCompanyID1.length(), XML_NODE_COMPANY_ID_1_MAX_CHAR, objPortData);
  strCompanyID1 = strCompanyID1.erase(XML_NODE_COMPANY_ID_1_MAX_CHAR, string::npos);
 }
 // County ID
 yNode = xNode.getChildNode(XML_NODE_COUNTY_ID);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strCountyID = yNode.getText(); } 
 if (strCountyID.length() > XML_NODE_COUNTY_ID_MAX_CHAR)
 {
  SendTruncationMessage("County ID", strCountyID.length(), XML_NODE_COUNTY_ID_MAX_CHAR, objPortData);
  strCountyID = strCountyID.erase(XML_NODE_COUNTY_ID_MAX_CHAR , string::npos);
 }
 // Customer Name
 yNode = xNode.getChildNode(XML_NODE_CUSTOMER_NAME);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strCustomerName = yNode.getText(); } 
 if (strCustomerName.length() > XML_NODE_CUSTOMER_NAME_MAX_CHAR)
 {
  SendTruncationMessage("Customer Name", strCustomerName.length(), XML_NODE_CUSTOMER_NAME_MAX_CHAR, objPortData);
  strCustomerName = strCustomerName.erase(XML_NODE_CUSTOMER_NAME_MAX_CHAR, string::npos);
 }
 // Emergency Medical Service Responder
 yNode = xNode.getChildNode(XML_NODE_EMER_MED_SRV_RESPONDER);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strEmergencyMedicalServiceProvider = yNode.getText(); } 
 if (strEmergencyMedicalServiceProvider.length() > XML_NODE_EMER_MED_SRV_RESPONDER_MAX_CHAR)
 {
  SendTruncationMessage("Emergency Medical Service Responder", strEmergencyMedicalServiceProvider.length(), XML_NODE_EMER_MED_SRV_RESPONDER_MAX_CHAR, objPortData);
  strEmergencyMedicalServiceProvider = strEmergencyMedicalServiceProvider.erase(XML_NODE_EMER_MED_SRV_RESPONDER_MAX_CHAR, string::npos);
 }
 // Emergency Services Number
 yNode = xNode.getChildNode(XML_NODE_EMER_SRV_NUMBER);
 if ((!yNode.isEmpty())&&(yNode.getText())) {strEmergencyServicesNumber = yNode.getText(); } 
 if (strEmergencyServicesNumber.length() > XML_NODE_EMER_SRV_NUMBER_MAX_CHAR )
 {
  SendTruncationMessage("Emergency Services Number", strEmergencyServicesNumber.length(), XML_NODE_EMER_SRV_NUMBER_MAX_CHAR , objPortData);
  strEmergencyServicesNumber = strEmergencyServicesNumber.erase(XML_NODE_EMER_SRV_NUMBER_MAX_CHAR , string::npos);
 }
 // Fire Department Service Responder
 yNode = xNode.getChildNode(XML_NODE_FIRE_DPT_SRV_RESPONDER);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strFireDepartmentServiceProvider = yNode.getText(); } 
 if (strFireDepartmentServiceProvider.length() > XML_NODE_FIRE_DPT_SRV_RESPONDER_MAX_CHAR)
 {
  SendTruncationMessage("Fire Department Service Responder", strFireDepartmentServiceProvider.length(), XML_NODE_FIRE_DPT_SRV_RESPONDER_MAX_CHAR, objPortData);
  strFireDepartmentServiceProvider = strFireDepartmentServiceProvider.erase(XML_NODE_FIRE_DPT_SRV_RESPONDER_MAX_CHAR, string::npos);
 }
 // House Number
 yNode = xNode.getChildNode(XML_NODE_HOUSE_NUMBER);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strHouseNumber = yNode.getText(); } 
 if (strHouseNumber.length() > XML_NODE_HOUSE_NUMBER_MAX_CHAR)
 {
  SendTruncationMessage("House Number", strHouseNumber.length(), XML_NODE_HOUSE_NUMBER_MAX_CHAR, objPortData);
  strHouseNumber = strHouseNumber.erase(XML_NODE_HOUSE_NUMBER_MAX_CHAR, string::npos);
 }
 // House Number Suffix
 yNode = xNode.getChildNode(XML_NODE_HOUSE_NUMBER_SUFFIX);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strHouseNumberSuffix = yNode.getText(); } 
 if (strHouseNumberSuffix.length() > XML_NODE_HOUSE_NUMBER_SUFFIX_MAX_CHAR)
 {
  SendTruncationMessage("House Number Suffix", strHouseNumberSuffix.length(), XML_NODE_HOUSE_NUMBER_SUFFIX_MAX_CHAR, objPortData);
  strHouseNumberSuffix = strHouseNumberSuffix.erase(XML_NODE_HOUSE_NUMBER_SUFFIX_MAX_CHAR, string::npos);
 }
 // Law Enforcement Service Responder
 yNode = xNode.getChildNode(XML_NODE_LAW_ENFCMNT_SVC_RESPONDER);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strLawEnforcementServiceProvider = yNode.getText(); } 
 if (strLawEnforcementServiceProvider.length() > XML_NODE_LAW_ENFCMNT_SVC_RESPONDER_MAX_CHAR)
 {
  SendTruncationMessage("Law Enforcement Service Responder", strLawEnforcementServiceProvider.length(), XML_NODE_LAW_ENFCMNT_SVC_RESPONDER_MAX_CHAR, objPortData);
  strLawEnforcementServiceProvider = strLawEnforcementServiceProvider.erase(XML_NODE_LAW_ENFCMNT_SVC_RESPONDER_MAX_CHAR, string::npos);
 }
 // Location
 yNode = xNode.getChildNode(XML_NODE_LOCATION);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strLocation = yNode.getText(); } 
 if (strLocation.length() >  XML_NODE_LOCATION_MAX_CHAR)
 {
  SendTruncationMessage("Location", strLocation.length(), XML_NODE_LOCATION_MAX_CHAR, objPortData);
  strLocation = strLocation.erase(XML_NODE_LOCATION_MAX_CHAR, string::npos);
 }
 // MSAG Community
 yNode = xNode.getChildNode(XML_NODE_MSAG_COMMUNITY);
 if ((!yNode.isEmpty())&&(yNode.getText())) {strMSAGCommunity = yNode.getText(); } 
 if (strMSAGCommunity.length() >  XML_NODE_MSAG_COMMUNITY_MAX_CHAR)
 {
  SendTruncationMessage("MSAG Community", strMSAGCommunity.length(), XML_NODE_MSAG_COMMUNITY_MAX_CHAR, objPortData);
  strMSAGCommunity = strMSAGCommunity.erase(XML_NODE_MSAG_COMMUNITY_MAX_CHAR, string::npos);
 }
 // Post Directional
 yNode = xNode.getChildNode(XML_NODE_POST_DIRECTIONAL);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strPostDirectional = yNode.getText(); } 
 if (strPostDirectional.length() >  XML_NODE_POST_DIRECTIONAL_MAX_CHAR)
 {
  SendTruncationMessage("Post Directional", strPostDirectional.length(), XML_NODE_POST_DIRECTIONAL_MAX_CHAR, objPortData);
  strPostDirectional = strPostDirectional.erase(XML_NODE_POST_DIRECTIONAL_MAX_CHAR, string::npos);
 }
 // Prefix Directional
 yNode = xNode.getChildNode(XML_NODE_PREFIX_DIRECTIONAL);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strPrefixDirectional = yNode.getText(); } 
 if (strPrefixDirectional.length() >  XML_NODE_PREFIX_DIRECTIONAL_MAX_CHAR)
 {
  SendTruncationMessage("Prefix Directional", strPrefixDirectional.length(), XML_NODE_PREFIX_DIRECTIONAL_MAX_CHAR, objPortData);
  strPrefixDirectional = strPrefixDirectional.erase(XML_NODE_PREFIX_DIRECTIONAL_MAX_CHAR, string::npos);
 }
 // Sector ID
 yNode = xNode.getChildNode(XML_NODE_SECTOR_ID);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strSectorID = yNode.getText(); } 
 if (strSectorID.length() >  XML_NODE_SECTOR_ID_MAX_CHAR )
 {
  SendTruncationMessage("Sector ID", strSectorID.length(), XML_NODE_SECTOR_ID_MAX_CHAR , objPortData);
  strSectorID = strSectorID.erase(XML_NODE_SECTOR_ID_MAX_CHAR , string::npos);
 }
 // State
 yNode = xNode.getChildNode(XML_NODE_STATE);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strState = yNode.getText(); } 
 if (strState.length() >  XML_NODE_STATE_MAX_CHAR)
 {
  SendTruncationMessage("State", strState.length(), XML_NODE_STATE_MAX_CHAR, objPortData);
  strState = strState.erase(XML_NODE_STATE_MAX_CHAR, string::npos);
 }
 // Street Name
 yNode = xNode.getChildNode(XML_NODE_STREET_NAME);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strStreetname = yNode.getText(); } 
 if (strStreetname.length() >  XML_NODE_STREET_NAME_MAX_CHAR)
 {
  SendTruncationMessage("Street Name", strStreetname.length(), XML_NODE_STREET_NAME_MAX_CHAR, objPortData);
  strStreetname = strStreetname.erase(XML_NODE_STREET_NAME_MAX_CHAR, string::npos);
 }
 // Street Name Suffix
 yNode = xNode.getChildNode(XML_NODE_STREET_NAME_SUFFIX);
 if ((!yNode.isEmpty())&&(yNode.getText()))  {strStreetNameSuffix = yNode.getText(); } 
 if (strStreetNameSuffix.length() >  XML_NODE_STREET_NAME_SUFFIX_MAX_CHAR )
 {
  SendTruncationMessage("Street Name Suffix", strStreetNameSuffix.length(),XML_NODE_STREET_NAME_SUFFIX_MAX_CHAR, objPortData);
  strStreetNameSuffix = strStreetNameSuffix.erase(XML_NODE_STREET_NAME_SUFFIX_MAX_CHAR, string::npos);
 }

 return true;
}
 

bool ALI_E2_Digits::fLoadNumberofDigits(unsigned char ch, int intDigits)
{
 if (intDigits != ((int) ch)) {return false;}
 chNumberofDigits = ch;
 return true;
}


bool ALI_E2_Digits::fLoadNumberingPlanandEncoding(unsigned char ch)
{
 switch(ch)
  {
   case 0x11: case 0x12: case 0x21: case 0x22: case 0x31: case 0x32: case 0x41: case 0x42: case 0x51: case 0x52: case 0x61: case 0x62: case 0x71: case 0x72:
        chNumberingPlanandEncoding = ch;
        return true;
   default:
        return false;
  }
 return false;
}


bool ALI_E2_Digits::fLoadTypeofDigits(unsigned char ch)
{
 if ((ch == 0x00)||(ch > 0x0F)) {return false;}
 chTypeofDigits = ch;
 return true;
}

bool ALI_E2_Digits::fLoadNatureofNumber(unsigned char ch)
{
 if (ch > 0x03) {return false;}
 chNatureofNumber = ch;
 return true;
}




void ALI_E2_Data::fClear()
{
 for (int i = 0; i < E2_TRANSACTIONID_LENGTH; i++){chTransactionId[i] = 0x00;}
 boolValidRecord                   = false;
 chComponentType                   = 0x00;     
 chComponentId                     = 0x00;     
 chOperationFamily                 = 0x00;     
 chOperationSpecifier              = 0x00;
 chErrorCode                       = 0x00;     
 chProblemType                     = 0x00;     
 chProblemSpecifier                = 0x00;     
 chPositionRequestType             = 0x00;
 chPositionResult                  = 0x00;
 chPositionInformationParameters   = 0x00;
 chPositionInformationLength       = 0x00;
 chPositionYear                    = 0x00;
 chPositionMonth                   = 0x00;
 chPositionDay                     = 0x00;
 chYear                            = 0x00;
 chMonth                           = 0x00;
 chDay                             = 0x00;
 chPositionGeoLength               = 0x00;
 chPositionGeoLPRI                 = 0x00;
 chPositionGeoScreening            = 0x00;
 chPositionGeoExt                  = 0x00;
 chPositionGeoTypeofShape          = 0x00;
 chPositionGeoLatSign              = 0x00;
 chPositionUncertaintyCode         = 0x00;
 chPositionConfidence              = 0x00;
 chPositionAltitudeSign            = 0x00;
 chPositionAltitudeUncertaintyCode = 0x00;
 chPositionSource                  = 0x00;
 chMobileCallStatus                = 0x00;
 chCompanyIdStringLength           = 0x00; 
 intLocationDescriptionLength      = 0; 
 for (int i = 0; i < 3; i++)                                    {chTimeofDay[i] = 0x00; chPositionTimeofDay[i] = 0x00;}
 for (int i = 0; i < 3; i++)                                    {chPositionGeoLatDegrees[i] = 0x00; chPositionGeoLongDegrees[i] = 0x00;}
 for (int i = 0; i < 2; i++)                                    {chPositionAltitude[i] = 0x00;} 
 for (int i = 0; i < E2_TRANSACTIONID_LENGTH; i++)              {chTransactionId[i] = 0x00;}
 for (int i = 0; i < E2_MAX_COMPANY_ID_STRING_LENGTH; i++)      {chCompanyIdString[i] = 0x00;}
 for (int i = 0; i < E2_MAX_LOCATION_DESCRIPTION_LENGTH+1; i++) {chLocationDescription[i] = 0x00;}

 CallbackNumber.fClear();
 EmergencyServicesRoutingDigits.fClear();  
 MobileIdentificationNumber.fClear();  
 InternationalMobileSubcriberIdentity.fClear();
}


unsigned long int ALI_E2_Data::fTransactionID()
{
 unsigned long int      ReturnResult = 0;
 long double            Additive;
 int                    power        = 0;

 for (int i =  E2_TRANSACTIONID_LENGTH -1; i >=0; i--)
  { 
   Additive = (long double) chTransactionId[i];
   Additive*= pow(256.0,power);
   power++;
   ReturnResult+= (unsigned long int) Additive; 
  } 
 return ReturnResult;
}

bool ALI_E2_Data::fIsACK()
{
 if (chPositionResult == 0x0A) {return true;}
 else                          {return false;}
}

bool ALI_E2_Data::fIsNAK()
{
 if (chPositionResult == 0x09) {return true;}
 else                          {return false;}
}

bool ALI_E2_Data::fLoadMobileCallStatus(unsigned char ch)
{
 int LeftDigit, RightDigit;
 // Authentication 0 - 2 Right 4 bytes
 // Authorization  0 - 15 Left 4 bytes

 RightDigit = (int)(ch&0x0F); 
 LeftDigit  = (int) ( ((ch>>4)&0xF) ) ;
 
 if ((RightDigit < 0)||(RightDigit > 2)) {return false;}

 chMobileCallStatus = ch;
 return true;
}



string ALI_E2_Data::TimeStamp(bool Position)
{
 long int                        iSeconds = 0;
 double                          Additive = 0.0;
 int                             power = 0;
 struct tm                       tmTimeInput, *ptrTime;
 time_t                          epochTimeStamp;
 char                            chTimeStamp[12];
 int                             iYearDifferential = E2_YEAR_OFFSET - UNIX_STRUCT_TM_YEAR_OFFSET;

 for (int i = 2; i >=0; i--)
  { 
   if (Position) {Additive = (unsigned long int) chPositionTimeofDay[i];}
   else          {Additive = (unsigned long int) chTimeofDay[i];}
   
   Additive*= pow(256.0,power); 
   power++;
   iSeconds+= (long int) Additive; 
  }

 iSeconds/=10;     // remove tenths of seconds .....

 tmTimeInput.tm_isdst = -1;
 tmTimeInput.tm_sec   =  (int(int(iSeconds)%3600)%60);
 tmTimeInput.tm_min   =  (int(int(iSeconds)%3600)/60);
 tmTimeInput.tm_hour  =  (int(iSeconds)/3600);
 if (Position) 
  {
   tmTimeInput.tm_mday  = chPositionDay;
   tmTimeInput.tm_mon   = (int) (chPositionMonth - 0x01);               // months since January (subtract 1)
   tmTimeInput.tm_year  = ((int) chPositionYear) + iYearDifferential;   // years since 1900
  }
 else          
  {
   tmTimeInput.tm_mday  = chDay;
   tmTimeInput.tm_mon   = (int) (chMonth - 0x01);               // months since January (subtract 1)
   tmTimeInput.tm_year  = ((int) chYear) + iYearDifferential;   // years since 1900
  }



 epochTimeStamp = timegm ( &tmTimeInput );

 ptrTime = localtime (&epochTimeStamp);

 strftime(chTimeStamp, sizeof(chTimeStamp), "%m/%d %H:%M", ptrTime);

 return chTimeStamp;
}

string ALI_E2_Data::Latitude()
{
 long double    ldLatitude, Additive = 0.0;
 long long int  Number               = 0;  
 int            power                = 0;
 ostringstream  oss;
 string         strOutput;
 size_t         sLength;

 for (int i = 2; i >=0; i--)
  { 
   Additive = (long double) chPositionGeoLatDegrees[i];
   Additive*= pow(256.0,power);
   power++;
   Number+= (long long int) Additive; 
  }

 ldLatitude = ( (Number * 90.0)/(pow(2.0, 23.0)) );
 oss << setprecision(E2_MAX_LATITUDE_LENGTH-3) << fixed << ldLatitude;
 
 strOutput = oss.str(); 
 sLength = strOutput.length();
 if (sLength > E2_MAX_LATITUDE_LENGTH) {strOutput.erase((E2_MAX_LATITUDE_LENGTH-1), string::npos);}

 return strOutput;
}

string ALI_E2_Data::Longitude()
{
 long double    ldLongitude, Additive = 0.0;
 long long int  Number                = 0;  
 int            power                 = 0;
 ostringstream  oss;
 string         strOutput;
 size_t         sLength;

 for (int i = 2; i >=0; i--)
  { 
   Additive = (long double) chPositionGeoLongDegrees[i];
   Additive*= pow(256.0,power);
   power++;
   Number+= (long long int) Additive; 
  }

 ldLongitude = ( (Number * 360.0)/(pow(2.0, 24.0)) );

 if (ldLongitude > 180) {ldLongitude -= 360.0;}
 oss << setprecision(E2_MAX_LONGITUDE_LENGTH-4) << showpos << fixed << ldLongitude; 
 strOutput = oss.str();
 sLength = strOutput.length();
 if (sLength > E2_MAX_LONGITUDE_LENGTH) {strOutput.erase((E2_MAX_LONGITUDE_LENGTH-1), string::npos);}

 return strOutput;
}

string ALI_E2_Data::Uncertainty()
{
 string         strOutput;
 long double    ldCode;
 int            power;
 long int       iCode;
 ostringstream  oss;

 strOutput.assign( E2_MAX_UNCERTAINTY_LENGTH ,' ');
 if (!chPositionUncertaintyCode) {return strOutput;}

 power = (int) chPositionUncertaintyCode;
 ldCode = 10.0 *(  pow(1.1,power) - 1);
 
 iCode = (long int) ldCode;

 oss << iCode;
 strOutput = oss.str();
 return strOutput;
}


string ALI_E2_Digits::AreaCode()
{
 int     FirstDigit, MiddleDigit, LastDigit;

 FirstDigit   = (int)(chDigitTwoandOne&0x0F); 
 MiddleDigit  = (int)(((chDigitTwoandOne>>4)&0xF)) ;
 LastDigit    = (int)(chDigitFourandThree&0x0F);

 return int2str(FirstDigit)+ int2str(MiddleDigit)+ int2str(LastDigit);
}

string ALI_E2_Digits::NPA()
{
 int     FirstDigit, MiddleDigit, LastDigit;

 FirstDigit   = (int)(((chDigitFourandThree>>4)&0xF)) ; 
 MiddleDigit  = (int)(chDigitSixandFive&0x0F);
 LastDigit    = (int)(((chDigitSixandFive>>4)&0xF)) ;

 return int2str(FirstDigit)+ int2str(MiddleDigit)+ int2str(LastDigit);
}

string ALI_E2_Digits::LastFour()
{
 int     FirstDigit, SecondDigit, ThirdDigit, FourthDigit;

 FirstDigit   = (int)(chDigitEightandSeven&0x0F); 
 SecondDigit  = (int)(((chDigitEightandSeven>>4)&0xF)) ;
 ThirdDigit   = (int)(chDigitTenandNine&0x0F);
 FourthDigit  = (int)(((chDigitTenandNine>>4)&0xF)) ;

 return int2str(FirstDigit)+ int2str(SecondDigit)+ int2str(ThirdDigit) +  int2str(FourthDigit);
}


void ALI_E2_Digits::fClear()
{
 chTypeofDigits             = 0x00;  
 chNatureofNumber           = 0x00; 
 chNumberingPlanandEncoding = 0x00;
 chNumberofDigits           = 0x00;
 chDigitTwoandOne           = 0x00;
 chDigitFourandThree        = 0x00;
 chDigitSixandFive          = 0x00;
 chDigitEightandSeven       = 0x00;
 chDigitTenandNine          = 0x00;
 chDigitTwelveandEleven     = 0x00;
 chDigitFourteenandThirteen = 0x00;
 chDigitFillerandFifteen    = 0x00;
}
 

bool ALI_E2_Digits::fLoadDigits(unsigned char ch, int intPair)
{
 int RightDigit, LeftDigit;

 RightDigit = (int)(ch&0x0F); 
 LeftDigit  = (int) ( ((ch>>4)&0xF) ) ;

 if  ((RightDigit > 9)||(RightDigit < 0))                  {return false;}
 if  ((intPair != 8) &&((LeftDigit > 9)||(LeftDigit < 0))) {return false;}
  
 switch (intPair)
  {
   case 21:   chDigitTwoandOne           = ch; break;
   case 43:   chDigitFourandThree        = ch; break;
   case 65:   chDigitSixandFive          = ch; break;
   case 87:   chDigitEightandSeven       = ch; break;
   case 109:  chDigitTenandNine          = ch; break;
   case 1211: chDigitTwelveandEleven     = ch; break;
   case 1413: chDigitFourteenandThirteen = ch; break;
   case 15:   chDigitFillerandFifteen    = ch; break;
   default: return false;
  }
 return true;
}
