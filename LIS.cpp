/*****************************************************************************
* FILE: LIS.cpp
*
* DESCRIPTION: This file contains all of the functions integral to the ngLIS Thread
*
* Features:
*

* AUTHOR: 08/11/2012 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation 
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"

//external class based variables
extern Thread_Trap                              OrderlyShutDown;
extern Thread_Trap                              UpdateVars;
extern queue <MessageClass>                     queue_objMainMessageQ;
extern queue <MessageClass>                     queue_objLISMessageQ;;
extern queue <ExperientDataClass>               queue_objLISData;
extern queue <ExperientDataClass>               queue_objMainData;
extern IP_Address                               HOST_IP;

extern Text_Data                                objBLANK_TEXT_DATA;
extern Call_Data                                objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_LIS_PORT;
extern sem_t                                    sem_tLISflag;
extern sem_t                                    sem_tMutexLISInputQ;
extern sem_t                                    sem_tMutexLISWorkTable;
extern sem_t                                    sem_tMutexLISPortDataQ;

//forward function Declarations
extern void    *LIS_Messaging_Monitor_Event(void *voidArg);
int            IndexOfTableWithBidID(string strBid, bool boolCallBackUsed = false);
int            IndexOfTableWithGeolocation(string strInput);
int            IndexOfTableWithEntity(string strInput);
void           POST_URL_Thread(ExperientDataClass objLISData, string strURL, string strRequest, string strUUID, string strType, int intPortNum );
void           POST_URN_Thread(ExperientDataClass objLISData, string strURL, string strRequest, string strUUID, string strType, int intPortNum );
void           GET_URL_THREAD(ExperientDataClass objLISData, string strURL, string strUUID, string strType, int PortNum );
bool           ConvertToXML(XMLNode*  ReturnNode, string strInput, int i);
void           Check_For_Stale_LIS_Bids();
//test
extern void SendHTTPSdereferenceData(string str1, string str2, int i);


// LIS Global VARS
struct itimerspec               itimerspec_LISHealthMonitorDelay;
timer_t                         timer_t_LISHealthMonitorTimerId;
queue <DataPacketIn>            queue_LISPortData;
vector <ExperientDataClass>     vLISworkTable;

extern string                   strMSRP_LIS_SERVER;
extern string                   strNG911_LIS_SERVER;

/****************************************************************************************************
*
* Name:
*  Function: void *LIS_Thread(void *voidArg)
*
*
* Description:
*   Handles all of the Next Generation LIS related activity for the controller. 
*
*
* Details: 
*   This function is implemented as a detached thread from the main program. The program recieves a 
*   pointer to the queue queue_objLISData from main() as its argument.
*
*   The Thread spawns (N) * 2 (A & B) UDP Port listening threads  and creates two timer event threads:
*  
*    a. ScoreBoard Event:       Utilized to process incoming LIS Records for transmission to main
*    b. Message Monitor Event:  Utilized to send error communications to main.
*
*   
*   A global semaphore is used to recieve a signal from three sources :
*
*     a. Data (from Main) has been pushed onto the LIS's input queue.
*     b. Data received from an LIS port pushed onto the Data queue.
*     c. Shutdown signal from main.
*          
*
*   The main loop idles waiting for a signal, it checks for the shutdown signal, if able to continue it processes 2 queues the first is 
*   queue_objLIS_InputData, the second is the queueLISPortData.
*
*   1. queue_objLIS_InputData: data is popped from the queue onto a table (array) of records and is processed 
*      by 2 timer event handlers LIS_ScoreBoard_Event and LIS_Messaging_Monitor_Event. 
*
*      a. Records are placed into the table by trunk number and remain there until 2 X timeout or a vLISd record is recieved
*
*      b. Upon receiving a record, the phone number is searched on a round robin basis for the first matching LIS Database (LIS-Steering)
*
*      c. A key is created to bid the available Database and the record is bid.
*
*   2. queueLISPortData:  data is popped from the queue and deciphered for type (ACK NAK or Record) then length.
*     
*      a. If the data is ACK reset heartbeat time
*      b. If the Data is NAK generate error message if 3 NAKs in a row set reduced Hearbeat Mode (No bids to port, reduced HB interval)
*      c. If Record Load into object clear table and pass to main
*      d. If Bad Data generate error message
*
*
                 
*  
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/

void *LIS_Thread(void *voidArg)
{

 struct sigevent                                 sigeventLISMessageMonitorEvent;

// pthread_attr_t                                pthread_attr_tAttr;
 volatile bool                                   boolLISdata;
 volatile bool                                   boolLISQueue;
 volatile bool                                   boolLISPortData;
 volatile bool                                   boolLISPortDataQueue;
 int                                             intRC;
 int                                             intRCproviderADR, intRCsubscriberADR, intRCserviceADR;
 string                                          strError;
 string                                          strTemp;
 bool                                            boolReturnValue;
 MessageClass                                    objMessage;
 ExperientDataClass                              objLISData;
 ExperientHTTPPort                               LIS_HTTP_Port;
 DataPacketIn                                    objData;
 int                                             index, intMainTableIndex, urnSVCindex;
 Port_Data                                       objPortData;
 struct timespec                                 timespecRemainingTime;
 Thread_Data                                     ThreadDataObj;
 bool                                            boolFirstBid;      
 extern vector <Thread_Data>                     ThreadData;
 string                                          strHTTPgetURI, strHTTPpostLocationRequest, strHTTPurl;
 string                                          strData;
 pthread_t                                       pthread_tLISmessagingThread;
 pthread_attr_t                                  LISpthread_attr_tAttr;
 volatile bool                                   boolTest = false;
 string                                          strCertFile; 
 XMLNode                                         objXMLnode;
 string                                          strOtherHeader;
 string                                          strTestUUID;
 char*                                           cptrResponse;
 string                                          strXMLString;
 int                                             iRecursion;
 int                                             iServiceBid;
 string                                          strBidServer;
 string                                          strECRFRequest;
 extern string                                   strLIS_CERTIFICATE;
 extern vector <ExperientDataClass>              vMainWorkTable;

 extern int IndexWithCallUniqueIDMainTable(string strUniqueID);
 extern int IndexWithNenaCallUniqueIDMainTable(string strUniqueID);

 strCertFile.clear();
 if (strLIS_CERTIFICATE != DEFAULT_VALUE_LIS_INI_KEY_8 ) {
  strCertFile = strLIS_CERTIFICATE;
 }

/*
 stringSideAorB[1] = "A";
 stringSideAorB[2] = "B";
 strLIS_NOT_ALLOWED_MESSAGE = charSTX;
 strLIS_NOT_ALLOWED_MESSAGE.append(intLIS_RECORD_LINE_WRAP*6, ' ');       
 strLIS_NOT_ALLOWED_MESSAGE.append(7, ' ');
 strLIS_NOT_ALLOWED_MESSAGE += "LIS NOT APPLICABLE";
 strLIS_NOT_ALLOWED_MESSAGE.append(14, ' ');
 strLIS_NOT_ALLOWED_MESSAGE += "FOR THIS CALL TYPE";
 strLIS_NOT_ALLOWED_MESSAGE += charETX;

 strLIS_FAILED_MESSAGE = charSTX;
 strLIS_FAILED_MESSAGE.append(intLIS_RECORD_LINE_WRAP*6, ' ');       
 strLIS_FAILED_MESSAGE.append(7, ' ');
 strLIS_FAILED_MESSAGE += "UNABLE TO RETRIEVE";
 strLIS_FAILED_MESSAGE.append(17, ' ');
 strLIS_FAILED_MESSAGE += "LIS RECORD";
 strLIS_FAILED_MESSAGE += charETX;
*/

/*
   LIS_HTTP_Port.enumPortType  = LIS;
   LIS_HTTP_Port.intPortNumber = 1;  
   LIS_HTTP_Port.SetAccept("application/held+xml;charset=utf-8");
   LIS_HTTP_Port.SetContentType("application/held+xml;charset=utf-8");
   LIS_HTTP_Port.SetFollowRedirects(1);
   LIS_HTTP_Port.SetURLPort(56072);
   LIS_HTTP_Port.SetLocalHost(NULL);
#ifdef IPWORKS_V16
   LIS_HTTP_Port.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
*/
 //  LIS_HTTP_Port.SetAuthScheme(1);
 //  LIS_HTTP_Port.SetUser(strCTI_USERNAME.c_str());
//   LIS_HTTP_Port.SetPassword(strCTI_PASSWORD.c_str());
/*
   LIS_HTTP_Port.SetTimeout(5);
   LIS_HTTP_Port.Config("KeepAlive=false");
*/
/*
0 (None) 		No events are logged.
1 (Info - default) 	Informational events are logged.
2 (Verbose) 		Detailed data is logged.
3 (Debug) 		Debug data is logged.
*/

/*   LIS_HTTP_Port.Config("LogLevel=3"); */
//   LIS_HTTP_Port.Config("AbsoluteTimeout=true");
/*   LIS_HTTP_Port.Config("TcpNoDelay=true");*/

 //  LIS_HTTP_Port.SetPragma("no-cache");
//   LIS_HTTP_Port.SetContentType("application/x-com-polycom-spipx");

/*
 if (strCertFile.length()) {
   LIS_HTTP_Port.SetSSLCertStoreType(6);
   LIS_HTTP_Port.SetSSLCertStore((char*)strCertFile.c_str(),strCertFile.length());
   LIS_HTTP_Port.SetSSLCertSubject("*");
 }
*/

 //stringLIS_LOG_SPACES.append(LOG_FIXED_FIELD_DISPLAY_SPACES,' ');
/*
 // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,248, objBLANK_CALL_RECORD, objBLANK_LIS_PORT, KRN_MESSAGE_142); enQueue_Message(LIS,objMessage);}
*/
 ThreadDataObj.strThreadName ="LIS";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in LIS.cpp::LIS_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Send InitiLISzed message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,200, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,LIS_MESSAGE_200, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(LIS,objMessage);

 
/*
 // set up LIS Message monitor timer event
 sigeventLISMessageMonitorEvent.sigev_notify 			= SIGEV_THREAD;
 sigeventLISMessageMonitorEvent.sigev_notify_function 		= LIS_Messaging_Monitor_Event;
 sigeventLISMessageMonitorEvent.sigev_notify_attributes 		= NULL;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventLISMessageMonitorEvent, &timer_t_LISHealthMonitorTimerId);
 if (intRC){Abnormal_Exit(LIS, EX_OSERR, KRN_MESSAGE_180, "timer_t_LISHealthMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspec_LISHealthMonitorDelay, 0,intLIS_MESSAGE_MONITOR_INTERVAL_NSEC , 0, intLIS_MESSAGE_MONITOR_INTERVAL_NSEC  );
 Set_Timer_Delay(&itimerspec_LISHealthMonitorDelay, 0,intLIS_MESSAGE_MONITOR_INTERVAL_NSEC , 0, 0  );
 timer_settime( timer_t_LISHealthMonitorTimerId, 0, &itimerspec_LISHealthMonitorDelay, NULL);
*/

 pthread_attr_init(&LISpthread_attr_tAttr);
 intRC = pthread_create(&pthread_tLISmessagingThread, &LISpthread_attr_tAttr, *LIS_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(LIS, EX_OSERR, LIS_MESSAGE_298);}

 // arm timer
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,202, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LIS_MESSAGE_202); 
 enQueue_Message(LIS,objMessage);



/*************************************************************************************************************************************************************************************************/
/**************************************************************************************************************************************************************************************************/
/*

									Main LIS Thread Loop


*/
/*************************************************************************************************************************************************************************************************/
/*************************************************************************************************************************************************************************************************/
 boolLISQueue           = false;
 boolLISPortDataQueue   = false;
 
 do {
   // wait for data signal from main or data queue

   intRC = sem_wait(&sem_tLISflag);  
   //cout << "LIS_FLAG" << endl;
   
   // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
   if (intRC)    {
     objMessage.fMessage_Create(LOG_WARNING,203, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LIS_MESSAGE_203); 
     enQueue_Message(LIS,objMessage);
   } 

   // Shut Down Trap Area
   if (boolSTOP_EXECUTION )    {
     //timer_delete(timer_t_LISHealthMonitorTimerId);
     pthread_join( pthread_tLISmessagingThread, NULL); 
 //    LIS_HTTP_Port.Interrupt();
     OrderlyShutDown.boolLISThreadReady = true;

     while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}

     // jump to while (!boolSTOP_EXECUTION  )
     break;
										
   }// end if (boolSTOP_EXECUTION  )

  if (boolUPDATE_VARS) {
    UpdateVars.boolLISThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    UpdateVars.boolLISThreadReady = false;       
  }





/*************************************************************************************************************************************************************************************************/
/**************************************************************************************************************************************************************************************************/
/*

									Process Data from Main/KRN Thread


*/
/*************************************************************************************************************************************************************************************************/
/*************************************************************************************************************************************************************************************************/

   // Lock LIS INPUT Q

   intRC = sem_wait(&sem_tMutexLISInputQ);						
   if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISInputQ, "LIS    - sem_wait@sem_tMutexLISInputQ in LIS_Thread", 1);}

   boolLISdata = (!queue_objLISData.empty());
   if (boolLISdata) {
    objLISData = queue_objLISData.front();
    queue_objLISData.pop(); 
    boolLISQueue = (!queue_objLISData.empty());  
   }
   
   sem_post(&sem_tMutexLISInputQ);  
  
   // Perform OPS on LIS INPUT 
   if (boolLISdata)   {
     
     //objLISData = queue_objLISData.front();
     //queue_objLISData.pop();

     // Update CallData in object to that of Main Table ....
     intRC = sem_wait(&sem_tMutexMainWorkTable);
     intMainTableIndex = IndexWithCallUniqueIDMainTable(objLISData.CallData.stringUniqueCallID);
     if (intMainTableIndex >= 0) {
      objLISData.CallData = vMainWorkTable[intMainTableIndex].CallData;
      objLISData.TextData = vMainWorkTable[intMainTableIndex].TextData;
      objLISData.intCallStateCode  = vMainWorkTable[intMainTableIndex].intCallStateCode;
      objLISData.intCallStatusCode = vMainWorkTable[intMainTableIndex].intCallStatusCode;
      objLISData.boolConnect       = vMainWorkTable[intMainTableIndex].boolConnect;
     }
     else {
      if (objLISData.ALIData.I3Data.enumLISfunction != I3_BY_VALUE) {
       sem_post(&sem_tMutexMainWorkTable); 
       break;
      }
     }
     sem_post(&sem_tMutexMainWorkTable);    


     // Lock Work Table

     intRC = sem_wait(&sem_tMutexLISWorkTable);					
     if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISWorkTable, "LIS    - sem_wait@sem_tMutexLISWorkTable in LIS_Thread", 1);}

     objLISData.SetADRflag();
     objLISData.ALIData.I3Data.SetADRflags();
     objLISData.ALIData.I3Data.ServiceURNList.fClear();
//     //cout << "bid id before deref -> " << objLISData.ALIData.I3Data.strBidId << endl;    
     switch  (objLISData.ALIData.I3Data.enumLISfunction)
      {
       case DEREFERENCE_URL:
//           //cout << "deref in LIS ... Bid ID            ->" << objLISData.ALIData.I3Data.strBidId << endl;
//            //cout << "deref in LIS ... PidflobyValEntity ->" << objLISData.strPidfloByValEntity << endl;
//            //cout << "deref in LIS ... URI               ->" << objLISData.ALIData.I3Data.objLocationURI.strURI << endl;
            if      (objLISData.ALIData.I3Data.objLocationURI.strURI.length()) {strHTTPgetURI = objLISData.ALIData.I3Data.objLocationURI.strURI;}
            else if (objLISData.TextData.objLocationURI.strURI.length())       {strHTTPgetURI = objLISData.TextData.objLocationURI.strURI;cout << "should not trip in LIS" << endl;}
            else                                                               {cout << "got here ? " << endl;break;}
            
            clock_gettime(CLOCK_REALTIME, &objLISData.timespecTimeRecordReceivedStamp);
            vLISworkTable.push_back(objLISData);

            objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                       objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, strHTTPgetURI); 
            enQueue_Message(LIS,objMessage);
            cout << "IN DEREF IN LIS" << endl;
           
            //turn off ALI Button ---
            objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_REBID;
            objLISData.WindowButtonData.fSetButtonsForALIbid();
            objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_REBID, true);
            Queue_WRK_Event(objLISData);
          
//            sleep(10);
//            cout << "Done sleeping" << endl;

  
 //           //cout << "de ref with nena callid -> " << objLISData.strNenaCallId << endl;
            thread (GET_URL_THREAD, objLISData, strHTTPgetURI, objLISData.strNenaCallId, "PIDFLO", 1 ).detach();


//            //cout << "objLISData.strCallInfoProviderURL -> " << objLISData.strCallInfoProviderURL << endl;

            //Process ADR .... ADR uses GET, Do not place in function as these spawn threads that could outlive the function.

            if (objLISData.strCallInfoProviderURL.length()) {           
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, objLISData.strCallInfoProviderURL); 
             enQueue_Message(LIS,objMessage);
             thread (GET_URL_THREAD, objLISData, objLISData.strCallInfoProviderURL, objLISData.strNenaCallId, "PROVIDER", 2 ).detach();
            }
            if (objLISData.strCallInfoServiceURL.length()) { 
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, objLISData.strCallInfoServiceURL); 
             enQueue_Message(LIS,objMessage);
             thread (GET_URL_THREAD, objLISData, objLISData.strCallInfoServiceURL, objLISData.strNenaCallId, "SERVICE", 3 ).detach();         
            }            
            if (objLISData.strCallInfoSubscriberURL.length()) { 
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                       objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, objLISData.strCallInfoSubscriberURL); 
             enQueue_Message(LIS,objMessage);
             thread (GET_URL_THREAD, objLISData, objLISData.strCallInfoSubscriberURL, objLISData.strNenaCallId, "SUBSCRIBER", 4 ).detach();                  
            }            
// fake a packet
//intRC = 0;
//boolTest = true;
            break;

       case POST_LIS_LOCATION_REQUEST:
  //cout << "post location request Nena callid-> " << objLISData.ALIData.I3Data.strNenaCallId << endl;
  //cout << "post location request callbacknum-> " << objLISData.ALIData.I3Data.I3XMLData.CallInfo.strCallbackNum << endl;

              strTestUUID = objLISData.ALIData.I3Data.strNenaCallId;          
   //         if (objLISData.TextData.TextType == MSRP_MESSAGE) {cout << "MSRP Message" << endl; strHTTPurl = strMSRP_LIS_SERVER;} //trunk based later ?
   //         else                                              {cout << "Not MSRP!" << endl;    strHTTPurl = strNG911_LIS_SERVER;}
            if      (objLISData.ALIData.I3Data.objLocationURI.strURI.length()) {strHTTPurl = objLISData.ALIData.I3Data.objLocationURI.strURI;}
            else if (objLISData.TextData.objLocationURI.strURI.length())       {strHTTPurl = objLISData.TextData.objLocationURI.strURI;}
            else                                                               {break;}

            boolFirstBid = (objLISData.ALIData.stringAliText.length() == 0);
            strHTTPpostLocationRequest = objLISData.CallData.fCreateLocationRequest(boolFirstBid).c_str();
            clock_gettime(CLOCK_REALTIME, &objLISData.timespecTimeRecordReceivedStamp);
            vLISworkTable.push_back(objLISData);
//            LIS_HTTP_Port.SetPostData(strHTTPpostLocationRequest.c_str(), strHTTPpostLocationRequest.length());
//            //cout << strHTTPpostLocationRequest << endl;
//            //cout << "To URL -> " << strHTTPurl << endl;

            objMessage.fMessage_Create(LOG_CONSOLE_FILE,205, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                       objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_205, strHTTPurl);            
            enQueue_Message(LIS,objMessage);

            //turn off ALI Button ---
            objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_REBID;
            objLISData.WindowButtonData.fSetButtonsForALIbid();
            objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_REBID, true);
            Queue_WRK_Event( objLISData);


            thread (POST_URL_Thread, objLISData, strHTTPurl, strHTTPpostLocationRequest, objLISData.strNenaCallId, "PIDFLO", 1 ).detach();
           // cout << "sleep start .............." << endl;
           // sleep(5);
           // cout << "sleep end ................" << endl;

            //Process ADR .... ADR uses GET, Do not place in function as these spawn threads that could outlive the function.
           // //cout << "objLISData.strCallInfoProviderURL -> " << objLISData.strCallInfoProviderURL << endl;
            
            if (objLISData.strCallInfoProviderURL.length()) {           
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, objLISData.strCallInfoProviderURL); 
             enQueue_Message(LIS,objMessage);
             thread (GET_URL_THREAD, objLISData, objLISData.strCallInfoProviderURL, objLISData.strNenaCallId, "PROVIDER", 2 ).detach();
            }
            if (objLISData.strCallInfoServiceURL.length()) { 
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, objLISData.strCallInfoServiceURL); 
             enQueue_Message(LIS,objMessage);
             thread (GET_URL_THREAD, objLISData, objLISData.strCallInfoServiceURL, objLISData.strNenaCallId, "SERVICE", 3 ).detach();         
            }            
            if (objLISData.strCallInfoSubscriberURL.length()) { 
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                                       objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204, objLISData.strCallInfoSubscriberURL); 
             enQueue_Message(LIS,objMessage);
             thread (GET_URL_THREAD, objLISData, objLISData.strCallInfoSubscriberURL, objLISData.strNenaCallId, "SUBSCRIBER", 4 ).detach();                  
            }  

// fake a packet
//intRC = 0;
//boolTest = true;     
            break;

       case I3_BY_VALUE:
            // Here we Got I3 by Value bid for FIRE EMS POLICE URI's 
            //cout << "Got I3 Data by Value" << endl;
            //cout << "Bid ID ->" << objLISData.ALIData.I3Data.strBidId << endl;
            // objLISData.ALIData.I3Data.fDisplay();
             
    
            // Need to get URN FIRE POLICE DATA
            clock_gettime(CLOCK_REALTIME, &objLISData.timespecTimeRecordReceivedStamp);
            objLISData.ALIData.I3Data.SetADRflags(true);
            objLISData.ALIData.I3Data.boolEmergencyCallDataRequestFlag = true;
            objLISData.ALIData.I3Data.ServiceURNList.boolBidMade = true;     
            vLISworkTable.push_back(objLISData);
       
            // If no ECRF do not bid
            if (strNG911_ECRF_SERVER == DEFAULT_VALUE_LIS_INI_KEY_9) {break;}

            // Bid Service Info
            iRecursion = 0;
            strBidServer = strNG911_ECRF_SERVER;
            //clear service list
            objLISData.ALIData.I3Data.ServiceURNList.fClear();
            do {
             strBidServer = NAPTR(strBidServer, iRecursion);
             iRecursion++;
            } while ((strBidServer.empty())&&(iRecursion < 7));

            if ((strBidServer.empty())&&(strNG911_ECRF_SERVER != DEFAULT_VALUE_LIS_INI_KEY_9) ) {
             objMessage.fMessage_Create(LOG_CONSOLE_FILE,281, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_281, strNG911_ECRF_SERVER);
             enQueue_Message(LIS,objMessage);
             break;
            }

            objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_204d, strNG911_SOS_SERVICE_BID, strBidServer);
            enQueue_Message(LIS,objMessage);  
            
//             thread (POST_URN_Thread, objLISData, strBidServer, objLISData.ECRF_URN_Service_Request(strNG911_SOS_SERVICE_BID, true),   objLISData.ALIData.I3Data.strNenaCallId, "LOST", 5 ).detach();

            break;
       case I3_DATA_RECEIVED:
            cout << "lis not used I3_DATA_RECEIVED" << endl;
            break;
       default:
            cout << "lis not coded default" << endl;
            break;



      }


      intRC = sem_post(&sem_tMutexLISWorkTable);					// UNLOCK Work Table

    }//end if (boolLISdata)

//   sem_post(&sem_tMutexLISInputQ);



/*************************************************************************************************************************************************************************************************/
/**************************************************************************************************************************************************************************************************/
/*

									Process Http Request Data Returned


*/
/*************************************************************************************************************************************************************************************************/
/*************************************************************************************************************************************************************************************************/
// //cout << "LIS_DATA .........................................................................................................................." << endl;
  // Lock LIS Data Q
/*
if (boolTest) {
 SendHTTPSdereferenceData("/datadisk1/ng911/testdata/pidflo.location.responce.xml", strTestUUID);
 experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC); //cout << "A" << endl;
 SendHTTPSdereferenceData("/datadisk1/ng911/testdata/service.info.responce.xml", strTestUUID);
 experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC); //cout << "B" << endl;
 SendHTTPSdereferenceData("/datadisk1/ng911/testdata/provider.info.responce.xml", strTestUUID); 
 experient_nanosleep(ONE_HUNDREDTH_SEC_IN_NSEC); //cout << "C" << endl;
 SendHTTPSdereferenceData("/datadisk1/ng911/testdata/subscriber.info.responce.xml", strTestUUID);
} //fake record  

boolTest = false;
experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
*/ 
////cout << "LIS DATA Q size is " << queue_LISPortData.size() << endl;

 

   intRC = sem_wait(&sem_tMutexLISPortDataQ);						
   if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISPortDataQ, "LIS    - sem_wait@sem_tMutexLISPortDataQ in LIS_Thread", 1);}

   boolLISPortData = (!queue_LISPortData.empty());
   if (boolLISPortData) {
       
    objData.fClear();
    objLISData.fClear_Record();
    objLISData.ALIData.fClear();
    objData = queue_LISPortData.front();    
    queue_LISPortData.pop(); 
    boolLISPortDataQueue = (!queue_LISPortData.empty());  
   }
   
   sem_post(&sem_tMutexLISPortDataQ);  
   
  cout << "lis port data " << boolLISPortData << endl;
  cout << "lis port Q    " << boolLISPortDataQueue << endl;
  
  if (!boolLISPortData) {continue;} // while (!boolSTOP_EXECUTION); >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  
   
//  if (boolLISPortData) {
  /*
     objData.fClear();
     objLISData.fClear_Record();
     objLISData.ALIData.fClear();
     objData = queue_LISPortData.front();
     queue_LISPortData.pop(); 
*/
     // Lock Work Table

     intRC = sem_wait(&sem_tMutexLISWorkTable);					
     if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISWorkTable, "LIS    - sem_wait@sem_tMutexLISWorkTable in LIS_Thread", 1);}

     strError.clear();


 //    index = IndexOfTableWithBidID(objLISData.ALIData.I3Data.strBidId);
     index = IndexOfTableWithBidID(objData.strRequestID);
     //cout << " got reference data with bid id of " << objData.strRequestID << endl;
     //cout << "port number is -> " << objData.intPortNum << endl;
     //cout << "table size is -> " << vLISworkTable.size() << endl;
     //cout << "Q size -> " << queue_LISPortData.size() << endl;
     //cout << "index = " << index << endl;


     boolReturnValue = true; //obsolete
     

     if (index < 0)  {
      // Data is unmatched to Bid!
      
      objMessage.fMessage_Create(LOG_CONSOLE_FILE,278, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objBLANK_TEXT_DATA,
                                 objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_278a, objData.stringDataIn); 
      enQueue_Message(LIS,objMessage);

      sem_post(&sem_tMutexLISWorkTable);
      continue;
     } 
       
    /*
     if (!boolReturnValue) {
       
       objLISData = vLISworkTable[index];
       objMessage.fMessage_Create(LOG_CONSOLE_FILE,277, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objBLANK_TEXT_DATA,
                                  objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LIS_MESSAGE_277, strError ); 
       enQueue_Message(LIS,objMessage);
       objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
       objLISData.WindowButtonData.fSetButtonsForALItimeout(true);
       objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT, true);

       Queue_WRK_Event(objLISData); 

       sem_post(&sem_tMutexLISWorkTable);
       break;
     
     } else ...
    */  

      if (vLISworkTable[index].ALIData.I3Data.boolEmergencyCallDataRequestFlag) {
       // ADR Record .......................................................................
       cout << "RX ADR RECORD PART -> " << objData.intPortNum << endl;
       if (vLISworkTable[index].ALIData.I3Data.fNoADRdataReceived()) {
        vLISworkTable[index].ALIData.I3Data.I3XMLData.fClear();
        vLISworkTable[index].ALIData.I3Data.ADRdata.fClear();
        vLISworkTable[index].ALIData.I3Data.strPidfXMLData.clear();
        vLISworkTable[index].ALIData.I3Data.strALI30WRecord.clear();
        vLISworkTable[index].ALIData.I3Data.strALI30XRecord.clear();
        vLISworkTable[index].ALIData.I3Data.strNGALIRecord.clear();
        vLISworkTable[index].ALIData.I3Data.boolPidflobyValue = false;               
       }
       
       switch (objData.intPortNum) {
        case 1: //PIDFLO
         cout << "CASE 1" << endl;
         // Here we rebid or got a Piflo need to check if we need to bid the ECRF
         vLISworkTable[index].ALIData.I3Data.boolPidfLoRXflag = true;
         vLISworkTable[index].ALIData.I3Data.fLoadLocationData(objData, &strError);

          // If no ECRF do not bid
         if (strNG911_ECRF_SERVER == DEFAULT_VALUE_LIS_INI_KEY_9) {break;}


         // Need to get URN FIRE POLICE DATA
         clock_gettime(CLOCK_REALTIME, &vLISworkTable[index].timespecTimeRecordReceivedStamp);
         vLISworkTable[index].ALIData.I3Data.boolEmergencyCallDataRequestFlag = true;

         // Bid Service Info
         iRecursion = 0;
         strBidServer = strNG911_ECRF_SERVER;
         vLISworkTable[index].ALIData.I3Data.ServiceURNList.fClear();
         do {
           strBidServer = NAPTR(strBidServer, iRecursion);
           iRecursion++;
         } while ((strBidServer.empty())&&(iRecursion < 7));

         if ((strBidServer.empty())&&(strNG911_ECRF_SERVER != DEFAULT_VALUE_LIS_INI_KEY_9) ) {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE,281, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                                     objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, vLISworkTable[index].ALIData, LIS_MESSAGE_281, strNG911_ECRF_SERVER);
          enQueue_Message(LIS,objMessage);
          break;
         }

         objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                                        objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, vLISworkTable[index].ALIData, LIS_MESSAGE_204d, strNG911_SOS_SERVICE_BID, strBidServer);
         enQueue_Message(LIS,objMessage);
           
         vLISworkTable[index].ALIData.I3Data.ServiceURNList.fSetBid();
         thread (POST_URN_Thread, vLISworkTable[index], strBidServer, vLISworkTable[index].ECRF_URN_Service_Request(strNG911_SOS_SERVICE_BID, true),
                 vLISworkTable[index].ALIData.I3Data.strNenaCallId, "LOST", 5 ).detach();
         

        
         break;
        case 2: //ProviderInfo
         if (ConvertToXML(&objXMLnode, objData.stringDataIn, 2)) {
          vLISworkTable[index].ALIData.I3Data.boolProviderInfoRXflag = true;
          vLISworkTable[index].ALIData.I3Data.ADRdata.fLoadProviderInfo(objXMLnode , &strError);
          vLISworkTable[index].ALIData.I3Data.fMapADRProviderInfoToI3xml();
         }
         break; 
        case 3: //ServiceInfo
         if (ConvertToXML(&objXMLnode, objData.stringDataIn, 3)) {
          vLISworkTable[index].ALIData.I3Data.boolServiceInfoRXflag = true;
          vLISworkTable[index].ALIData.I3Data.ADRdata.fLoadServiceInfo(objXMLnode , &strError);
          vLISworkTable[index].ALIData.I3Data.fMapADRServiceInfoToI3xml();
         }
         break;
        case 4: //SubscriberInfo
         if (ConvertToXML(&objXMLnode, objData.stringDataIn, 4)) {
          vLISworkTable[index].ALIData.I3Data.boolSubsciberInfoRXflag = true;
          vLISworkTable[index].ALIData.I3Data.ADRdata.fLoadSubscriberInfo(objXMLnode , &strError);
          vLISworkTable[index].ALIData.I3Data.fMapATTtypeofServiceToI3xml();
          vLISworkTable[index].ALIData.I3Data.fMapMainTelNumbertoI3xml();
          vLISworkTable[index].ALIData.I3Data.fMapCustomerNametoI3xml();
          vLISworkTable[index].ALIData.I3Data.fMapNotestoI3xml();
         }
         break;

        case 5: //URN REQUEST LIST SERVICES BY LOCATION
         vLISworkTable[index].ALIData.I3Data.fLoadServiceList(objData.stringDataIn);
         vLISworkTable[index].ALIData.I3Data.ServiceURNList.boolListBuilt = true;
         cout << "case 5 " << endl;
         // bid the list ....
         //check if we have service data to bid for .............
         if (vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN.empty()) {
          break;
         }
         // Here we got location Data ... bid ECRF for URN of FIRE POLICE EMS ...
         // reset timeout ....

         clock_gettime(CLOCK_REALTIME, &vLISworkTable[index].timespecTimeRecordReceivedStamp);
         iRecursion = 0;
         strBidServer = strNG911_ECRF_SERVER;
         do {
          strBidServer = NAPTR(strBidServer, iRecursion);
          iRecursion++; 
         } while ((strBidServer.empty())&&(iRecursion < 7));

         if ((strBidServer.empty())&&(strNG911_ECRF_SERVER != DEFAULT_VALUE_LIS_INI_KEY_9) ) {
            objMessage.fMessage_Create(LOG_CONSOLE_FILE,281, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                                       objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, vLISworkTable[index].ALIData, LIS_MESSAGE_281, strNG911_ECRF_SERVER);
            enQueue_Message(LIS,objMessage);
            break;
         }



         iServiceBid = 6;
         for (std::vector<SERVICE_URN>::iterator it = vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN.begin() ;
              it != vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN.end(); ++it)  {
          it->iBid = iServiceBid;
          objMessage.fMessage_Create(LOG_CONSOLE_FILE,204, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vLISworkTable[index].fCallData(), vLISworkTable[index].TextData,
                                    objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, vLISworkTable[index].ALIData, LIS_MESSAGE_204d, it->strURN, strBidServer);
          enQueue_Message(LIS,objMessage);

          strECRFRequest = vLISworkTable[index].ECRF_URN_Service_Request(it->strURN, false);

          thread (POST_URN_Thread, vLISworkTable[index], strBidServer, strECRFRequest, vLISworkTable[index].ALIData.I3Data.strNenaCallId, "LOST", iServiceBid ).detach();
          iServiceBid++;
         }
         // if this fails we need to set the data to return 


         break;

        default: //responder URN Recieved 6 +
          // PortNum used to match iBid in Service Vector ......
          ////cout << "Data RX" << endl;
          ////cout << objData.intPortNum << endl; 
          ////cout << objData.stringDataIn << endl;
          urnSVCindex = vLISworkTable[index].ALIData.I3Data.ServiceURNList.ServiceBidIndex(objData.intPortNum);
          if (urnSVCindex < 0) {
           SendCodingError("LIS.cpp - ServiceBidIndex() No Match -> "+ int2str(objData.intPortNum) );
           break;
          }              
          vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN[urnSVCindex].boolServiceURIreceivedFlag = true;
          vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN[urnSVCindex].strDataRX = objData.stringDataIn;          
          vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN[urnSVCindex].fDetermineURI();
          vLISworkTable[index].ALIData.I3Data.fConvertServiceListToALIbody();          
         break;
       } //end switch (objData.intPortNum)



        objMessage.fMessage_Create(LOG_CONSOLE_FILE,278, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , vLISworkTable[index].fCallData(), objBLANK_TEXT_DATA,
                                 objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, vLISworkTable[index].ALIData, LIS_MESSAGE_278, objData.stringDataIn); 
        enQueue_Message(LIS,objMessage); 

       if ((vLISworkTable[index].ALIData.I3Data.fAllADRdataReceived())&&(vLISworkTable[index].ALIData.I3Data.ServiceURNList.fAll_URIS_RX())) {
        objLISData = vLISworkTable[index];
        objLISData.fSetPANItoI3();
        objLISData.ALIData.I3Data.fCreateLegacyALIrecord();
        objLISData.ALIData.I3Data.fCreateLegacyALI30Xrecord();
        objLISData.ALIData.I3Data.fCreateNextGenALIrecord();

        objLISData.enumThreadSending = LIS;
        objLISData.ALIData.I3Data.enumLISfunction = I3_DATA_RECEIVED;
        clock_gettime(CLOCK_REALTIME, &objLISData.ALIData.timespecTimeAliAutoRebidOK);
        enqueue_Main_Input(LIS, objLISData);

        
        vLISworkTable.erase(vLISworkTable.begin()+index);        

       }
       else {
        sem_post(&sem_tMutexLISWorkTable);
        continue;
       }
       // ADR Record .......................................................................
      }
      else {
      // NON ADR Record .......................................................................
       //cout <<"LIS.cpp NON ADR Record .." << endl;
       objLISData = vLISworkTable[index];
       objLISData.ALIData.fClear();
       objLISData.ALIData.I3Data.fClear();
       objLISData.ALIData.I3Data.I3XMLData.XMLstring.clear();
       objLISData.ALIData.I3Data.strPidfXMLData.clear();
       objLISData.ALIData.I3Data.strALI30WRecord.clear();
       objLISData.ALIData.I3Data.strALI30XRecord.clear();
       objLISData.ALIData.I3Data.strNGALIRecord.clear();
       objLISData.ALIData.I3Data.fLoadLocationData(objData, &strError); 
       objLISData.fSetPANItoI3();     
       objLISData.ALIData.I3Data.fCreateLegacyALIrecord();
       objLISData.ALIData.I3Data.fCreateLegacyALI30Xrecord();
       objLISData.ALIData.I3Data.fCreateNextGenALIrecord();

       objLISData.enumThreadSending = LIS;
       objLISData.ALIData.I3Data.enumLISfunction = I3_DATA_RECEIVED;
       clock_gettime(CLOCK_REALTIME, &objLISData.ALIData.timespecTimeAliAutoRebidOK);
       enqueue_Main_Input(LIS, objLISData);

       objMessage.fMessage_Create(LOG_CONSOLE_FILE,278, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objBLANK_TEXT_DATA,
                                 objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_278, objData.stringDataIn); 
       enQueue_Message(LIS,objMessage); 
       vLISworkTable.erase(vLISworkTable.begin()+index);
      }
      // NON ADR Record .......................................................................
     
     sem_post(&sem_tMutexLISWorkTable);					
      
//  }//end if (boolLISPortData)

 cout << "past if" << endl;
 //  sem_post(&sem_tMutexLISPortDataQ);


   








  } while (!boolSTOP_EXECUTION);
/*************************************************************************************************************************************************************************************************/
/**************************************************************************************************************************************************************************************************/
/*

									End Main LIS Thread Loop


*/
/*************************************************************************************************************************************************************************************************/
/*************************************************************************************************************************************************************************************************/
 objMessage.fMessage_Create(0, 299, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LIS_MESSAGE_299);
 objMessage.fConsole();
 return NULL;

}// end LIS_Thread()
/*************************************************************************************************************************************************************************************************/
/**************************************************************************************************************************************************************************************************/
/*

									End LIS Thread


*/
/*************************************************************************************************************************************************************************************************/
/*************************************************************************************************************************************************************************************************/



/****************************************************************************************************
*
* Name:
*  Function: void LIS_Messaging_Monitor_Event()
*
*
* Description:
*   An event handler within LIS_Thread() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from LIS.  When activated it
*
*   1. Checks for messages on the message Q, if none then exit.
*
*   2. retrieves messages until Q is empty and processes them (send to main Q).
*
*
* Parameters: 				
*   union_sigvalArg			Unused
*
* Variables:
*   CLOCK_REALTIME                      Global  - <ctime> constant
*   intRC				Local   - return code
*   itimerspec_LISHealthMonitorDelay     Local   - <ctime> struct which contains timing data that for the timer
*   itimerspecDisableTimer              Global  - <ctime> struct which contains data that disables the timer
*   objMessage                          Local   - <MessageClass> object
*   queue_objLISMessageQ                Global  - <queue><MessageClass> queue of LIS messages
*   sem_tMutexLISMessageQ               Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   sem_tMutexMainMessageQ		Global  - <semaphore.h> mutex to prevent multiple Q operations at same time
*   timer_t_LISHealthMonitorTimerId      Global  - <ctime> timer id for this function
*                                                                       
* Functions:  
*   .empty()				Library <queue>                 (bool)
*   .front()				Library <queue>                 (*this)
*   .pop()				Library <queue>                 (void)
*   .push()				Library <queue>                 (void)
*   Semaphore_Error()                   Global  <globalfunctions.h>     (void)
*   sem_wait()             		Library <semaphore.h>           (int)
*   sem_post()				Library <semaphore.h>           (int)
*   timer_settime()                     Library <ctime>                 (int)
*
*
* Author(s):
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void *LIS_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 Thread_Data            	ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="LIS Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in LIS.cpp::LIS_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 //Disable Timer
//if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_t_LISHealthMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}

do
{

 experient_nanosleep(intLIS_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in LIS_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexLISMessageQ);
 if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISMessageQ, "sem_wait@sem_tMutexLISMessageQ in LIS_Messaging_Monitor_Event", 1);}

 while (!queue_objLISMessageQ.empty())
  {
    objMessage = queue_objLISMessageQ.front();
    queue_objLISMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;
      case 200:						
       // "[DEBUG] LIS Thread Created, PID %%% ThreadSelf %%%" 
       break; 
      case 201:						
       // LIS Thread Failed to InitiLISze, System Rebooting" should not pass through here.......
       break;
      case 202:						
       // "[DEBUG] LIS Health Monitor Timer Event InitiLISzed"
       break;
      case 203:
       // "[WARNING] LIS Thread Signalling Semaphore failed to Lock"						
       break;
      case 204:						
       // "<HTTP De-reference sent to : %%%"
       break;
      case 205:						
       // ""[DEBUG] LIS %%% %%% Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 206:						
       //  "[WARNING] LIS Mutex Semaphore Error (Code=%%%, MSG=%%%)"
       break;
      case 207:
       // "[WARNING] LIS %%% %%% < NAK Recieved"						
       break;
      case 208:						
       // "[WARNING] LIS %%% %%% - Begin Reduced Heartbeat Interval; Interval=%%% (Ping %%%)"
       break;
      case 209:
       // "LIS %%% %%% < ACK Received"						
       break;
      case 210:						
       // "[INFO] LIS %%% %%% - First ACK Received"
       break;
      case 211:						
       // "[WARNING] LIS %%% %%% - End Reduced Heartbeat Interval"
       break;
      case 212:						
       // "[WARNING] LIS %%% %%% - Heartbeat Send Failure"
       break;
      case 213:						
       // "LIS %%% %%% > Heartbeat Sent"
       break;
      case 214:                                           
       // "[DEBUG] LIS ScoreBoard Timer Event InitiLISzed"
       break;
      case 215:                                           
       // "[WARNING] LIS %%% %%% Unexpected Character Count %%%"
       break;
      case 216:
       // "[WARNING] LIS %%% %%% InvLISd LIS Data Received, /START OF SAMPLE/%%%/END OF SAMPLE/"
       break;
      case 217:						
       // "[WARNING] LIS %%% %%% < Response Received too Long, Length = (%%%) /START OF SAMPLE/%%%/END OF SAMPLE/"
       break;
      case 218:						
       // "[WARNING] LIS %%% %%% < InvLISd Data Received for %%%, Data=\"%%%\""
       break;
      case 219:						
       // "[WARNING] LIS %%% %%% < Telephone Number %%% Not Found for Trunk %%%, Data=\"%%%\"
       break;
      case 220:                                           
       // "[WARNING] LIS %%% %%% - Record Timeout (Trunk=%%%, Phone=%%%)"
       break;
      case 221:                                           
       // 
       break;
      case 222:						
       // "LIS %%% %%% > Request Sent to Database=%%%; RequestKey=%%%"
       break;
      case 223:						
       // "[ALARM] LIS %%% %%% - Port Down for %%%"
       break;
      case 224:						
       // "[WARNING] LIS %%% %%% - Reminder - Port Down for %%%" 
       break;
      case 225:                                           
       // "[WARNING] LIS No Port Pairs Available for Database=%%%; RequestKey=%%%"
       break;
      case 226:                                           
       //  "LIS %%% %%% < Data Recieved: %%%"
       break;
      case 227:                                           
       // "[ALARM] %%% - Port Restored"
       break;
      case 228:                                           
       // "[WARNING] LIS %%% %%% < Heartbeat ACK Timeout"
       break;
      case 229:                                           
       // "[WARNING] LIS %%% %%% < Broadcast Message Recieved: %%%"
       break;
      case 230:                                           
       // "[ALARM] LIS %%% %%% < Broadcast Message Host Going Out of Service"
       break;
      case 231:                                           
       // "[WARNING] LIS %%% %%% > LIS Request Retransmited; RequestKey=%%%"
       break;
      case 232:
       // "[WARNING] LIS %%% %%% < Extra Data Recieved:%%%"
       break;
      case 233:                                           
       // "[WARNING] %%% - UDP Error (Code=%%%, MSG=%%%)"
       break;
      case 234:                                           
       // "[WARNING] %%% - Port Restart Failed (Code=%%%)"
       break;
      case 235:                                           
       // "[WARNING] %%% - Port Data Reception Restored"
       break;
      case 236:                                           
       // "[ALARM] %%% - Port Data Reception Inhibited for %%% String Buffer Size > %%%"
       break;
      case 237:
       // "[WARNING] LIS %%% %%% - Unexpected Data Received from Outside IP Address %%%"
       break;
      case 238:
       // "[WARNING] LIS - Error creating checksum: LIS Request key length = %%%"
       break;
      case 239:
       // "[WARNING] LIS - Port Listen Thread Failed to InitiLISze, System Rebooting"
       break;
      case 240:
       // "[WARNING] LIS %%% %%% < Unexpected Data Recieved:%%%"
        break;
      case 241:
       //"[WARNING] %%% %%% %%% - Buffer Cleared Length = %%%"
       break;
      case 242:
       // "[WARNING] %%% - %%% Count Rollover=%%%"
       break;
      case 243:
       // "[WARNING] %%% - %%% Count Rollover=%%%"
       break;
      case 244:
       // "[WARNING] LIS %%% %%% < Consecutive NAK(s) over Past 24 Hours = %%%"
       break;
      case 245:
       //  "[WARNING] LIS %%% %%% < Stray ACK(s) over Past 24 Hours = %%%"
       break;
      case 246:
       // "[WARNING] LIS %%% %%% < Stray ACK Received"
       break;
      case 247:
       // "[WARNING] LIS %%% %%% < %%% Stray ACKs Received Between VLISd ACKs"
       break;
      case 248:
       //  "[WARNING] Unable to set CPU affinity for LIS Thread"
       break;
      case 249:
       // Consecutive InvLISd LIS Records Counter
       break;
      case 250:
       //  Discarded Packet Counter Rollover
       break;
      case 251:
       // "[WARNING] %%% - Incoming Network Packets Discarded over Past 24 Hours = %%%" 
       break;
      case 252:
       // "LIS %%% < Raw Data Received from IP=%%% (Data Follows) %%%"
       break;
      case 253:
       // "%%% > Data sent to IP=%%% (Data Follows)"
       break;
      case 254:
       // ">[WARNING] Configuration Error: LIS Database %%% Request Key Digits=%%%" 
       break;
      case 255:
       // ">[WARNING] LIS Record Fail on Database=%%%, Retry on Database=%%%" 
       break;
      case 256:
       // special rebid Tracker to main to count rebids
       break;
     case 257:
       //">[WARNING] Error Decoding LIS Record ; Message=%%%"
       break;
     case 258:
       //"">[WARNING] LIS E2 Record XML Field %%% Truncated; Length=%%%; Max=%%%"
       break;
     case 259:
       // ">[WARNING] LIS E2 Record Unable to Parse XML; Error=%%% (Data Follows)"
       break;
     case 260:
       // ">[WARNING] LIS Record Fail; No index available to Bid"
       break;
     case 261:
       // "-[WARNING] Area Code %%% not found in NPD Table, System Will Bid Test Key"
       break;  
     case 262: 
       // "[WARNING] LIS Bid Failed for %%%"
       break;
     case 263:
       // "[ALARM] LIS Port Pair %%% Down"
       break;
     case 264:
       // "[ALARM] LIS Port Pair %%% Restored"
       break;
     case 277:
       // ">[WARNING] HTTP Error: %%%"
       break;
     case 278:
       // ">HTTP Data Received : %%%"
       break;
     case 279:
       // ">NG911 Location Data received (Data Follows)"
       break;
     case 298:
       //  "-[WARNING] Mutex Semaphore Error (Code=%%%, MSG=%%%)"
       break;
     case 299:                                           
      // thread complete     
      break;
      
     case 999:
      // test area..................................
             
      break;	
     default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in LIS Monitor" << endl;
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock LIS Message Q then Main Message Q
 sem_post(&sem_tMutexLISMessageQ);
 sem_post(&sem_tMutexMainMessageQ);


 Check_For_Stale_LIS_Bids();



}while (!boolSTOP_EXECUTION);


return NULL;
}// end LIS_Messaging_Monitor_Event()


int IndexOfTableWithGeolocation(string strLocationURI) {
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vLISworkTable;
 vector <ExperientDataClass>::size_type         sz = vLISworkTable.size();
 string 					stringOne, stringTwo;

 if (strLocationURI.empty()) {return -1;}

  for (unsigned int i = 0; i< sz; i++)   { 
   if (strLocationURI == vLISworkTable[i].ALIData.I3Data.objGeoLocationHeaderURI.strURI) { return i;}
  }
  return -1;
}


int IndexOfTableWithEntity(string strInput) {
 extern vector <ExperientDataClass>             vLISworkTable;
 vector <ExperientDataClass>::size_type         sz = vLISworkTable.size();

 if (strInput.empty()) {return -1;}

  for (unsigned int i = 0; i< sz; i++)   {
   if (strInput == vLISworkTable[i].strPidfloByValEntity) {return i;}
  }
 return -1;
}



int IndexOfTableWithBidID(string strBid, bool boolCallBackUsed) {
 // table must be semaphore protected before calling this function !

 extern vector <ExperientDataClass>             vLISworkTable;
 vector <ExperientDataClass>::size_type         sz = vLISworkTable.size();
 string 					stringOne, stringTwo;

 if (strBid.empty()) {return -1;}

  for (unsigned int i = 0; i< sz; i++)   {

  if (strBid == vLISworkTable[i].strNenaCallId)                {return i;}
  if (strBid == vLISworkTable[i].ALIData.I3Data.strBidId)      {return i;}
  if (strBid == vLISworkTable[i].ALIData.I3Data.strNenaCallId) {return i;} 
/*
    if (boolCallBackUsed) {
     stringOne = StripPlusOne(vLISworkTable[i].ALIData.I3Data.I3XMLData.CallInfo.strCallbackNum);
    }
    else {
     stringOne = StripPlusOne(vLISworkTable[i].ALIData.I3Data.strBidId);
    }
    stringOne = FindandReplace(stringOne, "+", "");
    stringOne = RemoveAllSpaces(stringOne);
    stringTwo = StripPlusOne(strBid);
    stringTwo = FindandReplace(stringTwo, "+", "");
    stringTwo = RemoveAllSpaces(stringTwo);
    //cout << stringOne << " == " << stringTwo << endl;
    if (stringOne == stringTwo)       { return i;}

*/
  }
  return -1;
}


void POST_URN_Thread(ExperientDataClass objLISData, string strURL, string strRequest, string strUUID, string strType, int intPortNum ) {
 MessageClass					 objMessage;
 ExperientHTTPPort                               LIS_HTTP_Port;
 string                                          strCertFile;
 string                                          strLOSTaccept      = "application/lost+xml;charset=utf-8";
 string                                          strLOSTcontenttype = "application/lost+xml;charset=utf-8";
 string                                          strOtherHeader;
 string                                          strError           = "";
 int                                             intRC;
 int                                             index;
 DataPacketIn                                    objData;

 extern string                                   strNG911_SOS_SERVICE_BID;
 extern string                                   strECRF_CERTIFICATE;
 extern vector <ExperientDataClass>              vLISworkTable;
 extern vector <ExperientDataClass>     	 vMainWorkTable;
 extern int 					 intECRF_HTTP_TIMEOUT_SEC;

 extern int  IndexWithCallUniqueIDMainTable(string strUniqueID);
 extern int  IndexWithFreeswitchChannelUUID(string strUUID);
 extern void SetasRXServiceURNList(string strUUID, int intPortNum);

 strCertFile.clear();
 if (strECRF_CERTIFICATE != DEFAULT_VALUE_LIS_INI_KEY_11 ) {
  strCertFile = strECRF_CERTIFICATE;
 }

 //cout << "Call POST_URN_Thread" << endl;
 LIS_HTTP_Port.enumPortType  = LIS;
 LIS_HTTP_Port.intPortNumber = intPortNum;  
 LIS_HTTP_Port.SetAccept("application/lost+xml");
 LIS_HTTP_Port.SetContentType("application/lost+xml");
 LIS_HTTP_Port.SetFollowRedirects(1);
 LIS_HTTP_Port.SetURLPort(0);
 LIS_HTTP_Port.SetLocalHost(NULL);
#ifdef IPWORKS_V16
 LIS_HTTP_Port.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 //  LIS_HTTP_Port.SetAuthScheme(1);
 //  LIS_HTTP_Port.SetUser(strCTI_USERNAME.c_str());
 //   LIS_HTTP_Port.SetPassword(strCTI_PASSWORD.c_str());
 LIS_HTTP_Port.SetTimeout(intECRF_HTTP_TIMEOUT_SEC);
 LIS_HTTP_Port.Config("KeepAlive=false");
/*
0 (None) 		No events are logged.
1 (Info - default) 	Informational events are logged.
2 (Verbose) 		Detailed data is logged.
3 (Debug) 		Debug data is logged.
*/
 LIS_HTTP_Port.Config("LogLevel=3");
//   LIS_HTTP_Port.Config("AbsoluteTimeout=true");
 LIS_HTTP_Port.Config("TcpNoDelay=true");

 //  LIS_HTTP_Port.SetPragma("no-cache");

 if (strCertFile.length()) {
  LIS_HTTP_Port.SetSSLCertStoreType(6);
  LIS_HTTP_Port.SetSSLCertStore((char*) strECRF_CERTIFICATE.c_str(), strECRF_CERTIFICATE.length());
  LIS_HTTP_Port.SetSSLCertSubject("*");
 }

 LIS_HTTP_Port.strRequestID = strUUID;

 strOtherHeader = "Cache-Control: no-cache\r\n";
 strOtherHeader += "Nena-Call-ID: ";
 strOtherHeader += strUUID;         
 intRC = LIS_HTTP_Port.SetOtherHeaders((char*)strOtherHeader.c_str());
 intRC = LIS_HTTP_Port.SetPostData(strRequest.c_str(), strRequest.length());

 intRC = LIS_HTTP_Port.Post(strURL.c_str());
  
 if (intRC) {
   strError = LIS_HTTP_Port.GetLastError();
   switch (intPortNum) {
    case 5: 
        index = IndexOfTableWithBidID(strUUID);
        if (index >= 0) {
         vLISworkTable[index].ALIData.I3Data.ServiceURNList.boolBidMade   = true;  
         vLISworkTable[index].ALIData.I3Data.ServiceURNList.boolListBuilt = true;
         vLISworkTable[index].ALIData.I3Data.ServiceURNList.vServiceURN.clear(); 
        }     
        break;
    default:
        //cout << "remove bid id -> " << intPortNum << endl;
      //  vLISworkTable[index].ALIData.I3Data.ServiceURNList.fRemoveIndexWithBid(intPortNum);
       // //cout << "after remove bid" << endl;
        break;
   }
 }

 LIS_HTTP_Port.DoEvents(); 


// //cout << "Post URN -> " << strURL << endl;
// //cout << "Request ID -> " << LIS_HTTP_Port.strRequestID << endl;
// //cout << "Request" << strRequest << endl;



 if (intRC) {
    objLISData.ALIData.I3Data.enumLISfunction = ECRF_HTTP_TIMEOUT; 
    objLISData.intLogMessageNumber = 277;
    objLISData.strErrorMessage     = LIS_HTTP_Port.GetLastError();
 }
 else {
    objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_DATA_RECEIVED; 
 }
  
 enqueue_Main_Input(LIS, objLISData);




/*
 sem_wait(&sem_tMutexMainWorkTable);
 index = IndexWithFreeswitchChannelUUID(objLISData.FreeswitchData.objChannelData.strChannelID);
 if (index >= 0) {

  objLISData.CallData = vMainWorkTable[index].CallData;
  objLISData.TextData = vMainWorkTable[index].TextData;
  objLISData.ALIData  = vMainWorkTable[index].ALIData;  
  objLISData.intCallStateCode  = vMainWorkTable[index].intCallStateCode;
  objLISData.intCallStatusCode = vMainWorkTable[index].intCallStatusCode;
  objLISData.boolConnect       = vMainWorkTable[index].boolConnect;
 }
 else {
  //cout << "not found in index ....." << endl;
  sem_post(&sem_tMutexMainWorkTable); return;   
 }
 sem_post(&sem_tMutexMainWorkTable);    

 if (intRC) { 

  objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_TIMEOUT; 
  objMessage.fMessage_Create(LOG_CONSOLE_FILE,277, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                             objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_277b, strError ); 
  enQueue_Message(LIS,objMessage);
  objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
  objLISData.WindowButtonData.fSetButtonsForALIReceived();
  objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
  Queue_WRK_Event(objLISData);
 }
 else {
  ////cout << "ALI Button on" << endl;
  objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
  objLISData.WindowButtonData.fSetButtonsForALIReceived();
  objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
  Queue_WRK_Event(objLISData);
 }
 */

// //cout << "Exit POST_URN_Thread" << endl; 
}

void GET_URL_THREAD(ExperientDataClass objLISData, string strURL, string strUUID, string strType, int PortNum ) {

 MessageClass					 objMessage;
 ExperientHTTPPort                               LIS_HTTP_Port;
 string                                          strCertFile;
 string                                          strHELDaccept      = "application/held+xml;charset=utf-8";
 string                                          strHELDcontenttype = "application/held+xml;charset=utf-8";
 string                                          strOtherHeader;
 int                                             intRC;
 int                                             index;

 extern string                                   strLIS_CERTIFICATE;
 extern vector <ExperientDataClass>     	 vMainWorkTable;
 extern int 					 intLIS_HTTP_TIMEOUT_SEC;

 extern int IndexWithCallUniqueIDMainTable(string strUniqueID);

 strCertFile.clear();
 if (strLIS_CERTIFICATE != DEFAULT_VALUE_LIS_INI_KEY_8 ) {
  strCertFile = strLIS_CERTIFICATE;
 }


 //cout << "Call GET_URL_Thread" << endl;
 LIS_HTTP_Port.enumPortType  = LIS;
 LIS_HTTP_Port.intPortNumber = PortNum;  
 LIS_HTTP_Port.SetAccept("application/held+xml;charset=utf-8");
 LIS_HTTP_Port.SetContentType("application/held+xml;charset=utf-8");
 LIS_HTTP_Port.SetFollowRedirects(1);
 LIS_HTTP_Port.SetURLPort(0);
 LIS_HTTP_Port.SetLocalHost(NULL);
#ifdef IPWORKS_V16
 LIS_HTTP_Port.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 //  LIS_HTTP_Port.SetAuthScheme(1);
 //  LIS_HTTP_Port.SetUser(strCTI_USERNAME.c_str());
 //   LIS_HTTP_Port.SetPassword(strCTI_PASSWORD.c_str());
 LIS_HTTP_Port.SetTimeout(intLIS_HTTP_TIMEOUT_SEC);
 LIS_HTTP_Port.Config("KeepAlive=false");
/*
0 (None) 		No events are logged.
1 (Info - default) 	Informational events are logged.
2 (Verbose) 		Detailed data is logged.
3 (Debug) 		Debug data is logged.
*/
 LIS_HTTP_Port.Config("LogLevel=3");
//   LIS_HTTP_Port.Config("AbsoluteTimeout=true");
 LIS_HTTP_Port.Config("TcpNoDelay=true");

 //  LIS_HTTP_Port.SetPragma("no-cache");

 if (strCertFile.length()) {
  LIS_HTTP_Port.SetSSLCertStoreType(6);
  LIS_HTTP_Port.SetSSLCertStore((char*)strCertFile.c_str(),strCertFile.length());
  LIS_HTTP_Port.SetSSLCertSubject("*");
 }

 LIS_HTTP_Port.strRequestID = strUUID;

// //cout << "NGA Differece -> " << strURL << endl;


 strOtherHeader = "Cache-Control: no-cache\r\n";
 strOtherHeader += "Nena-Call-ID: ";
 strOtherHeader += strUUID;         
 intRC = LIS_HTTP_Port.SetOtherHeaders((char*)strOtherHeader.c_str());
 intRC = LIS_HTTP_Port.Get(strURL.c_str());

 cout << "get URL -> " << strURL << endl;

 LIS_HTTP_Port.DoEvents();          


 if (intRC) {
    objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_TIMEOUT; 
    objLISData.intLogMessageNumber = 277;
    objLISData.strErrorMessage     = LIS_HTTP_Port.GetLastError();
 }
 else {
    objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_DATA_RECEIVED; 
 }
  
 enqueue_Main_Input(LIS, objLISData);



/*

 // Update CallData in object to that of Main Table ....
 sem_wait(&sem_tMutexMainWorkTable);
 index = IndexWithCallUniqueIDMainTable(objLISData.CallData.stringUniqueCallID);
 if (index >= 0) {

  objLISData.CallData = vMainWorkTable[index].CallData;
  objLISData.TextData = vMainWorkTable[index].TextData;
  objLISData.ALIData  = vMainWorkTable[index].ALIData;  
  objLISData.intCallStateCode  = vMainWorkTable[index].intCallStateCode;
  objLISData.intCallStatusCode = vMainWorkTable[index].intCallStatusCode;
  objLISData.boolConnect       = vMainWorkTable[index].boolConnect;
 }
 else {
  sem_post(&sem_tMutexMainWorkTable); return;   
 }
 sem_post(&sem_tMutexMainWorkTable);    
 


 if (intRC) {
  objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_TIMEOUT; 
  objMessage.fMessage_Create(LOG_CONSOLE_FILE,277, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                             objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_277, LIS_HTTP_Port.GetLastError() ); 
  enQueue_Message(LIS,objMessage);
  objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
  objLISData.WindowButtonData.fSetButtonsForALItimeout(true);
  objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT, true);
  Queue_WRK_Event(objLISData);
 }
 else {        
  ////cout << "ALI Button on" << endl;
  objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
  objLISData.WindowButtonData.fSetButtonsForALIReceived();
  objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
  Queue_WRK_Event(objLISData);
 }
*/

cout << "Exit GET_URL_Thread" << endl; 
 return;
}




void POST_URL_Thread(ExperientDataClass objLISData, string strURL, string strRequest, string strUUID, string strType, int intPortNum ) {

 int 						intRC;
 MessageClass					objMessage;
 ExperientHTTPPort                              LIS_HTTP_Port;
 string                                         strCertFile        = "/home/experient/certificates/lisadr/lisadr.pem";
 string                                         strHELDaccept      = "application/held+xml;charset=utf-8";
 string                                         strHELDcontenttype = "application/held+xml;charset=utf-8";
 string                                         strOtherHeader;
 int                                            index;

 extern string                                  strLIS_CERTIFICATE;
 extern vector <ExperientDataClass>     	vMainWorkTable;
 extern int 					intLIS_HTTP_TIMEOUT_SEC;

 extern int IndexWithCallUniqueIDMainTable(string strUniqueID);

 strCertFile.clear();
 if (strLIS_CERTIFICATE != DEFAULT_VALUE_LIS_INI_KEY_8 ) {
  strCertFile = strLIS_CERTIFICATE;
 }


 //cout << "Call POST_URL_Thread" << endl;
 LIS_HTTP_Port.enumPortType  = LIS;
 LIS_HTTP_Port.intPortNumber = intPortNum;  
 LIS_HTTP_Port.SetAccept("application/held+xml;charset=utf-8");
 LIS_HTTP_Port.SetContentType("application/held+xml;charset=utf-8");
 LIS_HTTP_Port.SetFollowRedirects(1);
 LIS_HTTP_Port.SetURLPort(0);
 LIS_HTTP_Port.SetLocalHost(NULL);
#ifdef IPWORKS_V16
 LIS_HTTP_Port.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 //  LIS_HTTP_Port.SetAuthScheme(1);
 //  LIS_HTTP_Port.SetUser(strCTI_USERNAME.c_str());
 //   LIS_HTTP_Port.SetPassword(strCTI_PASSWORD.c_str());
 LIS_HTTP_Port.SetTimeout(intLIS_HTTP_TIMEOUT_SEC);
 LIS_HTTP_Port.Config("KeepAlive=false");
/*
0 (None) 		No events are logged.
1 (Info - default) 	Informational events are logged.
2 (Verbose) 		Detailed data is logged.
3 (Debug) 		Debug data is logged.
*/
 LIS_HTTP_Port.Config("LogLevel=3");
//   LIS_HTTP_Port.Config("AbsoluteTimeout=true");
 LIS_HTTP_Port.Config("TcpNoDelay=true");

 //  LIS_HTTP_Port.SetPragma("no-cache");

 if (strCertFile.length()) {
  LIS_HTTP_Port.SetSSLCertStoreType(6);
  LIS_HTTP_Port.SetSSLCertStore((char*)strCertFile.c_str(),strCertFile.length());
  LIS_HTTP_Port.SetSSLCertSubject("*");
 }

 LIS_HTTP_Port.strRequestID = strUUID;

 strOtherHeader = "Cache-Control: no-cache\r\n";
 strOtherHeader += "Nena-Call-ID: ";
 strOtherHeader += strUUID;         
 intRC = LIS_HTTP_Port.SetOtherHeaders((char*)strOtherHeader.c_str());
 intRC = LIS_HTTP_Port.SetPostData(strRequest.c_str(), strRequest.length());
 intRC = LIS_HTTP_Port.Post(strURL.c_str());

// //cout << "POST - URL " << strURL << endl;
// //cout << "POST - REQ " << strRequest << endl;
 LIS_HTTP_Port.DoEvents();         

 // need to send this to main to special section to match with table entry
 // due to long timeouts .... mutex here has caused locks ....

 
/*

 causes thread lockup .... fred

 // Update CallData in object to that of Main Table ....
 sem_wait(&sem_tMutexMainWorkTable);
 index = IndexWithCallUniqueIDMainTable(objLISData.CallData.stringUniqueCallID);
 if (index >= 0) {

  objLISData.CallData = vMainWorkTable[index].CallData;
  objLISData.TextData = vMainWorkTable[index].TextData;
  objLISData.ALIData  = vMainWorkTable[index].ALIData;  
  objLISData.intCallStateCode  = vMainWorkTable[index].intCallStateCode;
  objLISData.intCallStatusCode = vMainWorkTable[index].intCallStatusCode;
  objLISData.boolConnect       = vMainWorkTable[index].boolConnect;
 }
 else {

  sem_post(&sem_tMutexMainWorkTable); return;   
 }
 sem_post(&sem_tMutexMainWorkTable);    
*/

 if (intRC) {
    objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_TIMEOUT; 
    objLISData.intLogMessageNumber = 277;
    objLISData.strErrorMessage     = LIS_HTTP_Port.GetLastError();
 }
 else {
    objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_DATA_RECEIVED; 
 }
  
 enqueue_Main_Input(LIS, objLISData);

/*
 if (intRC) { 

  objLISData.ALIData.I3Data.enumLISfunction = LIS_HTTP_TIMEOUT; 
  objMessage.fMessage_Create(LOG_CONSOLE_FILE,277, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objLISData.fCallData(), objLISData.TextData,
                             objBLANK_LIS_PORT, objBLANK_FREESWITCH_DATA, objLISData.ALIData, LIS_MESSAGE_277, LIS_HTTP_Port.GetLastError() ); 
  enQueue_Message(LIS,objMessage);
  objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_TIMEOUT;
  objLISData.WindowButtonData.fSetButtonsForALItimeout(true);
  objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_TIMEOUT, true);
  Queue_WRK_Event(objLISData);
 }
 else {
  ////cout << "ALI Button on" << endl;
  objLISData.ALIData.intALIState = XML_STATUS_CODE_ALI_RECEIVED;
  objLISData.WindowButtonData.fSetButtonsForALIReceived();
  objLISData.fCallInfo_XML_Message(XML_STATUS_CODE_ALI_RECEIVED, true);
  Queue_WRK_Event(objLISData);
 }
 
*/

 cout << "Exit POST_URL_Thread" << endl; 
 return;
}


bool ConvertToXML(XMLNode*  ReturnNode, string strInput, int i) {
 XMLResults 		pResults;
 string                 strError;
 char*                  cptrResponse;
 string                 strXMLString;
 extern string		ConvertXMLError2String(XMLError xe);
 XMLNode                TopNode, ChildNode;

 extern XMLNode LoadChildNode( XMLNode ParentNode , string strName);

 if (strInput.empty()) { return false;}
 ReturnNode->emptyNode();
 TopNode = XMLNode::parseString(strInput.c_str(),NULL,&pResults); 

 if (pResults.error) {
  strError = XMLNode::getError(pResults.error);
  SendCodingError("LIS.cpp ConvertToXML() -> " + strError);

  return false;
 }

 switch (i) {
  case 1:
   ChildNode = LoadChildNode( TopNode , "locationResponse"); 
   break;
  case 2:
   ChildNode = LoadChildNode( TopNode , "EmergencyCallData.ProviderInfo");
   break; 
  case 3:
   ChildNode = LoadChildNode( TopNode , "EmergencyCallData.ServiceInfo"); 
   break;
  case 4:
   ChildNode = LoadChildNode( TopNode , "EmergencyCallData.SubscriberInfo");
   break;
  default:
   SendCodingError("LIS.cpp ConvertToXML() Unknown integer -> " + int2str(i)); 
   return false;
 }


 *ReturnNode = ChildNode.deepCopy();
/* 
cptrResponse =  ReturnNode->createXMLString();
 if (cptrResponse != NULL)
  {
   strXMLString = cptrResponse;
   free(cptrResponse);
  }
 cptrResponse = NULL;
 
 //cout << "ConvertToXML..................................................................................................." << endl;
 //cout << strXMLString << endl;
*/
 return true;
}


void Check_For_Stale_LIS_Bids() {

 int					intRC;
 size_t					sz;
 long double     			ldTimeDiff = 0.0;
 struct timespec 			timespecTimeNow;
 bool                                   boolRunningFlag;
 ExperientDataClass                     objLISData;
 extern vector <ExperientDataClass>     vLISworkTable;
 extern int 				intLIS_HTTP_TIMEOUT_SEC;
 extern ADR_REQUIREMENTS 		ADR_REQUIRED;

 // check table timeout .25 seconds after http timeout
 ldTimeDiff += intLIS_HTTP_TIMEOUT_SEC;
 ldTimeDiff += 0.25;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 intRC = sem_wait(&sem_tMutexLISWorkTable);					
 if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISWorkTable, "LIS.cpp fn Check_For_Stale_LIS_Bids() - sem_wait@sem_tMutexLISWorkTable", 1);}

 sz = vLISworkTable.size();
 for (unsigned int i = 0; i < sz; i++) {
  boolRunningFlag = true;

  if (time_difference(timespecTimeNow, vLISworkTable[i].timespecTimeRecordReceivedStamp)  > ldTimeDiff) {
    
    boolRunningFlag = boolRunningFlag && (vLISworkTable[i].ALIData.I3Data.boolPidfLoRXflag        ||(!ADR_REQUIRED.boolRequirePidfLo)); 
    boolRunningFlag = boolRunningFlag && (vLISworkTable[i].ALIData.I3Data.boolProviderInfoRXflag  ||(!ADR_REQUIRED.boolRequireProviderInfo)); 
    boolRunningFlag = boolRunningFlag && (vLISworkTable[i].ALIData.I3Data.boolServiceInfoRXflag   ||(!ADR_REQUIRED.boolRequireServiceInfo)); 
    boolRunningFlag = boolRunningFlag && (vLISworkTable[i].ALIData.I3Data.boolSubsciberInfoRXflag ||(!ADR_REQUIRED.boolRequireSubsciberInfo)); 
 //   boolRunningFlag = boolRunningFlag && (vLISworkTable[i].ALIData.I3Data.ServiceURNList.fAll_URIS_RX());

   if (boolRunningFlag) {
    // Data complete enough ....
    
    //cout << "send data after timeout" << endl;
    objLISData = vLISworkTable[i];
    objLISData.fSetPANItoI3();
    objLISData.ALIData.I3Data.fCreateLegacyALIrecord();
    objLISData.ALIData.I3Data.fCreateLegacyALI30Xrecord();
    objLISData.ALIData.I3Data.fCreateNextGenALIrecord();
    objLISData.enumThreadSending = LIS;
    objLISData.ALIData.I3Data.enumLISfunction = I3_DATA_RECEIVED;
    clock_gettime(CLOCK_REALTIME, &objLISData.ALIData.timespecTimeAliAutoRebidOK);
    enqueue_Main_Input(LIS, objLISData);
   }  
   //leave loop because of the erasure ....
   vLISworkTable.erase(vLISworkTable.begin()+i);
   break;
  }


 }

 sem_post(&sem_tMutexLISWorkTable);
 return;
}







