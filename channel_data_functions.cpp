/*****************************************************************************
* FILE: channel_data_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the Channel_Data Class 
*
*
*
* AUTHOR: 01/29/2012 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"

extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data			       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern Trunk_Type_Mapping		       	TRUNK_TYPE_MAP;
extern Port_Data                               	objBLANK_ANI_PORT;
extern Port_Data                               	objBLANK_ALI_PORT;
extern Port_Data                               	objBLANK_WRK_PORT;
extern Port_Data			       	objBLANK_AMI_PORT;
 
// constructor
Refer_Data::Refer_Data() {

 strContactParams.clear();
 strContactURI.clear();
 strContactHost.clear();
 strContactUser.clear();
 strContactPort.clear();

}
//assignment operator
Refer_Data& Refer_Data::operator=(const Refer_Data& b) {
  if (&b == this ) {return *this;}

  strContactParams = b.strContactParams;
  strContactURI    = b.strContactURI;
  strContactHost   = b.strContactHost;
  strContactUser   = b.strContactUser;
  strContactPort   = b.strContactPort;
  return *this;
}

void Refer_Data::fClear() {

 strContactParams.clear();
 strContactURI.clear();
 strContactHost.clear();
 strContactUser.clear();
 strContactPort.clear();

}

void Refer_Data::fDisplay() {

 cout << "Contact Params : " << strContactParams << endl;
 cout << "Contact URI    : " << strContactURI << endl;
 cout << "Contact Host   : " << strContactHost << endl;
 cout << "Contact User   : " << strContactUser << endl;
 cout << "Contact Port   : " << strContactPort << endl;

}

bool Refer_Data::fLoadContactParams(string strInput) {

 string strData;

 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_PARAMS_WO_COLON));

 if (strData.empty() ) {return false;}
 this->strContactParams = strData;
 return true;
}

bool Refer_Data::fLoadContactURI(string strInput) {

 string strData;

 strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_URI_WO_COLON));

 if (strData.empty() ) {return false;}
 this->strContactURI = strData;
 return true;
}

bool Refer_Data::fLoadContactHost(string strInput) {

 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_HOST_WO_COLON);

 if (strData.empty() ) {return false;}
 this->strContactHost = strData;
 return true;
}

bool Refer_Data::fLoadContactUser(string strInput) {

 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_USER_WO_COLON);

 if (strData.empty() ) {return false;}
 this->strContactUser = strData;
 return true;
}

bool Refer_Data::fLoadContactPort(string strInput) {

 string strData;

 strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_VARIABLE_SIP_CONTACT_PORT_WO_COLON);

 if (strData.empty() ) {return false;}
 this->strContactPort = strData;
 return true;
}


// constructor
Channel_Data::Channel_Data() {
  strChannelName    = "";
  strChannelID      = "";
  iGUIlineView      = 0;
  for (int i = 1; i <= NUM_WRK_STATIONS_MAX; i++) {iRingingLineNumber[i] = 0;}
  iLineNumber       = 0;
  strCallerID       = "";
  strConfDisplay    = "";
  strTranData       = "";
  strCustName       = "";
  boolConnected     = true;
  boolOnHold        = false;
  intTrunkType      = 0;
  boolIsPosition    = false;
  boolIsOutbound    = false;
  iTrunkNumber      = 0;
  iPositionNumber   = 0;
  iRingingPosition  = 0;
  strHangupCause    = "";
  iSLABarge         = 0;
  IPaddress.fClear();
  boolTDDMode       = false;
  strCallRecording  = "";
  strPresenceID     = "";
  boolMSRP          = false;
  strEndpointDisposition = "";
  strHangupDisposition   = "";
  strTransferDisposition = "";
  strValetExtension = "";
  boolParked        = false;   
 }

//assignment operator
Channel_Data& Channel_Data::operator=(const Channel_Data& b)
 {
  if (&b == this ) {return *this;}
  strChannelName    = b.strChannelName;
  strChannelID      = b.strChannelID;
  iGUIlineView      = b.iGUIlineView;
  for (int i = 1; i <= NUM_WRK_STATIONS_MAX; i++) {iRingingLineNumber[i] = b.iRingingLineNumber[i];}
  iLineNumber       = b.iLineNumber;
  strCallerID       = b.strCallerID;
  strConfDisplay    = b.strConfDisplay;
  strTranData       = b.strTranData;
  strCustName       = b.strCustName;
  boolConnected     = b.boolConnected;
  boolOnHold        = b.boolOnHold;
  intTrunkType      = b.intTrunkType;
  boolIsPosition    = b.boolIsPosition;
  boolIsOutbound    = b.boolIsOutbound;
  iTrunkNumber      = b.iTrunkNumber;
  iPositionNumber   = b.iPositionNumber;
  iRingingPosition  = b.iRingingPosition;
  strHangupCause    = b.strHangupCause;
  iSLABarge         = b.iSLABarge;
  IPaddress         = b.IPaddress;
  boolTDDMode       = b.boolTDDMode;
  strCallRecording  = b.strCallRecording;
  strPresenceID     = b.strPresenceID;
  boolMSRP          = b.boolMSRP;
  strEndpointDisposition = b.strEndpointDisposition;
  strHangupDisposition   = b.strHangupDisposition;
  strTransferDisposition = b.strTransferDisposition;
  strValetExtension      = b.strValetExtension;
  boolParked             = b.boolParked;   
  return *this;
 }

void Channel_Data::fClear()
{
 strChannelName.clear();
 strChannelID.clear();
 iGUIlineView      = 0;
 for (int i = 1; i <= NUM_WRK_STATIONS_MAX; i++) {iRingingLineNumber[i] = 0;}
 iLineNumber       = 0;

 strCallerID.clear();
 strConfDisplay.clear();
 strTranData.clear();
 strCustName.clear();
 boolConnected    = true;
 boolOnHold       = false;
 intTrunkType     = 0;
 boolIsPosition   = false;
 boolIsOutbound   = false;
 iPositionNumber  = 0;
 iRingingPosition = 0;
 strHangupCause.clear();
 iTrunkNumber     = 0;
 iSLABarge        = 0;
 IPaddress.fClear();
 boolTDDMode      = false; 
 strCallRecording.clear();
 strPresenceID.clear();
 boolMSRP         = false; 
 strEndpointDisposition.clear();
 strHangupDisposition.clear();
 strTransferDisposition.clear();
 strValetExtension.clear();
 boolParked      = false;  
}


void Channel_Data::fSetChannelHangup(Channel_Data objChannelData){
 this->boolConnected = false;
 this->boolOnHold    = false;
 this->boolParked    = false;
 this->strEndpointDisposition = objChannelData.strEndpointDisposition;
 this->strHangupCause         = objChannelData.strHangupCause;
 this->strHangupDisposition   = objChannelData.strHangupDisposition;
 this->strTransferDisposition = objChannelData.strTransferDisposition;
 return;
}

void Channel_Data::fSetChannelHangup(){
 this->boolConnected = false;
 this->boolOnHold    = false;
 this->boolParked    = false;
 this->strEndpointDisposition = "";
 this->strHangupCause         = "SOFTWARE";
 this->strHangupDisposition   = "";
 this->strTransferDisposition = "";
 return;
}



void Channel_Data::fDisplay(int i, bool showRuler)
{
 if (true) {
      cout << setw(7) << left << "Index" << setw(50) << left << "Channel Name" << setw(40) << left << "Channel UUID" << setw(15) << left << "Cid Number" << setw(40) << left << "Transfer Data" 
      << setw(6) << left << "Cdisp" << setw(25) << left << "Cid Name" << endl;
 }


 
 cout << setw(7)  << left << i;
 cout << setw(50) << left << strChannelName;
 cout << setw(40) << left << strChannelID; 
 cout << setw(15) << left << strCallerID;
 cout << setw(40) << left << strTranData;
 cout << setw(6)  << left << strConfDisplay;
 cout << setw(25) << left << strCustName <<endl;
 if (true) {
      cout << setw(7) << left << " " << setw(4) << left << "Con" << setw(5) << left << "Hold"<< setw(6) << left << "IsPos" << setw(4) << "Pos"
      << setw(6) << left << "IsOut"<< setw(6) << "Trunk" << setw(6) << left << "Type"  << setw(6) << "GUI" <<  setw(7) << "Line" << setw(20) << left << "Hangup Cause"<< setw(7) 
      << "Barge" << left << setw(20) << left << "IP Address" << left << setw(72)  << "Recording" << endl;
 } 
 cout << setw(7)  << left << " ";   
 cout << setw(4)  << left << boolConnected;
 cout << setw(5)  << left << boolOnHold;
 cout << setw(6)  << left << boolIsPosition;
 cout << setw(4)  << left << iPositionNumber;
 cout << setw(6)  << left << boolIsOutbound;
 cout << setw(6)  << left << iTrunkNumber;
 cout << setw(6)  << left << intTrunkType;
 cout << setw(6)  << left << iGUIlineView;
 cout << setw(7)  << left << iLineNumber;
 cout << setw(20) << left << strHangupCause;
 cout << setw(7)  << left << iSLABarge;
 cout << setw(20) << left << IPaddress.stringAddress; 
 cout << setw(72) << left << strCallRecording << endl;


 if (true) { 
  cout << setw(7) << left << " " << setw(35) << left << "Presence ID" << left << setw(5) << "Park" << left << setw(8) << "Lot" << left << setw(6) << "ring-P" <<endl;
 }
 cout << setw(7)  << left << " ";
 cout << setw(35) << left << strPresenceID;   
 cout << setw(5)  << left << boolParked;
 cout << setw(8)  << left << strValetExtension;
 cout << setw(6)  << left << iRingingPosition;
 cout << endl << endl;
 
}

void Channel_Data::fLoadLinkDataIntoChannelData(Link_Data oblLinkData, int iLinkNum, bool boolCallerIdsReversed)
{
 string                   		strlinknum;
 extern Telephone_Devices           	TelephoneEquipment;
 extern bool 				bool_BCF_ENCODED_IP;

// oblLinkData.fDisplay();
 strlinknum = int2str(iLinkNum);
 
 switch (iLinkNum)
  {
   case 1:
          this->strChannelName 	= oblLinkData.strChannelone;
          this->strChannelID	= oblLinkData.strChanneloneID;
	  this->IPaddress	= oblLinkData.IPaddressOne;
          if (boolCallerIdsReversed){
           this->strCustName	= oblLinkData.strCustNameTwo;
           this->strCallerID	= oblLinkData.strCallerIDtwo;
          }
          else{
           this->strCustName	= oblLinkData.strCustNameOne;          
           this->strCallerID	= oblLinkData.strCallerIDone;
          } 
          break;  
   case 2:
          this->strChannelName 	= oblLinkData.strChanneltwo;
          this->strChannelID	= oblLinkData.strChanneltwoID;
	  this->IPaddress	= oblLinkData.IPaddressTwo;
          if (boolCallerIdsReversed){
           this->strCustName	= oblLinkData.strCustNameOne;
           this->strCallerID	= oblLinkData.strCallerIDone;
          }
          else{
           this->strCustName	= oblLinkData.strCustNameTwo;          
           this->strCallerID	= oblLinkData.strCallerIDtwo;
          } 
          break;  

   default:
         SendCodingError( "channel_data_function.cpp -> fn fLoadLinkDataIntoChannelData() invalid link# "+ strlinknum); 
         return;
  }

  if (bool_BCF_ENCODED_IP) {
   this->iPositionNumber = TelephoneEquipment.fPositionNumberFromChannelName(this->strChannelName);
  }
  else {
   this->iPositionNumber = TelephoneEquipment.fPostionNumberFromIPaddress(this->IPaddress.stringAddress);
  }

  this->boolIsPosition  = (this->iPositionNumber > 0);
  if (this->boolIsPosition){
   this->fLoadPositionCallID(this->iPositionNumber);
   this->fLoadConferenceDisplayP(this->iPositionNumber);
   this->fLoadCustomerName(this->iPositionNumber);
   this->fLoadGUILineView(this->strChannelName);
   this->fLoadLineNumberFromChannel();
 //  this->fSetSharedLineFromLineNumber();   
  }
  else {
       //TBD cannot figure what to put here ...
  
  }
  this->strHangupCause.clear();
  this->boolConnected    = true;
  this->boolOnHold       = false;
  this->iTrunkNumber     = 0;
  this->iSLABarge        = 0;
  this->strCallRecording    = "";
  this->strPresenceID     = "";
  this->boolMSRP          = false;
  this->boolParked        = false;
  this->strValetExtension.clear();
  
}


void Channel_Data::fLoadMSRPPositionChannel(int i)
{
 extern Telephone_Devices                TelephoneEquipment;

 this->fClear();
 this->iPositionNumber = i;
 this->fLoadPositionCallID(this->iPositionNumber);
 this->fLoadConferenceDisplayP(this->iPositionNumber);
 this->fLoadCustomerName(this->iPositionNumber);


 this->boolConnected    = true;
 this->boolOnHold       = false;
 this->boolMSRP         = true;
 this->IPaddress.stringAddress = TelephoneEquipment.fIPaddressFromPositionNumber(i);
 this->boolIsPosition = true;
 this->strChannelName = "position_" + int2strLZ(i);
 this->strChannelID = int2str(i);
 this->boolParked        = false;
 this->strValetExtension.clear();
}

void Channel_Data::fSetPositionDataIntoChannelData(int i) {

 extern Telephone_Devices       TelephoneEquipment;

  this->strCallerID = TelephoneEquipment.fCallerIDnameofTelephonewithPosition(i);
  this->strCustName = TelephoneEquipment.fCallerIDnumberofTelephonewithPosition(i);
  this->IPaddress.stringAddress = TelephoneEquipment.fIPaddressFromPositionNumber(i);

 return;
}


int Channel_Data::fLoadGUILineView(string strInput)
{
 extern Telephone_Devices                TelephoneEquipment;

 vector<string>::size_type      sz = TelephoneEquipment.GUILineView.size();

 string strMatch;

 //get username if not already done.
 strMatch = FreeswitchUsernameFromChannel(strInput);

 if (strMatch.empty()) {strMatch = strInput;}

  // check list first for match ...
 if(!TelephoneEquipment.GUILineView.empty())
  {
   for (unsigned int i = 0; i< sz; i++)
    {
     if (strMatch == TelephoneEquipment.GUILineView[i].Name)
      { iGUIlineView = i; return i;}
    }// end for (unsigned int i = 0; i< sz; i++)
  }// end if(!TelephoneEquipment.GUILineView.empty())


return 0;
}




void Channel_Data::fLoadChannelDatafromTransferData(Transfer_Data objTransferData)
{
 strChannelName    = objTransferData.strTransferFromChannel;
 strChannelID      = objTransferData.strTransferFromUniqueid;
 iGUIlineView      = 0;
 iLineNumber       = 0;
 strCallerID       = objTransferData.strTransfertoNumber;
 strConfDisplay    = objTransferData.strConfDisplay;
 strCustName       = objTransferData.strTransferName;
 boolConnected     = false;
 boolOnHold        = false;
 intTrunkType      = 0;
 boolIsPosition    = false;
 boolIsOutbound    = false;
 iPositionNumber   = 0;
 strHangupCause.clear();
 iTrunkNumber      = 0;
 IPaddress         = objTransferData.IPaddress;   
 iSLABarge         = 0;
 strCallRecording  = "";
 strPresenceID     = "";
}
void Channel_Data::fLoadChannelDatafromTransferData(Transfer_Data objTransferData, int iPosition)
{
 strChannelName    = objTransferData.strTransferFromChannel;
 strChannelID      = objTransferData.strTransferFromUniqueid;
 iPositionNumber   = iPosition;
 iLineNumber       = fLoadLineNumberFromChannel(strChannelName, iPositionNumber);
 fLoadGUILineView(strChannelName);
 fLoadPositionCallID(iPositionNumber);
 fLoadConferenceDisplayP(iPositionNumber);
 fLoadCustomerName(iPositionNumber);
 boolConnected    = true;
 boolOnHold       = false;
 intTrunkType     = 0;
 boolIsPosition   = true;
 boolIsOutbound   = false;
 strHangupCause.clear();
 iTrunkNumber     = 0;
 IPaddress        = objTransferData.IPaddress;   
 iSLABarge        = 0;
 strCallRecording    = "";
 strPresenceID     = "";
 boolMSRP          = false;
}

int Channel_Data::fLoadLineNumberFromChannel(string strChannel,  int iPosition)
{
 int                                     index;
 extern Telephone_Devices                TelephoneEquipment;

 index = TelephoneEquipment.fIndexWithPositionNumber(iPosition);
 if (index < 0) {return -1;}

 iLineNumber = TelephoneEquipment.Devices[index].fLineNumberWithRegistrationName(FreeswitchUsernameFromChannel(strChannel));

 return iLineNumber;
}

int Channel_Data::fLoadLineNumberFromChannel()
{
 int                                     index;
 extern Telephone_Devices                TelephoneEquipment;

 index = TelephoneEquipment.fIndexWithPositionNumber(iPositionNumber);
 if (index < 0) {return -1;}

 iLineNumber = TelephoneEquipment.Devices[index].fLineNumberWithRegistrationName(FreeswitchUsernameFromChannel(strChannelName));
 return iLineNumber;
}




int Channel_Data::fLoadLineNumberFromUserName(string strUsername)
{
 int                                     index;
 extern Telephone_Devices                TelephoneEquipment;

 index = TelephoneEquipment.fIndexWithPositionNumber(iPositionNumber);
 if (index < 0) {return -1;}

 iLineNumber = TelephoneEquipment.Devices[index].fLineNumberWithRegistrationName(strUsername);
 return iLineNumber;
}


int Channel_Data::fLoadPositionNumber(string strInput)
{
 size_t found;

  found = strInput.find_first_not_of("0123456789");
  if (found != string::npos)
   {
    iPositionNumber = 0;
    boolIsPosition = false;
   } 
  else
   {
    iPositionNumber = char2int(strInput.c_str());
    if (iPositionNumber > 0) {boolIsPosition = true;}
    else                     {boolIsPosition = false;}
   }


  return  iPositionNumber;
}

int Channel_Data::fLoadPositionNumber(int iPosNumber)
{
 iPositionNumber = iPosNumber;
 if (iPositionNumber > 0) {boolIsPosition = true;}
 else                     {boolIsPosition = false;}

 return  iPositionNumber;
}



void Channel_Data::fLoadConferenceDisplayC(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_CALLER_PREFIX ;
 if (i) {strConfDisplay += int2str(i);}
}

void Channel_Data::fLoadConferenceDisplayD(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_DEST_PREFIX;
 if (i) {strConfDisplay += int2str(i);}
}

void Channel_Data::fLoadConferenceDisplayT(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_DEST_PREFIX;
 if (i) {strConfDisplay += int2str(i);}
}

void Channel_Data::fLoadConferenceDisplayP(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_POSITION_PREFIX;
 if (i) {strConfDisplay += int2str(i);}
}

/*
void Channel_Data::fLoadConferenceDisplayP()
{
 strConfDisplay = ANI_CONF_MEMBER_POSITION_PREFIX;
 strConfDisplay += StripLeadingZeroes(AsteriskPositionFromChannel(strChannelName));
}
*/
void Channel_Data::fLoadConferenceDisplayX(int i)
{
 strConfDisplay = ANI_CONF_MEMBER_PBX_TRANSFER_PREFIX;
 if (i) {strConfDisplay += int2str(i);}
}





/*
void Channel_Data::fLoadDestCallID(string strInput)
{
 if (strInput == "s") {strCallerID = "UNKNOWN";}
 else                 {strCallerID = strInput;}
}
*/
void Channel_Data::fLoadChannelName(string strChannel)
{
  strChannelName = strChannel;
}

void Channel_Data::fLoadChannelID(string strID)
{
  strChannelID = strID;
}

void Channel_Data::fLoadTransferData(string strID)
{
 extern Telephone_Devices       TelephoneEquipment;
  strTranData = TelephoneEquipment.fRemoveRoutingPrefix(strID);
}

void Channel_Data::fLoadHangupCause(string strInput)
{
  strHangupCause = strInput;
}

void Channel_Data::fLoadCustomerName(int iPosition)
{
 extern Telephone_Devices                TelephoneEquipment;
 strCustName = TelephoneEquipment.fCallerIDnameofTelephonewithPosition(iPosition);
 return;
}

void Channel_Data::fLoadPositionCallID(int iPosition)
{
 extern Telephone_Devices                TelephoneEquipment;
 strCallerID = TelephoneEquipment.fCallerIDnumberofTelephonewithPosition(iPosition);
 return;
}

bool Channel_Data::fLoadTrunkNumber(int iTrunk)
{
 size_t 	found;
 MessageClass	objMessage;
 string         stringWarning = "SOFTWARE LOAD";

 if (!Validate_Trunk_Number(iTrunk))
  {
   objMessage.fMessage_Create(LOG_WARNING, 813, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              KRN_MESSAGE_129b, int2str(iTrunk), stringWarning);
   enQueue_Message(AMI,objMessage);
   iTrunkNumber = 0;
   return false;
  }
 iTrunkNumber = iTrunk;

//load Trunk Type ...
 intTrunkType = int (TRUNK_TYPE_MAP.Trunk[iTrunkNumber].TrunkType);
 return true;
}

bool Channel_Data::fLoadTrunkNumber(string strInput, string stringRawData)
{
 size_t 	found;
 MessageClass	objMessage;

 if (strInput.empty()) 
  {
   objMessage.fMessage_Create(LOG_WARNING, 812, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_129a, "NO TRUNK DATA FOUND",
                              ASCII_String(stringRawData.c_str(), stringRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(AMI,objMessage);   
   return false;
  }

 found = strInput.find_first_not_of("0123456789");
 if (found != string::npos)
  {
   objMessage.fMessage_Create(LOG_WARNING, 812, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_129a, strInput,
                              ASCII_String(stringRawData.c_str(), stringRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(AMI,objMessage);
   return false;
  }
 iTrunkNumber = char2int(strInput.c_str());


 if (!Validate_Trunk_Number(iTrunkNumber))
  {
   objMessage.fMessage_Create(LOG_WARNING, 813, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_AMI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_129b, strInput,
                              ASCII_String(stringRawData.c_str(), stringRawData.length()),"","","","", NORMAL_MSG, NEXT_LINE);
   enQueue_Message(AMI,objMessage);
   iTrunkNumber = 0;
   return false;
  }

//load Trunk Type ...
 intTrunkType = int (TRUNK_TYPE_MAP.Trunk[iTrunkNumber].TrunkType);
 return true;
}


