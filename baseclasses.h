/*****************************************************************************
* FILE: baseclasses.h
*  
*
* DESCRIPTION:
*  Contains the base definitions of all classes declared in the controller program. 
*
*
*
* AUTHOR: 02/25/2007 Bob McCarthy
* LAST REVISED: 
* 1/5/2020 
******************************************************************************/

#include "header.h"
#include "globals.h"
#include "/datadisk1/src/simpleini/SimpleIni.h"          // ini software
#include "/datadisk1/src/xmlParser/xmlParser.h"


#ifndef classses_included_
#define classses_included_

#define DEBUG_MSG_900           "[DEBUG] %%% - %%% %%% Code %%%" 
#include "adr.h"

       
using namespace std;



//forward external function declaration
extern void             	Semaphore_Error 	   (int intReturnCode, threadorPorttype enumThreadCalling, sem_t *sem_tSempointer, string stringMessage, int intNumFails);
extern string           	Thread_Calling  	   (threadorPorttype enumThreadCalling);
extern string 	        	int2strLZ       	   (unsigned long long int intArg);         
extern string           	ASCII_String    	    (const char* charInput, size_t Length);
extern string           	ReplaceNullCharacters	(const char* charString, size_t length);
extern string           	HEX_String      	    (unsigned char* chInput, size_t sLength);
extern string           	AMITestScript   	    (string strInput);
extern string			FindandReplaceALL     	(string stringArg, string stringFind, string stringReplace);
extern void             	SendCodingError         (string strInput);
extern bool             	IsCLIregistrationPacket (string strInput);
extern bool             	IsCLIContentTypePacket  (string strInput);
extern unsigned long long int 	char2int		(const char* CharArg);
extern string                   fCheck_Delimiter        ( string stringData, bool &boolDelimeterFound, char Delimiter1 = '\0' , char Delimiter2 = '\0', char Delimiter3 = '\0');
extern string                   findWindowsUser         (string strInput);
extern string                   UUID_Generate_String    ();
//extern int              Test_return_position_from_raw_xml(const char* charString); 

// forward Class declaration

class XMLNode;
class ExperientDataClass;
class ExperientCommPort;
class Port_Data;
class Call_Data;
class Text_Data;
class ExperientTCPPort;
class Transfer_Data;
class DataPacketIn;
class Link_Data;
class ALISteering;
class Position_Alias;
class Position_Aliases;
class PORT_INIT_VARS;
class ADR_Data;
class ADR_REQUIREMENTS;
class PSAP_STATUS;
class Conf_Data;

/* 
 * This typedef define in/out paramters object for thread arguments
 */
typedef struct
{
   int  intPassedIn;   // input parameter
   int* intPassedOut;  // output parameter
} param_t;

typedef struct
{
   int         intlength;
   const char* chardata;  
} param_c;


struct MY_RESOURCES {
int 	fd;	// Hold file descriptor from inotify_init
int 	wd;	// Hold watch descriptor on selected folder from inotify_add_watch
};

class Memory_Data {
public:
    unsigned long long int   Freeswitch_OS_Memory_Total;
    unsigned long long int   Freeswitch_OS_Memory_Available;
    float                    Freeswitch_OS_Memory_Usage;
    unsigned long long int   Freeswitch_OS_Swap_Total;
    unsigned long long int   Freeswitch_OS_Swap_Free;
    float                    Freeswitch_OS_Swap_Usage;
    float                    Freeswitch_APP_CPU_Usage;
    float                    Freeswitch_APP_MEM_Usage;
    unsigned long long int   Freeswitch_PID;
    volatile bool            NewFreeswitchDataRX; 
    unsigned long long int   OS_Memory_Total;
    unsigned long long int   OS_Memory_Available;
    float                    OS_Memory_Usage;
    unsigned long long int   OS_Swap_Total;
    unsigned long long int   OS_Swap_Free;
    float                    OS_Swap_Usage;
    float                    Controller_APP_CPU_Usage;
    float                    Controller_APP_MEM_Usage;
    
 Memory_Data(); 
~Memory_Data(){}
  
 void fSendFSCLIrequest(); 
 bool fLoadControllerCPUusage(string stringCPU);
 bool fLoadControllerMEMusage(string strMEM);
 bool fLoadOSmemoryUsage(string strTotal, string strAvail);
 bool fLoadOSswapUsage(string strSwapTotal, string strSwapFree);
 bool fLoadFreeswitchMemoryData(string strData);
 void fDisplay();
 void fClear();   
};

class Position_Alias {
 public:
 int iPosition;
 int iAlias;

 Position_Alias() {
  iPosition = 0;
  iAlias = 0;
 }
 
 //assignment operator
 Position_Alias& operator= (const Position_Alias& b);

 //copy constructor
 Position_Alias(const Position_Alias *a);

 // equal operator
 bool operator == (const Position_Alias& a) const { return fCompare(a);} 
 ~Position_Alias(){}
 
 bool fCompare(const Position_Alias& a) const;
 bool fLoadPositionAlias(string strData);
 void fClear();
};

class Position_Aliases {
 public:
 vector <Position_Alias> vPositionAliases;


 Position_Aliases() {
  vPositionAliases.clear();
 }
 
 //assignment operator
 Position_Aliases& operator= (const Position_Aliases& b);

 //copy constructor
 Position_Aliases(const Position_Aliases *a);
 
 // equal operator
 bool operator == (const Position_Aliases& a) const { return fCompare(a);} 
  
 ~Position_Aliases(){}
 
 bool   fCompare(const Position_Aliases& a) const;
 bool   fLoadPositionAliases(string strData);
 string fAliasOfPosition(int i);
 string CADmessageWithAlias(string strData);
 string fDisplay();
 void   fClear();
};

class PSAP_STATUS {
 public:

 string strPSAPname;
 int    iPositionsOnline;
 int    iPositionsOnEmergencyCall;
 int    iPositionsOnAdminCall;
 int    i911Calls;
 int    i911CallsOnHold;
 int    i911CallsAbandoned;
 int    i911CallsParked;
 int    i911CallsRinging;
 int    iAdminCalls;
 int	iOutboundAdminCalls;
 int	iInboundAdminCalls;
 int    iAdminCallsOnHold;
 int    iAdminCallsAbandoned;
 int    iAdminCallsParked;
 int    iAdminCallsRinging;
 int    iMSRPCalls;
 int    iMSRPCallsOnHold;
 int    iMSRPCallsAbandoned;
 int    iMSRPCallsRinging;

 PSAP_STATUS();
~PSAP_STATUS(){}

 bool fCompare(const PSAP_STATUS& a) const;
 void fClear();
 void fClearCounters();
};


class Workstation_Group {
 public:
 string       		strGroupName;
 string             strPSAP_ID;
 vector <int> 		WorkstationsInGroup;
 vector <int> 		TrunksinGroup;
 bool         		bALLOW_MANUAL_ALI_BIDS;
 bool         		bSHOW_GOOGLE_MAP;
 bool         		bCONTACT_RELAY_INSTALLED;
 bool         		bSHOW_CALLBACK_BUTTON;
 bool         		bSHOW_SPEED_DIAL_BUTTON;
 bool         		bSHOW_DIAL_PAD_BUTTON;
 bool         		bSHOW_ANSWER_BUTTON;
 bool         		bSHOW_BARGE_BUTTON;
 bool         		bSHOW_HOLD_BUTTON;
 bool         		bSHOW_HANGUP_BUTTON;
 bool         		bSHOW_MUTE_BUTTON;
 bool         		bSHOW_VOLUME_BUTTON;
 bool         		bSHOW_ATTENDED_XFER_BUTTON;
 bool         		bTDD_AUTO_DETECT;
 bool               bSHOW_RAPID_LITE_BUTTON;
 string       		strTDD_DEFAULT_LANGUAGE;
 int          		iTDD_SEND_DELAY;
 string       		strTDD_SEND_TEXT_COLOR;
 string       		strTDD_RECEIVED_TEXT_COLOR;
 int          		iTDD_CPS_THRESHOLD;
 string       		strDISPATCHER_SAYS_PREFIX_EXPORT;
 string       		strCALLER_SAYS_EXPORT;
 string             strPSAPURI;
 vector <string> 	vTDD_MESSAGE_LIST;
 XMLNode            PhoneBookNode;
 XMLNode            SharedLineNode;
 PSAP_STATUS        PSAPStatus;

 //constructor
 Workstation_Group();
 
 //assignment operator
 Workstation_Group& operator= (const Workstation_Group& b);

 //copy constructor
 Workstation_Group(const Workstation_Group *a);
 
 // equal operator
 bool operator == (const Workstation_Group& a) const { return fCompare(a);} 
  
  // destructor
 ~Workstation_Group(){}
 
 
 
 
 bool fCompare(const Workstation_Group& a) const;
 bool TrunkInGroup( int iTrunk);
 bool WorkstationInGroup( int iPosition);
 bool fLoadTrunks( string stringArg);
 bool fLoadWorkstations( string stringArg);
 bool fLoadFromGlobalVARS();
 bool CheckAbandonedTrunkList(string strInput);
 bool HasThisURI(string strURI);
 bool AnyPositionInGroupActive(Conf_Data objConfData);
 bool AnyPositionInGroupHistory(Conf_Data objConfData);
 void fClear();
};


class Thread_Data
{
 public:
 string    strThreadName;
 pid_t     ThreadPID;
 pthread_t pth_SelfID;
 pid_t     ThreadID;
 string    strThread_UUID;

 Thread_Data()  {
   strThreadName.clear();
   ThreadPID      = getpid();
   pth_SelfID     = pthread_self();
   ThreadID       = syscall(SYS_gettid);
   strThread_UUID = UUID_Generate_String();
 }
 
~ Thread_Data(){} 

};




class PhoneBookEntry
{
 public:
 string strCallerID;
 string strCustName;
 string strTransferGroup;
 string strTransferCode;
 string strSpeedDialAction;
 string strIconFileName;
 sip_ng911_transfer_type ng911TransferType;

 PhoneBookEntry();

 ~ PhoneBookEntry(){}

 void fClear();
 void fDisplay(int i, bool boolshowHeaders = false);
};

class GUI_Line_View
{
 public:
 string Name;
 string Label;

 GUI_Line_View()
  {
   Name  ="not used";
   Label ="";
  }
 ~ GUI_Line_View(){}

 void fClear();
 void fDisplay(int i);
};

class ALI_E2_XML_Data
{
 public:
 string  strClassofService, strTypeofService; 
 string  strCellId, strCompanyID1, strCountyID, strCustomerName, strEmergencyMedicalServiceProvider, strEmergencyServicesNumber;
 string  strFireDepartmentServiceProvider, strHouseNumber, strHouseNumberSuffix, strLawEnforcementServiceProvider, strLocation;
 string  strMSAGCommunity, strPostDirectional, strPrefixDirectional, strSectorID, strState, strStreetname, strStreetNameSuffix;
 // constructor
 ALI_E2_XML_Data();

~ ALI_E2_XML_Data(){}

 ALI_E2_XML_Data& operator= (const ALI_E2_XML_Data& b);
 
 
string fClassofService();
bool   fLoadClassofService(string strInput);
bool   fLoadLocationData(const char* chXMLstring, Port_Data objPortData);
void   fClear();
void   fDisplay();
};


class ALI_E2_Digits
{
 public:
  unsigned char      chTypeofDigits;  
  unsigned char      chNatureofNumber; 
  unsigned char      chNumberingPlanandEncoding;
  unsigned char      chNumberofDigits;
  unsigned char      chDigitTwoandOne;
  unsigned char      chDigitFourandThree;
  unsigned char      chDigitSixandFive;
  unsigned char      chDigitEightandSeven;
  unsigned char      chDigitTenandNine;
  unsigned char      chDigitTwelveandEleven;
  unsigned char      chDigitFourteenandThirteen;
  unsigned char      chDigitFillerandFifteen;

 // constructor
 ALI_E2_Digits();
 
~ ALI_E2_Digits(){}

 ALI_E2_Digits& operator= (const ALI_E2_Digits& b);
 
 void fClear();
 bool fLoadTypeofDigits(unsigned char ch);
 bool fLoadNatureofNumber(unsigned char ch);
 bool fLoadNumberingPlanandEncoding(unsigned char ch);
 bool fLoadNumberofDigits(unsigned char ch, int intDigits);
 bool fLoadDigits(unsigned char ch, int intPair);
 string AreaCode();
 string NPA();
 string LastFour();
};



class ALI_E2_Data
{
 public:
  bool               boolValidRecord;
  unsigned char      chTransactionId[E2_TRANSACTIONID_LENGTH];
  unsigned char      chComponentType;     
  unsigned char      chComponentId;     
  unsigned char      chOperationFamily;     
  unsigned char      chOperationSpecifier;     
  unsigned char      chErrorCode;     
  unsigned char      chProblemType;     
  unsigned char      chProblemSpecifier;     
  unsigned char      chPositionRequestType;
  unsigned char      chPositionResult;
  unsigned char      chPositionInformationParameters;
  unsigned char      chPositionInformationLength;
  unsigned char      chPositionYear;
  unsigned char      chPositionMonth;
  unsigned char      chPositionDay;
  unsigned char      chPositionTimeofDay[3];
  unsigned char      chYear;
  unsigned char      chMonth;
  unsigned char      chDay;
  unsigned char      chTimeofDay[3];
  unsigned char      chPositionGeoLength;
  unsigned char      chPositionGeoLPRI;
  unsigned char      chPositionGeoScreening;
  unsigned char      chPositionGeoExt;
  unsigned char      chPositionGeoTypeofShape;
  unsigned char      chPositionGeoLatSign;
  unsigned char      chPositionGeoLatDegrees[3];
  unsigned char      chPositionGeoLongDegrees[3];
  unsigned char      chPositionUncertaintyCode;
  unsigned char      chPositionConfidence;
  unsigned char      chPositionAltitudeSign;
  unsigned char      chPositionAltitude[2];
  unsigned char      chPositionAltitudeUncertaintyCode;  
  unsigned char      chPositionSource;  
  ALI_E2_Digits      CallbackNumber;
  ALI_E2_Digits      EmergencyServicesRoutingDigits;  
  ALI_E2_Digits      MobileIdentificationNumber;  
  ALI_E2_Digits      InternationalMobileSubcriberIdentity;  


  unsigned char      chMobileCallStatus;
  unsigned char      chCompanyIdString[E2_MAX_COMPANY_ID_STRING_LENGTH];
  unsigned char      chCompanyIdStringLength; 
  unsigned char      chLocationDescription[E2_MAX_LOCATION_DESCRIPTION_LENGTH +1];
  int                intLocationDescriptionLength; 


  ALI_E2_Data();

  ~ALI_E2_Data(){}

  ALI_E2_Data& operator= (const ALI_E2_Data& b);


 bool                fLoadMobileCallStatus(unsigned char ch);
 unsigned long int   fTransactionID();
 bool                fIsACK();
 bool                fIsNAK();
 void                fClear();
 string              TimeStamp(bool Position = false);
 string              Latitude();
 string              Longitude();
 string              Uncertainty();
};



class Window_and_Button_Data
{
 public:
 bool boolTransferWindow;
 bool boolALIRebidButton;
 bool boolCADSendButton;
 bool boolRapidLiteButton;

 Window_and_Button_Data()
  {
   boolTransferWindow  = false;
   boolALIRebidButton  = false;
   boolCADSendButton   = false;
   boolRapidLiteButton = false;
  }
 ~ Window_and_Button_Data(){}

 Window_and_Button_Data& operator= (const Window_and_Button_Data& b)
  {
   if (&b == this ) {return *this;}
   boolTransferWindow  = b.boolTransferWindow;
   boolALIRebidButton  = b.boolALIRebidButton;
   boolCADSendButton   = b.boolALIRebidButton;
   boolRapidLiteButton = b.boolRapidLiteButton;
   return *this;
  }
 void fClear();
 void fDisplay();
 void fSetButtonsForALIbid();
 void fSetButtonsForALIReceived();
 void fSetButtonsForALItimeout(bool bALIreceived);
 void fSetButtonsForAbandoned();
 void fSetButtonsForManualALIReceived();
 void fSetButtonsForManualALITimeout();
 void fSetRapidLiteButton(bool boolValidCallback);
 void fSetNoALICadBid();
};

class LocationURI
{
 public:
 string strURI;
 bool   boolDataRecieved;
 string strExpires;

 LocationURI()
  {
   strURI.clear();
   boolDataRecieved = false;
   strExpires.clear();
  }
~LocationURI(){}

 //copy constructor
 LocationURI(const LocationURI *b);
 //assignment operator
 LocationURI& operator= (const LocationURI& b); 

 void   fClear();
 bool   fLoadLocationURI(string strInput);
 bool   fDataRecieved();
};


class SERVICE_URN {
 public:

 string strURN;
 int    iBid;
 string strPurpose;
 bool   boolServiceURIreceivedFlag;
 string strServiceURI;
 string strServiceNumber;
 string strDisplayName;
 string strDataRX;

 SERVICE_URN();
~SERVICE_URN(){}
 //copy constructor
 SERVICE_URN(const SERVICE_URN *b);
 //assignment operator
 SERVICE_URN& operator= (const SERVICE_URN& b);

 void   fClear();
 bool   fDetermineURI();
};


class SERVICELIST {
 public:
 
 string               ECRF_Server; 
 vector <SERVICE_URN> vServiceURN;
 bool                 boolBidMade;
 bool                 boolListBuilt;
 sem_t                sem_tMutexVServiceURN;
 SERVICELIST() {
  sem_init(&sem_tMutexVServiceURN,0,1);
  ECRF_Server.clear();
  vServiceURN.clear();
  boolBidMade = false;
  boolListBuilt = false;
 }
~SERVICELIST(){
  sem_destroy(&sem_tMutexVServiceURN);
 }
 //copy constructor
 SERVICELIST(const SERVICELIST *b);
 //assignment operator
 SERVICELIST& operator= (const SERVICELIST& b);

 void   fClear();
 bool   fAll_URIS_RX();
 int    ServiceBidIndex(int i);
 int    PoliceIndex();
 int    FireIndex();
 int    EMSIndex();
 void   fSetBid();
 int    fRemoveIndexWithBid(int i);
};


class ALI_I3_StreetAddress
{
 public:
 string  strHouseNum, strHouseNumSuffix, strPrefixDirectional, strStreetName, strStreetSuffix, strPostDirectional, strTextualAddress, strMSAGCommunity, strPostalCommunity;
 string  strStateProvince, strCountyID, strCountry, strTARCode, strPostalZipCode, strBuilding, strFloor, strUnitNum, strUnitType, strLocationDescription, strLandmarkAddress;

 ALI_I3_StreetAddress();
~ALI_I3_StreetAddress(){}
 // Declare Copy Constructor
 ALI_I3_StreetAddress(const ALI_I3_StreetAddress *a);

 // Declare Assignment operator
 ALI_I3_StreetAddress& operator=(const ALI_I3_StreetAddress& a);


void fLegacyHouseNum();
void fLegacyHouseNumSuffix();
void fLegacyPrefixDirectional();
void fLegacyStreetName();
void fLegacyStreetSuffix();
void fLegacyPostDirectional();
void fLegacyMSAGCommunity();
void fLegacyPostalCommunity();
void fLegacyStateProvince();
void fLegacyCountyID();
void fLegacyCountry();
void fLegacyFloor();


bool fLoadStreetAddress(XMLNode objXML);
void fDisplay();
void fClear();
};

class ALI_I3_CallInfo
{
 public:
  string  strCallbackNum, strCallingPartyNum; 
  string  strClassOfService , strClassOfServiceCode, strTypeOfService , strTypeOfServiceCode , strSourceOfService;
  string  strMainTelNum, strCustomerName, strCustomerCode, strAttentionIndicator , strAttentionIndicatorCode;
  string  strSpecialMessage, strAlsoRingsAtAddress;

 ALI_I3_CallInfo();
~ALI_I3_CallInfo(){}

 // Declare Copy Constructor
 ALI_I3_CallInfo(const ALI_I3_CallInfo *a);

 // Declare Assignment operator
 ALI_I3_CallInfo& operator=(const ALI_I3_CallInfo& a);


bool fLoadCallInfo(XMLNode objXML);
int  fLoadTypeofService(string strInput);
void fClear();
};

class ALI_I3_GeoLocation
{
 public:
  string  strLatitude, strLongitude; 
  string  strElevation , strDatum , strHeading, strSpeed , strPositionSource , strPositionSourceCode;
  string  strUncertainty, strConfidence, strDateStamp, strLocationDescription ;

 ALI_I3_GeoLocation();
~ALI_I3_GeoLocation(){}
 //Copy Constructor
 ALI_I3_GeoLocation(const ALI_I3_GeoLocation *a);

 // Declare Assignment operator
 ALI_I3_GeoLocation& operator=(const ALI_I3_GeoLocation& a);

 string Latitude();
 string Longitude();
 bool   fLoadGeoLocation(XMLNode objXML);
 void   fClear();
};

class ALI_I3_CellSite
{
 public:
  string  strCellID, strSectorID, strLocationDescription; 

 ALI_I3_CellSite();
~ALI_I3_CellSite(){}

 //Copy Constructor
 ALI_I3_CellSite(const ALI_I3_CellSite *a);

 // Declare Assignment operator
 ALI_I3_CellSite& operator=(const ALI_I3_CellSite& a);

 bool   fLoadCellSite(XMLNode objXML);
 void   fClear();
};


class ALI_I3_LocationInfo
{
 public:
  ALI_I3_StreetAddress	StreetAddress; 
  ALI_I3_GeoLocation	GeoLocation;
  ALI_I3_CellSite	CellSite;
  string                strComment;
  string                strMethod;
  
 ALI_I3_LocationInfo();
~ALI_I3_LocationInfo(){}

 //Copy Constructor
 ALI_I3_LocationInfo(const ALI_I3_LocationInfo *a);

 // Declare Assignment operator
 ALI_I3_LocationInfo& operator=(const ALI_I3_LocationInfo& a);

 bool fLoadLocationInfo(XMLNode  objXML);
 void fClear();
};

class ALI_I3_Police
{
 public:
  string strName, strTN;
  
 ALI_I3_Police();
~ALI_I3_Police(){}

 //Copy Constructor
 ALI_I3_Police(const ALI_I3_Police *a);

 // Declare Assignment operator
 ALI_I3_Police& operator=(const ALI_I3_Police& a);


 bool fLoadPolice(XMLNode  objXML);
 bool fTNnotZero();
 void fClear();
};

class ALI_I3_Fire
{
 public:
  string strName, strTN;
 
 ALI_I3_Fire();
~ALI_I3_Fire(){}

 //Copy Constructor
 ALI_I3_Fire(const ALI_I3_Fire *a);

 // Declare Assignment operator
 ALI_I3_Fire& operator=(const ALI_I3_Fire& a);

 bool fLoadFire(XMLNode  objXML);
 bool fTNnotZero();
 void fClear();
};

class ALI_I3_EMS
{
 public:
  string strName, strTN;
 
 ALI_I3_EMS();
~ALI_I3_EMS(){}

 //Copy Constructor
 ALI_I3_EMS(const ALI_I3_EMS *a);

 // Declare Assignment operator
 ALI_I3_EMS& operator=(const ALI_I3_EMS& a);

 bool fLoadEMS(XMLNode  objXML);
 bool fTNnotZero();
 void fClear();
};

class ALI_I3_Agency
{
 public:
  string strKind, strName, strTN;

 ALI_I3_Agency();
~ALI_I3_Agency(){}

 //Copy Constructor
 ALI_I3_Agency(const ALI_I3_Agency *a);

 // Declare Assignment operator
 ALI_I3_Agency& operator=(const ALI_I3_Agency& a);


 bool fLoadAgency(XMLNode  objXML);
 bool fTNnotZero();
 void fClear();
};

class ALI_I3_Agencies
{
 public:
  ALI_I3_Police			Police;
  ALI_I3_Fire			Fire;
  ALI_I3_EMS  			EMS;
  vector <ALI_I3_Agency> 	vOtherAgencies;
  string        		strAdditionalInfo;
  string			strESN;

 ALI_I3_Agencies();
~ALI_I3_Agencies(){}

//Copy Constructor
 ALI_I3_Agencies(const ALI_I3_Agencies *a);

 // Declare Assignment operator
 ALI_I3_Agencies& operator=(const ALI_I3_Agencies& a);

 bool fLoadAgencies(XMLNode  objXML);
 bool fLoadOtherAgencies(XMLNode  objXML);
 void fClear();
};

class ALI_I3_DataProvider
{
 public:
  string strDataProviderID, strName, strTN, strTelURI;

 ALI_I3_DataProvider();
~ALI_I3_DataProvider(){}

//Copy Constructor
 ALI_I3_DataProvider(const ALI_I3_DataProvider *a);

 // Declare Assignment operator
 ALI_I3_DataProvider& operator=(const ALI_I3_DataProvider& a);

 bool fLoadDataProvider(XMLNode  objXML);
 void fConvertTelURItoTN();
 void fClear();
};

class ALI_I3_AccessProvider
{
 public:
  string strAccessProviderID, strName, strTN, strTelURI;

 ALI_I3_AccessProvider();
~ALI_I3_AccessProvider(){}

//Copy Constructor
 ALI_I3_AccessProvider(const ALI_I3_AccessProvider *a);

 // Declare Assignment operator
 ALI_I3_AccessProvider& operator=(const ALI_I3_AccessProvider& a);

 bool fLoadAccessProvider(XMLNode  objXML);
 void fConvertTelURItoTN();
 void fClear();
};



class ALI_I3_GeneralUse
{
 public:
  string strID, strGeneralUse;

 ALI_I3_GeneralUse();
~ALI_I3_GeneralUse(){}
//Copy Constructor
 ALI_I3_GeneralUse(const ALI_I3_GeneralUse *a);

 // Declare Assignment operator
 ALI_I3_GeneralUse& operator=(const ALI_I3_GeneralUse& a);

 bool fLoadGeneralUse(XMLNode  objXML);
 void fClear();
};


class ALI_I3_SourceInfo
{
 public:
  ALI_I3_DataProvider		DataProvider;
  ALI_I3_AccessProvider		AccessProvider;	
  string			strALIUpdateGMT;
  string			strALIRetrievalGMT;
  vector <ALI_I3_GeneralUse>   	vGeneralUses;


 ALI_I3_SourceInfo();
~ALI_I3_SourceInfo(){}

 //Copy Constructor
 ALI_I3_SourceInfo(const ALI_I3_SourceInfo *a);

 // Declare Assignment operator
 ALI_I3_SourceInfo& operator=(const ALI_I3_SourceInfo& a);


 bool fLoadSourceInfo(XMLNode  objXML);
 bool fLoadGeneralUses(XMLNode  objXML);
 void fClear();
};

class ALI_I3_NetworkInfo
{
 public:
  string strPSAPALIHost, strRespALIHost, strPSAPID, strPSAPName, strRouterID, strExchange, strCLLI;

 ALI_I3_NetworkInfo();
~ALI_I3_NetworkInfo(){}
 //Copy Constructor
 ALI_I3_NetworkInfo(const ALI_I3_NetworkInfo *a);

 // Declare Assignment operator
 ALI_I3_NetworkInfo& operator=(const ALI_I3_NetworkInfo& a);

 bool fLoadNetworkInfo(XMLNode  objXML);
 void fClear();
};

class ALI_I3_Extension
{
 public:
  string strName, strSource, strVersion;

 ALI_I3_Extension();
~ALI_I3_Extension(){}
 //Copy Constructor
 ALI_I3_Extension(const ALI_I3_Extension *a);

 // Declare Assignment operator
 ALI_I3_Extension& operator=(const ALI_I3_Extension& a);

 bool fLoadExtension(XMLNode  objXML);
 void fClear(); 
};

class ALI_I3_XML_Data
{
 public:
 ALI_I3_CallInfo 		CallInfo;
 ALI_I3_LocationInfo		LocationInfo;
 ALI_I3_Agencies		Agencies;
 ALI_I3_SourceInfo      	SourceInfo;
 ALI_I3_NetworkInfo		NetworkInfo; 
 vector <ALI_I3_Extension> 	vExtension;
 string                         XMLstring;


 // constructor
 ALI_I3_XML_Data();
~ALI_I3_XML_Data(){}


 // Declare Copy Constructor
 ALI_I3_XML_Data(const ALI_I3_XML_Data *a);

 // Declare Assignment operator
 ALI_I3_XML_Data& operator=(const ALI_I3_XML_Data& a);


 bool fLoadExtensions(XMLNode  objXML);
 void fClear(); 
};


class ALI_I3_Data
{
 public:
 lis_functions    enumLISfunction;             
 LocationURI      objLocationURI;
 LocationURI      objGeoLocationHeaderURI;
 string           strPANI;
 string           strNenaCallId;
 string           strNenaIncidentId;
 string           strRawCallinfoHeader;
 SERVICELIST      ServiceURNList;
 string           strEntity;
 bool             boolPidflobyValue;
 string           strCallInfoProviderCID;
 string           strCallInfoProviderURL;
 string           strCallInfoServiceCID;
 string           strCallInfoServiceURL;
 string           strCallInfoSubscriberCID;
 string           strCallInfoSubscriberURL;
 ALI_I3_XML_Data  I3XMLData;
 ADR_Data         ADRdata;
 string           strBidId;
 string           strTimeStamp;
 struct timespec  tsTimeStamp;
 string           strALI30WRecord;
 string           strALI30XRecord;
 string           strNGALIRecord;
 bool             boolByValue;
 string           strPidfXMLData;
 string           strTupleID;
 bool             boolEmergencyCallDataRequestFlag;
 bool             boolProviderInfoRXflag;
 bool             boolServiceInfoRXflag;
 bool             boolSubsciberInfoRXflag;
 bool             boolPidfLoRXflag; 
 bool             boolADRreadyFlag;
 bool             boolServiceURIreadyFlag;
 bool             boolServiceFireRXflag;
 bool             boolServiceEmsRXflag;
 bool             boolServicePoliceRXflag;
 // Constructor
 ALI_I3_Data();
 
 // Destructor
 ~ALI_I3_Data(){}

 // Declare Copy Constructor
 ALI_I3_Data(const ALI_I3_Data *a);

 // Declare Assignment operator
 ALI_I3_Data& operator=(const ALI_I3_Data& a);


 string fTimeStamp();
 string fBidId();
 bool   fLoadBidID(string strInput);
 bool   fLoadLocationURI(string strInput);
 bool   fLoadGeoLocationURI(string strInput);
 bool   fLoadGeoLocationURINoLTGT(string strInput);
 bool   fLoadLocationData(string strInput, string* strError);
 bool   fLoadLocationData(DataPacketIn objData, string* strError);
 string fCreateLegacyALIrecord();
 string fCreateLegacyALI30Xrecord();
 string fCreateNextGenALIrecord();
 void   fLoadSimulationData(string strbid);
 bool   fLoadI3ALIData(XMLNode  objXML);
 bool   fLoadATTlocationData(XMLNode objXML);
 bool   fLoadLocationInfoNode(XMLNode GeoprivNode);
 bool   fLoadPidfloEntity(string strInput);
 bool   fLoadServiceList(string strInput);
 bool   fConvertServiceListToALIbody();
 string fGMTtoLocalTimeStamp(string strData);
 bool   fLoadNenaCallId(string strInput); 
 bool   fLoadNenaIncidentId(string strInput);
 bool   fLoadCallInfoEmergencyCallData(string strInput);
 bool   fLoadCallInfoEmergencyCallData(string strInput, string strType);
 void   fMapADRProviderInfoToI3xml();
 bool   fMapADRServiceInfoToI3xml();
 bool   fMapADRServiceInfoToI3xml(string strCOS, string strCode); 
 bool   fMapATTtypeofServiceToI3xml();
 bool   fMapMainTelNumbertoI3xml();
 bool   fMapMainTelNumbertoI3xml(string strData);
 bool   fMapCustomerNametoI3xml();
 bool   fMapNotestoI3xml();
 bool   SetADRflags(bool boolInput=false);
 bool   fNoADRdataReceived();
 bool   fAllADRdataReceived();
// bool   fServiceURIDataBidComplete();  //reworked 
// bool   SetServiceURIflags(bool boolInput=false);
 void   fClear(); 
 void   fDisplay();
};


class Phone_ALI {
 public:
  ofstream PhoneALIfile[NUM_WRK_STATIONS_MAX+1];

  // constructor
 Phone_ALI(){}
~Phone_ALI(){
  for (int i = 0; i < NUM_WRK_STATIONS_MAX; i++) {PhoneALIfile[i].close();} 
 }

 bool fupdatePhoneALIFile(int i, string strInput);
};

class ALI_Data
{
// friend class boost::serialization::access;
 
 public:
  ali_functions     eJSONEventCode;
  bid_type          enumALIBidType;
  string            stringALiRequestKey;
  string            stringType;
  string            strClassOfService;                  
  int               intALIState;
  string            stringAliText;
  string            stringAliTextMessage;
  int               intAliTextMessageLength;
  string            strCADmessageWithoutHeader;
  int               intCADmessageWithoutHeaderLength;
  string            stringALiTestMessageWOCR;
  int               intAliTextMessageWOCRLength;
  string            strCADmessageWithoutHeaderWOCR;
  int               intCADmessageWithoutHeaderWOCRLength;
  string            strCADmessageSent;
  string            stringAliRequest;
  string            stringAutoRepeatAliRequest;
  string            strALIRequestTrunk;
  bool              boolALI_Test_Key_Was_Bid;
  int               intALIPortPairUsedtoTX;
  int               intALIBidIndex;
  unsigned long int intTransactionIDBid[2+1];
  int               intE2RequestLength;
  int               intLastDatabaseBid;
  bool              bool_LF_AS_CR;
  int               intFoundinDatabase[MAX_NUMBER_ALI_DATABASES + 1];
  unsigned char     chE2BidRequest[E2_MAX_REQUEST_LENGTH];
  int               intNumberOfInitialBids;
  int               intNumberOfRetryBids;
  bool              boolALIRecordActive;
  bool              boolALIRecordFail;
  bool              boolALIRecordACKReceived[2+1];
  bool              boolALIRecordRetransmit[2+1];  
  string            stringALIReceivedTimeStamp;
  string            stringALITimeoutTimeStamp;
  string            stringALIRebidTimeStamp;   
  bool              boolALIRecordReceived;
  struct timespec   timespecTimeAliAutoRebidOK;
  int               intALITimeoutSec;
  ALI_E2_Data       E2Data;
  ALI_I3_Data       I3Data; 

 // Constructor
 ALI_Data()
  {
   eJSONEventCode		      = NO_ALI_FUNCTION_DEFINED;
   enumALIBidType                     = NO_BID;
   stringALiRequestKey                = "";
   stringType                         = "";
   strClassOfService                  = "";
   intALIState                        = 0;
   stringAliText                      = "";
   stringAliTextMessage               = "";
   intAliTextMessageLength            = 0;
   strCADmessageWithoutHeader         = "";
   intCADmessageWithoutHeaderLength   = 0;
   stringALiTestMessageWOCR           = "";
   intAliTextMessageWOCRLength        = 0;
   strCADmessageWithoutHeaderWOCR     = "";
   intCADmessageWithoutHeaderWOCRLength = 0;
   strCADmessageSent                  = "";
   stringAliRequest                   = "";
   stringAutoRepeatAliRequest         = "";
   strALIRequestTrunk                 = "";
   boolALI_Test_Key_Was_Bid           = false;
   intALIPortPairUsedtoTX             = 0;
   intALIBidIndex                     = 0;
   intTransactionIDBid[1]             = 0;
   intTransactionIDBid[2]             = 0;
   intE2RequestLength                 = 0;
   intLastDatabaseBid                 = 0;
   bool_LF_AS_CR                      = false;
   for (int i = 1; i <= MAX_NUMBER_ALI_DATABASES; i++){ intFoundinDatabase[i] = 0;}
   for (int i = 0; i < E2_MAX_REQUEST_LENGTH; i++)    {chE2BidRequest[i]      = 0x00;}
   intNumberOfInitialBids             = 0;
   intNumberOfRetryBids               = 0;
   boolALIRecordActive                = false;
   boolALIRecordFail                  = false;
   boolALIRecordACKReceived[1]        = false;
   boolALIRecordACKReceived[2]        = false;
   boolALIRecordRetransmit[1]         = true;
   boolALIRecordRetransmit[2]         = true;
   stringALIReceivedTimeStamp         = "";
   stringALITimeoutTimeStamp          = "";
   stringALIRebidTimeStamp            = "";   
   boolALIRecordReceived              = false;
   timespecTimeAliAutoRebidOK.tv_sec  = 0;
   timespecTimeAliAutoRebidOK.tv_nsec = 0;
   intALITimeoutSec                   = 0;
   E2Data.fClear();
   I3Data.fClear();
  }

 // Destructor
 ~ALI_Data(){}

 // assignment operator
 ALI_Data& operator= (const ALI_Data& b)
  {
   if (&b == this ) {return *this;}
   eJSONEventCode		      = b.eJSONEventCode;
   enumALIBidType                     = b.enumALIBidType;
   stringALiRequestKey                = b.stringALiRequestKey;
   stringType                         = b.stringType;
   strClassOfService                  = b.strClassOfService;
   intALIState                        = b.intALIState;
   stringAliText                      = b.stringAliText;
   stringAliTextMessage               = b.stringAliTextMessage;
   intAliTextMessageLength            = b.intAliTextMessageLength;
   strCADmessageWithoutHeader         = b.strCADmessageWithoutHeader;
   intCADmessageWithoutHeaderLength   = b.intCADmessageWithoutHeaderLength;
   stringALiTestMessageWOCR           = b.stringALiTestMessageWOCR;
   intAliTextMessageWOCRLength        = b.intAliTextMessageWOCRLength;
   strCADmessageWithoutHeaderWOCR     = b.strCADmessageWithoutHeaderWOCR;
   intCADmessageWithoutHeaderWOCRLength = b.intCADmessageWithoutHeaderWOCRLength;
   strCADmessageSent                  = b.strCADmessageSent;
   stringAliRequest                   = b.stringAliRequest;
   stringAutoRepeatAliRequest         = b.stringAutoRepeatAliRequest;
   strALIRequestTrunk                 = b.strALIRequestTrunk;
   boolALI_Test_Key_Was_Bid           = b.boolALI_Test_Key_Was_Bid ;
   intALIPortPairUsedtoTX             = b.intALIPortPairUsedtoTX;
   intALIBidIndex                     = b.intALIBidIndex;
   intTransactionIDBid[1]             = b.intTransactionIDBid[1];
   intTransactionIDBid[2]             = b.intTransactionIDBid[2];
   intE2RequestLength                 = b.intE2RequestLength;
   intLastDatabaseBid                 = b.intLastDatabaseBid;
   bool_LF_AS_CR                      = b.bool_LF_AS_CR;
   for (int i = 1; i <= MAX_NUMBER_ALI_DATABASES; i++){ intFoundinDatabase[i] = b.intFoundinDatabase[i];}
   for (int i = 0; i < E2_MAX_REQUEST_LENGTH; i++)    {chE2BidRequest[i]      = b.chE2BidRequest[i];}
   intNumberOfInitialBids             = b.intNumberOfInitialBids;
   intNumberOfRetryBids               = 0;
   boolALIRecordActive                = b.boolALIRecordActive;
   boolALIRecordFail                  = b.boolALIRecordFail;
   boolALIRecordACKReceived[1]        = b.boolALIRecordACKReceived[1];
   boolALIRecordACKReceived[2]        = b.boolALIRecordACKReceived[2];
   boolALIRecordRetransmit[1]         = b.boolALIRecordRetransmit[1];
   boolALIRecordRetransmit[2]         = b.boolALIRecordRetransmit[2];
   stringALIReceivedTimeStamp         = b.stringALIReceivedTimeStamp;
   stringALITimeoutTimeStamp          = b.stringALITimeoutTimeStamp;
   stringALIRebidTimeStamp            = b.stringALIRebidTimeStamp;   
   boolALIRecordReceived              = b.boolALIRecordReceived;
   timespecTimeAliAutoRebidOK.tv_sec  = b.timespecTimeAliAutoRebidOK.tv_sec;
   timespecTimeAliAutoRebidOK.tv_nsec = b.timespecTimeAliAutoRebidOK.tv_nsec;
   intALITimeoutSec                   = b.intALITimeoutSec;
   E2Data                             = b.E2Data;
   I3Data                             = b.I3Data;
   return *this;
  }

 // functions
 void   fClear();
 int    fHowManyDatabasesCanbeBid();
 string fALIStateString();
 unsigned long long int fFindCallback(int index, ALISteering ALIsvcData);

 int    fFindRowFromClassofService(ALISteering ALIsvcData);
 string fRowOfALIdata(int iRow, ALISteering ALIsvcData);
 bool   fIsPhaseTwoALI();
 bool   fLoadEncodedALItext(string strInput);
 string fALIbidType();
 string fALItoHTML(int intPosition);
};

class SMS_Data
{
 public:
 string           strSMSmessage;
 LocationURI      objLocationURI;
 string           strBidId;
 string           strFromUser;
 string           strFromHost;
 string           strToUser;
 string           strToHost;
 string           strConversation;

 SMS_Data();
~SMS_Data(){}

 void fClear();
 bool fLoadTo(string strInput);
 bool fLoadFrom(string strInput);
 bool fLoadLocationData(string strInput);
 bool fLoadBidID(string strInput);
 bool fLoadUUIDinBidID(string strInput);
 bool fLoadSMSmessage(string strInput);
 bool fLoadMSRPdispatcherSays(string strArg, bool Encoded);
 bool CreateCallerSays();
 bool CIVICA_TEST();
};




class TDD_Data
{
 public:
 bool                    boolTDDWindowActive;
 bool	                 boolTDDSendButton;
 bool                    boolTDDmute;
 bool                    boolTDDthresholdMet;
 string                  strTDDstring;
 string                  strTDDCallerBuffer;
 string                  strTDDcharacter;
 string                  strTDDconversation;
 struct timespec         timespecTimeTDDcharRecv;
 SMS_Data                SMSdata;

 // Constructor
 TDD_Data()
  {
   boolTDDWindowActive             = false;                   
   boolTDDSendButton               = false;
   boolTDDmute                     = false;
   boolTDDthresholdMet             = false;
   strTDDstring                    = "";
   strTDDCallerBuffer              = "";   
   strTDDcharacter                 = "";
   strTDDconversation              = "";
   timespecTimeTDDcharRecv.tv_sec  = 0;
   timespecTimeTDDcharRecv.tv_nsec = 0;
  }
 // Destructor
 ~TDD_Data(){}

 // assignment operator
 TDD_Data& operator= (const TDD_Data& b)
  {
   if (&b == this ) {return *this;}
   boolTDDWindowActive             = b.boolTDDWindowActive;                   
   boolTDDSendButton               = b.boolTDDSendButton;
   boolTDDmute                     = b.boolTDDmute;
   boolTDDthresholdMet             = b.boolTDDthresholdMet;
   strTDDstring                    = b.strTDDstring;
   strTDDCallerBuffer              = b.strTDDCallerBuffer;   
   strTDDcharacter                 = b.strTDDcharacter;
   strTDDconversation              = b.strTDDconversation;
   timespecTimeTDDcharRecv.tv_sec  = b.timespecTimeTDDcharRecv.tv_sec;
   timespecTimeTDDcharRecv.tv_nsec = b.timespecTimeTDDcharRecv.tv_nsec;
   SMSdata                         = b.SMSdata;
   return *this;
  }

 // functions
 void   fClear();
 void   fSetWindow(bool SetActive);
 bool   fSetTDDSendButton();
 bool   fLoadTDDReceived(string strTDD, bool IsString = false, bool IsEncoded = false);
 void   fLoad_TDDmute(string stringArg);
 void   fAddCharacterToBuffer();
 bool   CreateCallerSays();

};

class Text_Data
{
 public:
 TDD_Data                TDDdata;
 SMS_Data                SMSdata;
 text_type               TextType;
 bool                    boolTextWindowActive;                   
 bool                    boolTextSendButton;
 string                  strConversation;
 LocationURI             objLocationURI;

 // Constructor
 Text_Data()
  {
   TextType                         = NO_TEXT_TYPE;
   boolTextWindowActive             = false;                   
   boolTextSendButton               = false;
   strConversation.clear();
   objLocationURI.fClear();                   
  }
 // Destructor
 ~Text_Data(){}

 // assignment operator
 Text_Data& operator= (const Text_Data& b)
  {
   if (&b == this ) {return *this;}
   TextType             = b.TextType;                   
   TDDdata              = b.TDDdata;
   SMSdata              = b.SMSdata;
   boolTextWindowActive = b.boolTextWindowActive;
   boolTextSendButton   = b.boolTextSendButton;
   strConversation      = b.strConversation;
   objLocationURI       = b.objLocationURI;
   return *this;
  }

 // functions
 void   fClear();
 bool   fLoadTextType(string strArg);
 void   fSetWindow(bool SetActive);
 bool   fSetTextSendButton();
};



class NG911_Call_Info_Header
{
 public:
 string CallInfoHeader;

// constructor
 NG911_Call_Info_Header(){}
~NG911_Call_Info_Header(){}

};

class NG911_History_Info_Header
{
 public:
 string HistoryInfoHeader;

// constructor
 NG911_History_Info_Header(){}
~NG911_History_Info_Header(){}

};

class NG911_SIP_Headers
{
 public:
 vector <NG911_Call_Info_Header>    vCallInfoHeaders;
 vector <NG911_History_Info_Header> vHistoryInfoHeaders;


// constructor
 NG911_SIP_Headers(){}
~NG911_SIP_Headers(){}

void fLoadCallInfoHeaders(string strInput);
void fLoadHistoryInfoHeaders(string strInput);
void fAddHistoryInfoHeader(NG911_History_Info_Header objHeader);
void fAddCallInfoHeader(NG911_Call_Info_Header objHeader);
};







class IP_Address
{
 public:
  string        stringAddress;

 // constructor
 IP_Address()
  {
   stringAddress.clear();
  }

 ~IP_Address(){};
 // copy constructor
 IP_Address(const IP_Address& b)
  {
   stringAddress = b.stringAddress;
  }
 IP_Address& operator= (const IP_Address& b)
  {
   if (&b == this ) {return *this;}
   stringAddress = b.stringAddress;
   return *this;
  }
  bool operator == (const IP_Address& a) const { return fCompare(a);}  

  bool   fIsValid_IP_Address();
  void   fClear();
  string fParseIPaddressUsingValueKey(string strKey, string strData);
  string fDisplay();
  bool   fCompare(const IP_Address& a) const;
};

class PORT_INIT_VARS {
public:
 int                    intPORT_NUMBER;
 int                    intHEARTBEAT_INTERVAL_SEC;
 int                    intPORT_DOWN_THRESHOLD_SEC;
 int                    intPORT_DOWN_REMINDER_SEC;
 int                    intREMOTE_PORT_NUMBER;
 int                    intLOCAL_PORT_NUMBER;
 IP_Address             REMOTE_IP_ADDRESS;
 string                 strNotes;
 bool                   boolPhantomPort;
 string                 strVendorEmailAddress;
 Port_Connection_Type   ePortConnectionType;
 Port_ALI_Protocol_Type eALIportProtocol;
 ali_format             eRequestKeyFormat;

 Workstation_Group      WorkstationGroup;
 Position_Aliases       PositionAliases;
 bool                   boolNENA_CADheader;
 bool                   boolCAD_EraseFirstALI_CR;
 bool                   boolHeartbeatRequired;
 bool                   boolRecordACKRequired;
 bool                   boolHeartBeatACKrequired;
 bool                   boolSendCADEraseMsg;

 PORT_INIT_VARS();
 ~PORT_INIT_VARS(){}
  //copy constructor
  PORT_INIT_VARS(const PORT_INIT_VARS *a);
  //  Assignment operator
  PORT_INIT_VARS& operator=(const PORT_INIT_VARS& a);
  //  equals operator
 bool operator == (const PORT_INIT_VARS& a) const { return fCompare(a);}


bool fCompare(const PORT_INIT_VARS& a) const;
bool fLoad_Vendor_Email_Address(string stringArg);
void fClear();
};

class Refer_Data {

 public:
 string 		strContactParams;
 string			strContactURI;
 string			strContactHost;
 string 		strContactUser;
 string			strContactPort;

 // Constructor
 Refer_Data();
 
 // Destructor
 ~Refer_Data(){}

 //assignment operator
 Refer_Data& operator= (const Refer_Data& b);
 
 // functions
 void fClear();
 void fDisplay();
 bool fLoadContactParams(string strInput);
 bool fLoadContactURI(string strInput);
 bool fLoadContactHost(string strInput);
 bool fLoadContactUser(string strInput);
 bool fLoadContactPort(string strInput);
};

class Channel_Data {
 //friend class boost::serialization::access;
 public:
 string                    strChannelName;
 string                    strChannelID;
 int                       iGUIlineView;
 int                       iRingingLineNumber[NUM_WRK_STATIONS_MAX+1];
 int                       iLineNumber;
 string                    strCallerID;
 string                    strConfDisplay;
 string                    strTranData;
 string                    strCustName;
 bool                      boolConnected;
 bool                      boolOnHold;
 int                       intTrunkType;
 bool                      boolIsPosition;
 bool                      boolIsOutbound;
 IP_Address                IPaddress;
 int                       iTrunkNumber;
 int                       iPositionNumber;
 int                       iRingingPosition;
 string                    strHangupCause;
 int                       iSLABarge;
 bool                      boolTDDMode;
 string                    strCallRecording;
 string			   strPresenceID;
 bool                      boolMSRP;
 string                    strEndpointDisposition;
 string			   strHangupDisposition;
 string                    strTransferDisposition;
 string                    strValetExtension;
 bool                      boolParked;
 // Constructor
 Channel_Data();
 
 // Destructor
 ~Channel_Data(){}

 //assignment operator
 Channel_Data& operator= (const Channel_Data& b);
 
 // functions

 void fClear();
 void fDisplay(int i=0, bool showRuler=false);
 void fLoadConferenceDisplayC(int i);
 void fLoadConferenceDisplayD(int i);
 void fLoadConferenceDisplayT(int i);
// void fLoadConferenceDisplayP();
 void fLoadConferenceDisplayP(int i);
 void fLoadConferenceDisplayX(int i);
// void fLoadPositionCallID();
// void fLoadDestCallID(string strInput);
 void fLoadChannelName(string strChannel);
 void fLoadChannelID(string strID);
 void fLoadTransferData(string strID);
 void fLoadChannelDatafromTransferData(Transfer_Data objTransferData);
 void fLoadChannelDatafromTransferData(Transfer_Data objTransferData, int iPosition);
 bool fLoadTrunkNumber(string strInput, string strData);
 bool fLoadTrunkNumber(int iTrunk);
 int  fLoadPositionNumber(string strData);
 int  fLoadPositionNumber(int intArg);
 void fLoadHangupCause(string strInput);
 int  fLoadLineNumberFromChannel(string strChannel, int iPosition);
 int  fLoadLineNumberFromChannel();
 int  fLoadLineNumberFromUserName(string strUsername);
 void fLoadCustomerName(int i);
 void fLoadPositionCallID(int i);
 void fLoadLinkDataIntoChannelData(Link_Data oblLinkData, int iLinkNum, bool CallerIDsReversed=false);
 int  fLoadGUILineView(string strInput);
 void fLoadMSRPPositionChannel(int i);
 void fSetChannelHangup(Channel_Data objChannelData);
 void fSetChannelHangup();
 void fSetPositionDataIntoChannelData(int iPosition);
};

class Transfer_Data
{
 public:
 string           strTransferType;
 string           strTransferFromPosition;
 string           strTransferFromChannel;
 string           strTransferFromUniqueid;
 string           strTransferMatchingChannelId;
 string           strTransferTargetUUID;
 string           strTransfertoNumber;
 string           strNG911PSAP_TransferNumber;
 bool             boolGUIcancel;
 transfer_method  eTransferMethod;
 string           strTransferName;
 string           strConfDisplay;
 string           strRDNIS;
 bool             boolIsRingBack;
 string           strHangupCause;
 IP_Address       IPaddress;
 string           strValetExtension;
 vector <string>  vUUID;

 Transfer_Data();
 
~Transfer_Data(){}

 Transfer_Data& operator= (const Transfer_Data& b);
 

 void   	fDisplay(int i=0);
 void   	fDisplayLabel();
 void   	fClear();
 string 	fTransferMethod();
 transfer_method fTransferMethod(string strInput);

 int    	fReturnTransferNumberAsPositionNumber();
 int    	fLoadFromPosition(int i);
 int    	fLoadFromPosition(string strPosition);
 int    	fLoadFromPosition(string strData, string strKey);
 bool   	fLoad_Transfer_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum);
 void   	fLoadHangupCause(string strInput);
 void   	fLoadConferenceDisplayX(int i);
 void   	fLoadConferenceDisplayT(int i);
 void   	fLoad_RDNIS(string strData);
 bool   	fCheckINDIGITAL_Transfer_Number();
 bool   	fCheckINTRADO_TRANSFER(int i);
 bool   	fCheckREFER_TRANSFER(int i);
 bool   	fIsPark(bool &boolValet);
 
};

class Link_Data
{
// friend class boost::serialization::access;
 public:
 string                    strChannelone;
 string                    strChanneloneID;
 string                    strChanneltwo;
 string                    strChanneltwoID;
 string                    strCallerIDone;
 string                    strCallerIDtwo;
 string                    strCustNameOne;
 string                    strCustNameTwo;
 IP_Address                IPaddressOne;
 IP_Address                IPaddressTwo;
 int                       intTransferFromPosition;
 bool                      bBothChannelsPositions;
 
 // Constructor
 Link_Data()
  {
   strChannelone.clear();
   strChanneloneID.clear();
   strChanneltwo.clear();
   strChanneltwoID.clear();
   strCallerIDone.clear();
   strCallerIDtwo.clear();
   strCustNameOne.clear();
   strCustNameTwo.clear();
   IPaddressOne.fClear();
   IPaddressTwo.fClear();
   intTransferFromPosition = 0;
   bBothChannelsPositions = false;
  }
 // Destructor
 ~Link_Data(){}
 // assignment operator 
 Link_Data& operator= (const Link_Data& b)
  {
   if (&b == this ) {return *this;}
   strChannelone           = b.strChannelone;
   strChanneloneID         = b.strChanneloneID;
   strChanneltwo           = b.strChanneltwo;
   strChanneltwoID         = b.strChanneltwoID;
   strCallerIDone          = b.strCallerIDone;
   strCallerIDtwo          = b.strCallerIDtwo;
   strCustNameOne          = b.strCustNameOne;
   strCustNameTwo          = b.strCustNameTwo;
   IPaddressOne            = b.IPaddressOne;
   IPaddressTwo            = b.IPaddressTwo;
   intTransferFromPosition = b.intTransferFromPosition;
   bBothChannelsPositions  = b.bBothChannelsPositions;
   return *this;   
  }

 // functions
 void fClear();
 void fDisplay();
 bool fAreBothPositionChannels();
 bool fLoadExtension(string strArg);
};

class Freeswitch_Data  {

 public:
 freeswitch_event         enumFreeswitchEvent;
 Channel_Data             objChannelData;
 Channel_Data             objRing_Dial_Data;
 Link_Data                objLinkData;
 Refer_Data               objReferData;
 bool                     boolValidPosition;
 int                      iConfDisplayNumber;
 string                   strFreeswitchConferenceUUID;
 NG911_SIP_Headers        objNGSIPheaders;

 vector <Channel_Data>       vectPostionsOnCall;
 vector <Channel_Data>       vectDestChannelsOnCall;

 Freeswitch_Data();
 
 Freeswitch_Data& operator= (const Freeswitch_Data& b);

 // Destructor
 ~Freeswitch_Data(){}

 void   fClear();
 void   fDisplay();
 bool   fLoadChannelDataFromOutboundDial(string strArg);
 bool   fLoadBridgeDataFromFreeswitchSLA(string strArg);
 bool   fLoadBridgeDataFromFreeswitchLineRoll(string strArg);
 bool   fLoadChannelFromFreeswitchTransferSLA(string strArg);
 bool   fLoadChannelFromFreeswitchConferenceSLA(string strArg);
 bool   fLoadLinkDataFromFreeswitchTransferLineRoll(string strArg);
 bool   fLoadSLAHoldTransferDataFromFreeswitch(string strArg);
 int    fLoad_Bridge_Data_Into_Object(Link_Data objInput, int iChannel, int iTrunk=0 , bool boolcalleridsreversed=false, string strInput ="", bool boolPush=true);
 bool   fLoadChannelDataFromNonValetParkPickup(string strInput);
 int    fFindLegendInDestVector(string strLegend);
 int    fFindLegendInPosnVector(string strLegend);
 int    fFindPositionInPosnVector(int intPos); 
 int    fFindChannelInPosnVector(string strChannelID);
 int    fFindChannelInDestVector(string strChannelID);
 int    fFindDestChannel911Caller();
 int    fFindFirstDestChannelCLIDCaller();
 int    fLineNumberofPositiononCall(int iPos);
 int    fLineNumberofRingingCall(int iPos);
 void   fSetOnHoldinPostionVector(int iPos);
 void   fSetOffHoldinPostionVector(int iPos);
 bool   fLineRegistrationNameIsOnCall(string strName);
 bool   AddtoPositionVector(Channel_Data objChannelData);
 bool   fRemovePosnChannelFromVector(unsigned int index);
 bool   fRemoveDestChannelFromVector(unsigned int index);
 bool   fLoad_Channels_in_LinkData();
 bool   fLoad_Last_Channel_into_Channelobject();
 bool   fLoad_EavesDrop_into_Channelobject();
 bool   fLoad_LinkChannel_into_Channelobject(int ichannel);
 bool   fBuild_FreeSWITCH_Conference(bool bBlindTransfer=false);
 size_t fNumberofChannelsInCall();
 string fFindDestinationChannelIDfromPhoneNumber(string strPhoneNumber);
 string fFindPositionChannelIDfromPhoneNumber(string strPhoneNumber);
};

class Stream_ID
{
 public:
 string                 strUUID;
 string                 strCallerID;
 string                 strEpoch;

 Stream_ID()
  {
   strUUID      = "";
   strCallerID  = "";
   strEpoch     = "";
  }
 //destructor
 ~Stream_ID(){}
// assignment operator
 Stream_ID& operator= (const Stream_ID& b)
  {
   if (&b == this ) {return *this;}
   strUUID      = b.strUUID;
   strCallerID  = b.strCallerID;
   strEpoch     = b.strEpoch;
   return *this;
  } 

 bool fLoadStreamID(string strInput);
};


class Conf_Data
{
// friend class boost::serialization::access;
 public:
 string                 strParticipant;
 string                 strActiveConferencePositions;
 string                 strHistoryConferencePositions;
 string                 strOnHoldPositions;
 string                 strOnHoldPosAndTime;
 string                 strParkParticipant;
 string                 strParkTime;
 string			strParkLot;
 vector <Channel_Data>  vectConferenceChannelData;
 //constructor
 Conf_Data()
  {
   strParticipant                = "";
   strActiveConferencePositions  = "";
   strHistoryConferencePositions = "";
   strOnHoldPositions            = "";
   strOnHoldPosAndTime           = "";
   strParkParticipant            = "";
   strParkTime                   = "";
   strParkLot                    = "";
   vectConferenceChannelData.clear();
  }
 //destructor
 ~Conf_Data(){}

 // assignment operator
 Conf_Data& operator= (const Conf_Data& b)
  {
   if (&b == this ) {return *this;}
   strParticipant                = b.strParticipant;
   strActiveConferencePositions  = b.strActiveConferencePositions;
   strHistoryConferencePositions = b.strHistoryConferencePositions;
   strOnHoldPositions            = b.strOnHoldPositions;
   strOnHoldPosAndTime           = b.strOnHoldPosAndTime;
   strParkParticipant            = b.strParkParticipant;
   strParkTime                   = b.strParkTime;
   strParkLot                    = b.strParkLot;   
   vectConferenceChannelData     = b.vectConferenceChannelData;
   return *this;
  } 

 bool fPositionInHistory();
 bool fPositionInHistory(int i);
 bool fPositionActive(int i);
 int  fFindConferenceData(string strUID);
 int  fFindConferenceDataFromCallIDNumber(string strNumber);
 int  fFindConferenceDataFromConfDisplay(string strLegend);
 int  fFindConferenceDataFromPositionNumber(int i);
 int  fFindTransferTargetInConfVector(string strUUID);
 void fRemoveDuplicatePositionChannels(int i);
 void fConfDataVectorRemoveDuplicateUUID();  
 bool fRemoveTransferFromHistory(string strData);
 bool fAnyPositionsActiveOnCall();
 bool fAnyPositionsOnHold();
 void fSetPositionOnHoldinConfVector(int i);
 void fSetPositionOffHoldinConfVector(int i);
 string fOnHoldTimeStampsStrippedOfPositions();
 int  fNumofPositionsInHistory();
 void fClear();
};




class Call_Data  {
 //friend class boost::serialization::access;

 public:
 ani_functions          eJSONEventCode;
 wrk_functions          eJSONEventWRKcode;
 transfer_method        eJSONEventTransferMethod;
 string                 strJSONEventMergedCallID;
 unsigned long long int intUniqueCallID;
 string                 stringUniqueCallID;
 int                    intTrunk;
 string                 stringTrunk;
 int                    intPosn;
 string                 stringPosn;
 int                    CTIsharedLine;
 string                 strLastHangupPosn;
 string                 strCustName;
 unsigned long long int intCallbackNumber;
 bool                   boolCallBackVerified;
 bool                   boolLandLineCall;
 bool                   boolWirelessVoipCall;
 bool                   boolBlindTransferSet;
 bool                   boolIsOutbound;
 bool                   boolBlindXferUpdate;
 bool                   boolConfXferUpdate;
 int                    intANIformat;
 string                 stringInfoCode;
 string                 stringOneDigitAreaCode;
 string                 stringThreeDigitAreaCode;
 string                 stringTelnoPrefix;
 string                 stringTelnoSuffix;
 string                 stringSevenDigitPhoneNumber;
 string                 stringTenDigitPhoneNumber;
 string                 strSIPphoneNumber;
 string                 strCallBackDisplay;
 string                 strCallTypeDisplay;
 string                 stringPANI;
 string                 strDialedNumber;
 string                 strConferenceNumber;
 unsigned long long int intConferenceNumber;
 string                 strFreeswitchConfName;
 string                 strFreeswitchConfID;
 string                 strCallRecording;
 Conf_Data              ConfData;
 Transfer_Data          TransferData;
 Refer_Data             ReferData;
 vector <Transfer_Data> vTransferData;
 vector <Stream_ID>     vStreamID;
 // Constructor
   Call_Data();
 
 // Destructor
 ~Call_Data(){}

  // assignment operator
  Call_Data& operator= (const Call_Data& b);
  
 
 void   fClear();
 void   fDisplay();
 void   fLoadTrunk(int intArg);
 void   fLoadPosn(int intArg);
 bool   fLoadPosn(string strArg);
 void   fLoadCallRecording(Channel_Data objChannelData);
 bool   fLoadCallbackNumber(unsigned long long int intArg);
 bool   fLoadCallbackNumber(string strNumber);
 string fLoadCallBackDisplay(string strInput, bool BoolSetasCallBack = false);
 string fLoadCallBackDisplay();
 void   fLoadCustName(string strInput);
 bool   fLoadCallUniqueID(string strArg);
 bool   fLoad_Info_Code(string strData, bool boolIsNPD = false);
 bool   fLoad_Conference_Number(string strData);
 bool   fHasAttendedTransfer(int iPosition);
 bool   fConferenceStringAdd(conference_member eConf, int i);
 bool   fConferenceStringRemove(int i);
 bool   fConferenceStringRemove(string strInput);
 bool   fConferenceStringRemove(conference_member eConf, int i);
 bool   fIsInConference(string strConf, bool boolCheckHistory = false);
 bool   fIsInConference(conference_member eConf, int i);
 bool   fIsInConference(conference_member eConf);
 void   fSetCallPark(string strParticipant, string strLot);
 void   fRemoveCallPark();
 void   fOnHoldPositionRemove(int i);
 void   fOnHoldPositionAdd(int i);
 bool   fIsOnHold(int i);
 bool   fIsInPositionHistory(string strLegend);
 int    fNumPositionsInConference();
 int    fNumDestinationsInConference();
 int    fNumPositionsOnHold();
 string fTenDigitCallbackGUIformat();
 string fTenDigitCallbackGUIformat(string strInput);
 int    fLegendInConferenceChannelDataVector(string strLegend);
 int    fFindChannelIDinConferenceVector(string strUUID);
 int    fFindCallbackwithChannelIDinConferenceVector(string strCallback, string strUUID);
 void   fSetTrunkTypeString(bool bOutbound = false, string strPhoneNum = "");
 void   fSetANIformat(int iFormat, int i);
 bool   fIsOutboundCall();
 void   fReleaseConferenceNumber();
 void   fSetCallInfoToSticks();
 bool   fSet_PhoneNumberFromANI(ani_format enumANIformat, ani_type enumANItype, string strInputANI );
 bool   fSet_Phone_Numbers_From_Bad_ANI(string strAsteriskANI);
 bool   fPerformedABlindTransfer(int iPositionNumber);
 bool   fTransferDataIsRingback(string stringUUID);
 bool   fIsChannelUUIDfromPark(string stringUUID);
 int    fVerify_NPDNumber();
 void   fSetBadPANInumber();
 string fCreateLocationRequest(bool firstBid);
 string fCreateServiceRequest(bool firstBid);
 int    fSetSharedLineFromLineNumberPlusPosition(int iLine);
 int    fSetSharedLineFromLineNumberPlusPosition(Channel_Data objChannelData);
 int    fFindTransferTarget(string strTransferTargetID );
 int    fFindTransferData(string strTransferNumber);
 int    fFindTransferData(string strTransferNumber, unsigned int iPosition);
 int    fFindTransferData(string strChannel, string strUniqueId);
 int    fFindTransferData(unsigned int iPosition);
 int    fFindTransferDataWithTransferTarget(string strUUID);
 bool   fRemoveTransferData(unsigned int index);
 int    fFindParkData(string strTransferNumber);
// bool   fFindandChangeChannelNameinVectors(string strChannel, string strUniqueID);
 bool   fConferenceBlindTransferWasSet();
 void   fAddAllPositionsToConference();
 void   fSetTDDModeonConferenceVector(string stChannelUUID);
 string fFindChannelIDfromPhoneNumber(string strInput);
 bool   fIsNG911SIP();
};

class Port_Data
{
 public:
 int			intPortNum;
 int			intSideAorB;
 string			strSideAorB;
 threadorPorttype       enumPortType;
 string                 strThread;
 bool                   boolVendorEmailAddress;
 string                 strVendorEmailAddress;

 // Constructor
   Port_Data()
    {
     intPortNum 		= 0;
     intSideAorB		= 0;
     strSideAorB.clear();
     enumPortType		= NO_PORT_OR_THREAD;
     strThread.clear();
     boolVendorEmailAddress	= false;
     strVendorEmailAddress.clear();
    }
 // Destructor
 ~Port_Data(){}
 // assignment operator
 Port_Data& operator= (const Port_Data& b)
  {
   if (&b == this ) {return *this;}
   intPortNum 		  = b.intPortNum;
   intSideAorB		  = b.intSideAorB;
   strSideAorB            = b.strSideAorB;
   enumPortType		  = b.enumPortType;
   strThread              = b.strThread;
   boolVendorEmailAddress = b.boolVendorEmailAddress;
   strVendorEmailAddress  = b.strVendorEmailAddress;
   return *this;
  } 
 void   fClear();
 void   fLoadThreadData(threadorPorttype enumArg);
 void   fLoadPortData(threadorPorttype enumArg, int PortNum, int SideAorB = 0);
 string fRemoteIPaddressofPort();
 string fConnectionProtocol();
 string fNotes();
 int    fRemotePortNumber();
 bool   fUseNENACADheader();
 bool   fRemoveFirstALiCR();
 int    fCadPortAlias(int i);
 size_t fCadPortAliasVectorSize();
 vector <Position_Alias> fCadPortAliasVector();
};




/*

class ConfAGIRequestDataPacketIn
{
 public:
 string  strAction;
 string  strConference;
 string  strPosition;
 string  strLineNumber;
 string  strExtension;
 string  strMessageID;
 string  strControllerCallUniqueID;
 string  strFirstChannel;
 string  strFirstAsteriskUniqueID;
 string  strSecondChannel;
 string  strSecondAsteriskUniqueID;
 string  strMeetmeExists;
 string  strTransferType;

 // constructor
 ConfAGIRequestDataPacketIn()
  {
   strAction                 = XML_VALUE_NONE;
   strConference             = "";
   strPosition               = "";
   strLineNumber             = "";
   strExtension              = "";
   strMessageID              = "";
   strControllerCallUniqueID = "";
   strFirstChannel           = "";
   strFirstAsteriskUniqueID  = "";
   strSecondChannel          = ""; 
   strSecondAsteriskUniqueID = "";
   strMeetmeExists           = "false";
   strTransferType           = XML_VALUE_NONE;
  }

 ~ConfAGIRequestDataPacketIn(){};

 ConfAGIRequestDataPacketIn& operator= (const ConfAGIRequestDataPacketIn& b)
  {
   strAction                 = b.strAction;
   strConference             = b.strConference;
   strPosition               = b.strPosition;
   strLineNumber             = b.strLineNumber;
   strExtension              = b.strExtension;
   strMessageID              = b.strMessageID;
   strControllerCallUniqueID = b.strControllerCallUniqueID;
   strFirstChannel           = b.strFirstChannel;
   strFirstAsteriskUniqueID  = b.strFirstAsteriskUniqueID;
   strSecondChannel          = b.strSecondChannel; 
   strSecondAsteriskUniqueID = b.strSecondAsteriskUniqueID;
   strMeetmeExists           = b.strMeetmeExists;
   strTransferType           = b.strTransferType;
   return *this;
  }

 string fCreateAGIconferenceMessage(string strAction);
 bool   fChannelDataIsEmpty();
 string fDisplay();
};

*/

class DataPacketIn
{
 public:
 int	                     intPortNum;
 int                         intWorkstation;
 int                         intSideAorB;
 size_t         	     intLength;
 IP_Address                  IPAddress;
 string	                     stringDataIn;
 unsigned long int           intTransactionID;
 int                         intConnectionID;
 volatile bool               boolStringBufferExceeded;
 ALI_E2_Data                 E2Data;
 bool                        boolALIservicePacket;
 string                      strRequestID;

// ConfAGIRequestDataPacketIn  ConfRequest;

 // constructor
 DataPacketIn()
  {
   intPortNum 		    = 0;
   intWorkstation 	    = 0;
   intSideAorB 		    = 0;
   intLength 		    = 0;
   stringDataIn		    = "";
   boolStringBufferExceeded = false;
   intTransactionID         = 0;
   intConnectionID          = 0;
   boolALIservicePacket     = false;
   strRequestID             = "";
  }
 
 ~DataPacketIn(){};

 DataPacketIn& operator= (const DataPacketIn& b)
  {
   if (&b == this ) {return *this;}
   intPortNum 		    = b.intPortNum;
   intWorkstation 	    = b.intWorkstation;
   intSideAorB 		    = b.intSideAorB;
   intLength 		    = b.intLength;
   IPAddress                = b.IPAddress;
   stringDataIn		    = b.stringDataIn;
   boolStringBufferExceeded = b.boolStringBufferExceeded;
   E2Data                   = b.E2Data;
   intTransactionID         = b.intTransactionID;
   intConnectionID          = b.intConnectionID;
   boolALIservicePacket     = b.boolALIservicePacket;
   strRequestID             = b.strRequestID;
   return *this;
  } 
void   fClear(); 
bool   fSetACK();
bool   fSetNAK();
bool   fCreateLegacyALIrecord();
void   fLoadSimulatedHTTPderef(string strfile, string strUUID, int iPort);
//int    fLoadConfRequest();
string fDisplay();
};




// external Vars utilizing DataPacketIn
extern queue <DataPacketIn>	queueCadPortData[NUM_CAD_PORTS_MAX+1];
extern queue <DataPacketIn>	queueANIPortData[NUM_ANI_PORTS_MAX+1];
extern queue <DataPacketIn>	queueALIPortData[intMAX_ALI_PORT_PAIRS  + 1];
extern queue <DataPacketIn>	queueWRKPortData;
extern queue <DataPacketIn>     queue_ServerStatusData;
extern queue <DataPacketIn>     queue_GatewayInterrogateReplyData;
extern queue <DataPacketIn>     queue_LISPortData;;
 

class ExperientPing : public Ping
{
 ExperientPing(){};
 virtual ~ExperientPing(){};

 virtual int FireResponse(PingResponseEventParams *e)
  {

//   //cout << "ID:     " << e->RequestId << endl;
//   //cout << "SOURCE: " << e->ResponseSource << endl;
//   //cout << "STATUS: " << e->ResponseStatus << endl;
//   //cout << "TIME:   " << e->ResponseTime << endl;
//   //cout << "RSVD:   " << e->reserved << endl;
  return 0;
  } 
};




class MessageClass {
 public: 
        string                  strMessageUUID;
        string                  strControllerUUID;
        message_type            enumMessageType;      
        string                  stringTimeStamp;
        string                  stringThreadCalling;
        string                  stringMessageText;
        string                  strCC_AlarmEmailAddress;
        bool                    boolCC_Alarm;
        int                     intLogCode;
        int                     intMessageCode;
        int                     intReturnCode;
        string                  stringOutputMessage;
        string                  stringSubject;
        string                  stringEmailMessageBody;
        string                  stringAlarmMessageText;
        timespec                mtimespecTimeStamp;
        timespec                rtimespecTimeStamp; 
        Port_Data               objPortData;
        Call_Data               objCallData;
        string                  strLogXML;
        string                  strJSONoutput;

	// constructor
	MessageClass()
	{
         strMessageUUID.clear();
         strControllerUUID.clear();
         enumMessageType                = NORMAL_MSG;
         stringTimeStamp                = "";
         stringThreadCalling            = "";
         stringMessageText              = "";
         strCC_AlarmEmailAddress        = "";
         boolCC_Alarm                   = false;
         intLogCode                     = 0;
         intMessageCode                 = 0;
         intReturnCode                  = 0;
         stringOutputMessage            = "";
         stringSubject                  = "";
         stringEmailMessageBody         = "";
         stringAlarmMessageText         = "";
         mtimespecTimeStamp.tv_sec      = 0;
         mtimespecTimeStamp.tv_nsec     = 0;
         rtimespecTimeStamp.tv_sec      = 0;
         rtimespecTimeStamp.tv_nsec     = 0;        
         objPortData.fClear();
         objCallData.fClear();
//         objFreeswitchData.fClear();
//         objALIdata.fClear();
         strLogXML                      = XML_LOG_ELEMENT;
         strJSONoutput.clear();           
    }
    // destructor
    ~MessageClass(){}

        MessageClass& operator= (const MessageClass& b)
        { 
         if (&b == this ) {return *this;}
         strMessageUUID                 = b.strMessageUUID;
         strControllerUUID              = b.strControllerUUID;
         enumMessageType                = b.enumMessageType;
         stringTimeStamp                = b.stringTimeStamp;
         stringThreadCalling            = b.stringThreadCalling;
         stringMessageText              = b.stringMessageText;
         strCC_AlarmEmailAddress        = b.strCC_AlarmEmailAddress;
         boolCC_Alarm                   = b.boolCC_Alarm;
         intLogCode                     = b.intLogCode;
         intMessageCode                 = b.intMessageCode;
         intReturnCode                  = b.intReturnCode;
         stringOutputMessage            = b.stringOutputMessage;
         stringSubject                  = b.stringSubject;
         stringEmailMessageBody         = b.stringEmailMessageBody;
         stringAlarmMessageText         = b.stringAlarmMessageText;
         mtimespecTimeStamp.tv_sec      = b.mtimespecTimeStamp.tv_sec;
         mtimespecTimeStamp.tv_nsec     = b.mtimespecTimeStamp.tv_nsec;
         rtimespecTimeStamp.tv_sec      = b.rtimespecTimeStamp.tv_sec;
         rtimespecTimeStamp.tv_nsec     = b.rtimespecTimeStamp.tv_nsec;   
         objPortData                    = b.objPortData;
         objCallData                    = b.objCallData;
 //        objFreeswitchData              = b.objFreeswitchData;
 //        objALIdata                     = b.objALIdata;
         strLogXML                      = b.strLogXML;
         strJSONoutput                  = b.strJSONoutput;
          return *this;
        }   
        // functions
        void                    fScript_Create                  (int intArg1, int intArg2, string strData);
        void                    fMessage_Create                 (int intArg1, int intArg2, int intLine, string strFile, string strFunction, Call_Data objCallData, 
                                                                 Text_Data objTextData, Port_Data objPortData, Freeswitch_Data objFreeswitchData, ALI_Data objALIdata, 
                                                                 string stringArg1, string stringArg2 ="", string stringArg3 = "", string stringArg4 = "", string stringArg5 = "", 
                                                                 string stringArg6 = "",string stringArg7 = "", message_type enMessageType = NORMAL_MSG,
                                                                 message_display_type enumMessageDisplayType = NORMAL_MESSAGE );
        void                    fConsole                        ();
        unsigned long long int  fLog                            (log_type enumLogType);
        int                     fEmail                          (email_type enumEmailType, bool boolCC = false, bool boolAttach = false, string strAttachment = "" );        
        void                    fSetSubject                     (string stringArg1);
        void                    fClearMsg                       ();
        void                    fUpdatePositionInMessageText    ();
        void                    fUpdateCallInfoInMessageText    ();
        string                  fPort_Down_Alarm_PopUp_Message	();
        string                  fBlind_Xfer_Unable_PopUp_Message();
        string                  fALI_DB_OTS_PopUp_Message          ();
        string                  fPort_ShutDown_Alarm_PopUp_Message ();
        string                  fNo_Correlation_Alarm_PopUp_Message();
        string                  fANI_Alarm_Code_PopUp_Message      ();
        string                  fTransfer_Cancelled_PopUp_Message  ();
        string                  fTransfer_Cancelled_PopUp_Message_Warning();
        string                  fConf_Xfer_Unable_Empty_Lot_Message();
        string                  fConf_Xfer_Unable_Valet_Lot_Message();
        string                  fConf_Xfer_Failed_PopUp_Message();
        string                  fDial_Out_Failed_PopUp_Message     ();
        string                  fTransfer_Dialed_PopUp_Message     ();
        string                  fTel_Equipment_PopUp_Message       ();
        string                  fAbandoned_Call_PopUp_Message      ();
        string                  fALI_Failed_PopUp_Message          ();
        string                  fUnable_to_Rebid_ALI_PopUp_Message ();
        void                    fAddLogXMLattribute                (string strAttr, string strValue);
         
};

class ExperientEmail : public FileMailer
{ 

 public:
  ExperientEmail(){ SetRuntimeLicense((char*)IPWORKS_RUNTIME_KEY);};
  virtual ~ExperientEmail(){};

   void fIntializeHeaders();
   int  fProcessEmailQueue ();
   bool fSendTestMail();
};



class ServerData
{
 public:
 string           ServerName;
 IP_Address       Sync_IP;
 IP_Address       Gateway_IP;
 bool             boolIsMaster;
 bool             boolIsActive;
 struct timespec  timespecTimeLastHeartbeat;
 Ping             pingServer;
 sem_t            sem_tTimedGatewayReplyFlag;

 ServerData(){};
~ServerData(){};
 

};





// external functions utilizing MessageClass
extern void             enQueue_Message     (threadorPorttype enumPortType, MessageClass objArg1);
extern IP_Address	CAD_PORT_REMOTE_HOST[NUM_CAD_PORTS_MAX+1];


class ExperientDataClass {

//friend class boost::serialization::access; 

public:
        bool                    boolForceDisconnect;
        bool                    boolSMSmessage;
        threadorPorttype        enumThreadSending;
        wrk_functions           enumWrkFunction;
        ALI_Data                ALIData;
        ani_system              enumANISystem;
        ani_functions           enumANIFunction;
        rcc_state               enumRCCstate;
        rcc_function            enumRCCfunction;
        LocationURI             objGeoLocation;
        string                  strNenaCallId;
        string                  strNenaIncidentId;
        string                  strRawCallinfoHeader;
        string                  strPidfloByValEntity;
        string                  strCallInfoProviderCID;
        string                  strCallInfoProviderURL;
        string                  strCallInfoServiceCID;
        string                  strCallInfoServiceURL;
        string                  strCallInfoSubscriberCID;
        string                  strCallInfoSubscriberURL;
        string                  strDTMFChar;
        Freeswitch_Data         FreeswitchData;
        int	                intNumberAutoALIBids;
        int                     intANIPortCallReceived;
        int                     intCallStatusCode;
        int                     intCallStateCode;
        string                  stringRingingTimeStamp;
        string                  stringConnectTimeStamp;
        string                  stringDisConnectTimeStamp;
        string                  stringANIAlarmCode;
        string                  stringANIAlarmMessage;
        struct timespec         timespecTimeRecordReceivedStamp;
        bool                    boolIsHeartbeat;
        bool                    boolConnectOutofOrder;
        bool                    boolRinging;
        bool                    boolDisconnect;
        bool                    boolConnect;
        bool                    boolRingBack;
        bool                    boolAbandoned;
        bool                    boolMSRPnoAnswer; 
        struct timespec         timespecTimeAbandonedStamp; 
        struct timespec         timespecAbandonedPopUp;
        struct timespec         timespecSingleChannel;
        struct timespec         timespecRinging;
        string                  strCallOriginator;
        string                  stringWRKStationIPAddr;
        string                  stringWRKStationListenPort;
        string                  stringWRKStationMSGText;
        int                     intWorkstation_TCPconnectionID;
        string                  strGUIVersion;
        string                  strGUIxmlVersion;
        string                  strWindowsUser;
        string                  strWindowsOS;
        int                     intLogType;
        int                     intLogMessageNumber;
        string                  stringXMLString;
        string                  stringTimeOfEvent;
        bool                    boolBroadcastXMLtoSinglePosition;
        bool                    boolReloadWorkstationGrid;
        bool                    boolWorkstationIsOnACall;
        int                     IndexOpenForDialing;
        int                     intWorkStation;
        bool                    boolDoNotSendToCAD;
        bool                    boolCADEraseMsg;
        string                  stringCADEraseMsg;
        int                     intCADEraseMsgLength;
        bool                    boolRecordRebuiltFlag;
        string                  strEventUniqueID;
        Window_and_Button_Data  WindowButtonData;      
        TDD_Data                TDDdata;
        Text_Data               TextData;
        Call_Data               CallData;
        PSAP_STATUS             PSAPstatus;
        vector <PSAP_STATUS>    vPSAPstatus;
        string                  strErrorMessage;
        string                  LastTest;
        

	// Declare Init Constructor for ExperientDataClass
	ExperientDataClass();
	
	// Declare Destructor  for ExperientDataClass
	~ExperientDataClass();
       
        // Declare Copy Constructor for ExperientDataClass
        ExperientDataClass(const ExperientDataClass *a);

        // Declare Assignment operator
        ExperientDataClass& operator=(const ExperientDataClass& a);


        void   fSetNENA_ID_Vars();
        void   fSetANIfunction(ani_functions eANIfunction);
        void   fSetANIfunction(ani_functions eANIfunction,transfer_method eTransfer_Method  );
        void   fSetANIfunction(ani_functions eANIfunction,string  strUUID );           
        void   ProcessTrunkZeroRecord();
        bool   fBidURI(int iTableIndex);
	void   fClear_Record();
        void   fClear_Active_Flags();
	void   fSetCadRecord();
        void   fSetNenaIds();
//        int    fReturnTransferNumberAsPositionNumber();
	void   fDisplay(int i);
        int    fInt_of_Trunk();
        int    fInt_of_Posn();
	int    fCAD_Xmit_ALI(int i);
        bool   fCad_ReXmit(int i, bool boolACKrequired);
        void   fSet_Time_Recieved();
	void   fSet_Heartbeat_Record();
        void   fSetEventUniqueID(int intTag);
        void   fSetUniqueCallID(int intTag);
        bool   fIs_A_Record();
        bool   fIs_HeartBeat();
        string fCadAliTextMessage();
        int    fSelectALiDatabase(int intOffset);
  //      void   fCheckPeerStatusAudiocodes(string strInput, int intPortNum);
        bool   fLoadCallbackfromALI(bool boolLegacyALItransfer=false);
        bool   fLoadCallbackfromALI(ALISteering objALISteering);
        bool   fCreateAliRequestKey(ali_format enumAliReq);
        bool   fLoad_CallState(threadorPorttype enumArg, string stringArg, string strXMLData);       
        bool   fLoad_FREESWITCH_Data(const char* charString, int intPortNum);
        int    fFindLocalTransferToPosition();
  //      bool   fLoad_Info_Code(data_format enumArg, bool boolNPD, string strRawData, string stringArg, int intPortNum);
        bool   fLoad_PhoneNumbers(data_format eArg, threadorPorttype eArg2, int iPortNum, string sArg, string sArg2 ="", string sArg3 ="", 
                                  string sArg4 ="", string strXMLData = "", bool boolLineRoll = false, bool NG911SIPNAC= false);
        bool   fLoad_CallbackNumber(string strTenDigitNumber);
        bool   fLoad_CallbackVerfied(threadorPorttype enumArg, string stringArg, string strXMLData);
        bool   fLoad_Trunk(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum,bool boolMaybeBlank = false, string strXMLData = "");
        void   fSet_Trunk(int intArg);
        bool   fLoad_Position(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData = "", bool boolMayBeBlank = false, bool boolMayBeZero = true);
        bool   fLoadTransferExtensionIntoPositionAndConference();
//        bool   fLoad_Blind_Transfer_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum);
//        bool   fLoad_Transfer_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum);
        bool   fLoad_Transfer_Type(threadorPorttype enumArg, string stringArg, string strRawData, int intPortNum);
        bool   fLoad_MSG_Type(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        bool   fLoad_MSG_Number(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        bool   fLoad_RCC_State(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        bool   fLoad_ALI_Exists(string stringArg);
        bool   fLoad_ALI_Type(string stringArg);
        bool   fLoad_Call_Id(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        bool   fLoad_DTMF_Char(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData); 
        bool   fLoad_Participant(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        bool   fLoad_ANIPort(threadorPorttype enumArg, string stringArg, string strXMLData);
        bool   fLoad_WRK_Listen_Port(threadorPorttype enumArg, string stringArg, string strXMLData);
        bool   fLoad_CallStatusCode(threadorPorttype enumArg, string StatusCode, string strXMLData );
        void   fUpdateGUILineViewOnly(int iLineNumber);
        void   fUpdateGUILineViewandLineNumber(int iLineNumber);
//        void   fSetANICommand(int intArg);
        string fLoad_InBound_XML_Message( const char* charString, int intWorkstation);
        string fXML_Load_CallData(XMLNode XMLarg, const char* charString);     // new
        string fXML_Load_ALI_Data(XMLNode XMLarg, const char* charString);
        string fXML_Load_Destination_Data(XMLNode XMLarg, const char* charString);
        bool   fLoad_SIP_PhoneNumbers(data_format enumArg, int intPort, string sArg, bool boolLineRoll,  bool boolNG_SIP_911_NON_AUDIOCODES = false);
        bool   fLoad_Conference_Positions(threadorPorttype enumArg, string stringArg,  int intPortNum, string strXMLData, bool boolLoadHistory=false);
        bool   fLoad_OnHold_Positions(threadorPorttype enumArg, string stringArg,  int intPortNum, string strXMLData);
        bool   fLoad_ALIState(threadorPorttype enumArg, string stringArg, string strXMLData);
        bool   fLoad_ConferenceData(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortorTrunk, bool boolMaybeBlank = false, string strXMLData = "");
        int    fFindMatchestoALIDatabases();
 //       void   fLoadPositioAndTransferNumberFromAsteriskData();
 //       bool   fUpdateChannelData();
        bool   		fLoad_ALI_Trunk_from_Position(string stringArg);
        bool   		fFindTelephoneNumber(string stringArg);
        bool   		fFindpANI(string stringArg);
        string 		fFindZeroTelno(string stringArg);
        string          fXML_Load_TDDdata(XMLNode XMLarg, const char* charString);
        string          fXML_Load_TextData(XMLNode XMLarg, const char* charString);
        bool   		fLoad_Encoding(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        bool   		fLoad_Decode_ALI(data_format enumArg, threadorPorttype enumArg2, string stringArg, int intPortNum, string strXMLData);
        void            fSetPANItoI3();
        bool   		fLoad_pANI(threadorPorttype enumArg, string stringArg, string strXMLData = "");
        bool   		fLoad_MSG_Text(data_format enumArg, threadorPorttype enumArg2, string stringArg, bool boolEncodedText=false);
        bool            fALI_ServiceBid_XML_Message();
        bool            fCallData_XML_Tag(XMLNode* nodeArg, int StatusCode, bool boolWithALI=false);
        bool            fStreamInfo_XML_Tag(XMLNode* nodeArg);
        bool   		fCallInfo_XML_Message(int StatusCode, bool boolWithALI=false);
        bool            fWindowData_XML_Tag(XMLNode*  WindowDataNode);
        bool            fPSAPappletData_XML_Tag(XMLNode* PSAPappletDataNode);
        bool            fPSAPappletData_XML_Message(string strIndex="");
        bool            fTDDdata_XML_Tag(XMLNode*  TDDdataNode, int intTDDcode);
        bool            fTextData_XML_Tag(XMLNode*  TextDataNode, int intMessagecode);
        bool            fConferenceData_XML_Tag(XMLNode*  ConferenceDataNode);
        bool   		fLoad_RingDateTime( string stringArg);
        bool   		fClear_Grid_XML_Message(int intPosition);
        void            fClear_Position_History();
        void   		fSetCadEraseMsg();
        int    		fCAD_Xmit_Erase(int i);
        void   		fCAD_Strip_Uneeded_Data();
   //     bool   		fCreateAsterisk_XML_Message(string strArg="");
        void            fAddSentenceToTDDConversation(string strInput);
        void            fAddSentenceToTextConversation(string strInput);
        void            fCheckWirelessRebid();
        bool            fCheckTDDbuffer();
        void            fLogTDDConversation();
        void            fLogTextConversation();
        unsigned long int fGenerateE2ALIRequest(unsigned char* chRequest, int* intLength, bid_type eRequestType);
        bool            fCreateE2ALIPhoneData();
        Call_Data 	fCallData();
 //       void            fUpdate_ASTERISK_Conference(Asterisk_Data objAsteriskData);
        void            fUpdate_FREESWITCH_ConferenceData(Freeswitch_Data objFreeswitchData,int i,int j, bool CalleridsAreReversed=false);
        void            fUpdate_FREESWITCH_ConferenceData(Channel_Data objChannelData);
        void            fUpdate_FREESWITCH_ConferenceData(Channel_Data objChannelData, int intTranVectorIndex);
        void            fUpdate_FREESWITCH_ConferenceDataNewPosition(Channel_Data objChannelData);
        void            fUpdateLineNumbersInTable(Channel_Data objChannelData);
        void            fUpdateDestChannelLineNumbersInTable(Channel_Data objChannelData);
        void            fCheckOnHoldMessage(string strTime);
        void            UPdate_CallData_Conference_Data_Vector();
        bool            VerifyCallBack();
        void            fUpdateTrunkTypeStringwithTDD();
        void            fResetTransferWindow(bool boolTransferOK, bool boolSendToSingleWorkstation = true);
        bool            fLoadCallOriginator(conference_member eConf, int i);
        string          fXML_MessageDeleteRecord();
        bool            fUpdateGUIXMLMessage();
        void            fCreateDisconnectResponceXML(bool boolResponse);
        void            fSetAbandoned();
        void            fSendHangup();
 //       void            fRefreshConferenceData();
 //       bool            fAddCloneChannel(string strChannel, string strUniqueID);
        void            fUpdateMASQChannels();
        PhoneBookEntry   CallerIDandNamefromConference();
        void             UpdatePSAPstatus();
        void             fLoadLegacyTDDobjectWithTextData();
        void             fTurnOnTDDmode();
        void             fUpdatePhoneALI();
        bool 		 fUpdateMuteButton(bool boolmuteactive);
        void             SetADRflag();
        string           ECRF_URN_Service_Request(string strService, bool bListService);
        bool             ShowRingDialogBox();
};



class Workstation_Groups
{
 public:

 vector <Workstation_Group> WorkstationGroups;

 int IndexOfGroupWithTrunk(int iTrunk);
 int IndexOfGroupWithWorkstation(int iPosition);

};

class WorkStation
{
// friend class boost::serialization::access;
 public:
 int	                intPosition;
 IP_Address             RemoteIPAddress;
 unsigned int           intRemotePortNumber;
 struct timespec        timespecTimeLastHeartbeat;
 bool                   boolActive;
 string                 strGUIVersion;
 string                 strGUIxmlVersion;
 string                 strWindowsUser;
 string                 strWindowsOS;
 string                 strLineViewFilter;
 bool                   boolContactRelayInstalled;
 int                    intRCCrelayPin;
 IP_Address             RCCremoteIPaddress;
 unsigned int           intRCCremotePortNumber;
 unsigned int           intRCClocalPortNumber;
 volatile bool          boolRCCconnected;
 volatile bool          boolRCCSendDelayedSignal;
 int                    intTCP_ConnectionID;


  //constructor
  WorkStation()  {
    intPosition 			= 0;
    RemoteIPAddress.fClear();
    intRemotePortNumber 		= 0;
    timespecTimeLastHeartbeat.tv_sec 	= 0;
    timespecTimeLastHeartbeat.tv_nsec 	= 0;
    boolActive				= false;
    strGUIVersion.clear();
    strGUIxmlVersion.clear();
    strWindowsUser.clear();
    strWindowsOS.clear();
    strLineViewFilter.clear();
    boolContactRelayInstalled           = false;
    intRCCrelayPin                      = 1;
    intRCCremotePortNumber              = 0;
    intRCClocalPortNumber               = 0;
    boolRCCconnected                    = false;
    boolRCCSendDelayedSignal            = false;
    intTCP_ConnectionID                 = 0;                         
   }
  //destructor
  ~WorkStation(){} 
  // assignment operator
 WorkStation& operator=(const WorkStation& b)
  {
    if (&b == this ) {return *this;}
    intPosition 			= b.intPosition;
    RemoteIPAddress                     = b.RemoteIPAddress;
    intRemotePortNumber 		= b.intRemotePortNumber;
    timespecTimeLastHeartbeat.tv_sec 	= b.timespecTimeLastHeartbeat.tv_sec;
    timespecTimeLastHeartbeat.tv_nsec 	= b.timespecTimeLastHeartbeat.tv_nsec;
    boolActive				= b.boolActive;
    strGUIVersion                       = b.strGUIVersion;
    strGUIxmlVersion                    = b.strGUIxmlVersion;
    strWindowsUser                      = b.strWindowsUser;
    strWindowsOS                        = b.strWindowsOS;
    strLineViewFilter                   = b.strLineViewFilter;
    boolContactRelayInstalled           = b.boolContactRelayInstalled;
    intRCCrelayPin                      = b.intRCCrelayPin;
    RCCremoteIPaddress                  = b.RCCremoteIPaddress;
    intRCCremotePortNumber              = b.intRCCremotePortNumber;
    intRCClocalPortNumber               = b.intRCClocalPortNumber;
    boolRCCconnected                    = b.boolRCCconnected;
    boolRCCSendDelayedSignal            = b.boolRCCSendDelayedSignal;
    intTCP_ConnectionID                 = b.intTCP_ConnectionID;    
   return *this;
  }

 void   fRegisterWorkStation(ExperientDataClass objData);
 void   fRefreshWorkStationList(struct timespec timespecTimeNow);
 void   fTransmit(const char* charData, int intLength, bool addheader=true);
 void   fTransmitMainWorktable(bool boolReset = false);
 string fUpdateMuteButton(string stringText);
 string fUpdateTransmitTime(string strInput);
 string fString_To_Transmit(const char* charData, int intLength);
 string fCreateAlert( string stringText, Call_Data objCallData, string stringIPAddress = "",  int intRemotePort = 0,  bool boolShutdown = false);
 bool   fCheckForDuplicatePosition(ExperientDataClass objData);
 void   fSendACK(ExperientDataClass objData);
 bool   fCheckUserNameChange(ExperientDataClass objData);
 string fDisplay();
 void   fInitXMLSend();




};

extern WorkStation                                     WorkStationTable[NUM_WRK_STATIONS_MAX+1];

class Socket_Property
{
 public:
 bool		 boolConnected;
 struct timespec tstimeConnected;
};

class ExperientTCP_SERVER_Port : public IPDaemon
{
 public:
  int 	                      intPortNum;
  threadorPorttype            enumPortType;
  int                         intRemotePortNumber;
  int                         intLocalPortNumber;
  Port_Data		      objPortData;
  Call_Data                   objCallData;
  IP_Address                  Remote_IP;
  volatile bool               boolPortActive;
  volatile bool               boolReadyToSend;
  volatile bool               boolPortWasDown;
  struct timespec             timespecBufferTimeStamp;
  sem_t                       sem_tMutexTCPPortBuffer;
  volatile bool               boolOutsideIPTraffic;
  string                      stringOutsideIPAddr;
  int                         ContentLength;
  string                      TCP_Buffer;
  int                         LastConnectionId;
  vector <Socket_Property>    vConnectionList;                               

 ExperientTCP_SERVER_Port()
  {
   ContentLength   = 0;
   boolPortActive  = true;
   boolPortWasDown = false;
   sem_init(&sem_tMutexTCPPortBuffer,0,1);
   clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);
  };

 virtual ~ExperientTCP_SERVER_Port(){}
//fred
 // assignment operator
 ExperientTCP_SERVER_Port& operator=(const ExperientTCP_SERVER_Port& a);


virtual int FireConnectionRequest(IPDaemonConnectionRequestEventParams *e)
{
 size_t       					iCount = 0;
 float                                          denominator;
 list <string>                                  lstIpaddresses;
 vector <int>                                   vDisconnectList;
 list<string>::iterator 			it;
 string                                         strIPaddress;
 int                                            iConnectionID = 0;
 struct timespec                                tsTimeConnected;
 bool                                           boolSecondsLessThan;
 bool						boolSecondsEqual;
 bool                                           boolNanoSecondsLessThan;
 MessageClass 					objMessage;
 extern Text_Data				objBLANK_TEXT_DATA;
 extern Call_Data				objBLANK_CALL_RECORD;
 extern Port_Data                               objBLANK_WRK_PORT;
 extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
 extern ALI_Data                                objBLANK_ALI_DATA;
 extern string                                  int2str(unsigned long long int intArg);                                     
/*
char* Address;
  int Port;
  int Accept;
*/
 denominator = (float) vConnectionList.size();
 lstIpaddresses.clear();
 vDisconnectList.clear();

 switch (enumPortType)
  {
   case CTI:
        //cout << "Connection request " << e->Accept << endl; //cout << "connections-> " << GetConnectionCount() << endl;
        break;
   case WRK:
      //   //cout << "Connection request " << e->Accept << endl; //cout << "connections-> " << GetConnectionCount() << endl;
        for (unsigned int i = 1; i < vConnectionList.size(); i++) 
        {
         if (vConnectionList[i].boolConnected){iCount++;lstIpaddresses.push_back(GetRemoteHost(i));}
        } 

        if (((float) (iCount/denominator)) > .50) 
         {
   //       //cout << "above 50%" << endl;
          lstIpaddresses.sort();       
          lstIpaddresses.unique();  

          //for each unique ip address
          for (it=lstIpaddresses.begin(); it!=lstIpaddresses.end(); ++it)
           {
            tsTimeConnected.tv_sec  = 0; 
            tsTimeConnected.tv_nsec = 0;
            // check connection list for IP match
            for (unsigned int j = 1; j < vConnectionList.size(); j++)              
             {
              if (!vConnectionList[j].boolConnected) {continue;}
              strIPaddress = GetRemoteHost(j);
              if (strIPaddress != *it) {continue;} 
              if (tsTimeConnected.tv_sec == 0) {tsTimeConnected =  vConnectionList[j].tstimeConnected; iConnectionID = j; continue;}
              boolSecondsEqual        = (tsTimeConnected.tv_sec == vConnectionList[j].tstimeConnected.tv_sec);
              boolSecondsLessThan     = (tsTimeConnected.tv_sec  < vConnectionList[j].tstimeConnected.tv_sec);               
              boolNanoSecondsLessThan = (tsTimeConnected.tv_nsec < vConnectionList[j].tstimeConnected.tv_nsec);

              if ((boolSecondsEqual&&boolNanoSecondsLessThan)||(boolSecondsLessThan)) 
               {
                //stored connection is older, put into disconnect vector, update stored connection
                vDisconnectList.push_back(iConnectionID);
                tsTimeConnected = vConnectionList[j].tstimeConnected; iConnectionID = j; 
               }
              else
               {
                // iterator connection is older, put into disconnect vector
                vDisconnectList.push_back(j);
               }

             }// for (unsigned int j = 1; j < vConnectionList.size(); j++)
 
           }// for (it=lstIpaddresses.begin(); it!=lstIpaddresses.end(); ++it)           


         //we now have a list of connections to set for disconnect ......
         for (unsigned int i = 0; i < vDisconnectList.size(); i++) 
          {
           Disconnect(vDisconnectList[i]); 
           objMessage.fMessage_Create(LOG_INFO, 737, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                      objBLANK_WRK_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_737, int2str(vDisconnectList[i]));
           enQueue_Message(WRK,objMessage);
          } 

         }// if (((float) (iCount/denominator)) > .10)
        break;
   default:
        break;
  }
 return 0;
}

virtual int FireConnected(IPDaemonConnectedEventParams *e)
{ 
/*
 typedef struct {
  int ConnectionId;
  int StatusCode;
  char* Description;
  int reserved;
*/
 string strMessage = "error on connectionid " + e->ConnectionId;
 strMessage += " code = ";
 strMessage += e->StatusCode;
 strMessage += " Desc: ";
 strMessage += e->Description;
 if (e->StatusCode) {cout << strMessage << endl;}

 //sem_wait(&sem_tMutexConnectionVector);
 if (e->ConnectionId >= (int) vConnectionList.size())
  {
   SendCodingError( "baseclasses.h - ExperientTCP_SERVER_Port::FireConnected -> ConnectionId out of range: id = " +int2strLZ(e->ConnectionId));
   return 0;
  }

 vConnectionList[e->ConnectionId].boolConnected = true;
 clock_gettime(CLOCK_REALTIME, &vConnectionList[e->ConnectionId].tstimeConnected);
 //sem_post(&sem_tMutexConnectionVector);


// //cout << "connecting ip =" << GetRemoteHost(e->ConnectionId) << " id = " << e->ConnectionId << endl;

 return 0;
}

virtual int FireDataIn(IPDaemonDataInEventParams *e)
{
/*
 typedef struct {
  int ConnectionId;
  char* Text;
  int EOL;
  int lenText;
  int reserved;
*/
  DataPacketIn 		DataRecieved;
  bool                  boolIPOK = false;
  int                   intRC;
  string                stringSourceAddress = "";
  string      		strXMLmessage = "";
  string                strNullfreeData;
  string                strTemp;
  string                strTempTwo;
  int                   intRemotePortNumber = 0;
  MessageClass          objMessage;
  size_t		start;
  size_t                end;
  size_t                size_tLength;
  bool                  boolSameConnectionID = false;
  bool                  boolSameIPaddress = false;
  unsigned long int     intMaxBufferSize;
  bool                  boolDelimeterFound = false;
  bool                  boolUserNameIsBlank = false;
  string                strUserName;

  extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
  extern ALI_Data                                objBLANK_ALI_DATA;
  extern Text_Data				 objBLANK_TEXT_DATA;
   sem_wait(&sem_tMutexTCPPortBuffer);

   stringSourceAddress     = GetRemoteHost(e->ConnectionId);
   intRemotePortNumber     = GetRemotePort(e->ConnectionId);

   if (stringSourceAddress.empty()) {/*SendCodingError( "baseclasses.h - ExperientTCP_SERVER_Port::FireDataIn -> blank IP address encountered");*/ TCP_Buffer.clear(); sem_post(&sem_tMutexTCPPortBuffer);return 0;} 

 //    //cout << e->ConnectionId << endl;
 //    //cout << e->EOL << endl;
 //    //cout << e->lenText << endl;
//     //cout << stringSourceAddress << endl;
//     //cout << intRemotePortNumber << endl;

     if(!e->EOL){ SendCodingError( "baseclasses.h - Unexpected non EOL Packet received in ExperientTCP_SERVER_Port::FireDataIn()");}


     switch (enumPortType)
      {
       case WRK:

            intRC = sem_wait(&sem_tMutexWRKWorkTable);
            if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in FireDataIn(a)", 1);}
            DataRecieved.intWorkstation = 0;
            TCP_Buffer += e->Text;
            strUserName = findWindowsUser(TCP_Buffer);


            for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
             {
              boolSameConnectionID = (e->ConnectionId == WorkStationTable[i].intTCP_ConnectionID);
              if(!stringSourceAddress.empty()) {boolSameIPaddress  = (stringSourceAddress == WorkStationTable[i].RemoteIPAddress.stringAddress);}

              if ((WorkStationTable[i].boolActive)&&((boolSameConnectionID )||(boolSameIPaddress)))
               {
                boolUserNameIsBlank = strUserName.empty();

                if ((!boolUserNameIsBlank)&&(!WorkStationTable[i].strWindowsUser.empty()))
                 {
         //         if ((strUserName != WorkStationTable[i].strWindowsUser)&&(!boolSameConnectionID)) {fSendKillSocket( WorkStationTable[i].intTCP_ConnectionID);}
                  if ((strUserName != WorkStationTable[i].strWindowsUser)&&(!boolSameConnectionID)) {                  cout << "different user !!!" << endl;}


                 }
                WorkStationTable[i].intTCP_ConnectionID = e->ConnectionId;
                if(!stringSourceAddress.empty()) { WorkStationTable[i].RemoteIPAddress.stringAddress = stringSourceAddress;}
                DataRecieved.intWorkstation = i;
                boolIPOK=true; 
                break;
               }
             }
            sem_post(&sem_tMutexWRKWorkTable);

            if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC))
             {
              if(DataRecieved.intWorkstation){objCallData.fLoadPosn(DataRecieved.intWorkstation);}
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 715, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                         WRK_MESSAGE_715, stringSourceAddress, 
                                         ASCII_String(e->Text,(size_t) e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
              enQueue_Message(WRK,objMessage);
             }
            if ((e->lenText <= WRK_XML_HEARTBEAT_MAX_STRINGLENGTH)&&(e->lenText >= WRK_XML_HEARTBEAT_MIN_STRING_LENGTH)){boolIPOK=true;}
            if  (e->lenText <  WRK_XML_MIN_STRING_LENGTH)                                                               {boolIPOK=false;}



            while (TCP_Buffer.length() > 0)
             {
              start = TCP_Buffer.find("<?xml");
              if (start == string::npos) { sem_post(&sem_tMutexTCPPortBuffer);return 0;}
              end   = TCP_Buffer.find("</Event>");
              if (end == string::npos) { sem_post(&sem_tMutexTCPPortBuffer);return 0;}
              if (end < start)         {sem_post(&sem_tMutexTCPPortBuffer);return 0;}

              strXMLmessage = TCP_Buffer.substr(start,(end - start + 8));

              TCP_Buffer.erase(0, (end + 8));

              //    if(!boolIPOK) {Disconnect(e->ConnectionId); return 0;}

              DataRecieved.intPortNum 	               = intPortNum;
              DataRecieved.intSideAorB 	               = 0;
              DataRecieved.stringDataIn                  = strXMLmessage;
              DataRecieved.intLength                     = DataRecieved.stringDataIn.length();
              DataRecieved.IPAddress.stringAddress       = stringSourceAddress;
              DataRecieved.intConnectionID               = e->ConnectionId;

              // send downrange
              intRC = sem_wait(&sem_tMutexWRKPortDataQ);
              if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKPortDataQ, "sem_wait@sem_tMutexWRKPortDataQ in WRK_TCPPort", 1);}
              queueWRKPortData.push(DataRecieved);
              sem_post(&sem_tMutexWRKPortDataQ);
               sem_post(&sem_tWRKFlag);
             }

            sem_post(&sem_tMutexTCPPortBuffer);
            break;

      case CAD:

           intMaxBufferSize = MAX_CAD_RECORD_SIZE * 100;
           
           if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC))
            {
             objMessage.fMessage_Create(LOG_CONSOLE_FILE, 474, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                        objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                        CAD_MESSAGE_474, stringSourceAddress, 
                                        ASCII_String(e->Text,(size_t) e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
             enQueue_Message(CAD,objMessage);
            }

           strNullfreeData = ReplaceNullCharacters(e->Text, e->lenText);
           size_tLength = strNullfreeData.length();
           if (size_tLength == 0){sem_post(&sem_tMutexTCPPortBuffer); return 1;} 
          
           TCP_Buffer += strNullfreeData;

           // check if buffer is getting too large !!!!!
           if (TCP_Buffer.length() > intMaxBufferSize)
            {
             objMessage.fMessage_Create(LOG_CONSOLE_FILE, 476, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                        objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                        CAD_MESSAGE_476, int2strLZ(TCP_Buffer.length()));
             enQueue_Message(CAD,objMessage);
             TCP_Buffer.erase(0,intMaxBufferSize/2);
            }
           

            boolDelimeterFound = false;

            do
            {
             strTemp.clear();
             boolDelimeterFound= false;
             strTemp =  fCheck_Delimiter(TCP_Buffer, boolDelimeterFound, charACK,charNAK);


             if (strTemp.length() <=  TCP_Buffer.length()){TCP_Buffer.erase(0,strTemp.length());}


    
             if (boolDelimeterFound)
              {             
               DataRecieved.intPortNum 	                = intPortNum;
               DataRecieved.intSideAorB 	        = 0;
               DataRecieved.stringDataIn                = strTemp;
               DataRecieved.intLength                   = strTemp.length();
               DataRecieved.boolStringBufferExceeded    = false;
               // send downrange
               fQueue_Transmission( DataRecieved );
             }

  
            } while ((strTemp.length() >= intMaxBufferSize)||(boolDelimeterFound));

          // data is less than buffer length and has no delimiter
          if (  (!strTemp.empty()) && (strTemp.length() < intMaxBufferSize)  )
          {
           TCP_Buffer=strTemp;
           clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);
          }
         sem_post(&sem_tMutexTCPPortBuffer);
         break;

      case CTI:
      //   //cout << "got a CTI packet" << endl;
      //   //cout <<  e->Text << endl;

         if(fisHTTP_Post( e->Text,  e->ConnectionId) > 0) { }

         SetConnected(e->ConnectionId, false);
         
   //    //cout << e->ConnectionId << endl;
 //    //cout << e->EOL << endl;
 //    //cout << e->lenText << endl;
//     //cout << stringSourceAddress << endl;
//     //cout << intRemotePortNumber << endl;        
       

       sem_post(&sem_tMutexTCPPortBuffer);
       break;

       default:
            break;
      }



 return 0;

}

virtual int FireDisconnected(IPDaemonDisconnectedEventParams *e)
{ 
/*
 typedef struct {
  int ConnectionId;
  int StatusCode;
  char* Description;
  int reserved;
*/
////cout << "disconnect ip =" << GetRemoteHost(e->ConnectionId) << " id = " << e->ConnectionId << endl;


// sem_wait(&sem_tMutexConnectionVector);

   vConnectionList[e->ConnectionId].boolConnected = false;

 // sem_post(&sem_tMutexConnectionVector);

 return 0;

} 

virtual int FireError(IPDaemonErrorEventParams *e)
{
/* 
 typedef struct {
  int ErrorCode;
  char* Description;
  int reserved;
*/
string strMessage = "Error Code: " ;
strMessage += int2strLZ(e->ErrorCode);
strMessage += " message: ";
strMessage += e->Description;
// //cout << "TCP error in baseclasses.h fn ExperientTCP_SERVER_Port::FireError() " << e->ErrorCode << " " << e->Description << endl;;
 if (e->ErrorCode) {SendCodingError( "baseclasses.h - ExperientTCP_SERVER_Port::FireError -> " +strMessage);}
 return 0;
} 

virtual int FireReadyToSend(IPDaemonReadyToSendEventParams *e)
{
/*
 typedef struct {
  int ConnectionId;
  int reserved;
*/

 LastConnectionId = e->ConnectionId;
 

// //cout << "ready to send" << endl;
 return 0;
} 


Port_Data   fPortData();
int         fSendKillSocket(int i);
void        fInitializeCADport(int i);
void        fQueue_Transmission(DataPacketIn DataReceived );
void        fCheckConnections();
void        fInitializeConnectionActiveVector(int i);
int         fConnectionsActive();
int         fisHTTP_Post(string strInput, int i);
};

class ExperientTCPPort : public IPPort
{
 public:
  int 	                      intPortNum;
  int                         intSideAorB;
  threadorPorttype            enumPortType;
  bool                        boolBufferPreviouslyCleared;
  vector<unsigned char>       vBuffer; 
  ali_format                  eRequestKeyFormat;
  string                      strTCP_Buffer;                    
  struct timespec             timespecBufferTimeStamp;
  sem_t                       sem_tMutexTCPPortBuffer;
  volatile bool               boolOutsideIPTraffic;
  string                      stringOutsideIPAddr;
  struct timespec             timespecTimePortDown;
  volatile bool               boolPortAlarm;
  volatile bool               boolPortActive;
  volatile bool               boolPortWasDown;
  volatile bool               boolIntialConnect;
  int                         intPortDownThresholdSec;
  long int                    intPortDownReminderThreshold;
  volatile bool               boolPingStatus;
  IP_Address                  Remote_IP;
  int                         intRemotePortNumber;
  int                         intLocalPortNumber;
  unsigned long long int      intBadCharacterCount;
  sem_t                       sem_tMutexBadCharacterCount;
  string                      strVendorEmailAddress;
  bool			      boolVendorEmailAddress;
  Port_Data		      objPortData;
  Call_Data                   objCallData;
  volatile bool               boolReadyToSend;
  int                         intIPDaemonConnectionID;
  MessageClass                objMessage;
  bool                        boolFreeswitchLoggedin;
  volatile unsigned int       ContentLength;
  Port_ALI_Protocol_Type      eALIportProtocol;
  bool                        boolRestartListenThread;
  int                         intLastStatusCode;
  

 ExperientTCPPort()
  {
   ContentLength   = 0;
   boolPortActive  = true;
   boolPortWasDown = false;
   boolIntialConnect = false;
   boolRestartListenThread = false;
   sem_init(&sem_tMutexTCPPortBuffer,0,1);
   clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);
  };
 virtual ~ExperientTCPPort()
  {
 //  sem_destroy(&sem_tMutexTCPPortBuffer);
  };
 // assignment operator
 ExperientTCPPort& operator=(const ExperientTCPPort& a);

virtual int FireDataIn(IPPortDataInEventParams *e)
  {
  unsigned long int          intMaxBufferSize;
  string                     stringSourceAddress;
  DataPacketIn               AMIDataPacket;
  DataPacketIn               ALIDataPacket;
  int                        intRC;
  MessageClass               objMessage;
  extern unsigned int        intMAX_ALI_RECORD_SIZE;
  extern Text_Data	     objBLANK_TEXT_DATA;  
  extern Call_Data           objBLANK_CALL_RECORD;
  extern Freeswitch_Data     objBLANK_FREESWITCH_DATA;
  extern ALI_Data            objBLANK_ALI_DATA;
  size_t                     found;
  size_t                     end;
  string                     strTAB = "";
  string		     strETX = "";
  bool                       boolContentPacket;
  bool                       boolRegistrationPacket;
  string                     strDataGram = "";
  string                     strContent_Length;
  string                     strNullfreeData;
  bool                       boolContentLengthHeaderPresent = false;

  extern string int2str(unsigned long long int);

  strTAB += charTAB; strETX += charETX;
  // typedef struct {
  //  char* Text;
  //  int EOL;
  //  int lenText;
  //  int reserved;
  

 //  //cout << e->Text << endl;

  // if not master ... ignore
  if(!boolIsMaster) {return 0;}

 switch (enumPortType)
  {
   case AMI:
            intRC = sem_wait(&sem_tMutexTCPPortBuffer);
            if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexTCPPortBuffer, "sem_wait@sem_tMutexTCPPortBuffer in FireDataIn()", 1);}
            clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);
          //    //cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
         //   //cout << e->Text << endl;
         //       //cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;

            if (!e->lenText)         {sem_post(&sem_tMutexTCPPortBuffer); return 0;}
            if (boolAMI_TEST_SCRIPT) {fReadAMIScript(e->Text); sem_post(&sem_tMutexTCPPortBuffer); return 0;} 
            
            strTCP_Buffer += e->Text;
            do
             {
              if (!ContentLength) //first look
               {
                found = strTCP_Buffer.find("Content-Length: ",0);
                if (found!= string::npos)
                 {
                  end = strTCP_Buffer.find_first_not_of("01234567890", found+16);
                  if (end != string::npos)
                   {
                    strContent_Length = strTCP_Buffer.substr(found+16, (end -(found+16)));
                    ContentLength =  char2int(strContent_Length.c_str());
                    boolContentLengthHeaderPresent = true;
                   }
                  else {strTCP_Buffer.clear();boolContentLengthHeaderPresent = false;} 
                 }
                else {boolContentLengthHeaderPresent = false;}
              
                if((!ContentLength)&&(strTCP_Buffer.length()))
                {
                 strDataGram = strTCP_Buffer;
                 strTCP_Buffer.clear();
                 strDataGram = FindandReplaceALL(strDataGram, "\r\n", "\n");
                 strDataGram = FindandReplaceALL(strDataGram, "\n\n", "\n");
                 strDataGram = FindandReplaceALL(strDataGram, "\n\n", "\n");               
                 strDataGram = FindandReplaceALL(strDataGram, "\n", strTAB);
                 if (strDataGram[strDataGram.length()-1] != strTAB[0]){strDataGram += strTAB;}
                 strDataGram += strETX;
                }
               else
                {
                 found = strTCP_Buffer.find("\n\n",found);
                 if (found == string::npos) { strTCP_Buffer.clear(); ContentLength = 0; sem_post(&sem_tMutexTCPPortBuffer); return 0;}
                 strTCP_Buffer.erase(0, found+2);
                 
                 if (strTCP_Buffer.length() < ContentLength) {sem_post(&sem_tMutexTCPPortBuffer); return 0;}
                 strDataGram = strTCP_Buffer.substr(0, ContentLength);
                 strTCP_Buffer.erase(0,ContentLength);
                 ContentLength = 0;
                 strDataGram = FindandReplaceALL(strDataGram, "\r\n", "\n");
                 strDataGram = FindandReplaceALL(strDataGram, "\n\n", "\n");
                 strDataGram = FindandReplaceALL(strDataGram, "\n\n", "\n");               
                 strDataGram = FindandReplaceALL(strDataGram, "\n", strTAB);       
                 if (strDataGram[strDataGram.length()-1] != strTAB[0]){strDataGram += strTAB;}
                 strDataGram += strETX; 
                }
               }// end if  (!ContentLength) //first look
              else
               {
                if (strTCP_Buffer.length() < ContentLength) {sem_post(&sem_tMutexTCPPortBuffer); return 0;}
                strDataGram = strTCP_Buffer.substr(0, ContentLength);
                strTCP_Buffer.erase(0,ContentLength);
                ContentLength = 0;
                strDataGram = FindandReplaceALL(strDataGram, "\r\n", "\n");
                strDataGram = FindandReplaceALL(strDataGram, "\n\n", "\n");
                strDataGram = FindandReplaceALL(strDataGram, "\n\n", "\n");               
                strDataGram = FindandReplaceALL(strDataGram, "\n", strTAB);       
                if (strDataGram[strDataGram.length()-1] != strTAB[0]){strDataGram += strTAB;}
                strDataGram += strETX; 
               }



              AMIDataPacket.intPortNum 	                  = 1;
              AMIDataPacket.intSideAorB 	          = 0;
              AMIDataPacket.stringDataIn                  = strDataGram;
              AMIDataPacket.intLength                     = strDataGram.length();
              AMIDataPacket.IPAddress.stringAddress       = strASTERISK_AMI_IP_ADDRESS;
              boolContentPacket      = IsCLIContentTypePacket(strDataGram);
              boolRegistrationPacket = IsCLIregistrationPacket(strDataGram);
              if ((boolAMI_RECORD_SCRIPT)&&(!boolRegistrationPacket)&&(!boolContentPacket))
               {
                objMessage.fScript_Create(LOG_AMI_SCRIPT ,809, AMITestScript(strDataGram));
                enQueue_Message(AMI,objMessage);
          //    //cout << AMITestScript(strTCP_Buffer) << endl;
               }
              if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC))
               {
                if ((boolSHOW_CLI_REGISTRATION_PACKETS)|| ((!boolRegistrationPacket)&&(!boolContentPacket))    )
                 {
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 807, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA,
                                             objBLANK_ALI_DATA,AMI_MESSAGE_807," ", 
                                             ASCII_String(strDataGram.c_str(), strDataGram.length()),"","","","", DEBUG_MSG, NEXT_LINE);
                  enQueue_Message(AMI,objMessage);
                 }        
               }
              if (!boolContentPacket)
               {
                // send downrange to ANI
                intRC = sem_wait(&sem_tMutexANIPortDataQ);
                if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIPortDataQ, "sem_wait@sem_tMutexANIPortDataQ in ANI_TCPPort", 1);}
                queueANIPortData[1].push(AMIDataPacket);
                sem_post(&sem_tMutexANIPortDataQ);
                sem_post(&sem_tANIFlag);
               }                 

            } while (boolContentLengthHeaderPresent);
           sem_post(&sem_tMutexTCPPortBuffer);
           break; 

   case ALI:
        // ignore data arriving if port shut down
        if (!boolPortActive)
         {
          fIncrementBadCharacterCounter(e->lenText);
          return 0;
         }

         stringSourceAddress = GetRemoteHost();
         clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);
         intMaxBufferSize = intMAX_ALI_RECORD_SIZE * 100 + intALI_COMM_IN_BUFFFER_SIZE;
        
        

        switch (eALIportProtocol)
         {
          case  ALI_E2:
                // check Buffer size
                if ((vBuffer.size() + e->lenText) > intMaxBufferSize) {fCallParentEmergencyPortShutDown("Buffer Exceeded", vBuffer.size() + e->lenText); return 0;}
                if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC))
                 {
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 252, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                             ALI_MESSAGE_252,  stringSourceAddress, 
                                             HEX_String((unsigned char*) e->Text, e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
                  enQueue_Message(ALI,objMessage);
                 }        
                sem_wait(&sem_tMutexTCPPortBuffer);
                clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);
                fAddToBuffer( e->Text, e->lenText);
                fProcessE2TCPbuffer();
                sem_post(&sem_tMutexTCPPortBuffer); 
                break;
   
          case ALI_MODEM:
               // check Buffer size
                if ((strTCP_Buffer.length() + e->lenText) > intMaxBufferSize) {fCallParentEmergencyPortShutDown("Buffer Exceeded", strTCP_Buffer.length() + e->lenText); return 0;}
                if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC))
                 {
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 252, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                             ALI_MESSAGE_252,  stringSourceAddress, 
                                             ASCII_String(e->Text,(size_t) e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
                  enQueue_Message(ALI,objMessage);
                 }    
                intRC = sem_wait(&sem_tMutexTCPPortBuffer);
                clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);
                strNullfreeData = ReplaceNullCharacters(e->Text, e->lenText);
                strTCP_Buffer += strNullfreeData;   
                fProcessALITCPbuffer(stringSourceAddress);               

                intRC = sem_post(&sem_tMutexTCPPortBuffer);
                break;

          case ALI_SERVICE:
                if ((strTCP_Buffer.length() + e->lenText) > intMaxBufferSize) {fCallParentEmergencyPortShutDown("Buffer Exceeded", strTCP_Buffer.length() + e->lenText); return 0;}
                if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC))
                 {
                  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 252, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                             objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_252,  stringSourceAddress, 
                                             ASCII_String(e->Text,(size_t) e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
                  enQueue_Message(ALI,objMessage);
                 } 
                intRC = sem_wait(&sem_tMutexTCPPortBuffer);
                clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);
                strNullfreeData = ReplaceNullCharacters(e->Text, e->lenText);
                strTCP_Buffer += strNullfreeData;
                fProcessALIServiceTCPbuffer(stringSourceAddress);
                sem_post(&sem_tMutexTCPPortBuffer); 
                break;
          default:
              SendCodingError("baseclasses.h - coding error ExperientTCPPort::FireDataIn No ALI protocol defined");
              break;
         }

        break;
   case RCC:
        if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC))
         {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 912, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData,objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_912," ", 
                                     ASCII_String(e->Text,(size_t) e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
          enQueue_Message(RCC,objMessage);
         }
        clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);        
        if (!this->boolIntialConnect) {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 919, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData,objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_919, this->GetRemoteHost(), int2str(this->GetRemotePort()) );
          enQueue_Message(RCC,objMessage);
         this->boolIntialConnect = true;
        }
        fSet_Port_Active();
         
        break;
  case DSB:
        stringSourceAddress = GetRemoteHost();
        if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC))
         {
          objMessage.fMessage_Create(LOG_CONSOLE_FILE, 662, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData,objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_662, stringSourceAddress, 
                                     ASCII_String(e->Text,(size_t) e->lenText),"","","","", DEBUG_MSG, NEXT_LINE);
          enQueue_Message(DSB,objMessage);
         }        
        fSet_Port_Active();
        intRC = sem_wait(&sem_tMutexTCPPortBuffer);
        if (intRC){Semaphore_Error(intRC, AMI, &sem_tMutexTCPPortBuffer, "sem_wait@sem_tMutexTCPPortBuffer in FireDataIn()", 1);}
         clock_gettime(CLOCK_REALTIME, &this->timespecBufferTimeStamp);
         strNullfreeData = ReplaceNullCharacters(e->Text, e->lenText);
         strTCP_Buffer += strNullfreeData;
         fProcessDashboardTCPbuffer(stringSourceAddress);

        sem_post(&sem_tMutexTCPPortBuffer);        
        break;
   default: break;
  }
 return 0;
}


virtual int FireConnectionStatus(IPPortConnectionStatusEventParams *e)
 {
  int i;
  MessageClass          			 objMessage;
  extern Text_Data				 objBLANK_TEXT_DATA;
  extern Call_Data       			 objBLANK_CALL_RECORD;  
  extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
  extern ALI_Data                                objBLANK_ALI_DATA;

  if (boolSTOP_EXECUTION){return 0;} 

  switch (e->StatusCode)
   {   
    case 0: 
     this->intLastStatusCode = 0;
     return 0;
    default:
            i=0;
     
            switch (enumPortType)
              {
               case ALI:
                    // //cout << "ALI Port " << intPortNum << " Side " << intSideAorB << " TCP Status Code=" << e->StatusCode << " " << e->Description << endl;
                    break;
               case RCC:
                      if ((e->StatusCode == this->intLastStatusCode)&&(!boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC)) {
                       break;
                      }
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE, 906, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objPortData, objBLANK_FREESWITCH_DATA,
                                                 objBLANK_ALI_DATA,RCC_MESSAGE_906, int2strLZ(e->StatusCode), e->Description );
                      enQueue_Message(RCC,objMessage); 
                      break;
               case AMI:
                      if ((e->StatusCode == this->intLastStatusCode)&&(!boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC)) {
                       break;
                      }
                      objMessage.fMessage_Create(LOG_CONSOLE_FILE, 804, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objPortData, objBLANK_FREESWITCH_DATA,
                                                 objBLANK_ALI_DATA,AMI_MESSAGE_804, int2strLZ(e->StatusCode), e->Description );
                      enQueue_Message(AMI,objMessage); 
                      break;
              case DSB:
                      if ((e->StatusCode == this->intLastStatusCode)&&(!boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC)) {
                       break;
                      }                      objMessage.fMessage_Create(LOG_CONSOLE_FILE, 656, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                                 objPortData, objBLANK_FREESWITCH_DATA,
                                                 objBLANK_ALI_DATA,DSB_MESSAGE_656, int2strLZ(e->StatusCode), e->Description );
                      enQueue_Message(DSB,objMessage); 
                      break;
               default: break;
              }
   
   }
  this->intLastStatusCode = e->StatusCode;
  return 0;
  } 


 virtual int FireDisconnected(IPPortDisconnectedEventParams *e)
  {
   MessageClass           			  objMessage;
   extern Text_Data				  objBLANK_TEXT_DATA;
   extern Call_Data       			  objBLANK_CALL_RECORD;
   extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
   extern ALI_Data                                objBLANK_ALI_DATA;
   int                    			  intLogCode = LOG_WARNING * boolPortActive;
   boolReadyToSend = false;

   if (boolSTOP_EXECUTION){return 0;} 

   switch (enumPortType)
    {
     case ALI:
       //  //cout << "ALI Port " << intPortNum << " Side " << intSideAorB << " TCP Status: Disconnect, Code=" << e->StatusCode << " " << e->Description << endl;
          break;
     case RCC:
          intLogCode *= (!boolPortWasDown);
          objMessage.fMessage_Create(intLogCode, 902, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                     RCC_MESSAGE_902, int2strLZ(e->StatusCode), e->Description );
          enQueue_Message(RCC,objMessage);
          break;
     case AMI:
          boolFreeswitchLoggedin    = false;
          objMessage.fMessage_Create(LOG_WARNING, 802, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                     AMI_MESSAGE_802, int2strLZ(e->StatusCode), e->Description );
          enQueue_Message(AMI,objMessage);
          break; 
     case DSB:
          boolFreeswitchLoggedin    = false;
          objMessage.fMessage_Create(LOG_WARNING, 657, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                     DSB_MESSAGE_657, int2strLZ(e->StatusCode), e->Description );
          enQueue_Message(DSB,objMessage);
          break; 
     default: break;
    }
 
   return 0;
  } 


 virtual int FireReadyToSend(IPPortReadyToSendEventParams *e)
  {
   MessageClass           			  objMessage;
   extern Text_Data				  objBLANK_TEXT_DATA;
   extern Call_Data       			  objBLANK_CALL_RECORD;
   extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
   extern ALI_Data                                objBLANK_ALI_DATA;
   int                    			  intLogCode = LOG_INFO * boolPortActive;

   boolReadyToSend = true;
   switch (enumPortType)
    {
     case ALI:
      //    //cout << "ALI Port " << intPortNum << " Side " << intSideAorB << " TCP Status:Ready to Send"  << endl;
       break;
     case AMI:
          objMessage.fMessage_Create(LOG_INFO, 808, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, AMI_MESSAGE_808 );
          enQueue_Message(AMI,objMessage);
          break;
     case RCC:
          intLogCode *= (!boolPortWasDown);
          objMessage.fMessage_Create(LOG_INFO, 903, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_903 );
          enQueue_Message(RCC,objMessage);
       break;
     case DSB:
          objMessage.fMessage_Create(LOG_INFO, 658, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                     objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_658 );
          enQueue_Message(DSB,objMessage);
          break;
     default: break;
    }
   return 0;
  }


virtual int FireError(IPPortErrorEventParams *e)
  {
   // typedef struct {
   //  int ErrorCode;
   //  char* Description;
   // int reserved;
   string strMessage = Thread_Calling(this->enumPortType).erase(4);
   strMessage += int2strLZ(this->intPortNum);   
   strMessage += " Error Code: " ;
   strMessage += int2strLZ(e->ErrorCode);
   strMessage += " message: ";
   strMessage += e->Description;
// //cout << "TCP error in baseclasses.h fn ExperientTCP_SERVER_Port::FireError() " << e->ErrorCode << " " << e->Description << endl;;
   if (e->ErrorCode) {SendCodingError( "baseclasses.h - ExperientTCP_Port::FireError -> " +strMessage);}
   return 0;
  }

void        fInitializeRCCport(int i);
void        fInitializeALIPort(int i, int j);
void        fInitializeAMIport(int i);
void        fInitializeECATSport(int i);
void        fInitializeDSBport();
void        ConnectToALIServer();
void        ConnectToRCCdevice(int i=0);
void        fIncrementBadCharacterCounter(unsigned long long int i = 1);
Port_Data   fPortData();
Port_Data   fPortPairData();    
void        fAddToBuffer( const char* chInput, size_t intLength);
void        fEraseBuffer(size_t Position, size_t Length, int intError, string strSection);
void        fProcessE2TCPbuffer();
void        fProcessALITCPbuffer(string strAddress);
void        fProcessALIServiceTCPbuffer(string strAddress);
void        fProcessDashboardTCPbuffer(string stringSourceAddress);
void        fQueueE2ALIData(DataPacketIn Data);
bool        fPingStatus();
void        fSet_Port_Down();
void        fSet_Port_Active();
string      fPingStatusString();
void        fCallParentEmergencyPortShutDown(string stringArg, unsigned long long int intArg);
void        fCheckTimeSinceLastRX();
void        fReadAMIScript(string strText);
void        fShutDownPort();
};



class ExperientHTTPPort : public HTTP 
{
 public:
  int    			intPortNumber;
  string 			strBuffer;
  threadorPorttype            	enumPortType;
  DataPacketIn                  objDataReceived;
  sem_t                         sem_tWaitForData;
  string                        strRequestID;
  string                        strHeader;
  int                           iHeaderCount;


  ExperientHTTPPort(){sem_init(&sem_tWaitForData,0,0);}
  virtual ~ExperientHTTPPort(){sem_destroy(&sem_tWaitForData);}


 virtual int FireLog(HTTPLogEventParams *e) {
  /*
  typedef struct {
   int LogLevel;
   char* Message;
   char* LogType;
   int reserved;
  */
  // //cout << "Type -> " << e->LogType << " Message -> " << e->Message << endl;
  return 0;
 }
 // used for debug
 virtual int FireConnected(HTTPConnectedEventParams *e)
  { 
//   //cout << "CONNECTED -> Code: " << e->StatusCode << " Description: " << e->Description << endl;
   return 0;
  }

 virtual int FireConnectionStatus(HTTPConnectionStatusEventParams *e)
  {
 //  //cout << "Connection STATUS -> " << e->ConnectionEvent << " Code = " << e->StatusCode << " Description: " << e->Description << endl;
   return 0;
  }

 virtual int FireDisconnected(HTTPDisconnectedEventParams *e)
  { 
 //  //cout << "DISCONNECTED -> Code: " << e->StatusCode << " Description: " << e->Description << endl;
   return 0;
  }
   
 virtual int FireEndTransfer(HTTPEndTransferEventParams *e)
  {

   switch (e->Direction)
    {
     case 0: break;
     case 1: 
             switch (strBuffer.length())
              {
               case 0: break; //this is normal ......

               default:
                       
 
                       switch (enumPortType)
                        {
                         case CTI:
                            // if (strBuffer == "Push Message will be displayed successfully") { strBuffer.clear(); break;}
                              break;

                         case LIS:
                         // here the transmission ended without sending the buffer ...
                         // No content length header was provided ..... NGA911 SSL
                               objDataReceived.intPortNum     = this->intPortNumber;
                               objDataReceived.intWorkstation = 0;
                               objDataReceived.intSideAorB    = 0;
                               objDataReceived.intLength      = strBuffer.length();
                               objDataReceived.stringDataIn   = strBuffer;
                               objDataReceived.boolStringBufferExceeded = false;
                               strBuffer.clear();
                               objDataReceived.strRequestID = this->strRequestID;

                               fQueue_Transmission(objDataReceived );
                              break;

                         default: break;
                        }
              } // switch (strBuffer.length())
             break;


     default:
           break; 
    }
  // //cout << " End Transfer" << endl;

   return 0;
  } 



 virtual int FireError(HTTPErrorEventParams *e)
  {
   //cout << "HTTP Error -> Code: " << e->ErrorCode << " Description: " << e->Description << endl;
   return 0;
  } 
                     
 virtual int FireHeader(HTTPHeaderEventParams *e)
  {
   ////cout << "header -> Field: " << e->Field << " Value: " << e->Value << endl; 
   return 0;
  }

 virtual int FireRedirect(HTTPRedirectEventParams *e) 
  {
 //  string strAccept;

//   switch(e->Accept)
 //  {
 //   case 0:  strAccept = "false";      break;
//    case 1:  strAccept = "true";       break;
////    default: strAccept = "undefined";
 //  }
//   //cout << "Redirect to: " << e->Location << " Accepted: " << strAccept << endl; 
   return 0;
  }

 virtual int FireSetCookie(HTTPSetCookieEventParams *e)
  {
/*   string strSecure;

   switch(e->Secure)
   {
    case 0:  strSecure = "false";      break;
    case 1:  strSecure = "true";       break;
    default: strSecure = "undefined";
   }  
   //cout << "Cookie Set" << endl;
   //cout << "Name:    " << e->Name << endl;
   //cout << "Value:   " << e->Value << endl;
   //cout << "Expires: " << e->Expires << endl;
   //cout << "Domain:  " << e->Domain << endl;
   //cout << "Path:    " << e->Path << endl;
   //cout << "Secure:  " << strSecure << endl; */
   return 0;
  }

 virtual int FireStartTransfer(HTTPStartTransferEventParams *e)
  {
 /*  switch (e->Direction)
    {
     case 0: //cout  << "Client";break;
     case 1: //cout  << "Server";break;
     default: //cout << "Unknown:"; 
    }
   //cout << " Start Transfer" << endl;*/
   return 0;
  }

 virtual int FireStatus(HTTPStatusEventParams *e)
  {
 //  //cout << "Ver: " << e->HTTPVersion << " Code: " << e->StatusCode << " Description: " << e->Description << endl;
   return 0;
  }

#ifdef IPWORKS_V16
 virtual int FireSSLServerAuthentication(HTTPSSLServerAuthenticationEventParams *e)
  {
   e->Accept = true;
   //cout << "SSL certificate accepted " << endl;
   /*
   if (e->Accept) return 0;
    printf("Server provided the following certificate:\nIssuer: %s\nSubject: %s\n",
	   e->CertIssuer, e->CertSubject);
    printf("The following problems have been determined for this certificate: %s\n", e->Status);
    printf("Would you like to continue anyways? [y/n] ");
    if (getchar() == 'y') e->Accept = true;
    else exit(0);
    */
    return 0;
  }
#endif

 virtual int FireTransfer(HTTPTransferEventParams *e)                 
  {

  switch (e->Direction)
    {
  //   case 0: //cout  << "Client";break;
  //   case 1: //cout  << "Server";break;
 //    default: //cout << "Unknown:"; 
    }
//   //cout << " transfer bytes: " << e->BytesTransferred << " Pct: " << e->PercentDone << " length: " << e->lenText <<  endl;
//   //cout << "text ->" << endl;
 //  //cout << e->Text << endl;


  strBuffer += e->Text;

  switch (e->PercentDone)
   {
    case 100:

         objDataReceived.intPortNum     = this->intPortNumber;
         objDataReceived.intWorkstation = 0;
         objDataReceived.intSideAorB    = 0;
         objDataReceived.intLength      = strBuffer.length();
         objDataReceived.stringDataIn   = strBuffer;
         objDataReceived.boolStringBufferExceeded = false;
         objDataReceived.strRequestID = this->strRequestID;

         switch (enumPortType)
          {
           case CTI:
                if (strBuffer == "Push Message will be displayed successfully") { cout << "got reply" << endl; strBuffer.clear(); break;}
                
                break;

           case LIS:
                strBuffer.clear();
                fQueue_Transmission(objDataReceived );
                break;

           default: break;
          }
    default:
         switch (enumPortType)
          {
           case CTI:
               strBuffer.clear();
               break;
           default: break;
          }

   } 


  return 0; 
 }
   
void fQueue_Transmission(DataPacketIn DataReceived );
};




class ExperientUDPPort : public MCast {

// this class overrides the functions in the library class MCast in ipworks
// and adds some tags to identify port numbers and attributes

 
  public:
    
    int 	                intPortNum;
    int                         intSideAorB;
    threadorPorttype            enumPortType;
    ani_system			enumANISystem;
    bool                        boolBufferPreviouslyCleared;
    string                      stringBuffer;
    string                      stringHalfBuffer;
    struct timespec             timespecBufferTimeStamp;
 //   sem_t                       sem_tMutexUDPPortBuffer;  
    volatile bool               boolOutsideIPTraffic;
    string                      stringOutsideIPAddr;
    struct timespec             timespecTimePortDown;
    volatile bool               boolPortAlarm;
    volatile bool               boolPortActive;
    volatile bool               boolPortWasDown;
    int                         intPortDownThresholdSec;
    long int                    intPortDownReminderThreshold;
    volatile bool               boolPingStatus;
    IP_Address                  Remote_IP;
    unsigned long long int      intDiscardedPackets;
    sem_t                       sem_tMutexDiscardedPackets;
    string                      strVendorEmailAddress;
    bool			boolVendorEmailAddress;
    Port_Data			objPortData;
    bool                        boolAudiocodesRegistered;
    sem_t                       sem_tPortLock;

    // constructor
    ExperientUDPPort()
     {
      sem_init(&sem_tPortLock,0,1);
      sem_init(&sem_tMutexDiscardedPackets,0,1);
 //     sem_init(&sem_tMutexUDPPortBuffer,0,1); 
     }

    // destructor
    virtual ~ExperientUDPPort(){};

    // assignment operator
    ExperientUDPPort& operator=(const ExperientUDPPort& a);

    // functions
    void   Error_Handler(int intErrorCode = 0 , string stringErrorMessage = "");
    void   fQueue_Transmission(DataPacketIn UDPPortDataRecieved);
    void   fCheck_Buffer();
    void   fCheck_Time_Port_Down();
    void   fSet_Port_Down();
    bool   fPingStatus();
    string fPingStatusString();
    void   fIncrementDiscardedPacketCounter();
    void   fCheckResetDiscardedPacketCounter();
    void   fInitializeALIPort(int i, int j);
    void   fInitializeCADport(int i, bool boolSKIPACTIVE = false);
    void   fInitializeANIport(int i);
    Port_Data   fPortData();
    Port_Data   fPortPairData();    

    virtual int FireReadyToSend(MCastReadyToSendEventParams *e){
     // //cout << "ready to send\n";
     return 0;
    }

    virtual int FireError(MCastErrorEventParams *e){
    //    //cout << "FIRE ERROR............................................" << endl;     
      Error_Handler(e->ErrorCode, e->Description);
     return 0;
    }

   virtual int FireDataIn(MCastDataInEventParams *e){
     DataPacketIn 		UDPPortDataRecieved;
     int                        intRC;
     string                     strNullfreeData     = "";
     string                     stringTemp          = "";
     string                     stringTemp2         = "";
     string                     stringSourceAddress = "";
     bool                       boolDelimeterFound;
     int                        intSize;
     size_t                     size_tLength, start, end;
     unsigned int               intMaxBufferSize;
     bool                       boolIPOK; 
     MessageClass               objMessage;
     Call_Data                  objCallData;
     string			strSideAorB;
     string                     strXMLmessage;
     string                     strUserName;

     extern unsigned int        			intMAX_ALI_RECORD_SIZE;
     extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
     extern ALI_Data                                	objBLANK_ALI_DATA;
     extern Text_Data				        objBLANK_TEXT_DATA;
     // ignore data arriving outside of window
     if (!boolPortActive)
      {
       fIncrementDiscardedPacketCounter();
       return 0;
      }
     // lock the port
     intRC = sem_wait(&sem_tPortLock);
     if (intRC){Semaphore_Error(intRC, enumPortType, &sem_tPortLock, "sem_wait@sem_tPortLock in FireDataIn(a)", 1);}
     // check Data recived from correct ip address

 //    if ( e->SourceAddress == NULL) {return 0;}
     stringSourceAddress  = e->SourceAddress;    
     boolIPOK = false;
     switch (enumPortType)
      {
       case WRK:
            intRC = sem_wait(&sem_tMutexWRKWorkTable);
            if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in FireDataIn(a)", 1);}
            UDPPortDataRecieved.intWorkstation = 0;
            for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
             {
              if ((WorkStationTable[i].boolActive)&&(stringSourceAddress.compare(WorkStationTable[i].RemoteIPAddress.stringAddress)== 0))
               {
                UDPPortDataRecieved.intWorkstation = i;
                boolIPOK=true; 
                break;
               }
             }
            sem_post(&sem_tMutexWRKWorkTable);

            if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC))
             {
              if(UDPPortDataRecieved.intWorkstation){objCallData.fLoadPosn(UDPPortDataRecieved.intWorkstation);}
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 715, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, WRK_MESSAGE_715,  stringSourceAddress, 
                                         ASCII_String(e->Datagram,(size_t) e->lenDatagram),"","","","", DEBUG_MSG, NEXT_LINE);
              enQueue_Message(WRK,objMessage);
             }
            if ((e->lenDatagram <= WRK_XML_HEARTBEAT_MAX_STRINGLENGTH)&&(e->lenDatagram >= WRK_XML_HEARTBEAT_MIN_STRING_LENGTH)){boolIPOK=true;}
            if  (e->lenDatagram <  WRK_XML_MIN_STRING_LENGTH)                                                                   {boolIPOK=false;}


            
            UDPPortDataRecieved.intWorkstation = 0;
            stringBuffer += ReplaceNullCharacters(e->Datagram, e->lenDatagram);;
            strUserName = findWindowsUser(stringBuffer);

            while (stringBuffer.length() > 0)
             {
              start = stringBuffer.find("<?xml");
              if (start == string::npos) { sem_post(&sem_tPortLock);return 0;}
              end   = stringBuffer.find("</Event>");
              if (end == string::npos) { sem_post(&sem_tPortLock);return 0;}
              if (end < start)         { sem_post(&sem_tPortLock);return 0;}

              strXMLmessage = stringBuffer.substr(start,(end - start + 8));

              stringBuffer.erase(0, (end + 8));

              //    if(!boolIPOK) {Disconnect(e->ConnectionId); return 0;}

              UDPPortDataRecieved.intPortNum 	               = intPortNum;
              UDPPortDataRecieved.intSideAorB 	               = 0;
              UDPPortDataRecieved.stringDataIn                  = strXMLmessage;
              UDPPortDataRecieved.intLength                     = UDPPortDataRecieved.stringDataIn.length();
              UDPPortDataRecieved.IPAddress.stringAddress       = stringSourceAddress;


              // send downrange
              intRC = sem_wait(&sem_tMutexWRKPortDataQ);
              if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKPortDataQ, "sem_wait@sem_tMutexWRKPortDataQ in WRK_UDPPort", 1);}
              queueWRKPortData.push(UDPPortDataRecieved);
              sem_post(&sem_tMutexWRKPortDataQ);
               sem_post(&sem_tWRKFlag);
             }

            sem_post(&sem_tPortLock); 
            return 0;
            
       case CAD:
            if ((stringSourceAddress.compare(Remote_IP.stringAddress) == 0))             {boolIPOK=true;}
            if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC))
             {
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 474, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_474, stringSourceAddress, 
                                         ASCII_String(e->Datagram, (size_t)e->lenDatagram),"","","","", DEBUG_MSG, NEXT_LINE);
              enQueue_Message(CAD,objMessage);
             }
            break;
       case ANI:
            if ((stringSourceAddress.compare(Remote_IP.stringAddress) == 0))             {boolIPOK=true;}
            if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC))
             {
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 358, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_358, stringSourceAddress, 
                                         ASCII_String(e->Datagram, (size_t)e->lenDatagram),"","","","", DEBUG_MSG, NEXT_LINE);
              enQueue_Message(ANI,objMessage);
             }        
            break;
       case ALI:
            if ((stringSourceAddress.compare(Remote_IP.stringAddress) == 0)) {boolIPOK=true;}
            if ((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC))
             {
              objMessage.fMessage_Create(LOG_CONSOLE_FILE, 252, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData , objBLANK_TEXT_DATA,
                                         objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ALI_MESSAGE_252,  stringSourceAddress, 
                                         ASCII_String(e->Datagram,(size_t) e->lenDatagram),"","","","", DEBUG_MSG, NEXT_LINE);
              enQueue_Message(ALI,objMessage);
             }        
            break;
       case AGI:
            boolIPOK = true;
            break;
       default:
            SendCodingError("baseclasses.h - coding error in switch in virtual int FireDataIn(UDPPortDataInEventParams *e)");
      }


     if(!boolIPOK)
      {
       fIncrementDiscardedPacketCounter(); 
       boolOutsideIPTraffic = true;
       stringOutsideIPAddr = stringSourceAddress;
       sem_post(&sem_tPortLock);
       return 0;
      }
     
     strNullfreeData = ReplaceNullCharacters(e->Datagram, e->lenDatagram);
     stringTemp =  stringTemp2   = "";     
   
     switch (enumPortType)
      {
       // note buffer size must be divisible by 2 !!!!!!!!!!!
       case CAD:
            intMaxBufferSize = MAX_CAD_RECORD_SIZE * 100;
            break;
       case ALI:
            intMaxBufferSize = intMAX_ALI_RECORD_SIZE * 100;
            break;
       case ANI:
            switch(enumANISystem)
             {
              case (ANI_LINK): intMaxBufferSize = MAX_ANI_RECORD_SIZE * 100; break;
              case (ASTERISK): intMaxBufferSize = MAX_ASTERISK_RECORD_SIZE * 100; break;
              default:  SendCodingError( "baseclasses.h - coding error in switch in virtual int FireDataIn(UDPPortDataInEventParams *e) setting max ANI buffer size");
                        intMaxBufferSize = 65000;break;
             }
            break;

       default:
            
            // WRK and AGI
            UDPPortDataRecieved.intPortNum 	              = intPortNum;
            UDPPortDataRecieved.intSideAorB 	              = 0;
            UDPPortDataRecieved.stringDataIn                  = e->Datagram;
            UDPPortDataRecieved.intLength                     = UDPPortDataRecieved.stringDataIn.length();
            UDPPortDataRecieved.IPAddress.stringAddress       = stringSourceAddress;
            
            // send downrange
            fQueue_Transmission( UDPPortDataRecieved );
            sem_post(&sem_tPortLock);
            return 0;

      }// end switch

         
      stringTemp = stringBuffer;
      stringTemp+= strNullfreeData;
      
 //      //cout << "Max string size " << stringBuffer.max_size() << endl;

      // check if string is getting too large !!!!!
      if (stringTemp.length() > MAX_STRING_BUFFER_LENGTH)
       {
        // shut down port
        boolPortActive = false;
  //      SetAcceptData(false);
     
 //       //cout << "Shutting down length = " << stringTemp.length() << endl;
        UDPPortDataRecieved.boolStringBufferExceeded = true;
        UDPPortDataRecieved.intPortNum 	             = intPortNum;
        UDPPortDataRecieved.intSideAorB 	     = intSideAorB;
        UDPPortDataRecieved.stringDataIn             = "MAX STRING LENGTH IN BUFFER EXCEED";
        UDPPortDataRecieved.intLength                = stringTemp.length();
    
        // send flagged data downrange
        fQueue_Transmission( UDPPortDataRecieved );
        stringBuffer.clear();
        stringTemp.clear();
        sem_post(&sem_tPortLock);
        return 1;
       }

do
   {
    size_tLength = stringTemp.length();
    stringTemp2 = stringTemp;
    boolDelimeterFound = false;

    if (size_tLength == 0){continue;}


//     //cout << "port number " << intPortNum << endl;

 //    //cout << "port type " << enumPortType << endl;

    switch (enumPortType)
     {     
      case CAD:
          
           stringTemp =  fCheck_Delimiter(stringTemp, boolDelimeterFound, charACK,charNAK);
           break;
      
      case ALI:
           stringTemp =  fCheck_Delimiter(stringTemp, boolDelimeterFound, charACK, charNAK, charETX);
           break;

      case ANI:
            stringTemp = fCheck_Delimiter(stringTemp, boolDelimeterFound, charETX);
            break;
             

      default:
       SendCodingError("baseclasses.h - UDP Port Type not defined ");                        // test
       break;

     }//end switch 

    
    if (stringTemp.length() <  size_tLength){stringTemp2.erase(0,stringTemp.length());}
    else                                    {stringTemp2 = "";}

    
    if ((boolDelimeterFound && ((stringBuffer.length()+stringTemp.length()) <=  intMaxBufferSize)))
     {
       
      stringBuffer = stringTemp;
              
      UDPPortDataRecieved.intPortNum 	                = intPortNum;
      UDPPortDataRecieved.intSideAorB 	                = intSideAorB;
      UDPPortDataRecieved.stringDataIn                  = stringBuffer;
      UDPPortDataRecieved.intLength                     = stringBuffer.length();
      UDPPortDataRecieved.boolStringBufferExceeded      = false;
      // send downrange
      fQueue_Transmission( UDPPortDataRecieved );
      boolBufferPreviouslyCleared = false;
      stringBuffer.clear();
      stringTemp=stringTemp2;
      continue;
     }

    if ((stringBuffer.length()+stringTemp.length()) >=  intMaxBufferSize)
     {
      // fill buffer
      intSize = intMaxBufferSize -stringBuffer.length();
      stringBuffer.append(stringTemp,0,intSize);

      //send(flush) 1st half of buffer
      stringHalfBuffer.assign(stringBuffer,0,intMaxBufferSize/2);
         
      UDPPortDataRecieved.intPortNum 	                = intPortNum;
      UDPPortDataRecieved.intSideAorB 	                = intSideAorB;
      UDPPortDataRecieved.stringDataIn                  = stringHalfBuffer;
      UDPPortDataRecieved.intLength                     = stringHalfBuffer.length();
      UDPPortDataRecieved.boolStringBufferExceeded      = false;
      // send downrange
      
 //     //cout << UDPPortDataRecieved.stringDataIn << endl;

      fQueue_Transmission( UDPPortDataRecieved );
      stringBuffer.erase(0,intMaxBufferSize/2);
      stringTemp.erase(0, intSize);
      stringTemp+=stringTemp2;
//      //cout <<  "1/2 Buffer size cleared....length = " <<  UDPPortDataRecieved.intLength << endl;
     }
//    else {stringBuffer+= stringTemp;}

  
   } while ((stringTemp.length() >= intMaxBufferSize)||(boolDelimeterFound));

  // data is less than buffer length and has no delimiter
  if (  (!stringTemp.empty()) && (stringTemp.length() < intMaxBufferSize)  )
   {
    stringBuffer=stringTemp;
    clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);
   }
  

     ////cout << e-> Datagram	<< endl;
     ////cout << e->SourceAddress 	<< endl;
     ////cout << e->SourcePort	<< endl;
     ////cout << e->lenDatagram	<< endl;

  sem_post(&sem_tPortLock); 
  return 0;

 }// end virtual int FireDataIn()

};

class CadSitePorts
{
 public:

 string strSiteName;

 vector <ExperientUDPPort> CapdPorts;



};



class RCC_Device
{
public:

 bool                   boolContactRelayInstalled;
 int                    intRCCrelayPin;
 IP_Address             RCCremoteIPaddress;
 unsigned int           intRCCremotePortNumber;
 unsigned int           intRCClocalPortNumber;
 volatile bool          boolRCCconnected;
 volatile bool          boolRCCSendDelayedSignal;
 ExperientTCPPort       RelayContactClosurePort;

 RCC_Device()
  {
    boolContactRelayInstalled           = false;
    intRCCrelayPin                      = 1;
    intRCCremotePortNumber              = 0;
    intRCClocalPortNumber               = 0;
    boolRCCconnected                    = false;
    boolRCCSendDelayedSignal            = false; 
  }
 ~RCC_Device(){}

};








class ExperientCommPort{

public:

threadorPorttype                        enumPortType;
int                                     intPortNum;  
int                                     intSideAorB;
int                                     intLocalPort;

IP_Address                              RemoteIPAddress;
int                                     intRemotePortNumber;  
int					intHeartbeatInterval;
volatile bool                           boolRestartListenThread;
volatile bool           		boolReducedHeartBeatMode;
volatile bool                           boolReducedHeartBeatModeFirstAlarm;
volatile bool                           boolALIportPairDown;
volatile bool                           boolUnexpectedDataShowMessage;
volatile bool                           boolReadyToShutDown;
volatile bool                           boolEmergencyShutdown;
volatile bool                           boolFirstStrayAck;
unsigned long long int                  intUnexpectedDataCharacterCount;
struct timespec                         timespecReducedHeartBeatMode;
long int                                intPortDownReminderThreshold;
int                                     intPortDownThresholdSec;
bool                                    boolPortActive;
bool                                    boolACKReceived;
bool                                    boolNAKReceived;
unsigned long long int                  intConsecutiveNAKCounter;
unsigned long long int                  intStrayACKCounter;
unsigned long long int                  intConsecutiveBadRecordCounter;
bool                                    boolFirstACKRcvd;
timer_t                                 timer_tPortListenTimerId;
Port_Connection_Type                    ePortConnectionType;
Port_ALI_Protocol_Type                  eALIportProtocol;
ExperientUDPPort                        UDP_Port;
ExperientTCPPort                        TCP_Port;
ExperientTCP_SERVER_Port                TCP_Server_Port;
ali_format                              eRequestKeyFormat;		        		        
bool                                    boolHeartbeatRequired;
bool                                    boolRecordACKRequired;
bool                                    boolHeartBeatACKrequired;
struct timespec                         timespecTimeXmitStamp;
struct timespec	                        timespecTimeLastHeartbeat;
struct timespec	                        timespecTimeEmergencyShutDown;
bool                                    boolHeartbeatActive;
bool                                    boolPhantomPort;
long int                                intBidsActive;
bool                                    boolXmitRetry;
bool                                    boolSendCADEraseMsg;
bool                                    boolAudiocodesRegistered;
int                                     intTCPconnectionId;
int                                     intHBTransactionID;
Workstation_Group                       WorkstationGroup;
bool                                    boolNENA_CADheader;
bool                                    boolCAD_EraseFirstALI_CR;
Position_Aliases                        PositionAliases;
string                                  strNotes;

// functions
void                                    fEmergencyPortShutDown(Call_Data objData, string stringArg, unsigned long long int intArg);
void                                    fSetReducedHeartbeatMode(Call_Data objData, struct timespec timespecTimeNow);
void                                    fSetHeartbeatMissingMode();
void                                    fClearHeartbeatMissingMode();
void                                    fClearReducedHeartbeatMode(bool SupressMessage = false);
void                                    fSet_ACK_Received();
bool                                    fCheck_for_ACK();
void                                    fSetPortActive();
void                                    fSetPortInactive();
bool                                    fPortRestart();
void                                    fIncrementNAKCounter();
void                                    fIncrementStrayACKCounter();
void                                    fIncrementConsecutiveBadRecordCounter();
void                                    fIncrementBadCharacterCount(unsigned long long int iCount);
void                                    Check_Port_Restart();
void                                    fLoad_Vendor_Email_Address(string stringArg);
void                                    fInitializeALIport(int i, int j);
void                                    fInitializeWRKUDPport(int i);
void                                    fInitializeWRKTCPServerPort(int i);
void                                    fInitializeCTI_TCP_Port();
void                                    fInitializeECATS_UDP_Port();
void                                    fInitializeCADport(int i, bool boolSKIPACTIVE=false);
void                                    fResetWRKTCPSocket(int i);
int                                     fNextHBTransactionId();
bool                                    fPostionUsesPort(int i);
string                                  fDisplayPostionsUsingPort();
};



class Thread_Trap
{
 public:

 volatile bool                          boolCadThreadReady;
 volatile bool                          boolLogThreadReady;
 volatile bool                          boolALiThreadReady;
 volatile bool                          boolANIThreadReady;
 volatile bool                          boolWRKThreadReady;
 volatile bool                          boolAMIThreadReady;
 volatile bool                          boolRCCThreadReady;
 volatile bool                          boolCTIThreadReady;
 volatile bool                          boolLISThreadReady;
 volatile bool                          boolDSBThreadReady;
 volatile bool                          boolMAINThreadWait;


 Thread_Trap()
  {
   boolCadThreadReady = false;
   boolLogThreadReady = false;
   boolALiThreadReady = false;
   boolANIThreadReady = false;
   boolWRKThreadReady = false;
   boolAMIThreadReady = false;
   boolRCCThreadReady = false;
   boolCTIThreadReady = false;
   boolLISThreadReady = false;
   boolDSBThreadReady = false; 
   boolMAINThreadWait = false;

  }
 ~Thread_Trap(){}
 
 bool fAllThreadsReady();
 void fShowThreadsState();
};


class COSCallbackRow
{
 public:
 string  		strClassOfService;
 int 			iRow;
 // Constructor
 COSCallbackRow()
  {
   strClassOfService.clear();
   iRow = 0;
  }
 // Destructor 
~COSCallbackRow(){}
//  Copy Constructor
COSCallbackRow(const COSCallbackRow& b)
 {
  strClassOfService 	= b.strClassOfService;
  iRow 			= b.iRow;
 }
COSCallbackRow& operator= (const COSCallbackRow& b)
 {
  if (&b == this ) {return *this;}
  strClassOfService 	= b.strClassOfService;
  iRow 			= b.iRow;
  return *this;
 }
bool operator == (const COSCallbackRow& a) const { return fCompare(a);}  

 bool fLoadCOSplusRow(string strCOS, int iROW);
 bool fCompare(const COSCallbackRow& a) const;
};

class  ALISteering
{
 public:
  int                        intDatabaseNum;
  ali_format                 eRequestKeyFormat;
  int                        intNormalTimeOutSec;
  int                        intWirelessTimeoutSec;
  int                        intNumPortPairs;
  int                        intCallbackLocationRow[2];
  int                        intCallbackLocationColumn[2];
  bool                       boolPortPairInDatabase[intMAX_ALI_PORT_PAIRS  + 1];
  int                        intNumPANIRangePairs;
  long long unsigned int     intPANILowRange[intMAX_PANI_RANGE_PAIRS+1];
  long long unsigned int     intPANIHighRange[intMAX_PANI_RANGE_PAIRS+1];
  int                        intPANIRebidDelay[intMAX_PANI_RANGE_PAIRS+1];
  bool                       boolDatabaseSendsSingleALI;
  bool                       boolALIService;
  bool                       boolTreatLinefeedasCR;
  int                        iALIServiceIndex;
  trunk_type                 enumTrunkType;
  vector <COSCallbackRow>    vCOSCallbackRow;
  int                        iClassOfServiceRow;

 // Constructor
   ALISteering();
	
 // Destructor 
  ~ALISteering(){};
       
//  Copy Constructor
   ALISteering(const ALISteering *a);

//  Assignment operator
   ALISteering& operator=(const ALISteering& a);

// equal operator
  bool operator == (const ALISteering& a) const { return fCompare(a);}  

  bool fLoadClassofServiceData(XMLNode ClassofServiceNode);
  void fLoadClassOfServiceRow(string strInput);
  void fDisplayCOSVector(string strHeader="");
  bool fLoadCOSvector(string strInput);
  int  fLoadPortPairs(int intArg, string stringArg);
  int  fLoadPANIRanges(int intArg, int intArg2, string stringArg);
  int  fLoadTimeoutData( string stringArg);
  int  fSelectPortPair(int intNum);
  bool fLoadTrunkType(string strArg);
  bool fLoadCallbackLocation(string strArg, int index);
  bool fLoadClassOfServiceCallbackRow(string strArg); 
  bool fCanBidDatabase(trunk_type eTrunkType);
  int  fLoadALIRequestFormat(string strInput);
  bool fCompare(const ALISteering& a) const;
  void fClear();
};

class EmailSlam
{
 time_t			FilterData[LOG_MAX_NUMBER_OF_MESSAGE_CODES+1] [EMAIL_MAX_MESSAGE_CODE_PER_HR];
 sem_t			sem_tMutexFilterData;
 public:
 bool			EmailFiltering[LOG_MAX_NUMBER_OF_MESSAGE_CODES+1];
 unsigned long long int LostEmailCount[LOG_MAX_NUMBER_OF_MESSAGE_CODES+1];

  //constructor
  EmailSlam()
   {
    sem_init(&sem_tMutexFilterData,0,1);
    for (int i = 0; i <= LOG_MAX_NUMBER_OF_MESSAGE_CODES; i++)
     {
      EmailFiltering[i] = false;
      LostEmailCount[i] = 0;
      for (int j = 0; j < EMAIL_MAX_MESSAGE_CODE_PER_HR; j++) {FilterData[i][j] = 0;}
     }
   }
  //destructor
  ~EmailSlam()
   {
    sem_destroy(&sem_tMutexFilterData);
   }

 bool	CheckEmailSlamFilter(int MessageCode, time_t time_tTimeofMessage);
 void	fSupressedEmailCount(int MessageCode);
 void   fRefreshFilterData();

};



class Trunk_Type
{
public:
int                intTrunkNumber;
trunk_type         TrunkType;
bool               boolTrunkCallOK;
bool               boolWarningSent;
bool               boolTrunkNumberInUse;
int                RotateGroup;
time_t             time_tLastCall;

//Constructor
 Trunk_Type();
//destructor
 ~Trunk_Type(){};
//copy contructor
 Trunk_Type(const Trunk_Type *a);
//assignment operator
 Trunk_Type& operator=(const Trunk_Type& a);
//compare
bool operator == (const Trunk_Type& a) const { return fCompare(a);}

bool    fCompare(const Trunk_Type& a) const;
string  fTrunkType();
};

class Trunk_Rotate_Group
{
public:
trunk_rotate_rule  RotateRule;
int                RotateRuleValue;
int                GroupCount;
vector <int>       TrunkMember;

 Trunk_Rotate_Group();
~Trunk_Rotate_Group();
//copy contructor
 Trunk_Rotate_Group(const Trunk_Rotate_Group *a);
//assignment operator
 Trunk_Rotate_Group& operator=(const Trunk_Rotate_Group& a);
//compare
bool operator == (const Trunk_Rotate_Group& a) const { return fCompare(a);}


bool   fLoadRule(string strInput);
bool   fLoadRuleValue(string strInput);
bool   fLoadMembers(string strInput);
void   fDisplay(bool bShowLabels);
string fDisplayRule();
void   fClear();
bool   fCompare(const Trunk_Rotate_Group& a) const;
};



class Trunk_Type_Mapping
{
public:
Trunk_Type  Trunk[intMAX_NUM_TRUNKS+1];

 //constructor
 Trunk_Type_Mapping();
 //destructor
~Trunk_Type_Mapping();
//copy contructor
 Trunk_Type_Mapping(const Trunk_Type_Mapping *a);
//assignment operator
 Trunk_Type_Mapping& operator=(const Trunk_Type_Mapping& a);
//compare
bool operator == (const Trunk_Type_Mapping& a) const { return fCompare(a);}

bool    fLoadTrunkMapType(trunk_type eTrunkType, string stringArg);
bool    fIsTandem(int i);
bool    fIsCLID(int i);
bool    fIsINDIGITAL(int i);
bool    fIsMSRP(int i);
bool    fIsINTRADO(int i);
bool    fIsREFER(int i);
bool    fCompare(const Trunk_Type_Mapping& a) const;
};

class Trunk_Sequencing
{
public:
//friend class boost::serialization::access;

Trunk_Type_Mapping         TrunkMap;
unsigned int               intCallCount;
unsigned int               intNumber911trunks;
sem_t                      sem_tMutexTrunkSequence;

vector <Trunk_Rotate_Group> TrunkRotateGroup;

//Constructor
Trunk_Sequencing();
 
//destructor
~Trunk_Sequencing(){sem_destroy(&sem_tMutexTrunkSequence);}
 //copy constructor
  Trunk_Sequencing(const Trunk_Sequencing *a);
  //  Assignment operator
  Trunk_Sequencing& operator=(const Trunk_Sequencing& a);
  //  equals operator
 bool operator == (const Trunk_Sequencing& a) const { return fCompare(a);}

bool    fCompare(const Trunk_Sequencing& a) const;
void    fLoadTrunkMapRotateGroups();
void    fSetNumber911trunks();
void    fAddCall(unsigned int intTrunk);
bool    fCheckSequence(unsigned int i);
void    fReportDeadTrunks(unsigned int i);
void    fResetSequence(unsigned int i);
void    fDisplayLastCallTime();
void    fCheckTrunkUsagebyDays();
int     fNumberOfMonitoredTrunks();
int     fNumberOfAlarmingTrunks();
};

class Conference_Number
{

public:
//friend class boost::serialization::access;

bool                 bConferenceNumberInUse[MAX_CONFERENCE_NUMBER +1];
struct timespec      timespecTimePutinUse[MAX_CONFERENCE_NUMBER +1];


//Constructor
Conference_Number()
 {
  for (int i = 0; i <= MAX_CONFERENCE_NUMBER; i++) {bConferenceNumberInUse[i] = false;}
 }
//destructor
~Conference_Number(){}


string      AssignConferenceNumber(Call_Data objCallData, string strAGIMessageID, ani_functions enumTransfer= UNCLASSIFIED);
string      AssignConferenceNumber(string strAGIMessageID);
int         AssignConferenceNumber();
bool        ReleaseConferenceNumber(int i);
long double TimeInUse(unsigned int i);
int         ConferencesInUse();
int         OldestRecordInUse();
};

class Local_Call_Rules
{
 public:

 vector <string> LocalAreaCode;
 vector <string> LocalNXXprefix;

 Local_Call_Rules(){}
 
 ~Local_Call_Rules(){}

bool fRuleForAreaCodeExists();
bool fRuleForNXXexists();
bool fIsALocalAreaCode(string strAreaCode);
bool fIsALocalExchangePrefix(string stNXXprefix);
void fDisplay();
};




class ANI_Data_Format
{
 public:
 int   iANIformatReceived[intMAX_NUM_TRUNKS+1];
 sem_t  sem_tMutexANIformatReceived;

 ANI_Data_Format()
  {
   sem_init(&sem_tMutexANIformatReceived,0,1);
   for (int i=0; i <= intMAX_NUM_TRUNKS; i++) { iANIformatReceived[i] = 0;}
  }
~ANI_Data_Format(){sem_destroy(&sem_tMutexANIformatReceived);}

 void fSetANIformatReceived(int iFormat, int i);
 void fDisplayANIformatDetected();
};

class Phone_Line
{
 public:
 
 string LineRegistrationName;
 bool   OutboundDial;
 bool   boolInUse;
 bool   boolSharedLine;
 int    LineIndex;
 bool   boolMuted;
 string strExtMWIgateway;
 
  Phone_Line();
 
 ~Phone_Line(){}

 void fClear();
 void fLoadRegistrationName(string strName);
 void fLoadOutboundDial(bool bValue);
 void fLoadOutboundDial(string strData);
 void fLoadSharedLine(string strBoolean);
 void fLoadLineIndex(int index);
};



class Telephone_Device
{
 public:
        
 string               strDeviceName;
 Telephone_Data_type  eDeviceType;
 string               strMACaddress;
 bool                 boolIsAnAudiocodes;
 struct timespec      timespecTimeLastRegistered;
 struct timespec      timespecTimeFirstRegistered;
 bool                 boolRequiresRegistration;
 bool                 boolMonitorStatus;
 bool                 boolInitialRegistration;
 bool                 boolRegistered;
 bool                 boolFirstAlarm;
 IP_Address           IPaddress;
 long long int        intDeviceDownReminderThreshold;
 int                  intPositionNumber;
 vector<Phone_Line>   vPhoneLines;
 int                  iNumberOfLines;
 bool                 boolCanDialExtensions;
 bool                 boolCanDialOutsideLine;
 string               strCallerIDnumber;
 string               strCallerIDname;
 string               strNextGenURI;

 Telephone_Device();
 
 ~Telephone_Device(){}


void   fDisplay(int i = 0, bool showLabel=false);
bool   fCheckTimeout();
bool   fPingDevice();
void   fClear();
int    fLineNumberWithRegistrationName(string strName);
string DeviceString();
bool   fUpdateLinesInUse();
int    fIndexInPhoneLineVector(int index);
int    fFirstOutboundLineIndex();
int    fLineIndexofSharedLine(string strName);
int    fLineIndexofLineNumber(int line);
int    fLineNumberWithIndex(int Index);
};

class Telephone_Devices
{
 public:

 bool                      FirstDeviceRegistered;
 struct timespec           timespecTimeLastAsteriskRestart;
 vector <Telephone_Device> Devices;
 vector <GUI_Line_View>    GUILineView;
 vector <string>      	   ParkingLots;
 vector <string>           vRoutingPrefixes;
 size_t                    NumberofLineViews;  //not sure if needed
 int                       NumberofTelephones;
 size_t                    NumberofParkingLots;
 int                       HighestWorkstationNumber;
 size_t                    NumberofRoutingPrefixes;
 sem_t                     sem_tMutexTelephoneDeviceVector;


 Telephone_Devices();
 
~Telephone_Devices();


bool   fLoadDevice(int iWorkstation, string strData, string strURI, bool bMonitorstatus = true);
bool   fLoadMACaddress(int iWorkstation, string strData);
bool   fLoadIPaddress(int iWorkstation, string strData);
bool   fLoadDeviceType(int i, string strData);
bool   fLoadNumberofLines(int i, string strData);
bool   fLoadExtMWIGateway(string strExtMWIGateway, int iPhone, string strMWILineReg);
bool   fLoadIPGateway(string strName, string strIPData, bool boolExt, bool boolOut, bool boolIsAudiocodes);
void   fCheckTimeouts();
void   fRegisterDevice(string strInput, int intPortNum);
int    fIndexWithDeviceName(string strName);
int    fIndexWithIPaddress(IP_Address objIPAddress);
int    fIndexWithPositionNumber(int intPosition);
int    fPostionNumberFromIPaddress(string strAddress);
int    fPositionNumberFromChannelName(string strChannel);
bool   fIsAnAudiocodes(IP_Address objIPaddress);
bool   fIsAnNG911SipTrunk(IP_Address objIPAddress);
int    fIndexofLineView(string strName);
int    fFindTelephoneLineNumber(string strName);
bool   fcanDialthisNumber(string strGatewayname, string strNumber);
bool   fLoadCallerIdNumber(int iWorkstation, string strInput);
bool   fLoadCallerIdName(int iWorkstation, string strInput);
int    fDetermine_Line_Number(string strName, int iPos);
bool   fLineNumberAtPositionIsShared(int iLineNumber, int iPosition);
bool   fLineNumberAtPositionIsShared(Channel_Data objData);
bool   fDeviceWithThisWorkstationHasThisPSAPURI(int i, string strURI);
int    fIndexofTelephonewithRegistrationName(string strName);
int    fDetermine_Shared_Line_Phone_LineIndex(string strInput, int iPos);
int    fDetermine_Shared_Line_Phone_LineIndex(int GUIlineIndex, int iPos);
string fMWIaccountofDevicewithExternGateway(string strGateway);
string fCallerIDnameofTelephonewithRegistrationName(string strName);
string fCallerIDnameofTelephonewithPosition(int i);
string fCallerIDnumberofTelephonewithPosition(int i);
string fIPaddressFromPositionNumber(int j);
string fRemoveRoutingPrefix(string strInput);
string fTelMonitorString();
string fACMonitorString();
void   fDisplay();
};


class Last_On_Hold
{
 public:
 string strChannel;
 string strUniqueID;
 vector <Channel_Data> vLastonHold;

 void IintializeVector();
 bool Set_Last_OnHold(string strChannel, string strID, unsigned int i);
};

class ANI_Variables
{
 public:
// int   			intANI_PORT_DOWN_THRESHOLD_SEC;
// int  			intANI_PORT_DOWN_REMINDER_SEC;  
 int			intNUM_TRUNKS_INSTALLED;
 int			intDELAY_AFTER_FLASH_HOOK_MS;
 int			intDELAY_BETWEEN_DTMF_MS;
 int			intDelay_AFTER_STAR_EIGHT_PD_MS;
 int			intDelay_BETWEEN_DTMF_INDIGITAL_MS;
 int			intNUM_ANI_PORTS;
/*
 string			strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY;
 string			strASTERISK_AMI_POSITION_HEADER_911;
 string			strASTERISK_AMI_POSITION_HEADER_FIRE;
*/
 string			strCONFERENCE_NUMBER_DIAL_PREFIX;
//string			strASTERISK_AMI_IP_ADDRESS;
 string			strANI_16_DIGIT_REQ_TEST_KEY;
 string			strANI_14_DIGIT_REQ_TEST_KEY;
 string			strGATEWAY_TRANSFER_ORDER;

 bool			bool_BCF_ENCODED_IP;
 bool			boolAUDIOCODES_911_ANI_REVERSED;
 bool			boolANI_20_DIGIT_VALID_CALLBACK; 
 bool			boolAMI_RECORD_SCRIPT;
 bool			boolAMI_TEST_SCRIPT;
 bool			boolTDD_AUTO_DETECT;
 
 mutable bool		boolRestartFreeswitchCLIport;
 mutable bool 		boolRestartANIports;
 mutable int 		intOldNumberOfANIports;

 vector <string>   	vGATEWAY_TRANSFER_ORDER;
 vector <string>        NPD;
 ani_system		enumANI_SYSTEM;
 Trunk_Type_Mapping	TRUNK_TYPE_MAP;
 Trunk_Sequencing       Trunk_Map;
// ExperientCommPort      ANIPort[NUM_ANI_PORTS_MAX+1];
 PORT_INIT_VARS         ANI_PORT_VARS[NUM_ANI_PORTS_MAX+1];
 CSimpleIniA            ini;

  //constructor
  ANI_Variables();
   // destructor
  ~ANI_Variables(){}

  //copy constructor
  ANI_Variables(const ANI_Variables *a);
  //  Assignment operator
  ANI_Variables& operator=(const ANI_Variables& a);
  //  equals operator
 bool operator == (const ANI_Variables& a) const { return fCompare(a);}

 bool fLoadINIfile();
 bool fLoadANIports();
 bool fLoadNPDs();
 bool fLoadFreeswitchIPaddress();
 bool fLoadANIsystem();
 bool fLoad20digitCallback();
 bool fLoadRecordAMI();
 bool fLoadTestScript();
 bool fLoadTDDautoDetect();
 bool fLoadBCFencodedIP();
 bool fLoadAudiocodes911ANIreversed();
 bool fLoad16DigitTestKey();
 bool fLoad14DigitTestKey();
 bool fLoadDelayAfterFlashHook();
 bool fLoadDelayBetweenDTMF();
 bool fLoadDelayAfterStar8();
 bool fLoadDelayBetweenInDigitalDTMF();
 bool fLoadTrunkTypeMap();
 bool fLoadTrunkMap();
 bool fLoad_CC_Database_Email_addresses();
 bool fLoad_CC_Port_Email_addresses();
 bool fLoadPortDownThreshold();
 bool fLoadPortDownReminder();
 bool fLoadTrunksInstalled();
 bool fLoadConferenceNumberDialPrefix();
 bool fCompare(const ANI_Variables& a) const;
 bool Load_Gateway_Order();
 void Set_GLobal_VARS();

};

class CAD_Variables {
  public:
  int                           intNUM_CAD_PORTS; 
  bool                          boolCAD_EXISTS;
  bool                          boolCAD_ERASE;
  int                           intCAD_PORT_DOWN_THRESHOLD_SEC;
  int                           intCAD_PORT_DOWN_REMINDER_SEC;
  bool                          boolSEND_TO_CAD_ON_CONFERENCE;
  bool                          boolSEND_TO_CAD_ON_TAKE_CONTROL;
  
//ExperientCommPort             CADPort                                 [NUM_CAD_PORTS_MAX+1];
  mutable bool                  boolRestartLegacyCADPorts;
  mutable int                   intOldNumberOfLegacyCADports;
  CSimpleIniA                   ini;
  
  PORT_INIT_VARS                CAD_PORT_VARS[NUM_CAD_PORTS_MAX + 1 ];

  //constructor
  CAD_Variables();
   // destructor
  ~CAD_Variables(){}

  //copy constructor
  CAD_Variables(const CAD_Variables *a);
  //  Assignment operator
  CAD_Variables& operator=(const CAD_Variables& a);
  //  equals operator
 bool operator == (const CAD_Variables& a) const { return fCompare(a);}
  
  
 bool fCompare(const CAD_Variables& a) const;
 void Set_GLobal_VARS();
 bool fLoadINIfile();
 bool fLoadPortDownThreshold();
 bool fLoadPortDownReminder();
 bool fLoadCADexists();
 bool fLoadCADsendToConference();
 bool fLoadCADtakeControl();
 bool LoadPortData( int intArg, string charArg , string stringArg );
 bool fLoadCADPorts();
  
};

class ALI_Variables {
  public:
  bool			boolLEGACY_ALI;
  bool          boolLEGACY_ALI_ONLY;
  bool          boolTREAT_LF_AS_CR;
  bool 			boolUSE_ALI_SERVICE_SERVER;
  bool			boolSEND_LEGACY_ALI_ON_SIP_TRANSFER;
  bool			boolALLOW_MANUAL_ALI_BIDS;
  bool			boolIGNORE_BAD_ALI;
  bool			boolALLOW_CLID_ALI_BIDS;
  bool			boolCLID_BID_TEST_KEY;
  bool			boolSHOW_GOOGLE_MAP;
  bool			boolVERIFY_ALI_RECORDS;
  bool			boolREBID_PHASE_ONE_ALI_RECORDS_ONLY;
  bool			boolREBID_NG911;

  unsigned int  intDEFAULT_ALI_NOISE_THRESHOLD;
  int			intALI_PORT_DOWN_THRESHOLD_SEC;
  int			intALI_PORT_DOWN_REMINDER_SEC;
  int			intALI_WIRELESS_AUTO_REBID_SEC;
  int			intALI_WIRELESS_AUTO_REBID_COUNT;
  unsigned int	intMAX_ALI_RECORD_SIZE;
  int 			intNUM_ALI_SERVICE_PORTS;
  int			intNUM_ALI_PORT_PAIRS;
  int			intNUM_ALI_DATABASES;
  int			intCLID_ALI_BID_RULE_CODE;

  mutable bool                  boolRestartALILegacyPorts;
  mutable int                   intOldNumberOfLegacyALIports;
  mutable bool                  boolRestartALIservicePorts;
  mutable int                   intOldNumberOfALIservicePorts;

  ALISteering     	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
//  ExperientCommPort  	ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
//  ExperientCommPort     ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ]; 
  CSimpleIniA           ini;

  PORT_INIT_VARS         ALI_SERVICE_PORT_VARS[intMAX_ALI_PORT_PAIRS  + 1 ];
  PORT_INIT_VARS         ALI_PORT_VARS[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];

  //constructor
  ALI_Variables();
   // destructor
  ~ALI_Variables(){}

  //copy constructor
  ALI_Variables(const ALI_Variables *a);
  //  Assignment operator
  ALI_Variables& operator=(const ALI_Variables& a);
  //  equals operator
 bool operator == (const ALI_Variables& a) const { return fCompare(a);}

 bool fLoadINIfile();
 bool fLoadLegacyALISetting();
 bool fLoadLegacyALIonlySetting();
 bool fLoadUseALIserviceServer();
 bool fLoadSendLegacyALIonSIPtransfer();
 bool fLoadAllowManualBids();
 bool fLoadIgnoreBadALI();
 bool fLoadShowGoogleMap();
 bool fLoadVerifyALIrecords();
 bool fLoadRebidPhaseOneRecordsOnly();

 bool fLoadDefaultNoiseThreshold();
 bool fLoadPortDownThreshold();
 bool fLoadPortDownReminder();
 bool fLoadWirelessRebidSec();
 bool fLoadWirelessRebidCount();
 bool fLoadMaxALIrecordSize();
 bool fLoadAliServicePorts();
 bool fLoadAliPorts();
 bool LoadPortConnectionType( int intArg, string charArg , string stringArg);
 bool LoadPortData( int intArg, string charArg , string stringArg );
 bool LoadALIPortProtocolType( int i, string stringArg );
 bool fLoadAliDataBases();
 bool fLoadCLIDbidRules();
 void fLoadKeyFormats();
 void Set_GLobal_VARS();
 bool fLoadRebidNG911();
 bool fCompare(const ALI_Variables& a) const;
};


class Telephone_Variables
{
 public:

  int 			intRCC_REMOTE_TCP_PORT_NUMBER;
  bool			boolRCC_DEFAULT_CONTACT_POSITIONS;

  bool			boolUSE_POLYCOM_CTI;
  int			intPOLYCOM_CTI_HTTP_PORT;
  bool			boolADD_NINE_FOR_OUTBOUND_CALLS;
  bool			boolSHOW_ATTENDED_XFER_BUTTON;
  bool			boolSHOW_VOLUME_BUTTON;
  bool			boolSHOW_HANGUP_BUTTON;
  bool			boolSHOW_HOLD_BUTTON;
  bool			boolSHOW_BARGE_BUTTON;
  bool			boolSHOW_ANSWER_BUTTON;
  bool			boolSHOW_DIAL_PAD_BUTTON;
  bool			boolSHOW_SPEED_DIAL_BUTTON;
  bool			boolSHOW_CALLBACK_BUTTON;
  bool                  boolSHOW_LINE_VIEW_TAB;
  
  int			intDEVICE_DOWN_THRESHOLD_SEC;
  int			intDEVICE_DOWN_REMINDER_SEC;
  string		strCALLER_ID_NAME;
  string		strCALLER_ID_NUMBER;

  Local_Call_Rules      LocalCallRules;
  Telephone_Devices     TelephoneEquipment;
  XMLNode		TDDnode;
  XMLNode		PhoneBookNode;
  CSimpleIniA           ini;

   Telephone_Variables(){}
 
  ~Telephone_Variables(){}

 bool fLoadINIfile();
 bool fLoadRCCremotePort();
 bool fLoadRCCdefaultContactPositions();
 bool fLoadCTIoption(); 
 bool fLoadCTIhttpPort();
 bool fLoadDialNineForOutboundCallsRule();
 bool fLoadDeviceDownThreshold();
 bool fLoadDeviceDownReminderThreshold(); 
 bool fLoadDefaultCallerIDname();
 bool fLoadDefaultCallerIDnumber();
 bool fLoadLocalCallRules();
 bool fLoadTelephoneDevices();
 bool fLoadTelephoneLineRegistration();
 bool fLoadVOIPgateways();

      
};



class Kernel_Variables
{
 public:


 string    	stringPSAP_Name;
 string    	stringPRODUCT_NAME; 
 string    	stringCOMPANY_NAME; 
 string         strCUSTOMER_SUPPORT_NUMBER;
 string         strABOUT_SPLASH_FILE;     
 string    	stringDEFAULT_GATEWAY_DEVICE;
 string    	stringSYNC_ETHERNET_DEVICE;
 string    	stringPSAP_ID;	
 string    	intAVAIL_DISK_SPACE_WARN_GIG;
 string    	intMONTHS_TO_KEEP_LOG_FILES;    
 string    	strESME_ID_STRING ;   
 string    	strCONFIG_FILE_VERSION;
 string         stringEMAIL_SEND_TO;
 string         stringEMAIL_FROM;
 string         stringEMAIL_SEND_TO_ALARM;

 int       	intNUM_OF_SERVERS;
 int       	intSERVER_NUMBER;
 int        	intBEACON_PORT_NUMBER; 

 bool      	boolNG_ALI;
 bool           boolEMAIL_MONTHLY_LOGS;

 IP_Address     HOST_IP;
 IP_Address     REMOTE_IP;
 IP_Address	MULTICAST_GROUP;
 IP_Address     BEACON_IP;

 vector <string>        NPD[8];
 ServerData  		ServerStatusTable[MAX_NUM_OF_SERVERS+1];

};

class Workstation_Variables
{
 public:

  int 			intNUM_WRK_STATIONS;
  int			intWRK_LOCAL_LISTEN_PORT;
  int			intWRK_HEARTBEAT_INTERVAL_SEC;
  int			intWRK_PORT_DOWN_THRESHOLD_SEC;
  int			intWRK_PORT_DOWN_REMINDER_SEC;
  int			intWRK_GUI_TAB_BEHAVIOR_CODE; 

  Port_Connection_Type	enumWORKSTATION_CONNECTION_TYPE;

  string		stringTDD_DEFAULT_LANGUAGE;
  int			intTDD_SEND_DELAY;
  string		stringTDD_SEND_TEXT_COLOR;
  string		stringTDD_RECEIVED_TEXT_COLOR;
  int			intTDD_CPS_THRESHOLD;

  string		stringTRANSFER_DEFAULT_GROUP;
  bool			boolGUI_SIP_PHONE_EXISTS;
  
  string		strVIEW_POSITION_DEFAULT;
  bool			boolSHOW_RINGING_TAB;
  bool			bool911_TRANSFER_TANDEM_ONLY;

  vector <string>       vColorPaletteUsers;


 bool                   fLoadColorPaletteUsers(bool boolFirstLoad= false);
 void                   fShowColorPaletteUsers();
};

class TCP_PORT_INIT_VARS {
public:
 int			intPORT_NUMBER;
 int			intHEARTBEAT_INTERVAL_SEC;
 int			intPORT_DOWN_THRESHOLD_SEC;
 int			intPORT_DOWN_REMINDER_SEC;
 int			intREMOTE_PORT_NUMBER;
 IP_Address		REMOTE_IP_ADDRESS;

 TCP_PORT_INIT_VARS();
 ~TCP_PORT_INIT_VARS(){}
  //copy constructor
  TCP_PORT_INIT_VARS(const TCP_PORT_INIT_VARS *a);
  //  Assignment operator
  TCP_PORT_INIT_VARS& operator=(const TCP_PORT_INIT_VARS& a);
  //  equals operator
 bool operator == (const TCP_PORT_INIT_VARS& a) const { return fCompare(a);}


bool fCompare(const TCP_PORT_INIT_VARS& a) const;
void fClear();
};



class DSB_Variables {
public:
 bool			boolUSE_DASHBOARD;
 int                    intNUM_DSB_PORTS;
 string                 strDASHBOARD_API_KEY;
// int			intDSB_HEARTBEAT_INTERVAL_SEC;
// int			intDSB_PORT_DOWN_THRESHOLD_SEC;
// int			intDSB_PORT_DOWN_REMINDER_SEC;


 CSimpleIniA           	ini;
 mutable bool         	boolRestartDashboardPort;
 mutable int 		intOldNumberOfDSBports;
// ExperientTCPPort       DashboardPort;
 TCP_PORT_INIT_VARS     DSB_PORT_VARS;

  DSB_Variables();
 ~DSB_Variables(){}
  //copy constructor
  DSB_Variables(const DSB_Variables *a);
  //  Assignment operator
  DSB_Variables& operator=(const DSB_Variables& a);
  //  equals operator
 bool operator == (const DSB_Variables& a) const { return fCompare(a);}

 bool fLoadINIfile();
 bool fLoadUseDashboard();
 bool fLoadPortDownThreshold();
 bool fLoadPortDownReminder();
 bool fLoadDashboardIPaddress();
 bool fLoadDashboardPortNumber();
 bool fLoadDashboardAPIkey();
 bool fLoadHearbeatInterval();

 void Set_GLobal_VARS();

 bool fCompare(const DSB_Variables& a) const;
};

class ADV_Variables {
public:

 string					stringEMAIL_FROM;
 string					stringEMAIL_SEND_TO;
 display_mode				enumDISPLAY_MODE;
 bool					boolEmailON;
 string					strEMAIL_RELAY_SERVER;
 string					strEMAIL_RELAY_USER;
 string					strEMAIL_RELAY_PASSWORD;
 int					intDEBUG_MODE_REMINDER_SEC;
 int					intALARM_POPUP_DURATION_MS;
 int					intTCP_RECONNECT_RECURSION_LEVEL;
 int					intTCP_RECONNECT_INTERVAL_SEC;
 CSimpleIniA           			ini;
 bool                                   boolSTRATUS_SERVER_MONITOR;

  ADV_Variables();
 ~ADV_Variables(){}
  //copy constructor
  ADV_Variables(const ADV_Variables *a);
  //  Assignment operator
  ADV_Variables& operator=(const ADV_Variables& a);
  //  equals operator
 bool operator == (const ADV_Variables& a) const { return fCompare(a);}

 bool fLoadINIfile();

 bool fLoadDebugDownReminder();
 bool SetEmailTo();
 bool SetEmailFrom();
 bool SetDisplayMode();
 bool SetEmailOnOff();
 bool SetEmailRelayServer();
 bool SetEmailRelayUser();
 bool SetEmailRelayPassword();
 bool SetTCPRecursionLevel();
 bool SetTCPReconnectInterval();
 bool SetAlarmPopUpDuration();
 bool SetStratusServerMonitor();
 void Set_GLobal_VARS();

 bool fCompare(const ADV_Variables& a) const;
};

class Initialize_Global_Variables
{
 public:
 XMLNode                MasterNode;
 XMLNode                MainNode[NUM_WRK_STATIONS_MAX+1];
 Kernel_Variables       KRNinitVariable;
 Workstation_Variables  WRKinitVariable;
 Telephone_Variables    TELinitVariable;
 XMLNode                PhoneBookNode;      // eventually these will be arrays
 XMLNode                NG911TransferNode;  // eventually these will be arrays
 XMLNode                LineViewNode[NUM_WRK_STATIONS_MAX+1];
 sem_t                  sem_tMutexMasterNode;
 
 ADV_Variables          ADVinitVariable;
 DSB_Variables          DSBinitVariable;
 ALI_Variables          ALIinitVariable;
 ANI_Variables          ANIinitVariable;
 CAD_Variables          CADinitVariable;
 
 vector <string>        vInitXML;
 string                 strInitXML;


 Initialize_Global_Variables();
 
~Initialize_Global_Variables(){ sem_destroy(&sem_tMutexMasterNode);}


 bool                       fLoadPhonebook(bool bFirstLoad = false);
 void                       fLoadPhonebookList();
 bool                       fLoadLineViews(int i);
 bool                       fLoadMessageControl(bool bFirstLoad = false);
 bool                       fLoadNG911TranferList(bool boolFirstLoad = false);
 bool                       fIsSIPtransferNumberInternal(string strTransferNumber);
 bool                       fChannelIsInternalNG911Transfer(string strChannel);
 bool                       fLoadANIvariables(ANI_Variables &ANIvar);
 bool                       fLoadDSBvariables(DSB_Variables &DSBvar);
 bool                       fLoadALIvariables(ALI_Variables &ALIvar);
 bool                       fLoadADVvariables(ADV_Variables &ADVvar);
 bool                       fLoadCADvariables(CAD_Variables &CADvar);
 bool                       fReloadInitFileALI();
 bool                       fReloadInitFileANI();
 bool                       fReloadInitFileDSB();
 bool                       fReloadInitFileADV();
 bool                       fReloadInitFileCAD();
 sip_ng911_transfer_type    fNG911_TransferType(string strSIP_URI);
 string                     fInitXMLString(int i);
};

#endif
