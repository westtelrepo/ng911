/*****************************************************************************
* FILE: telephone_device_functions.cpp
* 
* DESCRIPTION: This file contains all of the functions implemented
*              by the Telephone_Device and Telphone Devices Classes
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//Globals
extern Port_Data                               objBLANK_ANI_PORT;
extern Port_Data                               objBLANK_ALI_PORT;
extern Port_Data                               objBLANK_WRK_PORT;
extern Port_Data			       objBLANK_AMI_PORT;
extern Text_Data			       objBLANK_TEXT_DATA;
extern Call_Data			       objBLANK_CALL_RECORD;
extern Freeswitch_Data                         objBLANK_FREESWITCH_DATA;
extern ALI_Data                                objBLANK_ALI_DATA;



// Constructor
Telephone_Devices::Telephone_Devices()
 {
  sem_init(&sem_tMutexTelephoneDeviceVector,0,1);
  timespecTimeLastAsteriskRestart.tv_sec  = 0;
  timespecTimeLastAsteriskRestart.tv_nsec = 0;
  NumberofLineViews                       = 0;
  NumberofTelephones                      = 0;
  NumberofParkingLots			  = 0;
  HighestWorkstationNumber                = 0;
  NumberofRoutingPrefixes                 = 0;
  FirstDeviceRegistered = false;
  vRoutingPrefixes.clear();
 }

//Destructor
Telephone_Devices::~Telephone_Devices(){sem_destroy(&sem_tMutexTelephoneDeviceVector);}

//Constructor
Telephone_Device::Telephone_Device()
 {
  strDeviceName.clear();
  eDeviceType                        = NO_PHONE_DEFINED;
  boolIsAnAudiocodes                 = false;
  boolRequiresRegistration           = false;
  boolMonitorStatus                  = true;
  timespecTimeLastRegistered.tv_sec  = 0;
  timespecTimeLastRegistered.tv_nsec = 0;
  boolInitialRegistration            = true;
  boolRegistered                     = false;
  boolFirstAlarm                     = false;
  intDeviceDownReminderThreshold     = intDEVICE_DOWN_REMINDER_SEC;
  intPositionNumber                  = 0;
  boolCanDialExtensions              = true;
  boolCanDialOutsideLine             = true;
  IPaddress.fClear();
  strCallerIDnumber.clear();
  strCallerIDname.clear();
  strNextGenURI.clear();
 }


void GUI_Line_View::fClear()
{
 Name.clear();
 Label.clear();
}

void GUI_Line_View::fDisplay(int i)
{
 if (!i) 
  { 
   cout << "          Shared Lines" << endl;
   cout << setw(5)  << left << "";
   cout << setw(20) << left << "Name";
   cout << setw(15) << left << "Label" << endl;
  } 
 else 
  {
   cout<< setw(5)   << left << i;
   cout << setw(20) << left << Name;
   cout << setw(15) << left << Label << endl;
  }

}


string Telephone_Devices::fRemoveRoutingPrefix(string strInput)
{
 string 	strReturnString;
 size_t         sz, found;
 size_t         iLengthofPrefix;
 string         strPrefix;

 
 sz = this->vRoutingPrefixes.size();
 strReturnString = strInput;
 if (sz == 0) {return strReturnString;}
 
 for (unsigned int i = 0; i < sz; i++){
  strPrefix = this->vRoutingPrefixes[i];
  iLengthofPrefix = strPrefix.length();

  if (!iLengthofPrefix)                    {continue;}
  if (iLengthofPrefix > strInput.length()) {continue;}

  found = strReturnString.find(strPrefix);
  if (found != 0)                          {continue;}
  else {return strReturnString.erase(0,iLengthofPrefix);}

 }

 return strReturnString;
}



bool Telephone_Devices::fIsAnAudiocodes(IP_Address objIPAddress)
{
 size_t sz;

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].IPaddress.stringAddress == objIPAddress.stringAddress)
    {    
     return Devices[i].boolIsAnAudiocodes;
    }
  }
 return false;

}

bool Telephone_Devices::fIsAnNG911SipTrunk(IP_Address objIPAddress)
{
 size_t sz;

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].IPaddress.stringAddress == objIPAddress.stringAddress)
    {    
     return (Devices[i].eDeviceType == NG911SIPTRUNK);   
    }
  }
 return false;

}

int Telephone_Devices::fPositionNumberFromChannelName(string strChannelName)
{
 size_t sz, szLines, found;
 string SearchName;

 sz = Devices.size();
 if(strChannelName.empty()) {return 0;}

 SearchName = FreeswitchUsernameFromChannel(strChannelName);

 if(SearchName.empty()) {return 0;}

 for (unsigned int i = 0; i < sz; i++)
  {
   szLines = Devices[i].vPhoneLines.size();
   for (unsigned int j = 1; j < szLines; j++)
    {
     found = SearchName.find(Devices[i].vPhoneLines[j].LineRegistrationName);
     if (found != string::npos) {
      if (Devices[i].vPhoneLines[j].LineRegistrationName.length() != SearchName.length()) {continue;}
      else                                                                                {return Devices[i].intPositionNumber;}
     } 
    }
  }
 return 0;
}



int Telephone_Devices::fPostionNumberFromIPaddress(string strIPaddress)
{
 size_t sz;

 sz = Devices.size();
 if(strIPaddress.empty()) {return 0;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].IPaddress.stringAddress == strIPaddress)
   {return Devices[i].intPositionNumber;}
  }
 return 0;
}

string Telephone_Devices::fIPaddressFromPositionNumber(int j)
{
 size_t sz;

 sz = Devices.size();
 if(j <= 0) {return "";}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].intPositionNumber == j)
   {return Devices[i].IPaddress.stringAddress;}
  }
 return "";
}

int Telephone_Devices::fIndexofTelephonewithRegistrationName(string strName)
{
 int 	intLineNumber;
 size_t sz;

 if (strName.empty()) {return -1;}

 sz = Devices.size();


 for (unsigned int i =0; i< sz; i++)
  {
   switch (Devices[i].eDeviceType)
    {
     case GUI_PHONE: case NON_GUI_PHONE: case BOTH_PHONES: case POLYCOM_670: case POLYCOM_VVX:
          intLineNumber = Devices[i].fLineNumberWithRegistrationName(strName);
          if (intLineNumber > 0) {return i;}
     default:
          break;
    }
  }
 return -1;
}

string Telephone_Devices::fCallerIDnameofTelephonewithRegistrationName(string strName)
{
 int index = -1;

 index = fIndexofTelephonewithRegistrationName(strName);
 if (index < 0) {return  "Unknown";}

 return this->Devices[index].strCallerIDname;
}

string Telephone_Devices::fCallerIDnameofTelephonewithPosition(int iPosition)
{
 int index = -1;

 index = fIndexWithPositionNumber(iPosition);
 if (index < 0) {return  "Unknown";}

 return this->Devices[index].strCallerIDname;
}

string Telephone_Devices::fCallerIDnumberofTelephonewithPosition(int iPosition)
{
 int index = -1;

 index = fIndexWithPositionNumber(iPosition);
 if (index < 0) {return  "Unknown";}

 return this->Devices[index].strCallerIDnumber;
}


int Telephone_Devices::fFindTelephoneLineNumber(string strName)
{
 int 	intLineNumber;
 size_t sz;

 if (strName.empty()) {return 0;}

 sz = Devices.size();


 for (unsigned int i =0; i< sz; i++)
  {
   switch (Devices[i].eDeviceType)
    {
     case GUI_PHONE: case NON_GUI_PHONE: case BOTH_PHONES: case POLYCOM_670: case POLYCOM_VVX:
          intLineNumber = Devices[i].fLineNumberWithRegistrationName(strName);
          if (intLineNumber > 0) {return intLineNumber;}
     default:
          break;
    }
  }
 return 0;
}

bool Telephone_Devices::fLineNumberAtPositionIsShared(Channel_Data objData)
{
 size_t sz; 

 if (objData.iLineNumber <= 0)       { return false;}
 if (objData.iPositionNumber <= 0)   { return false;}
 sz = Devices.size();

 for (unsigned int i =0; i< sz; i++)
  {
   switch (Devices[i].eDeviceType)
    {
     case GUI_PHONE: case NON_GUI_PHONE: case BOTH_PHONES: case POLYCOM_670: case POLYCOM_VVX:
          if (Devices[i].intPositionNumber == objData.iPositionNumber)
           {
            int szLines = Devices[i].vPhoneLines.size();
            if (objData.iLineNumber > szLines) { SendCodingError("telephone_device_functions.cpp - Coding Error in fLineNumberIsShared() iLineNumber too large"); return false;}
              return Devices[i].vPhoneLines[objData.iLineNumber].boolSharedLine;
           }
     default:
          break;
    }
  } 

 SendCodingError("telephone_device_functions.cpp - Coding Error in fLineNumberIsShared() line number not found");
 return false;
}

bool Telephone_Devices::fLineNumberAtPositionIsShared(int iLineNumber, int iPosition)
{
 size_t sz; 

 if (iLineNumber <= 0) { return false;}
 if (iPosition <= 0)   { return false;}
 sz = Devices.size();

 for (unsigned int i =0; i< sz; i++)
  {
   switch (Devices[i].eDeviceType)
    {
     case GUI_PHONE: case NON_GUI_PHONE: case BOTH_PHONES: case POLYCOM_670: case POLYCOM_VVX:
          if (Devices[i].intPositionNumber == iPosition)
           {
            int szLines = Devices[i].vPhoneLines.size();
            if (iLineNumber > szLines) { SendCodingError("telephone_device_functions.cpp - Coding Error in fLineNumberIsShared() iLineNumber too large"); return false;}
              return Devices[i].vPhoneLines[iLineNumber].boolSharedLine;
           }
     default:
          break;
    }
  } 

 SendCodingError("telephone_device_functions.cpp - Coding Error in fLineNumberIsShared() line number not found");
 return false;
}

int Telephone_Devices::fDetermine_Line_Number(string strName, int iPos)
{
 int                                    iDeviceIndex;
 int                                 	iLineNumber;


 if (iPos <= 0) 					{return 0;}
 if (strName.empty())					{return 0;}

 iDeviceIndex = fIndexWithPositionNumber(iPos);
 if (iDeviceIndex < 0) {return 0;}

 iLineNumber = Devices[iDeviceIndex].fLineNumberWithRegistrationName(strName);
 if (iLineNumber < 0) {return 0;}

 return iLineNumber;
}






int Telephone_Devices::fDetermine_Shared_Line_Phone_LineIndex(int GUIlineIndex, int iPos)
{
 int                                    iDeviceIndex;
 int                                 	iLineNumber;

 // this function uses the GUI Line View to get the channel name and then get the line number of the index
 // this function does not work if the gui line view is blank !!!!!! 
 if (iPos <= 0) 					{return 0;}
 if (GUIlineIndex > ( (int) NumberofLineViews) )	{return 0;}
 if (GUIlineIndex <= 0)                                 {return 0;}


 iDeviceIndex = fIndexWithPositionNumber(iPos);
 if (iDeviceIndex < 0) {return 0;}

 iLineNumber = Devices[iDeviceIndex].fLineNumberWithRegistrationName(GUILineView[GUIlineIndex].Name);
 if (iLineNumber < 0) {return 0;}
 if (! Devices[iDeviceIndex].vPhoneLines[iLineNumber].boolSharedLine) {return 0;}
 return Devices[iDeviceIndex].vPhoneLines[iLineNumber].LineIndex;
}

int Telephone_Devices::fDetermine_Shared_Line_Phone_LineIndex(string strInput, int iPos)
{
 string 				strData;
 int                                    iDeviceIndex;
 int                                 	iLineNumber;

 if (iPos <= 0) 	{return 0;}
 if (strInput.empty()) 	{return 0;}

 //Data contained in ANI var (older version of freeswitch )
  strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_ANI_WO_COLON);
  
  //Data contained in Caller-Username in newer version ANI var not present ...
  if (strData.empty()) { strData = ParseFreeswitchData(strInput, FREESWITCH_CLI_CALLER_USER_NAME_WO_COLON); }

  if (strData.empty()) {return 0;}

  iDeviceIndex = fIndexWithPositionNumber(iPos);
  if (iDeviceIndex < 0) {return 0;}

  iLineNumber = Devices[iDeviceIndex].fLineNumberWithRegistrationName(strData);
  if (iLineNumber < 0) {return 0;}
  if (! Devices[iDeviceIndex].vPhoneLines[iLineNumber].boolSharedLine) {return 0;}
  return Devices[iDeviceIndex].vPhoneLines[iLineNumber].LineIndex; 
}

int Telephone_Device::fLineIndexofSharedLine(string strName)
{
 size_t sz = vPhoneLines.size();

 // obj at 0 is blank ... start at 1
 for (unsigned int i =1; i < sz; i++){
  if (!vPhoneLines[i].boolSharedLine) {continue;}
  if (vPhoneLines[i].LineRegistrationName == strName) {return vPhoneLines[i].LineIndex;}
 } 

 return 0;
}

int Telephone_Devices::fIndexofLineView(string strName)
{
 // the index of a shared line is only meaningful with a position number .
 // i.e. the index (line number) may be different between phones.
 // furthermore the index(line number) of the polycom is stored elsewhere and most likely is different
 if (strName.empty()) {return 0;}

 for (unsigned int i =1; i<= NumberofLineViews; i++)
  {
   if (GUILineView[i].Name == strName) {return i;}
  }
 return 0;
}

int  Telephone_Devices::fIndexWithDeviceName(string strName)
{
 // note semaphore must be locked before calling this function!
 size_t sz;

 if (strName.empty()) {return -1;}

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].strDeviceName == strName) {return i;}
  }
 return -1;
}

int  Telephone_Devices::fIndexWithIPaddress(IP_Address objIPAddress)
{
 // note semaphore must be locked before calling this function!
 size_t sz;

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].IPaddress.stringAddress == objIPAddress.stringAddress) {return i;}
  }
 return -1;
}

int  Telephone_Devices::fIndexWithPositionNumber(int intPosition)
{
 // note semaphore must be locked before calling this function!
 size_t sz;
 if(!intPosition) {return -1;}

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   if (Devices[i].intPositionNumber == intPosition) {return i;}
  }
 return -1;
}


string Telephone_Devices::fTelMonitorString() {
 int 	iTel = 0;
 int 	iTelup = 0;
 size_t sz;

 sz = Devices.size();

 for (unsigned int i = 0; i < sz; i++) {
  if (!Devices[i].boolMonitorStatus) {continue;}
  switch (Devices[i].eDeviceType) {
   case GUI_PHONE: case NON_GUI_PHONE: case BOTH_PHONES: case POLYCOM_670: case POLYCOM_VVX: case YEALINK:
    iTel++;
    if (!Devices[i].boolFirstAlarm) {
     iTelup++;
    }
    break;
   default:
    break;
  }
 }

  return int2strLZ(iTelup) + int2strLZ(iTel);
}

string Telephone_Devices::fACMonitorString() {
 int 	iAC = 0;
 int 	iACup = 0;
 size_t sz;

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++) {
  switch (Devices[i].eDeviceType) {
   case AUDIOCODES:
    iAC++;
    if (!Devices[i].boolFirstAlarm) {
     iACup++;
    }
    break;
   default:
    break;
  }
 }

 if (iAC) {
  return int2strLZ(iACup) + int2strLZ(iAC);
 }
 
 return "";
}



void Telephone_Devices::fDisplay()
{
 vector <string>::size_type   sz;
 
 

 cout << endl;
  

 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++){Devices[i].fDisplay(i+1, (i==0));}
  cout << endl;


  cout << endl;

  sz = GUILineView.size();
  for (unsigned int i = 0; i < sz; i++){GUILineView[i].fDisplay(i);}
   cout << endl;

}

int Telephone_Device::fLineNumberWithRegistrationName(string strName)
{
 vector <string>::size_type   sz= vPhoneLines.size();
 string strSearch;
 size_t found;

 strSearch = RemoveSIPcolon(strName);

 // zero is not used ...
 for (unsigned int i = 1; i < sz; i++)  {
   found = strSearch.find(vPhoneLines[i].LineRegistrationName);
   if (found !=string::npos){
    if (vPhoneLines[i].LineRegistrationName.length() != strName.length() ) {continue;}
    else                                                                   {return i;}
   } 
 }
 return -1;
}



void Telephone_Device::fDisplay(int i, bool showLabel)
{
 vector <string>::size_type   sz = vPhoneLines.size();

 if (showLabel)
  {
   cout << endl;
   cout <<  setw(5) << left << "";
   cout << setw(20) << left << "Device Name";
   cout << setw(25) << left << "MAC Address";
   cout << setw(15) << left << "Registered";
   cout << setw(15) << left << "Monitor";
   cout << setw(15) << left << "Pos Number";
   cout << setw(20) << left << "IP Address" << endl;
  }


 cout << setw(5)  << left << i;
 cout << setw(20) << left << strDeviceName;
 cout << setw(25) << left << strMACaddress;
 if      (!boolRequiresRegistration) {cout << setw(15) << left << "N/A";}
 else if (boolRegistered)            {cout << setw(15) << left << "true";}
 else                                {cout << setw(15) << left << "false";}
 if      (boolMonitorStatus)         {cout << setw(15) << left << "true";}
 else                                {cout << setw(15) << left << "false";}
 cout << setw(15) << left << intPositionNumber;
 cout << setw(20) << left << IPaddress.stringAddress;

 if (sz == 0) {cout << endl; return;}

 cout << endl;
 cout << "Caller ID Name:   " << strCallerIDname << endl;;
 cout << "Caller ID Number: " << strCallerIDnumber << endl;
 cout << "Next Gen URI:     " << strNextGenURI << endl;

 for (unsigned int i = 1; i < sz; i++)    {
     cout << left << "Line "       << setw(5) << left << i;
     cout << left << "Line Index " << setw(4) << left << vPhoneLines[i].LineIndex << setw(20) << left << vPhoneLines[i].LineRegistrationName;
     cout << setw(10) << left << " Dial Out-> " << vPhoneLines[i].OutboundDial << " Shared-> " <<  vPhoneLines[i].boolSharedLine << endl;
 }


 cout << endl;
}


string Telephone_Devices::fMWIaccountofDevicewithExternGateway(string strGateway) {

 size_t ph_sz;
 size_t sz        = Devices.size();

 string strReturn = "";
 extern string strASTERISK_AMI_IP_ADDRESS;

 for (unsigned int i = 0; i < sz; i++) {

  ph_sz = Devices[i].vPhoneLines.size();

  for (unsigned int j = 0; j < ph_sz; j++) {

   if (Devices[i].vPhoneLines[j].strExtMWIgateway == strGateway) {

    strReturn  = "sip:";
    strReturn += Devices[i].vPhoneLines[j].LineRegistrationName;   
    strReturn += "@";
    strReturn += strASTERISK_AMI_IP_ADDRESS;
    return  strReturn;     
   }
  }
 }
 return "";
}


bool Telephone_Devices::fLoadExtMWIGateway(string strExtMWIGateway, int iPhone, string strMWILineReg) {

 size_t sz    = Devices.size();
 int    index = -1;
 int    LineNumber = -1;

  for (unsigned int i = 0; i < sz; i++) {
   if (iPhone == Devices[i].intPositionNumber) {index = i; break;}
  }
  if (index < 0)       {return false;}
 
  LineNumber = Devices[index].fLineNumberWithRegistrationName(strMWILineReg);
  if (LineNumber < 0)  {return false;}

  Devices[index].vPhoneLines[LineNumber].strExtMWIgateway = strExtMWIGateway;
  return true;
}


bool Telephone_Devices::fLoadDevice(int iWorkStation, string strData, string strURIData, bool bMonitorStatus)
{
 Telephone_Device    Device;

 if (!iWorkStation)   {return false;}
 if (strData.empty()) {return false;}

 Device.strDeviceName     = strData;
 Device.intPositionNumber = iWorkStation;
 Device.boolRequiresRegistration = true;
 Device.boolMonitorStatus = bMonitorStatus;
 Device.strNextGenURI     = strURIData; 
 clock_gettime(CLOCK_REALTIME, &Device.timespecTimeLastRegistered);
 Devices.push_back(Device);
 return true;
}

bool Telephone_Devices::fLoadMACaddress(int iWorkStation, string strData) 
{
 size_t sz    = Devices.size();
 int    index = -1;
 
 if (!iWorkStation)   {return false;}
 if (strData.empty()) {return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (iWorkStation == Devices[i].intPositionNumber) {index = i; break;}
  }
 if (index < 0)                                      {return false;}

 Devices[index].strMACaddress = strData;

 return true;
}

bool Telephone_Devices::fcanDialthisNumber(string strGatewayName, string strNumber)
{
 int Index;

 Index = fIndexWithDeviceName(strGatewayName);
 if (Index < 0) {return false;}

 // need to add REGEX's

 switch (strNumber.length())
  {
   case 0: case 1: case 2:
           return false;
   case 3: case 4:
           return Devices[Index].boolCanDialExtensions;
   default:
           return Devices[Index].boolCanDialOutsideLine;

  }
  
 return true;
}

bool Telephone_Devices::fLoadNumberofLines(int iWorkStation, string strData)
{
 size_t sz    = Devices.size();
 size_t found;
 int    index = -1;
 
 if (!iWorkStation)   {return false;}
 if (strData.empty()) {return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (iWorkStation == Devices[i].intPositionNumber) {index = i; break;}
  }
 if (index < 0)                                      {return false;}

 found = strData.find_first_not_of("0123456789");
 if (found != string::npos)                          {return false;}

 Devices[index].iNumberOfLines = char2int(strData.c_str());

 return true;
}


bool Telephone_Devices::fLoadIPaddress(int iWorkStation, string strData) 
{
 size_t sz    = Devices.size();
 int    index = -1;
 
 if (!iWorkStation)   {return false;}
 if (strData.empty()) {return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (iWorkStation == Devices[i].intPositionNumber) {index = i; break;}
  }
 if (index < 0)                                      {return false;}

 Devices[index].IPaddress.stringAddress = strData;

 return Devices[index].IPaddress.fIsValid_IP_Address();
}

bool Telephone_Devices::fLoadDeviceType(int iWorkStation, string strData) 
{
 size_t              sz    = Devices.size();
 int                 index = -1;
 Telephone_Data_type eType;
 
 if (!iWorkStation)   {return false;}
 if (strData.empty()) {return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (iWorkStation == Devices[i].intPositionNumber) {index = i; break;}
  }
 if (index < 0)                                      {return false;}

 eType = TelephoneDataType(strData);
 Devices[index].eDeviceType = eType;

 return true;
}

bool Telephone_Devices::fLoadCallerIdName(int iWorkStation, string strInput)
{
 size_t sz    = Devices.size();
 int    index = -1;
 
 if (!iWorkStation)   {return false;}
 if (strInput.empty()) {return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (iWorkStation == Devices[i].intPositionNumber) {index = i; break;}
  }
 if (index < 0)                                      {return false;}


 Devices[index].strCallerIDname = strInput;
 return true;
}
bool Telephone_Devices::fLoadCallerIdNumber(int iWorkStation, string strInput)
{
 size_t sz    = Devices.size();
 int    index = -1;
 
 if (!iWorkStation)   {return false;}
 if (strInput.empty()) {return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (iWorkStation == Devices[i].intPositionNumber) {index = i; break;}
  }
 if (index < 0)                                      {return false;}


 Devices[index].strCallerIDnumber = strInput;
 return true;
}

bool NG911SIPTrunkName(string strName)
{
 size_t found;

 found = strName.find("ESRP");
 if (found != string::npos)     {return true;}
 found = strName.find("esrp");
 if (found != string::npos)     {return true;}

 return false;
}



bool Telephone_Devices::fLoadIPGateway(string strName, string strIPData, bool boolExt,bool boolOut,bool boolIsAudiocodes)
{
 Telephone_Device Device;
 string           strIPaddr = strIPData;

 strIPaddr = RemoveTrailingSpaces(strIPaddr); 
 strIPaddr = RemoveLeadingSpaces(strIPaddr);

 sem_wait(&sem_tMutexTelephoneDeviceVector);
 
  Device.strDeviceName = strName;
  Device.boolIsAnAudiocodes = boolIsAudiocodes;
  if (boolIsAudiocodes)               {Device.eDeviceType = AUDIOCODES;    Device.boolRequiresRegistration = true;}
  else if (NG911SIPTrunkName(strName)){Device.eDeviceType = NG911SIPTRUNK; Device.boolRequiresRegistration = false;}
  else                                {Device.eDeviceType = IPGATEWAY;     Device.boolRequiresRegistration = false;}
  Device.IPaddress.stringAddress = strIPaddr;
  Device.boolCanDialExtensions   = boolExt;
  Device.boolCanDialOutsideLine  = boolOut;  
  if (!Device.IPaddress.fIsValid_IP_Address()) {return false;}                                
  clock_gettime(CLOCK_REALTIME, &Device.timespecTimeLastRegistered);
  Devices.push_back(Device);

 sem_post(&sem_tMutexTelephoneDeviceVector);
 return true;
}

string Telephone_Device::DeviceString()
 {
  switch (eDeviceType)
   {
    case GUI_PHONE: case NON_GUI_PHONE: case BOTH_PHONES:
      return "Phone";
    case AUDIOCODES:
      return "Audiocodes";
    case IPGATEWAY:
      return "IP Gateway";
    case NG911SIPTRUNK:
      return "SIP Trunk";
    default:
      return "";
   }

 }

int Telephone_Device::fLineNumberWithIndex(int Index)
{
 for(unsigned int i=1; i < this->vPhoneLines.size(); i++) {
  if (vPhoneLines[i].LineIndex == Index) {return i;}
 }
 return  0;
}


int Telephone_Device::fLineIndexofLineNumber(int line)
{
 if ((line <= 0)||(line >= ((int) this->vPhoneLines.size()))) {return 0;}

 return  vPhoneLines[line].LineIndex;
}

bool Telephone_Device::fCheckTimeout()
{
 struct timespec        timespecTimeNow;
 long double	        ldTimeDiff = 0;
 string                 strPingStatus;
 MessageClass           objMessage;

 if(!boolRequiresRegistration) {return true;}
			
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 if (!boolIsMaster) {timespecTimeLastRegistered = timespecTimeNow; return false;}

 ldTimeDiff = time_difference(timespecTimeNow, timespecTimeLastRegistered);

 if(ldTimeDiff > intDEVICE_DOWN_THRESHOLD_SEC)
  {
   boolRegistered = false;
   if (!IPaddress.stringAddress.length())                         {return false;}


   if ((!boolMonitorStatus) && (boolFirstAlarm))
    {
     boolFirstAlarm = false;
     intDeviceDownReminderThreshold = 0;
     objMessage.fMessage_Create(LOG_INFO, 379, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_379b,  strDeviceName);
     enQueue_Message(ANI,objMessage); 
     return false;    
    }
   else if (!boolMonitorStatus) {return false;}


   if (!boolFirstAlarm)
    {
     if (fPingDevice()) {strPingStatus = "Successful";}
     else               {strPingStatus = "Failed";}
     boolFirstAlarm = true;
     objMessage.fMessage_Create(LOG_ALARM, 386, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                                ANI_MESSAGE_386,  strDeviceName, seconds2time(int(ldTimeDiff)), strPingStatus);
     enQueue_Message(ANI,objMessage);
    }
   else if (ldTimeDiff > intDeviceDownReminderThreshold)
    {
     if (fPingDevice()) {strPingStatus = "Successful";}
     else               {strPingStatus = "Failed";}
     intDeviceDownReminderThreshold += intDEVICE_DOWN_REMINDER_SEC;
     objMessage.fMessage_Create(LOG_WARNING, 387, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                ANI_MESSAGE_387, strDeviceName, seconds2time(int(ldTimeDiff)), strPingStatus);
     enQueue_Message(ANI,objMessage);
    }// end if (!boolFirstAlarm) else if  (ldTimeDiff > intDeviceDownReminderThreshold)

  }// end if(ldTimeDiff > intDEVICE_DOWN_THRESHOLD_SEC)
 

 return (ldTimeDiff > intDEVICE_DOWN_THRESHOLD_SEC);
}

bool Telephone_Device::fPingDevice()
{
 Ping                           PingAddress;
 int                            intRC;
 extern IP_Address              HOST_IP;
 
 if(!IPaddress.stringAddress.length()) {return false;}
#ifdef IPWORKS_V16
 PingAddress.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 PingAddress.SetLocalHost((char*)HOST_IP.stringAddress.c_str());
 PingAddress.SetTimeout(intPING_TIMEOUT);
 PingAddress.SetPacketSize(1024);
 PingAddress.Config((char*)"AbsoluteTimeout=true");

 intRC = PingAddress.PingHost((char*)IPaddress.stringAddress.c_str());
 if (intRC) {return false;}
 else       {return true;}
}



void Telephone_Device::fClear()
{
   strDeviceName.clear();
   eDeviceType                        = NO_PHONE_DEFINED;
   strMACaddress.clear();
   boolIsAnAudiocodes                 = false;
   timespecTimeLastRegistered.tv_sec  = 0;
   timespecTimeLastRegistered.tv_nsec = 0;
   boolRegistered                     = false;
   boolFirstAlarm                     = false;
   intDeviceDownReminderThreshold     = intDEVICE_DOWN_REMINDER_SEC;
   intPositionNumber                  = 0;
   vPhoneLines.clear();
   iNumberOfLines                     = 0;
   boolCanDialExtensions              = true;
   boolCanDialOutsideLine             = true;
   IPaddress.fClear();
}

int  Telephone_Device::fIndexInPhoneLineVector(int index)
{
  size_t                                         sz;
 // this function should be semaphore locked prior to calling ...
 sz = vPhoneLines.size();
 for(unsigned int i = 1; i < sz; i++)
  {
   if (vPhoneLines[i].LineIndex == index) {return i;}
  }
 return 0;
}
int  Telephone_Device::fFirstOutboundLineIndex()
{
 size_t                                         sz;
 sz = vPhoneLines.size();
 for(unsigned int i = 1; i < sz; i++)
  {
   if (vPhoneLines[i].OutboundDial) {return vPhoneLines[i].LineIndex;}
  }
 return -1; 


}

bool Telephone_Device::fUpdateLinesInUse()
{
 XMLNode                                        MainNode, PolycomIPPhoneNode, CallLineInfoNode, LineKeyNumNode, LineStateNode, CallInfoNode, MutedNode;
 XMLResults                                     xe;
 string                                         strURL;
 ExperientHTTPPort                              HTTP_CallState_Port;
 int                                            iNumberofNodes;
 size_t                                         sz;
 size_t                                         iLineNumber;
 string                                         strLineNumber;
 string                                         strLineState;
 size_t                                         found;
 string                                         strMessage;
 int                                            Index;
 string                                         strNodeName;
 string						strMuted;
 bool                                           boolALLmuted = false;                               
 // this function should be semaphore locked prior to calling ...

 strURL = "http://" + IPaddress.stringAddress + "/polling/callstateHandler"; 

#ifdef IPWORKS_V16
 HTTP_CallState_Port.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
 HTTP_CallState_Port.SetURL(strURL.c_str());
 HTTP_CallState_Port.SetURLPort(80);
 HTTP_CallState_Port.SetLocalHost(NULL);
 HTTP_CallState_Port.SetAuthScheme(1);
 HTTP_CallState_Port.SetUser(strCTI_USERNAME.c_str());
 HTTP_CallState_Port.SetPassword(strCTI_PASSWORD.c_str());
// HTTP_CallState_Port.Config("Connection=Keep-Alive");
 HTTP_CallState_Port.SetOtherHeaders("Cache-Control: no-cache");
 HTTP_CallState_Port.SetPragma("no-cache");
 HTTP_CallState_Port.SetFollowRedirects(1);
 HTTP_CallState_Port.SetTimeout(2);
 HTTP_CallState_Port.Get(strURL.c_str());
 HTTP_CallState_Port.DoEvents();

//  //cout << HTTP_CallState_Port.objDataReceived.stringDataIn << endl;
 if (!HTTP_CallState_Port.objDataReceived.stringDataIn.length()) {return false;} 
// //cout << "HAS LENGTH" << endl;
 MainNode=XMLNode::parseString(HTTP_CallState_Port.objDataReceived.stringDataIn.c_str(),NULL,&xe);

 if (xe.error) {strMessage = "Telephone_Device::fUpdateLinesInUse() XML error -> ";strMessage += XMLNode::getError(xe.error);SendCodingError(strMessage);       return false;}
// //cout << "Parsed string" << endl;
// //cout << "Name -> " << MainNode.getName() << endl;
 if (MainNode.getName()) {
  strNodeName = MainNode.getName();
  if (strNodeName == "PolycomIPPhone") {
   PolycomIPPhoneNode = MainNode;
  }
  else {
   PolycomIPPhoneNode = MainNode.getChildNode("PolycomIPPhone");
  }
 }
 else {
  PolycomIPPhoneNode = MainNode.getChildNode("PolycomIPPhone");
 }

 if (PolycomIPPhoneNode.isEmpty())                                { return false;}

 iNumberofNodes = PolycomIPPhoneNode.nChildNode("CallLineInfo");
 ////cout << "Number of Nodes -> " << iNumberofNodes << endl;
 sz = vPhoneLines.size();

 for (int i = 0; i < iNumberofNodes; i++)
  {   
   CallLineInfoNode = PolycomIPPhoneNode.getChildNode("CallLineInfo", i);   
   LineKeyNumNode = CallLineInfoNode.getChildNode("LineKeyNum");
   if (LineKeyNumNode.isEmpty())                                 {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Missing LineKeyNum HTML TAG"); continue;}
   if (LineKeyNumNode.getText()) {strLineNumber = LineKeyNumNode.getText();} 
   else                                                          {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Missing Line Number"); continue;}
   found = strLineNumber.find_first_not_of("0123456789");
   if (found != string::npos)                                    {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Invalid Line Number" + strLineNumber); continue;}
   Index = char2int(strLineNumber.c_str());

   iLineNumber = fIndexInPhoneLineVector(Index);
   if (iLineNumber == 0)                                         {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Line Number not found->" + strLineNumber); continue;}   
   if (iLineNumber >=sz)                                         {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Index Too Large ? -> " + strLineNumber); continue;}
   LineStateNode = CallLineInfoNode.getChildNode("LineState");
   if (LineStateNode.isEmpty())                                  {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Missing LineState HTML TAG"); continue;}
   if (LineStateNode.getText()) {strLineState = LineStateNode.getText();} 
   else                                                          {SendCodingError( "Telephone_Device::fUpdateLinesInUse() Missing LineState Data"); continue;}
   strLineState = RemoveLeadingSpaces(RemoveTrailingSpaces(strLineState));
   vPhoneLines[iLineNumber].boolInUse = (!(strLineState == "Inactive"));

   vPhoneLines[iLineNumber].boolMuted = false; 
   CallInfoNode = CallLineInfoNode.getChildNode("CallInfo");
   if (!CallInfoNode.isEmpty()) {
    MutedNode = CallInfoNode.getChildNode("Muted");
    if (MutedNode.isEmpty()) {
     vPhoneLines[iLineNumber].boolMuted = false;
    } 
    else {
     if (MutedNode.getText()) {
      strMuted = MutedNode.getText();
      strMuted = RemoveLeadingSpaces(RemoveTrailingSpaces(strMuted));
      if (strMuted == "1") {vPhoneLines[iLineNumber].boolMuted = true; boolALLmuted = true;}
     }
    }
   }    
   // //cout << "iLineNumber " << iLineNumber << endl;
   ////cout << "Index " << Index << endl;
   // //cout << "State " << strLineState << endl;
   ////cout << "Muted " << strMuted << endl; 
  }

//Polycom Bug if one is muted all are muted ..... polycom only goes from active to end ...

 for (unsigned int i=1; i < sz; i++) {
  vPhoneLines[i].boolMuted = boolALLmuted;
 }


 return true;
}

bool Telephone_Devices::fDeviceWithThisWorkstationHasThisPSAPURI(int i, string strURI)
{
 int    index;
 bool   boolSame;

 index = fIndexWithPositionNumber(i);
 if (index < 0) {return false;}

 boolSame = (Devices[index].strNextGenURI == FQDNfromURI(strURI));

 return boolSame;
}


void Telephone_Devices::fCheckTimeouts() 
{
 size_t                 sz;
 bool                   AnyDeviceisRegistered = !FirstDeviceRegistered;
 struct timespec        timespecTimeNow;
 long double	        ldTimeDiff = 0;
 MessageClass           objMessage;


 //Disabled freeswitch restart .......

//extern Call_Data	                        objBLANK_CALL_RECORD; 
//extern Port_Data                                objBLANK_AMI_PORT;

 sem_wait(&sem_tMutexTelephoneDeviceVector);
 sz = Devices.size();
 for (unsigned int i = 0; i < sz; i++)
  {
   Devices[i].fCheckTimeout();
   AnyDeviceisRegistered = (AnyDeviceisRegistered||Devices[i].boolRegistered);
  }
 sem_post(&sem_tMutexTelephoneDeviceVector);

 if(!AnyDeviceisRegistered)
  {
   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
   ldTimeDiff = time_difference(timespecTimeNow, timespecTimeLastAsteriskRestart);
   if (ldTimeDiff > (intDEVICE_DOWN_THRESHOLD_SEC * 2.0))
    {
     timespecTimeLastAsteriskRestart = timespecTimeNow;
 //    if (RestartFreeswitch()) { objMessage.fMessage_Create(LOG_ALARM, 810, objBLANK_CALL_RECORD, objBLANK_AMI_PORT, AMI_MESSAGE_810);}
 //    else                     { objMessage.fMessage_Create(LOG_ALARM, 811, objBLANK_CALL_RECORD, objBLANK_AMI_PORT, AMI_MESSAGE_811);}
 //     enQueue_Message(AMI, objMessage);
    }
  }
}

void Telephone_Devices::fRegisterDevice(string strInput, int intPortNum)
{
 string                    strData;
 string                    strKey;
 string                    strStatus;
 Port_Data                 objPortData;
 IP_Address                objIPAddress;
 MessageClass              objMessage;
 int                       index;
 long double               ldTimeDiff;
 extern bool               bool_BCF_ENCODED_IP;

 if (!intPortNum)                    {return;}
 if (intPortNum > NUM_ANI_PORTS_MAX) {return;}


 objPortData.fLoadPortData(ANI, intPortNum);

 strStatus    = ParseFreeswitchData(strInput, FREESWITCH_CLI_FIELD_STATUS );

 if (bool_BCF_ENCODED_IP)
  {
   strData = URLdecode(ParseFreeswitchData(strInput, FREESWITCH_CLI_FIELD_CONTACT ));
   strKey  = Determine_BCF_Key(strData);
   if (strKey == "NOKEY")
    {
     objIPAddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_FIELD_NETWORK_IP );
    }
   else
    {
     objIPAddress.fParseIPaddressUsingValueKey(strKey, strData);
    }
  }
 else 
  {
   objIPAddress.stringAddress = ParseFreeswitchData(strInput, FREESWITCH_CLI_FIELD_NETWORK_IP );
  }

 if ((strStatus != FREESWITCH_CLI_STATUS_REGISTERED_UDP_KEY)&&(strStatus != FREESWITCH_CLI_STATUS_REGISTERED_UDPNAT_KEY)) {return;}
 if (!objIPAddress.fIsValid_IP_Address())                   {return;}


 sem_wait(&sem_tMutexTelephoneDeviceVector);
 index = fIndexWithIPaddress(objIPAddress);
 if (index < 0) {sem_post(&sem_tMutexTelephoneDeviceVector); return;}


 clock_gettime(CLOCK_REALTIME, &Devices[index].timespecTimeLastRegistered);
 Devices[index].boolRegistered = true;
 FirstDeviceRegistered = true;

 if (!Devices[index].boolMonitorStatus)  {
   // No Longer an Alarm just info !!!!!!
   if (!Devices[index].boolFirstAlarm)    {
     Devices[index].boolFirstAlarm                 = true;
     Devices[index].intDeviceDownReminderThreshold = intDEVICE_DOWN_REMINDER_SEC;
     Devices[index].timespecTimeFirstRegistered = Devices[index].timespecTimeLastRegistered;
     objMessage.fMessage_Create(LOG_INFO, 379, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_379, Devices[index].strDeviceName );
     enQueue_Message(ANI,objMessage);
   }
   else {
     /*
     No longer send reminder .......
     ldTimeDiff = time_difference(Devices[index].timespecTimeLastRegistered, Devices[index].timespecTimeFirstRegistered);
     if ( ldTimeDiff > Devices[index].intDeviceDownReminderThreshold)  {
       Devices[index].intDeviceDownReminderThreshold += intDEVICE_DOWN_REMINDER_SEC;

       objMessage.fMessage_Create(LOG_WARNING, 380, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                  objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                  ANI_MESSAGE_380, Devices[index].strDeviceName, seconds2time(int(ldTimeDiff)));
       enQueue_Message(ANI,objMessage);
     }
    */
   }
  sem_post(&sem_tMutexTelephoneDeviceVector); return;
 }


 if (Devices[index].boolInitialRegistration) 
  {
   Devices[index].boolInitialRegistration        = false;
   objMessage.fMessage_Create(LOG_INFO, 375, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                              ANI_MESSAGE_375, Devices[index].strDeviceName, strStatus );
   enQueue_Message(ANI,objMessage);
  }
 else if (Devices[index].boolFirstAlarm)
  {
   Devices[index].boolFirstAlarm                 = false;
   Devices[index].intDeviceDownReminderThreshold = intDEVICE_DOWN_REMINDER_SEC;
   objMessage.fMessage_Create(LOG_ALARM, 388, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              ANI_MESSAGE_388, Devices[index].strDeviceName );
   enQueue_Message(ANI,objMessage);
  }

 sem_post(&sem_tMutexTelephoneDeviceVector);

}

void Local_Call_Rules::fDisplay()
{
 size_t szAreacodes, szNXXprefixes;

 string strAreaCodes   = "Local Area Codes   = [ ";
 string strNXXprefixes = "Local NXX prefixes = [ ";

 szAreacodes   = LocalAreaCode.size();
 szNXXprefixes = LocalNXXprefix.size();
 
 cout << endl;
 
 for (unsigned int i = 0; i < szAreacodes; i++)   {strAreaCodes += LocalAreaCode[i];    strAreaCodes += " ";}
 for (unsigned int i = 0; i < szNXXprefixes; i++) {strNXXprefixes += LocalNXXprefix[i]; strNXXprefixes += " ";}
 strAreaCodes   += "]";
 strNXXprefixes += "]";

 cout << endl;
 cout << strAreaCodes << endl;
 cout << strNXXprefixes << endl;
 cout << endl;  

}
bool Local_Call_Rules::fRuleForAreaCodeExists()
{
 if (LocalAreaCode.size() > 0) {return true;}
 else                          {return false;}
}

bool Local_Call_Rules::fRuleForNXXexists()
{
 if (LocalNXXprefix.size() > 0) {return true;}
 else                           {return false;}
}

bool Local_Call_Rules::fIsALocalAreaCode(string strAreaCode)
{
 size_t sz = LocalAreaCode.size();

 // assumed that a 3 Digit Number is Passed in, we will return False if not 3 digit
 if (strAreaCode.length() != 3) { SendCodingError("telephone_device_functions.cpp - Coding Error in Local_Call_Rules::fIsALocalAreaCode()"); return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (LocalAreaCode[i] == strAreaCode) {return true;}
  }

 return false;
}

bool Local_Call_Rules::fIsALocalExchangePrefix(string strNXXprefix)
{
 size_t sz = LocalNXXprefix.size();

 // assumed that a 3 Digit Number is Passed in, we will return False if not 3 digit
 if (strNXXprefix.length() != 3) { SendCodingError("telephone_device_functions.cpp - Coding Error in Local_Call_Rules::fIsALocalExchangePrefix()"); return false;}

 for (unsigned int i = 0; i < sz; i++)
  {
   if (LocalNXXprefix[i] == strNXXprefix) {return true;}
  }

 return false;
}


//constructor
Phone_Line::Phone_Line()
 {
  LineRegistrationName		= "Not Used";
  OutboundDial			= false;
  boolInUse                     = false;
  boolSharedLine                = false;
  LineIndex                     = 0;
  boolMuted			= false;
  strExtMWIgateway.clear();
 }
void Phone_Line::fClear()
{
 LineRegistrationName		= "Not Used";
 OutboundDial			= false;
 boolInUse                      = false;
 boolSharedLine                 = false;
 LineIndex                      = 0;
 boolMuted			= false;
 strExtMWIgateway.clear();
}

void Phone_Line::fLoadLineIndex(int index)
{
 LineIndex = index;
}

void Phone_Line::fLoadRegistrationName(string strName)
{
 LineRegistrationName = strName;
}

void Phone_Line::fLoadOutboundDial(bool bValue)
{
 OutboundDial = bValue;
}

void Phone_Line::fLoadSharedLine(string strBoolean)
{
 if      (strBoolean == "True") {boolSharedLine = true;}
 else if (strBoolean == "true") {boolSharedLine = true;}
 else if (strBoolean == "T")    {boolSharedLine = true;}
 else if (strBoolean == "t")    {boolSharedLine = true;}
 else if (strBoolean == "1")    {boolSharedLine = true;}
 else    {boolSharedLine = false;}
}

void Phone_Line::fLoadOutboundDial(string strData)
{
 if      (strData == "True") {OutboundDial = true;}
 else if (strData == "true") {OutboundDial = true;}
 else if (strData == "T")    {OutboundDial = true;}
 else if (strData == "t")    {OutboundDial = true;}
 else if (strData == "1")    {OutboundDial = true;}
 else    {OutboundDial = false;}
}

//Constructor
PhoneBookEntry::PhoneBookEntry()
{
 strCallerID.clear();
 strCustName.clear();
 strTransferGroup.clear();
 strTransferCode.clear();
 strSpeedDialAction.clear();
 strIconFileName.clear();
 ng911TransferType = NO_TRANSFER_DEFINED;
}


void PhoneBookEntry::fDisplay(int i, bool boolshowHeaders)
{
 if (boolshowHeaders) {
      cout << setw(5) << left << "Num" << setw(48) << left << "Customer Name" << setw(40) << left << "Transfer Number" << setw(20) << left << "Transfer Group" << setw(5) << left << "code" 
      << setw(20) << left << "Transfer Type" << setw(40) << left << "icon file" << endl;
 }

 cout << setw(5)  << left << i;
 cout << setw(48) << left << strCustName;
 cout << setw(40) << left << strCallerID; 
 cout << setw(20) << left << strTransferGroup;
 cout << setw(5)  << left << strTransferCode;
 cout << setw(20) << left << NG911_TRANSFER_TYPE(ng911TransferType);
 cout << setw(40) << left << strIconFileName << endl;
}


void PhoneBookEntry::fClear()
{
 strCallerID ="";
 strCustName ="Unknown";
 strTransferGroup.clear();
 strTransferCode.clear();
 strSpeedDialAction = XML_VALUE_SPEED_DIAL_ACTION_DEFAULT;
 strIconFileName = XML_VALUE_ICON_FILE_DEFAULT;
 ng911TransferType = NO_TRANSFER_DEFINED;
}

