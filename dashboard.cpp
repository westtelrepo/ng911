/*****************************************************************************
* FILE: freeswitchcli.cpp
* 
* DESCRIPTION: This file contains all of the functions integral to the freeswitchcli Thread
*
* Features:
*
*    3. Data Reliability Checks             Prevents bad data from causing system crashes
*    6. Heartbeats:                         Sends Heartbeat Message to Freeswitch
*    7. Email/Alarm Notification:           Informational messages on startup;
*                                           Warning messages for unusual activity;
*                                           Alarm messages for connection down/unavailable;
*                                           Reminder messages that alarm condition still exists
*    8. Denial of Service Protection:       Ignores packets from non-registered IP addresses;
*    9. Communications:                     Utilizes TCP communications for connection to Freeswitch;
*                                           If necessary converted to serial communication via Ethernet to serial converter
*   10. Self-Diagnostics:                   Built in diagnostics to help troubleshoot line errors, etc.
*   11. Threaded Implementation:            Provides efficient utilization and reduces resource monopolization
*   12. NENA Compliant:                     Complies with the “NENA Recommended Generic Standards for E9-1-1 PSAP Equipment”
*
* AUTHOR: 2/2/2010 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2010 Experient Corporation 
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"                          // XML Software

using namespace std;

void  *Dashboard_TCP_Listen_Thread(void *voidArg);
void  *Dashboard_Login_Thread(void *voidArg);
int   ConnectToDashboard(int i);
void  InitializeDSBport();
int   LogInToDashboard();
void  Ping_Dashboard_Event(union sigval union_sigvalArg);

extern void   *Dashboard_Messaging_Monitor_Event(void *voidArg);

// Globals

pthread_t               DashboardListenThread, DashboardLoginThread;
timer_t                 timer_tDashboardMessageMonitorTimerId;
timer_t			timer_tDashboard_Ping_Event_TimerId;
int			intDSB_HEARTBEAT_INTERVAL_SEC;
int			intDSB_PORT_DOWN_THRESHOLD_SEC;
int			intDSB_PORT_DOWN_REMINDER_SEC;



queue <DataPacketIn>	queueDSBPortData;
struct itimerspec       itimerspecDashboardPingInterval;
struct itimerspec	itimerspecDashboardMessageMonitorDelay;
struct itimerspec       itimerspecDashboardSendDelay;
extern sem_t            sem_tMutexDSBMessageQ;
extern sem_t            sem_tMutexDSBPortDataQ;
extern sem_t            sem_tDSBflag;
volatile bool           boolDashboard_LISTEN_THREAD_SHUTDOWN     = false;
volatile bool         	boolDSB_LOGIN_THREAD_SHUTDOWN            = false;
volatile unsigned long long int iDashboardTransmitNumber; 

//string                 	strDashboardTCP_Buffer;
string			strACTION_LOGIN_DSB		= "SKV1";

extern Port_Data                                objBLANK_KRN_PORT;
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data	                        objBLANK_CALL_RECORD; 
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_DSB_PORT;
extern queue <MessageClass>	                queue_objMainMessageQ;
extern queue <MessageClass>	                queue_objDSBMessageQ;
extern queue <ExperientDataClass>               queue_objDSBData;
extern Initialize_Global_Variables              INIT_VARS;

ExperientTCPPort              			DashboardPort;

/****************************************************************************************************
*
* Name:
*   Function void *DSB_Thread(void *voidArg)
*
****************************************************************************************************/
void *DSB_Thread(void *voidArg)
{
 int                      intRC;
 MessageClass             objMessage;
 string                   strLogin,strUsername,strSecret;
 struct sigevent          sigeventDashboardMessagingMonitorEvent;
 struct sigevent	  sigeventPingDashboardEvent;
 pthread_attr_t           Dashboardpthread_attr_tAttr;
 pthread_t                pthread_tDashboardmessagingThread;
 string                   strTemp;
 ExperientDataClass       objDashboardData;
 struct timespec          timespecRemainingTime;
 Thread_Data              ThreadDataObj;
 param_t                  params; 
 DataPacketIn             objPortDataPacket;

 map <string,string>      MasterMap, NewMap;


 extern vector <Thread_Data> 	ThreadData;
 extern Thread_Trap         	OrderlyShutDown;
 extern Thread_Trap             UpdateVars;
 extern sem_t			sem_tMutexDSBInputQ;

 int	ProcessDSBPortData(DataPacketIn objDataPacket);
 int 	ProcessDSBCommands(ExperientDataClass objData);
 void   LoadMap(map <string,string> *objMap);
 string DashboardUpdate();

 pthread_attr_init(&Dashboardpthread_attr_tAttr);

/*
 // set CPU affinity
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING,805, objBLANK_CALL_RECORD, objBLANK_DSB_PORT, KRN_MESSAGE_142); enQueue_Message(DSB,objMessage);}
*/

 ThreadDataObj.strThreadName ="DSB";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();

 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in Dashboard.cpp::Dashboard_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,650, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            DSB_MESSAGE_650, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(DSB,objMessage);


 iDashboardTransmitNumber = 1; 
 string strEOL       = "\r\n";


// DashboardPort.SetEOL((char*)strEOL.c_str(), 2);
// DashboardPort.SetLocalHost(NULL);
// DashboardPort.SetLocalPort(Freeswitch_AMI_PORT_NUMBER+1);
// DashboardPort.SetRemotePort(Freeswitch_AMI_PORT_NUMBER);
// DashboardPort.SetRemoteHost((char*) strFreeswitch_AMI_IP_ADDRESS.c_str());
 InitializeDSBport();
 sleep(1);

 //Start DSB  Login Thread
 pthread_attr_setdetachstate(&Dashboardpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
 intRC = pthread_create(&DashboardLoginThread, &Dashboardpthread_attr_tAttr, *Dashboard_Login_Thread, (void*) 0);
 if (intRC) {SendCodingError("dashboard_cli.cpp - Unable to create DSB Login thread"); exit(1);}

 // set up Message ping Timed Event
 sigeventPingDashboardEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventPingDashboardEvent.sigev_notify_function 		        = Ping_Dashboard_Event;
 sigeventPingDashboardEvent.sigev_notify_attributes 			= NULL;
 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventPingDashboardEvent, &timer_tDashboard_Ping_Event_TimerId);
 if (intRC){Abnormal_Exit(AMI, EX_OSERR, KRN_MESSAGE_180, "timer_tDashboard_Ping_Event_TimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
 itimerspecDashboardPingInterval.it_value.tv_sec                       = DASHBOARD_PING_INTERVAL;	                        // Delay for n sec
 itimerspecDashboardPingInterval.it_value.tv_nsec                      = 0;	                				//           n nsec
 itimerspecDashboardPingInterval.it_interval.tv_sec                    = DASHBOARD_PING_INTERVAL;			        // if both 0 run once
 itimerspecDashboardPingInterval.it_interval.tv_nsec                   = 0;                 					//           n nsec
 timer_settime(timer_tDashboard_Ping_Event_TimerId, 0, &itimerspecDashboardPingInterval, NULL);	                                // arm timer


 params.intPassedIn = 1;
  // start DASHBOARD TCP listening thread
 pthread_attr_setdetachstate(&Dashboardpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
 intRC = pthread_create(&DashboardListenThread, &Dashboardpthread_attr_tAttr, *Dashboard_TCP_Listen_Thread, (void*) &params);
 if (intRC) {SendCodingError("Dashboard.cpp - Unable to create DSB Listen thread" ); exit(1);}


 intRC = pthread_create(&pthread_tDashboardmessagingThread, &Dashboardpthread_attr_tAttr, *Dashboard_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(DSB, EX_OSERR, DSB_MESSAGE_674);}


 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,655, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_655); 
 enQueue_Message(DSB,objMessage);

 
 // main loop ***************************************************************************
 do
 {
  intRC = sem_wait(&sem_tDSBflag);							// wait for data signal
   cout << "past DSB signal" << endl;
  // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
  if (intRC)   { 
   objMessage.fMessage_Create(LOG_WARNING,654, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_654); 
   enQueue_Message(DSB,objMessage); 
  } 
 
 // Update VARS Trap Area 
 if (boolUPDATE_VARS) {
    UpdateVars.boolDSBThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {
     nanosleep( &timespecNanoDelay, &timespecRemainingTime);
    }
    if (!boolSTOP_EXECUTION) { 
     if (INIT_VARS.DSBinitVariable.boolRestartDashboardPort){
      //cout << "restart DSB Port" << endl;
      DashboardPort.Disconnect();
      DashboardPort.DoEvents();
      sleep(1);
      pthread_join( DashboardListenThread, NULL); 
      InitializeDSBport();
      intRC = pthread_create(&DashboardListenThread, &Dashboardpthread_attr_tAttr, *Dashboard_TCP_Listen_Thread, (void*) &params);
      if (intRC) {SendCodingError("Dashboard.cpp - Unable to create DSB Listen thread recommend restart" ); }
     }
    }  
   UpdateVars.boolDSBThreadReady = false;       
 }

 //Process Kernel Commands /lock Q
 intRC = sem_wait(&sem_tMutexDSBInputQ);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexDSBInputQ, "sem_wait@sem_tMutexDSBInputQ in *Dashboard_Thread()", 1);}

 while ((!boolSTOP_EXECUTION )&&(!queue_objDSBData.empty()))  {

  objDashboardData.fClear_Record();
  objDashboardData = queue_objDSBData.front();
  queue_objDSBData.pop();
  ProcessDSBCommands(objDashboardData);
 }
 sem_post(&sem_tMutexDSBInputQ);

 //Process Port Data /lock Q
 intRC = sem_wait(&sem_tMutexDSBPortDataQ);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexDSBPortDataQ, "sem_wait@sem_tMutexDSBPortDataQ in *Dashboard_Thread()", 1);}

 while ((!boolSTOP_EXECUTION )&&(!queueDSBPortData.empty())) {

  objPortDataPacket.fClear();
  objPortDataPacket = queueDSBPortData.front();
  queueDSBPortData.pop();

  ProcessDSBPortData(objPortDataPacket);
 }
 sem_post(&sem_tMutexDSBPortDataQ);           
   



 } while (!boolSTOP_EXECUTION);
 // end main loop *********************************************************************** 

// //cout << "before timer delete" << endl;
 timer_delete(timer_tDashboard_Ping_Event_TimerId);
// //cout << "after timer delete" << endl;
// timer_delete(timer_tCLIMessageMonitorTimerId);
 pthread_join( DashboardListenThread, NULL); 
 pthread_join( DashboardLoginThread, NULL); 
 pthread_join( pthread_tDashboardmessagingThread, NULL); 
 OrderlyShutDown.boolDSBThreadReady = true;

 while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}       
 objMessage.fMessage_Create(0, 675, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_675);
 objMessage.fConsole();
 return NULL;
}// void *Dashboard_Thread(void *voidArg)




string DashboardUpdate() {
/*
  Main Worktable is assumed to be locked ....
  this function is primarily called from Main Thread.......
*/
  string         				strJSONoutput;
  JSON           				json;
  string         				strJSON_KEY = IPWORKS_RUNTIME_KEY;
  string                                        strFullVersionPrefix      = "{\"t\":\"full\",\"version\":{\"i\":";
  struct timespec				mtimespecTimeStamp,rtimespecTimeStamp; 
  map <string,string>                           objMap, objSendMap;
  map <string,string>::iterator                 cursor, searchIterator;

  string                                        strKeyPrefix;
  string                                        strValue;
  string					strLogin;
  int						intRC;

  extern string  				stringSERVER_HOSTNAME;
  extern string 				strDASHBOARD_API_KEY;
  extern vector <ExperientDataClass>      	vMainWorkTable; 
  extern map <string,string>     		objPreviousMap;
  extern string					stringVERSION_NUMBER;		
  extern volatile unsigned long long int 	iDashboardTransmitNumber; 
  extern ExperientTCPPort              		DashboardPort;

  extern void 	LoadMap(map <string,string> *objMap);
  extern bool 	SendMap(map <string,string> *objMap, unsigned long long int i, bool bfullmap);
  extern string	UUID_Generate_String();

  if (!DashboardPort.boolFreeswitchLoggedin) {
   objPreviousMap.clear(); 
   return "";
  }

  objMap.clear();
  objSendMap.clear();
  LoadMap(&objMap);

  //if previos map empty do this and return .....
  if (objPreviousMap.empty() ) {
   //Send entire map
   //need to create major version ?
  
   iDashboardTransmitNumber = 1;
   if (objMap.size() > 0) {
    SendMap(&objMap, iDashboardTransmitNumber, true);
   }
   objPreviousMap = objMap;
   return "";
  }
 


 
  // previous map was not empty ....
  for (cursor = objMap.begin(); cursor != objMap.end(); cursor ++) {

   searchIterator = objPreviousMap.find(cursor->first);
   if (searchIterator != objPreviousMap.end() ) {
    // do compare check of key .....
    if (cursor->second != searchIterator->second) {
     // add data to be sent to dashboard (it changed)
     //cout << "CHANGE" << endl;
     objSendMap.insert( pair<string,string> (cursor->first,cursor->second));
    }
    objPreviousMap.erase(searchIterator); 
   }
   else {
    //cout << "NEW" << endl;
    // Send data to dashboard ...it was not in old map
    objSendMap.insert( pair<string,string> (cursor->first,cursor->second));
   }
  }
  // whatever is left over send erase (key only) to dashboard ..... 
  for (cursor = objPreviousMap.begin(); cursor != objPreviousMap.end(); cursor ++) {
   //cout << "ERASE" << endl;
   objSendMap.insert( pair<string,string> (cursor->first,"null"));   
  }

  objPreviousMap.clear();
  objPreviousMap = objMap; 
 

 if (objSendMap.size() > 0) {
  iDashboardTransmitNumber++;
  SendMap(&objSendMap, iDashboardTransmitNumber, false);
 }

 return "";
}

bool SendMap(map <string,string> *objMap, unsigned long long int i, bool bfullmap) {

  string                                        strADDKeyValuePrefix      = "{\"t\":\"op\",\"op\":{\"i\":";
  string                                        strADDFullValuePrefix     = "{\"t\":\"full\",\"version\":1, \"kv\":{\"";
  string                                        strADDKeyValueAfterNumber = ",\"kv\":{\""; 
  string                                        strADDValuePrefix         = "\",\"v\":\"";
  string                                        strADDValueSuffix         = "\"}";
  string                                        strDataToSend;
  string					strKeyPrefix;
  bool						boolComma;
  bool						boolNULL;
  int						intRC;
  map <string,string>::iterator                 cursor;
  extern ExperientTCPPort              		DashboardPort;

   if (bfullmap) {
    strDataToSend = strADDFullValuePrefix;    
   }
   else {
    strKeyPrefix =  strADDKeyValuePrefix + int2str(i);
    strKeyPrefix += strADDKeyValueAfterNumber; 
    strDataToSend = strKeyPrefix;
   }
   boolComma = false;
   for (cursor = objMap->begin(); cursor != objMap->end(); cursor ++) {
    if (boolComma) {
     strDataToSend += ",\"";
    }
    strDataToSend += cursor->first;
    strDataToSend += "\":";
    if (cursor->second == "null") {
    boolNULL = true;
    strDataToSend += "null";
    }
    else {
     strDataToSend += "\"";
     strDataToSend += FindandReplaceALL(cursor->second, "\"","\\\"");
     strDataToSend += "\"";
    }
    boolComma = true;
   }

   if (bfullmap) {
    strDataToSend += "}}";
   }
   else {
    strDataToSend += "}}}";
   }
   ////cout << strDataToSend << endl;

   intRC = Transmit_Data(DSB, 1, strDataToSend.c_str(), strDataToSend.length(), 0);

   if (intRC) {
    //cout << "MAP SEND FAILURE CODE -> " << intRC << endl;
    return false;
   }

 return true;
}

void LoadMap(map <string,string> *objMap) {

  string         				strJSONoutput;
  string         				strJSON_KEY = IPWORKS_RUNTIME_KEY;
  JSON           				json;
  size_t					sz;
  struct timespec				mtimespecTimeStamp,rtimespecTimeStamp; 
  string					strTemp;
  trunk_type                 			eTrunkType;
  PSAP_STATUS 					objStatus;
  vector <PSAP_STATUS>                          vPSAPstatus;
  Workstation_Group                             WorkGroup;

  extern vector <ExperientDataClass>      	vMainWorkTable; 
  extern string					stringVERSION_NUMBER;
  extern Trunk_Type_Mapping			TRUNK_TYPE_MAP;
  extern Trunk_Sequencing                       Trunk_Map;
  extern ALISteering                    	ALI_Steering[intMAX_ALI_DATABASES + 1 ];
  extern RCC_Device              		RCCdevice[NUM_WRK_STATIONS_MAX+1];
  extern Workstation_Groups 			WorkstationGroups;
  extern string					stringPSAP_ID;

  int                                           intPosition;
  int                                           iPosAvailable;
  int						i911Calls;
  int						iAdminCalls;
  int						iMSRPCalls;
  int						index;
  int						intRC; 	
  bool						bPositionOn911Call[95];
  bool						bPositionOnAdminCall[95];
  bool						boolSinglePSAP;

  extern void AddTimeStamptoJSONobject(JSON *JSONobject, struct timespec RealtimeStamp, struct timespec MonotonictimeStamp);
  extern void AddCallDataToJSONobject(JSON *JSONobject, int i);
  extern void AddCallCountsToJSONobject(JSON *JSONobject);
  extern void AddPSAPDataToJSONobject(JSON *JSONobject, Workstation_Group WrkGroup);
  extern void AddTrunkDataToJSONobject(JSON *JSONobject, Trunk_Type objTrunk);
  extern void AddTrunkRotateDataToJSONobject(JSON *JSONobject, Trunk_Rotate_Group objRotateGroup);
  extern void AddPositionDataToJSONobject(JSON *JSONobject, WorkStation objWorkstation);
  extern void AddRCCPositionDataToJSONobject(JSON *JSONobject, int i);
  extern bool AddTelephonePositionDataToJSONobject(JSON *JSONobject, int i);
  extern bool AddTelephoneStatusDataToJSONobject(JSON *JSONobject, int i);

 for (unsigned int i = 0; i< 95; i++) {bPositionOn911Call[i] = bPositionOnAdminCall[i] = false;}
 json.SetRuntimeLicense((char*)strJSON_KEY.c_str());

 //add time
 clock_gettime(CLOCK_MONOTONIC, &mtimespecTimeStamp);
 clock_gettime(CLOCK_REALTIME, &rtimespecTimeStamp);
 AddTimeStamptoJSONobject(&json, rtimespecTimeStamp, mtimespecTimeStamp);
 strJSONoutput = json.GetOutputData(); 
 strTemp = "/time";
 objMap->insert( pair<string,string> (strTemp,strJSONoutput));
 json.Reset();
 json.Remove();
 json.Flush();

 //add Version
 json.StartObject();
 json.PutProperty("version", (char*) stringVERSION_NUMBER.c_str() , 2);
 json.EndObject();
 strTemp = "/version";
 strJSONoutput = json.GetOutputData();
 objMap->insert( pair<string,string> (strTemp,strJSONoutput));
 json.Reset();
 json.Remove();
 json.Flush();

 //add Trunk MAP
 for (int i = 0; i <= intNUM_TRUNKS_INSTALLED; i++) {
  if (!Trunk_Map.TrunkMap.Trunk[i].boolTrunkNumberInUse)         {continue;}
  AddTrunkDataToJSONobject(&json, Trunk_Map.TrunkMap.Trunk[i]);
  strTemp = "/trunk/" + int2str(i);
  strJSONoutput = json.GetOutputData();
  objMap->insert( pair<string,string> (strTemp,strJSONoutput));
  json.Reset();
  json.Remove();
  json.Flush();
 } 

 //add Rotate Groups
  sz = Trunk_Map.TrunkRotateGroup.size();
 for (unsigned int i = 0; i < sz; i++) {
  if (Trunk_Map.TrunkRotateGroup[i].RotateRule == NO_RULE)      {continue;}
  AddTrunkRotateDataToJSONobject(&json, Trunk_Map.TrunkRotateGroup[i]);
  strTemp = "/rotate_group/" + int2str(i);
  strJSONoutput = json.GetOutputData();
  objMap->insert( pair<string,string> (strTemp,strJSONoutput));
  json.Reset();
  json.Remove();
  json.Flush();
 } 

 //add calls
 sz = vMainWorkTable.size();
 i911Calls = 0;
 iAdminCalls = 0;
 iMSRPCalls = 0;
 for (unsigned int i = 0; i < sz; i++) {
  AddCallDataToJSONobject(&json, i);
  strTemp = "/call/" + vMainWorkTable[i].CallData.stringUniqueCallID;
  strJSONoutput = json.GetOutputData();
  objMap->insert( pair<string,string> (strTemp,strJSONoutput));
  json.Reset();
  json.Remove();
  json.Flush();
 }

// Lock Worktable Reset PSAP Count Data
/*************************************************************************************************************************************************/
 intRC = sem_wait(&sem_tMutexWRKWorkTable);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "dashboard.cpp sem_wait@sem_tMutexWRKWorkTable in fn LoadMap()", 1);}

 boolSinglePSAP = WorkstationGroups.WorkstationGroups.empty();
 if (boolSinglePSAP) {
  //Single PSAP
  objStatus.strPSAPname = stringPSAP_ID;
  objStatus.fClearCounters();

  for (int i = 1; i <= intNUM_WRK_STATIONS; i++) {
   //Static Position Data
    //RCC
   if (RCCdevice[i].boolContactRelayInstalled) {
    AddRCCPositionDataToJSONobject(&json, i);
    strTemp = "/position/" + int2str(i);
    strTemp +="/rcc";
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush();         
   }

   if (AddTelephonePositionDataToJSONobject(&json, i)) {
    strTemp = "/position/" + int2str(i);
    strTemp +="/phone";
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush();           
   }

   if (AddTelephoneStatusDataToJSONobject(&json, i)) {
    strTemp = "/position/" + int2str(i);
    strTemp +="/phone/status";
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush();           
   }

   if (WorkStationTable[i].boolActive) {
    objStatus.iPositionsOnline++;
    // PositionData 
    AddPositionDataToJSONobject(&json, WorkStationTable[i]);
    strTemp = "/position/" + int2str(i);
    strTemp +="/status";
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush();        
   }
  }
 }
 else {
  // MultiPSAP
  // determine positions online within each sub PSAP
  vPSAPstatus.clear();
  for (unsigned int i = 0; i< WorkstationGroups.WorkstationGroups.size(); i++) {
     if (WorkstationGroups.WorkstationGroups[i].strGroupName == "ALL")         {continue;}
      WorkstationGroups.WorkstationGroups[i].PSAPStatus.fClearCounters();

      for (unsigned int j = 0; j< WorkstationGroups.WorkstationGroups[i].WorkstationsInGroup.size(); j++)  {
       intPosition = WorkstationGroups.WorkstationGroups[i].WorkstationsInGroup[j];
       //Static Position Data
       //RCC
       if (RCCdevice[intPosition].boolContactRelayInstalled) {
        AddRCCPositionDataToJSONobject(&json, intPosition);
        strTemp = "/position/" + int2str(intPosition);
        strTemp +="/rcc";
        strJSONoutput = json.GetOutputData();
        objMap->insert( pair<string,string> (strTemp,strJSONoutput));
        json.Reset();
        json.Remove();
        json.Flush();         
       }

       if (AddTelephonePositionDataToJSONobject(&json, intPosition)) {
        strTemp = "/position/" + int2str(intPosition);
        strTemp +="/phone";
        strJSONoutput = json.GetOutputData();
        objMap->insert( pair<string,string> (strTemp,strJSONoutput));
        json.Reset();
        json.Remove();
        json.Flush();           
       }
 
       if (AddTelephoneStatusDataToJSONobject(&json, intPosition)) {
        strTemp = "/position/" + int2str(intPosition);
        strTemp +="/phone/status";
        strJSONoutput = json.GetOutputData();
        objMap->insert( pair<string,string> (strTemp,strJSONoutput));
        json.Reset();
        json.Remove();
        json.Flush();           
       }

       if (WorkStationTable[WorkstationGroups.WorkstationGroups[i].WorkstationsInGroup[j]].boolActive) {
        WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline++;
        //Position Data         
        AddPositionDataToJSONobject(&json, WorkStationTable[intPosition]);
        strTemp = "/position/" + int2str(intPosition);
        strTemp +="/status";
        strJSONoutput = json.GetOutputData();
        objMap->insert( pair<string,string> (strTemp,strJSONoutput));
        json.Reset();
        json.Remove();
        json.Flush();  
       } 
      }
     }
  }


  intRC = sem_post(&sem_tMutexWRKWorkTable); 
/*************************************************************************************************************************************************/
 // Calculate PSAP Count data.d
 for (unsigned int i = 0; i< sz; i++)   {

  eTrunkType = TRUNK_TYPE_MAP.Trunk[vMainWorkTable[i].CallData.intTrunk].TrunkType;

  if (vMainWorkTable[i].CallData.intTrunk > 94)                         {continue;}
//  //cout << "State code is -> " << vMainWorkTable[i].intCallStateCode << endl;
//  //cout << CallStateString(vMainWorkTable[i].intCallStateCode) << endl;

  if ((vMainWorkTable[i].intCallStateCode != 2)&&  
      (vMainWorkTable[i].intCallStateCode != 7)&&
      (vMainWorkTable[i].intCallStateCode != 6)&&
      (vMainWorkTable[i].intCallStateCode != 1)&&
      (vMainWorkTable[i].intCallStateCode != 4)&&
      (!vMainWorkTable[i].boolAbandoned))                               {continue;} // process connected or OnHold or abandoned all others continue ....


   switch(eTrunkType) {

    case CLID: case SIP_TRUNK:
     if (boolSinglePSAP) {
      if (vMainWorkTable[i].boolAbandoned) { 
       objStatus.iAdminCallsAbandoned++;     
       continue; 
      }
 
      objStatus.iAdminCalls++;
      if (vMainWorkTable[i].FreeswitchData.objRing_Dial_Data.boolIsOutbound) {
       objStatus.iOutboundAdminCalls++;
      }
      else {
       objStatus.iInboundAdminCalls++;
      }
      if ((vMainWorkTable[i].CallData.fNumPositionsOnHold() == vMainWorkTable[i].CallData.fNumPositionsInConference()) && (vMainWorkTable[i].CallData.fNumPositionsInConference() != 0) ) {
       objStatus.iAdminCallsOnHold++;
      }
      if (vMainWorkTable[i].CallData.ConfData.strParkTime.length()) { 
       objStatus.iAdminCallsParked++;
      } 
      if ((vMainWorkTable[i].boolRinging) && (!vMainWorkTable[i].boolConnect) && (!vMainWorkTable[i].boolAbandoned)) { 
       objStatus.iAdminCallsRinging++;
      }     
     }
     else {
      // Multi PSAP Site
      index = WorkstationGroups.IndexOfGroupWithTrunk(vMainWorkTable[i].CallData.intTrunk);
      if (index >=0) {
       if (vMainWorkTable[i].boolAbandoned) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iAdminCallsAbandoned++;     
        continue; 
       }

       WorkstationGroups.WorkstationGroups[index].PSAPStatus.iAdminCalls++;
       if (vMainWorkTable[i].FreeswitchData.objRing_Dial_Data.boolIsOutbound) {
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iOutboundAdminCalls++;
       }
       else {
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iInboundAdminCalls++;
       }
       if ((vMainWorkTable[i].CallData.fNumPositionsOnHold() == vMainWorkTable[i].CallData.fNumPositionsInConference()) && (vMainWorkTable[i].CallData.fNumPositionsInConference() != 0) ) {
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iAdminCallsOnHold++;
       } 
       if (vMainWorkTable[i].CallData.ConfData.strParkTime.length()) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iAdminCallsParked++;
       }
       if ((vMainWorkTable[i].boolRinging) && (!vMainWorkTable[i].boolConnect) && (!vMainWorkTable[i].boolAbandoned)) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iAdminCallsRinging++;
       }               
      }
     }
     break;

    case MSRP_TRUNK:
     if (boolSinglePSAP) {
      if (vMainWorkTable[i].boolAbandoned) { 
       objStatus.iMSRPCallsAbandoned++;     
       continue; 
      }

      objStatus.iMSRPCalls++;
      if ((vMainWorkTable[i].CallData.fNumPositionsOnHold() == vMainWorkTable[i].CallData.fNumPositionsInConference()) && (vMainWorkTable[i].CallData.fNumPositionsInConference() != 0) ) {
       objStatus.iMSRPCallsOnHold++;
      }
      if ((vMainWorkTable[i].boolRinging) && (!vMainWorkTable[i].boolConnect) && (!vMainWorkTable[i].boolAbandoned)) { 
       objStatus.iMSRPCallsRinging++;
      }    
     }
     else {
      // Multi PSAP Site
      index = WorkstationGroups.IndexOfGroupWithTrunk(vMainWorkTable[i].CallData.intTrunk);
      if (index >=0) {
       if (vMainWorkTable[i].boolAbandoned) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iMSRPCallsAbandoned++;     
        continue; 
       }

       WorkstationGroups.WorkstationGroups[index].PSAPStatus.iMSRPCalls++;
       if ((vMainWorkTable[i].CallData.fNumPositionsOnHold() == vMainWorkTable[i].CallData.fNumPositionsInConference()) && (vMainWorkTable[i].CallData.fNumPositionsInConference() != 0) ) {
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iMSRPCallsOnHold++;
       }
       if ((vMainWorkTable[i].boolRinging) && (!vMainWorkTable[i].boolConnect) && (!vMainWorkTable[i].boolAbandoned)) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.iMSRPCallsRinging++;
       }   
      }
     } 
     break;
    
    default:
     if (boolSinglePSAP) {
      if (vMainWorkTable[i].boolAbandoned) { 
       objStatus.i911CallsAbandoned++;     
       continue; 
      }

      objStatus.i911Calls++;
      if ((vMainWorkTable[i].CallData.fNumPositionsOnHold() == vMainWorkTable[i].CallData.fNumPositionsInConference()) && (vMainWorkTable[i].CallData.fNumPositionsInConference() != 0) ) {
       objStatus.i911CallsOnHold++;
      }
      if (vMainWorkTable[i].CallData.ConfData.strParkTime.length()) { 
       objStatus.i911CallsParked++;
      } 
      if ((vMainWorkTable[i].boolRinging) && (!vMainWorkTable[i].boolConnect) && (!vMainWorkTable[i].boolAbandoned)) { 
       objStatus.i911CallsRinging++;
      }       
     }
     else {
      // Multi PSAP Site
      index = WorkstationGroups.IndexOfGroupWithTrunk(vMainWorkTable[i].CallData.intTrunk);
      if (index >=0) {
       if (vMainWorkTable[i].boolAbandoned) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.i911CallsAbandoned++;     
        continue; 
       }

       WorkstationGroups.WorkstationGroups[index].PSAPStatus.i911Calls++;
       if ((vMainWorkTable[i].CallData.fNumPositionsOnHold() == vMainWorkTable[i].CallData.fNumPositionsInConference()) && (vMainWorkTable[i].CallData.fNumPositionsInConference() != 0) ) {
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.i911CallsOnHold++;
       }
       if (vMainWorkTable[i].CallData.ConfData.strParkTime.length()) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.i911CallsParked++;
       }
       if ((vMainWorkTable[i].boolRinging) && (!vMainWorkTable[i].boolConnect) && (!vMainWorkTable[i].boolAbandoned)) { 
        WorkstationGroups.WorkstationGroups[index].PSAPStatus.i911CallsRinging++;
       }       
      }
     } 
   } // end switch


  for (int j = 1; j < 95; j++) {
   if (vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF,j) ) {
    switch(eTrunkType) {
     case CLID: case SIP_TRUNK:
          if ((vMainWorkTable[i].CallData.fIsOnHold(j) )|| ( bPositionOn911Call[j] ) ){break;}
           bPositionOnAdminCall[j] = true;
           break;
     default:
           bPositionOn911Call[j] = true; bPositionOnAdminCall[j] = false;
           break;
    }

   } // end if ((vMainWorkTable[i].CallData.fIsInConference(POSITION_CONF,j)&&(!vMainWorkTable[i].CallData.fIsOnHold(j).)

  } // end  for (int j = 1; j < 95; j++)

 }// end for (unsigned int i = 0; i< sz; i++)

 for (int j = 1; j < 95; j++)  {
  if (boolSinglePSAP) {
   if (bPositionOnAdminCall[j]) { objStatus.iPositionsOnAdminCall++;}      // change later ?
   if (bPositionOn911Call[j])   { objStatus.iPositionsOnEmergencyCall++;} 
   continue;  
  } 
  index = WorkstationGroups.IndexOfGroupWithWorkstation(j);
  if (index < 0 ) {continue;}
  if  (bPositionOnAdminCall[j]) { WorkstationGroups.WorkstationGroups[index].PSAPStatus.iPositionsOnAdminCall++;}      // change later ?
  if  (bPositionOn911Call[j])   { WorkstationGroups.WorkstationGroups[index].PSAPStatus.iPositionsOnEmergencyCall++;}    
 }

/*************************************************************************************************************************************************/
 // PSAP INFO Data
 if (boolSinglePSAP) {
  WorkGroup.fLoadFromGlobalVARS();
  AddPSAPDataToJSONobject(&json, WorkGroup);
  strTemp = "/psap/" + objStatus.strPSAPname;
  strJSONoutput = json.GetOutputData();
  objMap->insert( pair<string,string> (strTemp,strJSONoutput));
  json.Reset();
  json.Remove();
  json.Flush();

  // PSAP Call Counts 
  strTemp = "/psap/" + objStatus.strPSAPname;
  strTemp += "/counts/calls";
  json.StartObject();
  json.PutProperty("active_911",   (char*) int2str(objStatus.i911Calls).c_str() , 2);
  json.PutProperty("active_admin", (char*) int2str(objStatus.iAdminCalls).c_str() , 2);
  json.PutProperty("active_msrp",  (char*) int2str(objStatus.iMSRPCalls).c_str() , 2);
  json.PutProperty("ringing_911",   (char*) int2str(objStatus.i911CallsRinging).c_str() , 2);
  json.PutProperty("ringing_admin", (char*) int2str(objStatus.iAdminCallsRinging).c_str() , 2);
  json.PutProperty("ringing_msrp",  (char*) int2str(objStatus.iMSRPCallsRinging).c_str() , 2);
  json.PutProperty("onhold_911",   (char*) int2str(objStatus.i911CallsOnHold).c_str() , 2);
  json.PutProperty("onhold_admin", (char*) int2str(objStatus.iAdminCallsOnHold).c_str() , 2);
  json.PutProperty("onhold_msrp",  (char*) int2str(objStatus.iMSRPCallsOnHold).c_str() , 2);
  json.PutProperty("abandoned_911",   (char*) int2str(objStatus.i911CallsAbandoned).c_str() , 2);
  json.PutProperty("abandoned_admin", (char*) int2str(objStatus.iAdminCallsAbandoned).c_str() , 2);
  json.PutProperty("abandoned_msrp",  (char*) int2str(objStatus.iMSRPCallsAbandoned).c_str() , 2);
  json.PutProperty("parked_911",   (char*) int2str(objStatus.i911CallsParked).c_str() , 2);
  json.PutProperty("parked_admin", (char*) int2str(objStatus.iAdminCallsParked).c_str() , 2);
  json.PutProperty("outbound_admin", (char*) int2str(objStatus.iOutboundAdminCalls).c_str() , 2);
  json.PutProperty("inbound_admin", (char*) int2str(objStatus.iInboundAdminCalls).c_str() , 2);
  json.EndObject(); 
  strJSONoutput = json.GetOutputData();
  objMap->insert( pair<string,string> (strTemp,strJSONoutput));
  json.Reset();
  json.Remove();
  json.Flush();
  iPosAvailable =  objStatus.iPositionsOnline - i911Calls - iMSRPCalls; // do we count admin calls ???
  if (iPosAvailable < 0 ) {
   iPosAvailable = 0;
  }
  strTemp = "/psap/" + objStatus.strPSAPname;
  strTemp += "/counts/positions";
  json.StartObject();
  json.PutProperty("total_installed",   (char*) int2str(intNUM_WRK_STATIONS).c_str() , 2);
  json.PutProperty("total_available", (char*) int2str(iPosAvailable).c_str() , 2);
  json.PutProperty("total_active",  (char*) int2str(objStatus.iPositionsOnline).c_str() , 2);
  json.EndObject(); 
  strJSONoutput = json.GetOutputData();
  objMap->insert( pair<string,string> (strTemp,strJSONoutput));
  json.Reset();
  json.Remove();
  json.Flush(); 
 }
 else {
  for (unsigned int i = 0; i< WorkstationGroups.WorkstationGroups.size(); i++)   {
   if (WorkstationGroups.WorkstationGroups[i].strGroupName == "ALL")             {continue;}
    AddPSAPDataToJSONobject(&json, WorkstationGroups.WorkstationGroups[i]);
    strTemp = "/psap/" + WorkstationGroups.WorkstationGroups[i].strPSAP_ID;
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush();
    strTemp = "/psap/" + WorkstationGroups.WorkstationGroups[i].strPSAP_ID;
    strTemp += "/counts/calls";
    json.StartObject();
    json.PutProperty("active_911",   (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.i911Calls).c_str() , 2);
    json.PutProperty("active_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iAdminCalls).c_str() , 2);
    json.PutProperty("active_msrp",  (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iMSRPCalls).c_str() , 2);
    json.PutProperty("ringing_911",   (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.i911CallsRinging).c_str() , 2);
    json.PutProperty("ringing_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iAdminCallsRinging).c_str() , 2);
    json.PutProperty("ringing_msrp",  (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iMSRPCallsRinging).c_str() , 2);
    json.PutProperty("onhold_911",   (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.i911CallsOnHold).c_str() , 2);
    json.PutProperty("onhold_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iAdminCallsOnHold).c_str() , 2);
    json.PutProperty("onhold_msrp",  (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iMSRPCallsOnHold).c_str() , 2);
    json.PutProperty("abandoned_911",   (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.i911CallsAbandoned).c_str() , 2);
    json.PutProperty("abandoned_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iAdminCallsAbandoned).c_str() , 2);
    json.PutProperty("abandoned_msrp",  (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iMSRPCallsAbandoned).c_str() , 2);
    json.PutProperty("parked_911",   (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.i911CallsParked).c_str() , 2);
    json.PutProperty("parked_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iAdminCallsParked).c_str() , 2);
    json.PutProperty("outbound_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iOutboundAdminCalls).c_str() , 2);
    json.PutProperty("inbound_admin", (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iInboundAdminCalls).c_str() , 2);
    json.EndObject(); 
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush();
    iPosAvailable =  WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline - WorkstationGroups.WorkstationGroups[i].PSAPStatus.i911Calls;
    iPosAvailable -= WorkstationGroups.WorkstationGroups[i].PSAPStatus.iMSRPCalls; // do we count admin calls ???
    if (iPosAvailable < 0 ) {
     iPosAvailable = 0;
    }
    strTemp = "/psap/" + objStatus.strPSAPname;
    strTemp += "/counts/positions";
    json.StartObject();
    json.PutProperty("total_installed",   (char*) int2str(WorkstationGroups.WorkstationGroups[i].WorkstationsInGroup.size()).c_str() , 2);
    json.PutProperty("total_available", (char*) int2str(iPosAvailable).c_str() , 2);
    json.PutProperty("total_active",  (char*) int2str(WorkstationGroups.WorkstationGroups[i].PSAPStatus.iPositionsOnline).c_str() , 2);
    json.EndObject(); 
    strJSONoutput = json.GetOutputData();
    objMap->insert( pair<string,string> (strTemp,strJSONoutput));
    json.Reset();
    json.Remove();
    json.Flush(); 
  
  } // end for
 } //end if else

/*************************************************************************************************************************************************/

 
 return;
}

bool AddTelephoneStatusDataToJSONobject(JSON *JSONobject, int iPosition) {

  extern Telephone_Devices                      TelephoneEquipment;
  int						index;
  size_t					sz;

  index = TelephoneEquipment.fIndexWithPositionNumber(iPosition);
  if (index < 0) {
   return false;
  }

  JSONobject->StartObject();
  if (TelephoneEquipment.Devices[index].boolRegistered) {
   JSONobject->PutProperty("registered", "true" , 4);
  }
  else { 
   JSONobject->PutProperty("registered", "false" , 4);
  } 
  if (TelephoneEquipment.Devices[index].boolFirstAlarm) {
   JSONobject->PutProperty("alarm", "true" , 4);
  }
  else { 
   JSONobject->PutProperty("alarm", "false" , 4);
  } 
  JSONobject->EndObject();
  
 return true;
}

bool AddTelephonePositionDataToJSONobject(JSON *JSONobject, int iPosition) {
/*
Todo ??? probably to a /status key
add    vPhoneLines.boolInUse
add    timespecTimeLastRegistered;
add    boolRegistered
add    boolFirstAlarm
*/
  extern Telephone_Devices                      TelephoneEquipment;
  int						index;
  size_t					sz;

  index = TelephoneEquipment.fIndexWithPositionNumber(iPosition);
  if (index < 0) {
   return false;
  }
  JSONobject->StartObject();
  JSONobject->PutProperty("name", (char*) TelephoneEquipment.Devices[index].strDeviceName.c_str() , 2);
  JSONobject->PutProperty("type", (char*) TelephoneDataType(TelephoneEquipment.Devices[index].eDeviceType).c_str() , 2);
  JSONobject->PutProperty("ip_addr", (char*) TelephoneEquipment.Devices[index].IPaddress.stringAddress.c_str() , 2);
  JSONobject->PutProperty("mac_addr", (char*) TelephoneEquipment.Devices[index].strMACaddress.c_str() , 2);
  if (TelephoneEquipment.Devices[index].strNextGenURI.length()) {
   JSONobject->PutProperty("uri", (char*) TelephoneEquipment.Devices[index].strNextGenURI.c_str() , 2);  
  }
  if (TelephoneEquipment.Devices[index].strCallerIDname.length()) {
   JSONobject->PutProperty("callerid_name", (char*) TelephoneEquipment.Devices[index].strCallerIDname.c_str() , 2);  
  }
  if (TelephoneEquipment.Devices[index].strCallerIDnumber.length()) {
   JSONobject->PutProperty("callerid_number", (char*) TelephoneEquipment.Devices[index].strCallerIDnumber.c_str() , 2);  
  }
  JSONobject->PutProperty("num_lines", (char*) int2str(TelephoneEquipment.Devices[index].iNumberOfLines).c_str() , 3);
  sz = TelephoneEquipment.Devices[index].vPhoneLines.size();
  if (sz > 0) { 
   JSONobject->PutName("phone_lines");
   JSONobject->StartArray();
   for (unsigned int i = 0; i < sz; i++) {
    JSONobject->StartObject();
    JSONobject->PutProperty("reg_name",   (char*) TelephoneEquipment.Devices[index].vPhoneLines[i].LineRegistrationName.c_str() , 2);
    JSONobject->PutProperty("line_index", (char*) int2str(TelephoneEquipment.Devices[index].vPhoneLines[i].LineIndex).c_str() , 3);
    if (TelephoneEquipment.Devices[index].vPhoneLines[i].boolSharedLine) {
     JSONobject->PutProperty("shared", "true" , 4);
    }
    else { 
     JSONobject->PutProperty("shared", "false" , 4);
    } 
    if (TelephoneEquipment.Devices[index].vPhoneLines[i].OutboundDial) {
     JSONobject->PutProperty("OutboundDial", "true" , 4);
    }
    else { 
     JSONobject->PutProperty("OutboundDial", "false" , 4);
    }      
    JSONobject->EndObject();
   }
   JSONobject->EndArray();
  }
  JSONobject->EndObject();

 return true;
}

void AddRCCPositionDataToJSONobject(JSON *JSONobject, int i ) {

  extern RCC_Device              		RCCdevice[NUM_WRK_STATIONS_MAX+1];

  if (i > NUM_WRK_STATIONS_MAX) {return;}

  JSONobject->StartObject();
  JSONobject->PutProperty("ip_addr", (char*) RCCdevice[i].RCCremoteIPaddress.stringAddress.c_str() , 2);
  JSONobject->PutProperty("local_port", (char*) int2str(RCCdevice[i].intRCClocalPortNumber).c_str() , 3);
  JSONobject->PutProperty("remote_port", (char*) int2str(RCCdevice[i].intRCCremotePortNumber).c_str() , 3);
  if (RCCdevice[i].boolRCCconnected) {
   JSONobject->PutProperty("connected", "true" , 4); 
  }
  else {
   JSONobject->PutProperty("connected", "false" , 4); 
  } 
  JSONobject->EndObject();  
 return;
}

void AddPositionDataToJSONobject(JSON *JSONobject, WorkStation objWorkstation) {

  JSONobject->StartObject();
  JSONobject->PutProperty("win_user", (char*) objWorkstation.strWindowsUser.c_str() , 2);
  JSONobject->PutProperty("ptk_version", (char*) objWorkstation.strGUIVersion.c_str() , 2);
  JSONobject->PutProperty("xml_version", (char*) objWorkstation.strGUIxmlVersion.c_str() , 2);
  JSONobject->PutProperty("win_os", (char*) objWorkstation.strWindowsOS.c_str() , 2);
  JSONobject->PutProperty("tcp_id", (char*) int2str(objWorkstation.intTCP_ConnectionID).c_str() , 2);
  JSONobject->PutProperty("ip_addr", (char*) objWorkstation.RemoteIPAddress.stringAddress.c_str() , 2);
  JSONobject->PutProperty("ip_port", (char*) int2str(objWorkstation.intRemotePortNumber).c_str() , 3);
  JSONobject->EndObject(); 
 return;
}

void AddTrunkRotateDataToJSONobject(JSON *JSONobject, Trunk_Rotate_Group objRotateGroup)  {

  size_t		sz;

  JSONobject->StartObject();
  JSONobject->PutProperty("rule",       (char*) objRotateGroup.fDisplayRule().c_str() , 2);
  JSONobject->PutProperty("rule_value", (char*) int2str(objRotateGroup.RotateRuleValue).c_str() , 3);
  sz = objRotateGroup.TrunkMember.size();
  if (sz > 0) {
   JSONobject->PutName("members");
   JSONobject->StartArray();
   for (unsigned int i = 0; i < sz; i++) {
    JSONobject->StartObject();
    JSONobject->PutProperty("trunk_number", (char*) int2str(objRotateGroup.TrunkMember[i]).c_str() , 3);
    JSONobject->EndObject();
   }
   JSONobject->EndArray();
  }

  JSONobject->EndObject();

 return;
}

void AddTrunkDataToJSONobject(JSON *JSONobject, Trunk_Type objTrunk) {

  char 				cbuff[28];
  struct tm 			timebuff;
  struct tm    			*timeinfo;
  long long int 		iTimeDifference;
  extern struct timespec       	timespecControllerStartTime;

 
  JSONobject->StartObject();
  JSONobject->PutProperty("type", (char*) objTrunk.fTrunkType().c_str() , 2);
  timeinfo =  gmtime_r(&objTrunk.time_tLastCall,&timebuff); 
  strftime(cbuff, sizeof(cbuff), "%Y-%m-%dT%H:%M:%SZ", timeinfo); 

  JSONobject->PutProperty("time_last_call", cbuff , 2);  
  if (objTrunk.RotateGroup > 0) {  
   JSONobject->PutProperty("rotate_group", (char*) int2str(objTrunk.RotateGroup).c_str() , 3);
   if (objTrunk.boolWarningSent) {
    JSONobject->PutProperty("alarm", "true" , 4);
   }
   else {
    JSONobject->PutProperty("alarm", "false" , 4); 
   } 
  }
  JSONobject->EndObject();
 return;
}

void  AddPSAPDataToJSONobject(JSON *JSONobject, Workstation_Group WrkGroup){

  size_t					sz;
 
  JSONobject->StartObject();

  if (WrkGroup.strGroupName.length()) { 
   JSONobject->PutProperty("name", (char*) WrkGroup.strGroupName.c_str() , 2);
  }
  if (WrkGroup.strPSAPURI.length()) {
   JSONobject->PutProperty("psapuri", (char*) WrkGroup.strPSAPURI.c_str() , 2);
  }

  sz = WrkGroup.TrunksinGroup.size();
  if (sz > 0) {
   JSONobject->PutName("trunks");
   JSONobject->StartArray();
   for (unsigned int i = 0; i < sz; i++) {
    JSONobject->StartObject();
    JSONobject->PutProperty("trunk_number", (char*) int2str(WrkGroup.TrunksinGroup[i]).c_str() , 3);
    JSONobject->EndObject();
   }
   JSONobject->EndArray();
  }

  sz = WrkGroup.WorkstationsInGroup.size();
  if (sz > 1) {
   JSONobject->PutName("positions");
   JSONobject->StartArray();
   for (unsigned int i = 1; i < sz; i++) {
    JSONobject->StartObject();
    JSONobject->PutProperty("position_number", (char*) int2str(WrkGroup.WorkstationsInGroup[i]).c_str() , 3);
    JSONobject->EndObject();
   }
   JSONobject->EndArray();
  }

  JSONobject->EndObject();
 return;
}

void AddCallDataToJSONobject(JSON *JSONobject, int i) {

 extern vector <ExperientDataClass>     vMainWorkTable; 
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;
 extern ALISteering                    	ALI_Steering[intMAX_ALI_DATABASES + 1 ];


 extern string 	TrunkType(int i);
 extern void 	AddConferenceDatatoJSONobject(JSON *JSONobject, Call_Data objCALLdata);

  JSONobject->StartObject();

  JSONobject->PutProperty("call_status", (char*) CallStateString(vMainWorkTable[i].intCallStateCode).c_str() , 2); 
  JSONobject->PutProperty("recording", (char*) vMainWorkTable[i].CallData.strCallRecording.c_str() , 2);
  JSONobject->PutProperty("cust_name", (char*) vMainWorkTable[i].CallData.strCustName.c_str() , 2);
  if (vMainWorkTable[i].CallData.intCallbackNumber) {
   JSONobject->PutProperty("callback", (char*) int2str(vMainWorkTable[i].CallData.intCallbackNumber).c_str() , 2);
  }
  JSONobject->PutProperty("ani",   (char*) vMainWorkTable[i].CallData.stringTenDigitPhoneNumber.c_str(), 2);
  JSONobject->PutProperty("pani",  (char*) vMainWorkTable[i].CallData.stringPANI.c_str(), 2);
  JSONobject->PutProperty("sip",  (char*) vMainWorkTable[i].CallData.strSIPphoneNumber.c_str(), 2);
  JSONobject->PutProperty("trunk", (char*) int2str(vMainWorkTable[i].CallData.intTrunk).c_str(), 2);
  JSONobject->PutProperty("Trunk_Type",   (char*) TrunkType( vMainWorkTable[i].CallData.intTrunk).c_str(), 2);     
  if (TRUNK_TYPE_MAP.fIsTandem(vMainWorkTable[i].CallData.intTrunk)){
   JSONobject->PutProperty("is_911",  "true" , 4);
  }
  else{
   JSONobject->PutProperty("is_911",  "false" , 4);
  }
  if (vMainWorkTable[i].CallData.boolIsOutbound) {
   JSONobject->PutProperty("Is_Outbound", "true" , 4); 
  } 
  else {
   JSONobject->PutProperty("Is_Outbound", "false" , 4); 
  } 
  JSONobject->PutProperty("original_calltaker", (char*) int2str(vMainWorkTable[i].CallData.intTrunk).c_str(), 2);

  AddConferenceDatatoJSONobject(JSONobject, vMainWorkTable[i].CallData);

  JSONobject->EndObject();
  JSONobject->EndObject();

}



int ProcessDSBCommands(ExperientDataClass objData)  {

 int				intRC;

/* 
   switch (objData.enumANIFunction) {
 
     case RFAI_BRIDGE: case RFAI_DROP_LEG:
             intRC = Send_RFAI_Info_Message(objData);
             return intRC;
     default:
     //         //cout << "Invalid XML Node" << endl << AMIData << endl;
              return 1;

   }// end switch

 */  

 return 0;
}

int  ProcessDSBPortData(DataPacketIn objDataPacket) {

 Port_Data                      objPortData;
 MessageClass                   objMessage;
 int				intPortNum = 1;
 size_t				found;

 objPortData.fLoadPortData(DSB, objDataPacket.intPortNum);

//Data from the Dashboard consists of Login and hearbeat Data
 DashboardPort.fSet_Port_Active();
 //Look for heartbeat reply
 found = objDataPacket.stringDataIn.find("{\"t\":\"pong\"");
 if (found != string::npos) {
  // right now there is nothing to do...
 }

 //Look for login:
 found = objDataPacket.stringDataIn.find("{\"t\":\"login_ok\"}");
 if (found != string::npos) {
  objMessage.fMessage_Create(LOG_CONSOLE_FILE, 663, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                             objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_663, objDataPacket.IPAddress.stringAddress);
  enQueue_Message(DSB,objMessage);
 }

 return 0;
}

void InitializeDSBport() {

 string                                 		strKey = IPWORKS_RUNTIME_KEY;
 extern ExperientTCPPort              			DashboardPort;
 extern Initialize_Global_Variables             	INIT_VARS;

  DashboardPort.SetRuntimeLicense((char*)strKey.c_str());
  DashboardPort.Remote_IP.stringAddress 	= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.REMOTE_IP_ADDRESS.stringAddress;
  DashboardPort.intRemotePortNumber 		= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.intREMOTE_PORT_NUMBER;
  DashboardPort.intPortDownReminderThreshold 	= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.intPORT_DOWN_REMINDER_SEC;
  DashboardPort.intPortDownThresholdSec 	= INIT_VARS.DSBinitVariable.DSB_PORT_VARS.intPORT_DOWN_THRESHOLD_SEC;

  INIT_VARS.DSBinitVariable.boolRestartDashboardPort = false;
  if (!boolUSE_DASHBOARD) {return;}
  ////cout << "init dashboard port" << endl;
  DashboardPort.fInitializeDSBport();

 return;
}


void Ping_Dashboard_Event(union sigval union_sigvalArg)
{
 Port_Data		objPortData;
 string 		strPING_EVENT="{\"t\":\"ping\",\"v\":\"hello\"}";

 if (boolSTOP_EXECUTION)             {return;}
 if (!boolUSE_DASHBOARD)             {return;}
 if (!DashboardPort.GetConnected())  {return;}
 Transmit_Data(DSB, 1, strPING_EVENT.c_str(), strPING_EVENT.length(), 0);
}



int ConnectToDashboard(int iRecursion=0)
{
 int							intRC;
 MessageClass						objMessage;
 string         					strMessage;
 Port_Data      					objPort;

 extern ExperientTCPPort              			DashboardPort;
 extern int						intTCP_RECONNECT_RECURSION_LEVEL;
 extern int						intTCP_RECONNECT_INTERVAL_SEC;

 if (boolSTOP_EXECUTION)  				{return 0;}
 if (!boolUSE_DASHBOARD)  				{return 0;}
 if (iRecursion == intTCP_RECONNECT_RECURSION_LEVEL )   {return 0;}

 objPort.fLoadPortData(DSB, DashboardPort.intPortNum);
// //cout << "ConnectToDashboard" << endl;
// if(!boolSTOP_EXECUTION) {DashboardPort.SetConnected(true);}
// if(!boolSTOP_EXECUTION) {DashboardPort.SetAcceptData(true);}

 intRC = DashboardPort.Connect((char*)  DashboardPort.Remote_IP.stringAddress.c_str(),  DashboardPort.intRemotePortNumber);

 if (intRC) {
  if ((boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC)||(iRecursion == 0)) {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 653, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPort,
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_653, DashboardPort.GetLastError(), int2str(iRecursion+1));
   enQueue_Message(DSB,objMessage);
  }
 
  DashboardPort.DoEvents();
  for (int i = 0; i < ((iRecursion+1)*intTCP_RECONNECT_INTERVAL_SEC); i++) {
   experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
   if (boolSTOP_EXECUTION)  {return 0;}
  }
  
  ConnectToDashboard(iRecursion+1);

 }

// DashboardPort.fSet_Port_Active();
 return 0;
}


int LogInToDashboard() {
 string 	strLogin;
 int 		intRC;
 extern string 	strCONTROLLER_UUID;
 extern string  stringSERVER_HOSTNAME;
 extern string 	strDASHBOARD_API_KEY;
 extern string  UUID_Generate_String();

// //cout << "log in to dashboard" << endl;
 if(!DashboardPort.boolReadyToSend){return 1;}
 if(boolSTOP_EXECUTION)  {return 1;}
 intRC = DashboardPort.SendLine((char *) strACTION_LOGIN_DSB.c_str());
 if (intRC) {return intRC;}
 experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);

 strLogin = "{\"t\":\"login\",\"apikey\":\"";
 strLogin += strDASHBOARD_API_KEY;
 strLogin += "\",\"skv\":\"/";
 strLogin += stringSERVER_HOSTNAME;
 strLogin += "/ng911\",\"major_version\":\"";
 strLogin += UUID_Generate_String();
 strLogin += "\"}";
 intRC = DashboardPort.SendLine((char *) strLogin.c_str());
 if (intRC) {return intRC;}
 
 return 0;
}


void *Dashboard_Login_Thread(void *voidArg)
{
 int intRC;
 Thread_Data                    ThreadDataObj; 
     
 extern vector <Thread_Data> 	ThreadData;
 extern map <string,string>     objPreviousMap;

 ThreadDataObj.strThreadName ="DSB Login";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in dashboard.cpp Dashboard_Login_Thread()", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 do {

  
  // just keep on trying to log into the Dashboard indefinately ... 
  if (!boolSTOP_EXECUTION)   {
    experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
    if (!boolUSE_DASHBOARD ){
     for (int i = 1; i <= 10; i++) { if (!boolSTOP_EXECUTION) {sleep(1);}}
     continue;
    }
    if(!DashboardPort.GetConnected())  {
  //    //cout << "not connected" << endl;
      DashboardPort.boolFreeswitchLoggedin = false;
      ConnectToDashboard();
      for (int i = 1; i <= 3; i++) {if (!boolSTOP_EXECUTION) {experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);}}
    }
    if((DashboardPort.GetConnected())&&(!boolSTOP_EXECUTION)&&(!DashboardPort.boolFreeswitchLoggedin)) 
     {
      intRC = LogInToDashboard(); 
      if (intRC) {DashboardPort.boolFreeswitchLoggedin = false;}
      else       {
        DashboardPort.boolFreeswitchLoggedin = true;
        sem_wait(&sem_tMutexMainWorkTable);
        objPreviousMap.clear(); 
        DashboardUpdate();
        sem_post(&sem_tMutexMainWorkTable);
        //sem unlock
      }
     }
    for (int i = 1; i <= 10; i++) { if (!boolSTOP_EXECUTION) {sleep(1);}}
  }
 } while (!boolSTOP_EXECUTION);

 DashboardPort.Disconnect();
 boolDSB_LOGIN_THREAD_SHUTDOWN = true;
 return NULL;
}

void *Dashboard_TCP_Listen_Thread(void *voidArg)
{
 Thread_Data                    ThreadDataObj; 
 int 				intRC;  
 param_t* p          = (param_t*) voidArg;
 int      intPortNum = p->intPassedIn;
 MessageClass                   objMessage;
 Port_Data                      objPortData; 
 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="DSB Listen"+ int2strLZ(intPortNum);;
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in Dashboard::Dashboard_TCP_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);
 objPortData.fLoadPortData(DSB, intPortNum);
 objMessage.fMessage_Create(0, 652, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                            DSB_MESSAGE_652, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 
 boolDashboard_LISTEN_THREAD_SHUTDOWN = false;

 while ((!boolSTOP_EXECUTION)&&(!INIT_VARS.DSBinitVariable.boolRestartDashboardPort)) {
  if (!boolUSE_DASHBOARD ){
   for (int i = 1; i <= 10; i++) { if (!boolSTOP_EXECUTION) {sleep(1);}}
   continue;
  }
  DashboardPort.DoEvents(); 
  experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
 }

 DashboardPort.Disconnect();
 sleep(1); 
 DashboardPort.DoEvents();
 objMessage.fMessage_Create(0, 652, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                            DSB_MESSAGE_652b, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 DashboardPort.Disconnect();
 boolDashboard_LISTEN_THREAD_SHUTDOWN = true;
 return NULL;								
}

 
void *Dashboard_Messaging_Monitor_Event(void *voidArg)
{
 int				intRC;
 MessageClass			objMessage;
 Thread_Data            	ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="DSB Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in Dashboard.cpp::Dashboard_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 //if (boolSTOP_EXECUTION) {return;}
// else                    {timer_settime(timer_tDashboardMessageMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}

do {

  experient_nanosleep(intDSB_MESSAGE_MONITOR_INTERVAL_NSEC);
  if (boolUPDATE_VARS) {continue;}

 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in Dashboard_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexDSBMessageQ);
 if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexDSBMessageQ, "sem_wait@sem_tMutexDSBMessageQ in Dashboard_Messaging_Monitor_Event", 1);}

 while (!queue_objDSBMessageQ.empty())
  {
    objMessage = queue_objDSBMessageQ.front();
    queue_objDSBMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 650:						
       // #define DSB_MESSAGE_650                         "-[DEBUG] Thread Created, PID %%% ThreadSelf %%%" 
       break; 
      case 651:						
       // #define DSB_MESSAGE_651                         "-[WARNING] Thread Failed to Initialize, System Rebooting"
       break;
      case 652:						
       // #define DSB_MESSAGE_652                         "-[INFO] Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 653:
       // 						
       break;
      case 654:
       // "-[WARNING] Thread Signalling Semaphore failed to Lock"						
       break; 
      case 655:
       // "-[DEBUG] Message Monitor Timer Event Initialized"					
       break; 
      case 656:
       // #define DSB_MESSAGE_656                         "-[INFO] DSB TCP Connection Status Code=%%% Desc=%%%" 					
       break; 
      case 657:
       // #define DSB_MESSAGE_657                         "-[WARNING] DSB TCP Status: Disconnect, Code=%%% Desc=%%%"					
       break; 
      case 658:
       // #define DSB_MESSAGE_658                         ">[INFO] DSB TCP Port Ready to Send"
       break;
      case 659:
       // 
       break;
      case 660:
       // 
       break;
      case 661:
       // 
       break;
      case 662:
       ////cout << "got message 662" << endl;
       break;
      case 663:
       // 
       break;
      case 664:
       // 
       break;
      case 665:
       // 
       break;
      case 666:
       //
       break;
      case 667:
       // 
       break;
      case 668:
       // 
       break;
      case 669:
       // 
       break;
      case 670:
       //
       break;
      case 671:
       // 
       break;
      case 672:
       // 
       break;
      case 673:
       //  #define DSB_MESSAGE_673                         "-DSB Messaging Event Thread Complete"
       break; 
      case 674:
       //  "-DSB Messaging Event Thread Failed to start!"
       break; 
      case 675:
       //  #define DSB_MESSAGE_675                         "-Thread Complete"
       break;   	
      default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in DSB Monitor\n";
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock AMI Message Q then Main Message Q
 sem_post(&sem_tMutexDSBMessageQ);
 sem_post(&sem_tMutexMainMessageQ);

 if (boolUSE_DASHBOARD) {
  DashboardPort.fCheckTimeSinceLastRX(); 
 }

}while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 673, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_DSB_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, DSB_MESSAGE_673);
 objMessage.fConsole();


return NULL;

}// end Dashboard_Messaging_Monitor_Event()





