/*****************************************************************************
* FILE: call_data_functions.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the Call_Data Class 
*
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"

extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data			       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;
extern vector <string>                         	NPD;

// Constructor
Call_Data::Call_Data()   {

  eJSONEventCode                = UNCLASSIFIED;
  eJSONEventWRKcode             = BAD_KEY;
  eJSONEventTransferMethod      = mBLANK;
  strJSONEventMergedCallID      = "";
  intUniqueCallID               = 0;
  stringUniqueCallID            = "";
  intTrunk                      = 0;
  stringTrunk                   = "";
  intPosn                       = 0;
  stringPosn                    = "";
  CTIsharedLine                 = 0;
  strLastHangupPosn             = "";
  strCustName                   = "";
  intCallbackNumber             = 0;
  boolCallBackVerified          = false;
  boolLandLineCall              = false;
  boolWirelessVoipCall          = false;
  boolBlindTransferSet          = false;
  boolIsOutbound                = false;
  boolBlindXferUpdate           = false;
  boolConfXferUpdate            = false;
  intANIformat                  = 0;
  stringInfoCode                = "";
  stringOneDigitAreaCode        = "";
  stringThreeDigitAreaCode      = "";
  stringTelnoPrefix             = "";
  stringTelnoSuffix             = "";
  stringSevenDigitPhoneNumber   = "";
  stringTenDigitPhoneNumber     = "";
  strSIPphoneNumber             = "";
  strCallBackDisplay            = "";
  strCallTypeDisplay            = "";
  stringPANI                    = "";
  strDialedNumber.clear();
  strConferenceNumber           = "";
  intConferenceNumber           = 0;
  strFreeswitchConfName         = "";
  strFreeswitchConfID           = "";
  strCallRecording.clear();
  ConfData.fClear();
  TransferData.fClear();
  ReferData.fClear();
  vTransferData.clear();
  vStreamID.clear();
 }

// assignment operator
Call_Data& Call_Data::operator=(const Call_Data& b)
 {
  if (&b == this ) {return *this;}
  eJSONEventCode                 = b.eJSONEventCode;
  eJSONEventWRKcode              = b.eJSONEventWRKcode;
  eJSONEventTransferMethod       = b.eJSONEventTransferMethod;
  strJSONEventMergedCallID       = b.strJSONEventMergedCallID;
  intUniqueCallID 		 = b.intUniqueCallID;
  stringUniqueCallID		 = b.stringUniqueCallID;
  intTrunk 			 = b.intTrunk;
  stringTrunk		         = b.stringTrunk;
  intPosn			 = b.intPosn;
  stringPosn			 = b.stringPosn;
  CTIsharedLine		 	 = b.CTIsharedLine;
  strLastHangupPosn              = b.strLastHangupPosn;
  strCustName                    = b.strCustName;
  intCallbackNumber		 = b.intCallbackNumber;
  boolCallBackVerified           = b.boolCallBackVerified;
  boolLandLineCall               = b.boolLandLineCall;
  boolWirelessVoipCall           = b.boolWirelessVoipCall;
  boolBlindTransferSet           = b.boolBlindTransferSet;
  boolIsOutbound                 = b.boolIsOutbound;
  boolBlindXferUpdate            = b.boolBlindXferUpdate;
  boolConfXferUpdate             = b.boolConfXferUpdate;
  intANIformat                   = b.intANIformat;
  stringInfoCode                 = b.stringInfoCode;
  stringOneDigitAreaCode         = b.stringOneDigitAreaCode;
  stringThreeDigitAreaCode       = b.stringThreeDigitAreaCode;
  stringTelnoPrefix              = b.stringTelnoPrefix;
  stringTelnoSuffix              = b.stringTelnoSuffix;
  stringSevenDigitPhoneNumber    = b.stringSevenDigitPhoneNumber;
  stringTenDigitPhoneNumber      = b.stringTenDigitPhoneNumber;
  strSIPphoneNumber              = b.strSIPphoneNumber;
  strCallBackDisplay             = b.strCallBackDisplay;
  strCallTypeDisplay             = b.strCallTypeDisplay;
  stringPANI                     = b.stringPANI;
  strDialedNumber                = b.strDialedNumber;
  strConferenceNumber            = b.strConferenceNumber;
  intConferenceNumber            = b.intConferenceNumber;
  strFreeswitchConfName          = b.strFreeswitchConfName;
  strFreeswitchConfID            = b.strFreeswitchConfID;
  strCallRecording               = b.strCallRecording;
  ConfData                       = b.ConfData;
  TransferData                   = b.TransferData;
  ReferData                      = b.ReferData;
  vTransferData                  = b.vTransferData; 
  vStreamID                      = b.vStreamID;  
  return *this;
 }

void Call_Data::fClear()
{
 eJSONEventCode                 = UNCLASSIFIED;
 eJSONEventWRKcode              = BAD_KEY;
 eJSONEventTransferMethod       = mBLANK;
 strJSONEventMergedCallID       = "";
 intUniqueCallID 		= 0;
 stringUniqueCallID		= "";
 intTrunk 			= 0;
 stringTrunk			= "";
 intPosn			= 0;
 stringPosn			= "";
 CTIsharedLine		        = 0;
 strLastHangupPosn              = "";
 strCustName                    = "";
 intCallbackNumber		= 0;
 boolCallBackVerified           = false;
 boolLandLineCall               = false;
 boolWirelessVoipCall           = false;
 boolBlindTransferSet           = false;
 boolIsOutbound                 = false;
 boolBlindXferUpdate            = false;
 boolConfXferUpdate             = false;
 intANIformat                   = 0;
 stringInfoCode                 = "";
 stringOneDigitAreaCode         = "";
 stringThreeDigitAreaCode       = "";
 stringTelnoPrefix              = "";
 stringTelnoSuffix              = "";
 stringSevenDigitPhoneNumber    = "";
 stringTenDigitPhoneNumber  	= ""; 
 strSIPphoneNumber              = "";
 strCallBackDisplay             = ""; 
 strCallTypeDisplay             = "";
 stringPANI                 	= "";
 strDialedNumber.clear();
 strConferenceNumber            = "";
 intConferenceNumber            = 0;
 strFreeswitchConfName          = "";
 strFreeswitchConfID            = "";
 strCallRecording.clear();
 ConfData.fClear();
 TransferData.fClear();
 ReferData.fClear();
 vTransferData.clear();
 vStreamID.clear();
}


void Call_Data::fDisplay()  {
 size_t sz;

 //cout << "CALL Data" << endl;
 //cout << "UNIQUE ID     (i): " << intUniqueCallID << endl;
 //cout << "UNIQUE ID     (s): " << stringUniqueCallID << endl;
 //cout << "TRUNK         (i): " << intTrunk << endl;
 //cout << "TRUNK         (s): " << stringTrunk << endl;
 //cout << "CTI SHRD LN#  (i): " << CTIsharedLine << endl;
 //cout << "POSITION      (i): " << intPosn << endl;
 //cout << "POSITION      (s): " << stringPosn << endl;
 //cout << "LAST HANGUP P (s): " << strLastHangupPosn << endl;
 //cout << "CALLBACK      (i): " << intCallbackNumber << endl;
 //cout << "CUST NAME     (s): " << strCustName << endl;
 //cout << "CALLBK VRF    (b): " << boolCallBackVerified << endl;
 //cout << "LANDLINE      (b): " << boolLandLineCall << endl;
 //cout << "WRLS VOIP     (b): " << boolWirelessVoipCall << endl;
 //cout << "BXFER SET     (b): " << boolBlindTransferSet << endl;
 //cout << "IS OUTBOUND   (b): " << boolIsOutbound << endl;
 //cout << "BXFER UPDTE   (b): " << boolBlindXferUpdate << endl;
 //cout << "CXFER UPDTE   (b): " << boolConfXferUpdate << endl;
 //cout << "ANI FORMAT    (i): " << intANIformat << endl;
 //cout << "INFO CODE     (s): " << stringInfoCode << endl;
 //cout << "NPD           (s): " << stringOneDigitAreaCode << endl;
 //cout << "AREA CODE     (s): " << stringThreeDigitAreaCode << endl;
 //cout << "PREFIX        (s): " << stringTelnoPrefix << endl;
 //cout << "SUFFIX        (s): " << stringTelnoSuffix << endl;
 //cout << "7 Digit N     (s): " << stringSevenDigitPhoneNumber << endl;
 //cout << "10 Digit N    (s): " << stringTenDigitPhoneNumber << endl;
 //cout << "SIP #         (s): " << strSIPphoneNumber << endl;
 //cout << "CALLB Disp    (s): " << strCallBackDisplay << endl;
 //cout << "PANI          (s): " << stringPANI << endl;
 //cout << "Dialed Number (s): " << strDialedNumber << endl;
 //cout << "CONF #        (s): " << strConferenceNumber << endl;
 //cout << "CONF #        (i): " << intConferenceNumber << endl;
 //cout << "FSW Conf-Name (s): " << strFreeswitchConfName << endl;
 //cout << "FSW Conf-ID   (s): " << strFreeswitchConfID << endl;
 //cout << "Call Recording(s): " << strCallRecording << endl;
 //cout << "ACTIVE  CONF  (s): " << ConfData.strActiveConferencePositions << endl;
 //cout << "HISTORY CONF  (s): " << ConfData.strHistoryConferencePositions << endl;
 //cout << "ONHOLD CONF   (s): " << ConfData.strOnHoldPositions << endl;
 //cout << "ONHOLD P+T    (s): " << ConfData.strOnHoldPosAndTime << endl;
 //cout << "PARKED CONF   (s): " << ConfData.strParkParticipant << endl;
 //cout << "PARK TIME     (s): " << ConfData.strParkTime << endl;
 //cout << "PARK LOT      (s): " << ConfData.strParkLot << endl;
 //cout << "TRANSFER OBJECT ->" << endl;
 TransferData.fDisplay(); 
 sz = vTransferData.size();
 //cout << "TRANSFER VECTOR ->" << endl;
 for (unsigned int i = 0; i < sz; i++) {vTransferData[i].fDisplay(i+1);}
 sz = ConfData.vectConferenceChannelData.size();
 //cout << endl << "CONFERENCE CHANNEL DATA:" << endl;
 for (unsigned int i = 0; i < sz; i++){ConfData.vectConferenceChannelData[i].fDisplay(i,(!i));}
}


int Call_Data::fSetSharedLineFromLineNumberPlusPosition(int i)
{
 extern Telephone_Devices                TelephoneEquipment;
// make sure position is set before calling !
// //cout << "SSLFP Position -> " << i << endl;

 if (TelephoneEquipment.fLineNumberAtPositionIsShared( i, this->intPosn)) { this->CTIsharedLine = i; return i;}
 else                                                                     { this->CTIsharedLine = 0; return 0;}

}

int Call_Data::fSetSharedLineFromLineNumberPlusPosition(Channel_Data objData)
{
 extern Telephone_Devices                TelephoneEquipment;

 int i = objData.iLineNumber;

//  //cout << "SSLFP Line -> " << i << endl;
//  //cout << "SSLFP Pos -> " << objData.iPositionNumber << endl;

 if (TelephoneEquipment.fLineNumberAtPositionIsShared( i, objData.iPositionNumber)) { this->CTIsharedLine = i; return i;}
 else                                                                               { this->CTIsharedLine = 0; return 0;}

}

void Call_Data::fLoadCallRecording(Channel_Data objChannelData)
{
// Stream_ID objStreamID;

 this->strCallRecording = objChannelData.strCallRecording;

// objStreamID.fLoadStreamID(this->strCallRecording);


 return;
}
void Call_Data::fLoadCustName(string strInput)
{
 if (strInput.empty()) {strCustName = "Unknown";}
 else                  {strCustName = strInput;}
}



string Call_Data::fCreateLocationRequest(bool firstBid)
{
 string strLocationRequest;

 strLocationRequest  = "<locationRequest xmlns=\"urn:ietf:params:xml:ns:geopriv:held\"";
 if (firstBid) {strLocationRequest += " responseTime=\"emergencyRouting\">\n";}
 else          {strLocationRequest += " responseTime=\"emergencyDispatch\">\n";} 
 strLocationRequest += "<locationType exact=\"true\">\ngeodetic\ncivic\nlocationURI\n</locationType>\n";
 strLocationRequest += "<device xmlns=\"urn:ietf:params:xml:ns:geopriv:held:id\">\n";
 strLocationRequest += "<uri>tel:+1";
 if (this->stringTenDigitPhoneNumber.length()) {
  strLocationRequest += this->stringTenDigitPhoneNumber;
 }
 else {
  strLocationRequest += this->stringPANI;
 }
 strLocationRequest += ";</uri>\n </device>\n </locationRequest>\n"; 


 return strLocationRequest;
}

bool Call_Data::fIsNG911SIP() {
 extern Trunk_Type_Mapping     TRUNK_TYPE_MAP;

 if ((TRUNK_TYPE_MAP.Trunk[this->intTrunk].TrunkType == NG911_SIP)||
     (TRUNK_TYPE_MAP.Trunk[this->intTrunk].TrunkType == REFER)) {
  return true;
 }
 
 return false;
}

void Call_Data::fSetTDDModeonConferenceVector(string strChannelUUID)
{
 size_t sz;

 sz =  this->ConfData.vectConferenceChannelData.size();
 ////cout << "size -> " << sz << " channel -> " << strChannelUUID << endl;
 for (unsigned int i = 0; i < sz; i++) { 
 // //cout << this->ConfData.vectConferenceChannelData[i].strChannelID << endl;
  if (strChannelUUID == this->ConfData.vectConferenceChannelData[i].strChannelID) {
   this->ConfData.vectConferenceChannelData[i].boolTDDMode = true; ////cout << "TDD MODE TRUE" << endl;
  }
 }
 return;
}

void Call_Data::fSetTrunkTypeString(bool bOutbound, string strNumberDialed)
{
 extern Trunk_Type_Mapping     TRUNK_TYPE_MAP;
 int                           iSwitch;
 bool                          boolIsSIP = false;

 if (CheckforManualBidTrunk(intTrunk)){strCallTypeDisplay = "Manual Bid"; return;}

 if (!bOutbound) {iSwitch = TRUNK_TYPE_MAP.Trunk[intTrunk].TrunkType;}
 else            {iSwitch = SIP_TRUNK;}

// //cout << "trunk = " << intTrunk << endl;
// DisplayTrunkType(intTrunk);


 switch (iSwitch)
  {
   case CAMA:      		boolLandLineCall = false; boolWirelessVoipCall = false;  strCallTypeDisplay = "9-1-1";                                        break;
   case LANDLINE:  		boolLandLineCall = true;  boolWirelessVoipCall = false;  strCallTypeDisplay = "9-1-1";                                        break;
   case WIRELESS:  		boolLandLineCall = false; boolWirelessVoipCall = true;   strCallTypeDisplay = "9-1-1";                                        break;
   case INDIGITAL_WIRELESS:	boolLandLineCall = false; boolWirelessVoipCall = true;   strCallTypeDisplay = "9-1-1";                                        break;
   case INTRADO:                boolLandLineCall = false; boolWirelessVoipCall = true;   strCallTypeDisplay = "9-1-1";                                        break;
   case CLID:      		boolLandLineCall = false; boolWirelessVoipCall = false;  strCallTypeDisplay = "POTS/Caller-ID";boolCallBackVerified = true;   break;
   case SIP_TRUNK: 		boolLandLineCall = false; boolWirelessVoipCall = true;   strCallTypeDisplay = "VoIP";          boolIsSIP = true;              break;
   case MSRP_TRUNK:             boolLandLineCall = false; boolWirelessVoipCall = false;  strCallTypeDisplay = "MSRP Text";     boolIsSIP = true;              break;
   default:                                                                 strCallTypeDisplay = "Unknown";    
  }

 if  ((boolANI_20_DIGIT_VALID_CALLBACK)&&(stringPANI == stringTenDigitPhoneNumber)&&(!stringPANI.empty())&&(!stringTenDigitPhoneNumber.empty())) {boolLandLineCall     = true;}
 if  ((!stringPANI.empty())&&(stringPANI != stringTenDigitPhoneNumber)&&(!stringTenDigitPhoneNumber.empty()))                                    {boolWirelessVoipCall = true;} 
 
 if      ((boolWirelessVoipCall)&&(!boolIsSIP))                       {strCallTypeDisplay.append(" Wireless/VoIP");}
 else if (boolLandLineCall)                                           {strCallTypeDisplay.append(" Landline");}
 
 else if (bOutbound)                                                  {strCallTypeDisplay.append(" (Outbound)");}
 else                                                                 {strCallTypeDisplay.append(" Caller");}
}



bool Call_Data::fSet_Phone_Numbers_From_Bad_ANI(string strAsteriskANI)
{
 bool    boolreturn = true;
 if      (fSet_PhoneNumberFromANI(EIGHT_DIGIT, PSUEDO_ANI ,strAsteriskANI))  {fSetANIformat(8, intTrunk);}
 else if (fSet_PhoneNumberFromANI(TEN_DIGIT, PSUEDO_ANI ,strAsteriskANI))    {fSetANIformat(10, intTrunk);}
 else if ((fSet_PhoneNumberFromANI(TWENTY_DIGIT, PSUEDO_ANI ,strAsteriskANI))&&(fSet_PhoneNumberFromANI(TWENTY_DIGIT, CALLBACK ,strAsteriskANI)))
                                                                             {fSetANIformat(20, intTrunk);}
 else if (fSet_PhoneNumberFromANI(TWENTY_DIGIT, PSUEDO_ANI ,strAsteriskANI)) {stringTenDigitPhoneNumber = stringPANI;}
 else if (fSet_PhoneNumberFromANI(TWENTY_DIGIT, CALLBACK ,strAsteriskANI))   {stringPANI = stringTenDigitPhoneNumber;}
 else                                                                        {fSetBadPANInumber(); boolreturn = false;}
 return boolreturn;    
}


bool Call_Data::fSet_PhoneNumberFromANI(ani_format enumANIformat, ani_type enumANItype , string strInputANI)
{
 string NumberString;
 string strTemp;
 size_t firstDelimeter  = 0;
 size_t secondDelimeter = 0;
 size_t found           = 0;

 if (strInputANI.empty()) {return false;}

 switch (enumANIformat)
  {
   case TEN_DIGIT:
        while (firstDelimeter < strInputANI.length())
         {
          firstDelimeter = strInputANI.find('*', firstDelimeter);
          if (firstDelimeter == string::npos)                       {return false;}
          secondDelimeter = strInputANI.find('B', firstDelimeter);
          if (secondDelimeter == string::npos)                      {return false;}
          if(strInputANI.length() < firstDelimeter+2)               { SendCodingError("globalfunctions.cpp - coding error in Call_Data::fSet_PhoneNumberFromANI-a"); return false;}
          NumberString.assign(strInputANI, firstDelimeter+1, (secondDelimeter - firstDelimeter -1));
          if (NumberString.length() != 12)                          {firstDelimeter++; continue;}
          strTemp = NumberString.substr(2,10);
          found = strTemp.find_first_not_of("0123456789");
          if (found != string::npos)                                {firstDelimeter++; continue;}
          fLoad_Info_Code(NumberString.substr(0, 2));
          switch (enumANItype)
           {
            case CALLBACK:
                 fLoadCallbackNumber(NumberString.substr(2,10));
                 break;
            case PSUEDO_ANI:
                 stringPANI.assign(NumberString, 2, 10);
                 break;
           }
          return true; 
         }// end while (firstDelimeter != string::npos)

        break;

   case TWENTY_DIGIT:
        // *40 NPANXXXXXX#*NPANXXXXXX
        switch (enumANItype)
         {
          case CALLBACK:
               while (firstDelimeter < strInputANI.length())
                {
                 firstDelimeter = strInputANI.find('*', firstDelimeter);
                 if (firstDelimeter == string::npos)                       {return false;}
                 secondDelimeter = strInputANI.find('#', firstDelimeter);
                 if (secondDelimeter == string::npos)                      {return false;}
                 if(strInputANI.length() < firstDelimeter+2)               { SendCodingError("globalfunctions.cpp - coding error in Call_Data::fSet_PhoneNumberFromANI-b"); return false;}
                 NumberString.assign(strInputANI, firstDelimeter+1, (secondDelimeter - firstDelimeter -1));
                 if (NumberString.length() != 12)                          {firstDelimeter++; continue;}
                 strTemp = NumberString.substr(2,10);
                 found = strTemp.find_first_not_of("0123456789");
                 if (found != string::npos)                                {firstDelimeter++; continue;}
                 fLoad_Info_Code(NumberString.substr(0, 2));
                 fLoadCallbackNumber(NumberString.substr(2,10));
                 return true;
    
                }// end while (firstDelimeter != string::npos)

               break;
          case PSUEDO_ANI:
               if (strInputANI.length() < 12)                                       {return false;}     
               firstDelimeter = strInputANI.length() -1;
               while (firstDelimeter != 0)
                {
                 firstDelimeter = strInputANI.rfind("*", firstDelimeter, 1);
                 if (firstDelimeter == string::npos)                                {return false;}
                 secondDelimeter = strInputANI.find('#', firstDelimeter);
                 if      ((secondDelimeter == string::npos)&&(firstDelimeter == 0)) {return false;}
                 else if  (secondDelimeter == string::npos)                         {firstDelimeter -= 1; continue;}
                 if(strInputANI.length() < firstDelimeter+2)                        { SendCodingError("globalfunctions.cpp - coding error in Call_Data::fSet_PhoneNumberFromANI-c"); return false;}
                 NumberString.assign(strInputANI, firstDelimeter+1, (secondDelimeter - firstDelimeter -1));
                 found = NumberString.find_first_not_of("0123456789");
                 if ((NumberString.length() != 10)||(found != string::npos) )
                  {
                   if (firstDelimeter == 0 )                                         {return false;}
                   firstDelimeter -= 1;
                   continue;
                  }
                 stringPANI = NumberString;
                 return true; 
                }// end while (firstDelimeter != string::npos)

               break;
         }// end switch (enumANItype)
        break;

   case EIGHT_DIGIT:
        // *NNPAXXXX#
        while (firstDelimeter < strInputANI.length())
         {
          firstDelimeter = strInputANI.find_first_of('*', firstDelimeter);
          if (firstDelimeter == string::npos)                       {return false;}
          secondDelimeter = strInputANI.find_first_of('#', firstDelimeter);
          if (secondDelimeter == string::npos)                      {return false;}
          if(strInputANI.length() < firstDelimeter+2)               { SendCodingError("globalfunctions.cpp - coding error in Call_Data::fSet_PhoneNumberFromANI-d"); return false;}
          NumberString.assign(strInputANI, firstDelimeter+1, (secondDelimeter - firstDelimeter-1));
          if (NumberString.length() != 8)                          {firstDelimeter++; continue;}
          found = NumberString.find_first_not_of("0123456789");
          if (found != string::npos)                               {firstDelimeter++; continue;}
          stringInfoCode = "0";
          stringInfoCode.append(NumberString,0,1);
          if (!fLoad_Info_Code(stringInfoCode, true))              {fSetCallInfoToSticks(); break;}
          

          switch (enumANItype)
           {
            case CALLBACK:
                 if(NumberString.length() < 2)                    { SendCodingError("globalfunctions.cpp - coding error in Call_Data::fSet_PhoneNumberFromANI-e"); return false;}
                 stringSevenDigitPhoneNumber.assign(NumberString, 1, 7);
                 stringTenDigitPhoneNumber = stringThreeDigitAreaCode + stringSevenDigitPhoneNumber;
                 fLoadCallbackNumber(stringTenDigitPhoneNumber);                
                 break;
            case PSUEDO_ANI:
                 stringPANI = stringThreeDigitAreaCode + NumberString.substr(1,7);
                 break;
           } 
          return true; 
         }// end while (firstDelimeter != string::npos)
        break;

  case TEN_DIGIT_NON_STANDARD:
        // *NPAXXXXXXX#
        while (firstDelimeter < strInputANI.length())
         {
          firstDelimeter = strInputANI.find('*', firstDelimeter);
          if (firstDelimeter == string::npos)                       {return false;}
          secondDelimeter = strInputANI.find('#', firstDelimeter);
          if (secondDelimeter == string::npos)                      {return false;}
          if(strInputANI.length() < firstDelimeter+2)                 { SendCodingError("globalfunctions.cpp - coding error in Call_Data::fSet_PhoneNumberFromANI-a"); return false;}
          NumberString.assign(strInputANI, firstDelimeter+1, (secondDelimeter - firstDelimeter -1));
          if (NumberString.length() != 10)                          {firstDelimeter++; continue;}
          switch (enumANItype)
           {
            case CALLBACK:
                 fLoadCallbackNumber(NumberString);
                 break;
            case PSUEDO_ANI:
                 stringPANI.assign(NumberString);
                 break;
           }
          return true; 
         }// end while (firstDelimeter != string::npos)
        break;

  case TEN_DIGIT_PRECHECKED:
       found = strInputANI.find_first_not_of("0123456789");
       if (found != string::npos)      {return false;}
       if (strInputANI.length() != 10) {return false;}

       switch (enumANItype)
        {
         case CALLBACK:
              fLoadCallbackNumber(strInputANI);
              break;
         case PSUEDO_ANI: 
              stringPANI = strInputANI;
              break;
        }      
       return true;

  }// end switch (enumANIformat)
  
return false;
}

string Call_Data::fFindChannelIDfromPhoneNumber(string strXferToNum)
{
 vector<Channel_Data>::size_type      sz = this->ConfData.vectConferenceChannelData.size();

 if (!strXferToNum.length())     {return "";}

 // check list first for match ...
 if(!this->ConfData.vectConferenceChannelData.empty()) {
   for (unsigned int i = 0; i< sz; i++) {
     if (strXferToNum == this->ConfData.vectConferenceChannelData[i].strCallerID) {
      return this->ConfData.vectConferenceChannelData[i].strChannelID;
     }
   }// end for (unsigned int i = 0; i< sz; i++)
 }// end if(!vectPostionsOnCall.empty())


return "";
}




int Call_Data::fVerify_NPDNumber()
{
 int                            intNPDNumber;
 MessageClass                   objMessage;
 Port_Data                      objPortData;

 objPortData.fLoadPortData(ANI, 1);

 if (!Validate_Integer(stringOneDigitAreaCode))
  {
   return -1;      
  }
 intNPDNumber = char2int(stringOneDigitAreaCode.c_str());

 // 8 is a test number set to 0 to process
 if(intNPDNumber == 8)
  {
   intNPDNumber = 0;
   objMessage.fMessage_Create(LOG_WARNING,353, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, 
                              ANI_MESSAGE_353, int2strLZ(1));
   enQueue_Message(ANI,objMessage);
  }                                      
 if(intNPDNumber > 7) 
  {
   return -1;      
  }
 if (intNPDNumber > 3){intNPDNumber -= 4;}
 return intNPDNumber;
}



bool Call_Data::fLoad_Info_Code(string stringArg, bool boolNPD)
{
 
 int       	intNPDNumber;
 Port_Data 	objPortData;
 MessageClass 	objMessage;


 if (stringArg.length() < 2)    {SendCodingError("call_data_functions.cpp - string passed to Call_Data::fLoad_Info_Code() too short"); return false;} 

 stringInfoCode = stringArg; 
 // TBC to VALIDATE CODE for now it is ignored ........
 
 if (boolNPD)
  {
   stringOneDigitAreaCode.assign( stringArg,1,1); 
   intNPDNumber = fVerify_NPDNumber();
   if(intNPDNumber < 0) { return false; }
   stringThreeDigitAreaCode    = NPD[intNPDNumber];
  }

   return true;
   
}// end Call_Data::fLoad_Info_Code(string stringArg, bool boolNPD)


int Call_Data::fFindTransferDataWithTransferTarget(string strUUID)
{
 vector<Transfer_Data>::size_type  szTranVector = vTransferData.size();

 if (strUUID.empty()) {return -1;}

 for (unsigned int i = 0; i < szTranVector; i++)
  {
   if (vTransferData[i].strTransferTargetUUID == strUUID) {return i;}
  }
 return -1;
}

int Conf_Data::fFindTransferTargetInConfVector(string strUUID)
{
 vector<Channel_Data>::size_type  szConfVector = this->vectConferenceChannelData.size();

 if (strUUID.empty()) {return -1;}

 for (unsigned int i = 0; i < szConfVector; i++)  {
   if (vectConferenceChannelData[i].strTranData == strUUID) {return i;}
 }
 return -1;
}


bool Call_Data::fHasAttendedTransfer(int iPositionNumber)
{
 vector<Transfer_Data>::size_type  szTranVector = vTransferData.size();
 if (vTransferData.empty())       {return false;}

 for (unsigned int i = 0; i < szTranVector; i++)
  {
   if (!Validate_Integer(vTransferData[i].strTransferFromPosition))           {continue;}
   if (((int) char2int(vTransferData[i].strTransferFromPosition.c_str()) == iPositionNumber)&& 
      (vTransferData[i].eTransferMethod == mATTENDED_TRANSFER))               {return true;}
  }
 return false;
}
bool Call_Data::fPerformedABlindTransfer(int iPositionNumber)
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();

 if (vTransferData.empty())       {return false;}

 for (unsigned int i = 0; i < szTranVector; i++)
  {
   if (!Validate_Integer(vTransferData[i].strTransferFromPosition))        {continue;}
   if (((int) char2int(vTransferData[i].strTransferFromPosition.c_str()) == iPositionNumber)&& 
      (vTransferData[i].eTransferMethod == mBLIND_TRANSFER))               {return true;}
  }
return false;
}

int Call_Data::fFindTransferData(string strTransferNumber, unsigned int iPosition)
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();
 int                                     i = szTranVector-1;
 vector<Transfer_Data>::reverse_iterator rit;

 if (vTransferData.empty())                                             {return -1;}
 for(  rit=vTransferData.rbegin() ; rit < vTransferData.rend(); ++rit) 
   {
    if ((rit->strTransfertoNumber == strTransferNumber)&&
       (char2int(rit->strTransferFromPosition.c_str()) == iPosition))   {return i;}
    i--;
   }
 return -1;
}

int Call_Data::fFindTransferData(unsigned int iPosition)
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();
 int                                     i = szTranVector-1;
 vector<Transfer_Data>::reverse_iterator rit;

 if (vTransferData.empty())                                             {return -1;}
 for(  rit=vTransferData.rbegin() ; rit < vTransferData.rend(); ++rit) 
   {
    if ((char2int(rit->strTransferFromPosition.c_str()) == iPosition))   {return i;}
    i--;
   }
 return -1;
}

int Call_Data::fFindParkData(string strTransferNumber)
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();
 int                                     i = szTranVector-1;
 vector<Transfer_Data>::reverse_iterator rit;
 if (vTransferData.empty())                                             {return -1;}
 for(  rit=vTransferData.rbegin() ; rit < vTransferData.rend(); ++rit) 
   {
    if (rit->strValetExtension == strTransferNumber)                  {return i;}
    i--;
   }
 return -1;
}


int Call_Data::fFindTransferData(string strTransferNumber)
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();
 int                                     i = szTranVector-1;
 vector<Transfer_Data>::reverse_iterator rit;
 if (vTransferData.empty())                                             {return -1;}
 for(  rit=vTransferData.rbegin() ; rit < vTransferData.rend(); ++rit) 
   {
    if (rit->strTransfertoNumber == strTransferNumber)                  {return i;}
    i--;
   }
 return -1;
}


int Call_Data::fFindTransferData(string strChannel, string strUniqueId)
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();
 int                                     i = szTranVector-1;
 vector<Transfer_Data>::reverse_iterator rit;

 if (vTransferData.empty())                                             {return -1;}
 for(  rit=vTransferData.rbegin() ; rit < vTransferData.rend(); ++rit) 
   {
     if (rit->strTransferFromUniqueid == strUniqueId) {return i;}
    i--;
   }
 return -1;
}

int Call_Data::fFindTransferTarget(string strTransferTargetID )
{
 vector<Transfer_Data>::size_type        szTranVector = vTransferData.size();
 int                                     i = szTranVector-1;
 vector<Transfer_Data>::reverse_iterator rit;

 if (vTransferData.empty())                                             {return -1;}
 for(  rit=vTransferData.rbegin() ; rit < vTransferData.rend(); ++rit) 
   {
     if (rit->strTransferTargetUUID == strTransferTargetID) {return i;}
    i--;
   }
 return -1;
}


bool Call_Data::fRemoveTransferData(unsigned int index)
{
 vector<Transfer_Data>::size_type      sz = vTransferData.size();

 if (index >= sz)                 {return false;}
 if (vTransferData.empty())       {return false;}

 vTransferData.erase(vTransferData.begin()+index);
 return true;
}



void Call_Data::fSetCallInfoToSticks()
{     
 fLoadCallbackNumber(BLOCKED_CALL_ID_NUMBER);       
 stringPANI =  "";
 stringInfoCode = "40";
}

void Call_Data::fSetBadPANInumber()
{   
 stringPANI =  BAD_911_ANI_NUMBER;
}



bool  Call_Data::fIsOutboundCall()
{
 size_t found;

 found = strCallTypeDisplay.find("Outbound");
 if (found == string::npos) {return false;}
 else                       {return true;}
}

void  Call_Data::fReleaseConferenceNumber()
{
 int                                    intRC;
 extern Conference_Number               objConferenceList;
 int                                    index;

 index = (int) intConferenceNumber;

 intConferenceNumber = 0; 
 strConferenceNumber = int2str(intConferenceNumber);

 if (index < MIN_CONFERENCE_NUMBER) {return;}
 if (index > MAX_CONFERENCE_NUMBER) {return;}
 
 intRC = sem_wait(&sem_tMutexConferenceNumberAssignmentObject);					
 if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexConferenceNumberAssignmentObject, "ANI    - sem_wait@sem_tMutexConferenceNumberAssignmentObject in ANI Thread()", 1);}
 objConferenceList.ReleaseConferenceNumber(index);
 sem_post(&sem_tMutexConferenceNumberAssignmentObject);

}

bool Call_Data::fLoad_Conference_Number(string strData)
{
 string strInput = strData;

 if (strInput.length() > 3)
  {
   strInput = strInput.substr( strInput.length()-3,3);
  }

 if (!ValidateFieldAndRange(ANI,strInput, strInput, 1, "Conference Number", intMAX_CONFERENCE_NUMBER, 0)) {return false;}
  
 strConferenceNumber = strInput;
 intConferenceNumber = char2int(strInput.c_str());

 return true;
}

int  Call_Data::fFindCallbackwithChannelIDinConferenceVector(string strCallback, string strUUID)
{
 size_t sz;
 
 sz = ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < sz; i++) {
   if ((strUUID == ConfData.vectConferenceChannelData[i].strChannelID)&&(strCallback == ConfData.vectConferenceChannelData[i].strCallerID)) {return i;}
 }

return -1;
}



int  Call_Data::fFindChannelIDinConferenceVector(string strUUID)
{
 size_t sz;
 

 sz = ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < sz; i++)
  {
   if (strUUID == ConfData.vectConferenceChannelData[i].strChannelID) {return i;}
  }

return -1;
}


int  Call_Data::fLegendInConferenceChannelDataVector(string strLegend)
{
 size_t sz;
 

 sz = ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < sz; i++)
  {
   if (strLegend == ConfData.vectConferenceChannelData[i].strConfDisplay) {return i;}
  }

return -1;
}

void Call_Data::fSetANIformat(int iFormat, int i)
{
 extern ANI_Data_Format                                 ANIdataFormat;
 intANIformat = iFormat;
 ANIdataFormat.fSetANIformatReceived(iFormat, i);
}

bool Call_Data::fLoadCallUniqueID(string strArg)
{
 size_t found;

 if (strArg.length() != CONTROLLER_UNIQUE_ID_LENGTH) {return false;} // TBC error message
 found = strArg.find_first_not_of("0123456789");
 if (found != string::npos)                          {return false;} //TBC error message 
 intUniqueCallID = char2int(strArg.c_str());
 stringUniqueCallID = strArg;
 return true;
}



void Call_Data::fLoadTrunk(int intArg)
{
 intTrunk = intArg;
 stringTrunk = int2strLZ(intArg);
}

void Call_Data::fLoadPosn(int intArg)
{
 intPosn 	= intArg;
 stringPosn 	= int2strLZ(intArg);
}

bool Call_Data::fLoadPosn(string strArg)
{
 size_t found;
 
 if (strArg.empty()) {return false;}
 found = strArg.find_first_not_of("0123456789");
 if (found != string::npos)
  {SendCodingError( "call_data_functions.cpp - coding error in call to fn Call_Data::fLoadPosn(string strArg)"); return false;}

  intPosn       = char2int(strArg.c_str());
  stringPosn    = int2strLZ(intPosn);
  return true;
}



string Call_Data::fTenDigitCallbackGUIformat()
{
 if (stringTenDigitPhoneNumber.length()!=10) {return "";}

 string strOutput ="(";
 strOutput+= stringTenDigitPhoneNumber.substr(0,3);
 strOutput+= ") " + stringTenDigitPhoneNumber.substr(3,3)+"-";
 strOutput+= stringTenDigitPhoneNumber.substr(6,4);
 return strOutput;
}

string Call_Data::fTenDigitCallbackGUIformat(string strInput)
{
 if (strInput.length()!=10) {return "";}

 string strOutput ="(";
 strOutput+= strInput.substr(0,3);
 strOutput+= ") " + strInput.substr(3,3)+"-";
 strOutput+= strInput.substr(6,4);
 return strOutput;
}


int Call_Data::fNumPositionsOnHold()
{
 size_t Found;
 int    iCount = 0;

 Found = ConfData.strOnHoldPositions.find(ANI_CONF_MEMBER_POSITION_PREFIX);

 while (Found != string::npos)
  {
   iCount++;
   Found =  ConfData.strOnHoldPositions.find(ANI_CONF_MEMBER_POSITION_PREFIX, Found+1 );
  }

return iCount;
}

int Call_Data::fNumPositionsInConference()
{
 size_t Found;
 int    iCount = 0;

 Found = ConfData.strActiveConferencePositions.find(ANI_CONF_MEMBER_POSITION_PREFIX);

 while (Found != string::npos)
  {
   iCount++;
   Found =  ConfData.strActiveConferencePositions.find(ANI_CONF_MEMBER_POSITION_PREFIX, Found+1 );
  }

return iCount;
}

int Call_Data::fNumDestinationsInConference()
{
 size_t Found;
 int    iCount = 0;

 Found = ConfData.strActiveConferencePositions.find(ANI_CONF_MEMBER_DEST_PREFIX);

 while (Found != string::npos)
  {
   iCount++;
   Found =  ConfData.strActiveConferencePositions.find(ANI_CONF_MEMBER_DEST_PREFIX, Found+1 );
  }

return iCount;
}

void Call_Data::fRemoveCallPark() {
 size_t sz;

 this->ConfData.strParkParticipant.clear();
 this->ConfData.strParkTime.clear();
 this->ConfData.strParkLot.clear();

 sz = this->ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < sz; i++) {
  this->ConfData.vectConferenceChannelData[i].boolParked = false;
  this->ConfData.vectConferenceChannelData[i].strValetExtension.clear();
 }
 
 return;
}

void Call_Data::fSetCallPark(string strParticipant, string strLot) {
 // Only One channel can be parked .....
 string 		strTime = "";
 string                 strFullTimeStamp;
 struct timespec 	timespecTimeNow;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 strFullTimeStamp = get_time_stamp(timespecTimeNow);
 strTime.append(strFullTimeStamp,0,27);

 this->ConfData.strParkParticipant = strParticipant;
 this->ConfData.strParkTime = strTime;
 this->ConfData.strParkLot = strLot;

 return;
}


bool Call_Data::fIsChannelUUIDfromPark(string stringUUID) {

size_t sz;
sz = this->ConfData.vectConferenceChannelData.size();

 for (unsigned int i = 0; i < sz; i++) {

  if ((stringUUID == this->ConfData.vectConferenceChannelData[i].strChannelID)&&
     (this->ConfData.vectConferenceChannelData[i].strCustName == "Call Park" )) {return true;}
 }
 return false;
}

bool Call_Data::fTransferDataIsRingback(string stringUUID) {

size_t sz;

sz = this->vTransferData.size();

for (unsigned int i = 0; i < sz; i++) {
// //cout << stringUUID << endl;
// //cout << this->vTransferData[i].strTransferFromUniqueid << endl;
// //cout << "method ringback ? " << (this->vTransferData[i].eTransferMethod == mRING_BACK) << endl;
 if ((stringUUID == this->vTransferData[i].strTransferFromUniqueid)&&(this->vTransferData[i].eTransferMethod == mRING_BACK)) {return true;}
}

return false;
}


void Call_Data::fOnHoldPositionAdd(int i)
{
 string 		strNewPosition;
 string                 strNewPositionAndTime;
 string                 strFullTimeStamp;
 string 		strTime = "";
 struct timespec 	timespecTimeNow;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 strFullTimeStamp = get_time_stamp(timespecTimeNow);
 strTime.append(strFullTimeStamp,0,27);


 strNewPosition = charTAB;
 strNewPosition += ANI_CONF_MEMBER_POSITION_PREFIX;
 strNewPosition += int2str(i);
 strNewPositionAndTime = strNewPosition + "." + strTime;
 
 strNewPosition += charTAB;
 strNewPositionAndTime += charTAB;

 if (!i)               {return;}
 if(!fIsOnHold(i)){
  ConfData.strOnHoldPositions.append(strNewPosition);
  ConfData.strOnHoldPosAndTime.append(strNewPositionAndTime);
  //modify confdata .... and position-data on hold boolean
  ConfData.fSetPositionOnHoldinConfVector(i);
 }
 else                  {return;}
} 

void Call_Data::fOnHoldPositionRemove(int i)
{
 size_t Position;
 size_t PositionEnd;

 string strRemovePosition;
 string strRemovePositionAndTimePrefix;

 strRemovePosition = charTAB;
 strRemovePosition += ANI_CONF_MEMBER_POSITION_PREFIX;
 strRemovePosition += int2str(i);
 strRemovePositionAndTimePrefix = strRemovePosition;
 strRemovePositionAndTimePrefix += ".";
 strRemovePosition += charTAB;

 Position =  ConfData.strOnHoldPositions.find(strRemovePosition);

 if (Position == string::npos) {return;}

 ConfData.strOnHoldPositions.erase(Position,strRemovePosition.length());

 Position = ConfData.strOnHoldPosAndTime.find(strRemovePositionAndTimePrefix);
 if (Position == string::npos)    {return;}
 PositionEnd = ConfData.strOnHoldPosAndTime.find_first_of(charTAB, Position + strRemovePositionAndTimePrefix.length()); 
 if (PositionEnd == string::npos) {return;}

 ConfData.strOnHoldPosAndTime.erase(Position, (PositionEnd - Position)+1);
 ConfData.fSetPositionOffHoldinConfVector(i);
} 

bool Call_Data::fIsOnHold(int i)
{
 size_t SearchPosition;
 string strFind;

 strFind = charTAB;
 strFind += ANI_CONF_MEMBER_POSITION_PREFIX;
 strFind += int2str(i);
 strFind += charTAB;

 SearchPosition =  ConfData.strOnHoldPositions.find(strFind);

 if (SearchPosition == string::npos) {return false;}
 else                                {return true;}
}

bool Call_Data::fConferenceBlindTransferWasSet()
{
 bool boolreturn = boolBlindTransferSet;

 boolBlindTransferSet = false;

 return boolreturn;
}


bool Call_Data::fConferenceStringAdd(conference_member eConf, int i)
{
 string strCaller = "";
 string strCallerWTABs = "";
 bool   bPositionAdded = false;

 strCallerWTABs += charTAB;

 switch (eConf)
  {
   case POSITION_CONF:
         strCaller += ANI_CONF_MEMBER_POSITION_PREFIX; 
         strCaller+= int2str(i);
        break;
   case CALLER_CONF:
        if (!i) {strCaller += ANI_CONF_MEMBER_CALLER_PREFIX ;}
        else    {strCaller += ANI_CONF_MEMBER_CALLER_PREFIX ; strCaller += int2str(i);}
        break;
   case DEST_CONF:
         strCaller += ANI_CONF_MEMBER_DEST_PREFIX; 
         strCaller+= int2str(i);
        break;
   case XFER_CONF:
         strCaller += ANI_CONF_MEMBER_PBX_TRANSFER_PREFIX;
         strCaller+= int2str(i);
         break;
   case TANDEM_CONF:
         strCaller += ANI_CONF_MEMBER_TANDEM_TRANSFER_PREFIX;
         strCaller+= int2str(i);
         break;
   case FLASH_CONF:
         strCaller += ANI_CONF_MEMBER_FLASH_TRANSFER_PREFIX;
         strCaller+= int2str(i);
         break;
  }
 
 strCallerWTABs += strCaller;
 strCallerWTABs += charTAB; 

 if(!fIsInConference(strCaller,true)){ConfData.strHistoryConferencePositions.append(strCallerWTABs); bPositionAdded = true;}
 if(!fIsInConference(strCaller))     {ConfData.strActiveConferencePositions.append(strCallerWTABs); bPositionAdded = true;}

 return bPositionAdded;
}

bool Call_Data::fConferenceStringRemove(string strConf)
{
 size_t Found;
 string strRemove;
 string strPositionConferencePrefix = ANI_CONF_MEMBER_POSITION_PREFIX;
 string strPositionNumber;
 int    i;

 if (!strConf.length()) {return false;}

 strRemove = charTAB;
 strRemove += strConf;
 strRemove += charTAB;
 Found =  ConfData.strActiveConferencePositions.find(strRemove);

 if (Found == string::npos) {return false;}

 ConfData.strActiveConferencePositions.erase(Found,strRemove.length());

 Found = strConf.find(ANI_CONF_MEMBER_POSITION_PREFIX);
 if (Found == string::npos) {return true;}
 strPositionNumber = strConf.erase(Found, strPositionConferencePrefix.length());

 Found = strPositionNumber.find_first_not_of("0123456789");
 if (Found != string::npos) {return true;}

 i =char2int(strPositionNumber.c_str());
 fOnHoldPositionRemove(i);

 
 return true; 
}

bool Call_Data::fConferenceStringRemove(conference_member eConf, int i)
{
 size_t Found;
 string strCaller = "";
 string strCallerWTABs = "";
 strCallerWTABs += charTAB;

 switch (eConf)
  {
   case POSITION_CONF:
         strCaller += ANI_CONF_MEMBER_POSITION_PREFIX; 
         strCaller+= int2str(i);
         fOnHoldPositionRemove(i);
        break;
   case CALLER_CONF:
        if (!i) {strCaller += ANI_CONF_MEMBER_CALLER_PREFIX ;}
        else    {strCaller += ANI_CONF_MEMBER_CALLER_PREFIX ; strCaller += int2str(i);}
        break;
   case DEST_CONF:
         strCaller += ANI_CONF_MEMBER_DEST_PREFIX; 
         strCaller+= int2str(i);
        break;
   case XFER_CONF:
         strCaller += ANI_CONF_MEMBER_PBX_TRANSFER_PREFIX;
         strCaller+= int2str(i);
         break;
   case TANDEM_CONF:
         strCaller += ANI_CONF_MEMBER_TANDEM_TRANSFER_PREFIX;
         strCaller+= int2str(i);
         break; 
   case FLASH_CONF:
         strCaller += ANI_CONF_MEMBER_FLASH_TRANSFER_PREFIX;
         strCaller+= int2str(i);
         break; 
  }
 
 strCallerWTABs += strCaller;
 strCallerWTABs += charTAB; 
 Found =  ConfData.strActiveConferencePositions.find(strCallerWTABs);

 if (Found == string::npos) {return false;}

 ConfData.strActiveConferencePositions.erase(Found,strCallerWTABs.length());

 return true; 
} 

void Call_Data::fAddAllPositionsToConference()
{
 for (int i = 1; i <= intNUM_WRK_STATIONS; i++) 
  {
   fConferenceStringAdd(POSITION_CONF,i);
  }
}





bool Call_Data::fConferenceStringRemove(int i)
{
 size_t Position;
 string strRemovePosition;

 if (!i) {return false;}

 strRemovePosition  = charTAB;
 strRemovePosition += ANI_CONF_MEMBER_POSITION_PREFIX;
 strRemovePosition += int2str(i);
 strRemovePosition += charTAB;

 Position =  ConfData.strActiveConferencePositions.find(strRemovePosition);

 if (Position == string::npos) {return false;}

 ConfData.strActiveConferencePositions.erase(Position,strRemovePosition.length());

 fOnHoldPositionRemove(i);



 return true; 
} 

bool Call_Data::fIsInConference(conference_member eConf)
{
 size_t SearchPosition;
 string strFind;
 string strCaller = "";

 switch (eConf)
  {
   case POSITION_CONF:
         strFind = ANI_CONF_MEMBER_POSITION_PREFIX ; 
        break;
   case CALLER_CONF:
        strFind = ANI_CONF_MEMBER_CALLER_PREFIX;
        break;
   case DEST_CONF:
         strFind = ANI_CONF_MEMBER_DEST_PREFIX ; 
        break;
   case XFER_CONF:
         strFind = ANI_CONF_MEMBER_PBX_TRANSFER_PREFIX;
         break;
   case TANDEM_CONF:
         strFind = ANI_CONF_MEMBER_TANDEM_TRANSFER_PREFIX;
         break;
   case FLASH_CONF:
         strFind = ANI_CONF_MEMBER_FLASH_TRANSFER_PREFIX;
         break;
  }

 SearchPosition = ConfData.strActiveConferencePositions.find(strFind);

 if (SearchPosition == string::npos) {return false;}
 else                                {return true;}  
}

bool Call_Data::fIsInConference(string strConf, bool boolCheckHistory)
{
 size_t SearchPosition;
 string strFind;

 strFind = charTAB + strConf + charTAB;

 if (boolCheckHistory) { SearchPosition =  ConfData.strHistoryConferencePositions.find(strFind);}
 else                  { SearchPosition =  ConfData.strActiveConferencePositions.find(strFind);}

 if (SearchPosition == string::npos) {return false;}
 else                                {return true;}
}

bool Call_Data::fIsInConference(conference_member eConf, int i)
{
 string strCaller = ConferenceMemberString(eConf,i);
 
 return fIsInConference(strCaller);
}

string ConferenceMemberString(conference_member eConf, int i)
{
 string strConferenceString = "";

 switch (eConf)
  {
   case POSITION_CONF:
         strConferenceString += ANI_CONF_MEMBER_POSITION_PREFIX ; 
         strConferenceString+= int2str(i);
        break;
   case CALLER_CONF:
        if (!i) {strConferenceString += ANI_CONF_MEMBER_CALLER_PREFIX;}
        else    {strConferenceString += ANI_CONF_MEMBER_CALLER_PREFIX; strConferenceString += int2str(i);}
        break;
   case DEST_CONF:
         strConferenceString += ANI_CONF_MEMBER_DEST_PREFIX ; 
         strConferenceString += int2str(i);
        break;
   case XFER_CONF:
         strConferenceString += ANI_CONF_MEMBER_PBX_TRANSFER_PREFIX;
         strConferenceString += int2str(i);
         break;
   case TANDEM_CONF:
         strConferenceString += ANI_CONF_MEMBER_TANDEM_TRANSFER_PREFIX;
         strConferenceString += int2str(i);
         break;
   case FLASH_CONF:
         strConferenceString += ANI_CONF_MEMBER_FLASH_TRANSFER_PREFIX;
         strConferenceString += int2str(i);
         break;    
  }
 
 return strConferenceString;
}

bool Call_Data::fIsInPositionHistory(string strLegend)
{
 size_t found;

 if (!strLegend.length()){return false;}

 found = ConfData.strHistoryConferencePositions.find(strLegend);
 if (found !=string::npos) {return true;}
 else                      {return false;}

}


bool Call_Data::fLoadCallbackNumber(unsigned long long int intArg)
{
 intCallbackNumber = intArg;
 stringTenDigitPhoneNumber = int2str(intArg);
 if (stringTenDigitPhoneNumber.length() != 10) {stringTenDigitPhoneNumber.clear(); intCallbackNumber = 0; return false;}
 stringSevenDigitPhoneNumber = stringTenDigitPhoneNumber.substr(3,7);
 stringThreeDigitAreaCode.assign(stringTenDigitPhoneNumber,0,3);
 stringTelnoPrefix.assign(stringTenDigitPhoneNumber,3,3);
 stringTelnoSuffix.assign(stringTenDigitPhoneNumber,6,4);
 return true;
}

bool Call_Data::fLoadCallbackNumber(string strNumber)
{
 size_t found;
 found = strNumber.find_first_not_of("0123456789");
 if (found != string::npos)    {return false;}
 if (strNumber.length() != 10) {return false;}

 intCallbackNumber = char2int(strNumber.c_str());
 stringTenDigitPhoneNumber = strNumber;
 stringSevenDigitPhoneNumber = stringTenDigitPhoneNumber.substr(3,7);
 stringThreeDigitAreaCode.assign(stringTenDigitPhoneNumber,0,3);
 stringTelnoPrefix.assign(stringTenDigitPhoneNumber,3,3);
 stringTelnoSuffix.assign(stringTenDigitPhoneNumber,6,4);
 return true;
}




string Call_Data::fLoadCallBackDisplay(string strInput, bool SetCallback)
{
 string                         strTemp = strInput;
 string                         strExtension;
 size_t                         found;
 bool                           bAllNumbers = false;

// int                            index;
 extern Telephone_Devices       TelephoneEquipment;

 
 if (strInput.empty()) {return "UNKNOWN";}

 strTemp = TelephoneEquipment.fRemoveRoutingPrefix(strInput);

 found = strTemp.find_first_not_of("0123456789");
 if (found == string::npos) {bAllNumbers = true;}

 if (bAllNumbers)
  {
   if ((strTemp[0] == '9')&&(strTemp.length() > 10)) {strTemp.erase(0,1);}
   if ((strTemp[0] == '1')&&(strTemp.length() > 10)) {strTemp.erase(0,1);}
     
   if (strTemp.length() == 4)
    {
     strCallBackDisplay = "Ext: " + strTemp; return strCallBackDisplay;
    }
   if (strTemp.length() == 10)
    { 
     if ((!intCallbackNumber)&&(SetCallback)) {fLoadCallbackNumber(strTemp); strCallBackDisplay = fTenDigitCallbackGUIformat(); return strCallBackDisplay;}
     else                                     { strCallBackDisplay = fTenDigitCallbackGUIformat(strTemp);                       return strCallBackDisplay;}
    }
  }// end if (bAllNumbers)
   
 strCallBackDisplay  =  strTemp;
 return  strCallBackDisplay;
}


string Call_Data::fLoadCallBackDisplay()
{
 if (intCallbackNumber) 
  {
    strCallBackDisplay  = stringTenDigitPhoneNumber;
   return  strCallBackDisplay;
  }
 else if (!strSIPphoneNumber.empty())
  {
    strCallBackDisplay  = strSIPphoneNumber;
   return  strCallBackDisplay;
  }

 strCallBackDisplay  = "UNKNOWN";
return  strCallBackDisplay;
}


bool Stream_ID::fLoadStreamID(string strInput)
{
 //format vx_epoch_callerid_streamid.wav
 // where x is an integer
 size_t indexA, indexB, indexC, indexD;

 if (strInput.empty())                    {return false;}

 do{
  indexA = strInput.find("/");

  if (indexA != string::npos){
   strInput.erase(0, indexA + 1);
  }
 }
 while (indexA != string::npos);

 indexA = strInput.find("_");
 if (indexA == string::npos)              {return false;}
 indexB = strInput.find("_", indexA+1);
 if (indexB == string::npos)              {return false;} 
 indexC = strInput.find("_", indexB+1);
 if (indexC == string::npos)              {return false;} 
 indexD = strInput.find(".wav", indexC+1);
 if (indexD == string::npos)              {return false;} 
 
  this->strEpoch    = strInput.substr(indexA+1, (indexB - indexA -1)); ////cout << "Epoch -> " << this->strEpoch << endl;
  this->strCallerID = strInput.substr(indexB+1, (indexC - indexB -1)); ////cout << "CLID  -> " << this->strCallerID << endl;
  this->strUUID     = strInput.substr(indexC+1, (indexD - indexC -1)); ////cout << "UUID  -> " << this->strUUID << endl;
 
 return true;
}


/*
bool Call_Data::fFindandChangeChannelNameinVectors(string strChannel, string strUniqueID)
{
 vector<Transfer_Data>::size_type             szTransVector = vTransferData.size();

 bool bMatchFound = false;

 for (unsigned int i = 0; i< szTransVector; i++) 
   {
    if (vTransferData[i].strTransferFromUniqueid == strUniqueID)
      { vTransferData[i].strTransferFromChannel = strChannel; vTransferData[i].eTransferMethod = mPOORMAN_BLIND_TRANSFER; bMatchFound = true; }
   }
 return bMatchFound;
}
*/
