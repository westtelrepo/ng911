
/*****************************************************************************
* FILE: rcc.cpp
*  
*
* DESCRIPTION:
* Contains the thread(s) which communicate to the Radio Contact Closure Switches 
*
* AUTHOR: 3/22/2011 Bob McCarthy
* LAST REVISED:  
*
* COPYRIGHT: 2006-2011 Experient Corporation
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//globals
queue <ExperientDataClass>                      queue_objRCCData;
string                                          strRCC_DEVICE_ALL_OPEN;
string                                          strRCC_DEVICE_ALL_CLOSE;
string                                          strRCC_DEVICE_OPEN_ONE    = "setstate,1:1,1";
string                                          strRCC_DEVICE_OPEN_TWO    = "setstate,1:2,1";
string                                          strRCC_DEVICE_OPEN_THREE  = "setstate,1:3,1";
string                                          strRCC_DEVICE_CLOSE_ONE   = "setstate,1:1,0";
string                                          strRCC_DEVICE_CLOSE_TWO   = "setstate,1:2,0";
string                                          strRCC_DEVICE_CLOSE_THREE = "setstate,1:3,0";
string                                          strRCC_DEVICE_GET_VERSION = "getversion";
bool                                            boolCONTACT_RELAY_INSTALLED = false;
//ExperientTCPPort                                RelayContactClosurePort[NUM_WRK_STATIONS_MAX+1];
RCC_Device                                      RCCdevice[NUM_WRK_STATIONS_MAX+1];
itimerspec                                      itimerspecRCCMessageMonitorDelay;
itimerspec                                      itimerspecRCCPingInterval;
itimerspec                                      itimerspecRCCSendDelay;
timer_t                                         timer_tRCCMessageMonitorTimerId;
timer_t                                         timer_tRCC_Ping_Event_TimerId;
timer_t                                         timer_tRCC_Send_Event_TimerId;


//fwd function
void    *Contact_Closure_Unit_Thread(void *voidArg);
void    *RCC_Messaging_Monitor_Event(void *voidArg);
void    Ping_RCC_Event(union sigval union_sigvalArg);
void    CheckRCCportsStatus();
string  RCCstatusString();

void *RCC_Thread(void *voidArg)
{
 int                            intRC;
 MessageClass                   objMessage;
 ExperientDataClass             objData;
 param_t                        params[NUM_WRK_STATIONS_MAX+1];
 pthread_t                      RCCListenThread[NUM_WRK_STATIONS_MAX+1];
 pthread_t 			pthread_tRCCMessagingThread;
 pthread_attr_t                 RCCpthread_attr_tAttr;
 struct sigevent                sigeventRCCMessagingMonitorEvent;
 struct sigevent	        sigeventPingRCCEvent;
 struct timespec                timespecRemainingTime;
 Thread_Data                    ThreadDataObj;      
 extern vector <Thread_Data> 	ThreadData;

 extern RCC_Device              RCCdevice[NUM_WRK_STATIONS_MAX+1];
 extern Port_Data               objBLANK_RCC_PORT;
 extern Text_Data		objBLANK_TEXT_DATA;
 extern Call_Data               objBLANK_CALL_RECORD;
 extern Freeswitch_Data         objBLANK_FREESWITCH_DATA;
 extern ALI_Data                objBLANK_ALI_DATA;
 extern Thread_Trap             OrderlyShutDown;
 extern Thread_Trap             UpdateVars;
 extern int                     intNUM_WRK_STATIONS;


/*
 intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
 if (intRC) {objMessage.fMessage_Create(LOG_WARNING, 901, objBLANK_CALL_RECORD, objBLANK_RCC_PORT, KRN_MESSAGE_142); enQueue_Message(RCC,objMessage);}
*/

 ThreadDataObj.strThreadName ="RCC";
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in rcc.cpp::RCC_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 // Initialize message
 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,900, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_RCC_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_900, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(RCC,objMessage);

 strRCC_DEVICE_ALL_OPEN =  strRCC_DEVICE_OPEN_ONE + charCR;
 strRCC_DEVICE_ALL_OPEN += strRCC_DEVICE_OPEN_TWO;
 strRCC_DEVICE_ALL_OPEN += charCR;
 strRCC_DEVICE_ALL_OPEN += strRCC_DEVICE_OPEN_THREE;

 strRCC_DEVICE_ALL_CLOSE =  strRCC_DEVICE_CLOSE_ONE + charCR;
 strRCC_DEVICE_ALL_CLOSE += strRCC_DEVICE_CLOSE_TWO;
 strRCC_DEVICE_ALL_CLOSE += charCR;
 strRCC_DEVICE_ALL_CLOSE += strRCC_DEVICE_CLOSE_THREE;

/*
 // set up RCC message monitor timer event
 sigeventRCCMessagingMonitorEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventRCCMessagingMonitorEvent.sigev_notify_function 		= RCC_Messaging_Monitor_Event;
 sigeventRCCMessagingMonitorEvent.sigev_notify_attributes 		= NULL;

 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventRCCMessagingMonitorEvent, &timer_tRCCMessageMonitorTimerId);
 if (intRC){Abnormal_Exit(RCC, EX_OSERR, KRN_MESSAGE_180, "timer_tRCCMessageMonitorTimerId", int2strLZ(errno));}

 // load settings into stucture and then stucture into the timer
// Set_Timer_Delay(&itimerspecRCCMessageMonitorDelay, 0, intRCC_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intRCC_MESSAGE_MONITOR_INTERVAL_NSEC );
 Set_Timer_Delay(&itimerspecRCCMessageMonitorDelay, 0, intRCC_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0 );
 timer_settime( timer_tRCCMessageMonitorTimerId, 0, &itimerspecRCCMessageMonitorDelay, NULL);		// arm timer
*/

  pthread_attr_init(&RCCpthread_attr_tAttr);
  pthread_attr_setdetachstate(&RCCpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);



 // start RCC messaging thread
 intRC = pthread_create(&pthread_tRCCMessagingThread, &RCCpthread_attr_tAttr, *RCC_Messaging_Monitor_Event, NULL);
 if (intRC) {Abnormal_Exit(RCC, EX_OSERR, RCC_MESSAGE_916);}

 // set up Message ping Timed Event
 sigeventPingRCCEvent.sigev_notify 			        = SIGEV_THREAD;
 sigeventPingRCCEvent.sigev_notify_function 		        = Ping_RCC_Event;
 sigeventPingRCCEvent.sigev_notify_attributes 			= NULL;
 // create timer
 intRC = timer_create(CLOCK_MONOTONIC, &sigeventPingRCCEvent, &timer_tRCC_Ping_Event_TimerId);
 if (intRC){Abnormal_Exit(RCC, EX_OSERR, KRN_MESSAGE_180, "timer_tRCC_Ping_Event_TimerId", int2strLZ(errno));}
 // load settings into stucture and then stucture into the timer
 itimerspecRCCPingInterval.it_value.tv_sec                       = RCC_PING_INTERVAL_SEC;	                        // Delay for n sec
 itimerspecRCCPingInterval.it_value.tv_nsec                      = 0;	                				//           n nsec
 itimerspecRCCPingInterval.it_interval.tv_sec                    = 0;			                                // if both 0 run once
 itimerspecRCCPingInterval.it_interval.tv_nsec                   = 0;                 					//           n nsec
 timer_settime(timer_tRCC_Ping_Event_TimerId, 0, &itimerspecRCCPingInterval, NULL);	                                // arm timer


 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,907, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objBLANK_RCC_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_907); 
 enQueue_Message(RCC,objMessage);

  pthread_attr_init(&RCCpthread_attr_tAttr);
  for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
   {
    if ( RCCdevice[i].boolContactRelayInstalled)
     { 
      params[i].intPassedIn = i;
      pthread_attr_setdetachstate(&RCCpthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
      intRC = pthread_create(&RCCListenThread[i], &RCCpthread_attr_tAttr, *Contact_Closure_Unit_Thread, (void*) &params[i]);
      if (intRC) {Abnormal_Exit(RCC, EX_OSERR, RCC_MESSAGE_904);}
     }
   }



 //*********************************************************main loop ******************************************************************
 
  while(!boolSTOP_EXECUTION)
   {
    intRC = sem_wait(&sem_tRCCflag);							// wait for data signal
    if (intRC)
     {
      objMessage.fMessage_Create(LOG_WARNING, 915, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                 objBLANK_RCC_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_915); 
      enQueue_Message(RCC,objMessage);
     } 

   // Shut Down Trap Area
   if (boolSTOP_EXECUTION)
    {
     break;
    }

  if (boolUPDATE_VARS) 
   {
    UpdateVars.boolRCCThreadReady = true;
    while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
    UpdateVars.boolRCCThreadReady = false;       
   }




   intRC = sem_wait(&sem_tMutexRCCInputQ);						// Lock RCC INPUT Q
   if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCInputQ, "RCC    - sem_wait@sem_tMutexRCCInputQ in RCC_Thread", 1);}
              
   while (!queue_objRCCData.empty())  
    {
     objData = queue_objRCCData.front();
     queue_objRCCData.pop();

     switch(objData.enumRCCfunction)
      {
       case SINGLE:
        SendRCC_Signal(objData.enumRCCstate, objData.intWorkStation); break;
       case DUAL:
        SendRCC_Signal(objData.enumRCCstate, objData); break;
       case WITH_DELAY:
        SendRCC_Signal(objData.enumRCCstate, objData.intWorkStation, RCC_HANGUP_BUSY_DELAY); break;
       default:
        SendCodingError("rcc.cpp - Coding Error in switch(objData.enumRCCfunction) in fn *RCC_Thread()");
      }
               
    }
   sem_post(&sem_tMutexRCCInputQ);
  

   }// while(!boolSTOP_EXECUTION)						


 // shutdowm thread ...
  //wait a second for timers to clear ...
  experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);


 // timer_delete(timer_tRCCMessageMonitorTimerId);
  pthread_join( pthread_tRCCMessagingThread, NULL);
  for (int i = 1; i <= intNUM_WRK_STATIONS; i++)
   {
    if ( RCCdevice[i].boolContactRelayInstalled)
     { 
      pthread_join( RCCListenThread[i], NULL);
     }
   }


  OrderlyShutDown.boolRCCThreadReady = true;
 // sleep(intTHREAD_SHUTDOWN_DELAY_SEC);
  while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}    
  objMessage.fMessage_Create(0, 999, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                             objBLANK_RCC_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_999);
  objMessage.fConsole();

 return NULL;
}

void *Contact_Closure_Unit_Thread(void *voidArg)
{
 param_t*                                       p              = (param_t*) voidArg;
 int                                            intWorkstation = p->intPassedIn;
 MessageClass                                   objMessage;
 Port_Data                                      objPortData;
 Thread_Data                                    ThreadDataObj;
 int						intRC;

 extern Text_Data				objBLANK_TEXT_DATA; 
 extern Call_Data                               objBLANK_CALL_RECORD; 
 extern Freeswitch_Data         		objBLANK_FREESWITCH_DATA;
 extern ALI_Data                		objBLANK_ALI_DATA;   
 extern vector <Thread_Data> 			ThreadData;

 if (intWorkstation < 1)  {return NULL;}

 objPortData.fLoadPortData(RCC, intWorkstation);

 ThreadDataObj.strThreadName ="RCC-Port"+int2str(intWorkstation);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in rcc.cpp::Contact_Closure_Unit_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,908, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_908, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(RCC,objMessage);

 RCCdevice[intWorkstation].RelayContactClosurePort.intLocalPortNumber = RCCdevice[intWorkstation].intRCClocalPortNumber;
 RCCdevice[intWorkstation].RelayContactClosurePort.Remote_IP.stringAddress = RCCdevice[intWorkstation].RCCremoteIPaddress.stringAddress; 
 RCCdevice[intWorkstation].RelayContactClosurePort.intRemotePortNumber = RCCdevice[intWorkstation].intRCCremotePortNumber;
 RCCdevice[intWorkstation].RelayContactClosurePort.fInitializeRCCport(intWorkstation);
 RCCdevice[intWorkstation].RelayContactClosurePort.ConnectToRCCdevice();

 
 while (!boolSTOP_EXECUTION)
  { 
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);

   if (!RCCdevice[intWorkstation].RelayContactClosurePort.GetConnected()){
   // //cout << "not connected!";
    RCCdevice[intWorkstation].RelayContactClosurePort.ConnectToRCCdevice();
   } 

   RCCdevice[intWorkstation].RelayContactClosurePort.DoEvents();
  } // end  while (!boolSTOP_EXECUTION)

 RCCdevice[intWorkstation].RelayContactClosurePort.Disconnect();
 sleep(1);
 RCCdevice[intWorkstation].RelayContactClosurePort.DoEvents();
 return NULL;
								
}// end Contact_Closure_Unit_Thread(void *voidArg)






void Ping_RCC_Event(union sigval union_sigvalArg) {
 int intRC;
 extern struct itimerspec itimerspecDisableTimer;

 //Disable Timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tRCC_Ping_Event_TimerId, 0, &itimerspecDisableTimer, NULL);}

 intRC = sem_wait(&sem_tMutexRCCtable);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in Ping_RCC_Event(b)", 1);}
 for (int i = 1; i <= intNUM_WRK_STATIONS; i++)  {
   if (boolSTOP_EXECUTION) {sem_post(&sem_tMutexRCCtable);return;}

   if (RCCdevice[i].boolContactRelayInstalled){
 //    //cout << "IN PING" << endl; 
     if (RCCdevice[i].RelayContactClosurePort.GetConnected()) {
       intRC = Transmit_Data(RCC, i, (char*)"PING", 4);
  //     //cout << "SEND PING" << endl;
       if (intRC) {RCCdevice[i].boolRCCconnected = false;}
       else       {RCCdevice[i].boolRCCconnected = true; }
     }
     else {
       RCCdevice[i].boolRCCconnected = false;
     }
    // if (!RCCdevice[i].boolRCCconnected){RCCdevice[i].RelayContactClosurePort.ConnectToRCCdevice();}
   }// endif RCCdevice[i].boolContactRelayInstalled

 }
 sem_post(&sem_tMutexRCCtable);
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tRCC_Ping_Event_TimerId, 0, &itimerspecRCCPingInterval, NULL);} 
}





void *RCC_Messaging_Monitor_Event(void *voidArg) {
 int                            intRC;
 MessageClass                   objMessage;
 Thread_Data                    ThreadDataObj;
 extern sem_t                   sem_tMutexRCCMessageQ;
 extern sem_t                   sem_tMutexMainMessageQ;
 extern queue <MessageClass>    queue_objMainMessageQ;
 extern queue <MessageClass>    queue_objRCCMessageQ;
 extern vector <Thread_Data>    ThreadData;

 ThreadDataObj.strThreadName ="RCC Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in rcc.cpp::RCC_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

 do {

// if (boolSTOP_EXECUTION) {return;}
 //timer_settime(timer_tRCCMessageMonitorTimerId, 0, &itimerspecDisableTimer, NULL);

 experient_nanosleep(intRCC_MESSAGE_MONITOR_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}


 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in RCC_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexRCCMessageQ);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCMessageQ, "sem_wait@sem_tMutexRCCMessageQ in RCC_Messaging_Monitor_Event", 1);}

 while (!queue_objRCCMessageQ.empty())
  {
    objMessage = queue_objRCCMessageQ.front();
    queue_objRCCMessageQ.pop();

    switch (objMessage.intMessageCode)
     {
      case 0:
       // no action taken.....
      break;

      case 900:						
       // "-[DEBUG] Thread Created, PID %%% ThreadSelf %%%"
       break; 
      case 901:						
       // "[WARNING] RCC Thread Failed to Initialize, System Rebooting" ... should not pass thru here handled in krn
       break;
      case 902:						
       // "-[WARNING] RCC TCP Status: Disconnect, Code=%%% Desc=%%%"
       break;
      case 903:
       // "-[INFO] RCC TCP Status: Connected"
       break;
      case 904:
       // "-[WARNING] RCC Port Thread Failed to Initialize, System Rebooting" 						
       break; 
      case 905:
       // "[WARNING] Unable to set CPU affinity for RCC Thread"					
       break; 
      case 906:
       //  "-[INFO] RCC TCP Connection Status Code=%%% Desc=%%%"					
       break; 
      case 907:
       //  "-[DEBUG] Message Monitor Timer Event Initialized"					
       break; 
      case 908:
       //"-[DEBUG] Listen Thread Created, PID %%% ThreadSelf %%%"
       break;
      case 909:
       // "-[WARNING] No RCC Transmissions Received from %%% during Last %%% Seconds, (Ping %%%)"
       break;
      case 910:
       // "-[ALARM] RCC Transmissions from %%% Restored."
       break;
      case 911:
       // "-[WARNING] Reminder - Port Down for %%%, (Ping %%%)" 
       break;
      case 912:
       // ">[RAW] TCP Data Received (Data Follows) %%%"
       break;
      case 913:
       // "<[RAW] Data sent to IP=%%% (Data Follows)"
       break;
      case 914: 
       // ">RCC %%% signal sent"
       break;
      case 915:
       //"-[WARNING] Thread Signalling Semaphore failed to Lock"
       break;
      case 916:
       //"-[WARNING] Port Messsaging Thread Failed to Initialize, System Rebooting"
      break;
      case 917:
       //"-[WARNING] Data failed to send to IP=%%% (Data Follows)"
       break;
      case 918:
       //"-[WARNING] Error Connecting to RCC: %%% tries: %%%"
       break;
      case 919:
       //">[INFO] Initial RX from IP %%% Port %%%"
       break;
      case 999:
       //  "-Thread Complete"
       break;     	
      default:
       // used to find uncoded messages
       //cout << objMessage.intMessageCode << " Message Code Unrecognized in RCC Monitor\n";
       break;
       
   }// end switch


  // Push message onto Main Message Q
  queue_objMainMessageQ.push(objMessage);
   
  }// end while

 // UnLock RCC Message Q then Main Message Q
 sem_post(&sem_tMutexRCCMessageQ);
 sem_post(&sem_tMutexMainMessageQ);

 
 CheckRCCportsStatus();



 //re-arm timer
// if(boolSTOP_EXECUTION) {return;}
 //else                   {timer_settime(timer_tRCCMessageMonitorTimerId, 0, &itimerspecRCCMessageMonitorDelay, NULL);}

 } while(!boolSTOP_EXECUTION);


return NULL;
}// end RCC_Messaging_Monitor_Event()

void CheckRCCportsStatus() {
 int                    intRC;
 int                    RCCportCount = 0;
 int                    RCCportActive = 0;
 extern volatile int    iNumberRCCdevicesInstalled;
 extern volatile int    iNumberRCCdevicesConnected;

 intRC = sem_wait(&sem_tMutexRCCtable);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in CheckRCCportsStatus(a)", 1);}
 for (int i = 1; i <= intNUM_WRK_STATIONS; i++)  {
   if ( RCCdevice[i].boolContactRelayInstalled) { 
     RCCportCount++; 
     RCCdevice[i].RelayContactClosurePort.fCheckTimeSinceLastRX();
     if (RCCdevice[i].boolRCCconnected) {
      RCCportActive++;
     }
   }
 }
 iNumberRCCdevicesInstalled = RCCportCount;
 iNumberRCCdevicesConnected = RCCportActive;
 sem_post(&sem_tMutexRCCtable);
}

string RCCstatusString() {
 int                    intRC;
 string                 strReturnstring;
 extern volatile int    iNumberRCCdevicesInstalled;
 extern volatile int    iNumberRCCdevicesConnected;
 
 intRC = sem_wait(&sem_tMutexRCCtable);
 if (intRC){Semaphore_Error(intRC, RCC, &sem_tMutexRCCtable, "sem_wait@sem_tMutexRCCtable in RCCstatusString(a)", 1);}
 strReturnstring = int2str(iNumberRCCdevicesConnected) + ",";
 strReturnstring += int2str(iNumberRCCdevicesInstalled);
 sem_post(&sem_tMutexRCCtable);

 return strReturnstring;
}
