/*****************************************************************************
* FILE: globals.h
*  
*
* DESCRIPTION:
*  Contains external variable declarations that are global to the controller.
*  It does not contain class derived variables due to nesting problems and they
*  are declared seperately within each program thread.
*
*
* AUTHOR: 01/29/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/

#include "defines.h"
#include "header.h"

#ifndef globals_included_
#define globals_included_
	
using namespace std;



extern string                                   strCONTROLLER_UUID;
extern const char				charACK; 						
extern const char				charNAK; 						
extern const char				charCR;  						
extern const char				charSTX;						
extern const char				charETX;
extern const char                               charLF;
extern const char                               charTAB;
extern const char                               charBKSP;
extern const char                               charSPACE;	
extern const int                                XML_STATUS_CODE_CONNECT; 
extern const int                                XML_STATUS_CODE_DISCONNECT;
extern const int                                XML_STATUS_CODE_ON_HOLD;
extern const int                                XML_STATUS_CODE_OFF_HOLD_CONNECTED;
extern const int                                XML_STATUS_CODE_ABANDONED;
extern const int                                XML_STATUS_CODE_RINGING;
extern const int                                XML_STATUS_CODE_ALI_NO_GEOLOCATION_URI;
extern const int                                XML_STATUS_CODE_ALI_MANUAL_BID;
extern const int                                XML_STATUS_CODE_MAN_ALI_TIMEOUT;
extern const int                                XML_STATUS_CODE_AUTO_ALI_REBID;
extern const int                                XML_STATUS_CODE_AUTO_ALI_TIMEOUT;
extern const int                                XML_STATUS_CODE_ALI_REBID;
extern const int                                XML_STATUS_CODE_ALI_TIMEOUT;
extern const int                                XML_STATUS_CODE_ALI_RECEIVED;
extern const int                                XML_STATUS_CODE_MAN_ALI_RECEIVED;
extern const int                                XML_STATUS_CODE_AUTO_ALI_RECEIVED;
extern const int                                XML_STATUS_CODE_ALI_DONT_UPDATE_STATUS;
extern const int                                XML_STATUS_CODE_SEND_TO_CAD; 
extern const int                                XML_STATUS_CODE_CONFERENCEJOIN;
extern const int                                XML_STATUS_CODE_CONFERENCELEAVE;
extern const int                                XML_STATUS_CODE_TDD_CALLER_SAYS;
extern const int                                XML_STATUS_CODE_TDD_CHAR_RECEIVED;
extern const int                                XML_STATUS_CODE_TDD_CHANGE_MUTE;
extern const int                                XML_STATUS_CODE_ALI_BID_NOT_ALLOWED;

extern string                                   strCTI_USERNAME;
extern string                                   strCTI_PASSWORD;
extern unsigned int                             intDEFAULT_ALI_NOISE_THRESHOLD;
extern bool                                     boolAUDIOCODES_911_ANI_REVERSED;
extern bool                                     boolUSE_CALL_RECORDER;
extern string                                   strCALLER_ID_NAME;
extern string                                   strCALLER_ID_NUMBER;
extern string                                   strCONFIG_FILE_VERSION;
extern string                                   strAGI_VERSION_NUMBER;
extern string                                   strGATEWAY_TRANSFER_ORDER;
extern int                                      intSIP_PHONE_MIN_JITTER;
extern int                                      intSIP_PHONE_MAX_JITTER;
extern int                                      intSIP_PHONE_JITTER_DEPTH;
extern bool                                     boolSIP_PHONE_ECHO_CANCELLATION;
extern bool                                     boolSIP_PHONE_SILENCE_DETECTION;
extern bool                                     boolSIP_PHONE_AUTO_GAIN_CONTROL;
extern unsigned int                             intSIP_PHONE_UDP_PORT;
//extern int                                     intTRANSFER_TRUNK_TYPE_LIST_CODE;
//extern int                                     intSPEED_DIAL_TRUNK_TYPE_LIST_CODE;
extern vector <string>                          vGATEWAY_TRANSFER_ORDER;
extern int                                      intALI_WIRELESS_AUTO_REBID_SEC;
extern int                                      intALI_WIRELESS_AUTO_REBID_COUNT;
extern int                                      intTDD_CPS_THRESHOLD_SEC;
extern unsigned int                             intDISPLAY_SPACES;
extern char                                     charCADHEARTBEATMSG                     [5];
extern char                                     charALIHEARTBEATMSG                     [3];
extern const float                              floatNANOSEC_PER_SEC;
extern const long int                           intNANOSEC_PER_SEC; 
extern const long double                        longdoubleNANOSEC_PER_SEC;
extern string                                   stringSERVER_HOSTNAME;
//extern string                                   strASTERISK_POSITION_PREFIX;
extern bool                                     boolUSE_DASHBOARD;
extern bool                                     BOOL_SEND_ECATS;
extern int                                      ECATS_PORT_NUMBER;
extern Port_Connection_Type                     ECATS_PORT_CONNECTION_TYPE;
extern unsigned int                             ECATS_MSG_BUFFER_SIZE;
extern int                                      ECATS_ALARM_DELAY_SEC;
extern int                                      ECATS_ALARM_REMINDER_SEC;
extern bool                                     boolJSON_LOGGING;
extern volatile bool                            boolDISPLAY_CONSOLE_OUTPUT;
extern volatile bool                            boolLOG_DISPLAY_KRN_HEARTBEAT_TRAFFIC;
extern volatile bool                            boolLOG_DISPLAY_ALI_HEARTBEAT_TRAFFIC;
extern volatile bool                            boolLOG_DISPLAY_ANI_HEARTBEAT_TRAFFIC;
extern volatile bool                            boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC;
extern volatile bool                            boolLOG_DISPLAY_WRK_HEARTBEAT_TRAFFIC;
extern volatile bool                            boolLOG_DISPLAY_RCC_HEARTBEAT_TRAFFIC;
extern volatile bool                            boolSHOW_CLI_REGISTRATION_PACKETS;

extern volatile bool                            boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_KRN_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_ALI_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_ANI_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_LIS_RAW_TRAFFIC;
extern volatile bool                            boolDISPLAY_AND_LOG_DSB_RAW_TRAFFIC;
extern volatile bool                            boolEmailSupress[1000];
extern volatile bool                            boolDEBUG;
extern volatile int                             intBecomeMaster;
extern bool                                     boolEmailON;
extern string                                   strEMAIL_RELAY_SERVER;
extern string                                   strEMAIL_RELAY_USER;
extern string                                   strEMAIL_RELAY_PASSWORD;                            
extern bool                                     boolEMAIL_MONTHLY_LOGS;
extern bool                                     boolSEND_TO_CAD_ON_TAKE_CONTROL;
extern bool                                     bool911_TRANSFER_TANDEM_ONLY;
extern bool                                     boolADD_NINE_FOR_OUTBOUND_CALLS;
extern string                                   stringPRODUCT_NAME;
extern string                                   stringCOMPANY_NAME;
extern string                                   strESME_ID_STRING;
extern string                                   stringPSAP_Name;
extern string                                   stringVERSION_NUMBER;
extern string                                   strCUSTOMER_SUPPORT_NUMBER;
extern string                                   strABOUT_SPLASH_FILE;
extern string                                   strABOUT_SPLASH_COLOR;
extern string                                   strABOUT_SPLASH_FONT;
extern string                                   strABOUT_SPLASH_FONT_PT;
extern string                                   stringPSAP_ID;
extern string                                   stringDEFAULT_GATEWAY_DEVICE;
extern string                                   strIPWORKS_RUN_TIME_KEY;
extern int                                      intNUM_TRUNKS_INSTALLED;
extern int                                      intSERVER_NUMBER;
extern unsigned int                             intAVAIL_DISK_SPACE_WARN_GIG;
extern int                                      intANI_PORT_DOWN_REMINDER_SEC;
extern int                                      intANI_PORT_DOWN_THRESHOLD_SEC;
extern int                                      intWRK_PORT_DOWN_THRESHOLD_SEC;
extern int                                      intWRK_PORT_DOWN_REMINDER_SEC;
extern int                                      intSYC_PORT_DOWN_THRESHOLD_SEC;
extern int                                      intSYC_PORT_DOWN_REMINDER_SEC;
extern int                                      intDEVICE_DOWN_THRESHOLD_SEC;
extern int                                      intDEVICE_DOWN_REMINDER_SEC;
extern int                                      intNUM_AUDIOCODES_DEVICES;
extern int                                      intTCP_RECONNECT_RECURSION_LEVEL;
extern int                                      intTCP_RECONNECT_INTERVAL_SEC;
extern string                                   stringNTP_SERVICE_START_SCRIPT;
extern string                                   stringNTP_SERVICE_STOP_SCRIPT;
extern string                                   stringEMAIL_SEND_TO_ALARM;
extern string                                   stringEMAIL_SEND_TO;
extern string                                   stringEMAIL_FROM;
extern string                                   stringEMAIL_CC_TO;
extern char*                                    charEMAIL_SEND_TO_ALARM;
extern char*                                    charEMAIL_SEND_TO;
extern char*                                    charEMAIL_FROM;
extern char*                                    charEMAIL_CC_TO;
extern char*                                    charEMAIL_LOCAL_HOST;

extern unsigned long int                        intMAX_CONFERENCE_NUMBER;
extern unsigned long int                        intMIN_CONFERENCE_NUMBER;
extern string                                   strCONFERENCE_NUMBER_DIAL_PREFIX;  
extern string                                   strNG911_PHONEBOOK_SIP_TRANSFER_HEADER;
extern bool                                     boolNG_ALI;
extern volatile bool                            boolMail_Transfer_Complete;
extern volatile bool                            boolUPDATE_VARS;
extern volatile bool                            boolSTOP_EXECUTION;
extern volatile bool                            boolSTOP_LOG_THREAD; 
extern volatile bool                            boolEMAIL_QUEUE_ACTIVE;
extern sem_t                                    sem_tMutexMainMessageQ;
extern sem_t                                    sem_tMutexLogMessageQ;
extern sem_t                                    sem_tMutexMainWorkTable;
extern sem_t                                    sem_tMutexOutputMessageQ;
extern sem_t                                    sem_tLogConsoleEmailFlag;
extern sem_t                                    sem_tMutexCLIMessageQ;
extern sem_t                                    sem_tMutexCTIMessageQ;
extern sem_t                                    sem_tMutexLISMessageQ;
extern sem_t                                    sem_tMutexDSBMessageQ;
extern sem_t                                    sem_tMutexCall_LIST;
extern sem_t                                    sem_tMutexMainInputQ;
extern sem_t                                    sem_tMutexDebugLogName;
extern sem_t                                    sem_tMutexEmailSupress;
extern sem_t                                    sem_tMutexThreadData;
extern int                                      intLastErrorCode;
extern string                                   stringLastErrorMessage;
extern I3_convert                               enumI3CONVERSION_TYPE;
extern int                                      intTCIP_TIMEOUT_SEC;
extern string                                   stringCurrentJSONlogFileName;
extern string                                   stringCurrentLogFileName;
extern string                                   strCurrentLogFileMonth;
extern string                                   stringCurrentXMLlogFileName;
extern string                                   stringCurrentDebugFileName;
extern string                                   stringCurrentAMIlogFileName;
extern ofstream					JSONlogfile;
extern ofstream                                 ReadableLogFile;
extern ofstream                                 XMLlogfile;
extern ofstream                                 DebugLogFile;
extern cpu_set_t                                CPU_AFFINITY_ALL_OTHER_THREADS_SET;
extern cpu_set_t                                CPU_AFFINITY_SYNC_THREAD_SET;

extern unsigned long long int                   intSEMAPHORE_ERRORS;
extern unsigned long long int                   intLogFileWriteError;
extern unsigned long long int                   intLogFileOpenError;
extern unsigned long long int                   intSignalSemaphoreFailureCounter;
extern volatile unsigned long long int          intDiscardedPackets;
extern unsigned long long int                   intSyncGatewayPingCounter;
extern volatile int                             intDayofMonth;
extern bool                                     boolSTRATUS_SERVER_MONITOR;

// ANI
extern int                                      intANI_DIGIT_FORMAT;
extern sem_t                                    sem_tMutexANIWorkTable;
extern bool					boolANI_20_DIGIT_VALID_CALLBACK;
extern string                                   strANI_14_DIGIT_REQ_TEST_KEY;
extern string 					strANI_16_DIGIT_REQ_TEST_KEY;
extern ani_system                               enumANI_SYSTEM;
extern unsigned int                             intANI_ROTATE_CHECK_MULTIPLIER;
extern string                                   strASTERISK_NON_GUI_DIALOUT_XFER_CHANNEL1_KEY;
extern string                                   strASTERISK_AMI_POSITION_HEADER_911;
extern string                                   strASTERISK_AMI_POSITION_HEADER_FIRE;
extern sem_t                                    sem_tMutexConferenceNumberAssignmentObject;
extern int                                      intCLID_DIAL_PREFIX_CODE;
extern int                                      int911_DIAL_PREFIX_CODE;
extern int                                      intSIP_DIAL_PREFIX_CODE;
extern string                                   strDIAL_PREFIX_STAR;
extern string                                   strDIAL_PREFIX_NINE;
extern string                                   strDIAL_PREFIX_NONE;
extern bool                                     boolTDD_AUTO_DETECT;
extern int                                      intDELAY_AFTER_FLASH_HOOK_MS;
extern int                                      intDELAY_BETWEEN_DTMF_MS;
extern int                                      intDelay_AFTER_STAR_EIGHT_PD_MS;
extern int                                      intDelay_BETWEEN_DTMF_INDIGITAL_MS;

// CAD Related Globals and constants

extern int                                      intNUM_CAD_PORTS;
extern int                                      intCAD_PORT_DOWN_REMINDER_SEC;
extern int                                      intCAD_PORT_DOWN_THRESHOLD_SEC;
extern volatile int                             intCadActivePosition;
extern volatile bool				boolSTOP_CAD_THREAD;
extern bool                                     boolCAD_EXISTS;
extern bool                                     boolCAD_ERASE;				
extern sem_t					sem_tMutexCadWorkTable;
extern sem_t					sem_tMutexCadInputQ;
extern sem_t					sem_tMutexCadMessageQ;
extern sem_t					sem_tMutexCadPortDataQ;
extern sem_t					sem_tCadFlag;

// LIS Related Globals
extern bool                                     boolSHOW_RAPID_LITE_BUTTON;
extern string                                   strNG911_ECRF_SERVER;
extern string                                   strNG911_SOS_SERVICE_BID;
extern int                                      intLIS_HTTP_TIMEOUT_SEC;
extern int                                      intECRF_HTTP_TIMEOUT_SEC;

//Dashboard
extern sem_t					sem_tDSBflag;

// ALI Related Globals
extern bool                                     boolUSE_ALI_SERVICE_SERVER;
extern bool                                     boolLEGACY_ALI;
extern bool                                     boolLEGACY_ALI_ONLY;
extern int                                      intNUM_ALI_PORT_PAIRS;
extern int                                      intNUM_ALI_SERVICE_PORTS;
extern sem_t					sem_tMutexAliWorkTable;
extern sem_t					sem_tMutexAliInputQ;
extern sem_t					sem_tMutexAliMessageQ;
extern sem_t					sem_tMutexAliPortDataQ;
extern sem_t					sem_tMutexAliDisconnectList;
extern sem_t					sem_tAliFlag;
extern int                                      intALI_PORT_DOWN_REMINDER_SEC;
extern int                                      intALI_PORT_DOWN_THRESHOLD_SEC;
extern int                                      intALI_REQUEST_LENGTH;
extern int                                      intNUM_ALI_DATABASES;
extern string                                   stringALI_LOG_SPACES;
extern bool                                     boolALLOW_MANUAL_ALI_BIDS;
extern int					intCLID_ALI_BID_RULE_CODE;
extern bool					boolALLOW_CLID_ALI_BIDS;
extern bool                                     boolIGNORE_BAD_ALI;
extern bool					boolCLID_BID_TEST_KEY;
extern unsigned long int                        intE2TransactionID;
extern sem_t					sem_tMutexE2TransactionID;
extern bool                                     boolSHOW_GOOGLE_MAP;
extern bool                                     boolVERIFY_ALI_RECORDS;
extern bool                                     boolREBID_PHASE_ONE_ALI_RECORDS_ONLY;
extern string                                   strALI_NOT_ALLOWED_MESSAGE;
extern string                                   strALI_FAILED_MESSAGE;
extern string                                   strALI_NO_GEOLOCATION_URI;
extern unsigned int                             intMAX_ALI_RECORD_SIZE;
extern bool                                     boolSEND_LEGACY_ALI_ON_SIP_TRANSFER;
extern unsigned int				intMONTHS_TO_KEEP_LOG_FILES;

extern bool					boolMULTICAST;
extern bool                                     BID_ALI_SIP_TRUNKS;
extern struct itimerspec			itimerspecDisableTimer;
extern struct timespec			        timespecNanoDelay;
extern struct timespec                          timespecRemainingTime;

// ANI Related Globals
//extern sem_t				        sem_tMutexANIWorkTable;
extern sem_t					sem_tMutexANIMessageQ;
extern sem_t					sem_tMutexANIPortDataQ;
extern sem_t                                    sem_tMutexANIPort;
extern sem_t                                    sem_tMutexANIInputQ;
extern sem_t					sem_tANIFlag;
extern int                                      intNUM_ANI_PORTS;
extern int                                      intANI_PORT_DOWN_REMINDER_SEC;
extern int                                      intANI_PORT_DOWN_THRESHOLD_SEC;


extern volatile bool				boolLastFileOpenOperationFailed;
extern volatile bool				boolLastWriteOperationFailed;




// SYC Related Globals
//extern sem_t                                    sem_tServerHeartbeatRecievedFlag;
//extern sem_t                                    sem_tMutex_ServerHeartbeatDataQ;
extern sem_t                                    sem_tMutex_ServerStatusTable;
//extern sem_t                                    sem_tMutexSYCMessageQ;
//extern sem_t                                    sem_tGatewayTimedResponseFlag;
//extern sem_t                                    sem_tMutex_GatewayInterrogateReplyQ;
//extern sem_t                                    sem_tGatewayInterrogateReplyFlag;
//extern sem_t                                    sem_tMutex_GatewayReplyQ;
//extern sem_t                                    sem_tGatewayReplyFlag;
extern sem_t                                    sem_tMutexConfAssignmentQ;
extern sem_t                                    sem_tConfAssignmentFlag;



// AMI Related Globals
extern sem_t                                    sem_tAMIFlag;
extern sem_t                                    sem_tMutexAMIInputQ;
extern string                                   strASTERISK_AMI_IP_ADDRESS;
extern bool                                     bool_FREESWITCH_VERSION_212_PLUS;

extern volatile bool                            boolIsMaster;                          
extern volatile bool                            boolNTP_Service_Running;
extern int                                      intTRUNK_ASSIGNMENT_PORT_NUMBER;               
extern int                                      intMASTER_SLAVE_SYNC_PORT_NUMBER;     
extern int                                      intMASTER_SLAVE_GATEWAY_INTERROGATE_PORT_NUMBER;
extern int                                      intMASTER_SLAVE_GATEWAY_REPLY_PORT_NUMBER;  
extern int                                      intNUM_OF_SERVERS;                     
extern bool					boolSINGLE_SERVER_MODE;
extern bool                                     boolAMI_RECORD_SCRIPT;
extern bool                                     boolAMI_TEST_SCRIPT;


//CTI
extern bool                                     bool_POLYCOM_404_FIRMWARE;

extern int                                      intPOLYCOM_CTI_END_CALL_SOFTKEY;
extern sem_t                                    sem_tCTIflag;
extern sem_t                                    sem_tMutexCTIInputQ;
extern bool                                     boolUSE_POLYCOM_CTI;
extern int                                      intPOLYCOM_CTI_HTTP_PORT;
extern int                                      intPOLYCOM_CTI_BARGE_DELAY_MS;

// WRK Related Globals
extern sem_t					sem_tMutexWRKWorkTable;
extern sem_t					sem_tMutexWRKMessageQ;
extern sem_t					sem_tMutexWRKPortDataQ;
extern sem_t                                    sem_tMutexWRKInputQ;
extern sem_t					sem_tWRKFlag;
extern int                                      intNUM_WRK_STATIONS;
extern int                                      intWRK_LOCAL_LISTEN_PORT;
extern int                                      intWRK_HEARTBEAT_INTERVAL_SEC;
extern string                                   stringTDD_DEFAULT_LANGUAGE;
extern int                                      intTDD_SEND_DELAY;
extern string                                   stringTDD_SEND_TEXT_COLOR;
extern string                                   stringTDD_RECEIVED_TEXT_COLOR;

extern int                                      intTDD_CPS_THRESHOLD;
extern string                                   strMAPPING_REGEX;
extern string                                   stringMSRP_DEFAULT_LANGUAGE;
extern string                                   stringMSRP_SEND_TEXT_COLOR;
extern string                                   stringMSRP_RECEIVED_TEXT_COLOR;
extern string                                   stringTRANSFER_DEFAULT_GROUP;
extern bool                                     boolGUI_SIP_PHONE_EXISTS;
extern Port_Connection_Type                     enumWORKSTATION_CONNECTION_TYPE;
//extern bool                                     boolSHOW_RING_DIALOG;
extern int                                      intSHOW_RING_DIALOG;      
extern string                                   strRINGTONE911;
extern string                                   strRINGTONETEXT;
extern string                                   strRINGTONEADMIN;
extern string                                   strRINGIMAGE911;
extern string                                   strRINGIMAGETEXT;
extern string                                   strRINGIMAGEADMIN;
extern bool                                     boolCLEAR_DIAL_PAD_AFTER_USE;

// RCC Globals
extern sem_t                                    sem_tMutexRCCtable;
extern sem_t                                    sem_tMutexRCCInputQ;
extern sem_t                                    sem_tMutexRCCMessageQ;
extern sem_t                                    sem_tRCCflag;
extern unsigned int                             intRCC_REMOTE_TCP_PORT_NUMBER;
extern string                                   strRCC_DEVICE_ALL_OPEN;
extern string                                   strRCC_DEVICE_OPEN_ONE;
extern string                                   strRCC_DEVICE_OPEN_TWO;
extern string                                   strRCC_DEVICE_OPEN_THREE;
extern string                                   strRCC_DEVICE_ALL_CLOSE;
extern string                                   strRCC_DEVICE_CLOSE_ONE;
extern string                                   strRCC_DEVICE_CLOSE_TWO;
extern string                                   strRCC_DEVICE_CLOSE_THREE;
extern string                                   strRCC_DEVICE_GET_VERSION;
extern bool                                     boolCONTACT_RELAY_INSTALLED;
extern bool                                     boolRCC_DEFAULT_CONTACT_POSITIONS;
extern itimerspec                               itimerspecRCCSendDelay;
extern timer_t                                  timer_tRCC_Send_Event_TimerId;
                      
//test
extern long double				SyncTableTimeToBroadcast[86400];
extern int                                      SyncTableNumberofEntries;
extern int                                      SyncTableIndex;
extern sem_t					sem_tMutexSyncBroadcastData;
extern list <struct timespec>			Timestamp;
extern int                                      SyncTimeStampEntries;
extern sem_t					sem_tMutexSyncTimeStampData;
extern string 					strTEST_TDD_CONVERSATION;







#endif
