/*****************************************************************************
* FILE: MessageClass.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the MessageClass 
*
*
*
* AUTHOR: 02/25/2007 Bob McCarthy
* LAST REVISED:  
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"
#include "/datadisk1/src/xmlParser/xmlParser.h"

extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data				objBLANK_CALL_RECORD;
extern Port_Data                                objBLANK_LOG_PORT;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
 

void MessageClass::fScript_Create(int intArg1, int intArg2, string strData)
{
 intLogCode		= intArg1;
 intMessageCode		= intArg2;
 stringOutputMessage    = strData;
 
}

/****************************************************************************************************
*
* Name:
*  Function: void MessageClass::fMessage_Create()
*
*
* Description:
*  This is a <MessageClass> function implementation 
*
* 
* Details:
*  The function loads object data from the parameters and places a timestamp of
*  when it was created into the class. it also creates an output string          
*    
* Parameters:
*   intArg1                                     - int (priority of message)				
*   intArg2				        - int (code of message)
*   intArg3                                     - int (port number if required)  
*   stringArg1					- string (thread calling)
*   stringArg2                                  - string (message)
*
* Variables:
*   CLOCK_MONOTONIC			Library	- <ctime> constant
*   intMessageCode                      Local   - <MessageClass> object variable
*   intLogCode                          Local   - <MessageClass> object variable
*   stringMessageText                   Local   - <MessageClass> object variable
*   stringOutputMessage                 Local   - <MessageClass> object variable
*   stringSERVER_HOSTNAME               Global  - <globals.h>
*   stringThreadCalling                 Local   - <MessageClass> object variable
*   stringTimeStamp                     Local   - <MessageClass> object variable
*   strUniqueCallId                     Local   - <cstring>
*   mtimespecTimeStamp                   Local   - <MessageClass> member
*   rtimespecTimeStamp                   Local   - <MessageClass> member
*                                                                       
* Functions:
*  clock_gettime()                      Library - <ctime>
*  FindandReplace()                     Global  - <globalfunctions.cpp>
*  get_time_stamp()                     Global  - <globalfunctions.cpp>
*  Thread_Calling()                     Global  - <globalfunctions.cpp>
*
* Author(s):
*   Bob McCarthy 			Original: 2/12/2008
*                                       Updated : N/A
*
****************************************************************************************************/
void MessageClass::fMessage_Create(int intArg1, int intArg2, int intLine, string strFile, string strFunction, Call_Data objDataArg, Text_Data objTextData,
                                   Port_Data objPortArg, Freeswitch_Data objFreeswitchData, ALI_Data objALIdata,
                                   string stringArg1, string stringArg2, string stringArg3, string stringArg4, 
                                   string stringArg5, string stringArg6, string stringArg7,
                                   message_type enMessageType, message_display_type enMessageDisplayType)
{ 
 string         stringMessageCode;
 string         strUniqueCallId;
 string         stringPrefix;
 string         stringCallData;
 string         stringPortData;
 string         strXMLPortData;
 string         stringMessageBase;
 string         stringArrowField;
 size_t         SearchPosition;
 string         strTrunk;
 string         strPosn;
 string         strTenDigitCallbackNumber;
 string         strPANI;
 string         strTempPortInfo = "";
 string         strXMLmessageText;
 string         strJSONmessageText;
 string         strXMLaliText;
 string         strXMLextraData;
 string         strXMLtddRecord;
 string         strTemp;
 string         strJSONoutput;
#ifdef IPWORKS_V16
 JSON           json;
 string         strJSON_KEY = IPWORKS_RUNTIME_KEY;
 ExperientDataClass objExpData;
#endif
 pid_t          tid;
 Thread_Data    objThreadData;

 extern string				stringCurrentJSONlogFileName;
 extern Trunk_Type_Mapping		TRUNK_TYPE_MAP;
 extern ALISteering                    	ALI_Steering[intMAX_ALI_DATABASES + 1 ];

 extern string TrunkType(int i);
 extern string EncodeBase64(string strInput);
 extern void   AddPortInfotoJSONobject(JSON *JSONobject, Port_Data objPortArg);
 extern void   AddALiInfotoJSONobject(JSON *JSONobject, ALI_Data objALIArg);
 extern void   AddNgALiInfotoJSONobject(JSON *JSONobject, ALI_Data objALIArg);
 extern void   AddCADInfotoJSONobject(JSON *JSONobject, Call_Data objCALLdata, ALI_Data objALIdata, Port_Data objPortArg);
 extern void   AddANiInfotoJSONobject(JSON *JSONobject, Call_Data objCALLdata);
 extern void   AddCallEventtoJSONobject(JSON *JSONobject, Call_Data objCALLdata, Text_Data objTextData);
 extern void   AddAliEventtoJSONobject(JSON *JSONobject, ALI_Data objALIdata);
 extern void   AddNgAliEventtoJSONobject(JSON *JSONobject, ALI_Data objALIdata);
 extern int    AddWRKEventtoJSONobject(JSON *JSONobject, Call_Data objCalldata, Text_Data objTextData);
 extern void   AddTimeStamptoJSONobject(JSON *JSONobject, struct timespec RealtimeStamp, struct timespec MonotonictimeStamp);
 fClearMsg();

 strTrunk.assign(3,' ');  
 strPosn.assign(3,' ');   
 strTenDigitCallbackNumber.assign(10,' ');                                              
 strPANI.assign(11,' ');


 clock_gettime(CLOCK_MONOTONIC, &mtimespecTimeStamp);
 clock_gettime(CLOCK_REALTIME, &rtimespecTimeStamp);
 strControllerUUID      = strCONTROLLER_UUID;
 strMessageUUID         = UUID_Generate_String();
 enumMessageType        = enMessageType;
 objCallData            = objDataArg;
 objPortData            = objPortArg;
 stringTimeStamp 	= get_time_stamp(rtimespecTimeStamp);
 intLogCode		= intArg1;
// if ((intLogCode == LOG_ALARM)&&(intMessageCode == 519)){//cout << "HERE IT IS" << endl;}       tbc
 intMessageCode		= intArg2;
 stringMessageCode      = int2str(intMessageCode);
 stringThreadCalling 	= Thread_Calling(objPortData.enumPortType).erase(3);

 if (stringArg1.length() > 1)
  {
   stringMessageBase.assign(stringArg1, 1, string::npos);
   stringArrowField.assign (stringArg1, 0, 1);
  }

 if (objCallData.intUniqueCallID)
  {
   strUniqueCallId     = int2str(objCallData.intUniqueCallID);
  }
 else
  {
   strUniqueCallId.assign(18,' ');
  }
 
 if (objCallData.intTrunk)                                    			    {strTrunk                         = "T" + objCallData.stringTrunk;}
 if ((objCallData.intUniqueCallID)||(objCallData.intPosn)||(intMessageCode == 307)) {strPosn                          = "P" + int2strLZ(objCallData.intPosn);}
 if (!objCallData.strSIPphoneNumber.empty())                                        {strTenDigitCallbackNumber        =        VOIP_CALL_STRING;}
 else if ((objCallData.intCallbackNumber))                                          {strTenDigitCallbackNumber        =       objCallData.stringTenDigitPhoneNumber;}
 if (!objCallData.stringPANI.empty())                     			    {strPANI                          = "p" + objCallData.stringPANI;}

 stringPortData = stringThreadCalling + " ";
 if (objPortData.intPortNum)  {stringPortData += int2strLZ(objPortData.intPortNum);}
 else                         {stringPortData += "  ";}                                         
 if (objPortData.intSideAorB) {stringPortData += objPortData.strSideAorB;}
 else                         {stringPortData += " ";}

 stringPortData   += " | ";
 stringArrowField += " | ";
 

 stringPrefix  = stringSERVER_HOSTNAME +  " | " + stringTimeStamp + " " +  stringMessageCode + " | ";

 stringCallData =  strUniqueCallId + " | "   + strTrunk + " | " + strPosn + " | " + strTenDigitCallbackNumber + " | " + strPANI + " | ";

 switch (enMessageDisplayType)
  {
   case NORMAL_MESSAGE:
        stringMessageText  = Create_Message(stringMessageBase, stringArg2, stringArg3, stringArg4, stringArg5, stringArg6, stringArg7);
        strXMLmessageText  = stringMessageText;
#ifdef IPWORKS_V16
        strJSONmessageText = stringMessageText;
#endif
        break;
   case ALI_RECORD:
        stringMessageText = stringMessageBase;
        stringOutputMessage = stringPrefix + stringCallData + stringPortData + stringArrowField + stringMessageText;
        stringOutputMessage += charCR;stringOutputMessage += charLF;
        stringOutputMessage += ReformatALIOutputToLog(stringArg2, stringCallData + stringPortData + stringArrowField, stringPrefix, objALIdata.bool_LF_AS_CR);
        strXMLmessageText = stringMessageBase;
#ifdef IPWORKS_V16
        strJSONmessageText = stringMessageBase;
#endif
        strXMLaliText = ReformatALIOutputToLog(stringArg2, "", "", objALIdata.bool_LF_AS_CR );
        break;
   case NEXT_LINE:
        stringMessageText = Create_Message(stringMessageBase, stringArg2);
        stringOutputMessage = stringPrefix + stringCallData + stringPortData + stringArrowField + stringMessageText;
        stringOutputMessage += charCR;stringOutputMessage += charLF;
        stringOutputMessage += ReformatLongOutputToLog(stringArg3,stringCallData + stringPortData + stringArrowField, stringPrefix, true);
        strXMLmessageText = stringMessageText;
#ifdef IPWORKS_V16
        strJSONmessageText = stringMessageText;
#endif
        strXMLextraData = stringArg3;    
        break;
   case TDD_CONVERSATION:
        stringMessageText = stringMessageBase;
        stringOutputMessage = stringPrefix + stringCallData + stringPortData + stringArrowField + stringMessageText;
        stringOutputMessage += ReformatTDDConversationToLog(stringArg2,stringCallData + stringPortData + stringArrowField, stringPrefix);
        strXMLmessageText = stringMessageBase;
#ifdef IPWORKS_V16
        strJSONmessageText = stringMessageBase;
#endif
        strXMLtddRecord = ReformatTDDConversationToLog(stringArg2, "", "");
        break;
  }// end  switch (enMessageDisplayType)


 // set email message
 objPortData.fLoadThreadData(objPortData.enumPortType);
 stringSubject = stringMessageText.substr(0,intEMAIL_SUBJECT_MAX_LENGTH);
 SearchPosition = stringSubject.find("] ");
 if (SearchPosition != string::npos)  
  {
   if      (objPortData.intSideAorB) {stringSubject.insert(SearchPosition+2, objPortData.strThread +" "+ int2strLZ(objPortData.intPortNum) + objPortData.strSideAorB + ": " );} 
   else if (objPortData.intPortNum)  {stringSubject.insert(SearchPosition+2, objPortData.strThread +" "+ int2strLZ(objPortData.intPortNum) + ": " ) ;} 
   else                              {stringSubject.insert(SearchPosition+2,objPortData.strThread + ": " );} 
  }
 stringEmailMessageBody.append                                	("Time Stamp         = " + stringTimeStamp + '\n');
 stringEmailMessageBody.append                                	("Server             = " + stringSERVER_HOSTNAME + '\n');
 stringEmailMessageBody.append                                	("Thread             = " + stringThreadCalling.erase(3) + '\n');

 if(objPortData.intSideAorB)                             {stringEmailMessageBody.append("Port Data          = " + objPortData.strThread +" "+ 
                                                          int2strLZ(objPortData.intPortNum)  + " " + objPortData.strSideAorB  + '\n');}                           
 else if (objPortData.intPortNum)                        {stringEmailMessageBody.append("Port Data          = " + objPortData.strThread +" "+ 
                                                          int2strLZ(objPortData.intPortNum)  + '\n');}                              
 
 if(intMessageCode) 		                           {stringEmailMessageBody.append("Message Code       = " + int2strLZ(intMessageCode) + '\n');}
 if (objCallData.intUniqueCallID) 			   {stringEmailMessageBody.append("Call Id            = " + strUniqueCallId + '\n');}
 if (objCallData.intTrunk)        			   {stringEmailMessageBody.append("Trunk              = " + objCallData.stringTrunk + '\n');}
 if ((objCallData.intUniqueCallID)||(objCallData.intPosn)) {stringEmailMessageBody.append("Position           = " + objCallData.stringPosn + '\n');}
 if (objCallData.intCallbackNumber)			   {stringEmailMessageBody.append("Call Back Number   = " + objCallData.stringTenDigitPhoneNumber + '\n');}
 if (!objCallData.stringPANI.empty())                      {stringEmailMessageBody.append("Psuedo ANI         = " + objCallData.stringPANI + '\n');}

 if((intMessageCode == 232)||(intMessageCode == 240))
                          {stringEmailMessageBody.append("Message Text       = " + reformat_Ali_Log_to_Email(stringOutputMessage,stringPrefix.length() ) + '\n');}
 else                     {stringEmailMessageBody.append("Message Text       = " + stringMessageText + '\n');}
 
 if (strXMLextraData.length()) {stringEmailMessageBody.append(strXMLextraData.substr(0,intEMAIL_MESSAGE_MAX_LENGTH-stringEmailMessageBody.length()));}

// set Log XML
fAddLogXMLattribute(XML_FIELD_SERVER_NAME, stringSERVER_HOSTNAME); 

if (stringTimeStamp.length() > 58)
 {
  fAddLogXMLattribute(XML_FIELD_UTC_DATE, stringTimeStamp.substr(0,10));
  fAddLogXMLattribute(XML_FIELD_UTC_TIME, stringTimeStamp.substr(11,12));
  fAddLogXMLattribute(XML_FIELD_LOCAL_DATE, stringTimeStamp.substr(34,10));
  strTemp = stringTimeStamp.substr(45,8)+stringTimeStamp.substr(19,4);
  fAddLogXMLattribute(XML_FIELD_LOCAL_TIME, strTemp);
  fAddLogXMLattribute(XML_FIELD_TIME_ZONE, stringTimeStamp.substr(54,3));
  fAddLogXMLattribute(XML_FIELD_DAY_OF_WEEK, stringTimeStamp.substr(30,3));
 }

fAddLogXMLattribute(XML_FIELD_MESSAGE_CODE, stringMessageCode);
switch (intLogCode)
 {
  case LOG_ALARM:
       fAddLogXMLattribute(XML_FIELD_MESSAGE_TYPE, "ALARM"); break;
  case LOG_WARNING:
       fAddLogXMLattribute(XML_FIELD_MESSAGE_TYPE, "WARNING"); break;
  case LOG_INFO:
       fAddLogXMLattribute(XML_FIELD_MESSAGE_TYPE, "INFO"); break;
  default:                            
       fAddLogXMLattribute(XML_FIELD_MESSAGE_TYPE, "NORMAL");
 } 

if  (objCallData.intUniqueCallID) {fAddLogXMLattribute(XML_FIELD_U_CALL_ID, strUniqueCallId);}
else                              {fAddLogXMLattribute(XML_FIELD_U_CALL_ID, "");} 
if  (objCallData.intTrunk)        {fAddLogXMLattribute(XML_FIELD_TRUNK, int2str(objCallData.intTrunk));} 
else                              {fAddLogXMLattribute(XML_FIELD_TRUNK, "");}
if ((objCallData.intUniqueCallID)||(objCallData.intPosn)||(intMessageCode == 307)) {fAddLogXMLattribute(XML_FIELD_POSITION, int2str(objCallData.intPosn));}  
else                                                                               {fAddLogXMLattribute(XML_FIELD_POSITION, "");}

fAddLogXMLattribute(XML_FIELD_THREAD_OR_PORT, stringThreadCalling);
strXMLPortData.clear();
if (objPortData.intPortNum)  {strXMLPortData += int2str(objPortData.intPortNum);}                                   
if (objPortData.intSideAorB) {strXMLPortData += objPortData.strSideAorB;}
fAddLogXMLattribute(XML_FIELD_PORT_NUMBER, strXMLPortData);


// XMLCSTR is const char *
//XMLCSTR xmlMessage = strXMLmessageText.c_str();
//ToXMLStringTool convertmessage;
 //      //cout << convertmessage.toXML(strXMLmessageText.c_str()) << endl; 

if(objCallData.intCallbackNumber)   {fAddLogXMLattribute(XML_FIELD_CALLBACK_NUMBER, int2str(objCallData.intCallbackNumber));}
else                                {fAddLogXMLattribute(XML_FIELD_CALLBACK_NUMBER, "");} 
if(!objCallData.stringPANI.empty()) {fAddLogXMLattribute(XML_FIELD_PSUEDO_ANI, objCallData.stringPANI);}
else                                {fAddLogXMLattribute(XML_FIELD_PSUEDO_ANI, "");}  
  
fAddLogXMLattribute(XML_FIELD_MESSAGE_TEXT, ConvertToXML(strXMLmessageText));       


switch (enMessageDisplayType)
 {
  case ALI_RECORD: 

       fAddLogXMLattribute(XML_FIELD_ALI_RECORD, ConvertToXML(strXMLaliText));
       break;
       
  case NEXT_LINE:

       fAddLogXMLattribute(XML_FIELD_EXTRA_DATA, ConvertToXML(strXMLextraData));
       break;

  case TDD_CONVERSATION:

       fAddLogXMLattribute(XML_FIELD_TDD_RECORD,ConvertToXML(strXMLtddRecord));
       break;

  default:
        if (stringMessageText.length() > intDISPLAY_SPACES) 
         {stringOutputMessage = ReformatLongOutputToLog(stringMessageText, stringCallData + stringPortData + stringArrowField, stringPrefix);}
        else 
         {stringOutputMessage = stringPrefix + stringCallData + stringPortData + stringArrowField + stringMessageText;}
        break;
 }

//json.StartArray();
//json.PutValue("Steve Widgetson", 2);
//json.PutValue("Wanda Widgetson", 2);
//json.PutValue("Randy Cooper", 2);
//json.PutValue("Linda Glover", 2);
//json.EndArray();



//Json code 
/*
    0 (Object)
    1 (Array)
    2 (String)
    3 (Number)
    4 (Bool)
    5 (Null)
    6 (Raw)
*/

#ifdef IPWORKS_V16
if ((!objCallData.intUniqueCallID)&&(objPortData.enumPortType != WRK)) {return;}

if (!boolJSON_LOGGING)           {return;}
if (intMessageCode == 0)         {cout << "message code zero ......................." << endl; return;}
if (intMessageCode == 262)       {cout << "message code 262 ......................." << endl; return;}
if (intMessageCode == 256)       {cout << "message code 256 ......................." << endl; return;}
if (intMessageCode == 255)       {cout << "message code 255 ......................." << endl; return;}

json.SetRuntimeLicense((char*)strJSON_KEY.c_str());

//json.SetOutputFile((char*) stringCurrentJSONlogFileName.c_str());
json.StartObject();
json.PutProperty("id", (char*) strMessageUUID.c_str(), 2);

if (objCallData.intUniqueCallID) {
 json.StartObject();
 json.PutProperty("calluid", (char*) int2str(objCallData.intUniqueCallID).c_str(), 2);
}

AddTimeStamptoJSONobject(&json, rtimespecTimeStamp, mtimespecTimeStamp);

//Debug object

json.StartObject();
json.PutName("d");
json.StartObject();
json.PutProperty("msg", (char*) strJSONmessageText.c_str(), 2);
json.StartObject();
json.PutProperty("code", (char*) int2str(intMessageCode).c_str(), 3);
json.StartObject();
json.PutProperty("s",   (char*) int2str(ConvertLOGcodetoSYSlogCode(intLogCode)).c_str(), 3);
json.StartObject();
json.PutName("src");
json.StartObject();
json.PutProperty("f",    (char*) strFile.c_str(), 2);
json.StartObject();
json.PutProperty("l",    (char*) int2str(intLine).c_str(), 3);
json.StartObject();
json.PutProperty("func", (char*) strFunction.c_str(), 2);
json.EndObject();
json.StartObject();
json.PutName("thread");
tid = syscall(SYS_gettid);
objThreadData = ThreadObjectWithTID(tid);
json.StartObject();
json.PutProperty("tid",  (char*) int2str(tid).c_str(), 3);
json.StartObject();
json.PutProperty("id",   (char*) objThreadData.strThread_UUID.c_str(), 2);
json.StartObject();
json.PutProperty("name", (char*) objThreadData.strThreadName.c_str(), 2);
json.EndObject();
json.StartObject();
json.PutName("proc");
json.StartObject();
json.PutProperty("pid", (char*) int2str(objThreadData.ThreadPID).c_str(), 3);
json.StartObject();
json.PutProperty("id",  (char*) strCONTROLLER_UUID.c_str(), 2);
json.StartObject();
json.PutProperty("name",(char*) stringPRODUCT_NAME.c_str(), 2);
json.StartObject();
json.PutProperty("ver",(char*) stringVERSION_NUMBER.c_str(), 2);
json.EndObject();
json.EndObject();

//thread object data.....

switch(objPortData.enumPortType)
{
 case MAIN:
      if (objDataArg.eJSONEventCode == UNCLASSIFIED) {json.Flush();return;} 
      if (objDataArg.eJSONEventCode == FORCE_DISCONNECT) {
       AddCallEventtoJSONobject(&json, objDataArg, objTextData);
       AddANiInfotoJSONobject(&json, objDataArg);
       json.EndObject(); //not calling port data
      }      
      break;
 case CAD:
      // CAD is inculded in ALI event .....
      if (objALIdata.eJSONEventCode == NO_ALI_FUNCTION_DEFINED) {json.Flush();return;}
      AddAliEventtoJSONobject(&json, objALIdata);
      AddCADInfotoJSONobject(&json, objDataArg, objALIdata, objPortArg);
      AddPortInfotoJSONobject(&json, objPortArg);
      break;
 case ALI:
      //note AddPortInfotoJSONobject() must be called after to close the object .....
      if (objALIdata.eJSONEventCode == NO_ALI_FUNCTION_DEFINED) {json.Flush();return;}
      if((objALIdata.stringType != "9")&&(objALIdata.eJSONEventCode == ALI_RECEIVED)) {
       objExpData.ALIData = objALIdata;
       objExpData.CallData = objCallData;
       objExpData.fLoadCallbackfromALI();
       objCallData = objExpData.CallData;
      }
      AddAliEventtoJSONobject(&json, objALIdata);
      AddALiInfotoJSONobject(&json, objALIdata);
      AddPortInfotoJSONobject(&json, objPortArg);
      break;
 case ANI:
      if (objDataArg.eJSONEventCode == UNCLASSIFIED) {json.Flush();return;}
      AddCallEventtoJSONobject(&json, objDataArg, objTextData);
      AddANiInfotoJSONobject(&json, objDataArg);
      AddPortInfotoJSONobject(&json, objPortArg);
      break;
 case WRK:
      if (objDataArg.eJSONEventWRKcode == BAD_KEY) {json.Flush();return;}
         if (AddWRKEventtoJSONobject(&json, objDataArg, objTextData) < 0) { json.EndObject(); break;}
 //      AddPortInfotoJSONobject(&json, objPortArg); // not yet 
       json.EndObject();
       this->strJSONoutput = json.GetOutputData(); 
       this->fLog(JSON_LOG); 
       json.Flush();
       return;

 case LOG:

      break;
 case AMI:

      break;
 case RCC:

      break;
 case CTI:

      break;
 case LIS:
       if (objALIdata.eJSONEventCode == NO_ALI_FUNCTION_DEFINED) {json.Flush();return;}     
       AddNgAliEventtoJSONobject(&json, objALIdata);
       AddNgALiInfotoJSONobject(&json, objALIdata);
 //    // AddPortInfotoJSONobject(&json, objPortArg); // not yet
      break;
 case ECATS:

      break;
 case NFY:

      break;
 default:
      stringThreadCalling 	= Thread_Calling(objPortData.enumPortType).erase(3);
      strTemp = "MessageClass.cpp - Thread " +  stringThreadCalling;
      strTemp += "not coded in JSON logs";   
      SendCodingError(strTemp);
      json.Flush();
      return;
}


//Call object
json.StartObject();
json.PutName("call");

json.StartObject();
json.PutProperty("id",  (char*) objCallData.stringUniqueCallID.c_str() , 2);

 json.StartObject();
if (TRUNK_TYPE_MAP.fIsTandem(objCallData.intTrunk)){
 json.PutProperty("is_911",  "true" , 4);
}
else{
 json.PutProperty("is_911",  "false" , 4);
}

json.StartObject();
json.PutProperty("trunk", (char*) int2str(objCallData.intTrunk).c_str(), 3);

json.StartObject();
json.PutProperty("trunk_type", (char*) TrunkType(objCallData.intTrunk).c_str(), 2);


json.StartObject();
json.PutProperty("call_back_display",   (char*) objCallData.strCallBackDisplay.c_str(), 2);
json.StartObject();
json.PutProperty("ani",   (char*) objCallData.stringTenDigitPhoneNumber.c_str(), 2);
json.StartObject();
json.PutProperty("pani",  (char*) objCallData.stringPANI.c_str(), 2);
json.StartObject();
json.PutProperty("sip",  (char*) objCallData.strSIPphoneNumber.c_str(), 2);
json.StartObject();
json.PutProperty("call_recording", (char*) objCallData.strCallRecording.c_str() , 2);
json.StartObject();
if (objCallData.boolIsOutbound) {
 json.PutProperty("is_outbound",  "true" , 4);
}
else {
 json.PutProperty("is_outbound",  "false" , 4);
}

json.EndObject();
json.EndObject();
json.EndObject();
json.EndObject();
//Call Object



json.EndObject();

this->strJSONoutput = json.GetOutputData();

this->fLog(JSON_LOG);

json.Flush();

#endif



 return;
}// end MessageClass::fMessage_Create


void MessageClass::fAddLogXMLattribute(string strAttr, string strValue)
{
 size_t position;
 string strAdd;
 string strEQ_BKSLASH_QUOTE = "=\"";
 string strQuote            = "\" ";
 string strBKSLASH_QUOTE    = "\" ";
 
 position = strLogXML.rfind("/>");
 if (position == string::npos) {return;}

 strAdd = strAttr + strEQ_BKSLASH_QUOTE;
 strAdd += strValue;
 strAdd += strBKSLASH_QUOTE;
 strLogXML.insert ( position, strAdd );
}

string MessageClass::fAbandoned_Call_PopUp_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;

 SearchPosition = strReturnMessage.find("[");
 if (SearchPosition == string::npos) {return "Error Coding Message";}

 strReturnMessage.erase(0, SearchPosition);
 return strReturnMessage;
}



string MessageClass::fTransfer_Cancelled_PopUp_Message_Warning()
{
// size_t SearchPosition;
 string strReturnMessage = stringMessageText;

 return strReturnMessage;
}

string MessageClass::fTransfer_Dialed_PopUp_Message()
{
 size_t start, end;
 string strData;

 start = stringMessageText.find("=");
 if (start == string::npos) {return "";}
 end   = stringMessageText.find( ")", start);
 if (end == string::npos) {return "";}
 strData = stringMessageText.substr(start+1, (end - start -1));

 strData = Create_Message( ANI_MESSAGE_373w, strData);

 
 return strData;
}

string MessageClass::fALI_Failed_PopUp_Message()
{
 string strReturnMessage;

 if (objCallData.intCallbackNumber)
  {strReturnMessage = Create_Message(ALI_MESSAGE_262p, fCallerIDGUIformat(objCallData.stringTenDigitPhoneNumber));}
 else 
  {strReturnMessage = Create_Message(ALI_MESSAGE_262p, fCallerIDGUIformat(objCallData.stringPANI));}

 return strReturnMessage;
}

string MessageClass::fBlind_Xfer_Unable_PopUp_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;
 
 SearchPosition = strReturnMessage.find("]");
 strReturnMessage.insert(SearchPosition +1, "\n");

 SearchPosition = strReturnMessage.find(";");
 strReturnMessage.insert(SearchPosition +1, "\n");
 return strReturnMessage;
}

string MessageClass::fConf_Xfer_Failed_PopUp_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;
 SearchPosition = strReturnMessage.find("ID=");
 if (SearchPosition != string::npos) {
  strReturnMessage = strReturnMessage.substr(0,SearchPosition);
 }
 return strReturnMessage;
}

string MessageClass::fConf_Xfer_Unable_Empty_Lot_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;
 
 SearchPosition = strReturnMessage.find("]");
 strReturnMessage.insert(SearchPosition +1, "\n");

 SearchPosition = strReturnMessage.find(";");
 strReturnMessage.insert(SearchPosition +1, "\n");
 return strReturnMessage;
}

string MessageClass::fConf_Xfer_Unable_Valet_Lot_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;
 
 SearchPosition = strReturnMessage.find("]");
 strReturnMessage.insert(SearchPosition +1, "\n");

 SearchPosition = strReturnMessage.find(";");
 strReturnMessage.insert(SearchPosition +1, "\n");
 return strReturnMessage;
}





string MessageClass::fTransfer_Cancelled_PopUp_Message()
{
 size_t start, end;
 string strData;

 start = stringMessageText.find("=");
 if (start == string::npos) {return "";}
 end   = stringMessageText.find( ")", start);
 if (end == string::npos) {return "";}
 strData = stringMessageText.substr(start+1, (end - start -1));

 strData = Create_Message( ANI_MESSAGE_366w, strData);

 
 return strData;
}




string MessageClass::fDial_Out_Failed_PopUp_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;
 
 SearchPosition = strReturnMessage.find("]");
 strReturnMessage.insert(SearchPosition +1, "\n");

 SearchPosition = strReturnMessage.find(";");
 strReturnMessage.insert(SearchPosition +1, "\n");
 return strReturnMessage;
}

string MessageClass::fUnable_to_Rebid_ALI_PopUp_Message() {

 string stringPopUp = this->stringMessageText;



 return stringPopUp;          
}



string MessageClass::fPort_Down_Alarm_PopUp_Message()
{
 //  "[ALARM] ALI Port Pair 1 Down"
 //  "[ALARM] CAD 1 - Port Restored"
 size_t index;
 string stringPopUp   = "";
 string stringPort    = ""; 
 string stringPortNum = "";
 if (objPortData.intPortNum){stringPortNum = int2str(objPortData.intPortNum);}
 stringPort = Thread_Calling(objPortData.enumPortType).erase(3);
 stringPopUp = "[ALARM] " + stringPort + " " + stringPortNum + objPortData.strSideAorB + " - ";
 index = stringMessageText.find(", ");
 if (index != string::npos){ stringPopUp.append(stringMessageText,8, index - 8);}
 else                      { stringPopUp.append(stringMessageText,8, string::npos);}                                                               

 return stringPopUp;          
}

string MessageClass::fALI_DB_OTS_PopUp_Message()
{
 //   "[ALARM] ALI 01 B < Broadcast Message Host Going Out of Service"
 //   "[ALARM] ALI 1B > Database going out of service"
 string stringPopUp;

 stringPopUp = "[ALARM] " + Thread_Calling(objPortData.enumPortType).erase(3) + " " + int2str(objPortData.intPortNum) 
               + objPortData.strSideAorB + " " + ALI_MESSAGE_230w;


 return stringPopUp;
}

string MessageClass::fPort_ShutDown_Alarm_PopUp_Message()
{
  //   "[ALARM] ALI 1B – Port blocked for next 3 minutes due to excessive incoming data"
 string stringPopUp;

 stringPopUp =  "[ALARM] " + Thread_Calling(objPortData.enumPortType).erase(3) + " " + int2str(objPortData.intPortNum) 
               + objPortData.strSideAorB + " " + KRN_MESSAGE_122w1;
 stringPopUp+= seconds2time(int_PORT_COOL_DOWN_PERIOD_SEC);
 stringPopUp+= KRN_MESSAGE_122w2;
 return stringPopUp;
}
/*
string MessageClass::fNo_Correlation_Alarm_PopUp_Message()
{
 string stringPopUp;
 string stringTemp;

 //    "[ALARM] ANI 1 > Unable to determine which position answered the call for callback number (303) 818-3060” 
 stringPopUp =  "[ALARM] " + Thread_Calling(objPortData.enumPortType).erase(3) + " " + int2str(objPortData.intPortNum) 
                               + " " + ANI_MESSAGE_313w;
 if (objCallData.stringTenDigitPhoneNumber.length() == 10)
  {
   stringTemp = "(" + objCallData.stringTenDigitPhoneNumber;
   stringTemp.insert(4,") ");
   stringTemp.insert(9,"-");
   stringPopUp+= stringTemp;
  }
 return stringPopUp;
}
*/
/*
string MessageClass::fANI_Alarm_Code_PopUp_Message()
{
  string stringPopUp;
  string stringTemp;
  size_t index, index2;

  //   "[ALARM] ANI 1 > ANI-Link Permanent Seizure of ANI Trunk Card 1 (Code = C75)"
  //   "[ALARM] ANI 1 > ANI-Link Communication Lost to ANI Trunk Card 1 (Code = C71)"
  //   "[ALARM] ANI 1 > ANI-Link Communication Lost to ANI Display Card 1 (Code = C81)"" 
   stringPopUp =  "[ALARM] " + Thread_Calling(objPortData.enumPortType).erase(3) + " " + int2str(objPortData.intPortNum) 
                  + objPortData.strSideAorB + " > ";
   index  = stringMessageText.find("ANI-Link");
   index2 = stringMessageText.find("Card");
   if ((index == string::npos)||(index2 == string::npos)) {return "[ALARM] Unspecified ANI-Link Error";}           
   stringPopUp.append(stringMessageText,index,(index2+5-index));
   stringTemp.assign(stringMessageText,index2+5, string::npos);
   stringPopUp += StripLeadingZeroes(stringTemp)+" ";
   index = stringMessageText.find("(");
   if (index == string::npos) {return "[ALARM] Unspecified ANI-Link Error";}
   stringPopUp.append(stringMessageText,index,string::npos);

 return stringPopUp;
}
*/
string MessageClass::fTel_Equipment_PopUp_Message()
{
 size_t SearchPosition;
 string strReturnMessage = stringMessageText;
 
 SearchPosition = strReturnMessage.find("[ALARM]");
 if (SearchPosition == string::npos) {return "Unknown Error";}
 strReturnMessage = strReturnMessage.substr(SearchPosition,string::npos);

 return strReturnMessage;
}



/****************************************************************************************************
*
* Name:
*  Function: MessageClass::fconsole()
*
*
* Description:
*  This is a <MessageClass> function implementation
*
* 
* Details:
*  The function displays object data to the console.
*    
* Parameters:
*   none
*
* Variables:
*   stringOutputMessage                 Local   - <MessageClass> object variable
*                                                                          
* Functions:
*   //cout                                Library - <iostream>
*   endl                                Library - <iostream>
*
* Author(s):
*   Bob McCarthy 			Original: 2/12/2007
*                                       Updated : N/A
*
****************************************************************************************************/
void MessageClass::fConsole()
{
 string    stringDisplay;
 string    stringMilliseconds;
 size_t    position = 0;
 
 stringDisplay = stringOutputMessage;
 if(stringDisplay.length() < 35) {return;}
 do 
 {
  stringMilliseconds.assign(stringOutputMessage,30,4);
  stringDisplay.erase(position,41);
  if(stringDisplay.length() < (position + 23)){ SendCodingError("MessageClass.cpp - Coding Error in MessageClass::fConsole()"); return;}
  stringDisplay.insert ( position + 23, stringMilliseconds );
  position = stringDisplay.find ( stringSERVER_HOSTNAME + " |", position );

 }while(position != string::npos);

 cout << stringDisplay << endl;

}// end fConsole()


void MessageClass::fUpdatePositionInMessageText()
{
 size_t SearchPosition;
 string strPosition; 
 if (stringOutputMessage.length() < 105) {return;}
 SearchPosition = stringOutputMessage.find( "| P", 100 );
 if (SearchPosition == string::npos) {return;}
 strPosition = int2strLZ(objCallData.intPosn);

 stringOutputMessage.replace(SearchPosition+3,2,strPosition);
}

void MessageClass::fUpdateCallInfoInMessageText()
{
 size_t SearchPosition;
 string strReplacePANI;

 if (objCallData.stringTenDigitPhoneNumber.length() != 10) { SendCodingError("MessageClass.cpp - phone number not set in fn MessageClass::fUpdatePositionInMessageText()"); return;}  
 if (stringOutputMessage.length() < 115) {return;}
 SearchPosition = stringOutputMessage.find_first_of( "|", 107 );
 if (SearchPosition == string::npos) {return;}
 stringOutputMessage.replace(SearchPosition + 2,10,objCallData.stringTenDigitPhoneNumber);

}





void MessageClass::fClearMsg()
{
 enumMessageType                = NORMAL_MSG;
 stringTimeStamp 		= "";
 stringThreadCalling 		= "";
 stringMessageText		= "";
 intLogCode			= 0;
 intMessageCode			= 0;
 intReturnCode			= 0;
 stringOutputMessage		= "";
 stringSubject			= "";
 stringEmailMessageBody		= "";
 mtimespecTimeStamp.tv_sec	= 0;
 mtimespecTimeStamp.tv_nsec	= 0;
 rtimespecTimeStamp.tv_sec	= 0;
 rtimespecTimeStamp.tv_nsec	= 0;
 objCallData.fClear();
 objPortData.fClear();
 strLogXML                      = XML_LOG_ELEMENT;        
         
}



/****************************************************************************************************
*
* Name:
*  Function: MessageClass::fLog()
*
*
* Description:
*  This is a <MessageClass> function implementation
*
* 
* Details:
*  The function writes object data to the Log file . This function is only utilized in
*  the Log Thread and does not currently need semaphore protection.
*    
* Parameters:
*   none
*
* Variables:
*   boolEMAIL_MONTHLY_LOGS              Global  - <Experient_Controller.cpp>
*   boolLastFileOpenOperationFailed	Global  - <globals.h>
*   boolLastWriteOperationFailed        Global  - <globals.h>
*   charAttachment[]                    Local   - char array holding name of attachment file
*   intLogFileOpenError                 Global  - counter
*   intLogFileWriteError                Global  - counter
*   intRC                               Local   - int return code for functions
*   objEmailLog                         Local   - <ExperientEmail> email message
*   objMessage                          Local   - <MessageClass> 
*   ptrToLogFile                        Global  - pointer to file for open/close/write
*   stringCurrentLogFileName            Global  - <Experient_Controller.cpp> <cstring> current active log file name
*   stringLogFileName                   Local   - <cstring> calculated log file name
*                                                                          
* Functions:
*   .Connect()                          Library - <ipworks.h><FileMailer>
*   .c_str()                            Library - <cstring>
*   .Disconnect()                       Library - <ipworks.h><FileMailer>
*   fclose()                            Library - <iostream>
*   .fIntializeHeaders()                Global  - <ExperientEmail>
*   fopen()                             Library - <iostream>
*   fputs()                             Library - <iostream>
*   GetLogFileName()                    Global  - <globalfunctions.h>
*   .Send()                             Library - <ipworks.h><FileMailer>
*   .SetAttachmentCount()               Library - <ipworks.h><FileMailer>
*   .SetAttachments()                   Library - <ipworks.h><FileMailer>
*   .SetMessageText()                   Library - <ipworks.h><FileMailer>
*   .SetSendTo()                        Library - <ipworks.h><FileMailer>
*   .SetSubject()                       Library - <ipworks.h><FileMailer>
*
* Author(s):
*   Bob McCarthy 			Original: 2/17/2007
*                                       Updated : N/A
*
****************************************************************************************************/
unsigned long long int MessageClass::fLog(log_type enumLogtype)
{
 int            intRC;
 string         stringLogFileName;
 ExperientEmail objEmailLog;
 MessageClass   objMessage;
 string         stringCurrentFile;
 struct stat    st;
 bool           boolBadWrite = false;
 bool           boolBadOpen = false;
 bool           boolFirstEntry = false;
 bool           boolRollover   = false;
 string         strXMLversionHeader = XML_LOG_HEADER;
 string         strOutputXML = strLogXML;
 string         strFilePathName;

 extern ofstream   ReadableLogFile;
 extern ofstream   XMLlogfile;
 extern ofstream   DebugLogFile;
 extern ofstream   AMIscriptLogfile;
 extern ofstream   JSONlogfile;

 strXMLversionHeader+= "\n";
 strXMLversionHeader+= XML_LOG_TAG_START;
 strXMLversionHeader+= "\n";

// if (Logfile.is_open()) {Logfile.close(); cout << "was open" << endl;} 
 if (enumLogtype == DEBUG_LOG) {sem_wait(&sem_tMutexDebugLogName);}

 stringLogFileName = GetLogFileName(enumLogtype);
 switch (enumLogtype)
  {
   case READABLE_LOG:
        stringCurrentFile = stringCurrentLogFileName;
        strFilePathName = charLOG_FILE_PATH_NAME;

        boolRollover = (Month_DayFromLogFileName(stringCurrentFile) != Month_DayFromLogFileName(stringLogFileName));
        break;
   case XML_LOG: 
        stringCurrentFile = stringCurrentXMLlogFileName;
        strFilePathName = charLOG_FILE_PATH_NAME;
        boolRollover = (Month_DayFromLogFileName(stringCurrentFile) != Month_DayFromLogFileName(stringLogFileName));
        break;
   case DEBUG_LOG:
        stringCurrentFile = stringCurrentDebugFileName;
        strFilePathName = charLOG_FILE_PATH_NAME;
        boolRollover = (Month_DayFromLogFileName(stringCurrentFile) != Month_DayFromLogFileName(stringLogFileName));
        break;
   case AMI_SCRIPT:
        stringCurrentFile = stringCurrentAMIlogFileName;
        strFilePathName = charAMI_LOG_FILE_PATH;
        boolRollover = (stringLogFileName != stringCurrentFile);
        break; 
  case JSON_LOG:
        stringCurrentFile = stringCurrentJSONlogFileName;
        strFilePathName = charJSON_LOG_FILE_PATH;
        boolRollover = (Month_DayFromLogFileName(stringCurrentFile) != Month_DayFromLogFileName(stringLogFileName));
  }        

 

 //check for Daily rollover

 if (boolRollover)
  {

/*
    if ((stringCurrentFile != "")&&(boolEMAIL_MONTHLY_LOGS))
     {
      objEmailLog.fIntializeHeaders();
      objEmailLog.SetSendTo((char*)stringEMAIL_SEND_TO.c_str());
      objEmailLog.AddAttachment((char*)stringCurrentFile.c_str());
      objEmailLog.SetSubject((char*)"[INFO] - PSAP Monthly Log");
      objEmailLog.SetMessageText((char*)"Monthy PSAP Log attached.");
      if(!objEmailLog.GetConnected()) {objEmailLog.Connect();}
      intRC = objEmailLog.Send();
     }

   // Close Out Previous XML Monthly Log
   if (enumLogtype == XML_LOG) { CloseOutXMLfiles();}
*/
   //set new current file 
   switch (enumLogtype)
    {
     case READABLE_LOG:
          stringCurrentLogFileName = stringLogFileName;
          stringCurrentFile = stringLogFileName;
          break;
     case XML_LOG: 
          stringCurrentXMLlogFileName = stringLogFileName;
          stringCurrentFile = stringLogFileName; 
          if(stat(stringLogFileName.c_str(),&st) != 0) {strLogXML = strXMLversionHeader +  strLogXML; boolFirstEntry = true;}     
          break;
     case DEBUG_LOG:
          stringCurrentDebugFileName = stringLogFileName;
          stringCurrentFile = stringLogFileName;
          break;
     case AMI_SCRIPT:
          stringCurrentAMIlogFileName = stringLogFileName;
          stringCurrentFile = stringLogFileName;
          break;
     case JSON_LOG:
          stringCurrentJSONlogFileName = stringLogFileName;
          stringCurrentFile = stringLogFileName;
          break;
    }  
                                  

  }// end if (stringLogFileName != stringCurrentFile)




 //Open File
 switch (enumLogtype)
  {
   case READABLE_LOG:
         ReadableLogFile.clear(); 
         ReadableLogFile.open( stringCurrentFile.c_str(), ios_base::app);
         boolBadOpen = (!ReadableLogFile.is_open());
         break;
   case XML_LOG: 
         XMLlogfile.clear(); 
         XMLlogfile.open( stringCurrentFile.c_str(), ios::app );
         boolBadOpen = (!XMLlogfile.is_open());         
         break;
   case DEBUG_LOG:
         DebugLogFile.clear(); 
         DebugLogFile.open( stringCurrentFile.c_str(), ios_base::app);
         boolBadOpen = (!DebugLogFile.is_open());
         break;
   case AMI_SCRIPT:
        AMIscriptLogfile.clear();
        AMIscriptLogfile.open( stringCurrentFile.c_str(), ios_base::app);
        boolBadOpen = (!AMIscriptLogfile.is_open());
        break;
   case JSON_LOG:
        JSONlogfile.clear();
        JSONlogfile.open( stringCurrentFile.c_str(), ios_base::app);
        boolBadOpen = (!JSONlogfile.is_open());     
  }
  
 if(boolBadOpen)
  { 
   switch (errno)
    {
     case EINTR:
      //interrupt by signal ...try again
      switch (enumLogtype)
       {
        case READABLE_LOG:
             ReadableLogFile.clear(); 
             ReadableLogFile.open( stringCurrentFile.c_str(), ios_base::app);
             boolBadOpen = ReadableLogFile.is_open();
             break;
        case XML_LOG: 
             XMLlogfile.clear(); 
             XMLlogfile.open( stringCurrentFile.c_str(), ios_base::app);
             boolBadOpen = XMLlogfile.is_open();
             break;
        case DEBUG_LOG:
             DebugLogFile.clear(); 
             DebugLogFile.open( stringCurrentFile.c_str(), ios_base::app);
             boolBadOpen = (!DebugLogFile.is_open());
             break;
        case AMI_SCRIPT:
             AMIscriptLogfile.clear();
             AMIscriptLogfile.open( stringCurrentFile.c_str(), ios_base::app);
             boolBadOpen = (!AMIscriptLogfile.is_open());
             break;
        case JSON_LOG:
             JSONlogfile.clear();
             JSONlogfile.open( stringCurrentFile.c_str(), ios_base::app);
             boolBadOpen = (!JSONlogfile.is_open());

       }
      if (boolBadOpen)
       {
        intLogFileOpenError++;
        if(!boolLastFileOpenOperationFailed)
         {objMessage.fMessage_Create(LOG_WARNING,507, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                                     objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_507, int2strLZ(errno)); enQueue_Message(LOG,objMessage);}
        boolLastFileOpenOperationFailed = true;
        ReadableLogFile.close();
        XMLlogfile.close();
        DebugLogFile.close();
        AMIscriptLogfile.close();
        JSONlogfile.close(); 
        if (enumLogtype == DEBUG_LOG) {sem_post(&sem_tMutexDebugLogName);}
        return 1;
       }
      break;

     case ENOENT:

        //if directory does not exist...create it otherwise this is bogus
        if(stat(strFilePathName.c_str(),&st) == 0) {break;}
        intRC = mkdir(strFilePathName.c_str(), S_IRWXU|S_IRWXG|S_IRWXO|S_IWOTH|S_ISVTX);      
        if ((intRC > 0)&&(intRC != EEXIST)) 
         {
          //unable to make directory
          intLogFileOpenError++;
          if(!boolLastFileOpenOperationFailed)
           {objMessage.fMessage_Create(LOG_WARNING,512, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ ,objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                                       objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_512, int2strLZ(errno)); enQueue_Message(LOG,objMessage);}
          boolLastFileOpenOperationFailed = true;
          ReadableLogFile.close();
          XMLlogfile.close();
          DebugLogFile.close();
          AMIscriptLogfile.close();
          JSONlogfile.close(); 
          return 1;
         }
        else
         {
          boolLastFileOpenOperationFailed = false;
          // directory created.. send message ..try to create file
          objMessage.fMessage_Create(LOG_WARNING,513, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                                     objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_513); 
          enQueue_Message(LOG,objMessage);

          // open/create file
          switch (enumLogtype)
           {
            case READABLE_LOG:
                   ReadableLogFile.clear(); 
                   ReadableLogFile.open( stringCurrentFile.c_str(), ios_base::app);
                   boolBadOpen = ReadableLogFile.is_open();
                   break;
            case XML_LOG:
                   XMLlogfile.clear(); 
                   XMLlogfile.open( stringCurrentFile.c_str(), ios_base::app);
                   boolBadOpen = XMLlogfile.is_open();
                   break;
             case DEBUG_LOG:
                   DebugLogFile.clear(); 
                   DebugLogFile.open( stringCurrentFile.c_str(), ios_base::app);
                   boolBadOpen = (!DebugLogFile.is_open());
                   break;
             case AMI_SCRIPT:
                  AMIscriptLogfile.clear();
                  AMIscriptLogfile.open( stringCurrentFile.c_str(), ios_base::app);
                  boolBadOpen = (!AMIscriptLogfile.is_open());
                  break;
            case JSON_LOG:
                  JSONlogfile.clear();
                  JSONlogfile.open( stringCurrentFile.c_str(), ios_base::app);
                  boolBadOpen = (!JSONlogfile.is_open());
           }
        if (boolBadOpen)
         {
          if(stat(charLOG_FILE_PATH_NAME,&st) == 0) { break;}
          intLogFileOpenError++;
          if(!boolLastFileOpenOperationFailed)
           {objMessage.fMessage_Create(LOG_WARNING,507, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                                       objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_507, int2strLZ(errno)); enQueue_Message(LOG,objMessage);}
          boolLastFileOpenOperationFailed = true;
          ReadableLogFile.close();
          XMLlogfile.close();
          DebugLogFile.close();
          AMIscriptLogfile.close();
          JSONlogfile.close();
          if (enumLogtype == DEBUG_LOG) {sem_post(&sem_tMutexDebugLogName);}  
          return 1;
         } 

       }// end if (intRC) else
      break;
 /* 
      //cout << "EACCES       = " << EACCES << endl;
      //cout << "EINTR        = " << EINTR << endl;
      //cout << "EISDIR       = " << EISDIR << endl;
      //cout << "ELOOP        = " << ELOOP << endl;
      //cout << "EMFILE       = " << EMFILE << endl;
      //cout << "ENAMETOOLONG = " << ENAMETOOLONG << endl;
      //cout << "ENFILE       = " << ENFILE << endl;
      //cout << "ENOENT       = " << ENOENT << endl;
      //cout << "ENOSPC       = " << ENOSPC << endl;
      //cout << "ENOTDIR      = " << ENOTDIR << endl;
      //cout << "ENXIO        = " << ENXIO << endl;
      //cout << "EOVERFLOW    = " << EOVERFLOW << endl;
      //cout << "EROFS        = " << EROFS << endl;
      //cout << "EINVAL       = " << EINVAL << endl;
      //cout << "EMFILE       = " << EMFILE << endl;
      //cout << "EMFILE       = " << EMFILE << endl;
      //cout << "ENOMEM       = " << ENOMEM << endl;
      //cout << "ETXTBSY      = " << ETXTBSY << endl;
      exit(1);
*/

      
     default:
      intLogFileOpenError++;
      if(!boolLastFileOpenOperationFailed)
       {objMessage.fMessage_Create(LOG_WARNING,507, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_507, int2strLZ(errno));  enQueue_Message(LOG,objMessage);}
      boolLastFileOpenOperationFailed = true;
      ReadableLogFile.close();
      XMLlogfile.close();
      DebugLogFile.close();
      AMIscriptLogfile.close();
      JSONlogfile.close(); 
      if (enumLogtype == DEBUG_LOG) {sem_post(&sem_tMutexDebugLogName);}        
      return 1;
 
    }// end switch (errno)

  }// end if (boolBadOpen)

 //file is now open .... if new make world readable...
  if (boolRollover) {chmod(stringCurrentFile.c_str(), S_IROTH|S_IRUSR|S_IWUSR|S_IRGRP);}

 //write to file
 switch (enumLogtype)
  {
   case READABLE_LOG:
        ReadableLogFile << stringOutputMessage << endl;
        boolBadWrite = ReadableLogFile.bad();
        break;
   case XML_LOG:     
        XMLlogfile << strLogXML << endl;
        boolBadWrite = XMLlogfile.bad();
        break;
   case DEBUG_LOG:
        DebugLogFile << stringOutputMessage << endl;
        boolBadWrite = DebugLogFile.bad();
        break;
   case AMI_SCRIPT:
        AMIscriptLogfile  << stringOutputMessage << endl;
        boolBadWrite = AMIscriptLogfile.bad();
        break;
   case JSON_LOG:
        JSONlogfile  << strJSONoutput << endl;  
        boolBadWrite = JSONlogfile.bad();
        break;

  }                   
 
 if (boolBadWrite)
  {
   intLogFileWriteError++;
   if(!boolLastWriteOperationFailed)
    {objMessage.fMessage_Create(LOG_WARNING,508, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                                objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_508,int2strLZ(errno)); enQueue_Message(LOG,objMessage);}
   boolLastWriteOperationFailed = true;
   ReadableLogFile.close();
   XMLlogfile.close();
   DebugLogFile.close();
   AMIscriptLogfile.close(); 
   JSONlogfile.close();
   if (enumLogtype == DEBUG_LOG) {sem_post(&sem_tMutexDebugLogName);}      
   return 1;
  }


 // close file
  ReadableLogFile.close();
  XMLlogfile.close();
  DebugLogFile.close();
  AMIscriptLogfile.close();
  JSONlogfile.close();              

 // restore message if previous failures
 if (boolLastFileOpenOperationFailed||boolLastWriteOperationFailed)
  {objMessage.fMessage_Create(LOG_WARNING,526, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_LOG_PORT, 
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_526); enQueue_Message(LOG,objMessage);}

 //reset error flags
 boolLastFileOpenOperationFailed = false;
 boolLastWriteOperationFailed = false;
 if (enumLogtype == DEBUG_LOG) {sem_post(&sem_tMutexDebugLogName);}
 return 0;


}// end fLog()

/****************************************************************************************************
*
* Name:
*  Function: MessageClass::fSetSubject()
*
*
* Description:
*  This is a <MessageClass> function implementation
*
* 
* Details:
*  The function sets the stringSubject member of the class.
*    
* Parameters:
*   stringArg1                          <cstring>
*
* Variables:
*   stringSubject                       <MessageClass><cstring> 
*                                                                          
* Functions:
*   none
*
* Author(s):
*   Bob McCarthy 			Original: 3/09/2007
*                                       Updated : N/A
*
****************************************************************************************************/
void MessageClass::fSetSubject(string stringArg1)       {stringSubject = stringArg1;}


