
/*****************************************************************************
* FILE: CommPortClasses.cpp
*  
*
* DESCRIPTION:
*  Contains function implementations of the ExperientCommPort class, 
*  ExperientUDPPort class, and Port_Data class
*
*
*
* AUTHOR: 2/5/2009 Bob McCarthy
* LAST REVISED:  
*
* COPYRIGHT: 2006-2009 Experient Corporation
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globalfunctions.h"
#include "globals.h"
#include "/datadisk1/src/simpleini/SimpleIni.h"          // ini software
#include "/datadisk1/src/xmlParser/xmlParser.h"

// External Variable Declaration
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data				objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//                                                                          ExperientHTTPPort FUNCTIONS
//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//                                                                          ExperientCommPort FUNCTIONS
//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
string Port_Data::fNotes()
{
 extern ExperientCommPort                        ALIPort	[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort                        ALIServicePort	[intMAX_ALI_PORT_PAIRS  + 1 ]; 
 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort                        ANIPort	[NUM_ANI_PORTS_MAX+1]; 

 switch (this->enumPortType) {
  case CAD:
       return CADPort[this->intPortNum].strNotes;

  case ALI:
         return ALIPort[this->intPortNum][this->intSideAorB].strNotes;

  case ANI:
       return ANIPort[this->intPortNum].strNotes;

  case CTI:
       break;
  default:
       break;
 }
  return "none";
}

vector <Position_Alias> Port_Data::fCadPortAliasVector() {
 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];
 std:vector <Position_Alias> empty_vector;
 empty_vector.clear();

 if (this->enumPortType != CAD) {return empty_vector;}

 return CADPort[this->intPortNum].PositionAliases.vPositionAliases;
}

size_t Port_Data::fCadPortAliasVectorSize() {
 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];
 if (this->enumPortType != CAD) {return 0;}
 return CADPort[this->intPortNum].PositionAliases.vPositionAliases.size();
}
int Port_Data::fCadPortAlias(int i) {

 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];

// //cout << "port num -> " << this->intPortNum << endl;
 if (this->enumPortType != CAD) {return 0;}

 return char2int(CADPort[this->intPortNum].PositionAliases.fAliasOfPosition(i).c_str());
}

bool Port_Data::fRemoveFirstALiCR() {

 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];

 if (this->enumPortType != CAD) {return false;}

 return CADPort[this->intPortNum].boolCAD_EraseFirstALI_CR;
}

bool Port_Data::fUseNENACADheader() {

 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];

 if (this->enumPortType != CAD) {return false;}

 return CADPort[this->intPortNum].boolNENA_CADheader;
}

string Port_Data::fConnectionProtocol()
{
 extern ExperientCommPort                        ALIPort	[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort                        ALIServicePort	[intMAX_ALI_PORT_PAIRS  + 1 ]; 
 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort                        ANIPort	[NUM_ANI_PORTS_MAX+1]; 

 switch (this->enumPortType) {
  case CAD:
       return ConnectionType(CADPort[this->intPortNum].ePortConnectionType);

  case ALI:
         return ConnectionType(ALIPort[this->intPortNum][this->intSideAorB].ePortConnectionType);

  case ANI:
       return ConnectionType(ANIPort[this->intPortNum].ePortConnectionType);

  case CTI:
       break;
  default:
       break;
 }
  return 0;
}


int Port_Data::fRemotePortNumber()
{
 extern ExperientCommPort                        ALIPort	[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort                        ALIServicePort	[intMAX_ALI_PORT_PAIRS  + 1 ]; 
 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort                        ANIPort	[NUM_ANI_PORTS_MAX+1]; 

 switch (this->enumPortType) {
  case CAD:
       return CADPort[this->intPortNum].intRemotePortNumber;

  case ALI:
       switch (this->intSideAorB){
        case 1: case 2:
         switch (ALIPort[this->intPortNum][this->intSideAorB].ePortConnectionType){
          case TCP: return ALIPort[this->intPortNum][this->intSideAorB].intRemotePortNumber;
          case UDP: return ALIPort[this->intPortNum][this->intSideAorB].intRemotePortNumber;
          default:
               SendCodingError("CommPortClasses.cpp - coding error Port_Data::fRemotePortNumber() No Connection Type Defined");
           return 0;
         }
        case 3:
         return ALIServicePort[this->intPortNum].intRemotePortNumber;
       }

  case ANI:
       return ANIPort[this->intPortNum].intRemotePortNumber;

  case CTI:
       break;
  default:
       break;
 }
  return 0;
}


string Port_Data::fRemoteIPaddressofPort()
{
 extern ExperientCommPort                        ALIPort	[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort                        ALIServicePort	[intMAX_ALI_PORT_PAIRS  + 1 ]; 
 extern ExperientCommPort                        CADPort       	[NUM_CAD_PORTS_MAX+1];
 extern ExperientCommPort                        ANIPort	[NUM_ANI_PORTS_MAX+1]; 

 switch (this->enumPortType) {
  case CAD:
       return CADPort[this->intPortNum].RemoteIPAddress.stringAddress;

  case ALI:
       switch (this->intSideAorB){
        case 1: case 2:
         switch (ALIPort[this->intPortNum][this->intSideAorB].ePortConnectionType){
          case TCP: return ALIPort[this->intPortNum][this->intSideAorB].RemoteIPAddress.stringAddress;
          case UDP: return ALIPort[this->intPortNum][this->intSideAorB].RemoteIPAddress.stringAddress;
          default:
               SendCodingError("CommPortClasses.cpp - coding error Port_Data::fRemoteIPaddressofPort() No Connection Type Defined");
           return "";
         }
        case 3:
         return ALIServicePort[this->intPortNum].RemoteIPAddress.stringAddress;
       }

  case ANI:
         return ANIPort[this->intPortNum].RemoteIPAddress.stringAddress;

  case CTI:
       break;
  default:
       break;
 }
  return "UNKNOWN";
}

void ExperientCommPort::fInitializeCADport(int i, bool boolSKIPACTIVE) {
 string strKey = IPWORKS_RUNTIME_KEY;

 intPortNum                                  = i; 
 enumPortType                                = CAD; 
 fSetPortInactive();
 boolReducedHeartBeatMode                    = false;
 boolReducedHeartBeatModeFirstAlarm          = false;
 boolACKReceived                             = false;
 boolNAKReceived                             = false;
 boolFirstACKRcvd                            = false;
 boolXmitRetry                               = true;
 intHeartbeatInterval                        = intCAD_HEARTBEAT_INTERVAL_SEC;
 intPortDownReminderThreshold                = intCAD_PORT_DOWN_REMINDER_SEC;
 intPortDownThresholdSec                     = intCAD_PORT_DOWN_THRESHOLD_SEC;
 boolUnexpectedDataShowMessage               = true;
 intUnexpectedDataCharacterCount             = 0; 
 boolReadyToShutDown                         = false;
 intStrayACKCounter                          = 0;
 intConsecutiveNAKCounter                    = 0;
 intHBTransactionID                          = 0;

#ifdef IPWORKS_V16
 UDP_Port.SetRuntimeLicense((char*)strKey.c_str());
 TCP_Server_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
//race condition don't put these in or fix the race .....
// boolNENA_CADheader                          = true;
// boolCAD_EraseFirstALI_CR                    = false;

  switch(ePortConnectionType) {
   case UDP: UDP_Port.fInitializeCADport(i, boolSKIPACTIVE);  break;
   case TCP: 
       if (boolSKIPACTIVE)                             {break;}
       TCP_Server_Port.fInitializeCADport(i);           break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fInitializeCADport() No Connection Type Defined");
        return;
  } 
 if (!boolSKIPACTIVE) {
  fSetPortActive();
 }   
}


void ExperientCommPort::fInitializeALIport(int i, int j)
{
 string strKey = IPWORKS_RUNTIME_KEY;
 enumPortType                                = ALI;
 intPortNum                                  = i;
 intSideAorB                                 = j;
 fSetPortInactive();
 boolRestartListenThread                     = false;
 boolReducedHeartBeatMode                    = false;
 boolReducedHeartBeatModeFirstAlarm          = false;
 boolALIportPairDown                         = false;
 boolACKReceived                             = false;
 boolNAKReceived                             = false;
 boolFirstACKRcvd                            = false;
 boolXmitRetry                               = true;
 intHeartbeatInterval                        = intALI_HEARTBEAT_INTERVAL_SEC;
 intPortDownReminderThreshold                = intALI_PORT_DOWN_REMINDER_SEC;
 intPortDownThresholdSec                     = intALI_PORT_DOWN_THRESHOLD_SEC;
 boolHeartbeatActive                         = false;
 //boolPhantomPort                             = false; //dont set it is set in initialize
 intBidsActive                               = 0;
 boolUnexpectedDataShowMessage               = true;
 intUnexpectedDataCharacterCount             = 0;
 intStrayACKCounter                          = 0; 
 intConsecutiveNAKCounter                    = 0;
 intConsecutiveBadRecordCounter              = 0; 
 boolReadyToShutDown                         = false;
 boolFirstStrayAck                           = false;
 intHBTransactionID                          = 0;
// boolNENA_CADheader                          = true;
// boolCAD_EraseFirstALI_CR                    = false;

#ifdef IPWORKS_V16
 UDP_Port.SetRuntimeLicense((char*)strKey.c_str());
 TCP_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
 switch(ePortConnectionType)
  {
   case UDP: UDP_Port.fInitializeALIPort(i,j); break;
   case TCP: TCP_Port.fInitializeALIPort(i,j); break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fInitializeALIport() No Connection Type Defined");
        return;
  } 

 fSetPortActive();    
}

void ExperientCommPort::fInitializeECATS_UDP_Port()
{
 int 					intRC;
 extern IP_Address			ECATS_IP;
 extern int                             ECATS_PORT_NUMBER;
 string strKey = IPWORKS_RUNTIME_KEY;

   enumPortType                                = LOG;
   ePortConnectionType                         = UDP;
   intPortNum                                  = 1;
   boolUnexpectedDataShowMessage               = true;
   intUnexpectedDataCharacterCount             = 0; 
   intHBTransactionID                          = 0;
   boolReadyToShutDown                         = false;
   boolPortActive                              = true; 
   boolRestartListenThread                     = false;


   UDP_Port.intPortDownThresholdSec            = ECATS_ALARM_DELAY_SEC;
   UDP_Port.intPortDownReminderThreshold       = ECATS_ALARM_REMINDER_SEC; 
   UDP_Port.boolPortWasDown                    = false;
   UDP_Port.boolPortAlarm                      = false;

   UDP_Port.enumPortType                       = LOG;
   UDP_Port.intPortNum                         = 1; 
   intLocalPort                                = ECATS_PORT_NUMBER;
#ifdef IPWORKS_V16
   UDP_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
   UDP_Port.Config((char*)"QOSFlags=16||160");
   UDP_Port.Config((char*)"MaxPacketSize=32768");
   UDP_Port.Config((char*)"InBufferSize=655360");
   UDP_Port.Config((char*)"OutBufferSize=655360");
   UDP_Port.boolPortActive                     = true; 
   UDP_Port.boolPingStatus                     = false;
   UDP_Port.SetLocalHost(NULL);
   UDP_Port.SetLocalPort(intLocalPort);
   UDP_Port.fPortData();
   UDP_Port.SetRemotePort(intLocalPort);
   UDP_Port.SetRemoteHost(ECATS_IP.stringAddress.c_str());   
   UDP_Port.intDiscardedPackets                = 0;

   fSetPortActive();
   
   intRC = UDP_Port.SetAcceptData(true);
   if (intRC){UDP_Port.Error_Handler();}
   intRC = UDP_Port.SetActive(true);
   if (intRC){UDP_Port.Error_Handler();}

}




void ExperientCommPort::fInitializeWRKUDPport(int i)
{
 int 					intRC;
 extern bool 				boolMULTICAST;
 extern IP_Address			MULTICAST_GROUP;
 string strKey = IPWORKS_RUNTIME_KEY;
   enumPortType                                = WRK; 
   ePortConnectionType                         = UDP;
   intPortNum                                  = i;
   boolUnexpectedDataShowMessage               = true;
   intUnexpectedDataCharacterCount             = 0; 
   intHBTransactionID                          = 0;
   boolReadyToShutDown                         = false;
   boolPortActive                              = true; 

   UDP_Port.intPortDownThresholdSec            = intWRK_PORT_DOWN_THRESHOLD_SEC;
   UDP_Port.intPortDownReminderThreshold       = intWRK_PORT_DOWN_REMINDER_SEC; 
   UDP_Port.boolPortWasDown                    = false;
   UDP_Port.boolPortAlarm                      = false;

   UDP_Port.enumPortType                       = WRK;
   UDP_Port.intPortNum                         = 1; 
   intLocalPort                                = intWRK_LOCAL_LISTEN_PORT;
#ifdef IPWORKS_V16
   UDP_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
   UDP_Port.Config((char*)"QOSFlags=16||160");
   UDP_Port.Config((char*)"MaxPacketSize=32768");
   UDP_Port.Config((char*)"InBufferSize=655360");
   UDP_Port.Config((char*)"OutBufferSize=655360");
   UDP_Port.boolPortActive                     = true; 
   UDP_Port.boolPingStatus                     = false;
   UDP_Port.SetLocalHost(NULL);
   UDP_Port.SetLocalPort(intLocalPort);
   UDP_Port.fPortData();

   UDP_Port.intDiscardedPackets                = 0;

   fSetPortActive();

   intRC = UDP_Port.SetAcceptData(true);
   if (intRC){UDP_Port.Error_Handler();}
   intRC = UDP_Port.SetActive(true);
   if (intRC){UDP_Port.Error_Handler();}

   if (boolMULTICAST)
    {
     intRC =  UDP_Port.SetMulticastGroup((char*)MULTICAST_GROUP.stringAddress.c_str());
     if (intRC){UDP_Port.Error_Handler();}
    }


}





void ExperientCommPort::fInitializeWRKTCPServerPort(int i)
{

 extern int                             intNUM_WRK_STATIONS;
 string                                 strMaxConnections ="MaxConnections=" + int2str(intNUM_WRK_STATIONS*3);
 string                                 strKey = IPWORKS_RUNTIME_KEY;

   enumPortType                                = WRK; 
   intPortNum                                  = i;

   boolReadyToShutDown                         = false;
   boolPortActive                              = true; 
   boolRestartListenThread                     = false;
#ifdef IPWORKS_V16
   TCP_Server_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
   TCP_Server_Port.fInitializeConnectionActiveVector(intNUM_WRK_STATIONS*3);
   TCP_Server_Port.enumPortType                       = WRK;
   TCP_Server_Port.intPortNum                         = 1; 
   intLocalPort                                       = intWRK_LOCAL_LISTEN_PORT;
   TCP_Server_Port.Config((char*)strMaxConnections.c_str());
   TCP_Server_Port.Config((char*)"InBufferSize=33554432");
   TCP_Server_Port.Config((char*)"OutBufferSize=33554432");
   TCP_Server_Port.SetDefaultMaxLineLength(65536);
   TCP_Server_Port.Config((char*)"TcpNoDelay=true");
   TCP_Server_Port.SetLinger(false);
   TCP_Server_Port.boolPortActive                     = true; 

   TCP_Server_Port.SetLocalHost(NULL);
   TCP_Server_Port.SetLocalPort(intLocalPort);
   TCP_Server_Port.fPortData();
   TCP_Server_Port.SetListening(true);

}

void ExperientCommPort::fInitializeCTI_TCP_Port()
{
 extern int                             intPOLYCOM_CTI_TCP_PORT;
 extern int                             intNUM_WRK_STATIONS;
 string                                 strMaxConnections ="MaxConnections=" + int2str(intNUM_WRK_STATIONS*200);
 string                                 strKey = IPWORKS_RUNTIME_KEY;

   enumPortType                                = CTI; 
   intPortNum                                  = 1;

   boolReadyToShutDown                         = false;
   boolPortActive                              = true; 
   boolRestartListenThread                     = false;

//   TCP_Server_Port.SetDefaultSingleLineMode(true);
#ifdef IPWORKS_V16
   TCP_Server_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
   TCP_Server_Port.fInitializeConnectionActiveVector(intNUM_WRK_STATIONS*200);
   TCP_Server_Port.enumPortType                       = CTI;
   TCP_Server_Port.intPortNum                         = 1; 
   intLocalPort                                       = intPOLYCOM_CTI_TCP_PORT;
   TCP_Server_Port.Config((char*)strMaxConnections.c_str());
   TCP_Server_Port.Config((char*)"InBufferSize=33554432");
   TCP_Server_Port.Config((char*)"OutBufferSize=33554432");
   TCP_Server_Port.SetDefaultMaxLineLength(65536);
   TCP_Server_Port.Config((char*)"TcpNoDelay=true");
   TCP_Server_Port.SetLinger(true);
   TCP_Server_Port.boolPortActive                     = true; 

   TCP_Server_Port.SetLocalHost(NULL);
   TCP_Server_Port.SetLocalPort(intLocalPort);
   TCP_Server_Port.fPortData();
   TCP_Server_Port.SetListening(true);

}





bool ExperientCommPort::fPostionUsesPort(int i)
{
 if (WorkstationGroup.WorkstationsInGroup.empty()) {return true;}
 return WorkstationGroup.WorkstationInGroup(i);
}



int ExperientCommPort::fNextHBTransactionId()
{
 intHBTransactionID++;
 if (intHBTransactionID > 999) {intHBTransactionID = 1;}
 return intHBTransactionID;
}

void ExperientCommPort::fResetWRKTCPSocket(int i)
{
 int 			intRC;
 int                    charResponse = -1;
 cout << endl << "WARNING! This procedure can possibly shut down the Controller, " << endl <<"Are you sure you wish to Restart the Workstation TCP socket ? y/n :" << endl;

  // Make Keyboard Input NoEcho
 charResponse = NonBlockingNoEchoInputChar();

 if ((charResponse !='y')&&(charResponse !='Y')) {
   cout << "Reset Cancelled ..." << endl;
   return;
 }


 intRC = sem_wait(&sem_tMutexWRKWorkTable);
 if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKWorkTable, "sem_wait@sem_tMutexWRKWorkTable in WRK Thread:Workstation_Check_Timed_Event()", 1);}

 Clear_Workstations();

 sem_post(&sem_tMutexWRKWorkTable);

 cout << "Shutting Down TCP Socket on Port " << TCP_Server_Port.GetLocalPort() << endl;
 TCP_Server_Port.Shutdown();

 sleep(5);

 fInitializeWRKTCPServerPort(i);
 cout << "Started TCP Socket on Port " << TCP_Server_Port.GetLocalPort() << endl;
}

int ExperientTCP_SERVER_Port::fisHTTP_Post(string strInput, int i)
{
 size_t 		found;
 string 		strReturn;
 time_t 		time_tTimeNow;
 struct tm		tmUniversalTimeStamp;
 struct timespec 	timespecTimeNow;
 char                   char_arrayUniversalTimeStamp[30];
 string                 strTimeStamp;

 found = strInput.find("POST / HTTP/1.1");
 if (found == string::npos) {return false;}

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 time_tTimeNow = timespecTimeNow.tv_sec; 
 gmtime_r(&time_tTimeNow,&tmUniversalTimeStamp);
 strftime(char_arrayUniversalTimeStamp, sizeof(char_arrayUniversalTimeStamp), "%a, %d %b %Y %H:%M:%S GMT", &tmUniversalTimeStamp );
 strTimeStamp = char_arrayUniversalTimeStamp;

 found = strInput.find("Expect: 100-continue");
 if (found != string::npos) 
  {
   strReturn = "HTTP/1.1 100 Continue\r\n\r\nServer: ExperientCTI\r\nTransfer-Encoding: chunked\r\nDate: " + strTimeStamp + "\r\n\r\n"; 
   Send(i, strReturn.c_str(), strReturn.length());
   //cout << "sent 100 continue" << endl;
   return 0;
  }


   strReturn = "HTTP/1.1 200 OK\r\n\r\nXServer: ExperientCTI\r\nConnection: Close\r\nDate: " + strTimeStamp +"\r\n\r\n"; 
   Send(i, strReturn.c_str(), strReturn.length()); 
   //cout << "sent 200 OK" << endl;

   return 1;

}


int ExperientTCP_SERVER_Port::fConnectionsActive()
{
 // this list shall not grow/shrink after program start!!!!!
 // or bad things could happen ............

 int icount = 0;

 for (unsigned int i = 1; i < vConnectionList.size(); i++)
  {
   if (vConnectionList[i].boolConnected) {icount++;}
  }
 return icount;

}




void ExperientTCP_SERVER_Port::fCheckConnections()
{
 string  strConnection;
 int     icounter = 0;
 cout << "tracked connections = " << fConnectionsActive() << endl;
 cout << "maximum connections = " << vConnectionList.size() << endl;
 for (unsigned int i = 1; i< vConnectionList.size(); i++)
  {
   if (!vConnectionList[i].boolConnected) {continue;}

   icounter++;

   strConnection = GetConnectionId(i);

   if (!strConnection.empty()) 
    {
     cout << icounter << " " << GetConnectionId(i) << " ";
     cout << GetRemoteHost(i) << " ";
     cout << "connected = " ;
     if (GetConnected(i)) {cout << "true" << endl;} 
     else                 {cout << "false" << endl;}  
    }
   else                   {cout << i << " - not connected" << endl;}
  }


}

void ExperientTCP_SERVER_Port::fInitializeConnectionActiveVector(int inumConnections) 
{

 vConnectionList.clear();

 vConnectionList.resize(inumConnections+1);

 for (int i = 0; i <= (inumConnections); i++)
  {
   vConnectionList[i].boolConnected           			= false;
   vConnectionList[i].tstimeConnected.tv_sec  	= 0;
   vConnectionList[i].tstimeConnected.tv_nsec 	= 0;
  } 



}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fSet_ACK_Received()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function sets  ExperientCommPort::boolACKReceived to true
*          
*    
* Parameters:
*   none                                                           
*
* Variables:
*  boolACKReceived                    Local - <ExperientCommPort> boolean
*                                                                        
* Functions:
*  none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fSet_ACK_Received()                {boolACKReceived = true;}

/****************************************************************************************************
*
* Name:
*  Function: bool ExperientCommPort::fCheck_for_ACK()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function returns the value of ExperientCommPort::boolACKReceived.
*          
*    
* Parameters:
*   none                                                           
*
* Variables:
*  boolACKReceived                    Local - <ExperientCommPort> boolean
*                                                                        
* Functions:
*  none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
bool ExperientCommPort::fCheck_for_ACK()                   {return boolACKReceived;}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fSetPortActive()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function sets the port flags to active
*          
*    
* Parameters:
*   none                                                           
*
* Variables:
*  boolPortActive                    Local - <ExperientCommPort> boolean
*  boolPortActive                    Local - <ExperientUDPPort> boolean
*  boolUnexpectedDataShowMessage     Local - <ExperientCommPort> boolean 
*  UDP_Port                          Local - <ExperientUDPPort> object
*
* Functions:
*  none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fSetPortActive()
{
 boolPortActive                = true;

 boolUnexpectedDataShowMessage = true;
 switch (ePortConnectionType)  {
   case UDP:  UDP_Port.boolPortActive  = true; break;
   case TCP:  TCP_Port.boolPortActive = true; break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fSetPortActive() No Connection Type Defined");
        return;
 }
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fSetPortInactive()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function sets the port flags to inactive
*          
*    
* Parameters:
*   none                                                           
*
* Variables:
*  boolOutsideIPTraffic              Local - <ExperientUDPPort> boolean
*  boolPortActive                    Local - <ExperientCommPort> boolean
*  boolPortActive                    Local - <ExperientUDPPort> boolean
*  boolUnexpectedDataShowMessage     Local - <ExperientCommPort> boolean 
*  UDP_Port                          Local - <ExperientUDPPort> object
*
* Functions:
*  none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fSetPortInactive()
{
 boolPortActive                = false;
 boolUnexpectedDataShowMessage = false;
 boolHeartbeatActive           = false;
 intBidsActive                 = 0;

 switch (ePortConnectionType)  {
   case UDP:
       UDP_Port.boolPortActive       = false;
       UDP_Port.boolOutsideIPTraffic = false;
       break;
   case TCP:
        TCP_Port.boolPortActive = false;
        break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fSetPortInactive() No Connection Type Defined");
        return;
 }
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fEmergencyPortShutDown(string stringArg, unsigned long long int intArg)
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is called to shut down a port if it's buffer got too large.  Therefore only
*   ports utilizing buffers are included in the case switch.  The time of shutdown is recorded and a boolean
*   is set. Reduced/missing heartbeat mode is also set. An Alarm is sent if the port had not been previously shutdown.
*          
*    
* Parameters:
*   stringArg                                <cstring>               Alarm message passed in
*   intArg                                   unsigned long long int  size of the buffer                                                        
*
* Variables:
*  ALI                                        Global  - <threadorPorttype>                    enumeration member
*  ANI                                        Global  - <threadorPorttype>                    enumeration member
*  CAD				              Global  - <threadorPorttype>                    enumeration member
*  boolEmergencyShutdown                      Local   - <ExperientCommPort> boolean
*  boolReducedHeartBeatModeFirstAlarm         Local   - <ExperientCommPort> boolean
*  CLOCK_REALTIME                             Library - <ctime>                               constant
*  enumPortType                               Local   - <ExperientCommPort><threadorPorttype> enumeration member
*  int_PORT_COOL_DOWN_PERIOD_SEC              Global  - <defines.h>                           how long to keep port shut down in seconds
*  intMessageCode                             Local   - integer                               Message Number Code
*  KRN_MESSAGE_122                            Global  - <defines.h>                           error message
*  LOG_ALARM                                  Global  - <defines.h>                           log type code
*  objBLANK_CALL_RECORD                       Global  - extern <Call_Data>                    blank call record
*  objMessage                                 Local   - <MessageClass>                        Messaging object
*  stringSideAorB                             Local   - <cstring>                             Used to identify ALI ports
*  timespecTimeEmergencyShutDown              Local   - <ExperientCommPort><ctime>            struct timespec time hack
*  UDP_Port                                   Local   - <ExperientUDPPort>                    object
*
* Functions:
*  clock_gettime()                            Library - <ctime>                       (int)
*  enQueue_Message()                          Global  - <globalfunctions.h>           (void)
*  .fMessage_Create()                         Local   - <MessageClass>                (void)
*  fSetPortInactive()                         Local   - <ExperientCommPort>           (void)
*  fSetReducedHeartbeatMode()                 Local   - <ExperientCommPort>           (void)
*  fSetHeartbeatMissingMode()                 Local   - <ExperientCommPort>           (void) 
*  fPortData()                                Local   - <ExperientUDPPort>            (Port_Data)
*  int2strLZ()                                Global  - <globalfunctions.h>           (string)  
*  seconds2time()                             Global  - <globalfunctions.h>           (string)

* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fEmergencyPortShutDown(Call_Data objCallData, string stringArg, unsigned long long int intArg)
{
 MessageClass           objMessage;
 int                    intMessageCode;
 string                 stringSideAorB;
 Port_Data              objPortData;

 switch(ePortConnectionType)  {
   case UDP: 
        objPortData = UDP_Port.fPortData(); break;
   case TCP: 
        objPortData = TCP_Port.fPortData(); 
      //  TCP_Port.SetAcceptData(false);
       // TCP_Port.Disconnect();        
        break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fEmergencyPortShutDown() No Connection Type Defined");
        return;
 }


 fSetPortInactive();
 clock_gettime(CLOCK_REALTIME, &timespecTimeEmergencyShutDown);

 switch(enumPortType)
  {
   case ALI:
        intMessageCode = 236;
        fSetReducedHeartbeatMode(objCallData, timespecTimeEmergencyShutDown);
        boolReducedHeartBeatModeFirstAlarm = true;     
        break;
   case ANI:
        intMessageCode = 328;
        fSetHeartbeatMissingMode();
        boolReducedHeartBeatModeFirstAlarm = true;
        break;
   case CAD:
        intMessageCode = 433;
        if (boolHeartbeatRequired) 
         {
          fSetReducedHeartbeatMode(objCallData,timespecTimeEmergencyShutDown);
          boolReducedHeartBeatModeFirstAlarm = true;
         }
        break;
   default:
        // not applicable would be an error if got here.... TBC
        SendCodingError( "CommPortClasses.cpp - Coding error in fn ExperientCommPort::fEmergencyPortShutDown()"); return;
  }// end switch
 
 if (!boolEmergencyShutdown)
  { 
   objMessage.fMessage_Create(LOG_ALARM, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA, objPortData, 
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_122, seconds2time(int_PORT_COOL_DOWN_PERIOD_SEC), stringArg, int2strLZ(intArg));
   enQueue_Message(enumPortType,objMessage);
  }

 boolEmergencyShutdown = true;

// UDP_Port.SetAcceptData(false);

}

/****************************************************************************************************
*
* Name:
*  Function: bool ExperientCommPort::fPortRestart()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is called to restart a port after it had been shut down. Note this function only affects
*   ports utilizing buffers. ALI, ANI & CAD are included in the case switch.
*
*   Note: when a port is "shut down" only the listen function is turned off.  The port will always attempt to
*   transmit.
*
*   1. First turn listening back "ON" if it fails return false.
*   2. Reset Port Active flags return true. 
*          
*    
* Parameters:
*  none                                                    
*
* Variables:
*  ALI                                        Global  - <threadorPorttype>                    enumeration member
*  ANI                                        Global  - <threadorPorttype>                    enumeration member
*  CAD				              Global  - <threadorPorttype>                    enumeration member
*  boolEmergencyShutdown                      Local   - <ExperientCommPort> boolean
*  enumPortType                               Local   - <ExperientCommPort><threadorPorttype> enumeration member
*  intUnexpectedDataCharacterCount            Local   - <ExperientCommPort> ull integer
*  intMessageCode                             Local   - integer                               Message Number Code
*  KRN_MESSAGE_123                            Global  - <defines.h>                           error message
*  KRN_MESSAGE_124                            Global  - <defines.h>                           error message
*  LOG_WARNING                                Global  - <defines.h>                           log type code
*  objBLANK_CALL_RECORD                       Global  - extern <Call_Data>                    blank call record
*  objMessage                                 Local   - <MessageClass>                        Messaging object
*  UDP_Port                                   Local  - <ExperientUDPPort>                     object
*
* Functions:
*  enQueue_Message()                          Global  - <globalfunctions.h>           (void)
*  .fMessage_Create()                         Local   - <MessageClass>                (void)
*  fSetPortActive()                           Local   - <ExperientCommPort>           (void)
*  fPortData()                                Local   - <ExperientUDPPort>            (Port_Data)
*  int2strLZ()                                Global  - <globalfunctions.h>           (string)  
*  .SetAcceptData()                           Library - <Ipworks><MCast>              (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
bool ExperientCommPort::fPortRestart()
{
 MessageClass           objMessage;
 int                    intMessageCode;
 int                    intRC = 0;
 Port_Data              objPortData;


 switch (ePortConnectionType)  {
   case UDP: intRC = UDP_Port.SetAcceptData(true); objPortData = UDP_Port.fPortData();break;
   case TCP:
             intRC = TCP_Port.SetAcceptData(true); objPortData = TCP_Port.fPortData();                     //IPDAEMON
             break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fPortRestart() No Connection Type Defined");
        return false;          
 }

 if (intRC)
  {
   switch(enumPortType)
   {
    case ALI:
         intMessageCode = 234;
        break;
    case ANI:
         intMessageCode = 329;
        break;
    case CAD:
         intMessageCode = 434;
        break;
    case WRK:
         intMessageCode = 734;
        break;
    default:
        SendCodingError("CommPortClasses.cpp - Coding Error in fn ExperientCommPort::fPortRestart()-1"); return false;
   }// end switch

   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_123, int2strLZ(intRC));
   enQueue_Message(enumPortType,objMessage);
   return false;
  }// end if (intRC) 

 //fClearReducedHeartbeatMode();
 boolEmergencyShutdown = false;
 intUnexpectedDataCharacterCount = 0;
  switch(enumPortType)
   {
    case ALI:
         intMessageCode = 235;
         break;
    case ANI:
         intMessageCode = 331;
          fSetPortActive();        
         break;
    case CAD:
         intMessageCode = 435;     
         break;
    case WRK:
         intMessageCode = 735;
         break;
    default:
        SendCodingError("CommPortClasses.cpp - Coding Error in fn ExperientCommPort::fPortRestart()-2"); return false;
   }// end switch

 

 objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, KRN_MESSAGE_124);
 enQueue_Message(enumPortType,objMessage);
 return true;
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::Check_Port_Restart()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to restart a port after a cool off period. 
*
*   Note: when a port is "shut down" only the listen function is turned off.  The port will always attempt to
*   transmit.
*
*   1. check if port was shut down. if not return
*   2. check if cool off period elapsed
*   3. attempt restart 
*          
*    
* Parameters:
*  none                                                    
*
* Variables:
*  boolEmergencyShutdown                      Local   - <ExperientCommPort> boolean
*  CLOCK_REALTIME                             Library - <ctime>                               constant
*  int_PORT_COOL_DOWN_PERIOD_SEC              Global  - <defines.h>
*  ldoubleTimeDiff                            Local   - long double                           time difference
*  objMessage                                 Local   - <MessageClass>                        Messaging object
*  timespecTimeEmergencyShutDown              Local   - <ExperientCommPort><ctime>            struct timespec time hack
*  timespecTimeNow                            Local   - <ctime> <timespec>                    time now
*  UDP_Port                                   Local   - <ExperientUDPPort>                      object
*
* Functions:
*  clock_gettime()                            Library - <ctime>                       (int)
*  fPortRestart()                             Local   - <ExperientCommPort>           (bool)
*  time_difference()                          Global  - <globalfunctions.h>           (long double)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::Check_Port_Restart()
{
 MessageClass           objMessage;
 long double            ldoubleTimeDiff;
 struct timespec        timespecTimeNow;
 
 if (!boolEmergencyShutdown){return;}
 
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);  
 ldoubleTimeDiff = time_difference(timespecTimeNow, timespecTimeEmergencyShutDown);
     
 if ( ldoubleTimeDiff > int_PORT_COOL_DOWN_PERIOD_SEC) {fPortRestart();}     
     
}// end Check_Port_Restart()

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fSetHeartbeatMissingMode()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to set Heartbeat Missing mode for an ANI port.  The Port IP address will be pinged while
*   the heart beat missing Mode boolean will be set and a time hack taken.        
*    
* Parameters:
*  none                                                    
*
* Variables:
*  boolReducedHeartBeatMode                   Local   - <ExperientCommPort> boolean
*  timespecReducedHeartBeatMode               Local   - <ExperientCommPort><ctime>            struct timespec time missing mode began
*  timespecTimeLastHeartbeat                  Local   - <ExperientCommPort><ctime>            struct timespec time of last heartbeat
*  UDP_Port                                   Local   - <ExperientUDPPort>                    object
*
* Functions:
*  .fPingStatus()                             Local   - <ExperientUDPPort>              (bool)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fSetHeartbeatMissingMode()
{
 timespecReducedHeartBeatMode = timespecTimeLastHeartbeat;
 boolReducedHeartBeatMode     = true;
 // Ping IP address and record status
 UDP_Port.fPingStatus();
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fClearHeartbeatMissingMode()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to clear Heartbeat Missing mode for an ANI port. 
*    
* Parameters:
*  none                                                    
*
* Variables:
*  boolReducedHeartBeatMode                   Local   - <ExperientCommPort> boolean
*  boolReducedHeartBeatModeFirstAlarm         Local   - <ExperientCommPort> boolean
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fClearHeartbeatMissingMode() 
{
  boolReducedHeartBeatMode     		= false;
  boolReducedHeartBeatModeFirstAlarm 	= false;
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fSetReducedHeartbeatMode(struct timespec timespecTimeNow)
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to set Reduced Heartbeat mode for a port. 
*    
* Parameters:
*  timespecTimeNow                            <ctime> <timespec> structure                                                    
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  boolPortActive                             Local   - <ExperientCommPort> boolean                          Port is Listening for UDP Packets           
*  boolReducedHeartBeatMode                   Local   - <ExperientCommPort> boolean
*  boolReducedHeartBeatModeFirstAlarm         Local   - <ExperientCommPort> boolean
*  enumPortType                               Local   - <ExperientCommPort> <threadorPorttype> enumeration     
*  CAD                                        Global  - <threadorPorttype> enumeration member
*  intHeartbeatInterval                       Local   - <ExperientCommPort> integer                          Port Heartbeat interval assigned
*  intREDUCED_ALI_HEARTBEAT_INTERVAL_SEC      Global  - <defines.h>
*  intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC      Global  - <defines.h>
*  timespecReducedHeartBeatMode               Local   - <ExperientCommPort><ctime><timespec> structure       time reduced HB mode began
*  UDP_Port                                   Local   - <ExperientUDPPort> object
*
* Functions:
*  .fPingStatus()                             Local   - <ExperientUDPPort>              (bool) 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fSetReducedHeartbeatMode(Call_Data objCallData, struct timespec timespecTimeNow)
{
 MessageClass                   objMessage;
 Port_Data                      PortData;

 boolReducedHeartBeatMode 	= true;
 boolPortActive                 = false;
 timespecReducedHeartBeatMode   = timespecTimeNow;
 
 switch (ePortConnectionType)  {
   case UDP: UDP_Port.fPingStatus(); PortData = UDP_Port.fPortData(); break;
   case TCP: TCP_Port.fPingStatus(); PortData = TCP_Port.fPortData(); break;
   default:
        SendCodingError("CommPortClasses.cpp - coding error ExperientCommPort::fSetReducedHeartbeatMode() No Connection Type Defined");
        return;
 }

 
 switch (enumPortType)
  {
   case CAD:
        intHeartbeatInterval 		= intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC;
        objMessage.fMessage_Create(LOG_WARNING, 408, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                   PortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                   CAD_MESSAGE_408, seconds2time(intHeartbeatInterval), UDP_Port.fPingStatusString() );
        enQueue_Message(CAD,objMessage);
        break;
   case ALI:
        intHeartbeatInterval 		= intREDUCED_ALI_HEARTBEAT_INTERVAL_SEC;
        objMessage.fMessage_Create(LOG_WARNING, 208, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                   PortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                   ALI_MESSAGE_208, seconds2time(intHeartbeatInterval), UDP_Port.fPingStatusString() );
        enQueue_Message(ALI,objMessage);       
        break;

   default:
        SendCodingError("CommPortClasses.cpp - Reduced Heartbeat Interval set manually in  ExperientCommPort::fSetReducedHeartbeatMode()");
        intHeartbeatInterval 		= 10;
  }// end switch


 

    
}// end ExperientCommPort::fSetReducedHeartbeatMode

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fClearReducedHeartbeatMode(bool boolSupressMessage)
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to clear Reduced Heartbeat mode for a ports utilizing heartbeats w/ ACKs and NAKs. 
*    
* Parameters:
*  boolSupressMessage                         boolean  (if true supress messaging)                                                    
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  boolNAKReceived                            Local   - <ExperientCommPort> boolean                          Port is Listening for UDP Packets
*  boolPingStatus                             Local   - <ExperientCommPort> boolean                          Last Ping Result     
*  boolReducedHeartBeatMode                   Local   - <ExperientCommPort> boolean                          port is in reduced hertbeat interval
*  boolReducedHeartBeatModeFirstAlarm         Local   - <ExperientCommPort> boolean                          true if Alarm was sent
*  enumPortType                               Local   - <ExperientCommPort> <threadorPorttype> enumeration     
*  CAD                                        Global  - <threadorPorttype> enumeration member
*  EX_SOFTWARE                                Library - <sysexits.h>                                         Exit Code
*  intHeartbeatInterval                       Local   - <ExperientCommPort> integer                          Port Heartbeat interval assigned
*  intMessageCode                             Local   - integer                                              message number
*  intALI_HEARTBEAT_INTERVAL_SEC              Global  - <defines.h>
*  intALI_PORT_DOWN_REMINDER_SEC              Global  - <defines.h>
*  intCAD_HEARTBEAT_INTERVAL_SEC              Global  - <defines.h>
*  intCAD_PORT_DOWN_REMINDER_SEC              Global  - <defines.h>
*  LOG_ALARM                                  Global  - <defines.h>                                          Log Code
*  LOG_MESSAGE_504                            Global  - <defines.h>                                          Log Message Template
*  objBLANK_CALL_RECORD                       Global  - <Call_Data> object                                   Blank Call Data
*  objMessage                                 Local   - <MessageClass> object
*  UDP_Port                                   Local   - <ExperientUDPPort> object
*
* Functions:
*  Abnormal_Exit()                             Global  - <globalfunctions.h>         (void)
*  enQueue_Message()                           Global  - <globalfunctions.h>         (void)
*  .fMessage_Create()                          Local   - <MessageClass>              (void) 
*  .fPortData()                                Local   - <ExperientUDPPort>          (Port_Data)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fClearReducedHeartbeatMode(bool boolSupressMessage)
{
 MessageClass           objMessage;
 int                    intMessageCode = 0;
 Port_Data              objPortData = UDP_Port.fPortData();

 boolReducedHeartBeatMode                 = false;
 boolNAKReceived                          = false;
 UDP_Port.boolPingStatus                  = false;
 
 switch (enumPortType)
  {
   case CAD:
    if (!boolHeartbeatRequired)              {return;}
    intPortDownReminderThreshold             = intCAD_PORT_DOWN_REMINDER_SEC;
    intHeartbeatInterval                     = intCAD_HEARTBEAT_INTERVAL_SEC;
    intMessageCode                           = 464;
    break;
   case ALI:
    intPortDownReminderThreshold             = intALI_PORT_DOWN_REMINDER_SEC;
    intHeartbeatInterval                     = intALI_HEARTBEAT_INTERVAL_SEC;
    intMessageCode                           = 227;
    if (ePortConnectionType == TCP)         {objPortData = TCP_Port.fPortData();}
    break;
   default:
    //ERROR
    Abnormal_Exit(enumPortType, EX_SOFTWARE, "*CODING ERROR IN ExperientCommPort::fClearReducedHeartbeatMode()" );
   }//end switch
 
  if((boolReducedHeartBeatModeFirstAlarm)&&(!boolSupressMessage))
   {
    objMessage.fMessage_Create(LOG_ALARM, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, LOG_MESSAGE_504);
    enQueue_Message(enumPortType,objMessage);
   }
  boolReducedHeartBeatModeFirstAlarm       = false;
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fIncrementNAKCounter()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to count consecutive NAKs on an ALI port IAW NENA specifications.  If
*   the number gets too large it rolls over and a message is generated. 
*    
* Parameters:
*  none                                                                               
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  enumPortType                               Local   - <ExperientCommPort> <threadorPorttype> enumeration     
*  CAD                                        Global  - <threadorPorttype> enumeration member
*  intConsecutiveNAKCounter                   Local   - <ExperientCommPort> unsigned long long int           number of consecutive NAKs on Port
*  intMessageCode                             Local   - integer                                              Message number
*  LOG_WARNING                                Global  - <defines.h>                                          Log Code
*  LOG_MESSAGE_524                            Global  - <defines.h>                                          Log Message Template 
*  objBLANK_CALL_RECORD                       Global  - <Call_Data> object                                   Blank Call Data                                        
*  objMessage                                 Local   - <MessageClass> object
*  UDP_Port                                   Local   - <ExperientUDPPort> object
*  ULONG_LONG_MAX                             Library - <limits.h>                                           value of largest unsigned long long integer
*
* Functions:
*  enQueue_Message()                           Global  - <globalfunctions.h>         (void)
*  .fMessage_Create()                          Local   - <MessageClass>              (void) 
*  .fPortData()                                Local   - <ExperientUDPPort>          (Port_Data)
*  int2strLZ()                                 Global  - <globalfunctions.h>         (string)
*  Thread_Calling()                            Global  - <globalfunctions.h>         (string)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fIncrementNAKCounter()
{
 MessageClass           objMessage;
 int                    intMessageCode;
 
 if (intConsecutiveNAKCounter == ULONG_LONG_MAX )
  {
   switch(enumPortType)
    {
     case ALI:
          intMessageCode = 242; break;
     case CAD:
          intMessageCode = 467; break;   // should never get this ...... but leaving here just in case things change later (network comm's vs serial)
     default:
          SendCodingError( "CommPortClasses.cpp - Coding error in ExperientCommPort::fIncrementNAKCounter() " + Thread_Calling(enumPortType));
          return;
    }
   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_524,  "NAK Counter", int2strLZ(intConsecutiveNAKCounter));
   enQueue_Message(enumPortType,objMessage);
   intConsecutiveNAKCounter = 0;
   return;
  }
 else {++intConsecutiveNAKCounter;}
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fIncrementStrayACKCounter()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to count stray ACKs on Ports that utilize them.  A stray ACK is defined
*   as an ACK that was recieved in the absence of a transmission that requires acknowledgement.
*   If the number gets too large, it rolls over and a message is generated. 
*    
* Parameters:
*  none                                                                               
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  enumPortType                               Local   - <ExperientCommPort> <threadorPorttype> enumeration     
*  CAD                                        Global  - <threadorPorttype> enumeration member
*  intStrayACKCounter                         Local   - <ExperientCommPort> unsigned long long int           number of stray ACKs on Port
*  intMessageCode                             Local   - integer                                              Message number
*  LOG_WARNING                                Global  - <defines.h>                                          Log Code
*  LOG_MESSAGE_524                            Global  - <defines.h>                                          Log Message Template 
*  objBLANK_CALL_RECORD                       Global  - <Call_Data> object                                   Blank Call Data                                        
*  objMessage                                 Local   - <MessageClass> object
*  UDP_Port                                   Local   - <ExperientUDPPort> object
*  ULONG_LONG_MAX                             Library - <limits.h>                                           value of largest unsigned long long integer
*
* Functions:
*  enQueue_Message()                           Global  - <globalfunctions.h>         (void)
*  .fMessage_Create()                          Local   - <MessageClass>              (void) 
*  .fPortData()                                Local   - <ExperientUDPPort>          (Port_Data)
*  int2strLZ()                                 Global  - <globalfunctions.h>         (string)
*  Thread_Calling()                            Global  - <globalfunctions.h>         (string)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fIncrementStrayACKCounter()
{
 MessageClass           objMessage;
 int                    intMessageCode;

 if (intStrayACKCounter == ULONG_LONG_MAX ) 
  {
   switch(enumPortType)
    {
     case ALI:
          intMessageCode = 243; break;
     case CAD:
          intMessageCode = 468; break;    // should never get this ...... but leaving here just in case things change later (network comm's vs serial)
     default:
          SendCodingError("CommPortClasses.cpp - Coding error in ExperientCommPort::fIncrementStrayACKCounter() " + Thread_Calling(enumPortType));
          return;
    }
   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_524, "Stray ACK Counter", int2strLZ(intStrayACKCounter));
   enQueue_Message(enumPortType,objMessage);
   intStrayACKCounter = 0;
   return;
  }
 else {++intStrayACKCounter;}
}
/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fLoad_Vendor_Email_Address(string stringArg)
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to load the value of a Port's Vendor Email address
*   a boolean is set if it is not the default value.  The email is used for notification
*   of port failures.
*    
* Parameters:
*  stringArg                                  <cstring>  Vendor email address                                                                               
*
* Variables:
*  boolVendorEmailAddress                     Local   - <ExperientUDPPort> boolean                           true if not the defualt email address
*  DEFAULT_VALUE_KRN_INI_KEY_13               Global  - <defines.h>                                          default ini email address
*  strVendorEmailAddress                      Local   - <ExperientUDPPort> <cstring> member                  the vendor email address
*  UDP_Port                                   Local   - <ExperientUDPPort> object
*
* Functions:
*  none
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fLoad_Vendor_Email_Address(string stringArg) {

 switch (this->ePortConnectionType) {
  case UDP:
   if (stringArg == DEFAULT_VALUE_KRN_INI_KEY_13) {UDP_Port.boolVendorEmailAddress = false;}
   else						  {UDP_Port.boolVendorEmailAddress = true; }
   UDP_Port.strVendorEmailAddress = stringArg;
   break;
  case TCP:
   if (stringArg == DEFAULT_VALUE_KRN_INI_KEY_13) {TCP_Port.boolVendorEmailAddress = false;}
   else						  {TCP_Port.boolVendorEmailAddress = true; }
   TCP_Port.strVendorEmailAddress = stringArg;
   break;
  default:
   break;
 }
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientCommPort::fIncrementConsecutiveBadRecordCounter()
*
* Description:
*   A ExperientCommPort Function 
*
*
* Details:
*   This function is utilized to count reception of consecutive "Bad" Records.  If the number gets too
*   large it will roll over and a message will be generated. If a threshold is exceeded then
*   the Port will be shut down for a cool off period. 
*   note: if after the cool off period the next record is "bad" the port will shut down again.  To
*         break the cycle a good record must be receieved.
*    
* Parameters:
*  none                                                                               
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  enumPortType                               Local   - <ExperientCommPort> <threadorPorttype> enumeration     
*  CAD                                        Global  - <threadorPorttype> enumeration member
*  intConsecutiveBadRecordCounter             Local   - <ExperientCommPort> unsigned long long int           number of consecutive Bad Records on Port
*  intMAX_CONSECUTIVE_BAD_ALI_RECORDS         Global  - <defines.h>                                          ALI threshold
*  intCAD_MAX_CONSECUTIVE_LONG_RESPONSES      Global  - <defines.h>                                          CAD threshold
*  intMessageCode                             Local   - integer                                              Message number
*  intThreshold                               Local   - unsigned long long int                               threshold to be checked
*  LOG_WARNING                                Global  - <defines.h>                                          Log Code
*  LOG_MESSAGE_524                            Global  - <defines.h>                                          Log Message Template 
*  objBLANK_CALL_RECORD                       Global  - <Call_Data> object                                   Blank Call Data                                        
*  objMessage                                 Local   - <MessageClass> object
*  stringErrorMsg                             Local   - <cstring>                                            error message to be logged
*  UDP_Port                                   Local   - <ExperientUDPPort> object
*  ULONG_LONG_MAX                             Library - <limits.h>                                           value of largest unsigned long long integer
*
* Functions:
*  enQueue_Message()                           Global  - <globalfunctions.h>         (void)
*  fEmergencyPortShutDown()                    Local   - <ExperientCommPort>         (void)
*  .fMessage_Create()                          Local   - <MessageClass>              (void) 
*  .fPortData()                                Local   - <ExperientUDPPort>          (Port_Data)
*  int2strLZ()                                 Global  - <globalfunctions.h>         (string)
*  Thread_Calling()                            Global  - <globalfunctions.h>         (string)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientCommPort::fIncrementConsecutiveBadRecordCounter()
{
 MessageClass           objMessage;
 int                    intMessageCode;
 unsigned long long int intThreshold;
 string                 stringErrorMsg;

  switch(enumPortType)
    {
     case ALI:
          intMessageCode = 249; intThreshold = intMAX_CONSECUTIVE_BAD_ALI_RECORDS; stringErrorMsg = "Consecutive Invalid ALI Records = "; break;
     case CAD:
          intMessageCode = 471; intThreshold = intCAD_MAX_CONSECUTIVE_LONG_RESPONSES; stringErrorMsg = "Consecutive LONG Responses = ";break;    
     default:
          SendCodingError("CommPortClasses.cpp - Coding error in ExperientCommPort::fIncrementBadRecordCounter() " + Thread_Calling(enumPortType));
          return;
    }

 if (intConsecutiveBadRecordCounter == ULONG_LONG_MAX ) 
  {
  
   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_524, "Bad Record Counter", int2strLZ(intConsecutiveBadRecordCounter));
   enQueue_Message(enumPortType,objMessage);
   intConsecutiveBadRecordCounter = 0;
   return;
  }
 else 
 {
  ++intConsecutiveBadRecordCounter;
  if (intConsecutiveBadRecordCounter >= intThreshold)
   {
    fEmergencyPortShutDown(objBLANK_CALL_RECORD,stringErrorMsg, intConsecutiveBadRecordCounter);
    return;
   }
 
 }
 return;
}

string ExperientCommPort::fDisplayPostionsUsingPort() {
    
 string strReturn ="";   
 for (int i=1; i<= intNUM_WRK_STATIONS; i++) {
  if (this->fPostionUsesPort(i))  {
      strReturn += int2str(i);
      strReturn += " ";
  }
 }
 
 if (strReturn.empty()) {
     strReturn = "ALL";
 }
 return strReturn;
}



void ExperientCommPort::fIncrementBadCharacterCount(unsigned long long int iCount)
{
 int            intMessageCode;
 MessageClass   objMessage;

  switch(enumPortType)
    {
     case ALI:
          intMessageCode = 249; break;
     case CAD:
          intMessageCode = 471; break;    
     default:
          SendCodingError( "CommPortClasses.cpp - Coding error in ExperientCommPort::fIncrementBadCharacterCount() " + Thread_Calling(enumPortType));
          return;
    }


 if ( (ULONG_LONG_MAX - iCount) < (intUnexpectedDataCharacterCount ) )
  {
  
   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              UDP_Port.fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_524, "Unexpected Character Counter", int2strLZ(intConsecutiveBadRecordCounter));
   enQueue_Message(enumPortType,objMessage);
   intUnexpectedDataCharacterCount = 0;
   return;
  }
 else 
  {
   intUnexpectedDataCharacterCount+= iCount;
  }


}


//constructor
PORT_INIT_VARS::PORT_INIT_VARS() {

 intPORT_NUMBER			= 0;
 intHEARTBEAT_INTERVAL_SEC 	= 0;
 intPORT_DOWN_THRESHOLD_SEC 	= 0;
 intPORT_DOWN_REMINDER_SEC	= 0;
 intREMOTE_PORT_NUMBER		= 0;
 intLOCAL_PORT_NUMBER           = 0;
 REMOTE_IP_ADDRESS.fClear();
 strNotes.clear();
 boolPhantomPort		= false;
 strVendorEmailAddress.clear();
 ePortConnectionType            = NOT_DEF;
 eALIportProtocol               = NO_ALI_PROTO;
 eRequestKeyFormat              = NO_FORMAT;

 WorkstationGroup.fClear();
 PositionAliases.fClear();
 boolNENA_CADheader              = true;
 boolCAD_EraseFirstALI_CR        = false;
 boolHeartbeatRequired           = false;
 boolRecordACKRequired           = false;
 boolHeartBeatACKrequired        = false;
 boolSendCADEraseMsg             = false;


}

//copy constructor
PORT_INIT_VARS::PORT_INIT_VARS(const PORT_INIT_VARS *a) {

 this->intPORT_NUMBER			= a->intPORT_NUMBER;
 this->intHEARTBEAT_INTERVAL_SEC 	= a->intHEARTBEAT_INTERVAL_SEC;
 this->intPORT_DOWN_THRESHOLD_SEC 	= a->intPORT_DOWN_THRESHOLD_SEC;
 this->intPORT_DOWN_REMINDER_SEC	= a->intPORT_DOWN_REMINDER_SEC;
 this->intREMOTE_PORT_NUMBER		= a->intREMOTE_PORT_NUMBER;
 this->intLOCAL_PORT_NUMBER             = a->intLOCAL_PORT_NUMBER;
 this->REMOTE_IP_ADDRESS		= a->REMOTE_IP_ADDRESS;
 this->strNotes				= a->strNotes;
 this->boolPhantomPort			= a->boolPhantomPort;
 this->strVendorEmailAddress		= a->strVendorEmailAddress;
 this->ePortConnectionType		= a->ePortConnectionType;
 this->eALIportProtocol			= a->eALIportProtocol;
 this->eRequestKeyFormat		= a->eRequestKeyFormat;
 this->WorkstationGroup                 = a->WorkstationGroup;
 this->PositionAliases                  = a->PositionAliases;
 this->boolNENA_CADheader               = a->boolNENA_CADheader;
 this->boolCAD_EraseFirstALI_CR         = a->boolCAD_EraseFirstALI_CR;
 this->boolHeartbeatRequired            = a->boolHeartbeatRequired;
 this->boolRecordACKRequired            = a->boolRecordACKRequired;
 this->boolHeartBeatACKrequired         = a->boolHeartBeatACKrequired;
 this->boolSendCADEraseMsg              = a->boolSendCADEraseMsg;                   
}

//assignment operator
PORT_INIT_VARS& PORT_INIT_VARS::operator=(const PORT_INIT_VARS& a) {
 if (&a == this ) {return *this;}

 this->intPORT_NUMBER			= a.intPORT_NUMBER;
 this->intHEARTBEAT_INTERVAL_SEC 	= a.intHEARTBEAT_INTERVAL_SEC;
 this->intPORT_DOWN_THRESHOLD_SEC 	= a.intPORT_DOWN_THRESHOLD_SEC;
 this->intPORT_DOWN_REMINDER_SEC	= a.intPORT_DOWN_REMINDER_SEC;
 this->intREMOTE_PORT_NUMBER		= a.intREMOTE_PORT_NUMBER;
 this->intLOCAL_PORT_NUMBER             = a.intLOCAL_PORT_NUMBER;
 this->REMOTE_IP_ADDRESS		= a.REMOTE_IP_ADDRESS;
 this->strNotes				= a.strNotes;
 this->boolPhantomPort			= a.boolPhantomPort;
 this->strVendorEmailAddress		= a.strVendorEmailAddress;
 this->ePortConnectionType		= a.ePortConnectionType;
 this->eALIportProtocol			= a.eALIportProtocol;
 this->eRequestKeyFormat		= a.eRequestKeyFormat;
  //CAD Specific
 this->WorkstationGroup                 = a.WorkstationGroup;
 this->PositionAliases                  = a.PositionAliases;
 this->boolNENA_CADheader               = a.boolNENA_CADheader;
 this->boolCAD_EraseFirstALI_CR         = a.boolCAD_EraseFirstALI_CR;
 this->boolHeartbeatRequired            = a.boolHeartbeatRequired;
 this->boolRecordACKRequired            = a.boolRecordACKRequired;
 this->boolHeartBeatACKrequired         = a.boolHeartBeatACKrequired;
 this->boolSendCADEraseMsg              = a.boolSendCADEraseMsg;
 return *this;
}

//Comparison
bool PORT_INIT_VARS::fCompare(const PORT_INIT_VARS& a) const {

 bool boolRunningCheck = true;

  boolRunningCheck = boolRunningCheck&&(this->intPORT_NUMBER 			== a.intPORT_NUMBER);
  boolRunningCheck = boolRunningCheck&&(this->intHEARTBEAT_INTERVAL_SEC 	== a.intHEARTBEAT_INTERVAL_SEC);
  boolRunningCheck = boolRunningCheck&&(this->intPORT_DOWN_THRESHOLD_SEC 	== a.intPORT_DOWN_THRESHOLD_SEC);
  boolRunningCheck = boolRunningCheck&&(this->intPORT_DOWN_REMINDER_SEC 	== a.intPORT_DOWN_REMINDER_SEC);
  boolRunningCheck = boolRunningCheck&&(this->intREMOTE_PORT_NUMBER 		== a.intREMOTE_PORT_NUMBER);
  boolRunningCheck = boolRunningCheck&&(this->intLOCAL_PORT_NUMBER 		== a.intLOCAL_PORT_NUMBER);
  boolRunningCheck = boolRunningCheck&&(this->REMOTE_IP_ADDRESS.fCompare(a.REMOTE_IP_ADDRESS));
  boolRunningCheck = boolRunningCheck&&(this->strNotes 				== a.strNotes);
  boolRunningCheck = boolRunningCheck&&(this->boolPhantomPort 			== a.boolPhantomPort);
  boolRunningCheck = boolRunningCheck&&(this->strVendorEmailAddress 		== a.strVendorEmailAddress);
  boolRunningCheck = boolRunningCheck&&(this->ePortConnectionType 		== a.ePortConnectionType);
  boolRunningCheck = boolRunningCheck&&(this->eALIportProtocol 			== a.eALIportProtocol);
  boolRunningCheck = boolRunningCheck&&(this->eRequestKeyFormat			== a.eRequestKeyFormat);
  //CAD Specific
  boolRunningCheck = boolRunningCheck&&(this->WorkstationGroup.fCompare(a.WorkstationGroup)); 
  boolRunningCheck = boolRunningCheck&&(this->PositionAliases.fCompare(a.PositionAliases));
  boolRunningCheck = boolRunningCheck&&(this->boolNENA_CADheader		== a.boolNENA_CADheader);
  boolRunningCheck = boolRunningCheck&&(this->boolCAD_EraseFirstALI_CR		== a.boolCAD_EraseFirstALI_CR);
  boolRunningCheck = boolRunningCheck&&(this->boolHeartbeatRequired		== a.boolHeartbeatRequired);
  boolRunningCheck = boolRunningCheck&&(this->boolRecordACKRequired		== a.boolRecordACKRequired);
  boolRunningCheck = boolRunningCheck&&(this->boolHeartBeatACKrequired		== a.boolHeartBeatACKrequired);
  boolRunningCheck = boolRunningCheck&&(this->boolSendCADEraseMsg		== a.boolSendCADEraseMsg);

  return boolRunningCheck;
}

void PORT_INIT_VARS::fClear() {

 intPORT_NUMBER			= 0;
 intHEARTBEAT_INTERVAL_SEC 	= 0;
 intPORT_DOWN_THRESHOLD_SEC 	= 0;
 intPORT_DOWN_REMINDER_SEC	= 0;
 intREMOTE_PORT_NUMBER		= 0;
 intLOCAL_PORT_NUMBER           = 0;
 REMOTE_IP_ADDRESS.fClear();
 strNotes.clear();
 boolPhantomPort		= false;
 strVendorEmailAddress.clear();
 ePortConnectionType            = NOT_DEF;
 eALIportProtocol               = NO_ALI_PROTO;
 eRequestKeyFormat              = NO_FORMAT;
 //CAD specific
 WorkstationGroup.fClear();
 PositionAliases.fClear();
 boolNENA_CADheader              = true;
 boolCAD_EraseFirstALI_CR        = false;
 boolHeartbeatRequired           = false;
 boolRecordACKRequired           = false;
 boolHeartBeatACKrequired        = false;
 boolSendCADEraseMsg             = false;

 
 return;
}

//constructor
TCP_PORT_INIT_VARS::TCP_PORT_INIT_VARS() {

 intPORT_NUMBER			= 0;
 intHEARTBEAT_INTERVAL_SEC 	= 0;
 intPORT_DOWN_THRESHOLD_SEC 	= 0;
 intPORT_DOWN_REMINDER_SEC	= 0;
 intREMOTE_PORT_NUMBER		= 0;
 REMOTE_IP_ADDRESS.fClear();
// strNotes.clear();
// boolPhantomPort		= false;
}

//copy constructor
TCP_PORT_INIT_VARS::TCP_PORT_INIT_VARS(const TCP_PORT_INIT_VARS *a) {

 this->intPORT_NUMBER			= a->intPORT_NUMBER;
 this->intHEARTBEAT_INTERVAL_SEC 	= a->intHEARTBEAT_INTERVAL_SEC;
 this->intPORT_DOWN_THRESHOLD_SEC 	= a->intPORT_DOWN_THRESHOLD_SEC;
 this->intPORT_DOWN_REMINDER_SEC	= a->intPORT_DOWN_REMINDER_SEC;
 this->intREMOTE_PORT_NUMBER		= a->intREMOTE_PORT_NUMBER;
 this->REMOTE_IP_ADDRESS		= a->REMOTE_IP_ADDRESS;
}

//assignment operator
TCP_PORT_INIT_VARS& TCP_PORT_INIT_VARS::operator=(const TCP_PORT_INIT_VARS& a) {
 if (&a == this ) {return *this;}

 this->intPORT_NUMBER			= a.intPORT_NUMBER;
 this->intHEARTBEAT_INTERVAL_SEC 	= a.intHEARTBEAT_INTERVAL_SEC;
 this->intPORT_DOWN_THRESHOLD_SEC 	= a.intPORT_DOWN_THRESHOLD_SEC;
 this->intPORT_DOWN_REMINDER_SEC	= a.intPORT_DOWN_REMINDER_SEC;
 this->intREMOTE_PORT_NUMBER		= a.intREMOTE_PORT_NUMBER;
 this->REMOTE_IP_ADDRESS		= a.REMOTE_IP_ADDRESS;

 return *this;
}

//Comparison
bool TCP_PORT_INIT_VARS::fCompare(const TCP_PORT_INIT_VARS& a) const {

 bool boolRunningCheck = true;

  boolRunningCheck = boolRunningCheck&&(this->intPORT_NUMBER 			== a.intPORT_NUMBER);
  boolRunningCheck = boolRunningCheck&&(this->intHEARTBEAT_INTERVAL_SEC 	== a.intHEARTBEAT_INTERVAL_SEC);
  boolRunningCheck = boolRunningCheck&&(this->intPORT_DOWN_THRESHOLD_SEC 	== a.intPORT_DOWN_THRESHOLD_SEC);
  boolRunningCheck = boolRunningCheck&&(this->intPORT_DOWN_REMINDER_SEC 	== a.intPORT_DOWN_REMINDER_SEC);
  boolRunningCheck = boolRunningCheck&&(this->intREMOTE_PORT_NUMBER 		== a.intREMOTE_PORT_NUMBER);
  boolRunningCheck = boolRunningCheck&&(this->REMOTE_IP_ADDRESS.fCompare(a.REMOTE_IP_ADDRESS));

  return boolRunningCheck;
}

void TCP_PORT_INIT_VARS::fClear() {

 intPORT_NUMBER			= 0;
 intHEARTBEAT_INTERVAL_SEC 	= 0;
 intPORT_DOWN_THRESHOLD_SEC 	= 0;
 intPORT_DOWN_REMINDER_SEC	= 0;
 intREMOTE_PORT_NUMBER		= 0;
 REMOTE_IP_ADDRESS.fClear();
 return;
}


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//                                                                          ExperientTCPPort FUNCTIONS
//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//assignment operator
ExperientTCPPort& ExperientTCPPort::operator=(const ExperientTCPPort& a)
{
  if (&a == this ) {return *this;}
  intPortNum				= a.intPortNum;
  intSideAorB				= a.intSideAorB;
  enumPortType				= a.enumPortType;
  boolBufferPreviouslyCleared		= a.boolBufferPreviouslyCleared;
  vBuffer				= a.vBuffer; 
  eRequestKeyFormat			= a.eRequestKeyFormat;
  strTCP_Buffer				= a.strTCP_Buffer;                    
  timespecBufferTimeStamp.tv_sec	= a.timespecBufferTimeStamp.tv_sec;
  timespecBufferTimeStamp.tv_nsec	= a.timespecBufferTimeStamp.tv_nsec;
  sem_tMutexTCPPortBuffer		= a.sem_tMutexTCPPortBuffer;
  boolOutsideIPTraffic			= a.boolOutsideIPTraffic;
  stringOutsideIPAddr			= a.stringOutsideIPAddr;
  timespecTimePortDown.tv_sec		= a.timespecTimePortDown.tv_sec;
  timespecTimePortDown.tv_nsec		= a.timespecTimePortDown.tv_nsec;
  boolPortAlarm				= a.boolPortAlarm;
  boolPortActive			= a.boolPortActive;
  boolPortWasDown			= a.boolPortWasDown;
  boolIntialConnect			= a.boolIntialConnect;
  intPortDownThresholdSec		= a.intPortDownThresholdSec;
  intPortDownReminderThreshold		= a.intPortDownReminderThreshold;
  boolPingStatus			= a.boolPingStatus;
  Remote_IP				= a.Remote_IP;
  intRemotePortNumber			= a.intRemotePortNumber;
  intLocalPortNumber			= a.intLocalPortNumber;
  intBadCharacterCount			= a.intBadCharacterCount;
  sem_tMutexBadCharacterCount		= a.sem_tMutexBadCharacterCount;
  strVendorEmailAddress			= a.strVendorEmailAddress;
  boolVendorEmailAddress		= a.boolVendorEmailAddress;
  objPortData				= a.objPortData;
  objCallData				= a.objCallData;
  boolReadyToSend			= a.boolReadyToSend;
  intIPDaemonConnectionID		= a.intIPDaemonConnectionID;
  objMessage				= a.objMessage;
  boolFreeswitchLoggedin		= a.boolFreeswitchLoggedin;
  ContentLength				= a.ContentLength;
  eALIportProtocol			= a.eALIportProtocol;
  intLastStatusCode                     = a.intLastStatusCode;
//boolRestartListenThread               = a.boolRestartListenThread;
  return *this;
}


void ExperientTCPPort::fInitializeALIPort(int i, int j)
{
// int                                    intRC;
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort               ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
 string                                 strKey = IPWORKS_RUNTIME_KEY;

 enumPortType                       = ALI;
 intPortNum                         = i;
 intSideAorB                        = j;
 intBadCharacterCount               = 0;
 boolPingStatus                     = false;
 vBuffer.reserve((intMAX_ALI_RECORD_SIZE * 100)+ intALI_COMM_IN_BUFFFER_SIZE + 1);
 vBuffer.clear();
 Config((char*)"InBufferSize=655360");
 Config((char*)"OutBufferSize=655360");
 SetLocalHost(NULL);
 fPortData();
 sem_init(&sem_tMutexTCPPortBuffer,0,1);
 sem_init(&sem_tMutexBadCharacterCount,0,1);

 switch (j)
  {
   case 1: case 2:
#ifdef IPWORKS_V16
    SetRuntimeLicense((char*)strKey.c_str());
#endif
    SetLocalPort(ALIPort[i] [j].intLocalPort);
    eRequestKeyFormat = ALIPort[i] [j].eRequestKeyFormat;
    eALIportProtocol  = ALIPort[i] [j].eALIportProtocol;
    SetRemoteHost((char*)ALIPort[i] [j].RemoteIPAddress.stringAddress.c_str());
    SetRemotePort(ALIPort[i] [j].intRemotePortNumber);
    Remote_IP.stringAddress  = ALIPort[i] [j].RemoteIPAddress.stringAddress;
    break;
   case 3:
#ifdef IPWORKS_V16
    SetRuntimeLicense((char*)strKey.c_str());
#endif
    SetLocalPort(ALIServicePort[i].intLocalPort);
    eRequestKeyFormat = ALIServicePort[i].eRequestKeyFormat;
    eALIportProtocol  = ALIServicePort[i].eALIportProtocol;
    SetRemoteHost((char*)ALIServicePort[i].RemoteIPAddress.stringAddress.c_str());
    SetRemotePort(ALIServicePort[i].intRemotePortNumber);
    Remote_IP.stringAddress  = ALIServicePort[i].RemoteIPAddress.stringAddress;
    break;
   default:
     SendCodingError("Coding error in fn ExperientTCPPort::fInitializeALIPort() 1 2 or 3 ");
  }    
 SetLinger(false);
 fSet_Port_Active();
}

void ExperientTCPPort::fInitializeDSBport() {
 extern int intDSB_PORT_DOWN_THRESHOLD_SEC;
 extern int intDSB_PORT_DOWN_REMINDER_SEC;

 string                             strKey = IPWORKS_RUNTIME_KEY;
 //cout << "INITIALIZING DSB PORT" << endl;
 enumPortType                       = DSB;
 intPortNum                         = 1;
 boolPingStatus                     = false;
 boolPortWasDown                    = false;
 intPortDownThresholdSec            = intDSB_PORT_DOWN_THRESHOLD_SEC;
 intPortDownReminderThreshold       = intDSB_PORT_DOWN_REMINDER_SEC;
#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
 Config((char*)"InBufferSize=655360");
 Config((char*)"OutBufferSize=655360");
 Config((char*)"KeepAliveRetryCount=5");
 Config((char*)"KeepAliveTime=5000");
 Config((char*)"KeepAliveInterval=1000");
 Config((char*)"AbsoluteTimeout=true");

 SetTimeout(4);
 SetLocalHost(NULL);
 SetLocalPort(intLocalPortNumber); 
 fPortData();
 sem_init(&sem_tMutexTCPPortBuffer,0,1);
 sem_init(&sem_tMutexBadCharacterCount,0,1);
 SetRemoteHost((char*)Remote_IP.stringAddress.c_str());
 SetRemotePort(intRemotePortNumber);
 SetLinger(false);
 fSet_Port_Active();
 clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);
}

void ExperientTCPPort::fInitializeRCCport(int i)
{
 string                                 strKey = IPWORKS_RUNTIME_KEY;

 enumPortType                       = RCC;
 intPortNum                         = i;
 boolPingStatus                     = false;
 boolPortWasDown                    = false;
 intPortDownThresholdSec            = RCC_PORT_TIMEOUT_INTERVAL_SEC;
 intPortDownReminderThreshold       = RCC_PORT_DOWN_REMINDER_INTERVAL_SEC;
#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
// Config((char*)"InBufferSize=655360");
// Config((char*)"OutBufferSize=655360");
 Config((char*)"KeepAliveRetryCount=5");
 Config((char*)"KeepAliveTime=5000");
 Config((char*)"KeepAliveInterval=1000");
 Config((char*)"AbsoluteTimeout=true");

 SetTimeout(4);
 SetLocalHost(NULL);
 SetLocalPort(intLocalPortNumber); 
 fPortData();
 sem_init(&sem_tMutexTCPPortBuffer,0,1);
 sem_init(&sem_tMutexBadCharacterCount,0,1);
 SetRemoteHost((char*)Remote_IP.stringAddress.c_str());
 SetRemotePort(intRemotePortNumber);
 SetLinger(false);
 fSet_Port_Active();
}

void ExperientTCPPort::fInitializeAMIport(int i)
{
 string                                 strKey = IPWORKS_RUNTIME_KEY;

 enumPortType                                = AMI;
 intPortNum                                  = i;
 string strEOL                               = "\n\n";
// SetEOL((char*)strEOL.c_str(), 2);
#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
 SetLocalHost(NULL);
 SetLocalPort(FREESWITCH_CLI_PORT_NUMBER+1);
 SetRemotePort(FREESWITCH_CLI_PORT_NUMBER);
 SetRemoteHost((char*) strASTERISK_AMI_IP_ADDRESS.c_str());
 
 fPortData();                
}

void ExperientTCPPort::fInitializeECATSport(int i)
{
 enumPortType                                = ECATS;
 intPortNum                                  = i;
 string strEOL                               = "\n\n";
 string strKey                               = IPWORKS_RUNTIME_KEY;
 extern IP_Address ECATS_IP;

#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
 SetEOL((char*)strEOL.c_str(), 2);
 SetLocalHost(NULL);
 SetLocalPort(FREESWITCH_CLI_PORT_NUMBER+1);
 SetRemotePort(ECATS_PORT_NUMBER);
 SetRemoteHost(ECATS_IP.stringAddress.c_str());
 SetLinger(false);
 fPortData();                
}





void ExperientTCPPort::fCheckTimeSinceLastRX()
{
 MessageClass                                   objMessage;
 struct timespec                                timespecTimeNow;
 long double                                    ldoubleTimeDiff;
 int                                            intReminderInterval;
 int                                            intLogCodeA, intLogCodeB;
 int                                            intRC;
 string                                         strMessage;
 extern Call_Data                               objBLANK_CALL_RECORD;
 extern int 					intDSB_PORT_DOWN_REMINDER_SEC;

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 switch (enumPortType)
  {
   case RCC:
            intReminderInterval = RCC_PORT_DOWN_REMINDER_INTERVAL_SEC; 
            strMessage = RCC_MESSAGE_909;
            intLogCodeA = 911;
            intLogCodeB = 909;           
            break;
   case DSB:
            intReminderInterval = intDSB_PORT_DOWN_REMINDER_SEC; 
            strMessage = DSB_MESSAGE_665;
            intLogCodeA = 666;
            intLogCodeB = 665;
            break;
   default: 
            SendCodingError("ExperientTCPPort::fCheckTimeSinceLastRX() -NO_PORT_TYPE");
            return;
  }

 intRC = sem_wait(&sem_tMutexTCPPortBuffer); // lock buffer
 ldoubleTimeDiff = time_difference(timespecTimeNow, this->timespecBufferTimeStamp);
 sem_post(&sem_tMutexTCPPortBuffer); // unlock buffer

 if (ldoubleTimeDiff > intPortDownThresholdSec) {
  fPingStatus();
          
  if (boolPortWasDown) {
   if (ldoubleTimeDiff > intPortDownReminderThreshold) {
    objMessage.fMessage_Create(LOG_WARNING, intLogCodeA, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               KRN_MESSAGE_121, seconds2time((unsigned long long int)(ldoubleTimeDiff)), fPingStatusString());
    enQueue_Message(enumPortType, objMessage);
    intPortDownReminderThreshold += intReminderInterval;
   }
  }
  else {
   objMessage.fMessage_Create(LOG_ALARM, intLogCodeB, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              strMessage, Remote_IP.stringAddress, seconds2time((unsigned long long int)(ldoubleTimeDiff)), fPingStatusString()); 
   enQueue_Message(enumPortType,objMessage);
   fSet_Port_Down();
  }       
 }

}

void ExperientTCPPort::fSet_Port_Active()
{
 int		intLogCode;
 string		strMESSAGE;
 extern int	intDSB_PORT_DOWN_REMINDER_SEC;

// 
 boolPortActive = true; 
 if (!boolPortWasDown) {return;}
 switch (enumPortType)  {
    case RCC:
             strMESSAGE = RCC_MESSAGE_910;
             intLogCode = 910;
             intPortDownReminderThreshold = RCC_PORT_DOWN_REMINDER_INTERVAL_SEC;
             boolPortWasDown = false;
             break;
   case DSB:
             intPortDownReminderThreshold = intDSB_PORT_DOWN_REMINDER_SEC;
             boolPortWasDown = false;
             strMESSAGE = DSB_MESSAGE_667;
             intLogCode = 667;
             break;
    default: break;
 }

 objMessage.fMessage_Create(LOG_ALARM, intLogCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            strMESSAGE, Remote_IP.stringAddress);
 enQueue_Message(enumPortType,objMessage);

}




void ExperientTCPPort::fCallParentEmergencyPortShutDown(string stringArg, unsigned long long int intArg)
{
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];

 ALIPort[intPortNum][intSideAorB].fEmergencyPortShutDown(objBLANK_CALL_RECORD, stringArg, intArg);
 vBuffer.clear();
}

void ExperientTCPPort::ConnectToRCCdevice(int iRecursion)
{
// SetAcceptData(true);
// SetConnected(true);
 //Connect((char*)Remote_IP.stringAddress.c_str(), intRemotePortNumber); 

 int							intRC;
 MessageClass						objMessage;
 
 extern int						intTCP_RECONNECT_RECURSION_LEVEL;
 extern int						intTCP_RECONNECT_INTERVAL_SEC;

 if (boolSTOP_EXECUTION)                                 {return;}
 if (iRecursion == intTCP_RECONNECT_RECURSION_LEVEL )    {return;}

 intRC = this->Connect((char*)  this->Remote_IP.stringAddress.c_str(),  this->intRemotePortNumber);

 if (intRC) {
  if ((boolDISPLAY_AND_LOG_RCC_RAW_TRAFFIC)||(iRecursion == 0)) {
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 918, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, this->objPortData,
                              objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, RCC_MESSAGE_918, this->GetLastError(), int2str(iRecursion+1));
   enQueue_Message(RCC,objMessage);
  }
  this->DoEvents();

  for (int i = 0; i < ((iRecursion+1)*intTCP_RECONNECT_INTERVAL_SEC); i++) {
   experient_nanosleep(ALMOST_ONE_SECOND_SEC_IN_NSEC);
   if (boolSTOP_EXECUTION)  {return;}  
  }

  this->ConnectToRCCdevice(iRecursion+1);

 }
 return;
}

void ExperientTCPPort::ConnectToALIServer()
{
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
 extern ExperientCommPort       ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];

 switch (intSideAorB)
  {
   case 1: case 2:
          if (ALIPort[intPortNum][intSideAorB].boolEmergencyShutdown ) {return;}
          SetAcceptData(true);
          SetConnected(true);
          Connect((char*)ALIPort[intPortNum][intSideAorB].RemoteIPAddress.stringAddress.c_str(), ALIPort[intPortNum][intSideAorB].intRemotePortNumber);
          break;
   default:
          if (ALIServicePort[intPortNum].boolEmergencyShutdown ) {return;}
          SetAcceptData(true);
          SetConnected(true);
          Connect((char*)ALIServicePort[intPortNum].RemoteIPAddress.stringAddress.c_str(), ALIServicePort[intPortNum].intRemotePortNumber);
          break; 
  }   
}

void ExperientTCPPort::fAddToBuffer(const char* chInput, size_t intLength)
{
 for (unsigned int i = 0; i < intLength; i++)
  {
   vBuffer.push_back(chInput[i]);
  }
}

void ExperientTCPPort::fProcessALITCPbuffer(string stringSourceAddress)
{
  int                                     intRC;
  DataPacketIn                            ALIDataPacket;
  string                                  strTempBuffer;
  bool                                    boolDelimeterFound;

  clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);

  do
   {
    strTempBuffer = strTCP_Buffer;
    strTempBuffer =  fCheck_Delimiter(strTempBuffer, boolDelimeterFound, charACK, charNAK, charETX);

    if (boolDelimeterFound)
     {
      ALIDataPacket.intPortNum 	                = intPortNum;
      ALIDataPacket.intSideAorB 	        = intSideAorB;
      ALIDataPacket.IPAddress.stringAddress     = stringSourceAddress;
      ALIDataPacket.stringDataIn                = strTempBuffer;
      ALIDataPacket.intLength                   = strTempBuffer.length();
      // send downrange
      intRC = sem_wait(&sem_tMutexAliPortDataQ);
      if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliPortDataQ, "sem_wait@sem_tMutexCadPortDataQ in fProcessALITCPbuffer", 1);}
      queueALIPortData[intPortNum].push(ALIDataPacket);
      sem_post(&sem_tMutexAliPortDataQ);
      sem_post(&sem_tAliFlag);
      strTCP_Buffer.erase(0,strTempBuffer.length()); 
     }
   } while (boolDelimeterFound);

}
void ExperientTCPPort::fProcessDashboardTCPbuffer(string stringSourceAddress) {

  int					intRC;
  DataPacketIn				DashBdDataPacket;
  size_t				start, end;
  string				strMessage;
  extern sem_t               		sem_tMutexDSBPortDataQ;
  extern queue <DataPacketIn>	   	queueDSBPortData;

  while (strTCP_Buffer.length() > 0)
   {
    start =strTCP_Buffer.find("{");
    if (start == string::npos) { strTCP_Buffer.clear(); return ;}
    end   = strTCP_Buffer.find("}");
    if (end == string::npos)   {return;}
    if (end < start)           {return;}

    strMessage = strTCP_Buffer.substr(start,(end - start + 1));

    strTCP_Buffer.erase(0, (end + 1)); 

    DashBdDataPacket.intPortNum 	            = intPortNum;
    DashBdDataPacket.stringDataIn                  = strMessage;
    DashBdDataPacket.intLength                     = DashBdDataPacket.stringDataIn.length();
    DashBdDataPacket.IPAddress.stringAddress       = stringSourceAddress;
  //  DashBdDataPacket.intTransactionID              = EventIdInteger(strXMLmessage);
    DashBdDataPacket.boolALIservicePacket          = false;

    // send downrange
    intRC = sem_wait(&sem_tMutexDSBPortDataQ);
    if (intRC){Semaphore_Error(intRC, DSB, &sem_tMutexDSBPortDataQ, "sem_wait@sem_tMutexDSBPortDataQ in fProcessDashboardTCPbuffer()", 1);}
    queueDSBPortData.push(DashBdDataPacket);
    sem_post(&sem_tMutexDSBPortDataQ);
    sem_post(&sem_tDSBflag);
   }

}

void ExperientTCPPort::fProcessALIServiceTCPbuffer(string stringSourceAddress)
{
  int                                     intRC;
  DataPacketIn                            ALIDataPacket;
  size_t                                  start, end;
  string                                  strXMLmessage;
  int                                     iLengthofEndTag = 8;
  while (strTCP_Buffer.length() > 0)
   {
    start =strTCP_Buffer.find("<?xml");
    if (start == string::npos) { strTCP_Buffer.clear(); return ;}
    end   = strTCP_Buffer.find("</Event>");
    if (end == string::npos)   {return;}
    if (end < start)           {return;}

    strXMLmessage = strTCP_Buffer.substr(start,(end - start + iLengthofEndTag));

    strTCP_Buffer.erase(0, (end + iLengthofEndTag)); 

    ALIDataPacket.intPortNum 	                = intPortNum;
    ALIDataPacket.intSideAorB 	                = 3;
    ALIDataPacket.stringDataIn                  = strXMLmessage;
    ALIDataPacket.intLength                     = ALIDataPacket.stringDataIn.length();
    ALIDataPacket.IPAddress.stringAddress       = stringSourceAddress;
    ALIDataPacket.intTransactionID              = EventIdInteger(strXMLmessage);
    ALIDataPacket.boolALIservicePacket          = true;

    // send downrange
    intRC = sem_wait(&sem_tMutexAliPortDataQ);
    if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliPortDataQ, "sem_wait@sem_tMutexCadPortDataQ in fProcessALITCPbuffer", 1);}
    queueALIPortData[intPortNum].push(ALIDataPacket);
    sem_post(&sem_tMutexAliPortDataQ);
    sem_post(&sem_tAliFlag);
   }

 return;
}
void ExperientTCPPort::fProcessE2TCPbuffer()
{
 vector<unsigned char>::size_type        szBuffer = vBuffer.size();
 bool                                    boolE4receiveFound = false;
 size_t                                  sIndex,sMessageLength, sMessageRemaining, sBytesProcessed, sStart, sSectionRemaining;
 ALI_E2_Data                             E2ALIData;
 DataPacketIn                            TCPPortDataRecieved;
 extern ExperientCommPort                ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];

 sMessageLength = 0;

  //Buffer should be semaphore locked prior to call ...

 E2ALIData.boolValidRecord = false;

 if (ALIPort[intPortNum][intSideAorB].boolEmergencyShutdown) {vBuffer.clear();}
 if (vBuffer.empty()) {return;}
 sBytesProcessed = 0;


// //cout << "received data" << endl;
// for (unsigned int p=0; p < szBuffer; p++) {cout << HEX_Convert(vBuffer[p]);}
// //cout << endl;



 for ( unsigned long int i = 0; i < szBuffer; i++)
  {
   sBytesProcessed++;
   if (vBuffer[i] == 0xE4)
    { boolE4receiveFound = true; sIndex = sStart = i; break;}
   fIncrementBadCharacterCounter();
  }
 if (!boolE4receiveFound) return;


 // check special length section
 if (vBuffer[sIndex+1] < 0x81)
  {
   if (sIndex > (szBuffer - 2)) {return;}
   sMessageLength = ((int) vBuffer[sIndex+1]);
   if ((sIndex + 2 + sMessageLength) >  szBuffer) { return;}      /// not enough data to process
   sBytesProcessed++;
   sIndex += 2;
  }
 else if (vBuffer[sIndex+1] == 0x81)
  {
   if (sIndex > (szBuffer - 3)) {return;}
   sMessageLength = ((int) vBuffer[sIndex+2]);
   if ((sIndex + 3 + sMessageLength) >  szBuffer) { return;}      /// not enough data to process
   sBytesProcessed+=2;
   sIndex += 3;
  }
 else if (vBuffer[sIndex+1] == 0x82)
  {
   if (sIndex > (szBuffer - 4)) {return;}
   sMessageLength =  (((int) vBuffer[sIndex+2])+255);
   sMessageLength += ((int) vBuffer[sIndex+3]);
   if ((sIndex + 4 + sMessageLength) >  szBuffer) { return;}      /// not enough data to process
   sBytesProcessed+=3;
   sIndex += 4;
  }
 else {fEraseBuffer(sStart, sMessageLength, 5, "Message Length Code"); return;} // Length coded incorrectly 
 


 // Load Transaction Id 

 if (sIndex > (szBuffer -2))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Transaction ID"); return;} // not enough data left in record to process  
 if (vBuffer[sIndex] != 0xC7)                                {fEraseBuffer(sStart, sMessageLength, 2, "Transaction ID"); return;} // transactionId not found 
 if (( sIndex + 2 +  E2_TRANSACTIONID_LENGTH) > szBuffer )   {fEraseBuffer(sStart, sMessageLength, 1, "Transaction ID"); return;} // not enough data left in record to process  
 if (((int) vBuffer[sIndex+1]) != E2_TRANSACTIONID_LENGTH)   {fEraseBuffer(sStart, sMessageLength, 3, "Transaction ID"); return;} //transactionid length not as per spec.
 for ( unsigned int i = 0; i < E2_TRANSACTIONID_LENGTH; i++) {E2ALIData.chTransactionId[i] =  vBuffer[sIndex+2+i];}
 sBytesProcessed+= (2+E2_TRANSACTIONID_LENGTH);

// //cout << "Transaction ID = " << HEX_String(E2ALIData.chTransactionId,  E2_TRANSACTIONID_LENGTH) << endl;

 // Load Component Sequence 
 sIndex+= (2+E2_TRANSACTIONID_LENGTH);

 if (sIndex > (szBuffer -2))                                  {fEraseBuffer(sStart, sMessageLength, 1, "Component Sequence"); return;} // not enough data left in record to process 
 if (vBuffer[sIndex] != 0xE8)                                 {fEraseBuffer(sStart, sMessageLength, 2, "Component Sequence"); return;} // Component Sequence Not found

 if (vBuffer[sIndex+1] < 0x81)
  {
   if (sIndex > (szBuffer - 2)) {return;}
   sMessageRemaining = ((int) vBuffer[sIndex+1]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Component Sequence"); return;} // Malformed record
   sBytesProcessed+=2;
   sIndex += 2;
  }
 else if (vBuffer[sIndex+1] == 0x81)
  {
   if (sIndex > (szBuffer - 3))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Component Sequence"); return;} // not enough data left in record to process 
   sMessageRemaining = ((int) vBuffer[sIndex+2]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Component Sequence"); return;} // Malformed record
   sBytesProcessed+=3;
   sIndex += 3;
  }
 else if (vBuffer[sIndex+1] == 0x82)
  {
   if (sIndex > (szBuffer - 4))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Component Sequence Length"); return;} // not enough data left in record to process 
   sMessageRemaining =  (((int) vBuffer[sIndex+2])+255);
   sMessageRemaining += ((int) vBuffer[sIndex+3]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Component Sequence"); return;} // Malformed record
   sBytesProcessed+=4;
   sIndex += 4;
  }
 else {fEraseBuffer(sStart, sMessageLength, 5, "Component Sequence Length Code"); return;} // Length coded incorrectly 


 // Load Componet Type
 switch (vBuffer[sIndex])
  {
   case 0xE9: case 0xEA: case 0xEB:  case 0xEC:   
        E2ALIData.chComponentType = vBuffer[sIndex];
        break;
   default:
        fEraseBuffer(sStart, sMessageLength, 5, "Component Type"); return;;                   // NonStandard Componet Type Code
  }
 if (vBuffer[sIndex+1] < 0x81)
  {
   if (sIndex > (szBuffer - 2))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Component Type Length"); return;} // not enough data left in record to process 
   sMessageRemaining = ((int) vBuffer[sIndex+1]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Component Type Length"); return;} // Malformed record
   sBytesProcessed+=2;
   sIndex += 2;
  }
 else if (vBuffer[sIndex+1] == 0x81)
  {
   if (sIndex > (szBuffer - 3))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Component Type Length"); return;} // not enough data left in record to process 
   sMessageRemaining = ((int) vBuffer[sIndex+2]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Component Type Length"); return;} // Malformed record
   sBytesProcessed+=3;
   sIndex += 3;
  }
 else if (vBuffer[sIndex+1] == 0x82)
  {
   if (sIndex > (szBuffer - 4))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Component Type Length"); return;} // not enough data left in record to process 
   sMessageRemaining =  (((int) vBuffer[sIndex+2])+255);
   sMessageRemaining += ((int) vBuffer[sIndex+3]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Component Type Length"); return;} // Malformed record
   sBytesProcessed+=4;
   sIndex += 4;
  }
 else {fEraseBuffer(sStart, sMessageLength, 5, "Component Type Length Code"); return;} // Length coded incorrectly


 // Load Componet Id
 if (sIndex > (szBuffer -3))                                  {fEraseBuffer(sStart, sMessageLength, 1, "Component ID"); return;}  // not enough data left in record to process 
 if (vBuffer[sIndex] != 0xCF)                                 {fEraseBuffer(sStart, sMessageLength, 2, "Component ID"); return;}  // Component Id Not found
 if (vBuffer[sIndex+1] != 0x01)                               {fEraseBuffer(sStart, sMessageLength, 3, "Component ID"); return;} //Component Id length not as per spec.
 E2ALIData.chComponentId = vBuffer[sIndex+2];
 sBytesProcessed+= 3;

 // Load Problem Code or Error Code if Present
 sIndex += 3;
 switch (vBuffer[sIndex])
  {
   case 0xD4:            // Error Code
        if (sIndex > (szBuffer -3))                           {fEraseBuffer(sStart, sMessageLength, 1, "Error Code"); return;} // not enough data left in record to process 
        if (vBuffer[sIndex+1] != 0x01)                        {fEraseBuffer(sStart, sMessageLength, 3, "Error Code"); return;}  //Error Code length not as per spec.
        switch (vBuffer[sIndex+2])
         {
          case 0x01: case 0x02: case 0x03: case 0x04:
               E2ALIData.chErrorCode = vBuffer[sIndex+2];
               break;
          default:
                fEraseBuffer(sStart, sMessageLength, 5, "Error Code"); return;                   // NonStandard Error Code
         }
//        //cout << "Error Code =" << ASCII_Convert(E2ALIData.chErrorCode) << endl;
        sIndex += 3;
        sBytesProcessed+= 3;
        break;
   case 0xD5:           // Problem Code
        if (sIndex > (szBuffer -4))                           {fEraseBuffer(sStart, sMessageLength, 1, "Problem Code"); return;}  // not enough data left in record to process 
        if (vBuffer[sIndex+1] != 0x02)                        {fEraseBuffer(sStart, sMessageLength, 3, "Problem Code"); return;}  //Problem Code length not as per spec.

        switch (vBuffer[sIndex+2])
         {
          case 0x01:

               switch (vBuffer[sIndex+3])
                {
                 case 0x01: case 0x02: case 0x03: case 0x04:
                      E2ALIData.chProblemSpecifier = vBuffer[sIndex+3];break;
                 default:
                      fEraseBuffer(sStart, sMessageLength, 5, "Problem Specifier"); return;;                   // NonStandard Problem Specifier 
                }
               E2ALIData.chProblemType = vBuffer[sIndex+2];
               break;
  
          case 0x02: 

               switch (vBuffer[sIndex+3])
                {
                 case 0x01: case 0x02: case 0x03:
                      E2ALIData.chProblemSpecifier = vBuffer[sIndex+3];break;
                 default:
                      fEraseBuffer(sStart, sMessageLength, 5, "Problem Specifier"); return;;                   // NonStandard Problem Specifier 
                }
               E2ALIData.chProblemType = vBuffer[sIndex+2];
               break;

          case 0x05:

               switch (vBuffer[sIndex+3])
                {
                 case 0x01: case 0x02: case 0x03: case 0x04: case 0x05: case 0x06:
                      E2ALIData.chProblemSpecifier = vBuffer[sIndex+3];break;
                 default:
                      fEraseBuffer(sStart, sMessageLength, 5, "Problem Specifier"); return;;                   // NonStandard Problem Specifier 
                }
               E2ALIData.chProblemType = vBuffer[sIndex+2];
               break;

          default:
               fEraseBuffer(sStart, sMessageLength, 5, "Problem Type"); return;;                              // NonStandard Problem type
         }   
        
        sIndex += 4;
        sBytesProcessed+= 4;
        break;
   default:
        break;             // do nothing         

  }

 // load Parameter Set
 if (sIndex > (szBuffer -2))                                  {fEraseBuffer(sStart, sMessageLength, 1, "Parameter Set"); return;} // not enough data left in record to process 
 if (vBuffer[sIndex] != 0xF2)                                 {fEraseBuffer(sStart, sMessageLength, 2, "Parameter Set"); return;}  // Parameter Set Not found

 if (vBuffer[sIndex+1] < 0x81)
  {
   if (sIndex > (szBuffer - 2))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Parameter Set"); return;}
   sMessageRemaining = ((int) vBuffer[sIndex+1]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Parameter Set"); return;} // Malformed record
   sBytesProcessed+=2;
   sIndex += 2;
  }
 else if (vBuffer[sIndex+1] == 0x81)
  {
   if (sIndex > (szBuffer - 3))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Parameter Set"); return;}
   sMessageRemaining = ((int) vBuffer[sIndex+2]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Parameter Set"); return;} // Malformed record
   sBytesProcessed+=3;
   sIndex += 3;
  }
 else if (vBuffer[sIndex+1] == 0x82)
  {
   if (sIndex > (szBuffer - 4))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Parameter Set"); return;}
   sMessageRemaining =  (((int) vBuffer[sIndex+2])+255);
   sMessageRemaining += ((int) vBuffer[sIndex+3]);
   if ((sMessageRemaining + sIndex - sStart) != sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Parameter Set"); return;} // Malformed record
   sBytesProcessed+=4;
   sIndex += 4;
  }
 else {fEraseBuffer(sStart, sMessageLength, 5, "Parameter Set Length Code"); return;} // Length coded incorrectly 
 
// //cout << "MessageLength = " << sMessageLength << endl;
 while (sBytesProcessed < sMessageLength)
  {
   switch (vBuffer[sIndex])
    {
     case 0xC0:   // Position result
          if (sIndex > (szBuffer -3))                         {fEraseBuffer(sStart, sMessageLength, 1, "Position Result"); return;} // not enough data left in record to process 
          if (vBuffer[sIndex+1] != 0x01)                      {fEraseBuffer(sStart, sMessageLength, 3, "Position Result"); return;}  //length not as per spec.         
          E2ALIData.chPositionResult =  vBuffer[sIndex+2];
          sIndex+= 3; sBytesProcessed += 3; 
          break;
     case 0xE1:          // Position Information
          if (sIndex > (szBuffer -2))                         {fEraseBuffer(sStart, sMessageLength, 1, "Position Information"); return;} // not enough data left in record to process 
          E2ALIData.chPositionInformationLength     = vBuffer[sIndex+2];
          sSectionRemaining = (int)  E2ALIData.chPositionInformationLength; 
          sIndex+= 2; sBytesProcessed+=2; 

          // need to reference length ....
           // Position Time
           if (vBuffer[sIndex] != 0xC0)                       {fEraseBuffer(sStart, sMessageLength, 2, "Position Time"); return;}  // Position Time not found
           if (sIndex > (szBuffer -8))                        {fEraseBuffer(sStart, sMessageLength, 1, "Position Time"); return;}  // not enough data left in record to process
           if (vBuffer[sIndex+1] != 0x06)                     {fEraseBuffer(sStart, sMessageLength, 3, "Position Time"); return;}  // length not as per spec.
           E2ALIData.chPositionYear         =  vBuffer[sIndex+2];
           E2ALIData.chPositionMonth        =  vBuffer[sIndex+3];
           E2ALIData.chPositionDay          =  vBuffer[sIndex+4];
           E2ALIData.chPositionTimeofDay[0] =  vBuffer[sIndex+5];
           E2ALIData.chPositionTimeofDay[1] =  vBuffer[sIndex+6];
           E2ALIData.chPositionTimeofDay[2] =  vBuffer[sIndex+7];
           sIndex+= 8;
           sBytesProcessed+=8;
           sSectionRemaining-= 8;
           //Geographic Position
           if (vBuffer[sIndex] != 0xC1)                            {fEraseBuffer(sStart, sMessageLength, 2, "Geographic Position"); return;}  // Position Geo not found
           if (sIndex > (szBuffer -1))                             {fEraseBuffer(sStart, sMessageLength, 1, "Geographic Position"); return;}  // not enough data left in record to process
           E2ALIData.chPositionGeoLength         = vBuffer[sIndex+1];
           if (sIndex > (szBuffer - E2ALIData.chPositionGeoLength)){fEraseBuffer(sStart, sMessageLength, 1, "Geographic Position"); return;}  // not enough data left in record to process
           E2ALIData.chPositionGeoLPRI           = ((vBuffer[sIndex+2]>>2)&0x03);
           E2ALIData.chPositionGeoScreening      = (vBuffer[sIndex+2]&0x03);
           E2ALIData.chPositionGeoExt            = ((vBuffer[sIndex+3]>>7)&0x01);
           E2ALIData.chPositionGeoTypeofShape    = (vBuffer[sIndex+3]&0x7F);
           E2ALIData.chPositionGeoLatSign        = ((vBuffer[sIndex+4]>>7)&0x01);
           E2ALIData.chPositionGeoLatDegrees[0]  = (vBuffer[sIndex+4]&0x7F);
           E2ALIData.chPositionGeoLatDegrees[1]  = vBuffer[sIndex+5];
           E2ALIData.chPositionGeoLatDegrees[2]  = vBuffer[sIndex+6];
           E2ALIData.chPositionGeoLongDegrees[0] = vBuffer[sIndex+7];
           E2ALIData.chPositionGeoLongDegrees[1] = vBuffer[sIndex+8];
           E2ALIData.chPositionGeoLongDegrees[2] = vBuffer[sIndex+9];
         
           switch (E2ALIData.chPositionGeoTypeofShape)
            {
             case 0x00:  // Ellipsoid Point
                  sIndex+= 10;
                  sBytesProcessed+=10;
                  sSectionRemaining-= 10;
                  break;
             case 0x01:   // Ellipsoid Point with uncertainty
                  E2ALIData.chPositionUncertaintyCode = (vBuffer[sIndex+10]&0x7F); 
                  E2ALIData.chPositionConfidence      = (vBuffer[sIndex+11]&0x7F); 
                  sIndex+= 12;
                  sBytesProcessed+=12;
                  sSectionRemaining-= 12;
                  break;
             case 0x02: // point with Altitude and uncertainty
                  E2ALIData.chPositionUncertaintyCode         = (vBuffer[sIndex+10]&0x7F);
                  E2ALIData.chPositionAltitudeSign            = ((vBuffer[sIndex+11]>>7)&0x01);
                  E2ALIData.chPositionAltitude[0]             = (vBuffer[sIndex+11]&0x7F); 
                  E2ALIData.chPositionAltitude[1]             = vBuffer[sIndex+12]; 
                  E2ALIData.chPositionAltitudeUncertaintyCode = (vBuffer[sIndex+13]&0x7F);
                  E2ALIData.chPositionConfidence              = (vBuffer[sIndex+14]&0x7F);
                  sIndex+= 15;
                  sBytesProcessed+=15;
                  sSectionRemaining-= 15;
                  break;
             default:
   //           //cout <<  HEX_Convert(E2ALIData.chPositionGeoTypeofShape) << endl;
              
              fEraseBuffer(sStart, sMessageLength, 5, "Geographic Shape"); return;  // invalid value of Geographic shape    
            }
          if (sSectionRemaining > 0) 
           {
           if (vBuffer[sIndex] != 0xC2)                       {fEraseBuffer(sStart, sMessageLength, 2, "Position Source"); return;}  // Position Source not found
           if (sIndex > (szBuffer -2))                        {fEraseBuffer(sStart, sMessageLength, 1, "Position Source"); return;}  // not enough data left in record to process
           if (vBuffer[sIndex+1] != 0x01)                     {fEraseBuffer(sStart, sMessageLength, 3, "Position Source"); return;}  // length not as per spec.
           E2ALIData.chPositionSource  = vBuffer[sIndex+2];
           sIndex+= 3;
           sBytesProcessed+=3;
           sSectionRemaining-= 3;
           }

  //        //cout << "processed = " << sBytesProcessed << endl;
          break;

     case 0xC2:   // Callback Number
          if (sIndex > (szBuffer -11))                                                   {fEraseBuffer(sStart, sMessageLength, 1, "Callback Number"); return;}  // not enough data left in record to process 
          if (vBuffer[sIndex+1] != 0x09)                                                 {fEraseBuffer(sStart, sMessageLength, 3, "Callback Number"); return;}   //length not as per spec.
          if(!E2ALIData.CallbackNumber.fLoadTypeofDigits(vBuffer[sIndex+2]))             {fEraseBuffer(sStart, sMessageLength, 5, "Callback Type of Digits"); return;} 
          if(!E2ALIData.CallbackNumber.fLoadNatureofNumber(vBuffer[sIndex+3]))           {fEraseBuffer(sStart, sMessageLength, 5, "Callback Nature of Number"); return;}
          if(!E2ALIData.CallbackNumber.fLoadNumberingPlanandEncoding(vBuffer[sIndex+4])) {fEraseBuffer(sStart, sMessageLength, 5, "Callback Num Pln and Encoding"); return;}
          if (vBuffer[sIndex+5] != 0x0A)                                                 {fEraseBuffer(sStart, sMessageLength, 3, "Callback Number of Digits"); return;}
          E2ALIData.CallbackNumber.chNumberofDigits = vBuffer[sIndex+5];
          if (!E2ALIData.CallbackNumber.fLoadDigits(vBuffer[sIndex+6], 21) )   {fEraseBuffer(sStart, sMessageLength, 5, "Callback Digits 2-1"); return;}
          if (!E2ALIData.CallbackNumber.fLoadDigits(vBuffer[sIndex+7], 43) )   {fEraseBuffer(sStart, sMessageLength, 5, "Callback Digits 4-3"); return;}
          if (!E2ALIData.CallbackNumber.fLoadDigits(vBuffer[sIndex+8], 65) )   {fEraseBuffer(sStart, sMessageLength, 5, "Callback Digits 6-5"); return;}
          if (!E2ALIData.CallbackNumber.fLoadDigits(vBuffer[sIndex+9], 87) )   {fEraseBuffer(sStart, sMessageLength, 5, "Callback Digits 8-7"); return;}
          if (!E2ALIData.CallbackNumber.fLoadDigits(vBuffer[sIndex+10],109))   {fEraseBuffer(sStart, sMessageLength, 5, "Callback Digits 10-9"); return;}          
          sIndex+= 11;
          sBytesProcessed+=11;
          break;

     case 0xC3:  // Emergency Services Routing Digits
          if (sIndex > (szBuffer -11))                                                                   {fEraseBuffer(sStart, sMessageLength, 1, "ESRD"); return;} // not enough data left in record to process
          if (vBuffer[sIndex+1] != 0x09)                                                                 {fEraseBuffer(sStart, sMessageLength, 3, "ESRD"); return;} // length not as per spec.
          if(!E2ALIData.EmergencyServicesRoutingDigits.fLoadTypeofDigits(vBuffer[sIndex+2]))             {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Type of Digits"); return;} 
          if(!E2ALIData.EmergencyServicesRoutingDigits.fLoadNatureofNumber(vBuffer[sIndex+3]))           {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Nature of Number"); return;} 
          if(!E2ALIData.EmergencyServicesRoutingDigits.fLoadNumberingPlanandEncoding(vBuffer[sIndex+4])) {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Num Pln and Encoding"); return;} 
          if (vBuffer[sIndex+5] != 0x0A)                                                                 {fEraseBuffer(sStart, sMessageLength, 3, "ESRD Number of Digits"); return;}
          E2ALIData.EmergencyServicesRoutingDigits.chNumberofDigits = vBuffer[sIndex+5]; 
          if (!E2ALIData.EmergencyServicesRoutingDigits.fLoadDigits(vBuffer[sIndex+6], 21) )   {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Digits 2-1"); return;}          
          if (!E2ALIData.EmergencyServicesRoutingDigits.fLoadDigits(vBuffer[sIndex+7], 43) )   {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Digits 4-3"); return;}
          if (!E2ALIData.EmergencyServicesRoutingDigits.fLoadDigits(vBuffer[sIndex+8], 65) )   {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Digits 6-5"); return;}
          if (!E2ALIData.EmergencyServicesRoutingDigits.fLoadDigits(vBuffer[sIndex+9], 87) )   {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Digits 8-7"); return;}
          if (!E2ALIData.EmergencyServicesRoutingDigits.fLoadDigits(vBuffer[sIndex+10],109))   {fEraseBuffer(sStart, sMessageLength, 5, "ESRD Digits 10-9"); return;}
          sIndex+= 11;
          sBytesProcessed+=11;
          break; 
               
     case 0xC4:   // Generalized time
          if (sIndex > (szBuffer -8))                        {fEraseBuffer(sStart, sMessageLength, 1, "Generalized Time"); return;} // not enough data left in record to process
          if (vBuffer[sIndex+1] != 0x06)                     {fEraseBuffer(sStart, sMessageLength, 3, "Generalized Time"); return;}  // length not as per spec.
          E2ALIData.chYear         =  vBuffer[sIndex+2];
          E2ALIData.chMonth        =  vBuffer[sIndex+3];
          E2ALIData.chDay          =  vBuffer[sIndex+4];
          E2ALIData.chTimeofDay[0] =  vBuffer[sIndex+5];
          E2ALIData.chTimeofDay[1] =  vBuffer[sIndex+6];
          E2ALIData.chTimeofDay[2] =  vBuffer[sIndex+7];
          sIndex+= 8;
          sBytesProcessed+=8;
          break;
              
     case 0xC5:  // Mobile Identification Number
          if (sIndex > (szBuffer -11))                        {fEraseBuffer(sStart, sMessageLength, 1, "Mobile ID Number"); return;} // not enough data left in record to process
          if (vBuffer[sIndex+1] != 0x09)                      {fEraseBuffer(sStart, sMessageLength, 3, "Mobile ID Number"); return;} // length not as per spec.
          if(!E2ALIData.MobileIdentificationNumber.fLoadTypeofDigits(vBuffer[sIndex+2]))             {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Type of Digits"); return;} 
          if(!E2ALIData.MobileIdentificationNumber.fLoadNatureofNumber(vBuffer[sIndex+3]))           {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Nature of Number"); return;} 
          if(!E2ALIData.MobileIdentificationNumber.fLoadNumberingPlanandEncoding(vBuffer[sIndex+4])) {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Num Pln and Encoding"); return;} 
          if (vBuffer[sIndex+5] != 0x0A)                                                             {fEraseBuffer(sStart, sMessageLength, 3, "Mobile ID Number of Digits"); return;}
          E2ALIData.MobileIdentificationNumber.chNumberofDigits = vBuffer[sIndex+5]; 
          if (!E2ALIData.MobileIdentificationNumber.fLoadDigits(vBuffer[sIndex+6], 21) )   {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Digits 2-1"); return;}          
          if (!E2ALIData.MobileIdentificationNumber.fLoadDigits(vBuffer[sIndex+7], 43) )   {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Digits 4-3"); return;}
          if (!E2ALIData.MobileIdentificationNumber.fLoadDigits(vBuffer[sIndex+8], 65) )   {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Digits 6-5"); return;}
          if (!E2ALIData.MobileIdentificationNumber.fLoadDigits(vBuffer[sIndex+9], 87) )   {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Digits 8-7"); return;}
          if (!E2ALIData.MobileIdentificationNumber.fLoadDigits(vBuffer[sIndex+10],109))   {fEraseBuffer(sStart, sMessageLength, 5, "Mobile ID Digits 10-9"); return;}
          sIndex+= 11;
          sBytesProcessed+=11;
          break;

     case 0xC6:  // International Mobile Subscriber Identity
          if (sIndex > (szBuffer -14))                       {fEraseBuffer(sStart, sMessageLength, 1, "Intl Mobile Subscr ID"); return;}  // not enough data left in record to process
          if (vBuffer[sIndex+1] != 0x0C)                     {fEraseBuffer(sStart, sMessageLength, 3, "Intl Mobile Subscr ID"); return;} // length not as per spec.
          if(!E2ALIData.InternationalMobileSubcriberIdentity.fLoadTypeofDigits(vBuffer[sIndex+2]))             {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Type of Digits"); return;} 
          if(!E2ALIData.InternationalMobileSubcriberIdentity.fLoadNatureofNumber(vBuffer[sIndex+3]))           {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Nature of Number"); return;} 
          if(!E2ALIData.InternationalMobileSubcriberIdentity.fLoadNumberingPlanandEncoding(vBuffer[sIndex+4])) {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Num Pln and Encoding"); return;} 
          if (vBuffer[sIndex+5] != 0x0A)                                                                       {fEraseBuffer(sStart, sMessageLength, 3, "Intl Mobile Subscr ID Number of Digits"); return;}
          E2ALIData.InternationalMobileSubcriberIdentity.chNumberofDigits = vBuffer[sIndex+5]; 
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+6], 21) )   {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 2-1"); return;}          
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+7], 43) )   {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 4-3"); return;}
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+8], 65) )   {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 6-5"); return;}
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+9], 87) )   {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 8-7"); return;}
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+10],109))   {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 10-9"); return;}
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+11],1211))  {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 12-11"); return;}
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+12],1413))  {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits 14-13"); return;}
          if (!E2ALIData.InternationalMobileSubcriberIdentity.fLoadDigits(vBuffer[sIndex+13], 15))   {fEraseBuffer(sStart, sMessageLength, 5, "Intl Mobile Subscr ID Digits F-15"); return;}
          sIndex+= 14;
          sBytesProcessed+=14;
          break;

     case 0xC7:      // Mobile Call Status
          if (sIndex > (szBuffer -3))                              {fEraseBuffer(sStart, sMessageLength, 1, "Mobile Call Status"); return;} // not enough data left in record to process 
          if (vBuffer[sIndex+1] != 0x01)                           {fEraseBuffer(sStart, sMessageLength, 3, "Mobile Call Status"); return;}  //length not as per spec.
          if (!E2ALIData.fLoadMobileCallStatus(vBuffer[sIndex+2])) {fEraseBuffer(sStart, sMessageLength, 5, "Mobile Call Status"); return;}        
          sIndex+= 3; sBytesProcessed += 3; 
          break;

     case 0xC8:      // Company ID
          if (sIndex > (szBuffer -2))                                                    {fEraseBuffer(sStart, sMessageLength, 1, "Company ID"); return;} // not enough data left in record to process 
          E2ALIData.chCompanyIdStringLength =  vBuffer[sIndex+1];
          sIndex+=2; sBytesProcessed+=2;
          if ((int) E2ALIData.chCompanyIdStringLength > E2_MAX_COMPANY_ID_STRING_LENGTH) {fEraseBuffer(sStart, sMessageLength, 3, "Company ID"); return;}  //length not as per spec.
          for (int i = 0; i < (int)E2ALIData.chCompanyIdStringLength; i++) { E2ALIData.chCompanyIdString[i] = vBuffer[sIndex];  sIndex++; sBytesProcessed++;}
          break;

     case 0xCE:      //location description

           if (vBuffer[sIndex+1] < 0x81)
            {
             if (sIndex > (szBuffer - 2))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Location Description"); return;} // not enough data left in record to process 
             E2ALIData.intLocationDescriptionLength = ((int) vBuffer[sIndex+1]);
             if ((E2ALIData.intLocationDescriptionLength + sIndex - sStart) > sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Location Description"); return;} // Malformed record
             sBytesProcessed+=2;
             sIndex += 2;
            }
           else if (vBuffer[sIndex+1] == 0x81)
            {
             if (sIndex > (szBuffer - 3))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Location Description"); return;} // not enough data left in record to process 
             E2ALIData.intLocationDescriptionLength = ((int) vBuffer[sIndex+2]);
             if ((E2ALIData.intLocationDescriptionLength + sIndex - sStart) > sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Location Description"); return;} // Malformed record
             sBytesProcessed+=3;
             sIndex += 3;
            }
           else if (vBuffer[sIndex+1] == 0x82)
            {
             if (sIndex > (szBuffer - 4))                                 {fEraseBuffer(sStart, sMessageLength, 1, "Location Description"); return;} // not enough data left in record to process 
             E2ALIData.intLocationDescriptionLength =  (((int) vBuffer[sIndex+2])+255);
             E2ALIData.intLocationDescriptionLength += ((int) vBuffer[sIndex+3]);
             if ((E2ALIData.intLocationDescriptionLength + sIndex - sStart) > sMessageLength) {fEraseBuffer(sStart, sMessageLength, 4, "Location Description"); return;} // Malformed record
             sBytesProcessed+=4;
             sIndex += 4;
            }
           else {fEraseBuffer(sStart, sMessageLength, 5, "Location Description Length Code"); return;} // Length coded incorrectly 

          

  //       if ((int) E2ALIData.chLocationDescriptionLength > E2_MAX_LOCATION_DESCRIPTION_LENGTH) {fEraseBuffer(sStart, sMessageLength, 3, "Location Description"); return;}  //length not as per spec.
          for (int i = 0; i <  E2ALIData.intLocationDescriptionLength; i++) { E2ALIData.chLocationDescription[i] = vBuffer[sIndex];  sIndex++; sBytesProcessed++;}
          break;
          
     default:
          SendCodingError("CommPortClasses.cpp - unknown E2  ALI Parameter = " + ASCII_Convert(vBuffer[sIndex]));
          fIncrementBadCharacterCounter();
          sIndex++;
          sBytesProcessed++;
          break;
       
   }
  }
// vBuffer.erase(vBuffer.begin(), vBuffer.begin()+(sMessageLength + 2 + sStart));
 vBuffer.erase(vBuffer.begin(), vBuffer.begin()+ sBytesProcessed);
// //cout << "buffer erased size = " << vBuffer.size() << endl;


 E2ALIData.boolValidRecord                                       = true;
 ALIPort[intPortNum][intSideAorB].intConsecutiveBadRecordCounter = 0;  
 TCPPortDataRecieved.intPortNum                                  = intPortNum;
 TCPPortDataRecieved.intSideAorB                                 = intSideAorB;
 TCPPortDataRecieved.stringDataIn                                = "";
 TCPPortDataRecieved.intLength                                   = 0;
 TCPPortDataRecieved.E2Data                                      = E2ALIData; 
 fQueueE2ALIData(TCPPortDataRecieved);

 return;
}

void ExperientTCPPort::fEraseBuffer(size_t Position,  size_t sDataLength, int intError, string strSection)
{
 MessageClass                   objMessage;
 string                         strErrorMessage, strError;
 unsigned char                  chData[sDataLength];
 int                            j = 0;
 extern ExperientCommPort       ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1];
 
 //Buffer should be semaphore locked prior to call ...
 switch (intError)
  {
   case 1:  strError = "Encoded Length of %%% is Greater than Message Length"; break;
   case 2:  strError = "Element %%% not found in Message"; break;
   case 3:  strError = "Encoded Length of %%% is not per specification"; break;
   case 4:  strError = "Encoded Length of %%% does not match remaining length"; break;
   case 5:  strError = "Unrecognized Value of %%%"; break;
   default: strError = "Unknown Error in Element %%%"; break;
  }

 for (size_t i=Position; i < Position+sDataLength; i++) {chData[j] = vBuffer[i]; j++;}

 strErrorMessage = Create_Message(strError, strSection);
 objMessage.fMessage_Create(LOG_WARNING,257, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                            ALI_MESSAGE_257, strErrorMessage, HEX_String(chData, sDataLength),"","","","", NORMAL_MSG, NEXT_LINE);
 enQueue_Message(ALI,objMessage);

 for (size_t i = 0; i <= Position; i++) {vBuffer.erase(vBuffer.begin());}
 ALIPort[intPortNum][intSideAorB].fIncrementConsecutiveBadRecordCounter();
}

void ExperientTCPPort::fQueueE2ALIData(DataPacketIn Data)
{
 int     intRC;

 Data.intTransactionID = Data.E2Data.fTransactionID();
 if      (Data.E2Data.fIsACK()) {Data.fSetACK();}
 else if (Data.E2Data.fIsNAK()) {Data.fSetNAK();}
 else if (!Data.fCreateLegacyALIrecord())      {return;}

 // Queue to ALI thread
 intRC = sem_wait(&sem_tMutexAliPortDataQ);
 if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliPortDataQ, "sem_wait@sem_tMutexAliPortDataQ in fQueueE2ALIData()", 1);}
 queueALIPortData[intPortNum].push(Data);
 sem_post(&sem_tMutexAliPortDataQ);
 sem_post(&sem_tAliFlag);

}


void ExperientTCPPort::fIncrementBadCharacterCounter(unsigned long long int iLength)
{
 MessageClass           objMessage;

 sem_wait(&sem_tMutexBadCharacterCount);

 intBadCharacterCount+= iLength; 
 if (intBadCharacterCount > (ULONG_LONG_MAX - intALI_COMM_IN_BUFFFER_SIZE) ) 
  {
   objMessage.fMessage_Create(LOG_WARNING, 250, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_524, "Discarded Packet Counter", int2strLZ(intBadCharacterCount));
   enQueue_Message(enumPortType,objMessage);
   intBadCharacterCount = 0;
  }
 sem_post(&sem_tMutexBadCharacterCount);
 return;
}
/*
void ExperientTCPPort::fCheck_Buffer(string strText)
{
 struct timespec             timespecTimeNow;
 long double                 ldoubleTimeDiff;
 int                         intRC;
 DataPacketIn                BufferData;
 MessageClass                objMessage;
 int                         intLogCode;
 int                         intMessageCode = 0;
 string                      stringSideAorB = "";
 double                      dblBufferForceTransmit = doubleBUFFER_FORCE_TRANSMIT;
 

 if (stringBuffer.empty()){sem_post(&sem_tPortLock);return;}
            
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 ldoubleTimeDiff = time_difference(timespecTimeNow, timespecBufferTimeStamp);

 
 // clear buffer and queue to thread if time exceeded 
 if (ldoubleTimeDiff > dblBufferForceTransmit)
  {
   
   BufferData.intPortNum                = intPortNum;
   BufferData.intSideAorB               = intSideAorB;
   BufferData.stringDataIn              = strTCP_Buffer;
   BufferData.intLength                 = strTCP_Buffer.length();
   BufferData.boolStringBufferExceeded  = false;

   strTCP_Buffer.clear();

   fQueue_Transmission(BufferData);
    
     switch (enumPortType)
      {
       case ALI:
            if (intSideAorB == 1){stringSideAorB = "A";}else{stringSideAorB = "B";}
            if (!boolIGNORE_BAD_ALI)
             {
              if(boolBufferPreviouslyCleared){intLogCode = LOG_CONSOLE_FILE;} else {intLogCode = LOG_WARNING;}
               objMessage.fMessage_Create(intLogCode, 241, objBLANK_CALL_RECORD, fPortData(), ALI_MESSAGE_241, int2str(BufferData.intLength));
               enQueue_Message(ALI,objMessage);
             }
            timespecBufferTimeStamp = timespecTimeNow;
            boolBufferPreviouslyCleared = true;
            sem_post(&sem_tPortLock);
            return;
       case ANI:
            intMessageCode = 347;break;
       case CAD:
            intMessageCode = 466;break;
       default:
            SendCodingError( "CommPortClasses.cpp - Coding Error in ExperientUDPPort::fCheck_Buffer()");
      }// end switch    
   
     if(boolBufferPreviouslyCleared){intLogCode = LOG_CONSOLE_FILE;} else {intLogCode = LOG_WARNING;}

   objMessage.fMessage_Create(intLogCode, intMessageCode, objBLANK_CALL_RECORD, fPortData(), KRN_MESSAGE_162, int2str(BufferData.intLength));
   enQueue_Message(enumPortType,objMessage);
   timespecBufferTimeStamp = timespecTimeNow;
   boolBufferPreviouslyCleared = true;
  }

 sem_post(&sem_tPortLock);
 
}// end ExperientUDPPort::fCheck_Buffer()

*/



void ExperientTCPPort::fReadAMIScript(string strText)
{
 size_t 			found;
 DataPacketIn               	AMIDataPacket;
 string               		strTAB = "";
 string		        	strETX = "";
 string                         strSendPacket;
 int                            intRC;
 bool                           boolContentPacket;
 bool                           boolRegistrationPacket;

 strTAB += charTAB; strETX += charETX;

  if(!strText.length()) {return;}

  strTCP_Buffer += strText;
                       
  strTCP_Buffer = FindandReplaceALL(strTCP_Buffer, "\\n\\n", strTAB+strETX);
  strTCP_Buffer = FindandReplaceALL(strTCP_Buffer, "\\r\\n\\r\\n", strTAB+strETX);
  strTCP_Buffer = FindandReplaceALL(strTCP_Buffer, "\\r\\n", strTAB);                                               
  strTCP_Buffer = FindandReplaceALL(strTCP_Buffer, "\\n", strTAB);                   

  found = strTCP_Buffer.find(strETX);
  while (found != string::npos)
  {
   strSendPacket = strTCP_Buffer.substr(0,found+1);
   AMIDataPacket.intPortNum 	                  	= 1;
   AMIDataPacket.intSideAorB 	          		= 0;
   AMIDataPacket.stringDataIn                  		= strSendPacket;
   AMIDataPacket.intLength                     		= strSendPacket.length();
   AMIDataPacket.IPAddress.stringAddress       		= strASTERISK_AMI_IP_ADDRESS;
   boolContentPacket      				= IsCLIContentTypePacket(strSendPacket);
   boolRegistrationPacket 				= IsCLIregistrationPacket(strSendPacket);

   if (!boolContentPacket)
    {
     // send downrange to ANI
     intRC = sem_wait(&sem_tMutexANIPortDataQ);
     if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIPortDataQ, "sem_wait@sem_tMutexANIPortDataQ in ANI_UDPPort", 1);}
     queueANIPortData[1].push(AMIDataPacket);
     sem_post(&sem_tMutexANIPortDataQ);
     sem_post(&sem_tANIFlag);
    }


   strTCP_Buffer.erase(0,found+1);
   found = strTCP_Buffer.find(strETX);
  } // end while



}

/****************************************************************************************************
*
* Name:
*  Function: Port_Data ExperientTCPPort::fPortData()
*
* Description:
*   An ExperientTCPPort Function 
*
*
* Details:
*   This function loads the values of ExperientTCPPort members into a Port_Data object and
*   returns the object.  Used for passing multiple variables to different class functions.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolVendorEmailAddress                       Local   - <ExperientTCPPort> boolean       
*  boolVendorEmailAddress                       Local   - <Port_Data> boolean
*  enumPortType                                 Local   - <ExperientTCPPort><threadorPorttype>   enumeration member
*  enumPortType                                 Local   - <Port_Data><threadorPorttype>          enumeration member
*  intPortNum                                   Local   - <ExperientTCPPort> integer             member
*  intPortNum                                   Local   - <Port_Data> integer                    member
*  intSideAorB                                  Local   - <ExperientTCPPort> integer             member
*  intSideAorB                                  Local   - <Port_Data> integer                    member
*  objPortData                                  Local   - <ExperientTCPPort><Port_Data>          object
*  strSideAorB                                  Local   - <Port_Data><cstring>                   member
*  strVendorEmailAddress                        Local   - <ExperientTCPPort><IP_Address>         object
*  strVendorEmailAddress                        Local   - <Port_Data><IP_Address>                object
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
Port_Data ExperientTCPPort::fPortData()
{
 objPortData.intPortNum   = intPortNum;
 objPortData.intSideAorB  = intSideAorB;

 switch(intSideAorB)
  {
   case 1: objPortData.strSideAorB = "A"; break;
   case 2: objPortData.strSideAorB = "B"; break;
   case 3: objPortData.strSideAorB = "S"; break;
   default:objPortData.strSideAorB = " ";
  }
 objPortData.enumPortType           = enumPortType;
 objPortData.boolVendorEmailAddress = boolVendorEmailAddress;
 objPortData.strVendorEmailAddress  = strVendorEmailAddress;
 return objPortData;
}
/****************************************************************************************************
*
* Name:
*  Function: Port_Data ExperientTCPPort::fPortPairData()
*
* Description:
*   An ExperientTCPPort Function 
*
*
* Details:
*   This function loads the values of ExperientTCPPort members into a Port_Data object and
*   returns the object.  Used for passing multiple variables to different class functions.
*   used to descibe the pair versus an individual port.       
*    
* Parameters:
*  none
*
* Variables:
*  boolVendorEmailAddress                       Local   - <ExperientTCPPort> boolean       
*  boolVendorEmailAddress                       Local   - <Port_Data> boolean
*  enumPortType                                 Local   - <ExperientTCPPort><threadorPorttype>   enumeration member
*  enumPortType                                 Local   - <Port_Data><threadorPorttype>          enumeration member
*  intPortNum                                   Local   - <ExperientTCPPort> integer             member
*  intPortNum                                   Local   - <Port_Data> integer                    member
*  intSideAorB                                  Local   - <ExperientTCPPort> integer             member
*  intSideAorB                                  Local   - <Port_Data> integer                    member
*  objPortPairData                              Local   - <ExperientTCPPort><Port_Data>          object
*  strSideAorB                                  Local   - <Port_Data><cstring>                   member
*  strVendorEmailAddress                        Local   - <ExperientTCPPort><IP_Address>         object
*  strVendorEmailAddress                        Local   - <Port_Data><IP_Address>                object
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2011 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2011 Experient Corporation
****************************************************************************************************/
Port_Data ExperientTCPPort::fPortPairData()
{
 Port_Data objPortPairData;

 objPortPairData.intPortNum             = intPortNum;
 objPortPairData.strSideAorB            = "";
 objPortPairData.enumPortType           = enumPortType;
 objPortPairData.boolVendorEmailAddress = boolVendorEmailAddress;
 objPortPairData.strVendorEmailAddress  = strVendorEmailAddress;
 return objPortPairData;
}
/****************************************************************************************************
*
* Name:
*  Function: void ExperientTCPPort::fSet_Port_Down()
*
* Description:
*   An ExperientTCPPort Function 
*
*
* Details:
*   This function places flags the port into a down status with a time hack.  If the port is
*   already down the function returns with no action.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolPortActive                             Local   - <ExperientTCPPort> boolean  member
*  boolPortWasDown                            Local   - <ExperientTCPPort> boolean  member
*  CLOCK_REALTIME                             Library - <ctime> constant
*  timespecTimePortDown                       Local   - <ExperientTCPPort> <ctime>  struct timespec  member 
*                                                                 
* Functions:
*   clock_gettime()                           Library - <cstring>
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientTCPPort::fSet_Port_Down()
{
 if(!boolPortActive){return;}
 clock_gettime(CLOCK_REALTIME, &timespecTimePortDown);
 boolPortActive  = false;
 boolPortWasDown = true;
}
/****************************************************************************************************
*
* Name:
*  Function: bool ExperientTCPPort::fPingStatus()
*
* Description:
*   An ExperientTCPPort Function 
*
*
* Details:
*   This function checks pings the remote IP address of the port.  The result of the ping is set 2 ways.  
*   Through the return boolean, and the ExperientTCPPort::boolPingStatus boolean.
*          
*    
* Parameters:
*  none
*
* Variables:
*  PingPort                                   Local   - <IPWorks><Ping>                         object
*  intPING_TIMEOUT                            Global  - <defines.h>                             Ping timeout seconds
*  intRC                                      Local   - integer                                 return code
*  Remote_IP                                  Local   - <ExperientTCPPort><IP_Address>          object
*  stringAddress                              Local   - <ExperientTCPPort><IP_Address><cstring> object member 
*                                                                
* Functions:
*  .Config()                                  Library  - <ipworks.h><Ping>    (int)
*  .c_str()                                   Library  - <cstring>            (const char*)
*  GetLocalHost()                             Library  - <ipworks.h><Ping>    (char*)
*  .PingHost()                                Library  - <ipworks.h><Ping>    (int)
*  .SetLocalHost()                            Library  - <ipworks.h><Ping>    (int)
*  .SetPacketSize()                           Library  - <ipworks.h><Ping>    (int)                        
*  .SetTimeout()                              Library  - <ipworks.h><Ping>    (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
bool ExperientTCPPort::fPingStatus()
{
 Ping   PingPort;
 int    intRC;
 string strKey = IPWORKS_RUNTIME_KEY;

#ifdef IPWORKS_V16
 PingPort.SetRuntimeLicense((char*)strKey.c_str());
#endif
 PingPort.SetLocalHost(GetLocalHost());
 PingPort.SetTimeout(intPING_TIMEOUT);
 PingPort.SetPacketSize(1024);
 PingPort.Config((char*)"AbsoluteTimeout=true");

 intRC = PingPort.PingHost((char*)Remote_IP.stringAddress.c_str());

 if(intRC){boolPingStatus = false;return false;}
 else     {boolPingStatus = true ;return true;}
}
/****************************************************************************************************
*
* Name:
*  Function: bool ExperientTCPPort::fPingStatusString()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function checks the ExperientTCPPort::boolPingStatus boolean value and returns a string
*   representing the results of a previous Ping from the localhost to the remote IP address.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolPingStatus                                  Local   - <ExperientTCPPort> boolean
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string ExperientTCPPort::fPingStatusString()
{
 if (boolPingStatus){return "Successful";}
 else               {return "Failed";}
}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//                                                                          ExperientUDPPort FUNCTIONS
//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//assignment operator
ExperientUDPPort& ExperientUDPPort::operator=(const ExperientUDPPort& a)
{
 if (&a == this ) {return *this;}
 intPortNum				= a.intPortNum;
 intSideAorB				= a.intSideAorB;
 enumPortType				= a.enumPortType;
 enumANISystem				= a.enumANISystem;
 boolBufferPreviouslyCleared		= a.boolBufferPreviouslyCleared;
 stringBuffer				= a.stringBuffer;
 stringHalfBuffer			= a.stringHalfBuffer;
 timespecBufferTimeStamp.tv_sec		= a.timespecBufferTimeStamp.tv_sec;
 timespecBufferTimeStamp.tv_nsec	= a.timespecBufferTimeStamp.tv_nsec;
 boolOutsideIPTraffic			= a.boolOutsideIPTraffic;
 stringOutsideIPAddr			= a.stringOutsideIPAddr;
 timespecTimePortDown.tv_sec		= a.timespecTimePortDown.tv_sec;
 timespecTimePortDown.tv_nsec		= a.timespecTimePortDown.tv_nsec;
 boolPortAlarm				= a.boolPortAlarm;
 boolPortActive				= a.boolPortActive;
 boolPortWasDown			= a.boolPortWasDown;
 intPortDownThresholdSec		= a.intPortDownThresholdSec;
 intPortDownReminderThreshold		= a.intPortDownReminderThreshold;
 boolPingStatus				= a.boolPingStatus;
 Remote_IP				= a.Remote_IP;
 intDiscardedPackets			= a.intDiscardedPackets;
 sem_tMutexDiscardedPackets		= a.sem_tMutexDiscardedPackets;
 strVendorEmailAddress			= a.strVendorEmailAddress;
 boolVendorEmailAddress			= a.boolVendorEmailAddress;
 objPortData				= a.objPortData;
 boolAudiocodesRegistered		= a.boolAudiocodesRegistered;
 sem_tPortLock				= a.sem_tPortLock;
 return *this;
}
void ExperientUDPPort::fInitializeCADport(int i, bool boolSKIPACTIVE)
{
 int                                    intRC;
 string                                 strKey = IPWORKS_RUNTIME_KEY;
 extern ExperientCommPort               CADPort[NUM_CAD_PORTS_MAX+1];
 extern IP_Address                      MULTICAST_GROUP;

 cout << "INITIALIZE CAD UDP PORT" << endl;
 enumPortType                       = CAD;
 intPortNum                         = i;
 boolPingStatus                     = false; 
 Remote_IP.stringAddress            = CADPort[i].RemoteIPAddress.stringAddress;
#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
 Config((char*)"QOSFlags=16||160");
 Config((char*)"MaxPacketSize=1500");
 Config((char*)"InBufferSize=655360");
 Config((char*)"OutBufferSize=655360");
 SetLocalHost(NULL);
 SetLocalPort(CADPort[i].intLocalPort);
 SetRemoteHost((char*)CADPort[i].RemoteIPAddress.stringAddress.c_str());
 SetRemotePort(CADPort[i].intRemotePortNumber);
 stringBuffer                       = "";
 intDiscardedPackets                = 0;
 fPortData();

 if (boolSKIPACTIVE) {return;}
 
 intRC = SetActive(true);
 if (intRC){Error_Handler();}

 if (boolMULTICAST)
  {
   intRC = SetMulticastGroup((char*)MULTICAST_GROUP.stringAddress.c_str());
   if (intRC){Error_Handler();}
  }



}

/*
void ExperientTCPPort::fInitializeDSBport() {
 extern int intDSB_PORT_DOWN_THRESHOLD_SEC;
 extern int intDSB_PORT_DOWN_REMINDER_SEC;

 string                             strKey = IPWORKS_RUNTIME_KEY;
 //cout << "INITIALIZING DSB PORT" << endl;
 enumPortType                       = DSB;
 intPortNum                         = 1;
 boolPingStatus                     = false;
 boolPortWasDown                    = false;
 intPortDownThresholdSec            = intDSB_PORT_DOWN_THRESHOLD_SEC;
 intPortDownReminderThreshold       = intDSB_PORT_DOWN_REMINDER_SEC;
#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
 Config((char*)"InBufferSize=655360");
 Config((char*)"OutBufferSize=655360");
 Config((char*)"KeepAliveRetryCount=5");
 Config((char*)"KeepAliveTime=5000");
 Config((char*)"KeepAliveInterval=1000");
 Config((char*)"AbsoluteTimeout=true");

 SetTimeout(4);
 SetLocalHost(NULL);
 SetLocalPort(intLocalPortNumber); 
 fPortData();
 sem_init(&sem_tMutexTCPPortBuffer,0,1);
 sem_init(&sem_tMutexBadCharacterCount,0,1);
 SetRemoteHost((char*)Remote_IP.stringAddress.c_str());
 SetRemotePort(intRemotePortNumber);
 SetLinger(false);
 fSet_Port_Active();
 clock_gettime(CLOCK_REALTIME, &timespecBufferTimeStamp);
}

*/
void ExperientUDPPort::fInitializeANIport(int i) {
 int                                    intRC;
 string                                 strKey = IPWORKS_RUNTIME_KEY;
 extern ExperientCommPort               ANIPort[NUM_ANI_PORTS_MAX+1];
// //cout << "init ANI port" << endl;
//   ANIPort[i].RemoteIPAddress.stringAddress               = strASTERISK_AMI_IP_ADDRESS;
   ANIPort[i].intPortNum                                  = i;
   ANIPort[i].enumPortType                                = ANI;
   ANIPort[i].ePortConnectionType                         = UDP; 
   ANIPort[i].boolReducedHeartBeatMode                    = false;
   ANIPort[i].boolReducedHeartBeatModeFirstAlarm          = false;
   ANIPort[i].boolEmergencyShutdown                       = false;
 //  ANIPort[i].intHeartbeatInterval                        = intANI_HEARTBEAT_INTERVAL_SEC;

   ANIPort[i].fSetPortActive();
//   ANIPort[i].intPortDownReminderThreshold                = intANI_PORT_DOWN_REMINDER_SEC;
//   ANIPort[i].intPortDownThresholdSec                     = intANI_PORT_DOWN_THRESHOLD_SEC;
   ANIPort[i].boolUnexpectedDataShowMessage               = true;
   ANIPort[i].intUnexpectedDataCharacterCount             = 0; 
   ANIPort[i].boolAudiocodesRegistered                    = false;
   ANIPort[i].boolReadyToShutDown                         = false;
   ANIPort[i].intRemotePortNumber                         = FREESWITCH_CLI_PORT_NUMBER;
   ANIPort[i].UDP_Port.enumPortType                       = ANI;
   ANIPort[i].UDP_Port.enumANISystem                      = enumANI_SYSTEM;
   ANIPort[i].UDP_Port.intPortNum                         = i;
   ANIPort[i].UDP_Port.stringBuffer                       = "";
   ANIPort[i].UDP_Port.boolPingStatus                     = false;
   ANIPort[i].UDP_Port.Remote_IP.stringAddress            = ANIPort[i].RemoteIPAddress.stringAddress;
   ANIPort[i].UDP_Port.intDiscardedPackets                = 0;
#ifdef IPWORKS_V16
   ANIPort[i].UDP_Port.SetRuntimeLicense((char*)strKey.c_str());
#endif
   ANIPort[i].UDP_Port.Config((char*)"QOSFlags=16||160");						// critical..minimize delay
   ANIPort[i].UDP_Port.Config((char*)"InBufferSize=655360");
   ANIPort[i].UDP_Port.Config((char*)"OutBufferSize=655360");
   ANIPort[i].UDP_Port.Config((char*)"MaxPacketSize=1500");
   ANIPort[i].UDP_Port.SetLocalHost(NULL);

   ANIPort[i].UDP_Port.SetRemoteHost((char*)ANIPort[i].UDP_Port.Remote_IP.stringAddress.c_str());

   ANIPort[i].UDP_Port.SetRemotePort(FREESWITCH_CLI_PORT_NUMBER);

   ANIPort[i].UDP_Port.fPortData();

   clock_gettime(CLOCK_REALTIME, &ANIPort[i].timespecTimeLastHeartbeat);

  // not needed for aniport used for tracking the freeswitch data TCP port
 //  intRC = ANIPort[i].UDP_Port.SetActive(true);
 //  if (intRC){ANIPort[i].UDP_Port.Error_Handler();}


}


void ExperientUDPPort::fInitializeALIPort(int i, int j)
{
 int                                    intRC;
 string                                 strKey = IPWORKS_RUNTIME_KEY;
 extern ExperientCommPort               ALIPort[intMAX_ALI_PORT_PAIRS  + 1 ] [ 2 + 1 ];
 extern ExperientCommPort               ALIServicePort[intMAX_ALI_PORT_PAIRS  + 1 ];
 extern IP_Address                      MULTICAST_GROUP;

 enumPortType                       = ALI;
 intPortNum                         = i;
 intSideAorB                        = j;
 intDiscardedPackets                = 0;
 boolPingStatus                     = false;
 Config((char*)"QOSFlags=16||160");						// critical..minimize delay
 Config((char*)"MaxPacketSize=1500");
 Config((char*)"InBufferSize=655360");
 Config((char*)"OutBufferSize=655360");
 SetLocalHost(NULL);
 stringBuffer                       = "";
 fPortData();

 switch (j)
  {
   case 1: case 2:
          ////cout << "setting ALI port !" << endl;
#ifdef IPWORKS_V16
          SetRuntimeLicense((char*)strKey.c_str());
#endif
          SetLocalPort(ALIPort[i] [j].intLocalPort);
          SetRemoteHost((char*)ALIPort[i] [j].RemoteIPAddress.stringAddress.c_str());
          SetRemotePort(ALIPort[i] [j].intRemotePortNumber);
          Remote_IP.stringAddress  = ALIPort[i] [j].RemoteIPAddress.stringAddress;
          break;
   case 3:
#ifdef IPWORKS_V16
          SetRuntimeLicense((char*)strKey.c_str());
#endif
          SetLocalPort(ALIServicePort[i].intLocalPort);
          SetRemoteHost((char*)ALIServicePort[i].RemoteIPAddress.stringAddress.c_str());
          SetRemotePort(ALIServicePort[i].intRemotePortNumber);
          Remote_IP.stringAddress  = ALIServicePort[i].RemoteIPAddress.stringAddress;
          break;
   default:
         SendCodingError("Coding error in fn ExperientUDPPort::fInitializeALIPort() 1 2 or 3 ");      
  }         
 intRC = SetActive(true);
 if (intRC){Error_Handler();}   

 if (boolMULTICAST)
  {
   intRC = SetMulticastGroup((char*)MULTICAST_GROUP.stringAddress.c_str());
   if (intRC){Error_Handler();}
  }
}


/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::fQueue_Transmission(DataPacketIn UDPPortDataReceived )
*
* Description:
*   An ExperientUDPPort Function used by function virtual int MCast::FireDataIn(MCastDataInEventParams *e)
*
*
* Details:
*   This function places the data recieved from the IPworks modified function into the appropriate Data Queue
*   and fires a signal to the associated thread to indicate the arrival of the data.
*          
*    
* Parameters:
*   UDPPortDataReceived                       <baseclasses.h><DataPacketIn> UDP Data Received                                                         
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  ANI                                        Global  - <threadorPorttype> enumeration member
*  CAD				              Global  - <threadorPorttype> enumeration member												
*  enumPortType                               Local   - <ExperientUDPPort><threadorPorttype> enumeration
*  intPortNum                                 Local   - <ExperientUDPPort> integer Port number
*  intRC                                      Local   - integer  function return code
*  queueALIPortData[]                         Global  - array of queue <DataPacketIn> the ALI ports Data queue
*  queueANIPortData[]                         Global  - array of queue <DataPacketIn> the ANI ports Data queue
*  queueCadPortData[]                         Global  - array of queue <DataPacketIn> the CAD ports Data queue
*  queue_ServerStatusData                     Global  - queue <DataPacketIn> the SYC port 1 Data queue
*  queueWRKPortData                           Global  - queue <DataPacketIn> the WRK port Data queue
*  sem_tAliFlag                               Global  - <globals.h><sem_t> ALI thread Signal semaphore
*  sem_tANIFlag                               Global  - <globals.h><sem_t> ANI thread Signal semaphore
*  sem_tCadFlag                               Global  - <globals.h><sem_t> CAD thread Signal semaphore
*  sem_tServerHeartbeatRecievedFlag           Global  - <globals.h><sem_t> SYC thread Signal semaphore
*  sem_tGatewayInterrogateReplyFlag           Global  - <globals.h><sem_t> Server_Status_Gateway_InterrogateReply_Response_Thread Signal semaphore
*  sem_tWRKFlag                               Global  - <globals.h><sem_t> WRK thread Signal semaphore
*  sem_tMutexAliPortDataQ                     Global  - <globals.h><sem_t> mutex semaphore
*  sem_tMutexANIPortDataQ                     Global  - <globals.h><sem_t> mutex semaphore
*  sem_tMutex_ServerHeartbeatDataQ            Global  - <globals.h><sem_t> mutex semaphore
*  sem_tMutex_GatewayInterrogateReplyQ        Global  - <globals.h><sem_t> mutex semaphore
*  sem_tMutexCadPortDataQ                     Global  - <globals.h><sem_t> mutex semaphore
*  sem_tMutexWRKPortDataQ                     Global  - <globals.h><sem_t> mutex semaphore
*  WRK                                        Global  - <threadorPorttype> enumeration member
*  SYC                                        Global  - <threadorPorttype> enumeration member 
*                         
* Functions:
*   Semaphore_Error()                         Global  - <globalfunctions.h>           (void)
*   sem_wait()				      Library - <semaphore.h>                 (int)
*   sem_post()				      Library - <semaphore.h>                 (int)
*   .push()				      Library - <queue>                       (void)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientUDPPort::fQueue_Transmission(DataPacketIn UDPPortDataReceived )
{
 int    intRC;

 switch (enumPortType)
  {
   case CAD:
        intRC = sem_wait(&sem_tMutexCadPortDataQ);
        if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadPortDataQ, "sem_wait@sem_tMutexCadPortDataQ in Cad_UDPPort", 1);}
        queueCadPortData[intPortNum].push(UDPPortDataReceived);
        sem_post(&sem_tMutexCadPortDataQ);
        sem_post(&sem_tCadFlag);
        break;

   case ALI:
        intRC = sem_wait(&sem_tMutexAliPortDataQ);
        if (intRC){Semaphore_Error(intRC, ALI, &sem_tMutexAliPortDataQ, "sem_wait@sem_tMutexCadPortDataQ in ALI_UDPPort", 1);}
        queueALIPortData[intPortNum].push(UDPPortDataReceived);
        sem_post(&sem_tMutexAliPortDataQ);
        sem_post(&sem_tAliFlag);
        break;

   case ANI:
        intRC = sem_wait(&sem_tMutexANIPortDataQ);
        if (intRC){Semaphore_Error(intRC, ANI, &sem_tMutexANIPortDataQ, "sem_wait@sem_tMutexANIPortDataQ in ANI_UDPPort", 1);}
        queueANIPortData[intPortNum].push(UDPPortDataReceived);
        sem_post(&sem_tMutexANIPortDataQ);
        sem_post(&sem_tANIFlag);
        break;
/*
    case AGI:
         intRC = sem_wait(&sem_tMutexConfAssignmentQ);
         if (intRC){Semaphore_Error(intRC, AGI, &sem_tMutexConfAssignmentQ, "sem_wait@sem_tMutexConfAssignmentQ in AGI_UDPPort", 1);}
         queue_ConfAssignmentRequestData.push(UDPPortDataReceived);
         sem_post(&sem_tMutexConfAssignmentQ);
         sem_post(&sem_tConfAssignmentFlag);
         break;
*/
   case WRK:
        intRC = sem_wait(&sem_tMutexWRKPortDataQ);
        if (intRC){Semaphore_Error(intRC, WRK, &sem_tMutexWRKPortDataQ, "sem_wait@sem_tMutexWRKPortDataQ in WRK_UDPPort", 1);}
        queueWRKPortData.push(UDPPortDataReceived);
        sem_post(&sem_tMutexWRKPortDataQ);
        sem_post(&sem_tWRKFlag);
        break;
   
//   case SYC:
//        switch(UDPPortDataReceived.intPortNum)
//         {
//          case 1:
//               intRC = sem_wait(&sem_tMutex_ServerHeartbeatDataQ);
//               if (intRC){Semaphore_Error(intRC, SYC, &sem_tMutex_ServerHeartbeatDataQ, "sem_wait@sem_tMutex_ServerHeartbeatDataQ in SYC_UDPPort", 1);}
//               queue_ServerStatusData.push(UDPPortDataReceived);
//               sem_post(&sem_tMutex_ServerHeartbeatDataQ);
//               sem_post(&sem_tServerHeartbeatRecievedFlag);
//               break;
//          case 2:
//               intRC = sem_wait(&sem_tMutex_GatewayInterrogateReplyQ);
//               if (intRC){Semaphore_Error(intRC, SYC, &sem_tMutex_GatewayInterrogateReplyQ, "sem_wait@sem_tMutex_GatewayInterrogateReplyQ in SYC_UDPPort", 1);}
//               queue_GatewayInterrogateReplyData.push(UDPPortDataReceived);
//               sem_post(&sem_tMutex_GatewayInterrogateReplyQ);
//               sem_post(&sem_tGatewayInterrogateReplyFlag);
//               break;
//         
 //
//          default:
//               //cout << "UDPPortDataReceived.intPortNum not defined in fQueue_Transmission" << endl;                        // test 
//         }// end switch(UDPPortDataReceived.intPortNum)
//        break;
   default:
    SendCodingError( "CommPortClasses.cpp - UDP Port Type not defined in fQueue_Transmission");                        // test
    break;

  }//end switch 

}// end ExperientUDPPort::fQueue_Transmission()

/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::fCheck_Buffer()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function checks the ExperientUDPPort::stringBuffer for latency.  If the the buffer contains data without receiving
*   data for a specified period of time it is flushed to the corresponding thread. 
*   note:
*        The ALI, ANI & CAD ports are the only ports
*        that utilize buffering.
*          
*    
* Parameters:
*  none
*
* Variables:
*  ALI                                        Global  - <threadorPorttype>                   enumeration member
*  ANI                                        Global  - <threadorPorttype>                   enumeration member
*  CAD				              Global  - <threadorPorttype>                   enumeration member	
*  ASTERISK                                   Global  - <header.h><ani_type>                 enumeration member
*  ALI_MESSAGE_241                            Global  - <defines.h>                          log message
*  KRN_MESSAGE_162                            Global  - <defines.h>                          log message
*  boolBufferPreviouslyCleared                Local   - <ExperientUDPPort> boolean           buffer had been cleared previously
*  boolStringBufferExceeded                   Local   - <DataPacketIn> boolean               object member
*  BufferData                                 Local   - <DataPacketIn>                       object - Data Flushed from the ExperientUDPPort Buffer
*  CLOCK_REALTIME                             Library - <ctime>                              constant
*  doubleBUFFER_FORCE_TRANSMIT                Global  - <defines.h>                          Time in sec data can remain in buffer
*  dblBufferForceTransmit                     Local   - double                               Time in sec data can remain in buffer
*  enumANISystem                              Local   - <ExperientUDPPort><ani_type>         Type of ANI system installed
*  enumPortType                               Local   - <ExperientUDPPort><threadorPorttype> enumeration member
*  intLength                                  Local   - <DataPacketIn> integer               object member
*  intLogCode                                 Local   - integer                              Logging type code
*  intMessageCode                             Local   - integer                              Message Number Code
*  intPortNum                                 Local   - <ExperientUDPPort> integer           object member
*  intPortNum                                 Local   - <DataPacketIn> integer               object member
*  intSideAorB                                Local   - <ExperientUDPPort> integer           object member 
*  intSideAorB                                Local   - <DataPacketIn> integer               object member 
*  intRC                                      Local   - integer                              function return code
*  ldoubleTimeDiff                            Local   - long double                          difference between time hacks
*  LOG_CONSOLE_FILE                           Global  - <defines.h>                          log type code
*  LOG_WARNING                                Global  - <defines.h>                          log type code
*  objBLANK_CALL_RECORD                       Global  - extern <Call_Data>                   blank call record
*  objMessage                                 Local   - <MessageClass>                       Messaging object
*  sem_tMutexUDPPortBuffer                    Local   - <ExperientUDPPort><sem_t>            mutex semaphore
*  stringBuffer                               Local   - <ExperientUDPPort><cstring>          object member (the buffer)
*  stringDataIn                               Local   - <DataPacketIn> <cstring>             object member 
*  stringSideAorB                             Local   - <cstring>                            Used to identify ALI ports
*  timespecBufferTimeStamp                    Local   - <ExperientUDPPort><ctime>            time last piece of data was placed in buffer
*  timespecTimeNow                            Local   - <ctime> struct timespec              Current time
*                                                                    
* Functions:
*  .clear()                                   Library - <cstring>                     (*this)
*  clock_gettime()                            Library - <ctime>                       (int)
*  .empty()                                   Library - <cstring>                     (bool)
*  enQueue_Message()                          Global  - <globalfunctions.h>           (void)
*  .find()                                    Library - <cstring>                     (size_t)
*  .fMessage_Create()                         Local   - <MessageClass>                (void)
*  fPortData()                                Local   - <ExperientUDPPort>            (Port_Data)
*  fQueue_Transmission()                      Local   - <ExperientUDPPort>            (void)
*  int2str()                                  Global  - <globalfunctions.h>           (int)
*   Semaphore_Error()                         Global  - <globalfunctions.h>           (void)
*   sem_wait()				      Library - <semaphore.h>                 (int)
*   sem_post()				      Library - <semaphore.h>                 (int)
*   time_difference()                         Global  - <globalfunctions.h>           (long double)
*   .push()				      Library - <queue>                       (void)

* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientUDPPort::fCheck_Buffer()
{
 struct timespec             timespecTimeNow;
 long double                 ldoubleTimeDiff;
 int                         intRC;
 DataPacketIn                BufferData;
 MessageClass                objMessage;
 int                         intLogCode;
 int                         intMessageCode = 0;
 string                      stringSideAorB = "";
 double                      dblBufferForceTransmit = doubleBUFFER_FORCE_TRANSMIT;
 

 intRC = sem_wait(&sem_tPortLock);
 if (intRC){Semaphore_Error(intRC, enumPortType, &sem_tPortLock, "sem_wait@sem_tPortLock in ExperientUDPPort::fCheck_Buffer()", 1);}

 if (stringBuffer.empty()){sem_post(&sem_tPortLock);return;}
            
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 ldoubleTimeDiff = time_difference(timespecTimeNow, timespecBufferTimeStamp);

 //asterisk ANI gets twice as long
 if ((enumANISystem == ASTERISK)&&(enumPortType == ANI)) {dblBufferForceTransmit *= 2;}

 // clear buffer and queue to thread if time exceeded 
 if (ldoubleTimeDiff > dblBufferForceTransmit)
  {
   
   BufferData.intPortNum                = intPortNum;
   BufferData.intSideAorB               = intSideAorB;
   BufferData.stringDataIn              = stringBuffer;
   BufferData.intLength                 = stringBuffer.length();
   BufferData.boolStringBufferExceeded  = false;

   stringBuffer.clear();

   fQueue_Transmission(BufferData);
    
     switch (enumPortType)
      {
       case ALI:
            if (intSideAorB == 1){stringSideAorB = "A";}else{stringSideAorB = "B";}
            if ((!boolIGNORE_BAD_ALI)&&(BufferData.intLength > intDEFAULT_ALI_NOISE_THRESHOLD))
             {
              if(boolBufferPreviouslyCleared){intLogCode = LOG_CONSOLE_FILE;} else {intLogCode = LOG_WARNING;}
               objMessage.fMessage_Create(intLogCode, 241, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                          fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                          ALI_MESSAGE_241, int2str(BufferData.intLength));
               enQueue_Message(ALI,objMessage);
             }
            timespecBufferTimeStamp = timespecTimeNow;
            boolBufferPreviouslyCleared = true;
            sem_post(&sem_tPortLock);
            return;
       case ANI:
            intMessageCode = 347;break;
       case CAD:
            intMessageCode = 466;break;
       default:
            SendCodingError( "CommPortClasses.cpp - Coding Error in ExperientUDPPort::fCheck_Buffer()");
      }// end switch    
   
     if(boolBufferPreviouslyCleared){intLogCode = LOG_CONSOLE_FILE;} else {intLogCode = LOG_WARNING;}

   objMessage.fMessage_Create(intLogCode, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              KRN_MESSAGE_162, int2str(BufferData.intLength));
   enQueue_Message(enumPortType,objMessage);
   timespecBufferTimeStamp = timespecTimeNow;
   boolBufferPreviouslyCleared = true;
  }

 sem_post(&sem_tPortLock);
 
}// end ExperientUDPPort::fCheck_Buffer()

/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::fSet_Port_Down()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function places flags the port into a down status with a time hack.  If the port is
*   already down the function returns with no action.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolPortActive                             Local   - <ExperientUDPPort> boolean  member
*  boolPortWasDown                            Local   - <ExperientUDPPort> boolean  member
*  CLOCK_REALTIME                             Library - <ctime> constant
*  timespecTimePortDown                       Local   - <ExperientUDPPort> <ctime>  struct timespec  member 
*                                                                 
* Functions:
*   clock_gettime()                           Library - <cstring>
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientUDPPort::fSet_Port_Down()
{
 if(!boolPortActive){return;}
 clock_gettime(CLOCK_REALTIME, &timespecTimePortDown);
 boolPortActive  = false;
 boolPortWasDown = true;
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::fCheck_Time_Port_Down()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function checks how long a port has been in a down status and if certain time levels have been
* exceeded sends out reminder warning messages.
*   If a port was down but now is 'UP' a alarm cleared message is generated and port down flags are cleared.
*  
*  note: this function is used for the ports that do not utilize a reduced heartbeat mode. (WRK, SYC, MAIN)
*
*        CAD and ANI utilize <globalfunctions.h>   void Check_Port_Down_Notification()
*        ALI         utilizes <ali.cpp>            void Check_ALI_Port_Down_Notification()
*          
*    
* Parameters:
*  none
*
* Variables:
*  MAIN                                       Global  - <header.h><threadorPorttype>         enumeration member
*  SYC                                        Global  - <header.h><threadorPorttype>         enumeration member
*  WRK                                        Global  - <header.h><threadorPorttype>         enumeration member
*  boolPortActive                             Local   - <ExperientUDPPort>                   boolean  member
*  boolPortAlarm                              Local   - <ExperientUDPPort>                   boolean  member 
*  boolPortWasDown                            Local   - <ExperientUDPPort>                   boolean  member
*  CLOCK_REALTIME                             Library - <ctime>                              constant
*  enumPortType                               Local   - <ExperientUDPPort><threadorPorttype> enumeration member
*  intLogCode                                 Local   - integer                              Logging type code
*  intPortDownReminderThreshold               Local   - <ExperientUDPPort> integer           object member seconds to generate a reminder
*  intMessageCode                             Local   - integer                              Message Number Code
*  intPortDownThresholdSec                    Local   - <ExperientUDPPort> integer           seconds to generate an Alarm when port first goes down
*  intBEACON_PORT_DOWN_REMINDER_SEC           Global  - <globals.h>                          seconds to generate a reminder
*  intSYC_PORT_DOWN_REMINDER_SEC              Global  - <globals.h>                          seconds to generate a reminder
*  intWRK_PORT_DOWN_REMINDER_SEC              Global  - <globals.h>                          seconds to generate a reminder
*  KRN_MESSAGE_120                            Global  - <defines.h>                          log message
*  KRN_MESSAGE_121                            Global  - <defines.h>                          log message
*  ldTimeDiff                                 Local   - long double                          time difference
*  LOG_ALARM                                  Global  - <defines.h>                          log type code
*  LOG_MESSAGE_504                            Global  - <defines.h>                          log message
*  LOG_MESSAGE_525                            Global  - <defines.h>                          log message
*  LOG_WARNING                                Global  - <defines.h>                          log type code
*  objBLANK_CALL_RECORD                       Global  - extern <Call_Data>                   blank call record
*  objMessage                                 Local   - <MessageClass>                       Messaging object
*  stringMessage                              Local   - <cstring>                            used to store log message
*  timespecTimeNow                            Local   - <ctime>                              struct timespec time now
*  timespecTimePortDown                       Local   - <ExperientUDPPort> <ctime>           struct timespec  member 
*                                                                 
* Functions:
*  clock_gettime()                            Library - <cstring>                     (int)
*  enQueue_Message()                          Global  - <globalfunctions.h>           (void)
*  .fMessage_Create()                         Local   - <MessageClass>                (void)
*  fPingStatus()                              Local   - <ExperientUDPPort>            (void)
*  fPingStatusString()                        Local   - <ExperientUDPPort>            (string)
*  fPortData()                                Local   - <ExperientUDPPort>            (Port_Data)
*  seconds2time()                             Global  - <globalfunctions.h>           (string)
*  time_difference()                          Global  - <globalfunctions.h>           (long double)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientUDPPort::fCheck_Time_Port_Down()
{
 struct timespec  timespecTimeNow;
 MessageClass     objMessage;
 int              intLogCode = LOG_WARNING;;
 int              intMessageCode; 
 long double      ldTimeDiff;
 string           stringMessage;

 // Port is UP
 if(boolPortActive)
  {
   // but had been down
   if(boolPortWasDown)
    {
     switch(enumPortType)
      {
       case WRK:
            if(boolPortAlarm){intMessageCode = 720;stringMessage = LOG_MESSAGE_504;}else {intMessageCode = 723;stringMessage = LOG_MESSAGE_525;}
            intPortDownReminderThreshold = intWRK_PORT_DOWN_REMINDER_SEC;
            break;
       case SYC:
            if(boolPortAlarm){intMessageCode = 609;stringMessage = LOG_MESSAGE_504;} else {intMessageCode = 612;stringMessage = LOG_MESSAGE_525;}
            intPortDownReminderThreshold = intSYC_PORT_DOWN_REMINDER_SEC;
            break;
       case MAIN:
            if(boolPortAlarm){intMessageCode = 111;stringMessage = LOG_MESSAGE_504;} else {intMessageCode = 112;stringMessage = LOG_MESSAGE_525;}
            intPortDownReminderThreshold = intBEACON_PORT_DOWN_REMINDER_SEC;
            break;
       default:
            SendCodingError( "CommPortClasses.cpp - Coding error in ExperientUDPPort::fCheck_Time_Port_Down() 1st switch(enumPortType) -> "+ int2str(enumPortType));
            return;
        }// end switch
     if(boolPortAlarm){intLogCode = LOG_ALARM;}
     objMessage.fMessage_Create(intLogCode, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringMessage);
     enQueue_Message(enumPortType,objMessage);
     boolPortWasDown = false; boolPortAlarm = false;
    }// end if(boolPortWasDown)
   
   return;
  }// end if(boolPortActive)
   
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 ldTimeDiff = time_difference(timespecTimeNow, timespecTimePortDown);
 fPingStatus();

 // Port Down not yet alarm ... check time
 if(!boolPortAlarm)
  {
   if (ldTimeDiff > intPortDownThresholdSec)
    {
     switch(enumPortType)
      {
       case WRK:
            intMessageCode = 721;
            break;
       case SYC:
            intMessageCode = 610;
            break;
       case MAIN:
            intMessageCode = 120;
            break;
       default:
            SendCodingError( "CommPortClasses.cpp - Coding error in ExperientUDPPort::fCheck_Time_Port_Down() 2nd switch(enumPortType) ->"+int2str(enumPortType));
            return;
      }// end switch
     objMessage.fMessage_Create(LOG_ALARM, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                KRN_MESSAGE_120, seconds2time(int(ldTimeDiff)),fPingStatusString());
     enQueue_Message(enumPortType,objMessage);
     boolPortAlarm = true;
    }// end if (ldTimeDiff > intPortDownThresholdSec)
   return;
  }// end if(!boolPortAlarm)

  // Port Down .. Previous alarm ... check time for reminder
  if (ldTimeDiff > intPortDownReminderThreshold)
   {
    switch(enumPortType)
     {
      case WRK:
           intMessageCode = 722;
           intPortDownReminderThreshold += intWRK_PORT_DOWN_REMINDER_SEC;
           break;
      case SYC:
           intMessageCode = 611;
           intPortDownReminderThreshold += intSYC_PORT_DOWN_REMINDER_SEC;
           break;
      default:
           SendCodingError("CommPortClasses.cpp - Coding error in ExperientUDPPort::fCheck_Time_Port_Down() 2nd switch(enumPortType)");
           return;
     }// end switch
    objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                               fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                               KRN_MESSAGE_121, seconds2time(int(ldTimeDiff)), fPingStatusString());
    enQueue_Message(enumPortType,objMessage);

   }// end  if (ldTimeDiff > intPortDownReminderThreshold)

}// end void ExperientUDPPort::fCheck_Time_Port_Down()

/****************************************************************************************************
*
* Name:
*  Function: bool ExperientUDPPort::fPingStatus()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function checks pings the remote IP address of the port.  The result of the ping is set 2 ways.  
*   Through the return boolean, and the ExperientUDPPort::boolPingStatus boolean.
*          
*    
* Parameters:
*  none
*
* Variables:
*  PingPort                                   Local   - <IPWorks><Ping>                         object
*  intPING_TIMEOUT                            Global  - <defines.h>                             Ping timeout seconds
*  intRC                                      Local   - integer                                 return code
*  Remote_IP                                  Local   - <ExperientUDPPort><IP_Address>          object
*  stringAddress                              Local   - <ExperientUDPPort><IP_Address><cstring> object member 
*                                                                
* Functions:
*  .Config()                                  Library  - <ipworks.h><Ping>    (int)
*  .c_str()                                   Library  - <cstring>            (const char*)
*  GetLocalHost()                             Library  - <ipworks.h><Ping>    (char*)
*  .PingHost()                                Library  - <ipworks.h><Ping>    (int)
*  .SetLocalHost()                            Library  - <ipworks.h><Ping>    (int)
*  .SetPacketSize()                           Library  - <ipworks.h><Ping>    (int)                        
*  .SetTimeout()                              Library  - <ipworks.h><Ping>    (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
bool ExperientUDPPort::fPingStatus()
{
 Ping   PingPort;
 int    intRC;
 string strKey = IPWORKS_RUNTIME_KEY;

#ifdef IPWORKS_V16
 PingPort.SetRuntimeLicense((char*)strKey.c_str());
#endif
 PingPort.SetLocalHost(GetLocalHost());
 PingPort.SetTimeout(intPING_TIMEOUT);
 PingPort.SetPacketSize(1024);
 PingPort.Config((char*)"AbsoluteTimeout=true");

 intRC = PingPort.PingHost((char*)Remote_IP.stringAddress.c_str());

 if(intRC){boolPingStatus = false;return false;}
 else     {boolPingStatus = true ;return true;}
}

/****************************************************************************************************
*
* Name:
*  Function: bool ExperientUDPPort::fPingStatusString()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function checks the ExperientUDPPort::boolPingStatus boolean value and returns a string
*   representing the results of a previous Ping from the localhost to the remote IP address.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolPingStatus                                  Local   - <ExperientUDPPort> boolean
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
string ExperientUDPPort::fPingStatusString()
{
 if (boolPingStatus){return "Successful";}
 else               {return "Failed";}
}

/****************************************************************************************************
*
* Name:
*  Function: Port_Data ExperientTCP_SERVER_Port::fPortData()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function loads the values of ExperientUDPPort members into a Port_Data object and
*   returns the object.  Used for passing multiple variables to different class functions.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolVendorEmailAddress                       Local   - <ExperientUDPPort> boolean       
*  boolVendorEmailAddress                       Local   - <Port_Data> boolean
*  enumPortType                                 Local   - <ExperientUDPPort><threadorPorttype>   enumeration member
*  enumPortType                                 Local   - <Port_Data><threadorPorttype>          enumeration member
*  intPortNum                                   Local   - <ExperientUDPPort> integer             member
*  intPortNum                                   Local   - <Port_Data> integer                    member
*  intSideAorB                                  Local   - <ExperientUDPPort> integer             member
*  intSideAorB                                  Local   - <Port_Data> integer                    member
*  objPortData                                  Local   - <ExperientUDPPort><Port_Data>          object
*  strSideAorB                                  Local   - <Port_Data><cstring>                   member
*  strVendorEmailAddress                        Local   - <ExperientUDPPort><IP_Address>         object
*  strVendorEmailAddress                        Local   - <Port_Data><IP_Address>                object
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
Port_Data ExperientTCP_SERVER_Port::fPortData()
{
 objPortData.intPortNum   = intPortNum;

 objPortData.enumPortType           = enumPortType;

 return objPortData;
}

void ExperientTCP_SERVER_Port::fInitializeCADport(int i)
{
 extern ExperientCommPort               CADPort[NUM_CAD_PORTS_MAX+1];
 string                                 strMaxConnections ="MaxConnections=3";
 string strKey = IPWORKS_RUNTIME_KEY;

 // THIS NEEDS TO BE REWRITTEN ...... ???? DO NOT SUPPORT SERVER ....
 
 
 intPortNum                                  = i;

 boolPortActive                              = true; 
 enumPortType                                = CAD;
 Config((char*)strMaxConnections.c_str());
 Config((char*)"InBufferSize=655360");
 Config((char*)"OutBufferSize=655360");
 Config((char*)"TcpNoDelay=true");
// Config((char*)"BindExclusively=true");
 SetLinger(false);
 boolPortActive                              = true; 

#ifdef IPWORKS_V16
 SetRuntimeLicense((char*)strKey.c_str());
#endif
 SetLocalHost(NULL);
 SetLocalPort(CADPort[i].intLocalPort);
 fPortData();
 
 
 SetListening(true);

}





int ExperientTCP_SERVER_Port::fSendKillSocket(int iConnectionId)
{
 int    		intRC;
 string 		strReplace;
 string 		strSize;
 string 		strContentType;
 string 		strContentHeader;
 string 		strDatatoSend;
 Port_Data          	objPortData;
 MessageClass        	objMessage;

 objPortData.fLoadPortData(WRK, intPortNum); 


 string strMessage = XML_LOG_HEADER;
 strMessage        += "<Event id=\"\" XMLspec=\"";
 strMessage        += chWORKSTATION_XML_SPECIFICATION;
 strMessage        += "\"><KillSocket/></Event>";
 strSize.assign(10, ' ');
 strContentType.assign(20, ' ');

 strReplace = int2str(strMessage.length());
 strSize.replace(1, strReplace.length(),strReplace);
 strReplace = "text/xml";
 strContentType.replace( 1, strReplace.length(),strReplace);
 strContentHeader = "\nContent-Length: " + strSize + "\nContent-Type: " + strContentType +"\n\n";

 strDatatoSend    = strContentHeader + strMessage;
 intRC = Send( iConnectionId, strDatatoSend.c_str(), strDatatoSend.length());

  if((boolDISPLAY_AND_LOG_ALL_RAW_TRAFFIC)||(boolDISPLAY_AND_LOG_WRK_RAW_TRAFFIC))
  { 
   objMessage.fMessage_Create(LOG_CONSOLE_FILE, 731, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                              objPortData,objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_531, GetRemoteHost(iConnectionId), 
                              ASCII_String(strDatatoSend.c_str(), strDatatoSend.length()),"","","","",  DEBUG_MSG, NEXT_LINE);
   enQueue_Message(WRK,objMessage);
  }        

return intRC;


}
/****************************************************************************************************
*
* Name:
*  Function: Port_Data ExperientUDPPort::fPortData()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function loads the values of ExperientUDPPort members into a Port_Data object and
*   returns the object.  Used for passing multiple variables to different class functions.
*          
*    
* Parameters:
*  none
*
* Variables:
*  boolVendorEmailAddress                       Local   - <ExperientUDPPort> boolean       
*  boolVendorEmailAddress                       Local   - <Port_Data> boolean
*  enumPortType                                 Local   - <ExperientUDPPort><threadorPorttype>   enumeration member
*  enumPortType                                 Local   - <Port_Data><threadorPorttype>          enumeration member
*  intPortNum                                   Local   - <ExperientUDPPort> integer             member
*  intPortNum                                   Local   - <Port_Data> integer                    member
*  intSideAorB                                  Local   - <ExperientUDPPort> integer             member
*  intSideAorB                                  Local   - <Port_Data> integer                    member
*  objPortData                                  Local   - <ExperientUDPPort><Port_Data>          object
*  strSideAorB                                  Local   - <Port_Data><cstring>                   member
*  strVendorEmailAddress                        Local   - <ExperientUDPPort><IP_Address>         object
*  strVendorEmailAddress                        Local   - <Port_Data><IP_Address>                object
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
Port_Data ExperientUDPPort::fPortData()
{
 objPortData.intPortNum   = intPortNum;
 objPortData.intSideAorB  = intSideAorB;

 switch(intSideAorB)
  {
   case 1: objPortData.strSideAorB = "A"; break;
   case 2: objPortData.strSideAorB = "B"; break;
   case 3: objPortData.strSideAorB = "S"; break;
   default:objPortData.strSideAorB = " ";
  }
 objPortData.enumPortType           = enumPortType;
 objPortData.boolVendorEmailAddress = boolVendorEmailAddress;
 objPortData.strVendorEmailAddress  = strVendorEmailAddress;
 return objPortData;
}
/****************************************************************************************************
*
* Name:
*  Function: Port_Data ExperientUDPPort::fPortPairData()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function loads the values of ExperientUDPPort members into a Port_Data object and
*   returns the object.  Used for passing multiple variables to different class functions.
*   used to descibe the pair versus an individual port.       
*    
* Parameters:
*  none
*
* Variables:
*  boolVendorEmailAddress                       Local   - <ExperientUDPPort> boolean       
*  boolVendorEmailAddress                       Local   - <Port_Data> boolean
*  enumPortType                                 Local   - <ExperientUDPPort><threadorPorttype>   enumeration member
*  enumPortType                                 Local   - <Port_Data><threadorPorttype>          enumeration member
*  intPortNum                                   Local   - <ExperientUDPPort> integer             member
*  intPortNum                                   Local   - <Port_Data> integer                    member
*  intSideAorB                                  Local   - <ExperientUDPPort> integer             member
*  intSideAorB                                  Local   - <Port_Data> integer                    member
*  objPortPairData                              Local   - <ExperientUDPPort><Port_Data>          object
*  strSideAorB                                  Local   - <Port_Data><cstring>                   member
*  strVendorEmailAddress                        Local   - <ExperientUDPPort><IP_Address>         object
*  strVendorEmailAddress                        Local   - <Port_Data><IP_Address>                object
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2011 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2011 Experient Corporation
****************************************************************************************************/
Port_Data ExperientUDPPort::fPortPairData()
{
 Port_Data objPortPairData;

 objPortPairData.intPortNum             = intPortNum;
 objPortPairData.strSideAorB            = "";
 objPortPairData.enumPortType           = enumPortType;
 objPortPairData.boolVendorEmailAddress = boolVendorEmailAddress;
 objPortPairData.strVendorEmailAddress  = strVendorEmailAddress;
 return objPortPairData;
}
/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::fIncrementDiscardedPacketCounter()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function inncrements the intDiscardedPackets counter.  If the value gets too
*   large the counter rolls over and a message is generated. 
*   note:  The counter is protected by a mutex semaphore.
*          
*    
* Parameters:
*  none
*
* Variables:
*  ALI                                        Global  - <threadorPorttype> enumeration member
*  ANI                                        Global  - <threadorPorttype> enumeration member
*  CAD                                        Global  - <threadorPorttype> enumeration member
*  SYC                                        Global  - <threadorPorttype> enumeration member
*  WRK                                        Global  - <threadorPorttype> enumeration member
*  enumPortType                               Local   - <ExperientUDPPort><threadorPorttype>                 enumeration
*  intDiscardedPackets                        Local   - <ExperientUDPPort> unsigned long long int            counter
*  intMessageCode                             Local   - integer                                              message number
*  LOG_MESSAGE_524                            Global  - <defines.h>                                          Message Template
*  LOG_WARNING                                Global  - <defines.h>                                          Log code
*  objBLANK_CALL_RECORD                       Global  - extern <Call_Data>  object                           blank call record
*  objMessage                                 Local   - <MessageClass> object                                Messaging object
*  sem_tMutexDiscardedPackets              Local   - <semaphore.h><ExperientUDPPort><sem_t>               Mutex semaphore
*  ULONG_LONG_MAX                             Library - <limits.h>                                           value of largest unsigned long long integer
*
* Functions:
*  enQueue_Message()                          Global - <globalfunctions.h>                  (void)
*  fMessage_Create()                          Local  - <MessageClass>                       (void)
*  fPortData()                                Local  - <ExperientUDPPort>                   (Port_Data)
*  Thread_Calling()                           Global - <globalfunctions.h>                  (string) 
*  sem_post()                                 Library - <semaphore.h>                        (int)
*  sem_wait()                                 Library - <semaphore.h>                        (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientUDPPort::fIncrementDiscardedPacketCounter()
{
 MessageClass           objMessage;
 int                    intMessageCode;

 sem_wait(&sem_tMutexDiscardedPackets);

 intDiscardedPackets++; 
 if (intDiscardedPackets == ULONG_LONG_MAX ) 
  {
   switch(enumPortType)
    {
     case ALI:
          intMessageCode = 250; break;
     case CAD:
          intMessageCode = 472; break;
     case ANI:
          intMessageCode = 356; break;
     case SYC:
          intMessageCode = 625; break;
     case WRK:
          intMessageCode = 729; break;
     default:
          SendCodingError("CommPortClasses.cpp - Coding error in ExperientCommPort::fIncrementDiscardedPacketCounter() " + Thread_Calling(enumPortType));
          return;
    }// end switch  

   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_524, "Discarded Packet Counter", int2strLZ(intDiscardedPackets));
   enQueue_Message(enumPortType,objMessage);
   intDiscardedPackets = 0;
  }
 sem_post(&sem_tMutexDiscardedPackets);
 return;
}

/****************************************************************************************************
*
* Name:
*  Function: void ExperientUDPPort::fCheckResetDiscardedPacketCounter()
*
* Description:
*   An ExperientUDPPort Function 
*
*
* Details:
*   This function is designed to be called once daily to check the number of discarded packets if a
*   threshold is exceeded a message is generated.  The counter is then reset to zero.
*   note:  The counter is protected by a mutex semaphore.
*          
*    
* Parameters:
*  none
*
* Variables:
*  ALI                                            Global  - <threadorPorttype> enumeration member
*  ANI                                            Global  - <threadorPorttype> enumeration member
*  CAD                                            Global  - <threadorPorttype> enumeration member
*  SYC                                            Global  - <threadorPorttype> enumeration member
*  WRK                                            Global  - <threadorPorttype> enumeration member
*  DAILY_DISCARDED_PACKET_NOTIFICATION_THRESHOLD  Global  - <defines.h>
*  enumPortType                                   Local   - <ExperientUDPPort><threadorPorttype>                 enumeration
*  intDiscardedPackets                            Local   - <ExperientUDPPort> unsigned long long int            counter
*  intMessageCode                                 Local   - integer                                              message number
*  LOG_MESSAGE_530                                Global  - <defines.h>                                          Message Template
*  LOG_WARNING                                    Global  - <defines.h>                                          Log code
*  objBLANK_CALL_RECORD                           Global  - extern <Call_Data>  object                           blank call record
*  objMessage                                     Local   - <MessageClass> object                                Messaging object
*  sem_tMutexDiscardedPackets                  Local   - <semaphore.h><ExperientUDPPort><sem_t>               Mutex semaphore
*  ULONG_LONG_MAX                                 Library - <limits.h>                                           value of largest unsigned long long integer
*
* Functions:
*  enQueue_Message()                          Global  - <globalfunctions.h>                  (void)
*  fMessage_Create()                          Local   - <MessageClass>                       (void)
*  fPortData()                                Local   - <ExperientUDPPort>                   (Port_Data)
*  int2str()                                  Global  - <globalfunctions.h>                  (string)
*  sem_post()                                 Library - <semaphore.h>                        (int)
*  sem_wait()                                 Library - <semaphore.h>                        (int)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void ExperientUDPPort::fCheckResetDiscardedPacketCounter()
{
 MessageClass           objMessage;
 int                    intMessageCode;

 sem_wait(&sem_tMutexDiscardedPackets);

 if(intDiscardedPackets > DAILY_DISCARDED_PACKET_NOTIFICATION_THRESHOLD)
  {
   switch(enumPortType)
    {
     case ALI:
          intMessageCode = 251; break;
     case CAD:
          intMessageCode = 473; break;
     case ANI:
          intMessageCode = 357; break;
     case SYC:
          intMessageCode = 626; break;
     case WRK:
          intMessageCode = 730; break;
     default:
          SendCodingError( "CommPortClasses.cpp - Coding error in ExperientCommPort::fIncrementDiscardedPacketCounter() " + fPortData().enumPortType);
          return;
    } // end switch 

   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              fPortData(), objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                              LOG_MESSAGE_530, int2str(intDiscardedPackets)); 
   enQueue_Message(enumPortType,objMessage);

  }// end if(intDiscardedPackets > DAILY_DISCARDED_PACKET_NOTIFICATION_THRESHOLD)

 intDiscardedPackets = 0;
 sem_post(&sem_tMutexDiscardedPackets);
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//
//                                                                          Port_Data FUNCTIONS
//
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/****************************************************************************************************
*
* Name:
*  Function: void Port_Data::fClear()
*
* Description:
*   A Port_Data Function 
*
*
* Details:
*   This function resets all class variables to thier initial states.         
*    
* Parameters:
*  none
*
* Variables:
*  boolVendorEmailAddress                     Local   - <Port_Data> boolean            true if email address is not the default
*  enumPortType	                              Local   - <Port_Data><threadorPorttype>  enumeration (Port type)
*  intPortNum                                 Local   - <Port_Data> integer            Port Number 
*  intSideAorB                                Local   - <Port_Data> integer            if ALI Port (1 or 2) 
*  NO_PORT_OR_THREAD	                      Local   - <threadorPorttype>             enumeration member
*  strSideAorB 	                              Local   - <Port_Data> <cstring>          if ALI Port (A or B) 
*  strThread                                  Local   - <Port_Data><cstring>           Thread name (ALI ANI CAD etc)
*  strVendorEmailAddress                      Local   - <Port_Data><cstring>           vendor Email address
*
* Functions:
*  none 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void Port_Data::fClear()
{
 intPortNum 		= 0;
 intSideAorB		= 0;
 strSideAorB 		= "";
 enumPortType		= NO_PORT_OR_THREAD;
 strThread              = "";
 boolVendorEmailAddress	= false;
 strVendorEmailAddress  = "";
}

/****************************************************************************************************
*
* Name:
*  Function: void Port_Data::fLoadThreadData(threadorPorttype enumArg)
*
* Description:
*   A Port_Data Function 
*
*
* Details:
*   This function Loads the Thread/Port Data type and name       
*    
* Parameters:
*  enumArg                                    <threadorPorttype>
*
* Variables:
*  enumPortType	                              Local   - <Port_Data><threadorPorttype>  enumeration (Port type)
*  strThread                                  Local   - <Port_Data><cstring>           Thread name (ALI ANI CAD etc)
*
* Functions:
*  erase()                                    Library - <cstring>           (*this) 
*  Thread_Calling()                           Global  - <globalfunctions.h> (string)
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void Port_Data::fLoadThreadData(threadorPorttype enumArg)
{
 enumPortType = enumArg;
 strThread    = Thread_Calling(enumArg).erase(3);
}

/****************************************************************************************************
*
* Name:
*  Function: void Port_Data::fLoadPortData(threadorPorttype enumArg, int PortNum, int SideAorB) 
*
* Description:
*   A Port_Data Function 
*
*
* Details:
*   This function Loads the Port number, side, type, and name.    
*    
* Parameters:
*  enumArg                                    <threadorPorttype>
*  portNum                                    integer
*  SideAorB                                   integer
*
* Variables:
*  intPortNum                                 Local  - <Port_Data> integer  the Port number
*  intSideAorB                                Local  - <Port_Data> integer  the Port side (for ALI Ports 1 or 2) 0 for all others
*
* Functions:
*  fLoadThreadData()                          Local  - <Port_Data>           (void) 
*
* AUTHOR: 11/19/2008 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2008 Experient Corporation
****************************************************************************************************/
void Port_Data::fLoadPortData(threadorPorttype enumArg, int PortNum, int SideAorB) 
{
 intPortNum 		= PortNum;
 intSideAorB 		= SideAorB;
 switch(SideAorB)
  {
   case 1: strSideAorB = "A";break;
   case 2: strSideAorB = "B";break;
   case 3: strSideAorB = "S";break;
   default:strSideAorB = "";
  }
 fLoadThreadData(enumArg);
}


//assignment operator
ExperientTCP_SERVER_Port& ExperientTCP_SERVER_Port::operator=(const ExperientTCP_SERVER_Port& a)
{
 if (&a == this ) {return *this;}
 intPortNum 				= a.intPortNum;
 enumPortType				= a.enumPortType;
 intRemotePortNumber			= a.intRemotePortNumber;
 intLocalPortNumber			= a.intLocalPortNumber;
 objPortData				= a.objPortData;
 objCallData				= a.objCallData;
 Remote_IP				= a.Remote_IP;
 boolPortActive				= a.boolPortActive;
 boolReadyToSend			= a.boolReadyToSend;
 boolPortWasDown			= a.boolPortWasDown;
 timespecBufferTimeStamp.tv_sec		= a.timespecBufferTimeStamp.tv_sec;
 timespecBufferTimeStamp.tv_nsec	= a.timespecBufferTimeStamp.tv_nsec; 
 sem_tMutexTCPPortBuffer		= a.sem_tMutexTCPPortBuffer;
 boolOutsideIPTraffic			= a.boolOutsideIPTraffic;
 stringOutsideIPAddr			= a.stringOutsideIPAddr;
 ContentLength				= a.ContentLength;
 TCP_Buffer				= a.TCP_Buffer;
 LastConnectionId			= a.LastConnectionId;
 vConnectionList			= a.vConnectionList;     

return *this;
}
void ExperientTCP_SERVER_Port::fQueue_Transmission(DataPacketIn DataReceived )
{
 int    			intRC;
 extern sem_t			sem_tMutexCadPortDataQ;
 extern sem_t			sem_tCadFlag;
 extern queue <DataPacketIn>    queueCadPortData                        [NUM_CAD_PORTS_MAX+1];;
 
 switch (enumPortType)
  {
   case CAD:
        intRC = sem_wait(&sem_tMutexCadPortDataQ);
        if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadPortDataQ, "sem_wait@sem_tMutexCadPortDataQ in ExperientTCP_SERVER_Port::fQueue_Transmission", 1);}
        queueCadPortData[intPortNum].push(DataReceived);
        sem_post(&sem_tMutexCadPortDataQ);
        sem_post(&sem_tCadFlag);
        break;

   default:
        
        break;
  }
}



void ExperientHTTPPort::fQueue_Transmission(DataPacketIn DataReceived )
{
 int    			intRC;
 extern sem_t			sem_tMutexLISPortDataQ;
 extern sem_t			sem_tLISflag;
 extern queue <DataPacketIn>    queue_LISPortData;;
 
 switch (enumPortType)
  {
   case LIS:
      //  //cout << "CP LOCK PORT -f" << endl;
      //  //cout << "port number -> " << DataReceived.intPortNum << endl;
        intRC = sem_wait(&sem_tMutexLISPortDataQ);
        if (intRC){Semaphore_Error(intRC, LIS, &sem_tMutexLISPortDataQ, "sem_wait@sem_tMutexLISPortDataQ in fQueue_Transmission", 1);}
        queue_LISPortData.push(DataReceived);
        sem_post(&sem_tMutexLISPortDataQ);
    //    //cout << "CP UNLOCK PORT -f" << endl;
        sem_post(&sem_tLISflag);
        break;

   case CTI:
     //   //cout << "got data" << endl;
        sem_post(&sem_tWaitForData);


   default:
        
        break;
  }
}


bool IP_Address::fCompare(const IP_Address& a) const
{
 bool boolRunningCheck = true;

 boolRunningCheck = boolRunningCheck && (this->stringAddress == a.stringAddress);

 return boolRunningCheck;
}



string IP_Address::fParseIPaddressUsingValueKey(string strKey, string strData)
{
 size_t StartPosition;
 size_t EndPosition;
 string strOutput;

 StartPosition = strData.find(strKey);
 if (StartPosition == string::npos)                   {return "";}
 StartPosition += strKey.length();

 strOutput.assign(strData, StartPosition, string::npos);

 EndPosition = strOutput.find(";");
 if (EndPosition != string::npos) { strOutput.erase(EndPosition, string::npos); }

 EndPosition = strOutput.find(":");
 if (EndPosition != string::npos) { strOutput.erase(EndPosition, string::npos); }

 //remove leading blanks ...
 strOutput= RemoveLeadingSpaces(strOutput);

 this->stringAddress = strOutput;
 if (!this->fIsValid_IP_Address()) {this->stringAddress.clear();}

 return strOutput;

}


bool IP_Address::fIsValid_IP_Address()
{

 int                    intRC;
 char                   NumericAddress[32];

// if ((stringAddress.length() < 7)||(stringAddress.length() > 15)) {return false;}

 intRC = inet_pton( AF_INET , (const char *)stringAddress.c_str() , NumericAddress);

 switch (intRC)
 {
  case  0: return false; // invalid ip address
  case  1: return true;  // valid ip address
  case -1: return false; // function error
  default: return false; // catch all
 }

}// bool IP_Address::fIsValid_IP_Address()

void IP_Address::fClear()
{
 stringAddress.clear();
}

string IP_Address::fDisplay()
{
 ostringstream  strOutput;
 strOutput << stringAddress << endl;
 return strOutput.str();
}

void DataPacketIn::fClear() {

   intPortNum 		    = 0;
   intWorkstation 	    = 0;
   intSideAorB 		    = 0;
   intLength 		    = 0;
   stringDataIn		    = "";
   boolStringBufferExceeded = false;
   intTransactionID         = 0;
   intConnectionID          = 0;
   boolALIservicePacket     = false;
   strRequestID             = "";
}

bool DataPacketIn::fSetACK()
{
 stringDataIn = charACK;
 intLength    = 1;
 return true;
}

bool DataPacketIn::fSetNAK()
{
 stringDataIn = charNAK;
 intLength    = 1;
 return true;
}

void DataPacketIn::fLoadSimulatedHTTPderef(string strFile, string strUUID, int iPort) {

string 		s, strOut;
ifstream 	inputfile;

const char* 	filename = strFile.c_str();

inputfile.open(filename, ifstream::in);
strOut.clear();
while(!inputfile.eof() ) {
getline(inputfile,s);
strOut += s;
}

inputfile.close();
//cout << "DATA.............................................................................................................................." << endl;
this->intPortNum     = iPort;
this->intWorkstation = 0;
this->intSideAorB    = 0;
this->boolStringBufferExceeded = false;
this->stringDataIn   = strOut;
this->strRequestID = strUUID;
//cout << this->stringDataIn << endl;
//cout <<"...................................................................................................................................." << endl;
return;
}


bool DataPacketIn::fCreateLegacyALIrecord()
{
 
 ALI_E2_XML_Data    objXMLData;
 Port_Data          objPortData;
// MessageClass       objMessage;


 string strLineOne,strLineTwo,strLineThree, strLineFourFiveSix, strLineSevenEight, strLineNine, strLineTen, strLineEleven ="";
 string strLineTwelve,strLineThirteen,strLineFourteen, strLineFifteen ="";
 string strBLANK, strCR, strSPACE, strDASH, strPPOUND = "";
 string strLPAREN, strRPAREN, strFWDSLASH;
 string strStreetNameCombined;

 strBLANK.assign(32, ' ');
 strCR    += charCR;
 strSPACE += charSPACE; 
 strDASH     = "-";
 strPPOUND   = "P#";
 strLPAREN   = "(";
 strRPAREN   = ")";
 strFWDSLASH = "/";

 objPortData.fLoadPortData(ALI, intPortNum, intSideAorB);
 objXMLData.fLoadLocationData((const char*) E2Data.chLocationDescription, objPortData);

 strLineOne += charSTX;
 strLineOne += "200";
 strLineOne += charCR;
 strLineTwo = "(";
 strLineTwo += E2Data.CallbackNumber.AreaCode();
 strLineTwo += ") ";
 strLineTwo += E2Data.CallbackNumber.NPA();
 strLineTwo += strDASH;
 strLineTwo += E2Data.CallbackNumber.LastFour();
 strLineTwo += strSPACE;
 strLineTwo += objXMLData.fClassofService();
 strLineTwo += strSPACE;
 strLineTwo += E2Data.TimeStamp(true);
 strLineTwo += strCR;


 strLineThree = strBLANK;
 strLineThree = InsertString(strLineThree, objXMLData.strCustomerName, 0, 32);

 strLineFourFiveSix = strBLANK + strBLANK + strBLANK;
 strLineFourFiveSix = InsertString(strLineFourFiveSix, objXMLData.strHouseNumber, 0, 10);                   // right justify this field ....
 strLineFourFiveSix = InsertString(strLineFourFiveSix, strSPACE, 10, 1);
 strLineFourFiveSix = InsertString(strLineFourFiveSix, objXMLData.strHouseNumberSuffix, 11, 4);
 strLineFourFiveSix = InsertString(strLineFourFiveSix, strSPACE, 15, 1);
 strLineFourFiveSix = InsertString(strLineFourFiveSix, objXMLData.strPrefixDirectional, 16, 2);
 strLineFourFiveSix = InsertString(strLineFourFiveSix, strSPACE, 18, 1);

 strStreetNameCombined = RemoveTrailingSpaces(objXMLData.strStreetname);
 strStreetNameCombined = RemoveLeadingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += objXMLData.strStreetNameSuffix;
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);
 strStreetNameCombined += strSPACE;
 strStreetNameCombined += objXMLData.strPostDirectional;
 strStreetNameCombined = RemoveTrailingSpaces(strStreetNameCombined);

 strLineFourFiveSix = InsertString(strLineFourFiveSix, strStreetNameCombined, 19, strStreetNameCombined.length()); 
 strLineFourFiveSix = InsertString(strLineFourFiveSix, "SP=", 88, 3);
 strLineFourFiveSix = InsertString(strLineFourFiveSix, objXMLData.strCompanyID1, 91, 5);

 strLineSevenEight = strBLANK + strBLANK;
 strLineSevenEight = InsertString(strLineSevenEight, objXMLData.strLocation, 0, 60); 
 strLineSevenEight = InsertString(strLineSevenEight, strCR, 60, 1); 
 strLineSevenEight = RemoveTrailingSpaces(strLineSevenEight);

 strLineNine = strBLANK;
 strLineNine = InsertString(strLineNine, objXMLData.strMSAGCommunity, 0, 32);

 strLineTen = strBLANK;
 strLineTen = InsertString(strLineTen, objXMLData.strState, 0, 2);
 strLineTen = InsertString(strLineTen, strSPACE, 2, 1);
 strLineTen = InsertString(strLineTen, objXMLData.strCountyID, 3, 5);
 strLineTen = InsertString(strLineTen, strSPACE, 8, 1);
 strLineTen = InsertString(strLineTen, "CELLID=", 9, 7);
 strLineTen = InsertString(strLineTen, objXMLData.strCellId, 16, 6);
 strLineTen = InsertString(strLineTen, strSPACE, 22, 1);
 strLineTen = InsertString(strLineTen, "SECTOR=", 23, 7);
 strLineTen = InsertString(strLineTen, objXMLData.strSectorID, 30, 2);

 strLineEleven = strBLANK;
 if (!E2Data.chPositionGeoLatSign) {strLineEleven = InsertString(strLineEleven, "+", 0, 1);}
 else                              {strLineEleven = InsertString(strLineEleven, "-", 0, 1);}
 strLineEleven = InsertString(strLineEleven, E2Data.Latitude(), 1, 10);
 strLineEleven = InsertString(strLineEleven, strSPACE, 11, 1);
 strLineEleven = InsertString(strLineEleven, E2Data.Longitude(), 12, 11);
 strLineEleven = InsertString(strLineEleven, strSPACE, 23, 1);
 strLineEleven = InsertString(strLineEleven, E2Data.Uncertainty(), 24, 7);
 strLineEleven = InsertString(strLineEleven, strCR, 31, 1);

 strLineTwelve = strBLANK;
 strLineTwelve = InsertString(strLineTwelve, "ESN=", 0, 4);
 strLineTwelve = InsertString(strLineTwelve, objXMLData.strEmergencyServicesNumber, 4, 5);
 strLineTwelve = InsertString(strLineTwelve, strSPACE, 9, 1);
 strLineTwelve = InsertString(strLineTwelve, strPPOUND, 10, 2);
 strLineTwelve = InsertString(strLineTwelve, E2Data.EmergencyServicesRoutingDigits.AreaCode(), 12, 3);
 strLineTwelve = InsertString(strLineTwelve, strDASH, 15, 1);
 strLineTwelve = InsertString(strLineTwelve, E2Data.EmergencyServicesRoutingDigits.NPA(), 16, 3);
 strLineTwelve = InsertString(strLineTwelve, strDASH, 19, 1);
 strLineTwelve = InsertString(strLineTwelve, E2Data.EmergencyServicesRoutingDigits.LastFour(), 20, 4);
 strLineTwelve = InsertString(strLineTwelve, strCR, 24, 1);
 strLineTwelve = RemoveTrailingSpaces(strLineTwelve);


 strLineThirteen = strBLANK;
 strLineThirteen = InsertString(strLineThirteen, "LAW=", 0, 4); 
 strLineThirteen = InsertString(strLineThirteen, objXMLData.strLawEnforcementServiceProvider, 4, 25);
 strLineThirteen = InsertString(strLineThirteen, strCR, 29, 1);
 strLineThirteen = RemoveTrailingSpaces(strLineThirteen);

 strLineFourteen = strBLANK;
 strLineFourteen = InsertString(strLineFourteen, "FIRE=", 0, 5); 
 strLineFourteen = InsertString(strLineFourteen, objXMLData.strLawEnforcementServiceProvider, 5, 25);
 strLineFourteen = InsertString(strLineFourteen, strCR, 30, 1);
 strLineFourteen = RemoveTrailingSpaces(strLineFourteen);

 strLineFifteen = strBLANK;
 strLineFifteen = InsertString(strLineFifteen, "EMS=", 0, 4); 
 strLineFifteen = InsertString(strLineFifteen, objXMLData.strLawEnforcementServiceProvider, 4, 25);
 strLineFifteen = InsertString(strLineFifteen, strCR, 29, 1);
 strLineFifteen = RemoveTrailingSpaces(strLineFifteen);


 stringDataIn +=strLineOne;
 stringDataIn +=strLineTwo;
 stringDataIn +=strLineThree;
 stringDataIn +=strLineFourFiveSix;
 stringDataIn +=strLineSevenEight;
 stringDataIn +=strLineNine;
 stringDataIn +=strLineTen;
 stringDataIn +=strLineEleven;
 stringDataIn +=strLineTwelve;
 stringDataIn +=strLineThirteen;
 stringDataIn +=strLineFourteen;
 stringDataIn +=strLineFifteen;

 

 stringDataIn += charETX;
 intLength    = stringDataIn.length();
 return true;
}








