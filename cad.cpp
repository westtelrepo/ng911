/*****************************************************************************
* FILE: cad.cpp
*
* DESCRIPTION: This file contains all of the functions integral to the CAD Thread
*
* Features:  
*
*    1. CAD Systems:                        Ability to connect to unlimited number of CAD systems
*    2. ACKs:                               Ability to customize if ACKs are required for Heartbeats and/or Records;
*                                           When ACK confirmation for records is required, waits for ACK or timeout before sending next record
*    3. Heartbeats:                         Sends heartbeat once every minute when no activity and connection is available;
*                                           Sends heartbeat once every ten seconds when connection is unavailable
*    4. Email/Alarm Notification:           Informational messages on startup;
*                                           Warning messages for unusual activity;
*                                           Alarm messages for connection down/unavailable;
*                                           Reminder messages that alarm condition still exists
*    5. Denial of Service Protection:       Ignores packets from non-registered IP addresses;
*                                           Timed shutdown when approaching full buffer;
*                                           Ports are turned off when not expecting data;
*                                           Smart Queue can identify and flush bad data
*    6. Communication:                      Utilizes UDP communications for connections;
*                                           If necessary converted to serial communication via Ethernet to serial converter
*    7. Self-Diagnostics:                   Built in diagnostics to help troubleshoot line errors, etc.
*    8. Threaded Implementation:            Provides efficient utilization and reduces resource monopolization
*    9. NENA Compliant:                     Complies with the “NENA Recommended Generic Standards for E9-1-1 PSAP Equipment”                                     
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation   
******************************************************************************/
#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"


//forward function declarations
void *Cad_Port_Listen_Thread(void *voidArg);
//void Cad_ScoreBoard_Event(union sigval union_sigvalArg);
//void Cad_Messaging_Monitor_Event(void *voidArg);
void Cad_ACK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
void Cad_NAK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
void Cad_Heartbeat_NAK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
void Cad_Heartbeat_ACK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
void Check_CAD_ACK_Timeout(ExperientDataClass* ptr_obj_arrayCadWorkTable);
void Send_CAD_Record(ExperientDataClass* ptr_obj_arrayCadWorkTable);
void Check_CAD_Heartbeat_Interval(ExperientDataClass* ptr_obj_arrayCadWorkTable);
void Check_CAD_Heartbeat_ACK_Timeout(ExperientDataClass* ptr_obj_arrayCadWorkTable);
void FlushQueue(queue <DataPacketIn> *PortQueue, int intPortNum, threadorPorttype enumPortType);
int  Start_CAD_Timed_Threads();


extern void *Cad_Messaging_Monitor_Event(void *voidArg);
extern void *Cad_ScoreBoard_Event(void *voidArg);

//external class based variables
extern queue <MessageClass>	                    queue_objMainMessageQ;
extern queue <MessageClass>	                    queue_objCadMessageQ;
extern Thread_Trap                              OrderlyShutDown;
extern Thread_Trap                              UpdateVars;
extern IP_Address                               HOST_IP;
extern IP_Address                               MULTICAST_GROUP;
extern Text_Data                                objBLANK_TEXT_DATA;
extern Call_Data                                objBLANK_CALL_RECORD;
extern Freeswitch_Data                          objBLANK_FREESWITCH_DATA;
extern ALI_Data                                 objBLANK_ALI_DATA;
extern Port_Data                                objBLANK_CAD_PORT;
	
using namespace std;


// CAD Related Globals
int                                     intNUM_CAD_PORTS; 
bool                                    boolCAD_EXISTS;
bool                                    boolCAD_ERASE;
ExperientCommPort                       CADPort                                 [NUM_CAD_PORTS_MAX+1];
sem_t                                   sem_tMutexCadWorkTable;
sem_t                                   sem_tMutexCadInputQ;
sem_t                                   sem_tMutexCadMessageQ;
sem_t                                   sem_tMutexCadPortDataQ;
sem_t                                   sem_tCadFlag;
struct itimerspec                       itimerspecCadMessageMonitorDelay;
struct itimerspec                       itimerspecCadScoreboardDelay;
struct itimerspec                       itimerspecCadPortListenDelay;
timer_t                                 timer_tCadMessageMonitorTimerId;
timer_t                                 timer_tCadScoreboardTimerId;
queue <DataPacketIn>                    queueCadPortData                        [NUM_CAD_PORTS_MAX+1];
ExperientDataClass                      obj_arrayCadWorkTable                   [intALI_MAN_BID_TRUNK_LAST+2];




// Cad Global Initialized variables
volatile int                            intCadActivePosition                    = 0;
int                                     intCadHeartbeatPosition                 = intALI_MAN_BID_TRUNK_LAST+1;


/****************************************************************************************************
*
* Name:
*   Function void *Cad_Thread(void *voidArg)
*
*
* Description:
*   Handles all of the CAD related activity for the controller. 
*
*
* Details:
*   This function is implemented as a detached thread from the main program. The program recieves a 
*   pointer to the input queue from main() as its argument.
*
*   The Thread spawns (N) number of  UDP Port listening threads and creates two timer event threads:
*  
*    a. ScoreBoard Event:       Utilized to process incoming ALI Records for transmission
*    b. Message Monitor Event:  Utilized to send error communications to main.
*
*   
*   A global semaphore is used to recieve a signal from three sources :
*
*     a. Data (from Main) has been pushed onto the CAD's input queue.
*     b. Data received from a CAD port pushed onto the Data queue.
*     c. Shutdown signal from main.
*
*   The main loop idles waiting for a signal, it checks for the shutdown signal, if able to continue it processes 2 queues the first is 
*   queue_objCadInputData, the second is the queueCadPortData.
*
*   1. queue_objCadInputData: data is popped from the queue onto a table (array) of records and is processed 
*      by 2 timer event handlers Cad_ScoreBoard_Event and Cad_Messaging_Monitor_Event. 
*
*      Records and Heartbeats are processed one at a time from transmission to response due to the inability to match records to reponses.
*
*   2. queueCadPortData:  data is popped from the queue and deciphered for length, then type (ACK NAK or undetermined).
*
*      A check for an active record (transmitted and awaiting ACK(s) ) is then performed 
*
*     a. if there is no active record it is assumed to be a stray and a stray(ACK/NAK) info message is generated.
*     b. if there is an active message: 
*         1. if ACK: 
             a. clear record on table (array)
*            b. reset CADPort[].intHeartbeatInterval to intCAD_HEARTBEAT_INTERVAL_SEC
*         2. if NAK: 
*            a. if record is the heartbeat record (intCadHeartbeatPosition):
*               1. clear record on table (array)  
*               2. reset CADPort[].intHeartbeatInterval to intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC
*            b. if record is regular record:
*               1. retransmit record (1 retransmit allowed) 
*                  a. if record has been previously retransmitted generate  error message and clear record 
*                     on table (array)
*
*   2. sets heartbeat record time stamp to present time 
*
*   3. continue till boolean for shutdown is set to true
  
*
* Parameters: 				
*   void *voidArg	      		queue <ExperientDataClass>* pointer
*
*
* Variables:
*   boolACK                              Local   - Data is an ACK if true
*   .boolACKReceived                     Global  - <ExperientCommPort> member                                            not sure if used
*   .boolRecordACKRequired               Global  - <ExperientCommPort> member.. true if port requires data ACKs
*   bool_arrayCADPORTACKREQUIRED[]       Global  - configuration array
*   bool_arrayHEARTBEATCADPORT[]         Global  - configuration array
*   .boolCadThreadReady                  Global  - <Thread_Trap> member.. volatile bool used to show that CAD thread is ready to shut down
*   boolDEBUG                            Global  - <globals.h> display debug MSGs if true
*   .boolEmergencyShutdown               Global  - <ExperientCommPort> member
*   .boolFirstACKRcvd                    Global  - <ExperientCommPort> member.. true when port first ACK recieved
*   .boolHeartbeatRequired               Global  - <ExperientCommPort> member.. true if port requires heartbeats
*   boolMULTICAST                        Global  - <globals.h> true if multicast is utilized
*   boolNAK                              Local   - Data is a NAK if true
*   .boolNAKReceived                     Global  - <ExperientCommPort> member
*   .boolPingStatus                      Global  - <ExperientCommPort> member
*   .boolPortActive                      Global  - <ExperientCommPort> member.. true if port is awaiting answer to data TX 
*   .boolReadyToShutDown                 Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatMode            Global  - <ExperientCommPort> member.. true if port is in Reduced Heartbeat mode
*   .boolReducedHeartBeatModeFirstAlarm  Global  - <ExperientCommPort> member.. true if Alarm message has been sent
*   .boolUnexpectedDataShowMessage       Global  - <ExperientCommPort> member
*   boolSTOP_EXECUTION                   Global	- when true, thread lock loop engages for shutdown
*   .boolXmitRetry                       Global  - <ExperientCommPort> member
*   CLOCK_REALTIME                       Library - <ctime> real time clock constant
*   CAD                                  Global  - <header.h> threadorPorttype enumeration member
*   CAD_LOCAL_PORT[]                     Global  - configuration array
*   CAD_MESSAGE_400                      Global  - <defines.h> CAD Thread Created output message
*   CAD_MESSAGE_409                      Global  - <defines.h> CAD Port Initialized output message
*   CAD_MESSAGE_418                      Global  - <defines.h> CAD Thread Signalling Semaphore failed to Lock output message
*   CAD_MESSAGE_425                      Global  - <defines.h> CAD ScoreBoard Timer Event Initialized output message
*   CAD_MESSAGE_426                      Global  - <defines.h> CAD Message Monitor Timer Event Initialized output message
*   CAD_MESSAGE_427                      Global  - <defines.h> CAD Unexpected Response Received output message
*   CAD_MESSAGE_431                      Global  - <defines.h> CAD Unexpected Character Count output message
*   CAD_MESSAGE_496                      Global  - <defines.h> CAD Port Listen Thread Failed to Initialize output message
*   CAD_MESSAGE_499                      Global  - <defines.h> CAD Thread complete output message
*   CADPort[]                            Global  - <ExperientCommPort> the CAD Port(s)
*   CadPortDataRecieved                  Local   - <DataPacketIn> struct holding Cad Port Data recieved
*   charACK                              Global  - <globals.h>
*   charNAK                              Global  - <globals.h>
*   CPU_AFFINITY_ALL_OTHER_THREADS_SET   Global  - <globals.h><cpu_set_t>
*   .enumPortType                        Global  - <ExperientCommPort><ipworks.h><IPPort> (threadorPorttype) member
*   errno                                Library - <errno.h> - error handling return code
*   ETIMEDOUT                            Library - <semaphore.h> - indicates a semaphore timeout
*   HOST_IP                              Global  - <IP_Address> object
*   i                                    Local	- general purpose integer variable
*   intCadActivePosition                 Global  - position number of active record
*   intCAD_CLEANUP_INTERVAL_NSEC         Global	- <defines.h>
*   intCAD_HEARTBEAT_INTERVAL_SEC        Global  - <defines.h>
*   intCadHeartbeatPosition              Global  - the position number used for heartbeats
*   intCAD_PORT_LISTEN_INTERVAL_NSEC     Global  - <defines.h>
*   intCAD_PORT_DOWN_REMINDER_SEC        Global  - <globals.h>
*   intCAD_PORT_DOWN_THRESHOLD_SEC       Global  - <globals.h>               
*   intCAD_MESSAGE_MONITOR_INTERVAL_NSEC Global  - <defines.h>
*   intDataToDisplay                     Local   - amount of data to display for responses greater than 1
*   intFirstLook                         Local   - used to rotate the first port to look at
*   .intHeartbeatInterval                Global  - <ExperientCommPort> member.. heartbeat interval of port
*   .intLength                           Global  - <DataPacketIn> member
*   intLogCode                           Local   - code for logging
*   intMAX_DATA_TO_DISPLAY               Global  - <defines.h>
*   intNUM_CAD_PORTS                     Global  - <defines.h>
*   intNUM_WRK_STATIONS                  Global  - <defines.h>
*   intPING_TIMEOUT                      Global  - <defines.h>
*   intPORT_DOWN_REMINDER_SEC            Global  - <defines.h>
*   intPortNum                           Local   - port number
*   .intPortNum                          Global  - <DataPacketIn> member
*   .intPortNum                          Global  - <ExperientCommPort><ipworks.h><IPPort> member
*   .intPortNum                          Global  - <ExperientCommPort><ipworks.h><UDPPort> member
*   .intPortNum                          Global  - <Thread_Statistic_Counters> member
*   .intPortDownReminderThreshold        Global  - <ExperientCommPort> member.. reminder email interval (sec)for port in reduced HB mode
*   .intPortDownThresholdSec             Global  - <ExperientCommPort> member
*   intRC                                Local   - return code for various library function calls
*   intTHREAD_SHUTDOWN_DELAY_SEC         Global  - <defines.h>
*   .intPosn                             Global  - <ExperientDataClass> member
*   .intUnexpectedDataCharacterCount     Global  - <ExperientCommPort> member .. running count of unexpected characters
*   itimerspecDelay                      Local   - <ctime> struct itimerspec which contains timing parameters
*   itimerspecCadMessageMonitorDelay     Global  - <ctime> struct itimerspec which contains timing parameters
*   itimerspecCadPortListenDelay         Global  - <ctime> struct itimerspec which contains timing parameters
*   itimerspecCadScoreboardDelay         Global  - <ctime> struct itimerspec which contains timing parameters
*   KRN_MESSAGE_142                      Global  - <defines.h> CPU affinity Error message      
*   LOG_CONSOLE_FILE                     Global  - <defines.h> code to log thread, display to console and log to disk
*   LOG_WARNING                          Global  - <defines.h> code to log thread, to email warning, display to console and log to disk
*   MULTICAST_GROUP                      Global  - <IP_Address> IP address of multicast group
*   objCadData                           Local   - <ExperientDataClass> object
*   obj_arrayCadWorkTable                Local   - <ExperientDataClass>> array of objects being processed
*   objMessage                           Local   - <MessageClass> object
*   objPing                              Local   - <ipworks.h><Ping> object
*   OrderlyShutDown                      Global  - <Thread_Trap> object
*   .PingPort                            Global  - <ExperientCommPort><ExperientPing> object
*   pthread_attr_tAttr                   Local   - <pthread.h> pthread_attr_t
*   PTHREAD_CREATE_JOINABLE              Library - <pthread.h> 
*   queueCadPortData                     Global  - <queue> <DataPacketIn>				
*   queue_objCadInputData                Local   - <queue> <ExperientDataClass> a pointer to a queue of ExperientDataClass objects from main()
*   sem_tCadFlag                         Global  - <semaphore.h> semaphore flag for interprocess communication
*   sem_tMutexCadInputQ                  Global  - <semaphore.h> semaphore flag set to mutex Cad Input Q
*   sem_tMutexCadPortDataQ               Global  - <semaphore.h> semaphore flag set to mutex Cad Port Data Q
*   sem_tMutexCadWorkTable               Global  - <semaphore.h> semaphore flag set to mutex Cad Work Table array
*   .sem_tMutexUDPPortBuffer             Global  - <ExperientCommPort> <semaphore.h> semaphore flag set to mutex UDP Port Buffer
*   sigeventCadACKTimerEvent             Local   - <csignal> struct sigevent that contains the data that defines the event handler
*   sigeventCadMessageMonitorEvent       Local	- <csignal> struct sigevent that contains the data that defines the event handler
*   sigeventCadScoreboardEvent           Local   - <csignal> struct sigevent that contains the data that defines the event handler
*   .sigev_notify                        Library - <csignal> struct sigevent member
*   .sigev_notify_attributes             Library - <csignal> struct sigevent member
*   .sigev_notify_function               Library - <csignal> struct sigevent member
*   size_tStringPosition                 Local   - <cstring> <size_t>
*   .stringAddress                       Global  - <IP_Address> member 
*   .stringBuffer                        Global  - <ExperientCommPort><ExperientUDPPort> member
*   .stringDataIn                        Global  - <DataPacketIn> member
*   string::npos                         Library - <cstring> 
*   stringTemp                           Local   - <cstring> temporary string data 
*   timespecNanoDelay                    Global  - <ctime> struct timespec holds time delay for nanosleep()
*   .timespecTimeLastHeartbeat           Global  - <ExperientDataClass> (struct timespec) member
*   timespecRemainingTime                Global  - <ctime> struct timespec argument for nanosleep()
*   .timespecTimeXmitStamp               Global  - <ExperientDataClass> (struct timespec) member
*   timer_tPortListenTimerId             Global  - <ExperientCommPort><ctime> timer id associated with Cad_Port_Listen_Event()
*   timer_tCadMessageMonitorTimerId      Global  - <ctime> timer id associated with Cad_Messaging_Monitor_Event()
*   timer_tCadScoreboardTimerId          Global  - <ctime> timer id associated with Cad_ScoreBoard_Event()
*   .UDP_Port                            Global  - <ExperientCommPort><ExperientUDPPort> object
*                                                                          
* Functions:
*   Abnormal_Exit()                     Global  <globalfunctions.h>                     (void)
*   ASCII_String()                      Global  <globalfunctions.h>                     (string)
*   Cad_ACK_Received()                  Local                                           (void)
*   Cad_Heartbeat_ACK_Received()        Local                                           (void)
*   Cad_Heartbeat_NAK_Received()        Local                                           (void)
*   Cad_NAK_Received()                  Local                                           (void)
*   Cad_Port_Listen_Event()             Local   SIGEV_THREAD function                   (void)
*   Cad_ScoreBoard_Event()              Local   SIGEV_THREAD function                   (void)
*   Cad_Stray_ACK()                     Local                                           (void)
*   Cad_Stray_NAK()                     Local                                           (void)
*   Cad_Messaging_Monitor_Event()       Local   SIGEV_THREAD function                   (void)
*   clock_gettime()                     Libray  <ctime>                                 (int)
*   .Config()                           Library <ipworks.h> <UDPPort>                   (char*)
*   .Config()                           Library <ipworks.h> <Ping>                      (char*)
*   Console_Message()                   Global  <globalfunctions.h>                     (void)
*   .c_str()                            Library <cstring>                               (const char*)
*   .empty()                            Library <queue>                                 (bool)
*   .end()                              Library <cstring>                               (*this)
*   enQueue_Message()                   Global  <globalfunctions.h>                     (void)
*   .erase()                            Library <cstring>                               (string::iterator)
*   .fAllThreadsReady()                 Global  <Thread_Trap>                           (bool)
*   .fClear_Record()                    Global  <ExperientDataClass>                    (void)
*   .fEmergencyPortShutDown             Global  <ExperientCommPort>                     (void)
*   .fIs_HeartBeat()                    Global  <ExperientDataClass>                    (bool)
*   FlushQueue()                        Local                                           (void)
*   .fMessage_Create()                  Global  <MessageClass>                          (void)
*   .front()                            Library <queue>                                 (*this)
*   .fSet_Heartbeat_Record()            Global  <ExperientDataClass>                    (void)
*   .fSetReducedHeartbeatMode()         Global  <ExperientCommPort>                     (void)
*   .fSet_Time_Recieved()               Global  <ExperientDataClass>                    (void)
*   getpid()                            Library <unistd.h>                              (pid_t)
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   .length()                           Library <cstring>                               (size_t)
*   nanosleep()                         Library <unistd.h>                              (int)
*   .pop()                              Library <queue>                                 (void)
*   PortQueueRoundRobin()               Global  <globalfunctions.h>                     (int)
*   Ports_Ready_To_Shut_Down()          Global  <globalfunctions.h>                     (bool)
*   pthread_create()                    Library <pthread.h>                             (int)
*   pthread_self()                      Library <pthread.h>                             (pthread_t)
*   pthread_setaffinity_np()            Library <pthread.h>                             (int)
*   pthread_attr_setdetachstate()       Library <pthread.h>                             (int)
*   Semaphore_Error()                   Global  <globalfunctions.h>                     (void)
*   sem_init()                          Library <semaphore.h>                           (int)
*   sem_wait()                          Library <semaphore.h>                           (int)
*   sem_post()                          Library <semaphore.h>                           (int)
*   .SetAcceptData()                    Library <ipworks.h> <UDPPort>                   (int)
*   .SetActive()                        Library <ipworks.h> <UDPPort>                   (int)
*   .SetDontRoute()                     Library <ipworks.h> <UDPPort>                   (int)
*   .SetLocalHost()                     Library <ipworks.h> <UDPPort>                   (int)                   
*   .SetLocalHost()                     Library <ipworks.h> <Ping>                      (int)
*   .SetLocalPort()                     Library <ipworks.h> <UDPPort>                   (int)
*   .SetMulticastGroup()                Library <ipworks.h> <UDPPort>                   (int)
*   .fSetPortInactive()                 Global  <ExperientDataClass>                    (void)
*   .SetRemoteHost()                    Library <ipworks.h> <UDPPort>                   (int)
*   .SetRemotePort()                    Library <ipworks.h> <UDPPort>                   (int)
*   .SetTimeout()                       Library <ipworks.h> <Ping>                      (int)
*   Set_Timer_Delay()                   Global  <globalfunctions.h>                     (void)
*   .size()                             Library <queue>                                 (size_t)
*   sleep()                             Library <unistd.h>                              (unsigned int)  
*   timer_settime()                     Library <ctime>                                 (int)
*   timer_create()                      Library <ctime>                                 (int)
*   timer_delete()                      Library <ctime>                                 (int)
*   .Error_Handler()                    Global  <ExperientUDPPort>                      (void)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2022 WestTel Corporation 
****************************************************************************************************/

void *Cad_Thread(void *voidArg)
{
        int                             i;
        int                             intRC;
        int                             intPortNum;
        int                             intDataToDisplay;
        int                             intFirstLook = 0;
        int                             intLogCode;
        queue <ExperientDataClass>*     queue_objCadInputData;
        
        ExperientDataClass              objCadData;
        struct sigevent                 sigeventCadScoreboardEvent, sigeventCadMessageMonitorEvent;
        string                          stringTemp;
        MessageClass                    objMessage;
        DataPacketIn                    CadPortDataRecieved;
        Ping                            objPing;
        size_t                          size_tStringPosition;
        pthread_t                       CADPortListenThread[NUM_CAD_PORTS_MAX];
        pthread_t                       pthread_tCadMessagingThread;
        pthread_t                       pthread_tCadScoreboardThread;
        param_t                         params[intNUM_CAD_PORTS+1];              
        pthread_attr_t                  pthread_attr_tAttr;
        Port_Data                       objPortData;
        struct timespec                 timespecRemainingTime;
        Thread_Data                     ThreadDataObj;
        
        
        extern vector <Thread_Data>         ThreadData;
        extern Initialize_Global_Variables  INIT_VARS;

        
        // fwd function declaration
        void Cad_ACK_Received (ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum); 
        void Cad_NAK_Received (ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
        void Cad_Stray_ACK    (ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
        void Cad_Stray_NAK    (ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
        void Cad_NAK_Heartbeat(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum);
        bool InitializeCADlegacyPorts(bool boolSKIPACTIVE = false);
        bool StartCADportListenThreads(pthread_t CADPortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t params[] );
        bool ShutDownCADlegacyPorts(pthread_t CADPortListenThread[], int numberofPorts);

        queue_objCadInputData 	= (queue <ExperientDataClass>*) voidArg;                         //recast voidArg
        bool boolACK,boolNAK 	= false;
/*
        // set CPU affinity
        intRC = pthread_setaffinity_np(pthread_self(), sizeof(CPU_AFFINITY_ALL_OTHER_THREADS_SET), &CPU_AFFINITY_ALL_OTHER_THREADS_SET);
        if (intRC) {objMessage.fMessage_Create(LOG_WARNING,442, objBLANK_CALL_RECORD, objBLANK_CAD_PORT, KRN_MESSAGE_142); enQueue_Message(CAD,objMessage);}
*/

        ThreadDataObj.strThreadName ="CAD";
        intRC = sem_wait(&sem_tMutexThreadData);
        if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cad.cpp::Cad_Thread()", 1);}
        ThreadData.push_back(ThreadDataObj);
        intRC = sem_post(&sem_tMutexThreadData);

        //create ping object
#ifdef IPWORKS_V16
        objPing.SetRuntimeLicense((char*)strIPWORKS_RUN_TIME_KEY.c_str());
#endif
	objPing.SetLocalHost((char*)HOST_IP.stringAddress.c_str());
        objPing.SetTimeout(intPING_TIMEOUT);

        //set heartbeat record
        obj_arrayCadWorkTable[intCadHeartbeatPosition].fClear_Record();			// set last+1 record
	obj_arrayCadWorkTable[intCadHeartbeatPosition].fSet_Heartbeat_Record();
//	obj_arrayCadWorkTable[intCadHeartbeatPosition].fSet_Time_Recieved();	       // to handle Heartbeats
     
 	objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,400, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objBLANK_CAD_PORT, 
                                   objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_400, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
	enQueue_Message(CAD,objMessage);
  
    
/*
	// Initialize CAD PORT I/O								expand this switch as we get more comms
        for (int i = 1; i <= intNUM_CAD_PORTS; i++)
        {
         CADPort[i].fInitializeCADport(i); 
                  
         objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,409, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, 
                                    objBLANK_TEXT_DATA,objBLANK_CAD_PORT, 
                                    objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_409); 
         enQueue_Message(CAD,objMessage);

        }// end for (int i = 1; i <= intNUM_CAD_PORTS; i++)

       
        // start port listen thread(s)
        pthread_attr_init(&pthread_attr_tAttr);
        pthread_attr_setdetachstate(&pthread_attr_tAttr, PTHREAD_CREATE_JOINABLE);
        for (int i = 1; i<= intNUM_CAD_PORTS; i++)
         {
          params[i].intPassedIn = i;
          intRC = pthread_create(&CADPortListenThread[i], &pthread_attr_tAttr, *Cad_Port_Listen_Thread, (void*) &params[i]);
          if (intRC) {Abnormal_Exit(CAD, EX_OSERR, CAD_MESSAGE_496, int2str(i));}
         }
*/
        //Start CAD Ports
        if (InitializeCADlegacyPorts()) {
         if (!StartCADportListenThreads( CADPortListenThread , &pthread_attr_tAttr, params)) {
           Abnormal_Exit(CAD, EX_OSERR, CAD_MESSAGE_496 , "00"); 
         }
        }
        

        //start CAD Timed Threads
        intRC = pthread_create(&pthread_tCadMessagingThread, &pthread_attr_tAttr, *Cad_Messaging_Monitor_Event, NULL);
        if (intRC) {Abnormal_Exit(CAD, EX_OSERR, CAD_MESSAGE_496);}
        intRC = pthread_create(&pthread_tCadScoreboardThread, &pthread_attr_tAttr, *Cad_ScoreBoard_Event, (void*) &obj_arrayCadWorkTable);
        if (intRC) {Abnormal_Exit(CAD, EX_OSERR, CAD_MESSAGE_496);}




  //      Start_CAD_Timed_Threads();
/*
        // set up Cad Message monitor timer event
	sigeventCadMessageMonitorEvent.sigev_notify 			= SIGEV_THREAD;		
	sigeventCadMessageMonitorEvent.sigev_notify_function 		= Cad_Messaging_Monitor_Event;
	sigeventCadMessageMonitorEvent.sigev_notify_attributes 		= NULL;

        // create timer
	intRC = timer_create(CLOCK_MONOTONIC, &sigeventCadMessageMonitorEvent, &timer_tCadMessageMonitorTimerId);
        if (intRC){Abnormal_Exit(CAD, EX_OSERR, KRN_MESSAGE_180, "timer_tCadMessageMonitorTimerId", int2strLZ(errno));}

        // load settings into stucture and then stucture into the timer
//	Set_Timer_Delay(&itimerspecCadMessageMonitorDelay, 0,intCAD_MESSAGE_MONITOR_INTERVAL_NSEC, 0, intCAD_MESSAGE_MONITOR_INTERVAL_NSEC );
	Set_Timer_Delay(&itimerspecCadMessageMonitorDelay, 0,intCAD_MESSAGE_MONITOR_INTERVAL_NSEC, 0, 0 );
	timer_settime( timer_tCadMessageMonitorTimerId, 0, &itimerspecCadMessageMonitorDelay, NULL);		// arm timer

	objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,426, objBLANK_CALL_RECORD, objBLANK_CAD_PORT, CAD_MESSAGE_426); 
	enQueue_Message(CAD,objMessage);
*/
/*
	// setup sigeventCadScoreboardEvent timer event
	sigeventCadScoreboardEvent.sigev_notify 			= SIGEV_THREAD;
	sigeventCadScoreboardEvent.sigev_value.sival_ptr 		= &obj_arrayCadWorkTable;		
	sigeventCadScoreboardEvent.sigev_notify_function 		= Cad_ScoreBoard_Event;
	sigeventCadScoreboardEvent.sigev_notify_attributes 	        = NULL;

        // create timer
	intRC = timer_create(CLOCK_MONOTONIC, &sigeventCadScoreboardEvent, & timer_tCadScoreboardTimerId);
        if (intRC){Abnormal_Exit(CAD, EX_OSERR, KRN_MESSAGE_180, "timer_tCadScoreboardTimerId", int2strLZ(errno));}

        // load settings into stucture and then stucture into the timer
 //       Set_Timer_Delay(&itimerspecCadScoreboardDelay, 0,intCAD_CLEANUP_INTERVAL_NSEC, 0, intCAD_CLEANUP_INTERVAL_NSEC );
        Set_Timer_Delay(&itimerspecCadScoreboardDelay, 0,intCAD_CLEANUP_INTERVAL_NSEC, 0, 0 );
 	timer_settime( timer_tCadScoreboardTimerId, 0, &itimerspecCadScoreboardDelay, NULL);			// arm timer

	objMessage.fMessage_Create(LOG_CONSOLE_FILE*boolDEBUG,425, objBLANK_CALL_RECORD, objBLANK_CAD_PORT, CAD_MESSAGE_425); 
	enQueue_Message(CAD,objMessage);
*/
	
	
	// CadProgram do loop
          
          do 
           {
	    							
	    intRC = sem_wait(&sem_tCadFlag);							// wait for data signal

            // this semaphore does not need error correcting ... subsequent code is protected from inability to lock 
            if (intRC)
             {
              objMessage.fMessage_Create(LOG_WARNING,418, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, 
                                         objBLANK_TEXT_DATA, objBLANK_CAD_PORT, 
                                         objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_418); 
	      enQueue_Message(CAD,objMessage);
             } 


            // Shut Down Trap Area
        if (boolSTOP_EXECUTION)	     {          
         break;                             // jump to while(!boolSTOP_EXECUTION)
        }// end if (boolSTOP_EXECUTION  )

	    
            // Process Inbound Data from Main Cad Input Q..........................................................
	    intRC = sem_wait(&sem_tMutexCadInputQ);						// Lock CAD INPUT Q
            if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadInputQ, "CAD    - sem_wait@sem_tMutexCadInputQ in Cad_Thread", 1);}
              
            if (!queue_objCadInputData->empty())
             {
              i = queue_objCadInputData->front().CallData.intPosn;
              if (i)
               {
	        intRC = sem_wait(&sem_tMutexCadWorkTable);					// Lock Work Table
                if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadWorkTable, "CAD    - sem_wait@sem_tMutexCadWorkTable in Cad_Thread", 1);}

                // check if index is already in use otherwise skip till next time TBC-> move to end of queue ??
                if (!obj_arrayCadWorkTable[i].fIs_A_Record())
                 {
                  queue_objCadInputData->front().fCAD_Strip_Uneeded_Data();
	          obj_arrayCadWorkTable[i] = queue_objCadInputData->front();
	          queue_objCadInputData->pop();
                  obj_arrayCadWorkTable[i].fSet_Time_Recieved();
                 }
                                                                 // UNLOCK Work Table
               sem_post(&sem_tMutexCadWorkTable);
             }
            else
             {
              SendCodingError( "cad.cpp - position zero sent to CAD thread !!!!!");
              queue_objCadInputData->pop();
             }
					        
             
             }//end if (!queue_objCadInputData.empty())
            sem_post(&sem_tMutexCadInputQ);						        // UNLock CAD INPUT Q
           
            // Process Data from CAD Ports............................................................................
            intFirstLook++;
            if (intFirstLook > intNUM_CAD_PORTS) {intFirstLook = 1;}           
                       
            while (PortQueueRoundRobin(queueCadPortData, intNUM_CAD_PORTS, intFirstLook, &sem_tMutexCadPortDataQ) != 0 )
             {
              // sequentially select port queue to pop() data (this prevents a DOS attack from hogging resources
              intPortNum = PortQueueRoundRobin(queueCadPortData, intNUM_CAD_PORTS, intFirstLook, &sem_tMutexCadPortDataQ); 
              intFirstLook++;
              if (intFirstLook > intNUM_CAD_PORTS) {intFirstLook = 1;}


               // lock CAD WORK Q
  	      intRC = sem_wait(&sem_tMutexCadWorkTable);
              if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadWorkTable, "[WARNING] CAD - sem_wait@sem_tMutexCadWorkTable in Cad_Thread", 1);}

              boolACK = boolNAK = false;

              // pop data off the Q
              intRC = sem_wait(&sem_tMutexCadPortDataQ);
              if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadPortDataQ, "CAD    - sem_wait@sem_tMutexCadPortDataQ in Cad_Thread", 1);}
	      CadPortDataRecieved = queueCadPortData[intPortNum].front();
              queueCadPortData[intPortNum].pop();
              sem_post(&sem_tMutexCadPortDataQ);

              intPortNum = CadPortDataRecieved.intPortNum;
              objPortData.fLoadPortData(CAD, intPortNum);

              // shut down port if string buffer size exceeded .. 
              if(CadPortDataRecieved.boolStringBufferExceeded)
               {
                CADPort[intPortNum].fEmergencyPortShutDown(objBLANK_CALL_RECORD, "String Buffer Size > ", CadPortDataRecieved.intLength);
                sem_post(&sem_tMutexCadWorkTable);
                continue;
               }


              // are we expecting data ?
              if (intCadActivePosition == 0 )
               {
                // turn off  port ..
                CADPort[intPortNum].boolPortActive = false;
                // Data recieved outside response window...
                FlushQueue(queueCadPortData, intPortNum, CAD);

                // skip to while (!queueCadPortData.empty())
                sem_post(&sem_tMutexCadWorkTable);continue;
                
               }
              else
               {
                // Data recieved inside response window..

                 // if port already acked or Heartbeat port not active skip...to while (!queueCadPortData.empty())
                 if( (CADPort[intPortNum].boolACKReceived)||(!CADPort[intPortNum].boolPortActive&&(intCadActivePosition == intCadHeartbeatPosition)) )
                  {
                   
                   FlushQueue(queueCadPortData, intPortNum, CAD);
//                  CADPort[intPortNum].intUnexpectedDataCharacterCount+= CadPortDataRecieved.intLength;

                   // skip to end of while (!queueCadPortData.empty())
                   sem_post(&sem_tMutexCadWorkTable);continue;
                  }

                // is data too long ?
                if (CadPortDataRecieved.intLength > 1)
                 {
                  if(CADPort[intPortNum].boolEmergencyShutdown) 
                   {CADPort[intPortNum].intUnexpectedDataCharacterCount = 0; sem_post(&sem_tMutexCadWorkTable); continue;}
                  // response recieved too long
                  
                  // data will only contain either a ACK or NAK at this point not both
                  size_tStringPosition = CadPortDataRecieved.stringDataIn.find_first_of(charACK);
                  if (size_tStringPosition != string::npos){boolACK = true;}
                  size_tStringPosition = CadPortDataRecieved.stringDataIn.find_first_of(charNAK);
                  if (size_tStringPosition != string::npos){boolNAK = true;}

                  // ack or nak will be at the end... it is exlcuded from bad char count                     
                  if (boolACK||boolNAK){CadPortDataRecieved.stringDataIn.erase(CadPortDataRecieved.stringDataIn.end()-1);}
                  stringTemp = "";
                  if (CadPortDataRecieved.stringDataIn.length() > intMAX_DATA_TO_DISPLAY) {intDataToDisplay = intMAX_DATA_TO_DISPLAY;}
                  else                                                                    {intDataToDisplay = CadPortDataRecieved.stringDataIn.length();}

                  stringTemp = ASCII_String(CadPortDataRecieved.stringDataIn.c_str(), intDataToDisplay);
                  
                  // show message under certain conditions
                  if (CADPort[intPortNum].boolReducedHeartBeatMode&&obj_arrayCadWorkTable[intCadActivePosition].fIs_HeartBeat()) 
                      {intLogCode = LOG_WARNING*CADPort[intPortNum].boolUnexpectedDataShowMessage*boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC;}
                  else
                      {intLogCode = LOG_WARNING*CADPort[intPortNum].boolUnexpectedDataShowMessage;}

                  if(intLogCode)
                   {
                    objMessage.fMessage_Create(intLogCode,427, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, 
                                               objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                                               objBLANK_ALI_DATA,CAD_MESSAGE_427, stringTemp); 
	            enQueue_Message(CAD,objMessage);
                   }

                  // increment count
                  CADPort[intPortNum].fIncrementBadCharacterCount(CadPortDataRecieved.stringDataIn.length());
                  CADPort[intPortNum].fIncrementConsecutiveBadRecordCounter();
                  // only show 1st message until an ack or nak is recieved or port is made active again
                  CADPort[intPortNum].boolUnexpectedDataShowMessage = false;
          
                  // if data contains neither ACK or NAK skip to while (!queueCadPortData.empty())
                  if (!(boolACK||boolNAK)){sem_post(&sem_tMutexCadWorkTable);continue;}
                 }
                else
                 {
                  // Data is of correct length (i.e. = 1)
                  size_tStringPosition = CadPortDataRecieved.stringDataIn.find_first_of(charACK);
                  if (size_tStringPosition != string::npos){boolACK = true;}
                  size_tStringPosition = CadPortDataRecieved.stringDataIn.find_first_of(charNAK);
                  if (size_tStringPosition != string::npos){boolNAK = true;}

                  // if data contains neither ACK or NAK show error 
                  if (!(boolACK||boolNAK))
                   {
                    stringTemp = ASCII_String(CadPortDataRecieved.stringDataIn.c_str(), 1);
                    
                    if (CADPort[intPortNum].boolReducedHeartBeatMode&&obj_arrayCadWorkTable[intCadActivePosition].fIs_HeartBeat()) 
                      {intLogCode = LOG_WARNING*CADPort[intPortNum].boolUnexpectedDataShowMessage*boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC;}
                    else
                      {intLogCode = LOG_WARNING*CADPort[intPortNum].boolUnexpectedDataShowMessage;}
                    if(intLogCode)
                     {
                      objMessage.fMessage_Create(intLogCode,427, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, 
                                                 objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, 
                                                 objBLANK_ALI_DATA,CAD_MESSAGE_427, stringTemp); 
	              enQueue_Message(CAD,objMessage);
                     }
                    // only show 1st message until an ack or nak is recieved or port is made active again
                    CADPort[intPortNum].boolUnexpectedDataShowMessage = false;
                    CADPort[intPortNum].fIncrementBadCharacterCount(1);
                    CADPort[intPortNum].fIncrementConsecutiveBadRecordCounter();
                    //... skip to while (!queueCadPortData.empty())
                    sem_post(&sem_tMutexCadWorkTable);continue;
                   }// end if (!(boolACK||boolNAK))
                  else
                   {
                    CADPort[intPortNum].intConsecutiveBadRecordCounter = 0;
                   }
                 }// if (CadPortDataRecieved.intLength > 1) else

               }// end if (intCadActivePosition == 0 ) else

       

              CADPort[intPortNum].boolUnexpectedDataShowMessage = true;
              if(CADPort[intPortNum].intUnexpectedDataCharacterCount > 0)
               {
                objMessage.fMessage_Create(LOG_WARNING*boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC,431, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, 
                                           objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_431,
                                           int2strLZ(CADPort[intPortNum].intUnexpectedDataCharacterCount)); 
	        enQueue_Message(CAD,objMessage);
                CADPort[intPortNum].intUnexpectedDataCharacterCount = 0;
               }              
              else if ((boolACK) && (intCadActivePosition == intCadHeartbeatPosition))	{ Cad_Heartbeat_ACK_Received    (obj_arrayCadWorkTable, intPortNum); }
              else if (boolACK)                                                         { Cad_ACK_Received              (obj_arrayCadWorkTable, intPortNum); }
              else if ((boolNAK) && (intCadActivePosition == intCadHeartbeatPosition))	{ Cad_Heartbeat_NAK_Received    (obj_arrayCadWorkTable, intPortNum); }
              else if (boolNAK)                                                         { Cad_NAK_Received              (obj_arrayCadWorkTable, intPortNum); }


              // Unlock CAD WORK Q
              sem_post(&sem_tMutexCadWorkTable);

             }//end while (!queueCadPortData.empty())

           // Update Trap
           if (boolUPDATE_VARS) {
            UpdateVars.boolCadThreadReady = true;
            while ((UpdateVars.boolMAINThreadWait)&&(!boolSTOP_EXECUTION)) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
            if (!boolSTOP_EXECUTION) {

             if (INIT_VARS.CADinitVariable.boolRestartLegacyCADPorts){
              // Shut down listen Threads()
              ShutDownCADlegacyPorts(CADPortListenThread, INIT_VARS.CADinitVariable.intOldNumberOfLegacyCADports);           
              INIT_VARS.CADinitVariable.boolRestartLegacyCADPorts = false;
              InitializeCADlegacyPorts();
              cout << "CALLING StartCADportListenThreads()" << endl;
              if (!StartCADportListenThreads(CADPortListenThread, &pthread_attr_tAttr, params)) {
                  SendCodingError("cad.cpp - listen thread failure - recommend restart!");
              }
              
             }
             else {
              InitializeCADlegacyPorts(true);
             }
             UpdateVars.boolCadThreadReady = false;       
            }
           }




           
       } while (!boolSTOP_EXECUTION);
	
	// end CadProgram do loop
       //  timer_delete( timer_tCadScoreboardTimerId );					// shut down timer events
	// timer_delete( timer_tCadMessageMonitorTimerId );

         pthread_join( pthread_tCadScoreboardThread, NULL);        
         pthread_join( pthread_tCadMessagingThread, NULL);

        // wait for port threads to shut down 
        //    while (!Ports_Ready_To_Shut_Down(CADPort, intNUM_CAD_PORTS)){nanosleep( &timespecNanoDelay, &timespecRemainingTime);}
         for (int i = 1; i <=  intNUM_CAD_PORTS; i++)
          {
           pthread_join( CADPortListenThread[i], NULL); 
          }

         OrderlyShutDown.boolCadThreadReady = true;
   //      sleep(intTHREAD_SHUTDOWN_DELAY_SEC);
        // wait for all threads to shut down
         while (!OrderlyShutDown.fAllThreadsReady()) {nanosleep( &timespecNanoDelay, &timespecRemainingTime);}

        objMessage.fMessage_Create(0, 499, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                                   objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_499);
        objMessage.fConsole();
        return NULL;
 
}// end Cad_Thread

/****************************************************************************************************
*
* Name:
*  Function: void Cad_ScoreBoard_Event()
*
*
* Description:
*   An event handler within the function void *Cad_Thread() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from within the thread Cad_Thread.  When activated it
*
*   1. Disarms the timer
*
*   2. Checks for an active record (one that has been transmitted awaiting ACK) and Checks if a timeout has occured.
*      Depending on whether the record is the heartbeat record or a regular record, a retransmit occurs.
*
*   3. or .. Checks for the oldest stored record and transmits it.
*
*   4. or .. if there is no stored record: Check if heartbeat interval time exceeded and transmit heartbeat
*
*   5. Re-arms the timer
*
*   note. the table is split between the normal Positions (1 - intNUM_WRK_STATIONS) and the manual rebid positions (95-99) 
*
* Parameters: 				
*   union sigval union_sigvalArg	a library <csignal> structure that contains a pointer to pass  
*					ExperientDataClass*  obj_arrayCadWorkTable created in Cad_Thread
*
* Variables:
*   CAD                                 Global  - <header.h> threadorPorttype enumeration member
*   CLOCK_REALTIME			Library - <ctime> constant
*   doubleDiffOld			Local   - oldest time differential
*   doubleDiffNew			Local   - time differential
*   enumCleanup_Process		        Local   - enumeration for readability
*   i					Local   - general purpose integer 
*   intCAD_ACK_THRESHOLD_SEC		Global  - constant integer of how long after xmit ACK is required
*   intCadActivePosition                Global  - current transmitted record awaiting ACK
*   intCadHeartbeatPosition             Global  - index (100) number of heartbeat record
*   intEnd                              Local   - integer used for loop end number
*   intNUM_WRK_STATIONS		        Global  - (const) integer number of installed system Workstations (positions)
*   intRC                               Local   - return code from a function
*   intStart                            Local   - integer used for loop start number
*   itimerspecCadScoreboardDelay        Global  - <ctime> struct itimerspec that holds timing data
*   itimerspecDisableTimer              Global  - <ctime> struct itimerspec used to disable a timer
*   obj_arrayCadWorkTable               Local   - <ExperientDataClass*> ptr to worktable passed as argument
*   ptrVal				Local   - temp pointer
*   sem_tMutexCadWorkTable		Global  - semaphore mutex to lock obj_arrayCadWorkTable
*   sem_tCadFlag                        Global  - semaphore signal destination
*   timer_tCadScoreboardTimerId         Global  - <ctime> structure holding timing info for this timed event
*   timespecTimeNow			Local   - <ctime> <timespec> structure holding current time
*   timespecTimeRecordReceivedStamp     Global  - <ExperientDataClass> member
*   y                                   Local   - general purpose integer 
*                                                                          
* Functions:  
*   clock_gettime()			Library <ctime>                         (int)
*   Check_CAD_Heartbeat_Interval()	Local                                   (void) 
*   Check_CAD_Heartbeat_ACK_Timeout()   Local                                   (void) 
*   Check_CAD_ACK_Timeout()	        Local                                   (void)  
*   .fIs_A_Record()			Global  <ExperientDataClass>            (bool)
*   Semaphore_Error()                   Global  <globalfunctions.h>             (void)
*   sem_wait()             		Library <semaphore.h>                   (int)
*   sem_post()				Library <semaphore.h>                   (int)
*   Send_CAD_Record()                   Local                                   (void)  
*   time_difference()			Global  <globalfunctions.h>             (long double)
*   timer_settime()                     Library <ctime>                         (int)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/

void *Cad_ScoreBoard_Event(void *voidArg)
{
// void 			*ptrVal 			= union_sigvalArg.sival_ptr;
// ExperientDataClass*  	obj_arrayCadWorkTable		= (ExperientDataClass*) ptrVal;	// recast arg


 ExperientDataClass*  	obj_arrayCadWorkTable		= (ExperientDataClass*) voidArg;	// recast arg
 MessageClass 		objMessage;
 long double		doubleDiffOld ,doubleDiffNew;
 struct timespec	timespecTimeNow;
 int                    intRC;
 int                    intStart, intEnd;
 Thread_Data            ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 void Check_CAD_ACK_Timeout           (ExperientDataClass* ptr_obj_arrayCadWorkTable); 
 void Send_CAD_Record                 (ExperientDataClass* ptr_obj_arrayCadWorkTable);
 void Check_CAD_Heartbeat_ACK_Timeout (ExperientDataClass* ptr_obj_arrayCadWorkTable);
 void Check_CAD_Heartbeat_Interval    (ExperientDataClass* ptr_obj_arrayCadWorkTable);

 ThreadDataObj.strThreadName ="CAD Scoreboard";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cad.cpp::Cad_ScoreBoard_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);


do
{

 experient_nanosleep(intCAD_CLEANUP_INTERVAL_NSEC);
 if (boolUPDATE_VARS) {continue;}


 // send signal to cad flag to keep queue check going
 sem_post(&sem_tCadFlag);


/*								
 // disable timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tCadScoreboardTimerId, 0, &itimerspecDisableTimer, NULL);}
 if(!boolIsMaster) 
  {
   intCadActivePosition = 0;
   clock_gettime(CLOCK_REALTIME, &timespecTimeNow);  
   for(int i = 1; i <= intNUM_CAD_PORTS; i++)
    {
     CADPort[i].timespecTimeLastHeartbeat.tv_sec = timespecTimeNow.tv_sec;
     CADPort[i].timespecTimeLastHeartbeat.tv_nsec = timespecTimeNow.tv_nsec;
     CADPort[i].fClearReducedHeartbeatMode(true);
    }
   //re-arm timer
   if(boolSTOP_EXECUTION) {return ;}
   else                   {timer_settime(timer_tCadScoreboardTimerId, 0, &itimerspecCadScoreboardDelay, NULL);}
   return;
  }
 */

 // Lock Work Table
 intRC = sem_wait(&sem_tMutexCadWorkTable);
 if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadWorkTable, "sem_wait@sem_tMutexCadWorkTable in Cad_ScoreBoard_Event", 1);}

 if       (intCadActivePosition==intCadHeartbeatPosition)     	                {Check_CAD_Heartbeat_ACK_Timeout (obj_arrayCadWorkTable);}
 else if  (intCadActivePosition)                                                {Check_CAD_ACK_Timeout           (obj_arrayCadWorkTable);}
 else
  {
   // at this point there is no active record : search for oldest recieved record & set as active
        clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
        doubleDiffOld = 0;


       for (int y = 1; y<=2; y++)
        {
         if (y == 1){intStart = 1;  intEnd = intNUM_WRK_STATIONS;}       // check installed workstations
         else       {intStart = 95; intEnd = 99;}                        // check manual rebid positions

        for (int i = intStart; i <= intEnd; i++)
         {
          if (obj_arrayCadWorkTable[i].fIs_A_Record())						
           {
            doubleDiffNew = time_difference (timespecTimeNow , obj_arrayCadWorkTable[i].timespecTimeRecordReceivedStamp);
            if (doubleDiffNew > doubleDiffOld) { intCadActivePosition = i; doubleDiffOld = doubleDiffNew; }
           }// end if (obj_arrayCadWorkTable[i].fIs_A_Record())

         }// end for (int i = 1; i <= intNUM_WRK_STATIONS; i++)

        }// end for (int y = 1; y<=2; y++)

       // if no active record found check heartbeat interval, if record found.. transmit it
       if (!intCadActivePosition) 	{Check_CAD_Heartbeat_Interval(obj_arrayCadWorkTable);} 
       else                             {Send_CAD_Record             (obj_arrayCadWorkTable);} 
  }
 
 // UnLock Work Table  
 sem_post(&sem_tMutexCadWorkTable);
 
  // check time in reduced heartbeat mode
 Check_Port_Down_Notification(CAD, intNUM_CAD_PORTS, CADPort);

 //check for port restart condition
 for (int i = 1; i <= intNUM_CAD_PORTS; i++) {CADPort[i].Check_Port_Restart();}

 // check for stale data in port buffers
 for (int i = 1; i <= intNUM_CAD_PORTS; i++) {CADPort[i].UDP_Port.fCheck_Buffer();} 
/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tCadScoreboardTimerId, 0, &itimerspecCadScoreboardDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);
 
 objMessage.fMessage_Create(0, 494, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, 
                            objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_494);
 objMessage.fConsole();

return NULL;								
}// end Cad_ScoreBoard_Event

/****************************************************************************************************
*
* Name:
*  Function: void Cad_Messaging_Monitor_Event()
*
*
* Description:
*   An event handler within *Cad_Thread() 
*
*
* Details:
*   This function is implemented as a SIGEV_THREAD timer from Cad_Thread.  When activated it
*
*   1. Disarms the timer
*
*   2. Checks for messages on the message Q & retrieves messages until Q is empty and processes them.
*
*   3. Checks for Port Down Status and checks if reminder message should be sent
*
*   4. Checks for Port Restart condition if a Port has been down
*
*   5. Checks the Cad Port Input Buffers and clears them of stale data
*
*   6. Re-arms the timer
*
*
* Parameters: 				
*   union_sigvalArg			unused for now
*
* Variables:
*   CAD                                  Global  - <header.h> threadorPorttype enumeration member
*   CADPort[]                            Global  - <ExperientDataClass> object array
*   .intMessageCode                      Local   - <MessageClass>
*   int2ndFailureCounter                 Global  - <Thread_Statistic_Counters> member
*   intACKCounter                        Global  - <Thread_Statistic_Counters> member
*   intACKTimeoutCounter                 Global  - <Thread_Statistic_Counters> member
*   intInhibitedTransmissionCounter      Global  - <Thread_Statistic_Counters> member
*   intNAKCounter                        Global  - <Thread_Statistic_Counters> member
*   intHeartBeatSentCounter              Global  - <Thread_Statistic_Counters> member
*   intNUM_CAD_PORTS                     Global  - <globals.h>
*   intRecordSentCounter                 Global  - <Thread_Statistic_Counters> member
*   intRetransmitCounter                 Global  - <Thread_Statistic_Counters> member
*   intSignalSemaphoreFailureCounter     Global  - <Thread_Statistic_Counters> member
*   intStrayACKCounter                   Global  - <Thread_Statistic_Counters> member
*   intStrayNAKCounter                   Global  - <Thread_Statistic_Counters> member
*   intUnrecognizedDataCounter           Global  - <Thread_Statistic_Counters> member
*   intMessageCode                       Global  - <MessageClass> member
*   intNUM_CAD_PORTS                     Global  - <defines.h>
*   intPortNum                           Global  - <MessageClass> member
*   intRC                                Local   - return code from functions
*   itimerspecCadMessageMonitorDelay     Global  - <ctime> struct which contains timing data that for the timer
*   itimerspecDisableTimer               Global  - <ctime> struct which contains data that disables the timer
*   objMessage				 Local   - <MessageClass> object
*   queue_objCadMessageQ		 Global  - <queue> <MessageClass> objects
*   sem_tMutexCadMessageQ		 Global  - <semaphore.h> mutex to prevent multiple operations on the queue
*   sem_tMutexMainMessageQ		 Global  - <semaphore.h> mutex to prevent multiple operations on the queue
*   timer_tCadMessageMonitorTimerId      Global  - <ctime> timer id for this function
*   .UDP_Port                            Global  - <ExperientCommPort><ExperientUDPPort> member object
*                                                                          
* Functions:
*   Check_Port_Down_Notification()      Global  <globalfunctions.h>                     (void)
*   .Check_Port_Restart()               Global  <ExperientDataClass>                    (void)
*   .empty()				Library <queue>                                 (bool)
*   .front()				Library <queue>                                 (*this)
*   .fCheck_Buffer()                    Global  <ExperientCommPort><ExperientUDPPort>   (void)                    
*   .pop()				Library <queue>                                 (void)                        
*   .push()				Library <queue>                                 (void)
*   Semaphore_Error()                   Global  <globalfunctions.h>                     (void)
*   sem_wait()             		Library <semaphore.h>                           (int)
*   sem_post()				Library <semaphore.h>                           (int)
*   timer_settime()                     Library <ctime>                                 (int)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
//void Cad_Messaging_Monitor_Event(union sigval union_sigvalArg)
void *Cad_Messaging_Monitor_Event(void *voidArg)
{
 MessageClass			objMessage;
 int                            intRC;
 Thread_Data                    ThreadDataObj;

 extern vector <Thread_Data> 	ThreadData;

 ThreadDataObj.strThreadName ="CAD Message Monitor";
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cad.cpp::Cad_Messaging_Monitor_Event", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);

/*
 //disable timer
 if(boolSTOP_EXECUTION) {return;}
// else                   {timer_settime(timer_tCadMessageMonitorTimerId, 0, &itimerspecDisableTimer, NULL);}
*/

do
{

 experient_nanosleep(intCAD_MESSAGE_MONITOR_INTERVAL_NSEC); 
 if (boolUPDATE_VARS) {continue;}

 //lock Main message Q then lock CAD message Q
 intRC = sem_wait(&sem_tMutexMainMessageQ);
 if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexMainMessageQ, "sem_wait@sem_tMutexMainMessageQ in Cad_Messaging_Monitor_Event", 1);}
 intRC = sem_wait(&sem_tMutexCadMessageQ);
 if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexCadMessageQ, "sem_wait@&sem_tMutexCadMessageQ in Cad_Messaging_Monitor_Event", 1);}

 // empty the CAD Q
 while (!queue_objCadMessageQ.empty())
  {
   objMessage = queue_objCadMessageQ.front();
   queue_objCadMessageQ.pop();

   switch (objMessage.intMessageCode)
    {
     case 0:
      // No action
      break;

     case 400:
      // CAD Thread Created
      break;

     case 401:						
      // Cad Heartbeat Sent
      break; 

     case 402:						
      // Cad Transmit Record
      break;

     case 403:						
      // Cad Retransmit Heartbeat
      break;

     case 404:						
      // Cad ACK Received Record 
      break;

     case 405:						
      // Cad ACK Received HeartBeat
      break;

     case 406:						
      // Cad NAK Recieved Record
      break;

     case 407:						
      // Cad NAK Received Heartbeat
      break;

     case 408:						
      // Begin Reduced Heartbeat Interval	
      break;

     case 409:						
      // Port Initialized 
      break;

     case 410:						
      // Cad Stray ACK
      break;

     case 411:						
      // Cad Stray NAK
      break;

     case 412:						
      // Cad ACK Timeout HeatBeat
      break;

     case 413:						
      // Cad ACK Timeout Record
      break;

     case 414:						
      // Unrecognized Response Received
      break;

     case 415:                                           
      // ALI Record Send Failure
      break;

     case 416:                                           
      // Heartbeat Send Failure
      break;

     case 417:                                           
      // Stray Cad Response too long
      break;

     case 418:
      // Signal Semaphore failed to lock 
      break;

     case 419:
      //  Port Down notification
      break;

     case 420:
      //  Port Down Reminder
      break;

     case 421:
      // First ACK recieved
      break;

     case 422:
      // End Reduced Heartbeat Interval
      break;

     case 423:
      // Port failed Ping Test
      break;

     case 424:
      // Listen event initialized
      break;

     case 425:
      // Scoreboard Event initialized
      break;

     case 426:
      // Health Monitor Timer Event Initialized
      break;

     case 427:
      // Cad Response too long 
      break;

     case 428:
      // Data not sent due to port failure
      break;

     case 429:
      // Heartbeat Retransmit Failure
      break;

     case 430:
      // Record retransmit
      break;

     case 431:
      // Unexpected Data
      break;

     case 432:
      // Listen Thread created
      break;

     case 433:
      // Port Data Reception Inhibited
      break;

     case 434:
      // Port Restart Failed
      break;

     case 435:
      // Port Data Reception Restored
      break;

     case 436:
      // Erase Msg Send Failure
      break;

     case 437:
      // Erase Msg Sent
      break;

     case 438:
      // Erase Msg Retransmited
      break;

     case 439:
      // Erase Msg Lost
      break;

     case 440:
      // Erase Msg ACK Timeout
      break;

     case 441:
      // Retransmit Failure
      break;
     case 444:
      // "-[INFO] Reconfiguring CAD Global Variables"
      break;

     case 445:
      // "-[INFO] No Changes to CAD Global Variables"
      break;
      
     case 446:
      // "-[INFO] Reconfiguring CAD Ports, Before %%% , After %%%"
      break;

     case 460:
      // Ping test results
      break;

     case 461:
      // Unexpected Data Received from Outside IP Address
      break;

     case 462:
      // TCP/IP Connect Attempt
      break;

     case 463:
      // UDP Error
      break;

     case 464:
      // Port Restored
      break;

     case 465:
      // UDP Error
      break;

     case 466:
      // Buffer cleared
      break;

     case 467:
      //  Stray NAK Counter Rollover
      break;

     case 468:
      //  Stray ACK Counter Rollover
      break;

     case 469:
      //  "[WARNING] CAD %%%  < Stray NAK(s) over Past 24 Hours = %%%"
      break;

     case 470:
      //  "[WARNING] CAD %%%  < Stray ACK(s) over Past 24 Hours = %%%"
      break;

     case 471:
      //   Consecutive Consecutive LONG CAD Response Counter
      break;

     case 472:
      //   Discarded Packet Counter Rollover
      break;

     case 473:
      //   "[WARNING] %%% - Incoming Network Packets Discarded over Past 24 Hours = %%%" 
      break;
     case 474:
      //   "CAD %%% < Raw Data Received from IP=%%% (Data Follows) %%%"
      break;
     case 475:
      // "%%% > Raw Data sent to IP=%%% (Data Follows)"
      break;
     case 476:
      // "-[WARNING] CAD Buffer size exceeded, size: %%%"
      break;
     case 477:
      // "-[INFO] No Changes to CAD Global Variables"
      break;
     case 478:
      // "-[INFO] Reconfiguring CAD Global Variables"
      break;
     case 479:
      // "-[INFO] Reconfiguring CAD %%% Ports, Before %%% , After %%%"
      break;
     case 494:
     //  "-CAD Scoreboard Event Thread Complete"
      break; 
     case 495:
      //  "-CAD Messaging Event Thread Complete"
      break;
     case 496:
      //  "[WARNING] CAD %%% - Port Listen Thread Failed to Initialize, System Rebooting"
      break;
     case 497:
      //  "[WARNING] CAD Thread Failed to Initialize, System Rebooting"
      break;

     case 498:
      //  "[WARNING] CAD Mutex Semaphore Error (Code=%%%, MSG=%%%)"
      break;
     case 499:
      // "CAD Thread Complete"
      break;
     case 999:
      // unused
      break;	
     default:
      cout << objMessage.intMessageCode + " Message Code Unrecognized in CAD Monitor" << endl;
    }// end switch

   // Push message onto Main Message Q
   queue_objMainMessageQ.push(objMessage);
     
  }// end while (!queue_objCadMessageQ.empty())

 // UnLock Cad Meesage Q then Main Message Q
 sem_post(&sem_tMutexCadMessageQ);
 sem_post(&sem_tMutexMainMessageQ);


/*
 //re-arm timer
 if(boolSTOP_EXECUTION) {return;}
 else                   {timer_settime(timer_tCadMessageMonitorTimerId, 0, &itimerspecCadMessageMonitorDelay, NULL);}
*/

} while (!boolSTOP_EXECUTION);

 objMessage.fMessage_Create(0, 495, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA,
                            objBLANK_CAD_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_495);
 objMessage.fConsole();

return NULL;
}// end Cad_Messaging_Monitor_Event()



/****************************************************************************************************
*
* Name:
*  Function: void FlushQueue(queue <DataPacketIn> *PortQueue, int intPortNum, threadorPorttype enumPortType)
*
*
* Description:
*   A Utility Function 
*
*
* Details:
*   This function empties "Flushes" the Queue that is passed in
*          
*    
* Parameters:
*   enumPortType                                - threadorPorttype				
*   *PortQueue				        - queue <DataPacketIn>
*   intPortNum					- integer
*
* Variables:
*   intRC                               Local   - int for function return code
*   sem_tMutexCadPortDataQ              Global  - <globals.h>                              
*                                                                          
* Functions:
*   .empty()                            Library - <queue>                       (bool)
*   .pop()                              Library - <queue>                       (void)
*   Semaphore_Error()                   Global  - <globalfunctions.h>           (void)                  
*   sem_post()				Library - <semaphore.h>                 (int)
*   sem_wait()                          Library - <semaphore.h>                 (int)                                 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void FlushQueue(queue <DataPacketIn> *PortQueue, int intPortNum, threadorPorttype enumPortType)
{
 int intRC;

 intRC = sem_wait(&sem_tMutexCadPortDataQ);
 if (intRC){Semaphore_Error(intRC, enumPortType, &sem_tMutexCadPortDataQ, "sem_wait@sem_tMutexCadPortDataQ in FlushQueue", 1);}

 while (!PortQueue[intPortNum].empty()) { PortQueue[intPortNum].pop();}

 sem_post(&sem_tMutexCadPortDataQ);
}

/****************************************************************************************************
*
* Name:
*  Function: void *Cad_Port_Listen_Thread(void *voidArg)
*
*
* Description:
*   An thread within Cad_Thread() 
*
*
* Details:
*   This function is an implemented thread from Cad_Thread.  When activated it
*   runs the Do_Events() function from the ipworks library until the boolean
*   boolSTOP_EXECUTION is true.  It's function is to keep listening on the 
*   selected port.
*
*
* Parameters:
*   voidArg                             an integer passed as void representing the port number
*
*
* Variables:
*   boolDEBUG                           Global  - <globals.h>
*   .boolReadyToShutDown                Global  - <ExperientCommPort> member
*   boolSTOP_EXECUTION                  Global  - <globals.h>
*   CAD                                 Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_432                     Global  - <defines.h> Listen Thread Created output message
*   CADPort[]                           Global  - <ExperientCommPort> object
*   intPortNum                          Local   - port number to listen to
*   LOG_CONSOLE_FILE                    Global  - <defines.h> log code
*   objMessage                          Local   - <MessageClass> object
*                                                                          
* Functions:  
*   .DoEvents()                         Library <ipworks><UDPPort>                      (int)
*   .fMessage_Create()                  Global  <MessageClass>                          (void)
*   getpid()                            Library <unistd.h>                              (pid_t)
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   pthread_self()                      Library <pthread.h>                             (pthread_t)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2022 WestTel Corporation 
****************************************************************************************************/ 
void *Cad_Port_Listen_Thread(void *voidArg)
{
 // cast voidArg
 param_t*       p          = (param_t*) voidArg;
 int            intPortNum = p->intPassedIn;
 int            intRC;
 MessageClass   objMessage;
 Port_Data      objPortData;
 Thread_Data    ThreadDataObj; 
     
 extern vector <Thread_Data>            ThreadData;
 extern Initialize_Global_Variables     INIT_VARS;

 ThreadDataObj.strThreadName ="CAD Port " + int2strLZ(intPortNum);
// ThreadDataObj.ThreadPID     = getpid();
// ThreadDataObj.pth_SelfID    = pthread_self();
 intRC = sem_wait(&sem_tMutexThreadData);
 if (intRC){Semaphore_Error(intRC, CAD, &sem_tMutexThreadData, "sem_wait@sem_tMutexThreadData in cad.cpp::Cad_Port_Listen_Thread", 1);}
 ThreadData.push_back(ThreadDataObj);
 intRC = sem_post(&sem_tMutexThreadData);
 objPortData.fLoadPortData(CAD, intPortNum);
 objMessage.fMessage_Create(LOG_CONSOLE_FILE,432, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_432, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 enQueue_Message(CAD,objMessage);

 while ((!boolSTOP_EXECUTION) && (!INIT_VARS.CADinitVariable.boolRestartLegacyCADPorts))   {
     
   experient_nanosleep(LISTEN_THREAD_SLEEP_NSEC);
   switch (CADPort[intPortNum].ePortConnectionType) {
     case TCP:
     CADPort[intPortNum].TCP_Server_Port.DoEvents(); break;
     case UDP:
     CADPort[intPortNum].UDP_Port.DoEvents(); break;
     default:
     SendCodingError("cad.cpp *Cad_Port_Listen_Thread() -No Connection type");break;
   }
 } 

 objMessage.fMessage_Create(0, 432, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD , objBLANK_TEXT_DATA, objPortData, 
                           objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_432b, int2strLZ(getpid()), int2strLZ(pthread_self()) ); 
 objMessage.fConsole(); 

 
 CADPort[intPortNum].TCP_Server_Port.Shutdown();
 CADPort[intPortNum].UDP_Port.SetActive(false);
 CADPort[intPortNum].boolReadyToShutDown = true;

 return NULL;
								
}// end Cad_Port_Listen_Thread

/****************************************************************************************************
*
* Name:
*  Function: void Cad_ACK_Received()
*
*
* Description:
*   A function called from within CAD_Thread() 
*
*
* Details:
*   This function is called if a CAD ACK for a record is received and accomplishes the following:
*          
*    1. set ACK on port
*    2. set port to inactive
*    3  check if this is first ACK
*    4. reset active position (xmited awaiting ACK)& clear record on table (array) to 0 if ACK on all CAD port(s)
*    5. clear Cad Reduced Heartbeat Interval on port if needed (CADPort[i].boolReducedHeartBeatMode)
*    6. reset CAD heartbeat interval on port to normal         (CADPort[i].intHeartbeatInterval)
*    7. reset time of last hearbeat        
*  
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable		ExperientDataClass*     pointer to array of ExperientDataClass objects 
*   intPortNum				int  	                port number receiving ACK
*
* Variables:
*   .boolACKReceived                    Global  - <ExperientCommPort> member
*   .boolFirstACKRcvd                   Global  - <ExperientCommPort> member
*   .boolPortActive                     Global  - <ExperientCommPort> member.. true if port active
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member.. true if reduced hearbeat interval activated on port
*   CAD                                 Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_404                     Global  - <defines.h> ACK Received output message
*   CAD_MESSAGE_421                     Global  - <defines.h> First ACK Received output message
*   CAD_MESSAGE_422                     Global  - <defines.h> End Reduced Heartbeat Interval output message
*   CADPort[]                           Global  - <ExperientCommPort> object
*   CLOCK_REALTIME                      Library - <ctime> constant
*   intCadActivePosition			Global  - <globals.h> current transmitted record awaiting ACK
*   .intCallbackNumber                  Global  - <ExperientDataClass> member
*   intNUM_CAD_PORTS                    Global  - <globals.h>
*   .intPosn                            Global  - <ExperientDataClass> member
*   .intUniqueCallID                    Global  - <ExperientDataClass> member
*   LOG_CONSOLE_FILE                    Global  - <defines.h> code to display to console and file to disk
*   LOG_WARNING                         Global  - <defines.h> code to email, display to console and file to disk
*   objMessage                          Local   - <MessageClass> object
*   timespecTimeLastHeartbeat           Global  - <ExperientDataClass><ctime> struct timespec member time of last heartbeat TX or ACK or NAK
*                                                                       
* Functions:
*   ClearPorts()                        Global   - <globalfunctions.h>                          (void) 
*   clock_gettime()			Library  - <ctime>                                      (int)
*   enQueue_Message()                   Global   - <globalfunctions.h>                          (void)                 
*   .fClear_Record()                    Global   - <ExperientDataClass>                         (void)
*   .fClearReducedHeartbeatMode()       Global   - <ExperientCommPort>                          (void)
*   .fMessage_Create()                  Global   - <MessageClass>                               (void)
*   .fSetPortInactive()                 Global   - <ExperientCommPort>                          (void)
*   int2strLZ()                         Global   - <globalfunctions.h>                          (string) 
*   IsAnyPortActive()                   Global   - <globalfunctions.h>                          (bool) 
*                   
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void Cad_ACK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum)
{ 
 MessageClass		objMessage;
 Port_Data              objPortData;

 objPortData.fLoadPortData(CAD, intPortNum);
 objMessage.fMessage_Create(LOG_CONSOLE_FILE, 404, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), 
                            objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_404, int2strLZ(intPortNum));
 enQueue_Message(CAD,objMessage);

 CADPort[intPortNum].boolACKReceived = true;
 CADPort[intPortNum].fSetPortInactive();

 //first CAD ACK Message
 if (!CADPort[intPortNum].boolFirstACKRcvd)
  {
   CADPort[intPortNum].boolFirstACKRcvd = true;
   objMessage.fMessage_Create(LOG_INFO,421, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_421); 
   enQueue_Message(CAD,objMessage);
  } 
   
 // set/reset Heartbeat interval on port [intPortNum] to normal
 if (CADPort[intPortNum].boolReducedHeartBeatMode)
  {
   objMessage.fMessage_Create(LOG_WARNING,422, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_422);
   enQueue_Message(CAD,objMessage);
  }
 CADPort[intPortNum].fClearReducedHeartbeatMode(CADPort[intPortNum].boolEmergencyShutdown);

 // reset heartbeat time

 clock_gettime(CLOCK_REALTIME, &CADPort[intPortNum].timespecTimeLastHeartbeat);
 CADPort[intPortNum].fSetPortInactive();


 // check for ACK on all ports.... clear and reset active postion if true....

 if (!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))
  {
   ptr_obj_arrayCadWorkTable[intCadActivePosition].fClear_Record();
   ClearPorts(CADPort, intNUM_CAD_PORTS);
   intCadActivePosition = 0;
  }

}// end Cad_ACK_Received

/****************************************************************************************************
*
* Name:
*  Function: void Cad_Heartbeat_ACK_Received()
*
*
* Description:
*   A function called from within CAD_Thread() 
*
*
* Details:
*   This function is called if a CAD ACK for a heartbeat is received and accomplishes the following:
*          
*    1. set port to inactive
*    2. check if this is first ACK
*    3. clear Cad Reduced Heartbeat Interval on port if needed (CADPort[i].boolReducedHeartBeatMode)
*    4. reset CAD heartbeat interval on port to normal         (CADPort[i].intHeartbeatInterval)
*    5. reset time of last heartbeat        
*  
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable		ExperientDataClass* pointer to array of ExperientDataClass objects 
*   intPortNum				int  	      port number receiving ACK
*
* Variables:
*   .boolACKReceived                    Global  - <ExperientCommPort> member
*   .boolFirstACKRcvd                   Global  - <ExperientCommPort> member
*   boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC   Global  - <globals.h> diplay/log heartbeats if true
*   .boolPortActive                     Global  - <ExperientCommPort> member.. true if port active
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member.. true if reduced interval heartbeat on port [i] after 2 failed tx attempts
*   .boolXmitRetry                      Global  - <ExperientCommPort> member.. true if a retry is allowed
*   CAD                                 Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_405                     Global  - <defines.h> ACK Received output message
*   CAD_MESSAGE_421                     Global  - <defines.h> First ACK Received output message
*   CAD_MESSAGE_422                     Global  - <defines.h> End Reduced Heartbeat Interval output message
*   CADPort[]                           Global  - <ExperientCommPort> object array
*   CLOCK_REALTIME                      Library - <ctime> constant
*   intCadActivePosition			Global  - <globals.h> current transmitted record awaiting ACK
*   LOG_CONSOLE_FILE                    Global  - <defines.h> code for log to file and display
*   LOG_WARNING                         Global  - <defines.h> code for log to email,file and display
*   objMessage                          Local   - <MessageClass> object
*   .timespecTimeLastHeartbeat           Global  - <ExperientDataClass><ctime> struct timespec member.. time of last heartbeat TX or ACK or NAK
*                                                                       
* Functions:
*   ClearPorts()                         Global   - <globalfunctions.h>                         (void) 
*   clock_gettime()                      Library  - <ctime>                                     (int)
*   enQueue_Message()                    Global   - <globalfunctions.h>                         (void)
*   .fClearReducedHeartbeatMode()        Global   - <ExperientCommPort>                         (void)
*   .fMessage_Create()                   Global   - <MessageClass>                              (void) 
*   .fSetPortInactive()                  Global   - <ExperientCommPort>                         (void)
*   int2strLZ()                          Global   - <globalfunctions.h>                         (string)
*   IsAnyPortActive()                    Global   - <globalfunctions.h>                         (bool) 
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/ 
void Cad_Heartbeat_ACK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum)
{ 
 MessageClass		objMessage;
 Port_Data              objPortData;

 objPortData.fLoadPortData(CAD, intPortNum);
 objMessage.fMessage_Create(LOG_CONSOLE_FILE * ((boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC)||(!CADPort[intPortNum].boolXmitRetry)||
                            (CADPort[intPortNum].boolReducedHeartBeatMode)||(CADPort[intPortNum].boolNAKReceived)) , 
                             405, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, objPortData, 
                             objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                             CAD_MESSAGE_405,"","","","","","",DEBUG_MSG);
 enQueue_Message(CAD,objMessage);

 //set port to not active
 CADPort[intPortNum].fSetPortInactive();

 //set ACK on port

 CADPort[intPortNum].boolACKReceived = true;

 //first CAD ACK Message
 if (!CADPort[intPortNum].boolFirstACKRcvd)
  {
   CADPort[intPortNum].boolFirstACKRcvd = true;
   objMessage.fMessage_Create(LOG_INFO,421, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_421); 
   enQueue_Message(CAD,objMessage);
  }

 // set/reset Heartbeat interval on port [intPortNum] to normal
 if (CADPort[intPortNum].boolReducedHeartBeatMode)
  {
   objMessage.fMessage_Create(LOG_WARNING,422, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                              objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_422);
   enQueue_Message(CAD,objMessage);
  }

 CADPort[intPortNum].fClearReducedHeartbeatMode(CADPort[intPortNum].boolEmergencyShutdown);

 // reset heartbeat time

 clock_gettime(CLOCK_REALTIME, &CADPort[intPortNum].timespecTimeLastHeartbeat);

 if (!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))
  {
   ClearPorts(CADPort, intNUM_CAD_PORTS);
   intCadActivePosition = 0;
  }

}// end Cad_Heartbeat_ACK_Received

/****************************************************************************************************
*
* Name:
*  Function: void Cad_NAK_Received()
*
*
* Description:
*   A function called from within CAD_Thread() 
*
*
* Details:
*   This function is called if a CAD NAK is received and accomplishes the following:
*          
*    1. retransmit record
*    2. if previously retransmitted:
*       a. generate error 
*       b. set ACK on port to prevent further xmits
*       c. check all ports for ACKs (if true clear record & reset active position to 0)       
*  
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable		        ExperientDataClass* pointer to array of ExperientDataClass objects 
*   intPortNum				        int  	      port number receiving NAK
*
* Variables:
*   .boolCADEraseMsg                            Global  - <ExperientDataClass> member.. true if data is Erase Message
*   .boolNAKReceived                            Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member.. true if reduced heartbeat mode active on port[]
*   CAD                                         Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_406                             Global  - <defines.h> NAK Recieved output message
*   CAD_MESSAGE_408                             Global  - <defines.h> Begin Reduced Heartbeat Interval output message
*   CAD_MESSAGE_428                             Global  - <defines.h> ALI Re cord Lost output message
*   CAD_MESSAGE_439                             Global  - <defines.h> Erase Msg Lost output message
*   CADPort[]                                   Global  - <ExperientCommPort> object array 
*   CLOCK_REALTIME                              Library - <ctime> constant
*   intCadActivePosition			        Global  - <globals.h> current transmitted record awaiting ACK
*   .intCallbackNumber                          Global  - <ExperientDataClass> member
*   .intHeartbeatInterval                       Global  - <ExperientCommPort> member.. current heartbeat interval on port[]
*   intLogCode                                  Local   - code to log thread
*   intMessageCode                              Local   - integer message code
*   intNUM_CAD_PORTS                            Global  - <globals.h>
*   .intPosn                                    Global  - <ExperientDataClass> member
*   intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC       Global  - <defines.h>
*   .intUniqueCallID                            Global  - <ExperientDataClass> member
*   LOG_CONSOLE_FILE                            Global  - <defines.h> code to display to console, file to disk
*   LOG_WARNING                                 Global  - <defines.h> code to email warning, display to console, file to disk
*   objMessage				        Local   - <MessageClass> object
*   stringLostRecordMsg                         Local   - <cstring> temp string to pass message data
*   .timespecTimeLastHeartbeat                  Global  - <ExperientDataClass><ctime> struct timespecmember.. time of last heartbeat TX or ACK or NAK on port[]
*   timespecTimeNow                             Local   - <ctime> struct timespec current time data
*   .timespecTimeXmitStamp                      Global  - <ExperientDataClass><ctime> struct timespec member.. time of last TX on port[]
*                                                                          
* Functions:
*   ClearPorts()                        Global   - <globalfunctions.h>                          (void) 
*   clock_gettime()                     Library  - <ctime>                                      (int)
*   enQueue_Message()		        Global   - <globalfunctions.h>                          (void)
*   .fCad_ReXmit()			Global   - <ExperientDataClass>                         (bool)
*   .fClear_Record()			Global   - <ExperientDataClass>                         (void)
*   .fMessage_Create()			Global   - <MessageClass>                               (void)
*   .fPingStatusString()                Global   - <ExperientUDPPort>                           (string)
*   .fSetReducedHeartbeatMode()         Global   - <ExperientCommPort>                          (void)
*   .fSetPortInactive()                 Global   - <ExperientCommPort>                          (void)
*   IsAnyPortActive()                   Global   - <globalfunctions.h>                          (bool) 
*   int2strLZ()			        Global   - <globalfunctions.h>                          (string)
*   seconds2time()                      Global   - <globalfunctions.h>                          (string)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Cad_NAK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum)              
{
 MessageClass		objMessage;
 struct timespec        timespecTimeNow;
 int                    intLogCode = LOG_WARNING;
 int                    intMessageCode;
 string                 stringLostRecordMsg;
 Port_Data              objPortData;

 objPortData.fLoadPortData(CAD, intPortNum); 

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 if (CADPort[intPortNum].boolReducedHeartBeatMode){intLogCode = LOG_CONSOLE_FILE;}
 
 // ALI Record NAK Recieved Msg
 objMessage.fMessage_Create(intLogCode, 406, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), 
                            objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_406, 
                            int2strLZ(intCadActivePosition), ptr_obj_arrayCadWorkTable[intCadActivePosition].CallData.fTenDigitCallbackGUIformat()); 
 enQueue_Message(CAD,objMessage);

 CADPort[intPortNum].boolNAKReceived = true;
  
 // attempt reXmit
 if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(intPortNum, CADPort[intPortNum].boolRecordACKRequired))
  {
   // Not able to TX....
   // set reduced heartbeat interval port set Port Inactive it will clear
   CADPort[intPortNum].fSetPortInactive();

   // reset HBeat time
   CADPort[intPortNum].timespecTimeLastHeartbeat = timespecTimeNow;
 
   // Record Lost Message
   if(ptr_obj_arrayCadWorkTable[intCadActivePosition].boolCADEraseMsg)
    {stringLostRecordMsg = CAD_MESSAGE_439; intMessageCode = 439;}
   else
    {stringLostRecordMsg = CAD_MESSAGE_428; intMessageCode = 428;}

   objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), 
                              objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringLostRecordMsg, 
                              int2strLZ(intCadActivePosition), ptr_obj_arrayCadWorkTable[intCadActivePosition].CallData.fTenDigitCallbackGUIformat());
   enQueue_Message(CAD,objMessage);

   // don't set/send message if already reduced HB mode....
   if ((!CADPort[intPortNum].boolReducedHeartBeatMode)&&(CADPort[intPortNum].boolHeartBeatACKrequired))
    {
     CADPort[intPortNum].fSetReducedHeartbeatMode(ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), timespecTimeNow);
    }
   
   
   if (!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))		      
    {
     ptr_obj_arrayCadWorkTable[intCadActivePosition].fClear_Record();
     ClearPorts(CADPort, intNUM_CAD_PORTS);
     intCadActivePosition = 0;
    }//end if (ptr_obj_arrayCadWorkTable[intCadActivePosition].fCheck_Ack_on_All_Ports())

  }// end if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(intPortNum))
  
}// end Cad_NAK_Received()

/****************************************************************************************************
*
* Name:
*  Function: void Cad_Heartbeat_NAK_Received()
*
*
* Description:
*   A function called from within CAD_Thread() 
*
*
* Details:
*   This function is called if a CAD NAK is received and accomplishes the following:
*          
*    
*    1. check if reduced heartbeat mode active
*    2. if previously retransmitted:
*       a. generate error 
*       b. set false on CADPort[i].boolPortActive to prevent further xmits
*       c. check if all ports inactive ( reset active position to 0)       
*  
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable		        ExperientDataClass* pointer to array of ExperientDataClass objects 
*   intPortNum				        int  	      port number receiving NAK
*
* Variables:
*   boolDEBUG                                   Global  - <globals.h> display/log debug data
*   .boolPortActive                             Global  - <ExperientCommPort> member
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member
*   .boolXmitRetry                              Global  - <ExperientCommPort> member
*   CAD                                         Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_407                             Global  - <defines.h> NAK Recieved output message
*   CAD_MESSAGE_408                             Global  - <defines.h> Begin Reduced Heartbeat Interval output message
*   CADPort[]                                   Global  - <ExperientCommPort> object .. array of port(s) 
*   CLOCK_REALTIME                              Library - <ctime>
*   intCadActivePosition			        Global  - current transmitted record awaiting ACK
*   intLogCode                                  Local   - code for log thread
*   intNUM_CAD_PORTS                            Global  - <globals.h>
*   intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC       Global  - <defines.h>
*   LOG_CONSOLE_FILE                            Global  - <defines.h> code to log thread to display to console, file to disk
*   LOG_WARNING                                 Global  - <defines.h> code to log thread to email warning, display to console, file to disk
*   objMessage				        Local   - <MessageClass> object
*   .timespecTimeLastHeartbeat                  Global  - <ExperientDataClass> member
*   timespecTimeNow                             Local   - <ctime> struct timespec current time
*                                                                          
* Functions:
*   ClearPorts()                                Global   - <globalfunctions.h>                          (void) 
*   clock_gettime()                             Library  - <ctime>                                      (int)
*   enQueue_Message()		                Global   - <globalfunctions.h>                          (void)
*   .fCad_ReXmit()			        Local    - <ExperientDataClass>                         (bool)
*   .fClear_Record()			        Global   - <ExperientDataClass>                         (void)
*   .fMessage_Create()                          Global   - <MessageClass>                               (void)
*   .fPingStatusString()                        Global   - <ExperientUDPPort>                           (string)
*   .fSetPortInactive()                         Global   - <ExperientCommPort>                          (void)
*   .fSetReducedHeartbeatMode()                 Global   - <ExperientCommPort>                          (void)
*   IsAnyPortActive()                           Global   - <globalfunctions.h>                          (bool)
*   int2strLZ()			                Global   - <globalfunctions.h>                          (string)
*   seconds2time()                              Global   - <globalfunctions.h>                          (string)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Cad_Heartbeat_NAK_Received(ExperientDataClass* ptr_obj_arrayCadWorkTable, int intPortNum)              
{
 MessageClass		objMessage;
 int                    intLogCode = LOG_WARNING;
 struct timespec        timespecTimeNow;
 Port_Data              objPortData;

 objPortData.fLoadPortData(CAD, intPortNum);
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 // NAK Message
 if (CADPort[intPortNum].boolReducedHeartBeatMode){intLogCode = LOG_CONSOLE_FILE*boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC ;}
 objMessage.fMessage_Create(intLogCode,407, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                            objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_407);
 enQueue_Message(CAD,objMessage);

 // set port to inactive .... reset heartbeat time to time now
 CADPort[intPortNum].fSetPortInactive();
 CADPort[intPortNum].boolNAKReceived = true;
 CADPort[intPortNum].timespecTimeLastHeartbeat = timespecTimeNow;

    
// clock_gettime(CLOCK_REALTIME, &ptr_obj_arrayCadWorkTable[intCadActivePosition].timespecTimeLastHeartbeat[intPortNum]);

 // if heartbeat NAK occurs during reduced heartbeat interval .... no retransmit
 
   // attempt reXmit
   if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(intPortNum, CADPort[intPortNum].boolHeartBeatACKrequired))
    {
     
     //reset single xmit retry
//     ptr_obj_arrayCadWorkTable[intCadActivePosition].bool_arrayCadXmitRetry[intPortNum] = true;
     CADPort[intPortNum].boolXmitRetry = true;

     // set reduced hb mode
     if((!CADPort[intPortNum].boolReducedHeartBeatMode)&&(CADPort[intPortNum].boolHeartBeatACKrequired))
      {
       CADPort[intPortNum].fSetReducedHeartbeatMode(ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), timespecTimeNow); 
      }
   
    }// end if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(intPortNum))
  
  
 // check for all ports inactive

 if (!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))
  {
   ClearPorts(CADPort, intNUM_CAD_PORTS);
   intCadActivePosition = 0;
  }
 
}// end Cad_Heartbeat_NAK_Received()

/****************************************************************************************************
*
* Name:
*  Function: Check_CAD_ACK_Timeout()
*
*
* Description:
*   A function called in function void Cad_ScoreBoard_Event() 
*
*
* Details:
*   This function:
*
*   1. Is called when there is an active record (one that has been transmitted awaiting ACK) and checks each port for a timeout.
*      
*   2. If a timeout has occured:
*      a. If record has not been previously retransmitted	: retransmit record
*      b. If record has been previously retransmitted		: generate error, set reduced heartbeatmode & clear record
*
*   3. if no timeout: continue
*  
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable	                ExperientDataClass* - pointer to array of ExperientDataClass objects 
*						
*
* Variables:
*   .boolACKReceived                            Global  - <ExperientCommPort> member.. true if ACK has  been received 
*   .boolCADEraseMsg                            Global  - <ExperientCommPort> member.. true if data is an Erase msg
*   .boolOutsideIPTraffic                       Global  - <ExperientCommPort> member.. true if Outside IP Traffic has been received 
*   .boolPortActive                             Global  - <ExperientCommPort> member.. true if port is active 
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member.. true if port is in reduced heartbeat mode
*   CAD                                         Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_413                             Global  - <defines.h> ALI Record ACK Timeout output message
*   CAD_MESSAGE_428                             Global  - <defines.h> ALI Record Lost output message
*   CAD_MESSAGE_439                             Global  - <defines.h> Erase Msg Lost output message
*   CAD_MESSAGE_440                             Global  - <defines.h> Erase Msg ACK Timeout output message
*   CAD_MESSAGE_461                             Global  - <defines.h> Unexpected Data Received from Outside IP Address output message
*   CADPort[]                                   Global  - <ExperientCommPort> object array
*   CLOCK_REALTIME			        Library - <ctime> constant
*   doubleTimeDiff			        Local   - time differential
*   i					        Local   - general purpose integer 
*   intCAD_ACK_THRESHOLD_SEC		        Global  - <defines.h>  how long after xmit ACK is required
*   intCadActivePosition		        Global  - <globals.h> current transmitted record awaiting ACK
*   .intCallbackNumber                          Global  - <ExperientDataClass> member
*   .intHeartbeatInterval	                Global  - <ExperientCommPort> member.. current heartbeat interval on port
*   intNUM_CAD_PORTS			        Global  - <defines.h> number of installed CAD ports
*   .intPosn                                    Global  - <ExperientDataClass> member
*   intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC       Global  - <defines.h>
*   .intUniqueCallID                            Global  - <ExperientDataClass> member
*   LOG_WARNING                                 Global  - <defines.h> code to log thread email warning, display to console, file to disk
*   objMessage				        Local	- <MessageClass> object
*   stringLostRecordMsg                         Local   - <cstring> temp string to hold message data
*   .stringOutsideIPAddr                        Global  - <ExperientCommPort><ExperientUDPPort> member
*   .timespecTimeLastHeartbeat                  Global  - <ExperientDataClass> member.. time of last heartbeat TX or ACK or NAK on port[]
*   timespecTimeNow                             Global  - <ctime> struct timespec holding current time data
*   .timespecTimeXmitStamp                      Global  - <ExperientDataClass> member.. time of last TX of record on port[]
*   .UDP_Port                                   Global  - <ExperientCommPort><ExperientUDPPort> object
*                                                                          
* Functions:
*   ClearPorts()                        Global   - <globalfunctions.h>                  (void)   
*   clock_gettime()			Library  - <ctime>                              (int)
*   enQueue_Message()		        Global   - <globalfunctions.h>                  (void)
*   .fCad_ReXmit()			Local    - <ExperientDataClass>                 (bool)
*   .fClear_Record()			Local    - <ExperientDataClass>                 (void)
*   .fMessage_Create()			Local    - <MessageClass>                       (void)
*   .fSetReducedHeartbeatMode()         Global   - <ExperientCommPort>                  (void)
*   int2strLZ()			        Global   - <globalfunctions.h>                  (string)
*   IsAnyPortActive()                   Global   - <globalfunctions.h>                  (bool)
*   seconds2time()                      Global   - <globalfunctions.h>                  (string)
*   time_difference()			Global   - <globalfunctions.h>                  (long double)
* 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_CAD_ACK_Timeout(ExperientDataClass* ptr_obj_arrayCadWorkTable) 
{
 long double		doubleTimeDiff;			
 struct timespec	timespecTimeNow;
 MessageClass		objMessage;
 int                    intMessageCode;
 string                 stringLostRecordMsg;
 Port_Data              objPortData;

 
 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);

 for(int i = 1; i <= intNUM_CAD_PORTS; i++)
  {
   doubleTimeDiff = 0;
   if (!CADPort[i].boolRecordACKRequired) {CADPort[i].boolACKReceived = true; CADPort[i].boolPortActive= false; continue;}
 
   // determine time difference..if port is active
   if (CADPort[i].boolPortActive )
    {doubleTimeDiff = time_difference(timespecTimeNow, CADPort[i].timespecTimeXmitStamp);}
  
   // Check to see if there is a timeout condition. If not do nothing....
   if  (doubleTimeDiff > intCAD_ACK_THRESHOLD_SEC)
    {
     objPortData.fClear();
     objPortData.fLoadPortData(CAD, i);
     // time threshold exceeded.... Check if port is not ACKed 
     if (!CADPort[i].boolACKReceived)
      {
       if (CADPort[i].UDP_Port.boolOutsideIPTraffic)
        {
         objMessage.fMessage_Create(LOG_WARNING,461, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), objBLANK_TEXT_DATA, 
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_461, CADPort[i].UDP_Port.stringOutsideIPAddr); 
         enQueue_Message(CAD,objMessage);
         CADPort[i].UDP_Port.boolOutsideIPTraffic = false;
        }

       if(ptr_obj_arrayCadWorkTable[intCadActivePosition].boolCADEraseMsg)
        {stringLostRecordMsg = CAD_MESSAGE_440; intMessageCode = 440;}
       else
        {stringLostRecordMsg = CAD_MESSAGE_413; intMessageCode = 413;}

       objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), objBLANK_TEXT_DATA,
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringLostRecordMsg, 
                                  int2strLZ(intCadActivePosition), ptr_obj_arrayCadWorkTable[intCadActivePosition].CallData.fTenDigitCallbackGUIformat());
       enQueue_Message(CAD,objMessage);
       
       // retransmit ... if not able to retransmit: set ACK on port & generate error
       if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(i,CADPort[i].boolRecordACKRequired))
        {
         
         CADPort[i].fSetPortInactive();

         //Record Lost
         if(ptr_obj_arrayCadWorkTable[intCadActivePosition].boolCADEraseMsg)
          {stringLostRecordMsg = CAD_MESSAGE_439; intMessageCode = 439;}
         else
          {stringLostRecordMsg = CAD_MESSAGE_428; intMessageCode = 428;}

         objMessage.fMessage_Create(LOG_WARNING, intMessageCode, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), 
                                    objBLANK_TEXT_DATA, objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, stringLostRecordMsg, 
                                    int2strLZ(intCadActivePosition), ptr_obj_arrayCadWorkTable[intCadActivePosition].CallData.fTenDigitCallbackGUIformat());
         enQueue_Message(CAD,objMessage);

         // check if in reduced hb mode ..
         if ((!CADPort[i].boolReducedHeartBeatMode)&&(CADPort[i].boolHeartBeatACKrequired))
          {
           CADPort[i].fSetReducedHeartbeatMode(ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(), timespecTimeNow);
          }
        CADPort[i].timespecTimeLastHeartbeat = CADPort[i].timespecTimeXmitStamp;  
       
         
        }// end if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(i))

      }// end if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCheck_Ack_on_Port(i))

    }// end if (doubleDiffOld > intCAD_ACK_THRESHOLD_SEC)

  }// end for (int i = 1; i <= intNUM_CAD_PORTS; i++)


 // Check for ACK on all ports (clear record and reset active position if condition true)

 if (!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))
  {
   ptr_obj_arrayCadWorkTable[intCadActivePosition].fClear_Record();
   ClearPorts(CADPort, intNUM_CAD_PORTS);
   intCadActivePosition = 0;
  }//end if (!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))

}// End Check_CAD_ACK_Timeout

 /****************************************************************************************************
*
* Name:
*  Function: void Send_CAD_Record()
*
*
* Description:
*   A function called in function void Cad_ScoreBoard_Event() 
*
*
* Details:
*   This function sends the oldest non active record (intCadActivePosition) for transmission on all ports
*
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable           ExperientDataClass* pointer to array of ExperientDataClass objects 
*						
*
* Variables:
*   .boolCADEraseMsg                    Global  - <ExperientDataClass> member
*   .boolSendCADEraseMsg                Global  - <ExperientCommPort> member
*   CADPort[]                           Global  - <ExperientCommPort> object array
*   i                                   Local   - integer counter
*   intCadActivePosition		Global  - current transmitted record awaiting ACK
*   intNUM_CAD_PORTS                    Global  - number of CAD ports
*   intRC                               Local   - return code for functions
*   NUM_CAD_PORTS_MAX                   Global  - <defines.h>
*
*                                                                       
* Functions:  
*   fCAD_Xmit_ALI()			Global   - <ExperientDataClass>                 (int)
*   fCAD_Xmit_Erase()                   Global   - <ExperientDataClass>                 (int)
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/      
void Send_CAD_Record(ExperientDataClass* ptr_obj_arrayCadWorkTable)
{
 int    intRC;
 extern ExperientCommPort                       CADPort                                 [NUM_CAD_PORTS_MAX+1];

 for(int i = 1; i <= intNUM_CAD_PORTS; i++)
  {
   if (!CADPort[i].fPostionUsesPort(intCadActivePosition))   {continue;}

   if (ptr_obj_arrayCadWorkTable[intCadActivePosition].boolCADEraseMsg)
    {
     if (CADPort[i].boolSendCADEraseMsg){intRC = ptr_obj_arrayCadWorkTable[intCadActivePosition].fCAD_Xmit_Erase(i);}
    }
   else                                 {intRC = ptr_obj_arrayCadWorkTable[intCadActivePosition].fCAD_Xmit_ALI(i);}

  }// end for (i = 1;...
   
}//void Send_CAD_Record()

/****************************************************************************************************
*
* Name:
*  Function: Check_CAD_Heartbeat_Interval()
*
*
* Description:
*   A function called in function void Cad_ScoreBoard_Event() 
*
*
* Details:
*   This function:
*
*   1. Is called when there is no active record (one that has been transmitted awaiting ACK) and checks if a the
*      heartbeat interval time has been exceeded.
*      
*   2. If time has been exceeded:		transmit heartbeat
*
*      a. if TX fails :                         1. if in reduced hearbeat mode set for no retransmit
*                                               2. else set for retransmit 
*      
*   3. if time has not been exceeded:		exit
*  
*
* Parameters: 				
*   ExperientDataClass* ptr_obj_arrayCadWorkTable	pointer to array of CAD objects 
*						
*
* Variables:
*   .boolRecordACKRequired  	                Global  - <ExperientCommPort> member .. true if ACK required on port
*   boolDEBUG                           Global  - <defines.h> show/log debug data
*   .boolPortActive                     Global  - <ExperientCommPort> member .. true if port is active
*   .boolHeartbeatRequired              Global  - <ExperientCommPort> member .. true if heartbeat required on port
*   boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC   Global  - <globals.h> diplay heartbeats if true 
*   .boolReducedHeartBeatMode           Global  - <ExperientCommPort> member .. true if port in reduced heartbeat mode
*   CAD                                 Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_401                     Global  - <defines.h> Heartbeat Sent output message
*   CAD_MESSAGE_416                     Global  - <defines.h> Heartbeat Send Failure output message
*   CADPort[]                           Global  - <ExperientCommPort> object array
*   charCADHEARTBEATMSG                 Global  - <globals.h> encoded heartbeat message
*   CLOCK_REALTIME			Library - <ctime> constant
*   doubleTimeDiff			Local   - time differential
*   i					Local   - general purpose integer 
*   intCAD_ACK_THRESHOLD_SEC		Global  - <defines.h>  how long after xmit an ACK recv is required
*   intCadHeartbeatPosition             Global  - position number used for heartbeats
*   intCadActivePosition		Global  - <globals.h> current transmitted record awaiting ACK
*   intLogCode                          Local   - code to send to log thread
*   .intHeartbeatInterval	        Global  - <ExperientCommPort> member .. current heartbeat interval on port
*   intNUM_CAD_PORTS			Global  - <defines.h> number of installed CAD ports
*   intRC                               Local   - return code
*   LOG_CONSOLE_FILE                    Global  - <defines.h> log code
*   LOG_WARNING                         Global  - <defines.h> log code 
*   objMessage				Local	- <MessageClass> object
*   .timespecTimeLastHeartbeat          Global  - <ExperientDataClass> <ctime> time of last heartbeat TX or ACK or NAK
*   timespecTimeNow			Local   - <ctime> struct timespec holding current time
*                                                                          
* Functions:  
*   clock_gettime()			Library <ctime>                                 (int)
*   enQueue_Message()		        Global  <globalfunctions.h>                     (void)
*   .fMessage_Create()			Local   <MessageClass>                          (void)
*   .fSetPortActive()                   Global  <ExperientCommPort>                     (void)
*   int2strLZ()                         Global  <globalfunctions.h>                     (string)
*   sem_post()				Library <semaphore.h>                           (int)                  
*   time_difference()                   Global  <globalfunctions.h>                     (long double)
*   Transmit_Data()                     Global  <globalfunctions.h>                     (int)
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_CAD_Heartbeat_Interval(ExperientDataClass* ptr_obj_arrayCadWorkTable)
{
 long double		doubleTimeDiff;
 struct timespec	timespecTimeNow;
 MessageClass		objMessage;
 int			intRC;
 int                    intLogCode = LOG_CONSOLE_FILE*boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC;
 Port_Data              objPortData;
  

 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 
 for (int i = 1; i<= intNUM_CAD_PORTS; i++)
  {
   if (!CADPort[i].boolHeartbeatRequired) {clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeLastHeartbeat); CADPort[i].boolPortActive = false;continue;}

   doubleTimeDiff = time_difference(timespecTimeNow, CADPort[i].timespecTimeLastHeartbeat);
   //  Check if heartbeat interval is exceeded
   if (doubleTimeDiff > CADPort[i].intHeartbeatInterval)
    {
     objPortData.fClear();
     objPortData.fLoadPortData(CAD, i); 
     intRC = Transmit_Data(CAD, i, charCADHEARTBEATMSG, 4);
     clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeLastHeartbeat);
     CADPort[i].timespecTimeXmitStamp = CADPort[i].timespecTimeLastHeartbeat;
     if (intRC)
      {
       // TX failed 
       if (CADPort[i].boolReducedHeartBeatMode){intLogCode = LOG_WARNING*boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC ;}
       else                                    {intLogCode = LOG_WARNING;}
       if (intLogCode)
        {
         objMessage.fMessage_Create(intLogCode,416, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA, 
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_416,"","","","","","",DEBUG_MSG); 
         enQueue_Message(CAD,objMessage);
        }

       //set new time interval to prevent immediate retry.
       clock_gettime(CLOCK_REALTIME, &CADPort[i].timespecTimeLastHeartbeat);
       CADPort[i].timespecTimeXmitStamp = CADPort[i].timespecTimeLastHeartbeat;
       // forces a timeout.... if ACKS not required
       if (!CADPort[i].boolEmergencyShutdown){CADPort[i].fSetPortActive();}
       intCadActivePosition = intCadHeartbeatPosition;

       //skip to next port in loop
       continue;
      }
     else
      {
       // TX successful
       if (CADPort[i].boolReducedHeartBeatMode){intLogCode = LOG_CONSOLE_FILE * boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC;}
       else                                    {intLogCode = LOG_CONSOLE_FILE * boolLOG_DISPLAY_CAD_HEARTBEAT_TRAFFIC;}

       objMessage.fMessage_Create(intLogCode,401, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_401,"","","","","","",DEBUG_MSG); 
       enQueue_Message(CAD,objMessage);
      }// end if else

                                                   
     if ((CADPort[i].boolHeartBeatACKrequired)&&(!CADPort[i].boolEmergencyShutdown)){CADPort[i].fSetPortActive();}
     else			                                                    {CADPort[i].fSetPortInactive();}
     intCadActivePosition = intCadHeartbeatPosition; 
    
   }//end if (doubleTimeDiff > CADPort[i].intHeartbeatInterval)

  }//end for (int i = 1; i<= intNUM_CAD_PORTS; i++)

}//end Check_CAD_Heartbeat_Interval

/****************************************************************************************************
*
* Name:
*  Function: Check_CAD_Heartbeat_ACK_Timeout()
*
*
* Description:
*   A function called in function void Cad_ScoreBoard_Event() 
*
*
* Details:
*   This function:
*
*   1. Is called when there is an active record (one that has been transmitted awaiting ACK) and checks if a timeout has occured.
*      
*   2. If a timeout has occured:
*      a. If record has not been previously retransmitted	: retransmit record
*      b. If record has been previously retransmitted		: generate error & clear record
*
*   3. if no timeout: exit
*  
*
* Parameters: 				
*   ptr_obj_arrayCadWorkTable	        ExperientDataClass*  pointer to array of ExperientDataClass objects 
*						
*
* Variables:
*   .boolACKReceived                            Global  - <ExperientCommPort> member
*   boolDEBUG                                   Global  - <globals.h> display/log debug info
*   .boolOutsideIPTraffic                       Global  - <ExperientCommPort><ExperientUDPPort> member
*   .boolPortActive                             Global  - <ExperientCommPort> member true if port is active
*   .boolReducedHeartBeatMode                   Global  - <ExperientCommPort> member true if reduced heartbeat mode on port
*   CAD                                         Global  - <header.h> threadorPorttype enumeration member
*   CAD_MESSAGE_408                             Global  - <defines.h> Begin Reduced Heartbeat Interval output message
*   CAD_MESSAGE_412                             Global  - <defines.h> Heartbeat ACK Timeout output message
*   CAD_MESSAGE_461                             Global  - <defines.h> Unexpected Data Received from Outside IP Address output message
*   CLOCK_REALTIME			        Library - <ctime> constant
*   doubleTimeDiff			        Local   - time differential
*   i					        Local   - general purpose integer 
*   intCAD_ACK_THRESHOLD_SEC		        Global  - constant integer of how long after xmit ACK is required
*   intCadActivePosition			        Global  - current transmitted record awaiting ACK
*   intLogCode                                  Local   - log code for log thread
*   intNUM_CAD_PORTS			        Global  - <defines.h> number of installed CAD ports
*   intREDUCED_CAD_HEARTBEAT_INTERVAL_SEC       Global  - <defines.h> 
*   LOG_WARNING                                 Global  - <defines.h> log code to email warning display to console and log to disk
*   objMessage				        Local	- <MessageClass> object
*   .stringOutsideIPAddr                        Global  - <ExperientCommPort><ExperientUDPPort> member
*   .timespecTimeLastHeartbeat                  Global  - <ExperientDataClass> <ctime> time of last heartbeat TX or ACK or NAK on port[]
*   timespecTimeNow			        Local   - <ctime> struct timespec holding current time
*   .timespecTimeXmitStamp                      Global  - <ExperientDataClass> <ctime> structure time of last TX on port[]
*   .UDP_Port                                   Global  -  <ExperientCommPort><ExperientUDPPort> object
*                                                                          
* Functions:
*   ClearPorts()                        Global  <globalfunctions.h>                     (void)  
*   clock_gettime()			Library <ctime>                                 (int)
*   enQueue_Message()		        Global  <globalfunctions.h>                     (void)
*   .fCad_ReXmit()			Global  <ExperientDataClass>                    (bool)
*   .fClear_Record()			Global  <ExperientDataClass>                    (void)
*   .fMessage_Create()			Global  <MessageClass>                          (void)
*   .fPingStatusString()                Global  <ExperientUDPPort>                      (string)
*   .fSetPortInactive()                 Global  <ExperientCommPort>                     (void)
*   .fSetReducedHeartbeatMode()         Global  <ExperientCommPort>                     (void)
*   int2strLZ()			        Global  <globalfunctions.h>                     (string)
*   IsAnyPortActive()                   Global  <globalfunctions.h>                     (bool)
*   seconds2time()                      Global  <globalfunctions.h>                     (string)
*   time_difference()			Global  <globalfunctions.h>                     (long double)
* 
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2007 Experient Corporation 
****************************************************************************************************/
void Check_CAD_Heartbeat_ACK_Timeout(ExperientDataClass* ptr_obj_arrayCadWorkTable) 
{
 long double		doubleTimeDiff;			
 struct timespec	timespecTimeNow;
 MessageClass		objMessage;
 int                    intLogCode      = LOG_WARNING;
 Port_Data              objPortData;


 clock_gettime(CLOCK_REALTIME, &timespecTimeNow);
 for(int i = 1; i <= intNUM_CAD_PORTS; i++)
  {
   doubleTimeDiff = 0;
   if (!CADPort[i].boolHeartBeatACKrequired) {continue;}

   // determine time difference..if port is active
   if (CADPort[i].boolPortActive )
    {doubleTimeDiff = time_difference(timespecTimeNow, CADPort[i].timespecTimeLastHeartbeat);}
  
   // Check to see if there is a timeout condition. If not do nothing....
   if  (doubleTimeDiff > intCAD_ACK_THRESHOLD_SEC)
    {
     objPortData.fClear();
     objPortData.fLoadPortData(CAD,i);
     // time threshold exceeded.... Check if port is not ACKed 
     if (!CADPort[i].boolACKReceived)
      {
       if (CADPort[i].boolReducedHeartBeatMode){intLogCode = LOG_WARNING*boolDISPLAY_AND_LOG_CAD_RAW_TRAFFIC ;}
       if (CADPort[i].UDP_Port.boolOutsideIPTraffic)
        {
         objMessage.fMessage_Create(intLogCode,461, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                    objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA,
                                    CAD_MESSAGE_461, CADPort[i].UDP_Port.stringOutsideIPAddr); 
         enQueue_Message(CAD,objMessage);
         CADPort[i].UDP_Port.boolOutsideIPTraffic = false;
        }
       objMessage.fMessage_Create(intLogCode,412, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objBLANK_CALL_RECORD, objBLANK_TEXT_DATA,
                                  objPortData, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, CAD_MESSAGE_412); 
       enQueue_Message(CAD,objMessage);
//       ptr_obj_arrayCadWorkTable[intCadActivePosition].timespecTimeLastHeartbeat[i] = ptr_obj_arrayCadWorkTable[intCadActivePosition].timespecTimeXmitStamp[i];
       
//       CADPort[i].timespecTimeLastHeartbeat = CADPort[i].timespecTimeXmitStamp;       
       CADPort[i].fSetPortInactive();			
       

       // retransmit ... if previous retransmit: set ACK on port & generate error
       if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(i, CADPort[i].boolHeartBeatACKrequired))
        {
         //  if reduced heartbeat mode suppress error (this code occurs on first 2X timeout and sets reduced interval.  
         //  it is supressed until interval restored to normal)
         if (!CADPort[i].boolReducedHeartBeatMode)
          {
           CADPort[i].fSetReducedHeartbeatMode(ptr_obj_arrayCadWorkTable[intCadActivePosition].fCallData(),timespecTimeNow);
           CADPort[i].fSetPortInactive();         
          }
         
     
           CADPort[i].boolXmitRetry = true;
       
        }// end if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCad_ReXmit(i))

      }// end if (!ptr_obj_arrayCadWorkTable[intCadActivePosition].fCheck_Ack_on_Port(i))

    }// end if (doubleDiffOld > intCAD_ACK_THRESHOLD_SEC)

  }// end for (int i = 1; i <= intNUM_CAD_PORTS; i++)
 
// if (IsAnyPortActive(CADPort, intNUM_CAD_PORTS)){cout << "a port is active" << endl;} 

 if(!IsAnyPortActive(CADPort, intNUM_CAD_PORTS))
  {
   ClearPorts(CADPort, intNUM_CAD_PORTS);
   intCadActivePosition = 0;
  }
 

}// Check_CAD_Heartbeat_ACK_Timeout                  

bool ShutDownCADlegacyPorts(pthread_t CADPortListenThread[], int numberofPorts) {
 
 extern ExperientCommPort                       CADPort[NUM_CAD_PORTS_MAX+1];

 for (int i = 1; i<= intNUM_CAD_PORTS; i++) {
   CADPort[i].TCP_Server_Port.Shutdown();
   CADPort[i].UDP_Port.SetActive(false);
   CADPort[i].TCP_Port.Disconnect();  
   CADPort[i].boolRestartListenThread = true;
 
 }

 for (int i = 1; i<= numberofPorts; i++) { 
  pthread_join( CADPortListenThread[i], NULL);
 }  

 return true;
}

bool InitializeCADlegacyPorts(bool boolSKIPACTIVE = false)
{
 extern int                             intNUM_CAD_PORTS;
 extern ExperientCommPort               CADPort[NUM_CAD_PORTS_MAX+1];
 extern Initialize_Global_Variables     INIT_VARS;
 
 // cout << "InitializeCADlegacyPorts" << endl;
 for(int i = 1; i<= intNUM_CAD_PORTS; i++)  {

    CADPort[i].intRemotePortNumber       = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].intREMOTE_PORT_NUMBER;
    CADPort[i].intLocalPort              = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].intLOCAL_PORT_NUMBER;
    CADPort[i].ePortConnectionType       = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].ePortConnectionType;
    CADPort[i].RemoteIPAddress           = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].REMOTE_IP_ADDRESS;
    CADPort[i].strNotes                  = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].strNotes;
    CADPort[i].fLoad_Vendor_Email_Address(INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].strVendorEmailAddress);
    CADPort[i].boolHeartbeatRequired     = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].boolHeartbeatRequired;
    CADPort[i].boolRecordACKRequired     = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].boolRecordACKRequired;
    CADPort[i].boolHeartBeatACKrequired  = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].boolHeartBeatACKrequired;
    CADPort[i].boolNENA_CADheader        = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].boolNENA_CADheader;
    CADPort[i].boolCAD_EraseFirstALI_CR  = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].boolCAD_EraseFirstALI_CR;
    CADPort[i].PositionAliases           = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].PositionAliases;
    CADPort[i].boolSendCADEraseMsg       = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].boolSendCADEraseMsg;
    CADPort[i].WorkstationGroup          = INIT_VARS.CADinitVariable.CAD_PORT_VARS[i].WorkstationGroup;

    
    
    
    CADPort[i].fInitializeCADport(i, boolSKIPACTIVE); 
 }
 return true;
}

bool StartCADportListenThreads(pthread_t CADPortListenThread[], pthread_attr_t *pthread_attr_tAttr, param_t params[] ) {

 // internal function to CAD thread
 extern int                     intNUM_CAD_PORTS;
 int                            intRC;
 string                         strErrorString;
 
 cout << "NUM CAD PORTS -> " << intNUM_CAD_PORTS << endl;
 if (intNUM_CAD_PORTS > NUM_CAD_PORTS_MAX) {SendCodingError("cad.cpp ->StartCADportListenThreads() -> Number of CAD ports > MAX!"); return false;}
 
  for (int i = 1; i<= intNUM_CAD_PORTS; i++)   {
      
    params[i].intPassedIn = i;
    intRC = pthread_create(&CADPortListenThread[i], pthread_attr_tAttr, *Cad_Port_Listen_Thread, (void*) &params[i]);
    if (intRC) {
     strErrorString = "cad.cpp ->StartCADportListenThreads() -> Unable to start CAD Port "; 
     strErrorString += int2strLZ(i); strErrorString += " Listen Thread!";
     SendCodingError(strErrorString); 
     return false;
    }
  }
return true;
}

