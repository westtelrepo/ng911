/*****************************************************************************
* FILE: trunk_sequence_mapping.cpp
* 
* DESCRIPTION: This file contains all of the functions implemented
*              by the Telephone_Device and Telphone Devices Classes
*
*
* AUTHOR: 11/19/2007 Bob McCarthy
* LAST REVISED: 
*
* COPYRIGHT: 2006-2012 Experient Corporation 
******************************************************************************/

#include "header.h"
#include "defines.h"
#include "baseclasses.h"
#include "globals.h"
#include "globalfunctions.h"

//Globals
extern Port_Data                               	objBLANK_ANI_PORT;
extern Port_Data                               	objBLANK_ALI_PORT;
extern Port_Data                               	objBLANK_WRK_PORT;
extern Port_Data			       	objBLANK_AMI_PORT;
extern Text_Data				objBLANK_TEXT_DATA;
extern Call_Data			       	objBLANK_CALL_RECORD;
extern Freeswitch_Data                         	objBLANK_FREESWITCH_DATA;
extern ALI_Data                                	objBLANK_ALI_DATA;

/////// Trunk_Type #########################################################################################################
//Constructor
Trunk_Type::Trunk_Type()
{
 intTrunkNumber       = 0;
 TrunkType            = CAMA;
 boolTrunkCallOK      = false;
 boolWarningSent      = false;
 boolTrunkNumberInUse = false;
 RotateGroup          = 0;
 time(&time_tLastCall);
}

//copy constructor
Trunk_Type::Trunk_Type(const Trunk_Type *a)
{
 intTrunkNumber       	= a->intTrunkNumber;
 TrunkType            	= a->TrunkType;
 boolTrunkCallOK      	= a->boolTrunkCallOK;
 boolWarningSent      	= a->boolWarningSent;
 boolTrunkNumberInUse 	= a->boolTrunkNumberInUse;
 RotateGroup          	= a->RotateGroup;
 time_tLastCall		= a->time_tLastCall;
}
//assignment operator
Trunk_Type& Trunk_Type::operator=(const Trunk_Type& a)
{
 if (&a == this ) {return *this;}
 intTrunkNumber       	= a.intTrunkNumber;
 TrunkType            	= a.TrunkType;
 boolTrunkCallOK      	= a.boolTrunkCallOK;
 boolWarningSent      	= a.boolWarningSent;
 boolTrunkNumberInUse 	= a.boolTrunkNumberInUse;
 RotateGroup          	= a.RotateGroup;
 time_tLastCall		= a.time_tLastCall;

 return *this;
}
//Comparison
bool Trunk_Type::fCompare(const Trunk_Type& a) const
{
 bool boolRunningCheck = true;

 //We just need to see if the types numbers and rotate group are the same, not times or booleans ....

 boolRunningCheck = boolRunningCheck && (this->intTrunkNumber		== a.intTrunkNumber);
 boolRunningCheck = boolRunningCheck && (this->TrunkType 		== a.TrunkType);
 boolRunningCheck = boolRunningCheck && (this->RotateGroup 		== a.RotateGroup);

 return boolRunningCheck;
}

string Trunk_Type::fTrunkType() {
extern string TrunkType(trunk_type  eTrunkType);

return TrunkType(this->TrunkType);
}


/////// Trunk_Type_Mapping #########################################################################################################

// Constructor
Trunk_Type_Mapping::Trunk_Type_Mapping()
 {
  for( int i = 0; i < intMAX_NUM_TRUNKS; i++)
   {
    Trunk[i].intTrunkNumber       = i;
    Trunk[i].TrunkType            = CAMA;
    Trunk[i].boolTrunkCallOK      = false;
    Trunk[i].boolWarningSent      = false;
    Trunk[i].boolTrunkNumberInUse = false;
    Trunk[i].RotateGroup          = 0;
    time(&Trunk[i].time_tLastCall);    
   }
 }
//Destructor
Trunk_Type_Mapping::~Trunk_Type_Mapping(){}
//copy constructor
Trunk_Type_Mapping::Trunk_Type_Mapping(const Trunk_Type_Mapping *a)
{
  for( int i = 0; i < intMAX_NUM_TRUNKS; i++)
   {
    Trunk[i].intTrunkNumber       = a->Trunk[i].intTrunkNumber;
    Trunk[i].TrunkType            = a->Trunk[i].TrunkType;
    Trunk[i].boolTrunkCallOK      = a->Trunk[i].boolTrunkCallOK;
    Trunk[i].boolWarningSent      = a->Trunk[i].boolWarningSent;
    Trunk[i].boolTrunkNumberInUse = a->Trunk[i].boolTrunkNumberInUse;
    Trunk[i].RotateGroup          = a->Trunk[i].RotateGroup;
    Trunk[i].time_tLastCall       = a->Trunk[i].time_tLastCall;    
   }
}
//assignment operator
Trunk_Type_Mapping& Trunk_Type_Mapping::operator=(const Trunk_Type_Mapping& a)
{
 if (&a == this ) {return *this;}

 for( int i = 0; i < intMAX_NUM_TRUNKS; i++){
    Trunk[i].intTrunkNumber       = a.Trunk[i].intTrunkNumber;
    Trunk[i].TrunkType            = a.Trunk[i].TrunkType;
    Trunk[i].boolTrunkCallOK      = a.Trunk[i].boolTrunkCallOK;
    Trunk[i].boolWarningSent      = a.Trunk[i].boolWarningSent;
    Trunk[i].boolTrunkNumberInUse = a.Trunk[i].boolTrunkNumberInUse;
    Trunk[i].RotateGroup          = a.Trunk[i].RotateGroup;
    Trunk[i].time_tLastCall       = a.Trunk[i].time_tLastCall;    
 }

 return *this;
}
//Comparison
bool Trunk_Type_Mapping::fCompare(const Trunk_Type_Mapping& a) const
{
 bool boolRunningCheck = true;

 for( int i = 0; i < intMAX_NUM_TRUNKS; i++){
  boolRunningCheck = boolRunningCheck && (this->Trunk[i] == a.Trunk[i]);  
 }

 return boolRunningCheck;
}
/////// Trunk_Rotate_Group #########################################################################################################

//constructor
Trunk_Rotate_Group::Trunk_Rotate_Group()
{
 RotateRule      = NO_RULE;
 RotateRuleValue = 0;
 GroupCount      = 0;
 TrunkMember.clear();
}
//destructor
Trunk_Rotate_Group::~Trunk_Rotate_Group(){}
//copy constructor
Trunk_Rotate_Group::Trunk_Rotate_Group(const Trunk_Rotate_Group *a)
{
  RotateRule		= a->RotateRule;
  RotateRuleValue	= a->RotateRuleValue;
  GroupCount 		= a->GroupCount;
  TrunkMember		= a->TrunkMember;
}
//assignment operator
Trunk_Rotate_Group& Trunk_Rotate_Group::operator=(const Trunk_Rotate_Group& a)
{
 if (&a == this ) {return *this;}

  RotateRule		= a.RotateRule;
  RotateRuleValue	= a.RotateRuleValue;
  GroupCount 		= a.GroupCount;
  TrunkMember		= a.TrunkMember;

 return *this;
}
//compare
bool Trunk_Rotate_Group::fCompare(const Trunk_Rotate_Group& a) const
{
 bool boolRunningCheck = true;

 boolRunningCheck = boolRunningCheck && (this->RotateRule == a.RotateRule);  
 boolRunningCheck = boolRunningCheck && (this->RotateRuleValue == a.RotateRuleValue);  
 boolRunningCheck = boolRunningCheck && (this->GroupCount == a.GroupCount);  
 boolRunningCheck = boolRunningCheck && (this->TrunkMember == a.TrunkMember);  
 return boolRunningCheck;
}


/////// Trunk_Sequencing #########################################################################################################

//constructor
Trunk_Sequencing::Trunk_Sequencing()
{
//TrunkMap;
intCallCount 		= 0;
intNumber911trunks 	= 0;
sem_init(&sem_tMutexTrunkSequence,0,1);
TrunkRotateGroup.clear();
} 

//copy constructor
Trunk_Sequencing::Trunk_Sequencing(const Trunk_Sequencing *a)
{
  TrunkMap 		= a->TrunkMap;
  intCallCount		= a->intCallCount;
  intNumber911trunks	= a->intNumber911trunks;
  // sem_tMutexTrunkSequence		= a->sem_tMutexTrunkSequence;
  TrunkRotateGroup      = a->TrunkRotateGroup;
}
//assignment operator
Trunk_Sequencing& Trunk_Sequencing::operator=(const Trunk_Sequencing& a)
{
 if (&a == this ) {return *this;}

  TrunkMap			= a.TrunkMap;
  intCallCount			= a.intCallCount;
  intNumber911trunks 		= a.intNumber911trunks;
 // sem_tMutexTrunkSequence	= a.sem_tMutexTrunkSequence;

  TrunkRotateGroup              = a.TrunkRotateGroup;
 return *this;
}
//compare
bool Trunk_Sequencing::fCompare(const Trunk_Sequencing& a) const
{
 bool boolRunningCheck = true;

 boolRunningCheck = boolRunningCheck && (this->TrunkMap == a.TrunkMap);  
 boolRunningCheck = boolRunningCheck && (this->intCallCount == a.intCallCount);  
 boolRunningCheck = boolRunningCheck && (this->intNumber911trunks == a.intNumber911trunks); 
 boolRunningCheck = boolRunningCheck && (this->TrunkRotateGroup == a.TrunkRotateGroup);  
// boolRunningCheck = boolRunningCheck && (this->sem_tMutexTrunkSequence == a.sem_tMutexTrunkSequence);  
 return boolRunningCheck;
}

int Trunk_Sequencing::fNumberOfMonitoredTrunks() {
 int iNum = 0;

 for (int i = 0; i <= intMAX_NUM_TRUNKS; i++)  {
   if( (TrunkMap.Trunk[i].boolTrunkNumberInUse)&&(TrunkMap.Trunk[i].RotateGroup != NO_RULE) ) {iNum++;}
 }
 return iNum;
}

int Trunk_Sequencing::fNumberOfAlarmingTrunks() {
 
 int iNum = 0;
 
 for (int i = 0; i <= intMAX_NUM_TRUNKS; i++)  {
   if( (TrunkMap.Trunk[i].boolTrunkNumberInUse)&&(TrunkMap.Trunk[i].RotateGroup != NO_RULE) ){ 
    if (TrunkMap.Trunk[i].boolWarningSent) {
     iNum++;
    }
   } 
 }
 return iNum;
}



void Trunk_Rotate_Group::fClear()
{
 RotateRule      = NO_RULE;
 RotateRuleValue = 0;
 GroupCount      = 0;
 TrunkMember.clear();
}

void Trunk_Rotate_Group::fDisplay(bool boolShowLabels)
{
 size_t sz = TrunkMember.size();
 string strComma = ", ";

 if (boolShowLabels)
  {
   cout << endl;
   cout << "      Trunk Rotate Rule" << endl;
   cout << setw(15) << left << "Rule";
   cout << setw(15) << left << "Value";
   cout << setw(25) << left << "Members" << endl;
  } 
    
 cout << setw(15) << left << fDisplayRule();
 cout << setw(15) << left << RotateRuleValue;  
 

 for (unsigned int i = 0; i < sz; i++)
  {
   if (i == sz -1) {strComma.clear();}
   cout << TrunkMember[i] << strComma;
  }
 cout << endl;

}

string Trunk_Rotate_Group::fDisplayRule()
{
 switch(this->RotateRule)
  {
   case MULTIPLIER: return "Multiplier";
   case GROUPCOUNT: return "GroupCount";
   case DAYS:       return "Days";
   case NO_RULE:    return "NoRule";
  }

 return "Error";
}


bool Trunk_Rotate_Group::fLoadRule(string strInput)
{
 if      (strInput == ANI_TRUNK_ROTATE_MULTIPLIER) {RotateRule = MULTIPLIER;}
 else if (strInput == ANI_TRUNK_ROTATE_GROUPCOUNT) {RotateRule = GROUPCOUNT;}
 else if (strInput == ANI_TRUNK_ROTATE_DAYS)       {RotateRule = DAYS;}
 else if (strInput == ANI_TRUNK_ROTATE_NO_RULE)    {RotateRule = NO_RULE;}
 else                                              {return false;}
 return true;
}

bool Trunk_Rotate_Group::fLoadRuleValue(string strInput)
{
 if(!Validate_Integer(strInput))       {return false;}    

 RotateRuleValue = char2int(strInput.c_str());

 return true;
}


bool Trunk_Rotate_Group::fLoadMembers(string stringArg)
{
 size_t         position = 0;
 string         stringTemp;
 string         stringNumber;
 int            intTrunk;

 stringTemp = stringArg;

 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   stringNumber.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   stringNumber = RemoveLeadingSpaces(stringNumber);
   stringNumber = RemoveTrailingSpaces(stringNumber);
   if(!Validate_Integer(stringNumber))       {return false;}   
   intTrunk = char2int(stringNumber.c_str());
   if (intTrunk > intMAX_NUM_TRUNKS)         {return false;}

   TrunkMember.push_back(intTrunk); 

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 stringNumber.assign(stringTemp,0 ,string::npos);
 stringNumber = RemoveLeadingSpaces(stringNumber);
 stringNumber = RemoveTrailingSpaces(stringNumber);
 if(!Validate_Integer(stringNumber))       {return false;}   
 intTrunk = char2int(stringNumber.c_str());
 if (intTrunk > intMAX_NUM_TRUNKS)         {return false;}

 TrunkMember.push_back(intTrunk); 
 return true;
}





/****************************************************************************************************
*
* Name:
*  Function: int Trunk_Type_Mapping::fLoadTrunkType( trunk_type eTrunkType, string stringArg)
*
*
* Description:
*  Trunk_Type_Mapping initializtion Function
*
*
* Details:
*   this function reads a string of numbers (1,2, .. intMAX_NUM_TRUNKS) and assigns member 
*   array TrunkType[X] = eTrunkType where X is each of the numbers read.
*
*
* Parameters: 				                             
*   stringArg                           <cstring>
*
* Variables:
*   CFG_MESSAGE_050			Global  <defines.h> error message
*   CLID				Global  <header.h> <trunk_type> enumeration member
*   EX_CONFIG				Library <sysexits.h> exit code
*   intMAX_NUM_TRUNKS			Global  <globals.h>
*   intTrunk                            Local   int trunk number read in
*   MAIN                                Global  <header.h> <threadorporttype> enumeration member
*   npos                                Library <cstring> constant
*   position                            Local   <cstring><size_t>
*   stringNumber                        Local   <cstring> trunk number read in 
*   stringTemp                          Local   <cstring> temp string
*   TrunkType[]				Local   <ANITrunkMapping> <trunk_type> member array
*                                                                      
* Functions:
*   .assign()				Library <cstring>		(void)
*   char2int()				Global  <globalfunctions.h>     (unsigned long long int)
*   .c_str()				Library <cstring>		(const char*)
*   .erase()				Library <cstring>		(void)
*   .find_first_of()			Library <cstring>		(size_t)
*   .find_last_of()			Library <cstring>		(size_t)
*
* Author(s):
*   Bob McCarthy 			Original: 5/29/2008
*                                       Updated : N/A
*
****************************************************************************************************/ 
bool Trunk_Type_Mapping::fLoadTrunkMapType( trunk_type eTrunkType, string stringArg)
{
 size_t         position = 0;
 string         stringTemp;
 string         stringNumber;
 int            intTrunk;

 stringTemp = stringArg;
 do 
  {
   position = stringTemp.find_first_of( ",", position);
   if (position == string::npos){continue;}
   stringNumber.assign(stringTemp,0,position);
   stringTemp.erase(0, position+1);
   stringNumber = RemoveLeadingSpaces(stringNumber);
   stringNumber = RemoveTrailingSpaces(stringNumber);
   if(!Validate_Integer(stringNumber))       {return false;}   
   intTrunk = char2int(stringNumber.c_str());
   if (intTrunk > intMAX_NUM_TRUNKS)         {return false;}

   Trunk[intTrunk].TrunkType           = eTrunkType;
   Trunk[intTrunk].boolTrunkNumberInUse = true;

  } while (position != string::npos);

 //at this point there will be a number left no comma .....

 stringNumber.assign(stringTemp,0 ,string::npos);
 stringNumber = RemoveLeadingSpaces(stringNumber);
 stringNumber = RemoveTrailingSpaces(stringNumber);
 if(!Validate_Integer(stringNumber))       {return false;}   
 intTrunk = char2int(stringNumber.c_str());
 if (intTrunk > intMAX_NUM_TRUNKS)         {return false;}

 Trunk[intTrunk].TrunkType            = eTrunkType;
 Trunk[intTrunk].boolTrunkNumberInUse = true;
 return true;
}

bool Trunk_Type_Mapping::fIsINDIGITAL(int itrunk)
{
 if (itrunk >intMAX_NUM_TRUNKS) {return false;}

 switch (Trunk[itrunk].TrunkType)
  {
   case INDIGITAL_WIRELESS: return true;  
   default:                 return false;
  }
}

bool Trunk_Type_Mapping::fIsINTRADO(int itrunk)
{
 if (itrunk >intMAX_NUM_TRUNKS) {return false;}

 switch (Trunk[itrunk].TrunkType)
  {
   case INTRADO: return true;  
   default:      return false;
  }
}

bool Trunk_Type_Mapping::fIsREFER(int itrunk)
{
 if (itrunk >intMAX_NUM_TRUNKS) {return false;}

 switch (Trunk[itrunk].TrunkType)
  {
   case REFER: return true;  
   default:      return false;
  }
}

bool Trunk_Type_Mapping::fIsTandem(int itrunk)
{
 if (itrunk >intMAX_NUM_TRUNKS) {return false;}

 switch (Trunk[itrunk].TrunkType)
  {
   case CLID: case SIP_TRUNK: return false;  
   default:   return true;
  }
}

bool Trunk_Type_Mapping::fIsCLID(int itrunk)
{
 if (itrunk >intMAX_NUM_TRUNKS) {return false;}

 switch (Trunk[itrunk].TrunkType)
  {
   case CLID: return true;  
   default:   return false;
  }
}

bool Trunk_Type_Mapping::fIsMSRP(int itrunk)
{
 if (itrunk >intMAX_NUM_TRUNKS) {return false;}

 switch (Trunk[itrunk].TrunkType)
  {
   case MSRP_TRUNK: return true;  
   default:   return false;
  }
}

void Trunk_Sequencing::fSetNumber911trunks()
{
 intNumber911trunks = 0;
 for (int i = 0; i <= intMAX_NUM_TRUNKS; i++)
  {
   if( (TrunkMap.Trunk[i].boolTrunkNumberInUse)&&(TrunkMap.fIsTandem(i)) ) {intNumber911trunks++;}
  }
}


void  Trunk_Sequencing::fLoadTrunkMapRotateGroups()
{
 size_t sz_Group = TrunkRotateGroup.size();
 size_t sz_Members;

 //skip group 0
 for (unsigned int i = 1; i< sz_Group; i++)
  {
   sz_Members = TrunkRotateGroup[i].TrunkMember.size();

   for (unsigned int j = 0; j < sz_Members; j++)
    {
     if (TrunkMap.Trunk[TrunkRotateGroup[i].TrunkMember[j]].RotateGroup) {Abnormal_Exit(MAIN, EX_CONFIG ,CFG_MESSAGE_070, int2strLZ(TrunkRotateGroup[i].TrunkMember[j]) );}
     TrunkMap.Trunk[TrunkRotateGroup[i].TrunkMember[j]].RotateGroup = i;
    }
  }



}

void Trunk_Sequencing::fCheckTrunkUsagebyDays()
{
 time_t timenow;
 long long int iTimediff;
 size_t sz_Group = TrunkRotateGroup.size();
 size_t sz_Members;

  //skip group 0
 for (unsigned int i = 1; i< sz_Group; i++)
  {
   if (TrunkRotateGroup[i].RotateRule != DAYS) {continue;}
   sz_Members = TrunkRotateGroup[i].TrunkMember.size();

   time(&timenow);
   for (unsigned int j = 0; j < sz_Members; j++)
    {
     iTimediff = timenow - TrunkMap.Trunk[TrunkRotateGroup[i].TrunkMember[j]].time_tLastCall;
     if (iTimediff > (TrunkRotateGroup[i].RotateRuleValue * SECONDS_PER_DAY))
      {
       TrunkMap.Trunk[TrunkRotateGroup[i].TrunkMember[j]].boolTrunkCallOK = false;      
      }
     else {TrunkMap.Trunk[TrunkRotateGroup[i].TrunkMember[j]].boolTrunkCallOK = true;}
    }
   fReportDeadTrunks(i);
  }

}

void Trunk_Sequencing::fDisplayLastCallTime() {
 struct tm                      *timeinfo;
 struct tm			timedata;
 char                           buffer [80];
 string                         strRule;
 long long int                  iTimeDifference;
 extern struct timespec         timespecControllerStartTime;
 unsigned int                   irule;
 cout << endl;
  cout << "Number of Monitored Trunks: " << fNumberOfMonitoredTrunks() << endl;
  cout << "number of Alarming Trunks:  " << fNumberOfAlarmingTrunks() << endl;
  cout << "TRUNK  TYPE      LAST_CALL                 MONITOR  GROUP  RULE " << endl;
  cout << "----------------------------------------------------------------" << endl;
 for (int i = 0; i<= intNUM_TRUNKS_INSTALLED; i++)  {
    if (!TrunkMap.Trunk[i].boolTrunkNumberInUse)                   {continue;}
    timeinfo = localtime_r (&TrunkMap.Trunk[i].time_tLastCall, &timedata);

    strftime (buffer,80,"%F %T %Z",timeinfo);

    cout << setw(7) << left << i;
    switch (TrunkMap.Trunk[i].TrunkType)     {
      case CAMA:         	cout << setw(10) << left << "CAMA"; break;
      case CLID:         	cout << setw(10) << left << "CLID"; break;
      case LANDLINE:     	cout << setw(10) << left << "LANDLINE"; break;
      case WIRELESS:     	cout << setw(10) << left << "WIRELESS"; break;
      case SIP_TRUNK:    	cout << setw(10) << left << "SIP"; break;
      case NG911_SIP:      	cout << setw(10) << left << "NG911-SIP"; break;
      case REFER:         	cout << setw(10) << left << "REFER"; break;
      case INDIGITAL_WIRELESS:	cout << setw(10) << left << "IDGTWRLS"; break;
      case MSRP_TRUNK:          cout << setw(10) << left << "MSRP"; break;
      case INTRADO:             cout << setw(10) << left << "INTRADO"; break;
      default:
       cout << setw(10) << left << "Unknown"; break;
    }

    iTimeDifference = TrunkMap.Trunk[i].time_tLastCall - timespecControllerStartTime.tv_sec;

    if (iTimeDifference > 1) { cout << setw(26) << left << buffer ;}
    else                     { cout << setw(26) << left << "YYYY-MM-DD 00:00:00 TZD";} 

    if (TrunkMap.Trunk[i].RotateGroup) {    cout << setw(9) << left << "Yes" << setw(7) << TrunkMap.Trunk[i].RotateGroup ;}
    else                               {    cout << setw(9) << left << "No "<< setw(7) << "0";}

    if      (TrunkRotateGroup.empty())  										{irule = 0;}
    else if ((unsigned int)TrunkMap.Trunk[i].RotateGroup > TrunkRotateGroup.size())					{irule = 0;}
    else                           											{irule = TrunkRotateGroup[TrunkMap.Trunk[i].RotateGroup].RotateRule;}
    switch (irule)
     {
      case DAYS:
        strRule =  "Days: " + int2str( TrunkRotateGroup[TrunkMap.Trunk[i].RotateGroup].RotateRuleValue);
        break;
      case GROUPCOUNT:
         strRule = "Count: " + int2str(TrunkRotateGroup[TrunkMap.Trunk[i].RotateGroup].RotateRuleValue);
        break;
      case MULTIPLIER:
         strRule = "Mult: " + int2str(TrunkRotateGroup[TrunkMap.Trunk[i].RotateGroup].RotateRuleValue);
        break;
      default: 
         strRule = "None";    
        break;
     }
    cout  << setw(11) << left << strRule << endl;;
 }
 cout << "  " << endl;
}


void Trunk_Sequencing::fAddCall(unsigned int intTrunk)
{
 int          GroupNumber;
 MessageClass objMessage;
 Call_Data    objCallData;

 if (intTrunk > intMAX_NUM_TRUNKS)                                     {return;}
 if (!TrunkMap.Trunk[intTrunk].boolTrunkNumberInUse)                   {return;}
 if (TrunkMap.Trunk[intTrunk].RotateGroup > (int)TrunkRotateGroup.size()-1) {return;}

 sem_wait(&sem_tMutexTrunkSequence);
 time (&TrunkMap.Trunk[intTrunk].time_tLastCall);
 GroupNumber = TrunkMap.Trunk[intTrunk].RotateGroup;
 if (!GroupNumber)                                                     { sem_post(&sem_tMutexTrunkSequence);return;} // Group zero is not monitored ......
 // Group 0


 TrunkRotateGroup[GroupNumber].GroupCount++;
 TrunkMap.Trunk[intTrunk].boolTrunkCallOK = true;
 if (TrunkMap.Trunk[intTrunk].boolWarningSent)
  {
   objCallData.fLoadTrunk(intTrunk);
   objMessage.fMessage_Create(LOG_ALARM, 394, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                              objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_394, int2strLZ(intTrunk));
   enQueue_Message(ANI,objMessage);
   TrunkMap.Trunk[intTrunk].boolWarningSent = false;
  }


 switch (TrunkRotateGroup[GroupNumber].RotateRule)
  {
   case DAYS:
        break;
   case GROUPCOUNT:
        if (TrunkRotateGroup[GroupNumber].GroupCount > TrunkRotateGroup[GroupNumber].RotateRuleValue) {fCheckSequence(GroupNumber);}
        break;
   case MULTIPLIER:
        if (TrunkRotateGroup[GroupNumber].GroupCount > TrunkRotateGroup[GroupNumber].RotateRuleValue*(int)TrunkRotateGroup[GroupNumber].TrunkMember.size()) {fCheckSequence(GroupNumber);}
        break;
   default:
        SendCodingError("trunk_sequence_mapping.cpp - no Rule in Trunk Group in fn Trunk_Sequencing::fAddCall() GroupNumber = " + int2str(GroupNumber));
  } 

 sem_post(&sem_tMutexTrunkSequence);
}

void Trunk_Sequencing::fResetSequence(unsigned int iGroup)
{
 size_t sz_Groups        = TrunkRotateGroup.size();
 size_t sz_Group;
 int    iTrunk;

 if (iGroup > sz_Groups-1) {return;}
 
 sz_Group = TrunkRotateGroup[iGroup].TrunkMember.size();

 for (unsigned int i = 0; i < sz_Group; i++)
  {
   iTrunk = TrunkRotateGroup[iGroup].TrunkMember[i];
   if (iTrunk > intMAX_NUM_TRUNKS)                    {return;}
   TrunkMap.Trunk[iTrunk].boolTrunkCallOK = false;
   TrunkRotateGroup[iGroup].GroupCount    = 0;
   time (&TrunkMap.Trunk[iTrunk].time_tLastCall);   
  }
 return;
}


bool Trunk_Sequencing::fCheckSequence(unsigned int iGroup)
{
 bool   boolRunningCheck = true;
 size_t sz_Groups        = TrunkRotateGroup.size();
 size_t sz_Group;
 int    iTrunk;

 if (iGroup > sz_Groups-1) {return false;}
 
 sz_Group = TrunkRotateGroup[iGroup].TrunkMember.size();

 for (unsigned int i = 0; i < sz_Group; i++)
  {
   iTrunk = TrunkRotateGroup[iGroup].TrunkMember[i];
   if (iTrunk > intMAX_NUM_TRUNKS)                                                 {return false;}
   boolRunningCheck = (boolRunningCheck && TrunkMap.Trunk[iTrunk].boolTrunkCallOK);
  }
 if (!boolRunningCheck) { fReportDeadTrunks(iGroup);}
 fResetSequence(iGroup);
 return boolRunningCheck;
}



void Trunk_Sequencing::fReportDeadTrunks(unsigned int iGroup)
{
 MessageClass   objMessage;
 Call_Data      objCallData;
 size_t         sz_Group;
 int            iTrunk;
 string         strMessage;
 size_t         sz_Groups        = TrunkRotateGroup.size();
 time_t         timenow;
 long long int  iTimediff;
 long long int  iDays;
 
 if (iGroup > sz_Groups-1) {return;}
 
 sz_Group = TrunkRotateGroup[iGroup].TrunkMember.size();

 for (unsigned int i = 0; i < sz_Group; i++)
  {
   iTrunk = TrunkRotateGroup[iGroup].TrunkMember[i];
   if (iTrunk > intMAX_NUM_TRUNKS)                   {return;}
   if (!TrunkMap.Trunk[iTrunk].boolTrunkCallOK)
    {
     objCallData.fLoadTrunk(iTrunk);
     switch (TrunkRotateGroup[iGroup].RotateRule)
      {
       case DAYS:
            time(&timenow);
            iTimediff = timenow - TrunkMap.Trunk[iTrunk].time_tLastCall;
            iDays = iTimediff/SECONDS_PER_DAY;
            strMessage = int2str(iDays) + " Days"; break;
       default:
            strMessage = int2str(TrunkRotateGroup[iGroup].GroupCount) + " Calls"; break;              
      }
 
      objMessage.fMessage_Create(LOG_ALARM, 377, __LINE__ , __FILE__,  __PRETTY_FUNCTION__ , objCallData, objBLANK_TEXT_DATA,
                                 objBLANK_ANI_PORT, objBLANK_FREESWITCH_DATA, objBLANK_ALI_DATA, ANI_MESSAGE_377, int2strLZ(iTrunk), strMessage);
      enQueue_Message(ANI,objMessage);
      TrunkMap.Trunk[iTrunk].boolWarningSent = true;

    }

  }
}




