/*****************************************************************************
* FILE: adr.h
*  
*
* DESCRIPTION:
*  Contains the base definitions of all classes used by ADR. 
* RFC 6351 X-Card v4.0 
* RFC 7852 Additional Data Related to an Emergency Call July 2016
* Intrado ADR-AdditionalData Interface Specification v 1.3.1 Nov 13 2019
*
* AUTHOR: 1/02/2021 Bob McCarthy
* LAST REVISED: 
* 1/02/2021 
******************************************************************************/
#include "header.h"
#include "globals.h"
#include "baseclasses.h"
#include "/datadisk1/src/simpleini/SimpleIni.h"          // ini software
#include "/datadisk1/src/xmlParser/xmlParser.h"

#ifndef adr_classses_included_
#define adr_classses_included_


using namespace std;

class DataPacketIn;

/********************************* Values ********************************************/
class vc_value_text {
 public:
 string		strText;
 void fClear();
};

class vc_value_text_list {
 public:
 string			strText;
 vector <string> 	vTextList;
 void fClear();
};

class vc_value_timestamp {
// xsd:string { pattern = "\d{8}T\d{6}(Z|[+\-]\d\d(\d\d)?)?" }
 public:
 string		strTimeStamp;
 void fClear();
};

class vc_value_time {
/*
  xsd:string { pattern = "(\d\d(\d\d(\d\d)?)?|-\d\d(\d\d?)|--\d\d)"
                         ~ "(Z|[+\-]\d\d(\d\d)?)?" }
  }
*/
 public:
 string		strTime;
 void fClear();
};

class vc_value_date_time {
/*
  xsd:string { pattern = "(\d{8}|--\d{4}|---\d\d)T\d\d(\d\d(\d\d)?)?"
                         ~ "(Z|[+\-]\d\d(\d\d)?)?" }
  }
*/
 public:
 string		strDateTime;
 void fClear();
};

class vc_value_date {
/*
   xsd:string { pattern = "\d{8}|\d{4}-\d\d|--\d\d(\d\d)?|---\d\d" }
  }
*/
 public:
 string		strDate;
 void fClear();
};

class vc_value_utc_offset {
/*
   value-utc-offset = element utc-offset {
    xsd:string { pattern = "[+\-]\d\d(\d\d)?" }
  }
*/
 public:
 string		strUTCoffset;
 void fClear();
};

class vc_value_language_tag {
/*
 xsd:string { pattern = "([a-z]{2,3}((-[a-z]{3}){0,3})?|[a-z]{4,8})"
                         ~ "(-[a-z]{4})?(-([a-z]{2}|\d{3}))?"
                         ~ "(-([0-9a-z]{5,8}|\d[0-9a-z]{3}))*"
                         ~ "(-[0-9a-wyz](-[0-9a-z]{2,8})+)*"
                         ~ "(-x(-[0-9a-z]{1,8})+)?|x(-[0-9a-z]{1,8})+|"
                         ~ "[a-z]{1,3}(-[0-9a-z]{2,8}){1,2}" }
  }
*/
 public:
 string		strLangTag;

 bool fLoadLanguageTag(XMLNode LanguageNode);
 void fClear();
};

class vc_value_uri {
// value-uri = element uri { xsd:anyURI }
 public:
 string strURI;
 void fClear();
};


/********************************* Parameters ********************************************/

class vc_media_type {
//param-mediatype = element mediatype { value-text }?
 public:
 vc_value_text media;

 bool fLoadMediaType(XMLNode MediaTypeNode);
 void fClear();
};

class vc_pref {
/*
param-pref = element pref {
    element integer {
      xsd:integer { minInclusive = "1" maxInclusive = "100" }
    }
  }?
*/
 public:
 int 	iPref;

 bool fLoadPref(XMLNode PrefNode);
 void fClear();
};

class vc_altid {
//param-altid = element altid { value-text }?
 public:
 vc_value_text id;

 bool fLoadAltID(XMLNode AltIDnode);
 void fClear();
};

class vc_pid {
/*
param-pid = element pid {
    element text { xsd:string { pattern = "\d+(\.\d+)?" } }+
  }?
*/
 public:
 string		strPID;

 bool fLoadPID(XMLNode PIDnode);
 void fClear();
};

class vc_type {
//param-type = element type { element text { "work" | "home" }+ }?
 public:
 vc_value_text_list	TypeList;
	
 bool fLoadTypeList(XMLNode TypeNode);
 void fClear();
};

class vc_param_lang {
//param-language = element language { value-language-tag }?
/*
value-language-tag = element language-tag {
    xsd:string { pattern = "([a-z]{2,3}((-[a-z]{3}){0,3})?|[a-z]{4,8})"
                         ~ "(-[a-z]{4})?(-([a-z]{2}|\d{3}))?"
                         ~ "(-([0-9a-z]{5,8}|\d[0-9a-z]{3}))*"
                         ~ "(-[0-9a-wyz](-[0-9a-z]{2,8})+)*"
                         ~ "(-x(-[0-9a-z]{1,8})+)?|x(-[0-9a-z]{1,8})+|"
                         ~ "[a-z]{1,3}(-[0-9a-z]{2,8}){1,2}" }
  }
*/
 public:
 vc_value_language_tag		LanguageTag;

 bool fLoadLanguageTag(XMLNode LanguageNode);
 void fClear();
};

class vc_calscale {
//param-calscale = element calscale { element text { "gregorian" } }?
 public:
 vc_value_text		Text;

 bool fLoadCalScale(XMLNode CalscaleNode);
 void fClear();
};

class vc_sort_as {
//param-sort-as = element sort-as { value-text+ }?
 public:
 vc_value_text		Text;

 bool fLoadSortAs(XMLNode SortNode);
 void fClear();
};

class vc_param_geo {
//param-geo = element geo { value-uri }?
 public:
 vc_value_uri		URI;

 bool fLoadGeo(XMLNode GeoNode);
 void fClear();
};

class vc_param_tz {
//param-tz = element tz { value-text | value-uri }?
 public:
 vc_value_text		Text;
 vc_value_uri		URI;

 bool fLoadTZ(XMLNode TZnode);
 void fClear();
};

class vc_label {
//param-label = element label { value-text }?
 public:
 vc_value_text		Text;

 bool fLoadLabel(XMLNode LabelNode);
 void fClear();
};

class vc_parameters {
 public:
 vc_media_type 	Mediatype;
 vc_altid	AltID;
 vc_pid		PID;
 vc_pref	Pref;
 vc_type	Type;
 vc_param_lang	Language;
 vc_calscale	Calscale;
 vc_sort_as	SortAs;
 vc_param_geo	Geo;
 vc_param_tz	TZ;
 vc_label	Label;

bool fLoadParameters(XMLNode ParameterNode);
void fClear();
};

/**************************************************************************************/


class vc_iana_token {
// xsd:string { pattern = "[a-zA-Z0-9-]+" }
 public:
 string		strIanaToken;

};



class vc_x_name {
// xsd:string { pattern = "x-[a-zA-Z0-9-]+" }
 public:
 string		strXname;

};

/********************************** Properties ****************************************************/
/* 
# Top-level grammar
property = property-adr | property-anniversary | property-bday
         | property-caladruri | property-caluri | property-categories
         | property-clientpidmap | property-email | property-fburl
         | property-fn | property-geo | property-impp | property-key
         | property-kind | property-lang | property-logo
         | property-member | property-n | property-nickname
         | property-note | property-org | property-photo
         | property-prodid | property-related | property-rev
         | property-role | property-gender | property-sound
         | property-source | property-tel | property-title
         | property-tz | property-uid | property-url
start = element vcards {
    element vcard {
      (property
       | element group {
           attribute name { text },
           property*
         })+
    }+
  }

*/

class vc_source { 
/*
# 6.1.3
property-source = element source {
    element parameters { param-altid, param-pid, param-pref,
                         param-mediatype },
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

};

class vc_kind {
/*
# 6.1.4
property-kind = element kind {
    element text { "individual" | "group" | "org" | "location" |
                   x-name | iana-token }*
  }
*/
 public:
 vc_value_text	Text;

 bool fLoadKind(XMLNode KindNode);
 void fClear();
};

class vc_full_name {
 
/*
# 6.2.1
property-fn = element fn {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type }?,
    value-text
  }
*/
 public:
 vc_parameters 		Parameters; 
 vc_value_text		fn;

bool fLoadFullName(XMLNode NameNode);
void fClear();
};

class vc_name {
/*
# 6.2.2
property-n = element n {
    element parameters { param-language, param-sort-as, param-altid }?,
    element surname { text }+,
    element given { text }+,
    element additional { text }+,
    element prefix { text }+,
    element suffix { text }+
  }
*/
 public:
 vc_parameters 		Parameters; 

 vector <string> 	vSurname;
 vector <string> 	vGiven;
 vector <string> 	vAdditional;
 vector <string> 	vPrefix;
 vector <string> 	vSuffix;

 bool fLoadName(XMLNode NameNode);
 void fClear();
};

class vc_nickname {
/*
# 6.2.3
property-nickname = element nickname {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type }?,
    value-text-list
  }
*/
 public:
 vc_parameters 		Parameters; 
 vc_value_text_list	NicknameList;

 bool fLoadName(XMLNode NameNode);
 void fClear();
};

class vc_photo {
/*
# 6.2.4
property-photo = element photo {
    element parameters { param-altid, param-pid, param-pref, param-type,
                         param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters; 
 vc_value_uri        	URI; 

 bool fLoadPhoto(XMLNode PhotoNode);
 void fClear();
};

class vc_bday {
/*
# 6.2.5
property-bday = element bday {
    element parameters { param-altid, param-calscale }?,
    (value-date-and-or-time | value-text)
  }
*/
 public:
 vc_parameters 		Parameters; 
 vc_value_date		Date;
 vc_value_date_time	DateTime;
 vc_value_time		Time;
 vc_value_text		Text;

bool fLoadBday(XMLNode BdayNode);
void fClear();
};

class vc_anniversary {
/*
# 6.2.6
property-anniversary = element anniversary {
    element parameters { param-altid, param-calscale }?,
    (value-date-and-or-time | value-text)
  }
*/
 public:
 vc_parameters 		Parameters; 
 vc_value_date		Date;
 vc_value_date_time	DateTime;
 vc_value_time		Time;
 vc_value_text		Text;

bool fLoadAnniversary(XMLNode AnvNode);
void fClear();
};

class vc_gender {
/*
# 6.2.7
property-gender = element gender {
    element sex { "" | "M" | "F" | "O" | "N" | "U" },
    element identity { text }?
  }
*/
 public:
 string		strSex;
 string		strIdentity;

bool fLoadGender(XMLNode GenderNode);
void fClear();
};

class vc_adr {
/*
# 6.3.1
param-label = element label { value-text }?
property-adr = element adr {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type, param-geo, param-tz,
                         param-label }?,
    element pobox { text }+,
    element ext { text }+,
    element street { text }+,
    element locality { text }+,
    element region { text }+,
    element code { text }+,
    element country { text }+
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_type                Type;
 vc_label               Label;
 string			pobox;
 string			ext;
 string			street;
 string			locality;
 string			region;
 string			code;
 string			country;

 bool fLoadAdr(XMLNode AdrNode);
 void fClear();
};

class vc_tel {
/*
# 6.4.1
property-tel = element tel {
    element parameters {
      param-altid,
      param-pid,
      param-pref,
      element type {
        element text { "work" | "home" | "text" | "voice"
                     | "fax" | "cell" | "video" | "pager"
                     | "textphone" }+
      }?,
      param-mediatype
    }?,
    (value-text | value-uri)
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;
 vc_value_text		Text;

 bool fLoadTel( XMLNode TelNode);
 void fClear();
};

class vc_email {
/*
# 6.4.2
property-email = element email {
    element parameters { param-altid, param-pid, param-pref,
                         param-type }?,
    value-text
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_text		Text;

 bool fLoadEmail( XMLNode EmailNode);
 void fClear();
};

class vc_impp {
/*
# 6.4.3
property-impp = element impp {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    value-uri
  } 
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

 bool fLoadImpp(XMLNode ImppNode);
 void fClear();
};

class vc_lang {
/*
# 6.4.4
property-lang = element lang {
    element parameters { param-altid, param-pid, param-pref,
                         param-type }?,
    value-language-tag
  }
*/
 public:
 vector <vc_parameters> 		vParameters;
 vector <vc_value_language_tag>		vLanguageTag;

 bool fLoadLang(XMLNode LangNode);
 void fClear();
};

class vc_tz {
/*
# 6.5.1
property-tz = element tz {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    (value-text | value-uri | value-utc-offset)
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_text		Text;
 vc_value_uri		URI;
 vc_value_utc_offset	UTCoffset;

 bool fLoadTZ(XMLNode TZNode); // not implemented ignored by NENA
 void fClear();
};

class vc_geo {
/*
# 6.5.2
property-geo = element geo {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

 bool fLoadGeo(XMLNode GeoNode);
 void fClear();
};

class vc_title {
/*
# 6.6.1
property-title = element title {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type }?,
    value-text
  }

*/
 public:
 vc_parameters 		Parameters;
 vc_value_text		Text;

 bool fLoadTitle(XMLNode TitleNode);
 void fClear();
};

class vc_role {
/*
# 6.6.2
property-role = element role {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type }?,
    value-text
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_text		Text;

 bool fLoadRole(XMLNode RoleNode);
 void fClear(); 
};

class vc_logo {
/*
# 6.6.3
property-logo = element logo {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;			
};


class vc_org {
/*
# 6.6.4
property-org = element org {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type, param-sort-as }?,
    value-text-list
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_text_list 	Org;

 bool fLoadOrg(XMLNode OrgNode);
 void fClear();
};

class vc_member {
/*
# 6.6.5
property-member = element member {
    element parameters { param-altid, param-pid, param-pref,
                         param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri 		URI;

};

class vc_related {
/*
property-related = element related {
    element parameters {
      param-altid,
      param-pid,
      param-pref,
      element type {
        element text {
          "work" | "home" | "contact" | "acquaintance" |
          "friend" | "met" | "co-worker" | "colleague" | "co-resident" |
          "neighbor" | "child" | "parent" | "sibling" | "spouse" |
          "kin" | "muse" | "crush" | "date" | "sweetheart" | "me" |
          "agent" | "emergency"
        }+
      }?,
      param-mediatype
    }?,
    (value-uri | value-text)
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;
 vc_value_text		Text;

 bool fLoadRelated(XMLNode RelatedNode);
 void fClear(); 
};

class vc_categories {
/*
# 6.7.1
property-categories = element categories {
    element parameters { param-altid, param-pid, param-pref,
                         param-type }?,
    value-text-list
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_text_list	List;

};

class vc_note {
/*
# 6.7.2
property-note = element note {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type }?,
    value-text
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_text		Text;

 bool fLoadNote(XMLNode NoteNode);
 void fClear();
};

class vc_proid {
//# 6.7.3
// property-prodid = element prodid { value-text }

 public:
 vc_value_text		Text;

};
 
class vc_rev {
//# 6.7.4
// property-rev = element rev { value-timestamp }
 public:
 vc_value_timestamp	TimeStamp;

 bool fLoadRev(XMLNode RevNode);
 void fClear();
};

class vc_sound {
/*
# 6.7.5
property-sound = element sound {
    element parameters { param-language, param-altid, param-pid,
                         param-pref, param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

};

class vc_uid {
/*
# 6.7.6
property-uid = element uid { value-uri }
*/
 public:
 vc_value_uri		URI;

 bool fLoadUID(XMLNode UIDNode);
 void fClear();
};

class vc_clientpid_map {
/*
# 6.7.7
property-clientpidmap = element clientpidmap {
    element sourceid { xsd:positiveInteger },
    value-uri
  }
*/
 public:
 unsigned int		SourceId;
 vc_value_uri		URI;

};

class vc_url {
/*
# 6.7.8
property-url = element url {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

};

class vc_key {
/*
# 6.8.1
property-key = element key {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    (value-uri | value-text)
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;
 vc_value_text		Text;

};

class vc_fburl {
/*
# 6.9.1
property-fburl = element fburl {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

};

class vc_caladruri {
/*
# 6.9.2
property-caladruri = element caladruri {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

};

class vc_caluri {
/*
# 6.9.3
property-caluri = element caluri {
    element parameters { param-altid, param-pid, param-pref,
                         param-type, param-mediatype }?,
    value-uri
  }
*/
 public:
 vc_parameters 		Parameters;
 vc_value_uri		URI;

};


class vcard {
 public:
 vc_uid			uid;
 vc_kind		kind;
 vc_name		name;
 vc_full_name		fullname;
 vector<vc_nickname>	vNickname;
 vc_gender              gender;
 vector <vc_title>	vTitle;
 vector <vc_role>       vRole;
 vc_bday		birthday;
 vc_anniversary         anniversary;
 vc_lang                lang;
 vc_org			org;
 vector <vc_photo>	vPhoto;
 vector <vc_note>	vNote;
 vector <vc_email> 	vEmail;
 vc_geo			Geo;
 vc_tz			TZ;  // ignored
 vector <vc_tel>	vTel;
 vector <vc_adr>	vAdr;
 vector <vc_impp>	vImpp;
 vector <vc_related>	vRelated;
 vc_rev			Rev;

 // Constructor
 vcard();
 
 // Destructor
 ~vcard(){}

 //Copy Constructor
 vcard(const vcard *a);

 // Declare Assignment operator
 vcard& operator=(const vcard& a);

bool fLoadTel(XMLNode objTelNode);
bool fLoadTitle(XMLNode TitleNode);
bool fLoadRole(XMLNode RoleNode);
bool fLoadPhoto(XMLNode MainNode);
bool fLoadEmail(XMLNode MainNode);
bool fLoadGeo(XMLNode MainNode);
bool fLoadImpp(XMLNode MainNode);
bool fLoadNote(XMLNode MainNode);
bool fLoadRelated(XMLNode MainNode);
bool fLoadNicknames(XMLNode MainNode); 
bool fLoadAddresses(XMLNode MainNode);
int  fMainTelnumber();
void fClear();
};


class Service_Info {
/*
Value	|Description 				|ServiceEnvironment	|ServiceType	|ServiceMobility	
	|					|(ASVC1)		|(ASVC2)	|(ASVC3)
1 	|Residence 				|Residence 		|POTS 		|Fixed
2 	|Business 				|Business 		|POTS 		|Fixed
3 	|Residence PBX				|Residence 		|MLTS-hosted 	|Fixed
4 	|Business PBX 				|Business 		|MLTS-hosted 	|Fixed
5 	|Centrex 				|Business 		|MLTS-hosted 	|Fixed
6 	|Coin 1 way out 			|Business 		|one-way 	|Fixed
7 	|Coin 2 way 				|Business 		|coin 		|Fixed
8 	|Wireless Phase 0 			|unknown 		|Wireless 	|Mobile
9 	|Residence OPX 				|Residence 		|POTS 		|Fixed
0 	|Business OPX 				|Business 		|POTS 		|Fixed
A 	|Customer owned Coin 			|Business 		|coin 		|Fixed
B 	|Not Available 				|unknown 		|unknown 	|Unknown
C 	|VoIP Residence 			|Residence 		|VoIP 		|Fixed
D 	|VoIP Business 				|Business 		|VoIP 		|Fixed
E 	|VoIP Coin or Pay Phone			|Business 		|coin 		|Fixed
F 	|VoIP Wireless 				|unknown 		|VoIP 		|Mobile
J 	|VoIP Nomadic 				|unknown 		|VoIP 		|Nomadic
K 	|VoIP Enterprise 			|Business 		|VoIP 		|Unknown
G 	|Wireless Phase I 			|unknown 		|Wireless 	|Mobile
H 	|Wireless Phase II 			|unknown 		|Wireless 	|Mobile
I 	|Wireless Phase II returning Phase I	|unknown 		|Wireless 	|Mobile
V 	|Voice Over IP 				|unknown		|VoIP 		|Unknown

Note: to map legacy values to the NG9-1-1 data structure, see Table A-15-2 Class of Service
Mapping in NENA-STA-010.2-2016. In addition to the values included in the ADR data, the method
value from the PIDF-LO is required to determine Class of Service mapping.
For example, a method value of any value other than Cell combined with an ASVC2 value of
Wireless and an ASVC3 value of Mobile indicates a Wireless phase 2 call.
*/

 public:
 string		strDataProviderReference;
 string		strServiceEnvironment;
 string 	strServiceType;
 string		strServiceMobility;
// string         strLegacyClassOfService;
// string         strLegacyClassOfServiceCode;

 // Constructor
 Service_Info();
 
 // Destructor
 ~Service_Info(){}

 //Copy Constructor
 Service_Info(const Service_Info *a);

 // Declare Assignment operator
 Service_Info& operator=(const Service_Info& a);

 void fClear();
};

class Provider_Info {
 public:
 string		strDataProviderReference;
 string		strDataProviderString;
 string		strProviderID;
 string		strProviderIDSeries;
 string		strTypeOfProvider;
 string		strContactURI;
 string		strLanguage;
 vcard		vcardDataProviderContact;
 string		strSubcontractorPrincipal;
 string		strSubcontractorPriority;


 // Constructor
 Provider_Info();
 
 // Destructor
 ~Provider_Info(){}

 //Copy Constructor
 Provider_Info(const Provider_Info *a);

 // Declare Assignment operator
 Provider_Info& operator=(const Provider_Info& a);

 void fClear();
};

class Subscriber_Info {
 public:
 string		strDataProviderReference;
 vcard		vcardSubscriberData;
 bool           boolPrivacyRequested;

 // Constructor
 Subscriber_Info();
 
 // Destructor
 ~Subscriber_Info(){}

 //Copy Constructor
 Subscriber_Info(const Subscriber_Info *a);

 // Declare Assignment operator
 Subscriber_Info& operator=(const Subscriber_Info& a);

 void fClear();
};

class ADR_Data {
 public:

 Provider_Info   	ProviderInfo;
 Service_Info    	ServiceInfo;
 Subscriber_Info	SubscriberInfo;
 bool                   boolRequireProviderInfo;
 bool                   boolRequireServiceInfo;
 bool                   boolRequireSubsciberInfo;

 // Constructor
 ADR_Data();
 
 // Destructor
 ~ADR_Data(){}

 //Copy Constructor
 ADR_Data(const ADR_Data *a);

 // Declare Assignment operator
 ADR_Data& operator=(const ADR_Data& a);
 
bool fLoadSubscriberInfo(DataPacketIn objData, string* strError);
bool fLoadSubscriberInfo(XMLNode MainNode, string* strError);
bool fLoadServiceInfo(DataPacketIn objData, string* strError);
bool fLoadServiceInfo(XMLNode MainNode, string* strError);
bool fLoadProviderInfo(DataPacketIn objData, string* strError); 
bool fLoadProviderInfo(XMLNode MainNode, string* strError);
bool fLoadXcard(vcard* Vcard, XMLNode objXCardNode, string* strError);
bool fLoadProviderReference(string* strProviderReference, XMLNode objProviderReferenceNode, string* strError);
bool fLoadEmergencyCallData(XMLNode MainNode, string* strError);
void fClear();
};

class ADR_REQUIREMENTS {
 public:
 bool                   boolRequirePidfLo;
 bool                   boolRequireProviderInfo;
 bool                   boolRequireServiceInfo;
 bool                   boolRequireSubsciberInfo;

 ADR_REQUIREMENTS(){
  boolRequirePidfLo        = true;
  boolRequireProviderInfo  = true;
  boolRequireServiceInfo   = true;
  boolRequireSubsciberInfo = true;
 };
~ADR_REQUIREMENTS(){}

 void fSetalltrue();
 bool fAreAllFalse();
};


#endif

